from modules.data_reader import DataReader
from modules.lang_parser import LangEstimator, FastLangEstimator
from modules.topic_classifier import (prepare_english_df,
                                      train_catboost_on_stacked,
                                      preprocess_en_dataset_for_cb,
                                      preprocess_df_for_inference,
                                      infer_english_model,
                                      preprocess_ru_dataset_for_cb,
                                      CB_RU_FEATURES,
                                      CB_RU_CAT_FEATURES,
                                      train_lr, make_dump, load_dump,
                                      STR_2_INT_LABEL,
                                      infer_russian_model,
                                      RELEASE_TEXT_COLUMN)
from modules.news_filter import filter_news
from modules.thread_maker import RunPipeline
from modules.ranking_threads import ranking_threads
from argparse import ArgumentParser
import io
import pandas as pd
import sys
import os
import json

def stage_1(inp_dir, fast=False):
    data = DataReader(inp_dir).read()
    le = FastLangEstimator() if fast else LangEstimator()
    test_df_with_lang = le.estimate(data)
    return test_df_with_lang


def stage_2(df, en_dump_path='./en_news_filter_model', ru_dump_path='./ru_news_filter_model'):
    df = filter_news(df, en_dump_path=en_dump_path, ru_dump_path=ru_dump_path)
    return df


def stage_3(df, ru_dump='./ru_dump_without_nn', en_dump='./en_dump_without_nn'):
    restored_lr_model, restored_cb_model, restored_tfidf_vectorizer = load_dump(ru_dump)

    ru_df = preprocess_df_for_inference(df[df['title_estimated_lang'] == 'ru'], lang='ru')

    if ru_df.shape[0] > 0:
        cb_preds = infer_russian_model(restored_lr_model,
                                       restored_cb_model,
                                       ru_df,
                                       restored_tfidf_vectorizer)

        ru_df['category'] = cb_preds
    else:
        ru_df = pd.DataFrame({'filename': [], 'category': []})

    restored_lr_model, restored_cb_model, restored_tfidf_vectorizer = load_dump(en_dump)

    en_df = preprocess_df_for_inference(df[df['title_estimated_lang'] == 'en'], lang='en')

    if en_df.shape[0] > 0:
        cb_preds = infer_english_model(restored_lr_model,
                                       restored_cb_model,
                                       en_df,
                                       restored_tfidf_vectorizer)

        en_df['category'] = cb_preds
    else:
        en_df = pd.DataFrame({'filename': [], 'category': []})

    return pd.concat([ru_df, en_df])


def stage_4(df):
    df = RunPipeline(df)
    return df

def stage_5(df):
    return ranking_threads(df)

def parse_args():
    parser = ArgumentParser()
    parser.add_argument('mode')
    parser.add_argument('src_dir')
    args = parser.parse_args()
    if args.mode == 'language':
        df = stage_1(args.src_dir)
        ru_df = df[df['title_estimated_lang'] == 'ru']
        en_df = df[df['title_estimated_lang'] == 'en']
        ru_df['filename'] = ru_df['filename'].apply(lambda x: os.path.basename(x))
        en_df['filename'] = en_df['filename'].apply(lambda x: os.path.basename(x))
        res_ru = {
            'lang_code': 'ru',
            'articles': list(ru_df['filename'])
        }
        res_en = {
            'lang_code': 'en',
            'articles': list(en_df['filename'])
        }
        res = [res_ru, res_en]
        js = json.dumps(res, indent=4)
        print(js)
    elif args.mode == 'news':
        df = stage_1(args.src_dir, fast=True)
        df = stage_2(df)
        res = {
            'articles': list(df[df['is_news']]['filename'].apply(lambda x: os.path.basename(x)))
        }
        js = json.dumps(res, indent=4)
        print(js)
    elif args.mode == 'categories':
        df = stage_1(args.src_dir, fast=True)
        ru_df = df[df['title_estimated_lang'] == 'ru']
        en_df = df[df['title_estimated_lang'] == 'en']
        ru_df['filename'] = ru_df['filename'].apply(lambda x: os.path.basename(x))
        en_df['filename'] = en_df['filename'].apply(lambda x: os.path.basename(x))
        df = pd.concat([ru_df, en_df])
        df = stage_3(df)

        res = []
        for cat in STR_2_INT_LABEL:
            item = {
                'category': cat.lower(),
                'articles': list(df[df['category'] == cat]['filename'])
            }
            res.append(item)
        js = json.dumps(res, indent=4)
        print(js)
    elif args.mode == 'threads':
        df = stage_1(args.src_dir)
        ru_df = df[df['title_estimated_lang'] == 'ru']
        en_df = df[df['title_estimated_lang'] == 'en']
        df = pd.concat([ru_df, en_df])
        df = stage_4(df)
        df['filename'] = df['filename'].apply(lambda x: os.path.basename(x))
        res = []
        groups = df.groupby('thread')
        for i, g in groups:
            title = g.iloc[0]['thread_title']
            articles = list(g['filename'])
            item = {
                'title': title,
                'articles': articles
            }
            res.append(item)
        js = json.dumps(res, indent=4, ensure_ascii=False)
        print(js)

    elif args.mode == 'top':
        df = stage_1(args.src_dir, fast=True)
        ru_df = df[df['title_estimated_lang'] == 'ru']
        en_df = df[df['title_estimated_lang'] == 'en']
        df = pd.concat([ru_df, en_df])
        df = stage_3(df)
        df = stage_4(df)
        result_dict = stage_5(df)
        js = json.dumps(result_dict, indent=4, ensure_ascii=False)
        print(js)
        

def main():
    parse_args()


if __name__ == '__main__':
    main()
