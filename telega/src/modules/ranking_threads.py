import pandas as pd
import os

def thread_score(item):
    return len(item[1])

def get_ranked_threads(dataframe, any_threads=False):
    dataframe.reset_index(drop=True, inplace=True)
    thread_id_and_indesies = dataframe.groupby(['thread']).groups.items()
    sorted_threads = sorted(thread_id_and_indesies, key=thread_score, reverse=True)
    threads = []
    for thread_id_and_indesies in sorted_threads:
        first_article_int_thread = dataframe.loc[thread_id_and_indesies[1][0]]
        thread_info = {"title":  first_article_int_thread.title, 
                       "articles": []}
        if any_threads:
            thread_info["category"] = first_article_int_thread.category.lower()

        for article_id in thread_id_and_indesies[1]:
            thread_info["articles"].append(os.path.basename(dataframe.iloc[article_id].filename))

        threads.append(thread_info)  
        
    return threads

def ranking_threads(df):
    result_data = []
    
    any_dict = {"category": "any",
                "threads": get_ranked_threads(df, any_threads=True)}
    
    result_data.append(any_dict)

    for category, category_ids in df.groupby(["category"]).groups.items():
        category_dict = {"category": category.lower(),
                         "threads": get_ranked_threads(df.iloc[category_ids])}
        result_data.append(category_dict)
        
    return result_data