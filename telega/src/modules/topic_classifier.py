import catboost as cb
import pandas as pd
from sklearn.model_selection import train_test_split, KFold
import numpy as np
from sklearn.preprocessing import LabelBinarizer
import catboost
from tldextract import extract
from operator import itemgetter
from string import punctuation
from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.multiclass import OneVsRestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.naive_bayes import MultinomialNB
import pickle
import os
from modules.regexp_topic_classifier import patch_dataset_with_re_factors


MAX_SEQUENCE_LEN = 20
UNK = 'UNK'
PAD = 'PAD'
EMBEDDING_DIM = 50
HID_SIZE = 32
N_CATEGORIES = 7

PUNCTS_S = set(punctuation)

RELEASE_TEXT_COLUMN = 'text'
RELEASE_DICT_COLUMN = 'description'

STR_2_INT_LABEL = {
    'ECONOMY': 0,
    'SOCIETY': 1,
    'ENTERTAINMENT': 2,
    'SCIENCE': 3,
    'SPORTS': 4,
    'TECHNOLOGY': 5,
    'OTHER': 6
}

CATS_MAP_EN = {
    'ECONOMY': ['ECONOMY', 'MONEY',],
    'SOCIETY': ['WEDDINGS', 'WELLNESS', 'STYLE & BEAUTY', 'HOME & LIVING', 'PARENTING', 'POLITICS'],
    'ENTERTAINMENT': ['ARTS', 'ARTS & CULTURE', 'MEDIA'],
    'SCIENCE': ['SCIENCE',],
    'SPORTS': ['HEALTHY LIVING', 'SPORTS'],
    'TECHNOLOGY': ['TECH'],
    'OTHER': ['WEIRD NEWS',]
}


CATS_MAP_RU = {
    'ECONOMY': ['Экономика', 'Бизнес',],
    'SOCIETY': [
        # 'Россия',
        'Мир',
        'Интернет и СМИ',
        'Из жизни'
    ],
    'ENTERTAINMENT': ['Путешествия', 'Культура', 'Культпросвет '],
    'SCIENCE': ['Наука и техника'],
    'SPORTS': ['Спорт', 'ЧМ-2014', 'Сочи'],
    'TECHNOLOGY': ['Оружие'],
    'OTHER': ['Ценности', 'Легпром', 'МедНовости', 'Библиотека']
}


CB_EN_CAT_FEATURES = [
    'domain_prefix',
    'domain_suffix',
    'domain',
    'authors',
    'word_length',
    'weekday',
    'lr_pred',
    # 'regexp_estimated_POLITICS',
]

CB_EN_FEATURES = [
    'domain_prefix',
    'domain_suffix',
    'domain',
    'authors',
    'word_length',
    'weekday',
    'lr_pred',
    'regexp_estimated_POLITICS',
    'regexp_estimated_POLITICS_to_all_words_ratio',
] + ['lr_proba_' + k for k in STR_2_INT_LABEL]

CB_EN_INFERENCE_FEATURES = [
    'authors',
    'domain',
    'domain_suffix',
    'domain_prefix',
    'word_length',
    'weekday',
    'lr_pred',
    'regexp_estimated_POLITICS',
    'regexp_estimated_POLITICS_to_all_words_ratio',
] + ['lr_proba_' + k for k in STR_2_INT_LABEL]


CB_EN_INFERENCE_CAT_FEATURES = [
    'authors',
    'domain',
    'domain_suffix',
    'domain_prefix',
    'word_length',
    'weekday',
    'lr_pred',
    # 'regexp_estimated_POLITICS',
]

CB_RU_CAT_FEATURES = [
    'domain_prefix',
    'domain_suffix',
    'domain',
    # 'authors',
    'word_length',
    'weekday',
    'lr_pred',
    # 'regexp_estimated_POLITICS',
    # 'regexp_estimated_POLITICS2',
    # 'regexp_estimated_ECONOMICS',
    # 'regexp_estimated_ENTERTAINMENT',
    # 'regexp_estimated_SCIENCE',
    # 'regexp_estimated_SPORTS',
    # 'regexp_estimated_TECH',
]

CB_RU_FEATURES = [
    'domain_prefix',
    'domain_suffix',
    'domain',
    # 'authors',
    'word_length',
    'weekday',
    'regexp_estimated_POLITICS',
    'regexp_estimated_POLITICS_to_all_words_ratio',
    'regexp_estimated_POLITICS2',
    'regexp_estimated_POLITICS2_to_all_words_ratio',
    'regexp_estimated_ECONOMICS',
    'regexp_estimated_ECONOMICS_to_all_words_ratio',
    'regexp_estimated_ENTERTAINMENT',
    'regexp_estimated_ENTERTAINMENT_to_all_words_ratio',
    'regexp_estimated_SCIENCE',
    'regexp_estimated_SCIENCE_to_all_words_ratio',
    'regexp_estimated_SPORTS',
    'regexp_estimated_SPORTS_to_all_words_ratio',
    'regexp_estimated_TECH',
    'regexp_estimated_TECH_to_all_words_ratio',
    'lr_pred',
] + ['lr_proba_' + k for k in STR_2_INT_LABEL]

CB_RU_INFERENCE_FEATURES = [
    # 'author',
    'domain',
    'domain_suffix',
    'domain_prefix',
    'word_length',
    'weekday',
    'regexp_estimated_POLITICS',
    'regexp_estimated_POLITICS_to_all_words_ratio',
    'regexp_estimated_POLITICS2',
    'regexp_estimated_POLITICS2_to_all_words_ratio',
    'regexp_estimated_ECONOMICS',
    'regexp_estimated_ECONOMICS_to_all_words_ratio',
    'regexp_estimated_ENTERTAINMENT',
    'regexp_estimated_ENTERTAINMENT_to_all_words_ratio',
    'regexp_estimated_SCIENCE',
    'regexp_estimated_SCIENCE_to_all_words_ratio',
    'regexp_estimated_SPORTS',
    'regexp_estimated_SPORTS_to_all_words_ratio',
    'regexp_estimated_TECH',
    'regexp_estimated_TECH_to_all_words_ratio',
    'lr_pred',
] + ['lr_proba_' + k for k in STR_2_INT_LABEL]


CB_RU_INFERENCE_CAT_FEATURES = [
    # 'author',
    'domain',
    'domain_suffix',
    'domain_prefix',
    'word_length',
    'weekday',
    'lr_pred',
    # 'regexp_estimated_POLITICS',
    # 'regexp_estimated_POLITICS2',
    # 'regexp_estimated_ECONOMICS',
    # 'regexp_estimated_ENTERTAINMENT',
    # 'regexp_estimated_SCIENCE',
    # 'regexp_estimated_SPORTS',
    # 'regexp_estimated_TECH',
]


def get_tf_idfs(df, text_column, min_df=50):
    tfidf_vec = TfidfVectorizer(min_df=min_df, lowercase=True)
    tfidfs = tfidf_vec.fit_transform(df[text_column].apply(str))
    return tfidf_vec, tfidfs


def get_counts(df, text_column, min_df=50):
    count_vec = CountVectorizer(min_df=min_df, lowercase=True)
    counts = count_vec.fit_transform(df[text_column].apply(str))
    return count_vec, counts


def train_lr(df, text_column, target_column, min_df=50):
    lr = OneVsRestClassifier(LogisticRegression())
    labels = df[target_column].apply(lambda x: STR_2_INT_LABEL[x])
    vectorizer, tfidfs = get_tf_idfs(df, text_column, min_df=min_df)
    lr.fit(tfidfs, labels)
    return lr, vectorizer


def train_lda(df, text_column, target_column, min_df=100):
    lda = LinearDiscriminantAnalysis(tol=0.01)
    labels = df[target_column].apply(lambda x: STR_2_INT_LABEL[x])
    vectorizer, counts = get_counts(df, text_column, min_df=min_df)
    lda.fit(counts.toarray(), labels)
    return lda, vectorizer


def infer_lr(lr_model, tfidf_vectorizer, res_df, text_column):
    df = res_df.copy()
    tfidfs = tfidf_vectorizer.transform(df[text_column].apply(str))
    preds, preds_proba = lr_model.predict(tfidfs), lr_model.predict_proba(tfidfs)
    df['lr_pred'] = preds
    for k, i in STR_2_INT_LABEL.items():
        df['lr_proba_' + k] = preds_proba[:, i]
    return df, preds, preds_proba


def infer_lda(lda_model, count_vectorizer, res_df, text_column):
    df = res_df.copy()
    counts = count_vectorizer.transform(df[text_column].apply(str))
    preds, preds_proba = lda_model.predict(counts), lda_model.predict_proba(counts)
    df['lda_pred'] = preds
    for k, i in STR_2_INT_LABEL.items():
        df['lda_proba_' + k] = preds_proba[:, i]
    return df, preds, preds_proba


def filter_english_df(df):
    labels = {}
    for k, v in CATS_MAP_EN.items():
        for x in v:
            labels[x] = k
    new_df = df.copy()
    new_df['text'] = new_df.headline.str.lower() + " " + new_df.short_description.str.lower()
    new_df = new_df[new_df['category'].isin(labels)]
    new_df['category'] = new_df['category'].apply(lambda x: labels[x])
    return new_df


def filter_russian_df(df):
    labels = {}
    for k, v in CATS_MAP_RU.items():
        for x in v:
            labels[x] = k
    new_df = df.copy()
    new_df = new_df[new_df['topic'].isin(labels)]
    new_df['category'] = new_df['topic'].apply(lambda x: labels[x])
    return new_df


def prepare_english_df(path):
    df = pd.read_json(path, lines=True)
    return filter_english_df(df)


def prepare_russian_df(path):
    df = pd.read_csv(path)
    return filter_russian_df(df)


def preprocess_en_dataset_for_cb(src_df):
    df = src_df.copy()
    df['weekday'] = df.date.apply(lambda x: x.weekday())

    train_parsed_url = df.link.apply(extract)

    df['domain_prefix'] = train_parsed_url.apply(itemgetter(0))
    df['domain'] = train_parsed_url.apply(itemgetter(1))
    df['domain_suffix'] = train_parsed_url.apply(itemgetter(2))
    df['words'] = df[RELEASE_TEXT_COLUMN].apply(lambda x: ' '.join(x.split('\n')).count(' ') + 1)
    df['word_length'] = df.words.apply(lambda i: len(i))

    return df


def train_catboost_on_stacked(df,
                              category_column,
                              features=None,
                              cat_features=None,
                              eval_data=None,
                              iterations=1000,
                              plot=False):
    if features is None:
        features = CB_EN_FEATURES
    if cat_features is None:
        cat_features = CB_EN_CAT_FEATURES
    clf = catboost.CatBoostClassifier(iterations=iterations, custom_loss='MultiClass', eval_metric='Accuracy')
    tr_pool = catboost.Pool(data=df[features], label=df[category_column], cat_features=cat_features)

    if eval_data is not None:
        ts_pool = catboost.Pool(data=eval_data[features], label=eval_data[category_column], cat_features=cat_features)
        clf.fit(tr_pool, eval_set=ts_pool, plot=plot)
    else:
        clf.fit(tr_pool, plot=plot)
    return clf


def infer_catboost_model(model, df, features=None):
    if features is None:
        features = CB_EN_FEATURES
    preds = model.predict(df[features])
    return preds


def infer_english_model(lr_model,
                        cb_model,
                        test_en_data,
                        tfidf_vectorizer,
                        cb_features=None,
                        text_column=RELEASE_TEXT_COLUMN):
    if cb_features is None:
        cb_features = CB_EN_INFERENCE_FEATURES
    prep_en_data, prep_lr_preds, prep_lr_preds_proba = infer_lr(lr_model, tfidf_vectorizer, test_en_data, text_column)
    prep_en_data['authors'] = prep_en_data['author'].apply(lambda x: x if isinstance(x, str) else '')
    prep_en_data['domain_prefix'] = prep_en_data['domain_prefix'].apply(lambda x: x if isinstance(x, str) else '')
    prep_en_data['domain'] = prep_en_data['domain'].apply(lambda x: x if isinstance(x, str) else '')
    prep_en_data['domain_suffix'] = prep_en_data['domain_suffix'].apply(lambda x: x if isinstance(x, str) else '')
    cb_predictions = infer_catboost_model(cb_model, prep_en_data, cb_features)
    return cb_predictions


def infer_russian_model(lr_model,
                        cb_model,
                        test_en_data,
                        tfidf_vectorizer,
                        cb_features=None,
                        text_column=RELEASE_TEXT_COLUMN):
    if cb_features is None:
        cb_features = CB_RU_INFERENCE_FEATURES
    prep_en_data, prep_lr_preds, prep_lr_preds_proba = infer_lr(lr_model, tfidf_vectorizer, test_en_data, text_column)
    prep_en_data['authors'] = prep_en_data['author'].apply(lambda x: x if isinstance(x, str) else '')
    prep_en_data['domain_prefix'] = prep_en_data['domain_prefix'].apply(lambda x: x if isinstance(x, str) else '')
    prep_en_data['domain'] = prep_en_data['domain'].apply(lambda x: x if isinstance(x, str) else '')
    prep_en_data['domain_suffix'] = prep_en_data['domain_suffix'].apply(lambda x: x if isinstance(x, str) else '')
    cb_predictions = infer_catboost_model(cb_model, prep_en_data, cb_features)
    return cb_predictions


def str2weekday(s):
    return pd.datetime.strptime(s[:-6], '%Y-%m-%dT%H:%M:%S').weekday()


def preprocess_df_for_inference(src_df, lang='en'):
    df = src_df.copy()
    df[RELEASE_TEXT_COLUMN] = df['title'].apply(str) + ' ' + df['description'].apply(str)
    df = patch_dataset_with_re_factors(df, RELEASE_TEXT_COLUMN, max_len=200, lang=lang)
    df['weekday'] = df['published_time'].apply(str2weekday)
    df['word_length'] = df[RELEASE_TEXT_COLUMN].apply(lambda x: ' '.join(x.split('\n')).count(' ') + 1)
    return df


def extract_weekday_from_url(url):
    parsed = [x for x in url[len('https://lenta.ru/news/'):].split('/') if x]
    if not parsed[0][0].isdigit():
        parsed = parsed[1:]
        if not parsed[0][0].isdigit():
            return -1
    year = int(parsed[0])
    month = int(parsed[1])
    day = int(parsed[2])
    return pd.datetime(year, month, day).weekday()


def preprocess_ru_dataset_for_cb(src_df):
    df = src_df.copy()
    df['weekday'] = df.url.apply(extract_weekday_from_url)

    train_parsed_url = df.url.apply(extract)

    df['domain_prefix'] = train_parsed_url.apply(itemgetter(0))
    df['domain'] = train_parsed_url.apply(itemgetter(1))
    df['domain_suffix'] = train_parsed_url.apply(itemgetter(2))
    df['word_length'] = df[RELEASE_TEXT_COLUMN].apply(lambda x: ' '.join(x.split('\n')).count(' ') + 1)

    return df


def make_dump(lr_model, cb_model, tfidf_vectorizer, dir):
    if not os.path.exists(dir):
        os.mkdir(dir)
    cb_model.save_model(os.path.join(dir, 'catboost'))
    with open(os.path.join(dir, 'logreg'), 'wb') as f:
        pickle.dump(lr_model, f)
    with open(os.path.join(dir, 'tfidf_vectorizer'), 'wb') as f:
        pickle.dump(tfidf_vectorizer, f)


def load_dump(dir):
    cb_model = cb.CatBoostClassifier(custom_loss='MultiClass', eval_metric='Accuracy')
    cb_model = cb_model.load_model(os.path.join(dir, 'catboost'))
    with open(os.path.join(dir, 'tfidf_vectorizer'), 'rb') as f:
        tfidf_vectorizer = pickle.load(f)
    with open(os.path.join(dir, 'logreg'), 'rb') as f:
        lr_model = pickle.load(f)
    return lr_model, cb_model, tfidf_vectorizer


def get_fstrs(cb_model, features):
    return sorted(list(zip(cb_model.feature_importances_, features)), reverse=True)
