from sklearn.linear_model import LogisticRegression
from sklearn.multiclass import OneVsRestClassifier
import numpy as np
import pandas as pd
import pickle
import os
from string import punctuation


RU_TH = 0.1
EN_TH = 0.1


NEWS_ESTIMATION_TEXT_COL = 'concated_train_text'


def replace_punct(s):
    for p in punctuation:
        s = s.replace(p, ' ')
    return s


def take_first(x, n=170, reverse=False):
    words = [x for x in x.split(' ') if x]
    if reverse:
        words = list(reversed(words))
    cnt = 0
    taken = []
    for w in words:
        cnt += len(w)
        taken.append(w)
        if cnt >= n:
            return ' '.join(list(reversed(taken)))
    if reverse:
        return ' '.join(list(reversed(taken)))
    return ' '.join(taken)


def load_news_classification_model(dir):
    with open(os.path.join(dir, 'logreg'), 'rb') as f:
        lr = pickle.load(f)
    with open(os.path.join(dir, 'tfidf_vectorizer'), 'rb') as f:
        tfidf_vectorizer = pickle.load(f)
    return lr, tfidf_vectorizer


def save_news_classification_model(lr_model, tfidf_vectorizer, dir):
    if not os.path.exists(dir):
        os.mkdir(dir)
    with open(os.path.join(dir, 'logreg'), 'wb') as f:
        pickle.dump(lr_model, f)
    with open(os.path.join(dir, 'tfidf_vectorizer'), 'wb') as f:
        pickle.dump(tfidf_vectorizer, f)


def filter_news(df, en_dump_path='./en_news_filter_model', ru_dump_path='./ru_news_filter_model'):
    dfs = []
    for lang, path, th in [('en', en_dump_path, EN_TH), ('ru', ru_dump_path, RU_TH)]:
        lr_model, tfidf_vectorizer = load_news_classification_model(path)
        lang_df = df[df.title_estimated_lang == lang]
        if lang_df.shape[0] == 0:
            continue
        pred_texts = []

        for i in range(lang_df.shape[0]):
            n = lang_df.iloc[i]
            title = ''
            if isinstance(n.description, str):
                title = n.description
            text = ''
            if len(title) < 120:
                text = take_first(str(n.article), 170 - len(title))
            pred_texts.append(title + ' ' + text)
        lang_df[NEWS_ESTIMATION_TEXT_COL] = pred_texts
        tfidfs = tfidf_vectorizer.transform(lang_df[NEWS_ESTIMATION_TEXT_COL].apply(replace_punct))
        preds = lr_model.predict_proba(tfidfs)[:, 1] > th
        lang_df['is_news'] = preds
        lang_df.drop(columns=NEWS_ESTIMATION_TEXT_COL, inplace=True)
        dfs.append(lang_df)
    if len(dfs) == 0:
        return pd.DataFrame(data={'is_news': [], 'filename': []})
    return pd.concat(dfs)
