from string import ascii_lowercase, punctuation
from collections import defaultdict
from langdetect import detect
# from whatthelang import WhatTheLang


ru_stopwords = ['и',
 'в',
 'во',
 'не',
 'что',
 'он',
 'на',
 'я',
 'с',
 'со',
 'как',
 'а',
 'то',
 'все',
 'она',
 'так',
 'его',
 'но',
 'да',
 'ты',
 'к',
 'у',
 'же',
 'вы',
 'за',
 'бы',
 'по',
 'только',
 'ее',
 'мне',
 'было',
 'вот',
 'от',
 'меня',
 'еще',
 'нет',
 'о',
 'из',
 'ему',
 'теперь',
 'когда',
 'даже',
 'ну',
 'вдруг',
 'ли',
 'если',
 'уже',
 'или',
 'ни',
 'быть',
 'был',
 'него',
 'до',
 'вас',
 'нибудь',
 'опять',
 'уж',
 'вам',
 'ведь',
 'там',
 'потом',
 'себя',
 'ничего',
 'ей',
 'может',
 'они',
 'тут',
 'где',
 'есть',
 'надо',
 'ней',
 'для',
 'мы',
 'тебя',
 'их',
 'чем',
 'была',
 'сам',
 'чтоб',
 'без',
 'будто',
 'чего',
 'раз',
 'тоже',
 'себе',
 'под',
 'будет',
 'ж',
 'тогда',
 'кто',
 'этот',
 'того',
 'потому',
 'этого',
 'какой',
 'совсем',
 'ним',
 'здесь',
 'этом',
 'один',
 'почти',
 'мой',
 'тем',
 'чтобы',
 'нее',
 'сейчас',
 'были',
 'куда',
 'зачем',
 'всех',
 'никогда',
 'можно',
 'при',
 'наконец',
 'два',
 'об',
 'другой',
 'хоть',
 'после',
 'над',
 'больше',
 'тот',
 'через',
 'эти',
 'нас',
 'про',
 'всего',
 'них',
 'какая',
 'много',
 'разве',
 'три',
 'эту',
 'моя',
 'впрочем',
 'хорошо',
 'свою',
 'этой',
 'перед',
 'иногда',
 'лучше',
 'чуть',
 'том',
 'нельзя',
 'такой',
 'им',
 'более',
 'всегда',
 'конечно',
 'всю',
 'между']

en_stopwords = ['i',
 'me',
 'my',
 'myself',
 'we',
 'our',
 'ours',
 'ourselves',
 'you',
 "you're",
 "you've",
 "you'll",
 "you'd",
 'your',
 'yours',
 'yourself',
 'yourselves',
 'he',
 'him',
 'his',
 'himself',
 'she',
 "she's",
 'her',
 'hers',
 'herself',
 'it',
 "it's",
 'its',
 'itself',
 'they',
 'them',
 'their',
 'theirs',
 'themselves',
 'what',
 'which',
 'who',
 'whom',
 'this',
 'that',
 "that'll",
 'these',
 'those',
 'am',
 'is',
 'are',
 'was',
 'were',
 'be',
 'been',
 'being',
 'have',
 'has',
 'had',
 'having',
 'do',
 'does',
 'did',
 'doing',
 'a',
 'an',
 'the',
 'and',
 'but',
 'if',
 'or',
 'because',
 'as',
 'until',
 'while',
 'of',
 'at',
 'by',
 'for',
 'with',
 'about',
 'against',
 'between',
 'into',
 'through',
 'during',
 'before',
 'after',
 'above',
 'below',
 'to',
 'from',
 'up',
 'down',
 'in',
 'out',
 'on',
 'off',
 'over',
 'under',
 'again',
 'further',
 'then',
 'once',
 'here',
 'there',
 'when',
 'where',
 'why',
 'how',
 'all',
 'any',
 'both',
 'each',
 'few',
 'more',
 'most',
 'other',
 'some',
 'such',
 'no',
 'nor',
 'not',
 'only',
 'own',
 'same',
 'so',
 'than',
 'too',
 'very',
 's',
 't',
 'can',
 'will',
 'just',
 'don',
 "don't",
 'should',
 "should've",
 'now',
 'd',
 'll',
 'm',
 'o',
 're',
 've',
 'y',
 'ain',
 'aren',
 "aren't",
 'couldn',
 "couldn't",
 'didn',
 "didn't",
 'doesn',
 "doesn't",
 'hadn',
 "hadn't",
 'hasn',
 "hasn't",
 'haven',
 "haven't",
 'isn',
 "isn't",
 'ma',
 'mightn',
 "mightn't",
 'mustn',
 "mustn't",
 'needn',
 "needn't",
 'shan',
 "shan't",
 'shouldn',
 "shouldn't",
 'wasn',
 "wasn't",
 'weren',
 "weren't",
 'won',
 "won't",
 'wouldn',
 "wouldn't"]

ru_letters = set('абвгдеёжзийклмнопрстуфхцъыьэюя')
en_letters = set(ascii_lowercase)
punct_s = set(punctuation)

EN_KEYWORDS = set(en_stopwords)
RU_KEYWORDS = set(ru_stopwords)
EN_TH = 0.1
RU_TH = 0.1


class LangEstimator:
    def __init__(self, ru_stopwords_th=RU_TH, en_stopwords_th=EN_TH):
        self.RU_TH = ru_stopwords_th
        self.EN_TH = en_stopwords_th
        # self.wtl = WhatTheLang()

    def detect_language_freq(self, text):
        words = text.lower()
        text = ''.join(words)
        counts = defaultdict(int)
        for t in text:
            if t in punct_s:
                continue
            if t in ru_letters:
                counts['ru'] += 1
            elif t in en_letters:
                counts['en'] += 1
            else:
                counts['other'] += 1

        return words, counts

    def detect_language(self, text):
        # try:
        #     return self.wtl.predict_lang(text)
        # except:
        #     return 'other'
        words, hypos = self.detect_language_freq(text)
        if not hypos:
            return 'UNK'
        hypo_lang, hypo_freq = max(hypos.items(), key=lambda x: x[1])
        if hypo_lang == 'ru':
            stopwords_count = len([w for w in words if w in RU_KEYWORDS])
            if stopwords_count == 0 or stopwords_count / len(words) < self.RU_TH:
                return self._detect_language_hard(text)
        elif hypo_lang == 'en':
            stopwords_count = len([w for w in words if w in EN_KEYWORDS])
            if stopwords_count == 0 or stopwords_count / len(words) < self.EN_TH:
                return self._detect_language_hard(text)

        return self._detect_language_hard(text) # hypo_lang
#
    def estimate(self, df, estimate_by_title=True):
        res_df = df.copy()
        if estimate_by_title:
            res_df['title_estimated_lang'] = res_df['title'].apply(self.detect_language)
        else:
            res_df['content_estimated_lang'] = res_df['article'].apply(self.detect_language)
        return res_df

    def _detect_language_hard(self, text):
        try:
            return detect(text)
        except:
            return 'other'

    def estimate_hard(self, df):
        res_df = df.copy()
        res_df['estimated_lang'] = res_df['title'].apply(self._detect_language_hard)
        return res_df


class FastLangEstimator:
    def __init__(self):
        pass

    def detect_language_freq(self, text):
        words = text.lower()
        text = ''.join(words)
        counts = defaultdict(int)
        for t in text:
            if t in punct_s:
                continue
            if t in ru_letters:
                counts['ru'] += 1
            elif t in en_letters:
                counts['en'] += 1
            else:
                counts['other'] += 1

        return words, counts

    def detect_language(self, text):
        words, hypos = self.detect_language_freq(text)
        if not hypos:
            return 'other'
        hypo_lang, hypo_freq = max(hypos.items(), key=lambda x: x[1])
        return hypo_lang

    def estimate(self, df):
        res_df = df.copy()
        res_df['title_estimated_lang'] = res_df['title'].apply(self.detect_language)
        return res_df
