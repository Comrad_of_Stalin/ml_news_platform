from tqdm import tqdm
import pandas as pd
import numpy as np
from tldextract import extract
import os
from operator import itemgetter
import bs4
from multiprocessing import Pool

def procces_html(fname):

    def get_mat_prop(soup, prop):
            e = soup.find('meta', property=prop)
            return e['content'] if e else None

    def get_author(article):
        if article is None or article.address is None:
            return None
        q = soup.article.address.find('a', {'rel': 'author'})
        return q.text if q is not None else None
    
    try:
        with open(fname, encoding='utf-8') as f:
            contents = f.read()

            soup = bs4.BeautifulSoup(contents, 'lxml')

            result_dir = {
                'filename': fname,
                'article': soup.article.text,
                'url': get_mat_prop(soup, 'og:url'),
                'site_name': get_mat_prop(soup, 'og:site_name'),
                'published_time': get_mat_prop(soup, 'article:published_time'),
                'title': get_mat_prop(soup, 'og:title'),
                'description': get_mat_prop(soup, 'og:description'),
                'charset': soup.find('meta')['charset'],
                'author': get_author(soup.article)
            }

    except Exception as ex:
        print('EXCEPTION: ', ex)
        return None

    return result_dir


class DataReader:
    def __init__(self, folder, parsed_data_save_filename='./parsed/parsed_data.csv', error_log='reader_error_log.txt', do_backup=True):
        self.folder = folder
        self.FEATURES = ['filename',
                         'url',
                         'site_name',
                         'published_time',
                         'title',
                         'description',
                         'charset',
                         'article',
                         'author']
        self.parsed_data_file = parsed_data_save_filename
        self.error_log = error_log
        self.do_backup = do_backup

    def read(self):
        filenames = self._scan_filenames()
        with open(self.error_log, 'w') as e:
            pool = Pool(processes=10)
            results = pool.map(procces_html, filenames)
            results = [x for x in results if x is not None]

        df = pd.DataFrame(data=results)

        print('SH: ', df.shape)

        parsed_url = df.url.apply(extract)

        df['domain_prefix'] = parsed_url.apply(itemgetter(0))
        df['domain'] = parsed_url.apply(itemgetter(1))
        df['domain_suffix'] = parsed_url.apply(itemgetter(2))
        return df

    def _try_backup(self, df):
        try:
            df.to_csv(self.parsed_data_file)
        except Exception as ex:
            # with open(self.error_log) as f:
            #     print(str(ex), file=f)
            pass

    def _scan_filenames(self):
        filenames = []
        for p, _, fl in os.walk(self.folder):
            if fl and os.path.isfile(os.path.join(p, fl[0])):
                filenames.extend([os.path.join(p, f) for f in fl])
        return filenames


if __name__ == "__main__":
    reader = DataReader('./data')
    print(reader.read().head())
