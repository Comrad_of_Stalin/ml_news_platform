import pandas as pd
from langdetect import detect
from multiprocessing import Process
import multiprocessing
from collections import defaultdict
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.cluster import KMeans
from sklearn.feature_extraction.text import TfidfVectorizer
from natasha import NamesExtractor
import os
import spacy
import regex

ru_stopwords = ['и',
                'в',
                'во',
                'не',
                'что',
                'он',
                'на',
                'я',
                'с',
                'со',
                'как',
                'а',
                'то',
                'все',
                'она',
                'так',
                'его',
                'но',
                'да',
                'ты',
                'к',
                'у',
                'же',
                'вы',
                'за',
                'бы',
                'по',
                'только',
                'ее',
                'мне',
                'было',
                'вот',
                'от',
                'меня',
                'еще',
                'нет',
                'о',
                'из',
                'ему',
                'теперь',
                'когда',
                'даже',
                'ну',
                'вдруг',
                'ли',
                'если',
                'уже',
                'или',
                'ни',
                'быть',
                'был',
                'него',
                'до',
                'вас',
                'нибудь',
                'опять',
                'уж',
                'вам',
                'ведь',
                'там',
                'потом',
                'себя',
                'ничего',
                'ей',
                'может',
                'они',
                'тут',
                'где',
                'есть',
                'надо',
                'ней',
                'для',
                'мы',
                'тебя',
                'их',
                'чем',
                'была',
                'сам',
                'чтоб',
                'без',
                'будто',
                'чего',
                'раз',
                'тоже',
                'себе',
                'под',
                'будет',
                'ж',
                'тогда',
                'кто',
                'этот',
                'того',
                'потому',
                'этого',
                'какой',
                'совсем',
                'ним',
                'здесь',
                'этом',
                'один',
                'почти',
                'мой',
                'тем',
                'чтобы',
                'нее',
                'сейчас',
                'были',
                'куда',
                'зачем',
                'всех',
                'никогда',
                'можно',
                'при',
                'наконец',
                'два',
                'об',
                'другой',
                'хоть',
                'после',
                'над',
                'больше',
                'тот',
                'через',
                'эти',
                'нас',
                'про',
                'всего',
                'них',
                'какая',
                'много',
                'разве',
                'три',
                'эту',
                'моя',
                'впрочем',
                'хорошо',
                'свою',
                'этой',
                'перед',
                'иногда',
                'лучше',
                'чуть',
                'том',
                'нельзя',
                'такой',
                'им',
                'более',
                'всегда',
                'конечно',
                'всю',
                'между']

en_stopwords = ['i',
                'me',
                'my',
                'myself',
                'we',
                'our',
                'ours',
                'ourselves',
                'you',
                "you're",
                "you've",
                "you'll",
                "you'd",
                'your',
                'yours',
                'yourself',
                'yourselves',
                'he',
                'him',
                'his',
                'himself',
                'she',
                "she's",
                'her',
                'hers',
                'herself',
                'it',
                "it's",
                'its',
                'itself',
                'they',
                'them',
                'their',
                'theirs',
                'themselves',
                'what',
                'which',
                'who',
                'whom',
                'this',
                'that',
                "that'll",
                'these',
                'those',
                'am',
                'is',
                'are',
                'was',
                'were',
                'be',
                'been',
                'being',
                'have',
                'has',
                'had',
                'having',
                'do',
                'does',
                'did',
                'doing',
                'a',
                'an',
                'the',
                'and',
                'but',
                'if',
                'or',
                'because',
                'as',
                'until',
                'while',
                'of',
                'at',
                'by',
                'for',
                'with',
                'about',
                'against',
                'between',
                'into',
                'through',
                'during',
                'before',
                'after',
                'above',
                'below',
                'to',
                'from',
                'up',
                'down',
                'in',
                'out',
                'on',
                'off',
                'over',
                'under',
                'again',
                'further',
                'then',
                'once',
                'here',
                'there',
                'when',
                'where',
                'why',
                'how',
                'all',
                'any',
                'both',
                'each',
                'few',
                'more',
                'most',
                'other',
                'some',
                'such',
                'no',
                'nor',
                'not',
                'only',
                'own',
                'same',
                'so',
                'than',
                'too',
                'very',
                's',
                't',
                'can',
                'will',
                'just',
                'don',
                "don't",
                'should',
                "should've",
                'now',
                'd',
                'll',
                'm',
                'o',
                're',
                've',
                'y',
                'ain',
                'aren',
                "aren't",
                'couldn',
                "couldn't",
                'didn',
                "didn't",
                'doesn',
                "doesn't",
                'hadn',
                "hadn't",
                'hasn',
                "hasn't",
                'haven',
                "haven't",
                'isn',
                "isn't",
                'ma',
                'mightn',
                "mightn't",
                'mustn',
                "mustn't",
                'needn',
                "needn't",
                'shan',
                "shan't",
                'shouldn',
                "shouldn't",
                'wasn',
                "wasn't",
                'weren',
                "weren't",
                'won',
                "won't",
                'wouldn',
                "wouldn't"]


def detect_lang_ru_en(text):
    if regex.search(r'\p{IsCyrillic}', text):
        return 'ru'
    else:
        return 'en'


def get_list_of_tokens(text, ru_extractor, en_extractor):
    tokens = []
    lang = detect_lang_ru_en(text)
    if lang == 'ru':
        all_tokens = ru_extractor(text)
        for token in all_tokens:
            if token.fact.last is not None:
                token = token.fact.last
                tokens.append(token)
    else:
        doc = en_extractor(text)
        for token in doc.ents:
            if token.label_ == 'PERSON':
                full_name = token.text.split(' ')
                tokens.append(full_name[-1])

    return tokens


def extract_entities(df):
    df_result = df.copy()
    ru_extractor = NamesExtractor()
    en_extractor = spacy.load('./models/spacy_ner_model_en')
    df_result['Persons'] = df_result['title'].apply(lambda x: get_list_of_tokens(x, ru_extractor,
                                                                                 en_extractor))
    df_result['language'] = df_result['title'].apply(detect_lang_ru_en)
    return df_result


def extract_entities_parallel(cpu_amount, df):
    step = df.shape[0] // cpu_amount
    dataframes = []
    pool = multiprocessing.Pool(processes=cpu_amount)

    for cpu in range(cpu_amount):
        dataframes.append(df[cpu * step: min((cpu + 1) * step, df.shape[0])])

    results = pool.map(extract_entities, dataframes)

    return pd.concat(results)


def KmeansPersonQuorum(df):
    df = df.reset_index(drop=True)
    mlb = MultiLabelBinarizer()
    person = mlb.fit_transform(df.Persons)
    kmeans = KMeans(n_clusters=min(df.Persons.shape[0] // 2, 100), n_jobs=7, max_iter=10, n_init=4, random_state=42).fit(person)
    df_with_cluster = pd.concat([pd.DataFrame(kmeans.labels_, columns=['thread']), df], axis=1)
    spam_thread = df_with_cluster.thread.value_counts(ascending=False).index[0]
    df_filtered = df_with_cluster[df_with_cluster.thread != spam_thread]
    return df_filtered.drop(columns=['thread']).reset_index(drop=True)


def AssignThreadsToNews(df, stop_words):
    filtered_df = KmeansPersonQuorum(df)
    vectorizer = TfidfVectorizer(analyzer='word', stop_words=stop_words)
    X = vectorizer.fit_transform(filtered_df.article)
    kmeans = KMeans(n_clusters=min(filtered_df.shape[0] // 3, 1500), n_jobs=7, max_iter=30, n_init=5, random_state=42).fit(X)

    res_df = pd.concat([pd.DataFrame(kmeans.labels_, columns=['thread']), filtered_df], axis=1)

    df_with_title = res_df[['thread', 'title']].groupby('thread').first()
    df_with_title.reset_index(inplace=True)

    df_with_title['thread_title'] = df_with_title['title']
    df_with_title.drop('title', axis=1, inplace=True)

    res_df = res_df.join(df_with_title.set_index('thread'), on='thread', how='left')

    return res_df


def RunPipeline(df):
    df_with_entities = extract_entities_parallel(8, df)
    df_ru_final = AssignThreadsToNews(df_with_entities[df_with_entities.language == 'ru'], ru_stopwords)
    df_en_final = AssignThreadsToNews(df_with_entities[df_with_entities.language == 'en'], en_stopwords)
    df_en_final['thread'] = df_en_final['thread'] + len(df_ru_final['thread'].unique())
    return pd.concat([df_ru_final, df_en_final])
