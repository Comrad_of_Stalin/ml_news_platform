/* Generated code for Python module 'chardet.langcyrillicmodel'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_chardet$langcyrillicmodel" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_chardet$langcyrillicmodel;
PyDictObject *moduledict_chardet$langcyrillicmodel;

/* The declarations of module constants used, if any. */
static PyObject *const_tuple_f18bdee9536a45fe5ae287fb6355492a_tuple;
static PyObject *const_str_plain_IBM866_char_to_order_map;
static PyObject *const_str_plain_RussianLangModel;
static PyObject *const_str_plain_IBM866;
extern PyObject *const_str_plain___spec__;
static PyObject *const_tuple_9222b66a29def824eb1194a0241b88ae_tuple;
extern PyObject *const_str_plain_precedence_matrix;
static PyObject *const_str_digest_d834b118d24a3c0276d3f8779844d004;
extern PyObject *const_str_plain_charset_name;
extern PyObject *const_str_plain_keep_english_letter;
static PyObject *const_tuple_666844a46456f676cd51b40483cca4f3_tuple;
static PyObject *const_str_plain_win1251_char_to_order_map;
static PyObject *const_str_digest_0c808d739d4314a3145ee0e332b5d593;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_language;
static PyObject *const_tuple_534421f243659b6bda00351214dd02e2_tuple;
extern PyObject *const_str_plain_Ibm866Model;
static PyObject *const_float_0_976601;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_Win1251CyrillicModel;
static PyObject *const_tuple_b24235f9a763afce8962d2938259e649_tuple;
static PyObject *const_str_digest_6ce9cc579b89a18ecd64c58d6deebbd4;
static PyObject *const_str_plain_MacCyrillic;
extern PyObject *const_str_plain_char_to_order_map;
extern PyObject *const_str_digest_79273f970a414aa39a32d8f0b259063b;
extern PyObject *const_str_plain_Latin5CyrillicModel;
static PyObject *const_str_plain_IBM855_char_to_order_map;
extern PyObject *const_str_plain_origin;
static PyObject *const_tuple_f8e41cfab8cb474168ecd9a8e1d7cea1_tuple;
static PyObject *const_str_digest_e0dbb4d1b25dda2af5281efae8ac80d3;
static PyObject *const_tuple_ffd1e1f94e28d6aae394e365bb952ebe_tuple;
static PyObject *const_str_plain_IBM855;
static PyObject *const_str_plain_Russian;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_latin5_char_to_order_map;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_Ibm855Model;
extern PyObject *const_str_plain_Koi8rModel;
static PyObject *const_str_plain_KOI8R_char_to_order_map;
extern PyObject *const_str_plain_MacCyrillicModel;
extern PyObject *const_str_digest_aada818c73a52b8f999cbecf8cef2e77;
extern PyObject *const_str_plain_typical_positive_ratio;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_plain_macCyrillic_char_to_order_map;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_f18bdee9536a45fe5ae287fb6355492a_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 412680 ], 1285 );
    const_str_plain_IBM866_char_to_order_map = UNSTREAM_STRING_ASCII( &constant_bin[ 413965 ], 24, 1 );
    const_str_plain_RussianLangModel = UNSTREAM_STRING_ASCII( &constant_bin[ 413989 ], 16, 1 );
    const_str_plain_IBM866 = UNSTREAM_STRING_ASCII( &constant_bin[ 413965 ], 6, 1 );
    const_tuple_9222b66a29def824eb1194a0241b88ae_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 414005 ], 20485 );
    const_str_digest_d834b118d24a3c0276d3f8779844d004 = UNSTREAM_STRING_ASCII( &constant_bin[ 434490 ], 25, 0 );
    const_tuple_666844a46456f676cd51b40483cca4f3_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 434515 ], 1285 );
    const_str_plain_win1251_char_to_order_map = UNSTREAM_STRING_ASCII( &constant_bin[ 435800 ], 25, 1 );
    const_str_digest_0c808d739d4314a3145ee0e332b5d593 = UNSTREAM_STRING_ASCII( &constant_bin[ 435825 ], 6, 0 );
    const_tuple_534421f243659b6bda00351214dd02e2_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 435831 ], 1285 );
    const_float_0_976601 = UNSTREAM_FLOAT( &constant_bin[ 437116 ] );
    const_tuple_b24235f9a763afce8962d2938259e649_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 437124 ], 1285 );
    const_str_digest_6ce9cc579b89a18ecd64c58d6deebbd4 = UNSTREAM_STRING_ASCII( &constant_bin[ 438409 ], 28, 0 );
    const_str_plain_MacCyrillic = UNSTREAM_STRING_ASCII( &constant_bin[ 438437 ], 11, 1 );
    const_str_plain_IBM855_char_to_order_map = UNSTREAM_STRING_ASCII( &constant_bin[ 438448 ], 24, 1 );
    const_tuple_f8e41cfab8cb474168ecd9a8e1d7cea1_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 438472 ], 1285 );
    const_str_digest_e0dbb4d1b25dda2af5281efae8ac80d3 = UNSTREAM_STRING_ASCII( &constant_bin[ 439757 ], 34, 0 );
    const_tuple_ffd1e1f94e28d6aae394e365bb952ebe_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 439791 ], 1285 );
    const_str_plain_IBM855 = UNSTREAM_STRING_ASCII( &constant_bin[ 438448 ], 6, 1 );
    const_str_plain_Russian = UNSTREAM_STRING_ASCII( &constant_bin[ 413989 ], 7, 1 );
    const_str_plain_latin5_char_to_order_map = UNSTREAM_STRING_ASCII( &constant_bin[ 441076 ], 24, 1 );
    const_str_plain_KOI8R_char_to_order_map = UNSTREAM_STRING_ASCII( &constant_bin[ 441100 ], 23, 1 );
    const_str_plain_macCyrillic_char_to_order_map = UNSTREAM_STRING_ASCII( &constant_bin[ 441123 ], 29, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_chardet$langcyrillicmodel( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_c7eeae79af12d37842d2faad2d3a62fe;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_6ce9cc579b89a18ecd64c58d6deebbd4 );
    codeobj_c7eeae79af12d37842d2faad2d3a62fe = MAKE_CODEOBJ( module_filename_obj, const_str_digest_e0dbb4d1b25dda2af5281efae8ac80d3, 1, const_tuple_empty, 0, 0, CO_NOFREE );
}

// The module function declarations.


// The module function definitions.



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_chardet$langcyrillicmodel =
{
    PyModuleDef_HEAD_INIT,
    "chardet.langcyrillicmodel",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(chardet$langcyrillicmodel)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(chardet$langcyrillicmodel)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_chardet$langcyrillicmodel );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("chardet.langcyrillicmodel: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("chardet.langcyrillicmodel: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("chardet.langcyrillicmodel: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initchardet$langcyrillicmodel" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_chardet$langcyrillicmodel = Py_InitModule4(
        "chardet.langcyrillicmodel",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_chardet$langcyrillicmodel = PyModule_Create( &mdef_chardet$langcyrillicmodel );
#endif

    moduledict_chardet$langcyrillicmodel = MODULE_DICT( module_chardet$langcyrillicmodel );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_chardet$langcyrillicmodel,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_chardet$langcyrillicmodel,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_chardet$langcyrillicmodel,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_chardet$langcyrillicmodel,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_chardet$langcyrillicmodel );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_d834b118d24a3c0276d3f8779844d004, module_chardet$langcyrillicmodel );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_c7eeae79af12d37842d2faad2d3a62fe;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_c7eeae79af12d37842d2faad2d3a62fe = MAKE_MODULE_FRAME( codeobj_c7eeae79af12d37842d2faad2d3a62fe, module_chardet$langcyrillicmodel );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_c7eeae79af12d37842d2faad2d3a62fe );
    assert( Py_REFCNT( frame_c7eeae79af12d37842d2faad2d3a62fe ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c7eeae79af12d37842d2faad2d3a62fe );
#endif
    popFrameStack();

    assertFrameObject( frame_c7eeae79af12d37842d2faad2d3a62fe );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c7eeae79af12d37842d2faad2d3a62fe );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c7eeae79af12d37842d2faad2d3a62fe, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c7eeae79af12d37842d2faad2d3a62fe->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c7eeae79af12d37842d2faad2d3a62fe, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = const_tuple_f18bdee9536a45fe5ae287fb6355492a_tuple;
        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_KOI8R_char_to_order_map, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = const_tuple_b24235f9a763afce8962d2938259e649_tuple;
        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_win1251_char_to_order_map, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = const_tuple_ffd1e1f94e28d6aae394e365bb952ebe_tuple;
        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_latin5_char_to_order_map, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = const_tuple_f8e41cfab8cb474168ecd9a8e1d7cea1_tuple;
        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_macCyrillic_char_to_order_map, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = const_tuple_666844a46456f676cd51b40483cca4f3_tuple;
        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_IBM855_char_to_order_map, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = const_tuple_534421f243659b6bda00351214dd02e2_tuple;
        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_IBM866_char_to_order_map, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = const_tuple_9222b66a29def824eb1194a0241b88ae_tuple;
        UPDATE_STRING_DICT0( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        tmp_dict_key_1 = const_str_plain_char_to_order_map;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_KOI8R_char_to_order_map );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KOI8R_char_to_order_map );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_dict_value_1 = tmp_mvar_value_3;
        tmp_assign_source_11 = _PyDict_NewPresized( 6 );
        tmp_res = PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_precedence_matrix;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_dict_value_2 = tmp_mvar_value_4;
        tmp_res = PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_typical_positive_ratio;
        tmp_dict_value_3 = const_float_0_976601;
        tmp_res = PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_keep_english_letter;
        tmp_dict_value_4 = Py_False;
        tmp_res = PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_charset_name;
        tmp_dict_value_5 = const_str_digest_0c808d739d4314a3145ee0e332b5d593;
        tmp_res = PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_language;
        tmp_dict_value_6 = const_str_plain_Russian;
        tmp_res = PyDict_SetItem( tmp_assign_source_11, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_Koi8rModel, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        tmp_dict_key_7 = const_str_plain_char_to_order_map;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_win1251_char_to_order_map );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_win1251_char_to_order_map );
        }

        CHECK_OBJECT( tmp_mvar_value_5 );
        tmp_dict_value_7 = tmp_mvar_value_5;
        tmp_assign_source_12 = _PyDict_NewPresized( 6 );
        tmp_res = PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_precedence_matrix;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
        }

        CHECK_OBJECT( tmp_mvar_value_6 );
        tmp_dict_value_8 = tmp_mvar_value_6;
        tmp_res = PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_plain_typical_positive_ratio;
        tmp_dict_value_9 = const_float_0_976601;
        tmp_res = PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_keep_english_letter;
        tmp_dict_value_10 = Py_False;
        tmp_res = PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_11 = const_str_plain_charset_name;
        tmp_dict_value_11 = const_str_digest_79273f970a414aa39a32d8f0b259063b;
        tmp_res = PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_11, tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_12 = const_str_plain_language;
        tmp_dict_value_12 = const_str_plain_Russian;
        tmp_res = PyDict_SetItem( tmp_assign_source_12, tmp_dict_key_12, tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_Win1251CyrillicModel, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        tmp_dict_key_13 = const_str_plain_char_to_order_map;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_latin5_char_to_order_map );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_latin5_char_to_order_map );
        }

        CHECK_OBJECT( tmp_mvar_value_7 );
        tmp_dict_value_13 = tmp_mvar_value_7;
        tmp_assign_source_13 = _PyDict_NewPresized( 6 );
        tmp_res = PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_13, tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_14 = const_str_plain_precedence_matrix;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
        }

        CHECK_OBJECT( tmp_mvar_value_8 );
        tmp_dict_value_14 = tmp_mvar_value_8;
        tmp_res = PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_14, tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_15 = const_str_plain_typical_positive_ratio;
        tmp_dict_value_15 = const_float_0_976601;
        tmp_res = PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_15, tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_16 = const_str_plain_keep_english_letter;
        tmp_dict_value_16 = Py_False;
        tmp_res = PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_16, tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_17 = const_str_plain_charset_name;
        tmp_dict_value_17 = const_str_digest_aada818c73a52b8f999cbecf8cef2e77;
        tmp_res = PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_17, tmp_dict_value_17 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_18 = const_str_plain_language;
        tmp_dict_value_18 = const_str_plain_Russian;
        tmp_res = PyDict_SetItem( tmp_assign_source_13, tmp_dict_key_18, tmp_dict_value_18 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_Latin5CyrillicModel, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        PyObject *tmp_dict_key_22;
        PyObject *tmp_dict_value_22;
        PyObject *tmp_dict_key_23;
        PyObject *tmp_dict_value_23;
        PyObject *tmp_dict_key_24;
        PyObject *tmp_dict_value_24;
        tmp_dict_key_19 = const_str_plain_char_to_order_map;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_macCyrillic_char_to_order_map );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_macCyrillic_char_to_order_map );
        }

        CHECK_OBJECT( tmp_mvar_value_9 );
        tmp_dict_value_19 = tmp_mvar_value_9;
        tmp_assign_source_14 = _PyDict_NewPresized( 6 );
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_19, tmp_dict_value_19 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_20 = const_str_plain_precedence_matrix;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
        }

        CHECK_OBJECT( tmp_mvar_value_10 );
        tmp_dict_value_20 = tmp_mvar_value_10;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_20, tmp_dict_value_20 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_21 = const_str_plain_typical_positive_ratio;
        tmp_dict_value_21 = const_float_0_976601;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_21, tmp_dict_value_21 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_22 = const_str_plain_keep_english_letter;
        tmp_dict_value_22 = Py_False;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_22, tmp_dict_value_22 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_23 = const_str_plain_charset_name;
        tmp_dict_value_23 = const_str_plain_MacCyrillic;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_23, tmp_dict_value_23 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_24 = const_str_plain_language;
        tmp_dict_value_24 = const_str_plain_Russian;
        tmp_res = PyDict_SetItem( tmp_assign_source_14, tmp_dict_key_24, tmp_dict_value_24 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_MacCyrillicModel, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_dict_key_25;
        PyObject *tmp_dict_value_25;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_dict_key_26;
        PyObject *tmp_dict_value_26;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_dict_key_27;
        PyObject *tmp_dict_value_27;
        PyObject *tmp_dict_key_28;
        PyObject *tmp_dict_value_28;
        PyObject *tmp_dict_key_29;
        PyObject *tmp_dict_value_29;
        PyObject *tmp_dict_key_30;
        PyObject *tmp_dict_value_30;
        tmp_dict_key_25 = const_str_plain_char_to_order_map;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_IBM866_char_to_order_map );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IBM866_char_to_order_map );
        }

        CHECK_OBJECT( tmp_mvar_value_11 );
        tmp_dict_value_25 = tmp_mvar_value_11;
        tmp_assign_source_15 = _PyDict_NewPresized( 6 );
        tmp_res = PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_25, tmp_dict_value_25 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_26 = const_str_plain_precedence_matrix;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
        }

        CHECK_OBJECT( tmp_mvar_value_12 );
        tmp_dict_value_26 = tmp_mvar_value_12;
        tmp_res = PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_26, tmp_dict_value_26 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_27 = const_str_plain_typical_positive_ratio;
        tmp_dict_value_27 = const_float_0_976601;
        tmp_res = PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_27, tmp_dict_value_27 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_28 = const_str_plain_keep_english_letter;
        tmp_dict_value_28 = Py_False;
        tmp_res = PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_28, tmp_dict_value_28 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_29 = const_str_plain_charset_name;
        tmp_dict_value_29 = const_str_plain_IBM866;
        tmp_res = PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_29, tmp_dict_value_29 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_30 = const_str_plain_language;
        tmp_dict_value_30 = const_str_plain_Russian;
        tmp_res = PyDict_SetItem( tmp_assign_source_15, tmp_dict_key_30, tmp_dict_value_30 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_Ibm866Model, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_dict_key_31;
        PyObject *tmp_dict_value_31;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_dict_key_32;
        PyObject *tmp_dict_value_32;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_dict_key_33;
        PyObject *tmp_dict_value_33;
        PyObject *tmp_dict_key_34;
        PyObject *tmp_dict_value_34;
        PyObject *tmp_dict_key_35;
        PyObject *tmp_dict_value_35;
        PyObject *tmp_dict_key_36;
        PyObject *tmp_dict_value_36;
        tmp_dict_key_31 = const_str_plain_char_to_order_map;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_IBM855_char_to_order_map );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IBM855_char_to_order_map );
        }

        CHECK_OBJECT( tmp_mvar_value_13 );
        tmp_dict_value_31 = tmp_mvar_value_13;
        tmp_assign_source_16 = _PyDict_NewPresized( 6 );
        tmp_res = PyDict_SetItem( tmp_assign_source_16, tmp_dict_key_31, tmp_dict_value_31 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_32 = const_str_plain_precedence_matrix;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_RussianLangModel );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RussianLangModel );
        }

        CHECK_OBJECT( tmp_mvar_value_14 );
        tmp_dict_value_32 = tmp_mvar_value_14;
        tmp_res = PyDict_SetItem( tmp_assign_source_16, tmp_dict_key_32, tmp_dict_value_32 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_33 = const_str_plain_typical_positive_ratio;
        tmp_dict_value_33 = const_float_0_976601;
        tmp_res = PyDict_SetItem( tmp_assign_source_16, tmp_dict_key_33, tmp_dict_value_33 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_34 = const_str_plain_keep_english_letter;
        tmp_dict_value_34 = Py_False;
        tmp_res = PyDict_SetItem( tmp_assign_source_16, tmp_dict_key_34, tmp_dict_value_34 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_35 = const_str_plain_charset_name;
        tmp_dict_value_35 = const_str_plain_IBM855;
        tmp_res = PyDict_SetItem( tmp_assign_source_16, tmp_dict_key_35, tmp_dict_value_35 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_36 = const_str_plain_language;
        tmp_dict_value_36 = const_str_plain_Russian;
        tmp_res = PyDict_SetItem( tmp_assign_source_16, tmp_dict_key_36, tmp_dict_value_36 );
        assert( !(tmp_res != 0) );
        UPDATE_STRING_DICT1( moduledict_chardet$langcyrillicmodel, (Nuitka_StringObject *)const_str_plain_Ibm855Model, tmp_assign_source_16 );
    }

    return MOD_RETURN_VALUE( module_chardet$langcyrillicmodel );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
