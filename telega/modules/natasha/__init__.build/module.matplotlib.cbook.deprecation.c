/* Generated code for Python module 'matplotlib.cbook.deprecation'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_matplotlib$cbook$deprecation" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_matplotlib$cbook$deprecation;
PyDictObject *moduledict_matplotlib$cbook$deprecation;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain___delete__;
static PyObject *const_str_digest_26acb95b859af724e6a8c94f3e8d9076;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_index;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_digest_c152cc6b1c2a07ae832a9da408c82333;
extern PyObject *const_str_plain_catch_warnings;
static PyObject *const_tuple_str_plain_name_str_plain_signature_str_plain_POK_tuple;
extern PyObject *const_str_plain___name__;
static PyObject *const_tuple_3d9adb719d7f26b7c1d877aafb3665cd_tuple;
extern PyObject *const_str_plain_UserWarning;
static PyObject *const_str_plain_notes_header;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_tuple_4962842542ed13e68d3c735958ea22ec_tuple;
static PyObject *const_str_digest_91a49c69f4285f5d67501ac0224f36d6;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain___get__;
static PyObject *const_str_digest_5b4164fe172a091435bdedc3dba93f0d;
extern PyObject *const_str_plain_args;
static PyObject *const_str_digest_c99941a615f8d934a0079401e1843f4b;
extern PyObject *const_str_plain___exit__;
static PyObject *const_tuple_str_plain_param_str_plain_name_tuple;
static PyObject *const_str_plain__deprecated_property;
extern PyObject *const_str_plain_default;
static PyObject *const_str_digest_7c73872a1161663710c8a735d0adf9e1;
static PyObject *const_str_digest_baa534a2c591faaa7f7304271d345845;
extern PyObject *const_str_plain_ignore;
extern PyObject *const_str_plain_param;
extern PyObject *const_str_plain_instance;
extern PyObject *const_str_plain_removal;
extern PyObject *const_str_digest_50e4933a9d0fc470d2deeb63d403662b;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_6fa65115bdc4603b700ccce19105bbfc;
static PyObject *const_str_digest_9521915caf951ff5c39d8dd5f8a1fb93;
static PyObject *const_str_digest_618c96c176b977c5f504ed0a642f72f7;
static PyObject *const_tuple_c23ae7cb5dd8709b04397346a1d90fa8_tuple;
extern PyObject *const_str_plain_signature;
extern PyObject *const_str_plain_strip;
static PyObject *const_str_digest_4dc23b8033bde8f5594de299e9979616;
extern PyObject *const_str_plain___enter__;
static PyObject *const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple;
extern PyObject *const_str_plain___signature__;
extern PyObject *const_str_plain_func;
static PyObject *const_str_digest_9cb45d425ee90670d1d23be4f0d00a1d;
static PyObject *const_tuple_type_property_tuple;
static PyObject *const_str_digest_e25a340f340b508bcb2c9ba7a5c6dd60;
static PyObject *const_tuple_str_plain__deprecated_parameter_class_tuple_empty_tuple;
extern PyObject *const_str_plain_names;
extern PyObject *const_str_plain_bind;
static PyObject *const_tuple_44f3e9a432120f85e825f4f40b4a8f27_tuple;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_str_plain_wrapper;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_contextlib;
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_digest_4db78317356ed7c8749566230afe4ac3;
static PyObject *const_tuple_str_plain__warn_external_tuple;
extern PyObject *const_str_plain_alternative;
extern PyObject *const_str_plain_warnings;
static PyObject *const_str_digest_e40b4bce85d44a72d598c38ec3c8872d;
static PyObject *const_str_digest_2bb788eca591478fe8c4968926c3b184;
static PyObject *const_str_digest_20680fddcd5600f02b803ac07ad26db3;
static PyObject *const_str_digest_8af1b95219abe614597fa695e4d0622d;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_plain__generate_deprecation_warning;
static PyObject *const_str_digest_aef16e9fac7ee50acd7ddda112bc6970;
extern PyObject *const_str_plain_wraps;
static PyObject *const_str_digest_6c04cf4e6fd1ae25a30c888eaeb5f4cc;
extern PyObject *const_str_plain_fget;
extern PyObject *const_str_digest_2af24ee1ac2586d3cdf67e9808301ea2;
extern PyObject *const_str_plain_arguments;
static PyObject *const_dict_8e618b9c8c01085ba9af4b4138d93a54;
static PyObject *const_str_digest_9a45f3fe2ce4bccb7e2deeab00592029;
extern PyObject *const_str_plain_fset;
extern PyObject *const_str_plain_parameters;
static PyObject *const_tuple_b242710c6192a722fa0f4d5a18675914_tuple;
static PyObject *const_str_digest_21397472cac24efd9c899754fc958a99;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain__make_keyword_only;
extern PyObject *const_str_plain_simplefilter;
extern PyObject *const_str_plain_owner;
extern PyObject *const_str_plain__suppress_matplotlib_deprecation_warning;
extern PyObject *const_str_digest_618a63e015481c6ac4c16aadc3be5237;
extern PyObject *const_str_plain_message;
static PyObject *const_str_digest_b5569535a1e20d95bba28c4f9579bc66;
static PyObject *const_str_plain_warning_cls;
extern PyObject *const_str_plain_old;
extern PyObject *const_tuple_str_newline_tuple;
extern PyObject *const_str_plain_finalize;
static PyObject *const_str_digest_668fe558c901f4408742633d58c2006e;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_64b3bdb8b98db31bb95bfc994b2747d0;
extern PyObject *const_str_plain_attribute;
extern PyObject *const_str_plain_new;
static PyObject *const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple;
static PyObject *const_str_digest_564e3b63ea9764ae143ea40ab98b97f0;
static PyObject *const_str_digest_d78eb6a8aaa1023c3a418ab943b242ac;
extern PyObject *const_str_plain_name;
static PyObject *const_tuple_4a447ae08f02a862f84b1e940737f3de_tuple;
static PyObject *const_str_digest_4dd76f99f7cda49e24f2ec6902453b96;
static PyObject *const_str_digest_7e58ec12901653a99ccbe6c630247a0f;
extern PyObject *const_str_plain_bound;
static PyObject *const_str_digest_e35b4ca67805dd83ae05fba930bdb412;
static PyObject *const_str_digest_09349449244a66b7d1fc2b9fa7edf9d0;
static PyObject *const_str_digest_7351bd1520ee1e45bba392f411640a43;
static PyObject *const_str_plain__deprecated_parameter_class;
extern PyObject *const_str_plain__deprecated_parameter;
extern PyObject *const_str_plain_MatplotlibDeprecationWarning;
static PyObject *const_str_digest_1ed0834fb44639b6c86bc76dc2a7d4b6;
extern PyObject *const_str_plain_contextmanager;
static PyObject *const_str_plain_since;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_str_digest_fdbb208f62d71b18577f0b1117f34570;
static PyObject *const_tuple_da87431ac29c1a04c8d6d0f3b3fc2bb6_tuple;
extern PyObject *const_str_plain_PendingDeprecationWarning;
extern PyObject *const_str_plain_pop;
static PyObject *const_str_digest_2d03aa0e4987d339d81bf09c043a8b47;
static PyObject *const_dict_c3a87ba873c63ff91ba7b1a628042fcb;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_obj_type;
static PyObject *const_str_digest_4e6d4d30ab12f2dea40a4c4c87d51b05;
static PyObject *const_str_digest_c324388e8872f2163c30f519b2d58f8a;
static PyObject *const_str_digest_893a7ac8b87a8aa2fd33f50d9212578f;
static PyObject *const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple;
extern PyObject *const_str_digest_25731c733fd74e8333aa29126ce85686;
static PyObject *const_str_digest_f13229e14be656569ff96e8edb661822;
static PyObject *const_str_digest_b476cba358e3693377c097f885e27bc3;
static PyObject *const_dict_577b6678bf9964110fef48548911d3aa;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_40b735750604d760d22884177c72c9b4;
static PyObject *const_str_digest_cc52526bc81fb768b83183d7748c8ba6;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_digest_72fd6686a1bcf90a38f58fc375cd915c;
extern PyObject *const_str_angle_listcomp;
static PyObject *const_str_digest_7654d5c228106810e555b45def28becc;
extern PyObject *const_str_plain_warning;
static PyObject *const_str_plain_KWO;
extern PyObject *const_str_plain_fdel;
extern PyObject *const_str_plain_pending;
static PyObject *const_str_digest_bcd6e8071a1b0f259058c810a8077cf3;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_class;
extern PyObject *const_str_plain_deprecated;
static PyObject *const_str_digest_0e3811383b231b7c65c4082d4334c412;
static PyObject *const_str_digest_f76bbb1cda8fc672dad73d81f6efb3b6;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_POSITIONAL_OR_KEYWORD;
static PyObject *const_str_digest_df5f2b7198e185abac8602ca838db37a;
extern PyObject *const_str_plain__;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_deprecate;
static PyObject *const_tuple_str_plain_param_str_plain_kwonly_tuple;
static PyObject *const_str_digest_fc9cad0e6ace3451e8288fbfcf53ad88;
extern PyObject *const_str_plain___module__;
static PyObject *const_tuple_b38c1c33ffba006c73a8ae2a1546149c_tuple;
extern PyObject *const_str_plain_functools;
extern PyObject *const_str_plain_warn_deprecated;
static PyObject *const_tuple_be47f1afd36e8dbac94a95aead116e13_tuple;
static PyObject *const_str_digest_d8486a1b232bbac034ebcb89ee222341;
extern PyObject *const_str_plain__delete_parameter;
extern PyObject *const_str_plain_kind;
extern PyObject *const_str_plain_doc;
extern PyObject *const_str_plain__warn_external;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_partial;
extern PyObject *const_str_plain_replace;
extern PyObject *const_str_plain_values;
extern PyObject *const_str_plain_KEYWORD_ONLY;
static PyObject *const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple;
static PyObject *const_str_plain_old_doc;
extern PyObject *const_str_plain_Parameter;
static PyObject *const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_func_tuple;
extern PyObject *const_str_plain__rename_parameter;
extern PyObject *const_str_newline;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_inspect;
static PyObject *const_str_plain_new_doc;
extern PyObject *const_str_plain_function;
extern PyObject *const_str_plain___repr__;
static PyObject *const_str_digest_b7663a8fc994f1116640c4cbdb8b5d0b;
static PyObject *const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple;
static PyObject *const_tuple_b914f6dc85e7e57ab7e4b12b20ba9de3_tuple;
static PyObject *const_str_plain_kwonly;
static PyObject *const_str_plain_POK;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_mplDeprecation;
extern PyObject *const_str_plain_addendum;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_obj;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_property;
extern PyObject *const_str_empty;
static PyObject *const_tuple_337cc82e4c9f5c22938c1b22943c0ec9_tuple;
extern PyObject *const_str_plain_cleandoc;
static PyObject *const_dict_288ceb0c92afcb06d9d92fe674369750;
static PyObject *const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_obj_tuple;
extern PyObject *const_str_plain___set__;
static PyObject *const_str_digest_39eaa473e3cbe47ce40ea0eb73c1ad7e;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_26acb95b859af724e6a8c94f3e8d9076 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873370 ], 6, 0 );
    const_str_digest_c152cc6b1c2a07ae832a9da408c82333 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873376 ], 70, 0 );
    const_tuple_str_plain_name_str_plain_signature_str_plain_POK_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_signature_str_plain_POK_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_signature_str_plain_POK_tuple, 1, const_str_plain_signature ); Py_INCREF( const_str_plain_signature );
    const_str_plain_POK = UNSTREAM_STRING_ASCII( &constant_bin[ 1873446 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_signature_str_plain_POK_tuple, 2, const_str_plain_POK ); Py_INCREF( const_str_plain_POK );
    const_tuple_3d9adb719d7f26b7c1d877aafb3665cd_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_3d9adb719d7f26b7c1d877aafb3665cd_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_3d9adb719d7f26b7c1d877aafb3665cd_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_3d9adb719d7f26b7c1d877aafb3665cd_tuple, 2, const_str_plain_arguments ); Py_INCREF( const_str_plain_arguments );
    PyTuple_SET_ITEM( const_tuple_3d9adb719d7f26b7c1d877aafb3665cd_tuple, 3, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_3d9adb719d7f26b7c1d877aafb3665cd_tuple, 4, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_plain_since = UNSTREAM_STRING_ASCII( &constant_bin[ 48869 ], 5, 1 );
    PyTuple_SET_ITEM( const_tuple_3d9adb719d7f26b7c1d877aafb3665cd_tuple, 5, const_str_plain_since ); Py_INCREF( const_str_plain_since );
    const_str_plain_notes_header = UNSTREAM_STRING_ASCII( &constant_bin[ 1873449 ], 12, 1 );
    const_tuple_4962842542ed13e68d3c735958ea22ec_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_4962842542ed13e68d3c735958ea22ec_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_4962842542ed13e68d3c735958ea22ec_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_4962842542ed13e68d3c735958ea22ec_tuple, 2, const_str_plain_old ); Py_INCREF( const_str_plain_old );
    PyTuple_SET_ITEM( const_tuple_4962842542ed13e68d3c735958ea22ec_tuple, 3, const_str_plain_since ); Py_INCREF( const_str_plain_since );
    PyTuple_SET_ITEM( const_tuple_4962842542ed13e68d3c735958ea22ec_tuple, 4, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_4962842542ed13e68d3c735958ea22ec_tuple, 5, const_str_plain_new ); Py_INCREF( const_str_plain_new );
    const_str_digest_91a49c69f4285f5d67501ac0224f36d6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1578480 ], 18, 0 );
    const_str_digest_5b4164fe172a091435bdedc3dba93f0d = UNSTREAM_STRING_ASCII( &constant_bin[ 1873461 ], 26, 0 );
    const_str_digest_c99941a615f8d934a0079401e1843f4b = UNSTREAM_STRING_ASCII( &constant_bin[ 1873487 ], 13, 0 );
    const_tuple_str_plain_param_str_plain_name_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_param_str_plain_name_tuple, 0, const_str_plain_param ); Py_INCREF( const_str_plain_param );
    PyTuple_SET_ITEM( const_tuple_str_plain_param_str_plain_name_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_plain__deprecated_property = UNSTREAM_STRING_ASCII( &constant_bin[ 1873415 ], 20, 1 );
    const_str_digest_7c73872a1161663710c8a735d0adf9e1 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873500 ], 37, 0 );
    const_str_digest_baa534a2c591faaa7f7304271d345845 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873376 ], 29, 0 );
    const_str_digest_6fa65115bdc4603b700ccce19105bbfc = UNSTREAM_STRING_ASCII( &constant_bin[ 1873537 ], 53, 0 );
    const_str_digest_9521915caf951ff5c39d8dd5f8a1fb93 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873590 ], 17, 0 );
    const_str_digest_618c96c176b977c5f504ed0a642f72f7 = UNSTREAM_STRING_ASCII( &constant_bin[ 1256384 ], 27, 0 );
    const_tuple_c23ae7cb5dd8709b04397346a1d90fa8_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_c23ae7cb5dd8709b04397346a1d90fa8_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_c23ae7cb5dd8709b04397346a1d90fa8_tuple, 1, const_str_plain_instance ); Py_INCREF( const_str_plain_instance );
    PyTuple_SET_ITEM( const_tuple_c23ae7cb5dd8709b04397346a1d90fa8_tuple, 2, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_c23ae7cb5dd8709b04397346a1d90fa8_tuple, 3, const_str_plain__warn_external ); Py_INCREF( const_str_plain__warn_external );
    PyTuple_SET_ITEM( const_tuple_c23ae7cb5dd8709b04397346a1d90fa8_tuple, 4, const_str_plain_warning ); Py_INCREF( const_str_plain_warning );
    PyTuple_SET_ITEM( const_tuple_c23ae7cb5dd8709b04397346a1d90fa8_tuple, 5, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_4dc23b8033bde8f5594de299e9979616 = UNSTREAM_STRING_ASCII( &constant_bin[ 1578464 ], 34, 0 );
    const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 0, const_str_plain_since ); Py_INCREF( const_str_plain_since );
    PyTuple_SET_ITEM( const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 1, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 3, const_str_plain_alternative ); Py_INCREF( const_str_plain_alternative );
    PyTuple_SET_ITEM( const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 4, const_str_plain_pending ); Py_INCREF( const_str_plain_pending );
    PyTuple_SET_ITEM( const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 5, const_str_plain_obj_type ); Py_INCREF( const_str_plain_obj_type );
    PyTuple_SET_ITEM( const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 6, const_str_plain_addendum ); Py_INCREF( const_str_plain_addendum );
    PyTuple_SET_ITEM( const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 7, const_str_plain_removal ); Py_INCREF( const_str_plain_removal );
    PyTuple_SET_ITEM( const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 8, const_str_plain_warning ); Py_INCREF( const_str_plain_warning );
    PyTuple_SET_ITEM( const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 9, const_str_plain__warn_external ); Py_INCREF( const_str_plain__warn_external );
    const_str_digest_9cb45d425ee90670d1d23be4f0d00a1d = UNSTREAM_STRING_ASCII( &constant_bin[ 1873607 ], 24, 0 );
    const_tuple_type_property_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_type_property_tuple, 0, (PyObject *)&PyProperty_Type ); Py_INCREF( (PyObject *)&PyProperty_Type );
    const_str_digest_e25a340f340b508bcb2c9ba7a5c6dd60 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873631 ], 47, 0 );
    const_tuple_str_plain__deprecated_parameter_class_tuple_empty_tuple = PyTuple_New( 2 );
    const_str_plain__deprecated_parameter_class = UNSTREAM_STRING_ASCII( &constant_bin[ 1873678 ], 27, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__deprecated_parameter_class_tuple_empty_tuple, 0, const_str_plain__deprecated_parameter_class ); Py_INCREF( const_str_plain__deprecated_parameter_class );
    PyTuple_SET_ITEM( const_tuple_str_plain__deprecated_parameter_class_tuple_empty_tuple, 1, const_tuple_empty ); Py_INCREF( const_tuple_empty );
    const_tuple_44f3e9a432120f85e825f4f40b4a8f27_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_44f3e9a432120f85e825f4f40b4a8f27_tuple, 0, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_44f3e9a432120f85e825f4f40b4a8f27_tuple, 1, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_44f3e9a432120f85e825f4f40b4a8f27_tuple, 2, const_str_empty ); Py_INCREF( const_str_empty );
    PyTuple_SET_ITEM( const_tuple_44f3e9a432120f85e825f4f40b4a8f27_tuple, 3, Py_False ); Py_INCREF( Py_False );
    PyTuple_SET_ITEM( const_tuple_44f3e9a432120f85e825f4f40b4a8f27_tuple, 4, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyTuple_SET_ITEM( const_tuple_44f3e9a432120f85e825f4f40b4a8f27_tuple, 5, const_str_empty ); Py_INCREF( const_str_empty );
    const_tuple_str_plain__warn_external_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__warn_external_tuple, 0, const_str_plain__warn_external ); Py_INCREF( const_str_plain__warn_external );
    const_str_digest_e40b4bce85d44a72d598c38ec3c8872d = UNSTREAM_STRING_ASCII( &constant_bin[ 969596 ], 6, 0 );
    const_str_digest_2bb788eca591478fe8c4968926c3b184 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873705 ], 15, 0 );
    const_str_digest_20680fddcd5600f02b803ac07ad26db3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873720 ], 39, 0 );
    const_str_digest_8af1b95219abe614597fa695e4d0622d = UNSTREAM_STRING_ASCII( &constant_bin[ 1873759 ], 39, 0 );
    const_str_plain__generate_deprecation_warning = UNSTREAM_STRING_ASCII( &constant_bin[ 1873798 ], 29, 1 );
    const_str_digest_aef16e9fac7ee50acd7ddda112bc6970 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873827 ], 67, 0 );
    const_str_digest_6c04cf4e6fd1ae25a30c888eaeb5f4cc = UNSTREAM_STRING_ASCII( &constant_bin[ 1873894 ], 35, 0 );
    const_dict_8e618b9c8c01085ba9af4b4138d93a54 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_8e618b9c8c01085ba9af4b4138d93a54, const_str_plain_removal, const_str_empty );
    assert( PyDict_Size( const_dict_8e618b9c8c01085ba9af4b4138d93a54 ) == 1 );
    const_str_digest_9a45f3fe2ce4bccb7e2deeab00592029 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873929 ], 46, 0 );
    const_tuple_b242710c6192a722fa0f4d5a18675914_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b242710c6192a722fa0f4d5a18675914_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_b242710c6192a722fa0f4d5a18675914_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_b242710c6192a722fa0f4d5a18675914_tuple, 2, const_str_plain__warn_external ); Py_INCREF( const_str_plain__warn_external );
    PyTuple_SET_ITEM( const_tuple_b242710c6192a722fa0f4d5a18675914_tuple, 3, const_str_plain_warning ); Py_INCREF( const_str_plain_warning );
    PyTuple_SET_ITEM( const_tuple_b242710c6192a722fa0f4d5a18675914_tuple, 4, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    const_str_digest_21397472cac24efd9c899754fc958a99 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873975 ], 25, 0 );
    const_str_digest_b5569535a1e20d95bba28c4f9579bc66 = UNSTREAM_STRING_ASCII( &constant_bin[ 1874000 ], 34, 0 );
    const_str_plain_warning_cls = UNSTREAM_STRING_ASCII( &constant_bin[ 1874034 ], 11, 1 );
    const_str_digest_668fe558c901f4408742633d58c2006e = UNSTREAM_STRING_ASCII( &constant_bin[ 1874045 ], 650, 0 );
    const_str_digest_64b3bdb8b98db31bb95bfc994b2747d0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1874695 ], 405, 0 );
    const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple = PyTuple_New( 17 );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 0, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 1, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 3, const_str_plain_alternative ); Py_INCREF( const_str_plain_alternative );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 4, const_str_plain_pending ); Py_INCREF( const_str_plain_pending );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 5, const_str_plain_obj_type ); Py_INCREF( const_str_plain_obj_type );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 6, const_str_plain_addendum ); Py_INCREF( const_str_plain_addendum );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 7, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    const_str_plain_old_doc = UNSTREAM_STRING_ASCII( &constant_bin[ 1875100 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 8, const_str_plain_old_doc ); Py_INCREF( const_str_plain_old_doc );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 9, const_str_plain_finalize ); Py_INCREF( const_str_plain_finalize );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 10, const_str_plain__deprecated_property ); Py_INCREF( const_str_plain__deprecated_property );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 11, const_str_plain_warning ); Py_INCREF( const_str_plain_warning );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 12, const_str_plain_wrapper ); Py_INCREF( const_str_plain_wrapper );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 13, const_str_plain_notes_header ); Py_INCREF( const_str_plain_notes_header );
    const_str_plain_new_doc = UNSTREAM_STRING_ASCII( &constant_bin[ 1875107 ], 7, 1 );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 14, const_str_plain_new_doc ); Py_INCREF( const_str_plain_new_doc );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 15, const_str_plain_since ); Py_INCREF( const_str_plain_since );
    PyTuple_SET_ITEM( const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 16, const_str_plain_removal ); Py_INCREF( const_str_plain_removal );
    const_str_digest_564e3b63ea9764ae143ea40ab98b97f0 = UNSTREAM_STRING_ASCII( &constant_bin[ 1875114 ], 2244, 0 );
    const_str_digest_d78eb6a8aaa1023c3a418ab943b242ac = UNSTREAM_STRING_ASCII( &constant_bin[ 1877358 ], 29, 0 );
    const_tuple_4a447ae08f02a862f84b1e940737f3de_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_4a447ae08f02a862f84b1e940737f3de_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_4a447ae08f02a862f84b1e940737f3de_tuple, 1, const_str_plain_instance ); Py_INCREF( const_str_plain_instance );
    PyTuple_SET_ITEM( const_tuple_4a447ae08f02a862f84b1e940737f3de_tuple, 2, const_str_plain_owner ); Py_INCREF( const_str_plain_owner );
    PyTuple_SET_ITEM( const_tuple_4a447ae08f02a862f84b1e940737f3de_tuple, 3, const_str_plain__warn_external ); Py_INCREF( const_str_plain__warn_external );
    PyTuple_SET_ITEM( const_tuple_4a447ae08f02a862f84b1e940737f3de_tuple, 4, const_str_plain_warning ); Py_INCREF( const_str_plain_warning );
    PyTuple_SET_ITEM( const_tuple_4a447ae08f02a862f84b1e940737f3de_tuple, 5, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_4dd76f99f7cda49e24f2ec6902453b96 = UNSTREAM_STRING_ASCII( &constant_bin[ 1877387 ], 20, 0 );
    const_str_digest_7e58ec12901653a99ccbe6c630247a0f = UNSTREAM_STRING_ASCII( &constant_bin[ 1877407 ], 47, 0 );
    const_str_digest_e35b4ca67805dd83ae05fba930bdb412 = UNSTREAM_STRING_ASCII( &constant_bin[ 1877454 ], 22, 0 );
    const_str_digest_09349449244a66b7d1fc2b9fa7edf9d0 = UNSTREAM_STRING_ASCII( &constant_bin[ 908 ], 4, 0 );
    const_str_digest_7351bd1520ee1e45bba392f411640a43 = UNSTREAM_STRING_ASCII( &constant_bin[ 1877476 ], 60, 0 );
    const_str_digest_1ed0834fb44639b6c86bc76dc2a7d4b6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1877536 ], 34, 0 );
    const_str_digest_fdbb208f62d71b18577f0b1117f34570 = UNSTREAM_STRING_ASCII( &constant_bin[ 1877570 ], 403, 0 );
    const_tuple_da87431ac29c1a04c8d6d0f3b3fc2bb6_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_da87431ac29c1a04c8d6d0f3b3fc2bb6_tuple, 0, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    PyTuple_SET_ITEM( const_tuple_da87431ac29c1a04c8d6d0f3b3fc2bb6_tuple, 1, const_str_plain_new_doc ); Py_INCREF( const_str_plain_new_doc );
    PyTuple_SET_ITEM( const_tuple_da87431ac29c1a04c8d6d0f3b3fc2bb6_tuple, 2, const_str_plain__deprecated_property ); Py_INCREF( const_str_plain__deprecated_property );
    PyTuple_SET_ITEM( const_tuple_da87431ac29c1a04c8d6d0f3b3fc2bb6_tuple, 3, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    const_str_digest_2d03aa0e4987d339d81bf09c043a8b47 = UNSTREAM_STRING_ASCII( &constant_bin[ 1877973 ], 51, 0 );
    const_dict_c3a87ba873c63ff91ba7b1a628042fcb = _PyDict_NewPresized( 7 );
    PyDict_SetItem( const_dict_c3a87ba873c63ff91ba7b1a628042fcb, const_str_plain_message, const_str_empty );
    PyDict_SetItem( const_dict_c3a87ba873c63ff91ba7b1a628042fcb, const_str_plain_name, const_str_empty );
    PyDict_SetItem( const_dict_c3a87ba873c63ff91ba7b1a628042fcb, const_str_plain_alternative, const_str_empty );
    PyDict_SetItem( const_dict_c3a87ba873c63ff91ba7b1a628042fcb, const_str_plain_pending, Py_False );
    PyDict_SetItem( const_dict_c3a87ba873c63ff91ba7b1a628042fcb, const_str_plain_obj_type, const_str_plain_attribute );
    PyDict_SetItem( const_dict_c3a87ba873c63ff91ba7b1a628042fcb, const_str_plain_addendum, const_str_empty );
    PyDict_SetItem( const_dict_c3a87ba873c63ff91ba7b1a628042fcb, const_str_plain_removal, const_str_empty );
    assert( PyDict_Size( const_dict_c3a87ba873c63ff91ba7b1a628042fcb ) == 7 );
    const_str_digest_4e6d4d30ab12f2dea40a4c4c87d51b05 = UNSTREAM_STRING_ASCII( &constant_bin[ 1878024 ], 31, 0 );
    const_str_digest_c324388e8872f2163c30f519b2d58f8a = UNSTREAM_STRING_ASCII( &constant_bin[ 1878055 ], 140, 0 );
    const_str_digest_893a7ac8b87a8aa2fd33f50d9212578f = UNSTREAM_STRING_ASCII( &constant_bin[ 1878195 ], 27, 0 );
    const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple, 0, const_str_plain_since ); Py_INCREF( const_str_plain_since );
    PyTuple_SET_ITEM( const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple, 1, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple, 3, const_str_plain_alternative ); Py_INCREF( const_str_plain_alternative );
    PyTuple_SET_ITEM( const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple, 4, const_str_plain_pending ); Py_INCREF( const_str_plain_pending );
    PyTuple_SET_ITEM( const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple, 5, const_str_plain_obj_type ); Py_INCREF( const_str_plain_obj_type );
    PyTuple_SET_ITEM( const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple, 6, const_str_plain_addendum ); Py_INCREF( const_str_plain_addendum );
    PyTuple_SET_ITEM( const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple, 7, const_str_plain_removal ); Py_INCREF( const_str_plain_removal );
    PyTuple_SET_ITEM( const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple, 8, const_str_plain_warning_cls ); Py_INCREF( const_str_plain_warning_cls );
    const_str_digest_f13229e14be656569ff96e8edb661822 = UNSTREAM_STRING_ASCII( &constant_bin[ 3187 ], 4, 0 );
    const_str_digest_b476cba358e3693377c097f885e27bc3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1873376 ], 59, 0 );
    const_dict_577b6678bf9964110fef48548911d3aa = _PyDict_NewPresized( 3 );
    const_str_digest_39eaa473e3cbe47ce40ea0eb73c1ad7e = UNSTREAM_STRING_ASCII( &constant_bin[ 1878222 ], 6, 0 );
    PyDict_SetItem( const_dict_577b6678bf9964110fef48548911d3aa, const_str_digest_2af24ee1ac2586d3cdf67e9808301ea2, const_str_digest_39eaa473e3cbe47ce40ea0eb73c1ad7e );
    PyDict_SetItem( const_dict_577b6678bf9964110fef48548911d3aa, const_str_digest_618a63e015481c6ac4c16aadc3be5237, const_str_digest_26acb95b859af724e6a8c94f3e8d9076 );
    PyDict_SetItem( const_dict_577b6678bf9964110fef48548911d3aa, const_str_digest_50e4933a9d0fc470d2deeb63d403662b, const_str_digest_e40b4bce85d44a72d598c38ec3c8872d );
    assert( PyDict_Size( const_dict_577b6678bf9964110fef48548911d3aa ) == 3 );
    const_str_digest_40b735750604d760d22884177c72c9b4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1878228 ], 67, 0 );
    const_str_digest_cc52526bc81fb768b83183d7748c8ba6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1259337 ], 12, 0 );
    const_str_digest_72fd6686a1bcf90a38f58fc375cd915c = UNSTREAM_STRING_ASCII( &constant_bin[ 1873508 ], 28, 0 );
    const_str_digest_7654d5c228106810e555b45def28becc = UNSTREAM_STRING_ASCII( &constant_bin[ 887329 ], 14, 0 );
    const_str_plain_KWO = UNSTREAM_STRING_ASCII( &constant_bin[ 1878295 ], 3, 1 );
    const_str_digest_bcd6e8071a1b0f259058c810a8077cf3 = UNSTREAM_STRING_ASCII( &constant_bin[ 887330 ], 13, 0 );
    const_str_digest_0e3811383b231b7c65c4082d4334c412 = UNSTREAM_STRING_ASCII( &constant_bin[ 1878298 ], 36, 0 );
    const_str_digest_f76bbb1cda8fc672dad73d81f6efb3b6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1878334 ], 646, 0 );
    const_str_digest_df5f2b7198e185abac8602ca838db37a = UNSTREAM_STRING_ASCII( &constant_bin[ 1878980 ], 5, 0 );
    const_tuple_str_plain_param_str_plain_kwonly_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_param_str_plain_kwonly_tuple, 0, const_str_plain_param ); Py_INCREF( const_str_plain_param );
    const_str_plain_kwonly = UNSTREAM_STRING_ASCII( &constant_bin[ 658439 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_param_str_plain_kwonly_tuple, 1, const_str_plain_kwonly ); Py_INCREF( const_str_plain_kwonly );
    const_str_digest_fc9cad0e6ace3451e8288fbfcf53ad88 = UNSTREAM_STRING_ASCII( &constant_bin[ 1468996 ], 32, 0 );
    const_tuple_b38c1c33ffba006c73a8ae2a1546149c_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b38c1c33ffba006c73a8ae2a1546149c_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_b38c1c33ffba006c73a8ae2a1546149c_tuple, 1, const_str_plain_instance ); Py_INCREF( const_str_plain_instance );
    PyTuple_SET_ITEM( const_tuple_b38c1c33ffba006c73a8ae2a1546149c_tuple, 2, const_str_plain__warn_external ); Py_INCREF( const_str_plain__warn_external );
    PyTuple_SET_ITEM( const_tuple_b38c1c33ffba006c73a8ae2a1546149c_tuple, 3, const_str_plain_warning ); Py_INCREF( const_str_plain_warning );
    PyTuple_SET_ITEM( const_tuple_b38c1c33ffba006c73a8ae2a1546149c_tuple, 4, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_tuple_be47f1afd36e8dbac94a95aead116e13_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_be47f1afd36e8dbac94a95aead116e13_tuple, 0, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_be47f1afd36e8dbac94a95aead116e13_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_be47f1afd36e8dbac94a95aead116e13_tuple, 2, const_str_plain_bound ); Py_INCREF( const_str_plain_bound );
    PyTuple_SET_ITEM( const_tuple_be47f1afd36e8dbac94a95aead116e13_tuple, 3, const_str_plain_signature ); Py_INCREF( const_str_plain_signature );
    PyTuple_SET_ITEM( const_tuple_be47f1afd36e8dbac94a95aead116e13_tuple, 4, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_be47f1afd36e8dbac94a95aead116e13_tuple, 5, const_str_plain_since ); Py_INCREF( const_str_plain_since );
    PyTuple_SET_ITEM( const_tuple_be47f1afd36e8dbac94a95aead116e13_tuple, 6, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    const_str_digest_d8486a1b232bbac034ebcb89ee222341 = UNSTREAM_STRING_ASCII( &constant_bin[ 1878985 ], 55, 0 );
    const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple, 0, const_str_plain_since ); Py_INCREF( const_str_plain_since );
    PyTuple_SET_ITEM( const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple, 1, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple, 3, const_str_plain_alternative ); Py_INCREF( const_str_plain_alternative );
    PyTuple_SET_ITEM( const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple, 4, const_str_plain_pending ); Py_INCREF( const_str_plain_pending );
    PyTuple_SET_ITEM( const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple, 5, const_str_plain_obj_type ); Py_INCREF( const_str_plain_obj_type );
    PyTuple_SET_ITEM( const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple, 6, const_str_plain_addendum ); Py_INCREF( const_str_plain_addendum );
    PyTuple_SET_ITEM( const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple, 7, const_str_plain_removal ); Py_INCREF( const_str_plain_removal );
    PyTuple_SET_ITEM( const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple, 8, const_str_plain_deprecate ); Py_INCREF( const_str_plain_deprecate );
    const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_func_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_func_tuple, 0, const_str_plain_wrapper ); Py_INCREF( const_str_plain_wrapper );
    PyTuple_SET_ITEM( const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_func_tuple, 1, const_str_plain_new_doc ); Py_INCREF( const_str_plain_new_doc );
    PyTuple_SET_ITEM( const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_func_tuple, 2, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    const_str_digest_b7663a8fc994f1116640c4cbdb8b5d0b = UNSTREAM_STRING_ASCII( &constant_bin[ 1879040 ], 1711, 0 );
    const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple, 0, const_str_plain_since ); Py_INCREF( const_str_plain_since );
    PyTuple_SET_ITEM( const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple, 2, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple, 3, const_str_plain_signature ); Py_INCREF( const_str_plain_signature );
    PyTuple_SET_ITEM( const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple, 4, const_str_plain_POK ); Py_INCREF( const_str_plain_POK );
    PyTuple_SET_ITEM( const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple, 5, const_str_plain_KWO ); Py_INCREF( const_str_plain_KWO );
    PyTuple_SET_ITEM( const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple, 6, const_str_plain_names ); Py_INCREF( const_str_plain_names );
    PyTuple_SET_ITEM( const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple, 7, const_str_plain_kwonly ); Py_INCREF( const_str_plain_kwonly );
    PyTuple_SET_ITEM( const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple, 8, const_str_plain_wrapper ); Py_INCREF( const_str_plain_wrapper );
    const_tuple_b914f6dc85e7e57ab7e4b12b20ba9de3_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b914f6dc85e7e57ab7e4b12b20ba9de3_tuple, 0, const_str_plain_since ); Py_INCREF( const_str_plain_since );
    PyTuple_SET_ITEM( const_tuple_b914f6dc85e7e57ab7e4b12b20ba9de3_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_b914f6dc85e7e57ab7e4b12b20ba9de3_tuple, 2, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_b914f6dc85e7e57ab7e4b12b20ba9de3_tuple, 3, const_str_plain_signature ); Py_INCREF( const_str_plain_signature );
    PyTuple_SET_ITEM( const_tuple_b914f6dc85e7e57ab7e4b12b20ba9de3_tuple, 4, const_str_plain_wrapper ); Py_INCREF( const_str_plain_wrapper );
    const_tuple_337cc82e4c9f5c22938c1b22943c0ec9_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_337cc82e4c9f5c22938c1b22943c0ec9_tuple, 0, const_str_plain_since ); Py_INCREF( const_str_plain_since );
    PyTuple_SET_ITEM( const_tuple_337cc82e4c9f5c22938c1b22943c0ec9_tuple, 1, const_str_plain_old ); Py_INCREF( const_str_plain_old );
    PyTuple_SET_ITEM( const_tuple_337cc82e4c9f5c22938c1b22943c0ec9_tuple, 2, const_str_plain_new ); Py_INCREF( const_str_plain_new );
    PyTuple_SET_ITEM( const_tuple_337cc82e4c9f5c22938c1b22943c0ec9_tuple, 3, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_337cc82e4c9f5c22938c1b22943c0ec9_tuple, 4, const_str_plain_signature ); Py_INCREF( const_str_plain_signature );
    PyTuple_SET_ITEM( const_tuple_337cc82e4c9f5c22938c1b22943c0ec9_tuple, 5, const_str_plain_wrapper ); Py_INCREF( const_str_plain_wrapper );
    const_dict_288ceb0c92afcb06d9d92fe674369750 = _PyDict_NewPresized( 7 );
    PyDict_SetItem( const_dict_288ceb0c92afcb06d9d92fe674369750, const_str_plain_message, const_str_empty );
    PyDict_SetItem( const_dict_288ceb0c92afcb06d9d92fe674369750, const_str_plain_name, const_str_empty );
    PyDict_SetItem( const_dict_288ceb0c92afcb06d9d92fe674369750, const_str_plain_alternative, const_str_empty );
    PyDict_SetItem( const_dict_288ceb0c92afcb06d9d92fe674369750, const_str_plain_pending, Py_False );
    PyDict_SetItem( const_dict_288ceb0c92afcb06d9d92fe674369750, const_str_plain_obj_type, Py_None );
    PyDict_SetItem( const_dict_288ceb0c92afcb06d9d92fe674369750, const_str_plain_addendum, const_str_empty );
    PyDict_SetItem( const_dict_288ceb0c92afcb06d9d92fe674369750, const_str_plain_removal, const_str_empty );
    assert( PyDict_Size( const_dict_288ceb0c92afcb06d9d92fe674369750 ) == 7 );
    const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_obj_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_obj_tuple, 0, const_str_plain_wrapper ); Py_INCREF( const_str_plain_wrapper );
    PyTuple_SET_ITEM( const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_obj_tuple, 1, const_str_plain_new_doc ); Py_INCREF( const_str_plain_new_doc );
    PyTuple_SET_ITEM( const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_obj_tuple, 2, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_matplotlib$cbook$deprecation( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_561302bb77fe5608dcb90b4fdd324ab2;
static PyCodeObject *codeobj_cd7b2ffbb718252ce76ae8bf45e44893;
static PyCodeObject *codeobj_d5e28f735b78dd2d76d51578bad34796;
static PyCodeObject *codeobj_fc791ac736d18b32f8b916e8551290fc;
static PyCodeObject *codeobj_6bfb613eacf0e6932019da285c419273;
static PyCodeObject *codeobj_08c2622f35a3431ed3ee5a285ea3d111;
static PyCodeObject *codeobj_9b0223749077f0347c73c340756f3f94;
static PyCodeObject *codeobj_112af24ecb9a463e995654111c3f152a;
static PyCodeObject *codeobj_1317ce8661d54881623c1f54531120a4;
static PyCodeObject *codeobj_970c410aa615c58e7f1b7a3220cbc778;
static PyCodeObject *codeobj_b1d4f7855cc9d5161385116400c1b545;
static PyCodeObject *codeobj_c8d2e9bec54f9a47225eb0f6eca366a7;
static PyCodeObject *codeobj_f69e57f244bdf4cd7fa01d4f158653f8;
static PyCodeObject *codeobj_27e5cf7205556ea8e8b237cc9fb5b318;
static PyCodeObject *codeobj_5178978a9b5ece2f6d3ebd0363c6209a;
static PyCodeObject *codeobj_7363cb49922ef5792c2d6b06382529ea;
static PyCodeObject *codeobj_6be97f9686a674b9178476de4ba08905;
static PyCodeObject *codeobj_f5dbb84adc05a8b8017b07af877767e0;
static PyCodeObject *codeobj_eb66ce6a82024475054afab867381783;
static PyCodeObject *codeobj_8fa74b6215bc1eaee564d2cb5e6e5d20;
static PyCodeObject *codeobj_e9d6ffae23703aacdce7f642eb17fba8;
static PyCodeObject *codeobj_f861e5fed7f650f3780bd3601addbbda;
static PyCodeObject *codeobj_d8a9c1581f4e5683e4559f567d64b192;
static PyCodeObject *codeobj_b58bb223bc3f0f04560651ca8575ec15;
static PyCodeObject *codeobj_5023d2a43be9bdf9a8b72aeeb1807ffe;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_4e6d4d30ab12f2dea40a4c4c87d51b05 );
    codeobj_561302bb77fe5608dcb90b4fdd324ab2 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 396, const_tuple_str_plain_name_str_plain_signature_str_plain_POK_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cd7b2ffbb718252ce76ae8bf45e44893 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 399, const_tuple_str_plain_param_str_plain_kwonly_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d5e28f735b78dd2d76d51578bad34796 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 354, const_tuple_str_plain_param_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fc791ac736d18b32f8b916e8551290fc = MAKE_CODEOBJ( module_filename_obj, const_str_digest_7c73872a1161663710c8a735d0adf9e1, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_6bfb613eacf0e6932019da285c419273 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___delete__, 220, const_tuple_b38c1c33ffba006c73a8ae2a1546149c_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_08c2622f35a3431ed3ee5a285ea3d111 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___get__, 208, const_tuple_4a447ae08f02a862f84b1e940737f3de_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_9b0223749077f0347c73c340756f3f94 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 318, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_112af24ecb9a463e995654111c3f152a = MAKE_CODEOBJ( module_filename_obj, const_str_plain___set__, 214, const_tuple_c23ae7cb5dd8709b04397346a1d90fa8_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_1317ce8661d54881623c1f54531120a4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__delete_parameter, 325, const_tuple_b914f6dc85e7e57ab7e4b12b20ba9de3_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_970c410aa615c58e7f1b7a3220cbc778 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__deprecated_parameter_class, 317, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_b1d4f7855cc9d5161385116400c1b545 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__deprecated_property, 207, const_tuple_str_plain___class___tuple, 0, 0, CO_OPTIMIZED | CO_NOFREE );
    codeobj_c8d2e9bec54f9a47225eb0f6eca366a7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__generate_deprecation_warning, 24, const_tuple_4da2877bb04d0a9ec32dcc716efb27e4_tuple, 7, 1, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f69e57f244bdf4cd7fa01d4f158653f8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__make_keyword_only, 374, const_tuple_5d8e03f8f2fe888c68244454229366a3_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_27e5cf7205556ea8e8b237cc9fb5b318 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__rename_parameter, 269, const_tuple_337cc82e4c9f5c22938c1b22943c0ec9_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5178978a9b5ece2f6d3ebd0363c6209a = MAKE_CODEOBJ( module_filename_obj, const_str_plain__suppress_matplotlib_deprecation_warning, 418, const_tuple_empty, 0, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7363cb49922ef5792c2d6b06382529ea = MAKE_CODEOBJ( module_filename_obj, const_str_plain_deprecate, 183, const_tuple_c27f8e26d51e5061e25d316d58e35aa3_tuple, 7, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_6be97f9686a674b9178476de4ba08905 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_deprecated, 117, const_tuple_523525036ccf8e04146e8b9f59583c5e_tuple, 1, 7, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f5dbb84adc05a8b8017b07af877767e0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_finalize, 226, const_tuple_da87431ac29c1a04c8d6d0f3b3fc2bb6_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_eb66ce6a82024475054afab867381783 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_finalize, 237, const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_func_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_8fa74b6215bc1eaee564d2cb5e6e5d20 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_finalize, 193, const_tuple_str_plain_wrapper_str_plain_new_doc_str_plain_obj_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_e9d6ffae23703aacdce7f642eb17fba8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_warn_deprecated, 58, const_tuple_714663466307e2c50201f8a2e9f85e0d_tuple, 1, 7, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f861e5fed7f650f3780bd3601addbbda = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapper, 246, const_tuple_b242710c6192a722fa0f4d5a18675914_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
    codeobj_d8a9c1581f4e5683e4559f567d64b192 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapper, 358, const_tuple_3d9adb719d7f26b7c1d877aafb3665cd_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
    codeobj_b58bb223bc3f0f04560651ca8575ec15 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapper, 404, const_tuple_be47f1afd36e8dbac94a95aead116e13_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
    codeobj_5023d2a43be9bdf9a8b72aeeb1807ffe = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapper, 299, const_tuple_4962842542ed13e68d3c735958ea22ec_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
}

// The module function declarations.
static PyObject *matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning$$$genobj_1__suppress_matplotlib_deprecation_warning_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_17__unpack_list( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_1__generate_deprecation_warning( PyObject *defaults, PyObject *kw_defaults );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_2_warn_deprecated( PyObject *kw_defaults );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated( PyObject *kw_defaults );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_1_finalize(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_2___get__(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_3___set__(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_4___delete__(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_5_finalize(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_6_finalize(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_7_wrapper(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_4__rename_parameter( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_4__rename_parameter$$$function_1_wrapper(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_5___repr__(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_6__delete_parameter( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_6__delete_parameter$$$function_1_wrapper(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_7__make_keyword_only( PyObject *defaults );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_7__make_keyword_only$$$function_1_wrapper(  );


static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning(  );


// The module function definitions.
static PyObject *impl_matplotlib$cbook$deprecation$$$function_1__generate_deprecation_warning( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_since = python_pars[ 0 ];
    PyObject *par_message = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    PyObject *par_alternative = python_pars[ 3 ];
    PyObject *par_pending = python_pars[ 4 ];
    PyObject *par_obj_type = python_pars[ 5 ];
    PyObject *par_addendum = python_pars[ 6 ];
    PyObject *par_removal = python_pars[ 7 ];
    PyObject *var_warning_cls = NULL;
    struct Nuitka_FrameObject *frame_c8d2e9bec54f9a47225eb0f6eca366a7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_c8d2e9bec54f9a47225eb0f6eca366a7 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c8d2e9bec54f9a47225eb0f6eca366a7, codeobj_c8d2e9bec54f9a47225eb0f6eca366a7, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c8d2e9bec54f9a47225eb0f6eca366a7 = cache_frame_c8d2e9bec54f9a47225eb0f6eca366a7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c8d2e9bec54f9a47225eb0f6eca366a7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c8d2e9bec54f9a47225eb0f6eca366a7 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_removal );
        tmp_compexpr_left_1 = par_removal;
        tmp_compexpr_right_1 = const_str_empty;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_called_instance_1 = PyDict_Copy( const_dict_577b6678bf9964110fef48548911d3aa );
            CHECK_OBJECT( par_since );
            tmp_args_element_name_1 = par_since;
            tmp_args_element_name_2 = const_str_digest_9cb45d425ee90670d1d23be4f0d00a1d;
            frame_c8d2e9bec54f9a47225eb0f6eca366a7->m_frame.f_lineno = 29;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
            }

            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 29;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_removal;
                assert( old != NULL );
                par_removal = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_truth_name_1;
            CHECK_OBJECT( par_removal );
            tmp_truth_name_1 = CHECK_IF_TRUE( par_removal );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 31;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                nuitka_bool tmp_condition_result_3;
                int tmp_truth_name_2;
                CHECK_OBJECT( par_pending );
                tmp_truth_name_2 = CHECK_IF_TRUE( par_pending );
                if ( tmp_truth_name_2 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 32;
                    type_description_1 = "ooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_raise_type_1;
                    PyObject *tmp_make_exception_arg_1;
                    tmp_make_exception_arg_1 = const_str_digest_6fa65115bdc4603b700ccce19105bbfc;
                    frame_c8d2e9bec54f9a47225eb0f6eca366a7->m_frame.f_lineno = 33;
                    {
                        PyObject *call_args[] = { tmp_make_exception_arg_1 };
                        tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                    }

                    assert( !(tmp_raise_type_1 == NULL) );
                    exception_type = tmp_raise_type_1;
                    exception_lineno = 33;
                    RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooooooooo";
                    goto frame_exception_exit_1;
                }
                branch_no_3:;
            }
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_args_element_name_3;
                tmp_called_instance_2 = const_str_digest_df5f2b7198e185abac8602ca838db37a;
                CHECK_OBJECT( par_removal );
                tmp_args_element_name_3 = par_removal;
                frame_c8d2e9bec54f9a47225eb0f6eca366a7->m_frame.f_lineno = 35;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3 };
                    tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_format, call_args );
                }

                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 35;
                    type_description_1 = "ooooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = par_removal;
                    assert( old != NULL );
                    par_removal = tmp_assign_source_2;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        branch_end_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( par_message );
        tmp_operand_name_1 = par_message;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_left_name_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_left_name_3;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_1;
            nuitka_bool tmp_condition_result_5;
            int tmp_truth_name_3;
            PyObject *tmp_left_name_5;
            PyObject *tmp_right_name_2;
            nuitka_bool tmp_condition_result_6;
            int tmp_truth_name_4;
            PyObject *tmp_right_name_3;
            PyObject *tmp_right_name_4;
            nuitka_bool tmp_condition_result_7;
            int tmp_truth_name_5;
            PyObject *tmp_right_name_5;
            nuitka_bool tmp_condition_result_8;
            int tmp_truth_name_6;
            tmp_left_name_4 = const_str_digest_5b4164fe172a091435bdedc3dba93f0d;
            CHECK_OBJECT( par_pending );
            tmp_truth_name_3 = CHECK_IF_TRUE( par_pending );
            if ( tmp_truth_name_3 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_1;
            }
            else
            {
                goto condexpr_false_1;
            }
            condexpr_true_1:;
            tmp_right_name_1 = const_str_digest_8af1b95219abe614597fa695e4d0622d;
            Py_INCREF( tmp_right_name_1 );
            goto condexpr_end_1;
            condexpr_false_1:;
            tmp_left_name_5 = const_str_digest_20680fddcd5600f02b803ac07ad26db3;
            if ( par_removal == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "removal" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 44;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_truth_name_4 = CHECK_IF_TRUE( par_removal );
            if ( tmp_truth_name_4 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 44;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_6 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_2;
            }
            else
            {
                goto condexpr_false_2;
            }
            condexpr_true_2:;
            tmp_right_name_2 = const_str_digest_fc9cad0e6ace3451e8288fbfcf53ad88;
            goto condexpr_end_2;
            condexpr_false_2:;
            tmp_right_name_2 = const_str_empty;
            condexpr_end_2:;
            tmp_right_name_1 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_5, tmp_right_name_2 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            condexpr_end_1:;
            tmp_left_name_3 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_4, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_left_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_3 = const_str_dot;
            tmp_left_name_2 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_3, tmp_right_name_3 );
            Py_DECREF( tmp_left_name_3 );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_alternative );
            tmp_truth_name_5 = CHECK_IF_TRUE( par_alternative );
            if ( tmp_truth_name_5 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );

                exception_lineno = 47;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_7 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_right_name_4 = const_str_digest_d78eb6a8aaa1023c3a418ab943b242ac;
            goto condexpr_end_3;
            condexpr_false_3:;
            tmp_right_name_4 = const_str_empty;
            condexpr_end_3:;
            tmp_left_name_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_4 );
            Py_DECREF( tmp_left_name_2 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 47;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_addendum );
            tmp_truth_name_6 = CHECK_IF_TRUE( par_addendum );
            if ( tmp_truth_name_6 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_1 );

                exception_lineno = 48;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_8 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_4;
            }
            else
            {
                goto condexpr_false_4;
            }
            condexpr_true_4:;
            tmp_right_name_5 = const_str_digest_c99941a615f8d934a0079401e1843f4b;
            goto condexpr_end_4;
            condexpr_false_4:;
            tmp_right_name_5 = const_str_empty;
            condexpr_end_4:;
            tmp_assign_source_3 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_5 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 48;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_message;
                assert( old != NULL );
                par_message = tmp_assign_source_3;
                Py_DECREF( old );
            }

        }
        branch_no_4:;
    }
    {
        PyObject *tmp_assign_source_4;
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_7;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_pending );
        tmp_truth_name_7 = CHECK_IF_TRUE( par_pending );
        if ( tmp_truth_name_7 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_9 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_PendingDeprecationWarning );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PendingDeprecationWarning );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PendingDeprecationWarning" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 50;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_assign_source_4 = tmp_mvar_value_1;
        goto condexpr_end_5;
        condexpr_false_5:;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_MatplotlibDeprecationWarning );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MatplotlibDeprecationWarning );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MatplotlibDeprecationWarning" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 51;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_assign_source_4 = tmp_mvar_value_2;
        condexpr_end_5:;
        assert( var_warning_cls == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_warning_cls = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_left_name_6;
        PyObject *tmp_right_name_6;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        CHECK_OBJECT( var_warning_cls );
        tmp_called_name_1 = var_warning_cls;
        CHECK_OBJECT( par_message );
        tmp_left_name_6 = par_message;
        tmp_dict_key_1 = const_str_plain_func;
        CHECK_OBJECT( par_name );
        tmp_dict_value_1 = par_name;
        tmp_right_name_6 = _PyDict_NewPresized( 7 );
        tmp_res = PyDict_SetItem( tmp_right_name_6, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_name;
        CHECK_OBJECT( par_name );
        tmp_dict_value_2 = par_name;
        tmp_res = PyDict_SetItem( tmp_right_name_6, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_obj_type;
        CHECK_OBJECT( par_obj_type );
        tmp_dict_value_3 = par_obj_type;
        tmp_res = PyDict_SetItem( tmp_right_name_6, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_since;
        CHECK_OBJECT( par_since );
        tmp_dict_value_4 = par_since;
        tmp_res = PyDict_SetItem( tmp_right_name_6, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_removal;
        if ( par_removal == NULL )
        {
            Py_DECREF( tmp_right_name_6 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "removal" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 54;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_5 = par_removal;
        tmp_res = PyDict_SetItem( tmp_right_name_6, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_alternative;
        CHECK_OBJECT( par_alternative );
        tmp_dict_value_6 = par_alternative;
        tmp_res = PyDict_SetItem( tmp_right_name_6, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_addendum;
        CHECK_OBJECT( par_addendum );
        tmp_dict_value_7 = par_addendum;
        tmp_res = PyDict_SetItem( tmp_right_name_6, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_args_element_name_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
        Py_DECREF( tmp_right_name_6 );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        frame_c8d2e9bec54f9a47225eb0f6eca366a7->m_frame.f_lineno = 53;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8d2e9bec54f9a47225eb0f6eca366a7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8d2e9bec54f9a47225eb0f6eca366a7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8d2e9bec54f9a47225eb0f6eca366a7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c8d2e9bec54f9a47225eb0f6eca366a7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c8d2e9bec54f9a47225eb0f6eca366a7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c8d2e9bec54f9a47225eb0f6eca366a7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c8d2e9bec54f9a47225eb0f6eca366a7,
        type_description_1,
        par_since,
        par_message,
        par_name,
        par_alternative,
        par_pending,
        par_obj_type,
        par_addendum,
        par_removal,
        var_warning_cls
    );


    // Release cached frame.
    if ( frame_c8d2e9bec54f9a47225eb0f6eca366a7 == cache_frame_c8d2e9bec54f9a47225eb0f6eca366a7 )
    {
        Py_DECREF( frame_c8d2e9bec54f9a47225eb0f6eca366a7 );
    }
    cache_frame_c8d2e9bec54f9a47225eb0f6eca366a7 = NULL;

    assertFrameObject( frame_c8d2e9bec54f9a47225eb0f6eca366a7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_1__generate_deprecation_warning );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_alternative );
    Py_DECREF( par_alternative );
    par_alternative = NULL;

    CHECK_OBJECT( (PyObject *)par_pending );
    Py_DECREF( par_pending );
    par_pending = NULL;

    CHECK_OBJECT( (PyObject *)par_obj_type );
    Py_DECREF( par_obj_type );
    par_obj_type = NULL;

    CHECK_OBJECT( (PyObject *)par_addendum );
    Py_DECREF( par_addendum );
    par_addendum = NULL;

    Py_XDECREF( par_removal );
    par_removal = NULL;

    CHECK_OBJECT( (PyObject *)var_warning_cls );
    Py_DECREF( var_warning_cls );
    var_warning_cls = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    Py_XDECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_alternative );
    Py_DECREF( par_alternative );
    par_alternative = NULL;

    CHECK_OBJECT( (PyObject *)par_pending );
    Py_DECREF( par_pending );
    par_pending = NULL;

    CHECK_OBJECT( (PyObject *)par_obj_type );
    Py_DECREF( par_obj_type );
    par_obj_type = NULL;

    CHECK_OBJECT( (PyObject *)par_addendum );
    Py_DECREF( par_addendum );
    par_addendum = NULL;

    Py_XDECREF( par_removal );
    par_removal = NULL;

    Py_XDECREF( var_warning_cls );
    var_warning_cls = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_1__generate_deprecation_warning );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_2_warn_deprecated( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_since = python_pars[ 0 ];
    PyObject *par_message = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    PyObject *par_alternative = python_pars[ 3 ];
    PyObject *par_pending = python_pars[ 4 ];
    PyObject *par_obj_type = python_pars[ 5 ];
    PyObject *par_addendum = python_pars[ 6 ];
    PyObject *par_removal = python_pars[ 7 ];
    PyObject *var_warning = NULL;
    PyObject *var__warn_external = NULL;
    struct Nuitka_FrameObject *frame_e9d6ffae23703aacdce7f642eb17fba8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_e9d6ffae23703aacdce7f642eb17fba8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e9d6ffae23703aacdce7f642eb17fba8, codeobj_e9d6ffae23703aacdce7f642eb17fba8, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e9d6ffae23703aacdce7f642eb17fba8 = cache_frame_e9d6ffae23703aacdce7f642eb17fba8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e9d6ffae23703aacdce7f642eb17fba8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e9d6ffae23703aacdce7f642eb17fba8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__generate_deprecation_warning );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__generate_deprecation_warning );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_generate_deprecation_warning" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 110;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_since );
        tmp_tuple_element_1 = par_since;
        tmp_args_name_1 = PyTuple_New( 7 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_message );
        tmp_tuple_element_1 = par_message;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_name );
        tmp_tuple_element_1 = par_name;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_alternative );
        tmp_tuple_element_1 = par_alternative;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 3, tmp_tuple_element_1 );
        CHECK_OBJECT( par_pending );
        tmp_tuple_element_1 = par_pending;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 4, tmp_tuple_element_1 );
        CHECK_OBJECT( par_obj_type );
        tmp_tuple_element_1 = par_obj_type;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 5, tmp_tuple_element_1 );
        CHECK_OBJECT( par_addendum );
        tmp_tuple_element_1 = par_addendum;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 6, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_removal;
        CHECK_OBJECT( par_removal );
        tmp_dict_value_1 = par_removal;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_e9d6ffae23703aacdce7f642eb17fba8->m_frame.f_lineno = 110;
        tmp_assign_source_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_warning == NULL );
        var_warning = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_empty;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$cbook$deprecation;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain__warn_external_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_e9d6ffae23703aacdce7f642eb17fba8->m_frame.f_lineno = 113;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_2 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_matplotlib$cbook$deprecation,
                const_str_plain__warn_external,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain__warn_external );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var__warn_external == NULL );
        var__warn_external = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var__warn_external );
        tmp_called_name_2 = var__warn_external;
        CHECK_OBJECT( var_warning );
        tmp_args_element_name_1 = var_warning;
        frame_e9d6ffae23703aacdce7f642eb17fba8->m_frame.f_lineno = 114;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e9d6ffae23703aacdce7f642eb17fba8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e9d6ffae23703aacdce7f642eb17fba8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e9d6ffae23703aacdce7f642eb17fba8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e9d6ffae23703aacdce7f642eb17fba8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e9d6ffae23703aacdce7f642eb17fba8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e9d6ffae23703aacdce7f642eb17fba8,
        type_description_1,
        par_since,
        par_message,
        par_name,
        par_alternative,
        par_pending,
        par_obj_type,
        par_addendum,
        par_removal,
        var_warning,
        var__warn_external
    );


    // Release cached frame.
    if ( frame_e9d6ffae23703aacdce7f642eb17fba8 == cache_frame_e9d6ffae23703aacdce7f642eb17fba8 )
    {
        Py_DECREF( frame_e9d6ffae23703aacdce7f642eb17fba8 );
    }
    cache_frame_e9d6ffae23703aacdce7f642eb17fba8 = NULL;

    assertFrameObject( frame_e9d6ffae23703aacdce7f642eb17fba8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_2_warn_deprecated );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_alternative );
    Py_DECREF( par_alternative );
    par_alternative = NULL;

    CHECK_OBJECT( (PyObject *)par_pending );
    Py_DECREF( par_pending );
    par_pending = NULL;

    CHECK_OBJECT( (PyObject *)par_obj_type );
    Py_DECREF( par_obj_type );
    par_obj_type = NULL;

    CHECK_OBJECT( (PyObject *)par_addendum );
    Py_DECREF( par_addendum );
    par_addendum = NULL;

    CHECK_OBJECT( (PyObject *)par_removal );
    Py_DECREF( par_removal );
    par_removal = NULL;

    CHECK_OBJECT( (PyObject *)var_warning );
    Py_DECREF( var_warning );
    var_warning = NULL;

    CHECK_OBJECT( (PyObject *)var__warn_external );
    Py_DECREF( var__warn_external );
    var__warn_external = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_alternative );
    Py_DECREF( par_alternative );
    par_alternative = NULL;

    CHECK_OBJECT( (PyObject *)par_pending );
    Py_DECREF( par_pending );
    par_pending = NULL;

    CHECK_OBJECT( (PyObject *)par_obj_type );
    Py_DECREF( par_obj_type );
    par_obj_type = NULL;

    CHECK_OBJECT( (PyObject *)par_addendum );
    Py_DECREF( par_addendum );
    par_addendum = NULL;

    CHECK_OBJECT( (PyObject *)par_removal );
    Py_DECREF( par_removal );
    par_removal = NULL;

    Py_XDECREF( var_warning );
    var_warning = NULL;

    Py_XDECREF( var__warn_external );
    var__warn_external = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_2_warn_deprecated );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_3_deprecated( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_since = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *par_message = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    PyObject *par_alternative = python_pars[ 3 ];
    PyObject *par_pending = python_pars[ 4 ];
    PyObject *par_obj_type = python_pars[ 5 ];
    PyObject *par_addendum = python_pars[ 6 ];
    struct Nuitka_CellObject *par_removal = PyCell_NEW1( python_pars[ 7 ] );
    PyObject *var_deprecate = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_defaults_1;
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( par_message );
        tmp_tuple_element_1 = par_message;
        tmp_defaults_1 = PyTuple_New( 6 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_name );
        tmp_tuple_element_1 = par_name;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_alternative );
        tmp_tuple_element_1 = par_alternative;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_pending );
        tmp_tuple_element_1 = par_pending;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 3, tmp_tuple_element_1 );
        CHECK_OBJECT( par_obj_type );
        tmp_tuple_element_1 = par_obj_type;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 4, tmp_tuple_element_1 );
        CHECK_OBJECT( par_addendum );
        tmp_tuple_element_1 = par_addendum;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 5, tmp_tuple_element_1 );
        tmp_assign_source_1 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate( tmp_defaults_1 );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_removal;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] = par_since;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[1] );


        assert( var_deprecate == NULL );
        var_deprecate = tmp_assign_source_1;
    }
    // Tried code:
    CHECK_OBJECT( var_deprecate );
    tmp_return_value = var_deprecate;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_alternative );
    Py_DECREF( par_alternative );
    par_alternative = NULL;

    CHECK_OBJECT( (PyObject *)par_pending );
    Py_DECREF( par_pending );
    par_pending = NULL;

    CHECK_OBJECT( (PyObject *)par_obj_type );
    Py_DECREF( par_obj_type );
    par_obj_type = NULL;

    CHECK_OBJECT( (PyObject *)par_addendum );
    Py_DECREF( par_addendum );
    par_addendum = NULL;

    CHECK_OBJECT( (PyObject *)par_removal );
    Py_DECREF( par_removal );
    par_removal = NULL;

    CHECK_OBJECT( (PyObject *)var_deprecate );
    Py_DECREF( var_deprecate );
    var_deprecate = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_obj = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *par_message = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    PyObject *par_alternative = python_pars[ 3 ];
    PyObject *par_pending = python_pars[ 4 ];
    PyObject *par_obj_type = python_pars[ 5 ];
    PyObject *par_addendum = python_pars[ 6 ];
    struct Nuitka_CellObject *var_func = PyCell_EMPTY();
    PyObject *var_old_doc = NULL;
    PyObject *var_finalize = NULL;
    struct Nuitka_CellObject *var__deprecated_property = PyCell_EMPTY();
    struct Nuitka_CellObject *var_warning = PyCell_EMPTY();
    PyObject *var_wrapper = NULL;
    PyObject *var_new_doc = NULL;
    struct Nuitka_CellObject *outline_0_var___class__ = PyCell_EMPTY();
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    struct Nuitka_FrameObject *frame_7363cb49922ef5792c2d6b06382529ea;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    bool tmp_result;
    PyObject *locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_b1d4f7855cc9d5161385116400c1b545_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_b1d4f7855cc9d5161385116400c1b545_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_7363cb49922ef5792c2d6b06382529ea = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7363cb49922ef5792c2d6b06382529ea, codeobj_7363cb49922ef5792c2d6b06382529ea, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7363cb49922ef5792c2d6b06382529ea = cache_frame_7363cb49922ef5792c2d6b06382529ea;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7363cb49922ef5792c2d6b06382529ea );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7363cb49922ef5792c2d6b06382529ea ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( PyCell_GET( par_obj ) );
        tmp_isinstance_inst_1 = PyCell_GET( par_obj );
        tmp_isinstance_cls_1 = (PyObject *)&PyType_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_obj_type );
            tmp_compexpr_left_1 = par_obj_type;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_1;
                tmp_assign_source_1 = const_str_plain_class;
                {
                    PyObject *old = par_obj_type;
                    assert( old != NULL );
                    par_obj_type = tmp_assign_source_1;
                    Py_INCREF( par_obj_type );
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( PyCell_GET( par_obj ) );
            tmp_source_name_1 = PyCell_GET( par_obj );
            tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___init__ );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "coooooocooccoNocc";
                goto frame_exception_exit_1;
            }
            assert( PyCell_GET( var_func ) == NULL );
            PyCell_SET( var_func, tmp_assign_source_2 );

        }
        {
            PyObject *tmp_assign_source_3;
            int tmp_or_left_truth_1;
            PyObject *tmp_or_left_value_1;
            PyObject *tmp_or_right_value_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_name );
            tmp_or_left_value_1 = par_name;
            tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
            if ( tmp_or_left_truth_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;
                type_description_1 = "coooooocooccoNocc";
                goto frame_exception_exit_1;
            }
            if ( tmp_or_left_truth_1 == 1 )
            {
                goto or_left_1;
            }
            else
            {
                goto or_right_1;
            }
            or_right_1:;
            CHECK_OBJECT( PyCell_GET( par_obj ) );
            tmp_source_name_2 = PyCell_GET( par_obj );
            tmp_or_right_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___name__ );
            if ( tmp_or_right_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;
                type_description_1 = "coooooocooccoNocc";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_3 = tmp_or_right_value_1;
            goto or_end_1;
            or_left_1:;
            Py_INCREF( tmp_or_left_value_1 );
            tmp_assign_source_3 = tmp_or_left_value_1;
            or_end_1:;
            {
                PyObject *old = par_name;
                assert( old != NULL );
                par_name = tmp_assign_source_3;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( PyCell_GET( par_obj ) );
            tmp_source_name_3 = PyCell_GET( par_obj );
            tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___doc__ );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 191;
                type_description_1 = "coooooocooccoNocc";
                goto frame_exception_exit_1;
            }
            assert( var_old_doc == NULL );
            var_old_doc = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_1_finalize(  );

            ((struct Nuitka_FunctionObject *)tmp_assign_source_5)->m_closure[0] = par_obj;
            Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_5)->m_closure[0] );


            assert( var_finalize == NULL );
            var_finalize = tmp_assign_source_5;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            CHECK_OBJECT( PyCell_GET( par_obj ) );
            tmp_isinstance_inst_2 = PyCell_GET( par_obj );
            tmp_isinstance_cls_2 = (PyObject *)&PyProperty_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 201;
                type_description_1 = "coooooocooccoNocc";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_6;
                tmp_assign_source_6 = const_str_plain_attribute;
                {
                    PyObject *old = par_obj_type;
                    assert( old != NULL );
                    par_obj_type = tmp_assign_source_6;
                    Py_INCREF( par_obj_type );
                    Py_DECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_7;
                tmp_assign_source_7 = Py_None;
                assert( PyCell_GET( var_func ) == NULL );
                Py_INCREF( tmp_assign_source_7 );
                PyCell_SET( var_func, tmp_assign_source_7 );

            }
            {
                PyObject *tmp_assign_source_8;
                int tmp_or_left_truth_2;
                PyObject *tmp_or_left_value_2;
                PyObject *tmp_or_right_value_2;
                PyObject *tmp_source_name_4;
                PyObject *tmp_source_name_5;
                CHECK_OBJECT( par_name );
                tmp_or_left_value_2 = par_name;
                tmp_or_left_truth_2 = CHECK_IF_TRUE( tmp_or_left_value_2 );
                if ( tmp_or_left_truth_2 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 204;
                    type_description_1 = "coooooocooccoNocc";
                    goto frame_exception_exit_1;
                }
                if ( tmp_or_left_truth_2 == 1 )
                {
                    goto or_left_2;
                }
                else
                {
                    goto or_right_2;
                }
                or_right_2:;
                CHECK_OBJECT( PyCell_GET( par_obj ) );
                tmp_source_name_5 = PyCell_GET( par_obj );
                tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_fget );
                if ( tmp_source_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 204;
                    type_description_1 = "coooooocooccoNocc";
                    goto frame_exception_exit_1;
                }
                tmp_or_right_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_or_right_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 204;
                    type_description_1 = "coooooocooccoNocc";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_8 = tmp_or_right_value_2;
                goto or_end_2;
                or_left_2:;
                Py_INCREF( tmp_or_left_value_2 );
                tmp_assign_source_8 = tmp_or_left_value_2;
                or_end_2:;
                {
                    PyObject *old = par_name;
                    assert( old != NULL );
                    par_name = tmp_assign_source_8;
                    Py_DECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_source_name_6;
                CHECK_OBJECT( PyCell_GET( par_obj ) );
                tmp_source_name_6 = PyCell_GET( par_obj );
                tmp_assign_source_9 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___doc__ );
                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 205;
                    type_description_1 = "coooooocooccoNocc";
                    goto frame_exception_exit_1;
                }
                assert( var_old_doc == NULL );
                var_old_doc = tmp_assign_source_9;
            }
            // Tried code:
            {
                PyObject *tmp_assign_source_10;
                PyObject *tmp_dircall_arg1_1;
                tmp_dircall_arg1_1 = const_tuple_type_property_tuple;
                Py_INCREF( tmp_dircall_arg1_1 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
                    tmp_assign_source_10 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
                }
                if ( tmp_assign_source_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_2;
                }
                assert( tmp_class_creation_1__bases == NULL );
                tmp_class_creation_1__bases = tmp_assign_source_10;
            }
            {
                PyObject *tmp_assign_source_11;
                tmp_assign_source_11 = PyDict_New();
                assert( tmp_class_creation_1__class_decl_dict == NULL );
                tmp_class_creation_1__class_decl_dict = tmp_assign_source_11;
            }
            {
                PyObject *tmp_assign_source_12;
                PyObject *tmp_metaclass_name_1;
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_key_name_1;
                PyObject *tmp_dict_name_1;
                PyObject *tmp_dict_name_2;
                PyObject *tmp_key_name_2;
                nuitka_bool tmp_condition_result_5;
                int tmp_truth_name_1;
                PyObject *tmp_type_arg_1;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                PyObject *tmp_bases_name_1;
                tmp_key_name_1 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_2;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_1;
                }
                else
                {
                    goto condexpr_false_1;
                }
                condexpr_true_1:;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
                tmp_key_name_2 = const_str_plain_metaclass;
                tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
                if ( tmp_metaclass_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_2;
                }
                goto condexpr_end_1;
                condexpr_false_1:;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
                if ( tmp_truth_name_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_2;
                }
                tmp_condition_result_5 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_2;
                }
                else
                {
                    goto condexpr_false_2;
                }
                condexpr_true_2:;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_subscribed_name_1 = tmp_class_creation_1__bases;
                tmp_subscript_name_1 = const_int_0;
                tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
                if ( tmp_type_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_2;
                }
                tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
                Py_DECREF( tmp_type_arg_1 );
                if ( tmp_metaclass_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_2;
                }
                goto condexpr_end_2;
                condexpr_false_2:;
                tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
                Py_INCREF( tmp_metaclass_name_1 );
                condexpr_end_2:;
                condexpr_end_1:;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_bases_name_1 = tmp_class_creation_1__bases;
                tmp_assign_source_12 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
                Py_DECREF( tmp_metaclass_name_1 );
                if ( tmp_assign_source_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_2;
                }
                assert( tmp_class_creation_1__metaclass == NULL );
                tmp_class_creation_1__metaclass = tmp_assign_source_12;
            }
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_key_name_3;
                PyObject *tmp_dict_name_3;
                tmp_key_name_3 = const_str_plain_metaclass;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
                tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_2;
                }
                tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
                tmp_dictdel_key = const_str_plain_metaclass;
                tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_2;
                }
                branch_no_4:;
            }
            {
                nuitka_bool tmp_condition_result_7;
                PyObject *tmp_source_name_7;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_source_name_7 = tmp_class_creation_1__metaclass;
                tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___prepare__ );
                tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_assign_source_13;
                    PyObject *tmp_called_name_1;
                    PyObject *tmp_source_name_8;
                    PyObject *tmp_args_name_1;
                    PyObject *tmp_tuple_element_1;
                    PyObject *tmp_kw_name_1;
                    CHECK_OBJECT( tmp_class_creation_1__metaclass );
                    tmp_source_name_8 = tmp_class_creation_1__metaclass;
                    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___prepare__ );
                    if ( tmp_called_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 207;
                        type_description_1 = "coooooocooccoNocc";
                        goto try_except_handler_2;
                    }
                    tmp_tuple_element_1 = const_str_plain__deprecated_property;
                    tmp_args_name_1 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_1 );
                    PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
                    CHECK_OBJECT( tmp_class_creation_1__bases );
                    tmp_tuple_element_1 = tmp_class_creation_1__bases;
                    Py_INCREF( tmp_tuple_element_1 );
                    PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
                    CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                    tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
                    frame_7363cb49922ef5792c2d6b06382529ea->m_frame.f_lineno = 207;
                    tmp_assign_source_13 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
                    Py_DECREF( tmp_called_name_1 );
                    Py_DECREF( tmp_args_name_1 );
                    if ( tmp_assign_source_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 207;
                        type_description_1 = "coooooocooccoNocc";
                        goto try_except_handler_2;
                    }
                    assert( tmp_class_creation_1__prepared == NULL );
                    tmp_class_creation_1__prepared = tmp_assign_source_13;
                }
                {
                    nuitka_bool tmp_condition_result_8;
                    PyObject *tmp_operand_name_1;
                    PyObject *tmp_source_name_9;
                    CHECK_OBJECT( tmp_class_creation_1__prepared );
                    tmp_source_name_9 = tmp_class_creation_1__prepared;
                    tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___getitem__ );
                    tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 207;
                        type_description_1 = "coooooocooccoNocc";
                        goto try_except_handler_2;
                    }
                    tmp_condition_result_8 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_6;
                    }
                    else
                    {
                        goto branch_no_6;
                    }
                    branch_yes_6:;
                    {
                        PyObject *tmp_raise_type_1;
                        PyObject *tmp_raise_value_1;
                        PyObject *tmp_left_name_1;
                        PyObject *tmp_right_name_1;
                        PyObject *tmp_tuple_element_2;
                        PyObject *tmp_getattr_target_1;
                        PyObject *tmp_getattr_attr_1;
                        PyObject *tmp_getattr_default_1;
                        PyObject *tmp_source_name_10;
                        PyObject *tmp_type_arg_2;
                        tmp_raise_type_1 = PyExc_TypeError;
                        tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                        CHECK_OBJECT( tmp_class_creation_1__metaclass );
                        tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                        tmp_getattr_attr_1 = const_str_plain___name__;
                        tmp_getattr_default_1 = const_str_angle_metaclass;
                        tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                        if ( tmp_tuple_element_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 207;
                            type_description_1 = "coooooocooccoNocc";
                            goto try_except_handler_2;
                        }
                        tmp_right_name_1 = PyTuple_New( 2 );
                        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                        CHECK_OBJECT( tmp_class_creation_1__prepared );
                        tmp_type_arg_2 = tmp_class_creation_1__prepared;
                        tmp_source_name_10 = BUILTIN_TYPE1( tmp_type_arg_2 );
                        assert( !(tmp_source_name_10 == NULL) );
                        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___name__ );
                        Py_DECREF( tmp_source_name_10 );
                        if ( tmp_tuple_element_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_right_name_1 );

                            exception_lineno = 207;
                            type_description_1 = "coooooocooccoNocc";
                            goto try_except_handler_2;
                        }
                        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                        tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                        Py_DECREF( tmp_right_name_1 );
                        if ( tmp_raise_value_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 207;
                            type_description_1 = "coooooocooccoNocc";
                            goto try_except_handler_2;
                        }
                        exception_type = tmp_raise_type_1;
                        Py_INCREF( tmp_raise_type_1 );
                        exception_value = tmp_raise_value_1;
                        exception_lineno = 207;
                        RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "coooooocooccoNocc";
                        goto try_except_handler_2;
                    }
                    branch_no_6:;
                }
                goto branch_end_5;
                branch_no_5:;
                {
                    PyObject *tmp_assign_source_14;
                    tmp_assign_source_14 = PyDict_New();
                    assert( tmp_class_creation_1__prepared == NULL );
                    tmp_class_creation_1__prepared = tmp_assign_source_14;
                }
                branch_end_5:;
            }
            {
                PyObject *tmp_assign_source_15;
                {
                    PyObject *tmp_set_locals_1;
                    CHECK_OBJECT( tmp_class_creation_1__prepared );
                    tmp_set_locals_1 = tmp_class_creation_1__prepared;
                    locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207 = tmp_set_locals_1;
                    Py_INCREF( tmp_set_locals_1 );
                }
                // Tried code:
                // Tried code:
                tmp_dictset_value = const_str_digest_72fd6686a1bcf90a38f58fc375cd915c;
                tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207, const_str_plain___module__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_4;
                }
                tmp_dictset_value = const_str_digest_b476cba358e3693377c097f885e27bc3;
                tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207, const_str_plain___qualname__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 207;
                    type_description_1 = "coooooocooccoNocc";
                    goto try_except_handler_4;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_b1d4f7855cc9d5161385116400c1b545_2, codeobj_b1d4f7855cc9d5161385116400c1b545, module_matplotlib$cbook$deprecation, sizeof(void *) );
                frame_b1d4f7855cc9d5161385116400c1b545_2 = cache_frame_b1d4f7855cc9d5161385116400c1b545_2;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_b1d4f7855cc9d5161385116400c1b545_2 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_b1d4f7855cc9d5161385116400c1b545_2 ) == 2 ); // Frame stack

                // Framed code:
                tmp_dictset_value = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_2___get__(  );

                ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = outline_0_var___class__;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );
                ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] = var_warning;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] );


                tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207, const_str_plain___get__, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 208;
                    type_description_2 = "c";
                    goto frame_exception_exit_2;
                }
                tmp_dictset_value = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_3___set__(  );

                ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = outline_0_var___class__;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );
                ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] = var_warning;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] );


                tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207, const_str_plain___set__, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 214;
                    type_description_2 = "c";
                    goto frame_exception_exit_2;
                }
                tmp_dictset_value = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_4___delete__(  );

                ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] = outline_0_var___class__;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[0] );
                ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] = var_warning;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_dictset_value)->m_closure[1] );


                tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207, const_str_plain___delete__, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 220;
                    type_description_2 = "c";
                    goto frame_exception_exit_2;
                }

#if 0
                RESTORE_FRAME_EXCEPTION( frame_b1d4f7855cc9d5161385116400c1b545_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_1;

                frame_exception_exit_2:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_b1d4f7855cc9d5161385116400c1b545_2 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_b1d4f7855cc9d5161385116400c1b545_2, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_b1d4f7855cc9d5161385116400c1b545_2->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_b1d4f7855cc9d5161385116400c1b545_2, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_b1d4f7855cc9d5161385116400c1b545_2,
                    type_description_2,
                    outline_0_var___class__
                );


                // Release cached frame.
                if ( frame_b1d4f7855cc9d5161385116400c1b545_2 == cache_frame_b1d4f7855cc9d5161385116400c1b545_2 )
                {
                    Py_DECREF( frame_b1d4f7855cc9d5161385116400c1b545_2 );
                }
                cache_frame_b1d4f7855cc9d5161385116400c1b545_2 = NULL;

                assertFrameObject( frame_b1d4f7855cc9d5161385116400c1b545_2 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_1;

                frame_no_exception_1:;
                goto skip_nested_handling_1;
                nested_frame_exit_1:;
                type_description_1 = "coooooocooccoNocc";
                goto try_except_handler_4;
                skip_nested_handling_1:;
                {
                    nuitka_bool tmp_condition_result_9;
                    PyObject *tmp_compexpr_left_2;
                    PyObject *tmp_compexpr_right_2;
                    CHECK_OBJECT( tmp_class_creation_1__bases );
                    tmp_compexpr_left_2 = tmp_class_creation_1__bases;
                    tmp_compexpr_right_2 = const_tuple_type_property_tuple;
                    tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 207;
                        type_description_1 = "coooooocooccoNocc";
                        goto try_except_handler_4;
                    }
                    tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_7;
                    }
                    else
                    {
                        goto branch_no_7;
                    }
                    branch_yes_7:;
                    tmp_dictset_value = const_tuple_type_property_tuple;
                    tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207, const_str_plain___orig_bases__, tmp_dictset_value );
                    if ( tmp_res != 0 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 207;
                        type_description_1 = "coooooocooccoNocc";
                        goto try_except_handler_4;
                    }
                    branch_no_7:;
                }
                {
                    PyObject *tmp_assign_source_16;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_args_name_2;
                    PyObject *tmp_tuple_element_3;
                    PyObject *tmp_kw_name_2;
                    CHECK_OBJECT( tmp_class_creation_1__metaclass );
                    tmp_called_name_2 = tmp_class_creation_1__metaclass;
                    tmp_tuple_element_3 = const_str_plain__deprecated_property;
                    tmp_args_name_2 = PyTuple_New( 3 );
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
                    CHECK_OBJECT( tmp_class_creation_1__bases );
                    tmp_tuple_element_3 = tmp_class_creation_1__bases;
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
                    tmp_tuple_element_3 = locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207;
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
                    CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                    tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
                    frame_7363cb49922ef5792c2d6b06382529ea->m_frame.f_lineno = 207;
                    tmp_assign_source_16 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
                    Py_DECREF( tmp_args_name_2 );
                    if ( tmp_assign_source_16 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 207;
                        type_description_1 = "coooooocooccoNocc";
                        goto try_except_handler_4;
                    }
                    assert( PyCell_GET( outline_0_var___class__ ) == NULL );
                    PyCell_SET( outline_0_var___class__, tmp_assign_source_16 );

                }
                CHECK_OBJECT( PyCell_GET( outline_0_var___class__ ) );
                tmp_assign_source_15 = PyCell_GET( outline_0_var___class__ );
                Py_INCREF( tmp_assign_source_15 );
                goto try_return_handler_4;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate );
                return NULL;
                // Return handler code:
                try_return_handler_4:;
                Py_DECREF( locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207 );
                locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207 = NULL;
                goto try_return_handler_3;
                // Exception handler code:
                try_except_handler_4:;
                exception_keeper_type_1 = exception_type;
                exception_keeper_value_1 = exception_value;
                exception_keeper_tb_1 = exception_tb;
                exception_keeper_lineno_1 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_DECREF( locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207 );
                locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207 = NULL;
                // Re-raise.
                exception_type = exception_keeper_type_1;
                exception_value = exception_keeper_value_1;
                exception_tb = exception_keeper_tb_1;
                exception_lineno = exception_keeper_lineno_1;

                goto try_except_handler_3;
                // End of try:
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate );
                return NULL;
                // Return handler code:
                try_return_handler_3:;
                CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
                Py_DECREF( outline_0_var___class__ );
                outline_0_var___class__ = NULL;

                goto outline_result_1;
                // Exception handler code:
                try_except_handler_3:;
                exception_keeper_type_2 = exception_type;
                exception_keeper_value_2 = exception_value;
                exception_keeper_tb_2 = exception_tb;
                exception_keeper_lineno_2 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                // Re-raise.
                exception_type = exception_keeper_type_2;
                exception_value = exception_keeper_value_2;
                exception_tb = exception_keeper_tb_2;
                exception_lineno = exception_keeper_lineno_2;

                goto outline_exception_1;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate );
                return NULL;
                outline_exception_1:;
                exception_lineno = 207;
                goto try_except_handler_2;
                outline_result_1:;
                assert( PyCell_GET( var__deprecated_property ) == NULL );
                PyCell_SET( var__deprecated_property, tmp_assign_source_15 );

            }
            goto try_end_1;
            // Exception handler code:
            try_except_handler_2:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_class_creation_1__bases );
            tmp_class_creation_1__bases = NULL;

            Py_XDECREF( tmp_class_creation_1__class_decl_dict );
            tmp_class_creation_1__class_decl_dict = NULL;

            Py_XDECREF( tmp_class_creation_1__metaclass );
            tmp_class_creation_1__metaclass = NULL;

            Py_XDECREF( tmp_class_creation_1__prepared );
            tmp_class_creation_1__prepared = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto frame_exception_exit_1;
            // End of try:
            try_end_1:;
            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
            Py_DECREF( tmp_class_creation_1__bases );
            tmp_class_creation_1__bases = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
            Py_DECREF( tmp_class_creation_1__class_decl_dict );
            tmp_class_creation_1__class_decl_dict = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
            Py_DECREF( tmp_class_creation_1__metaclass );
            tmp_class_creation_1__metaclass = NULL;

            CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
            Py_DECREF( tmp_class_creation_1__prepared );
            tmp_class_creation_1__prepared = NULL;

            {
                PyObject *tmp_assign_source_17;
                tmp_assign_source_17 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_5_finalize(  );

                ((struct Nuitka_FunctionObject *)tmp_assign_source_17)->m_closure[0] = var__deprecated_property;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_17)->m_closure[0] );
                ((struct Nuitka_FunctionObject *)tmp_assign_source_17)->m_closure[1] = par_obj;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_17)->m_closure[1] );


                assert( var_finalize == NULL );
                var_finalize = tmp_assign_source_17;
            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_10;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                CHECK_OBJECT( par_obj_type );
                tmp_compexpr_left_3 = par_obj_type;
                tmp_compexpr_right_3 = Py_None;
                tmp_condition_result_10 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                {
                    PyObject *tmp_assign_source_18;
                    tmp_assign_source_18 = const_str_plain_function;
                    {
                        PyObject *old = par_obj_type;
                        assert( old != NULL );
                        par_obj_type = tmp_assign_source_18;
                        Py_INCREF( par_obj_type );
                        Py_DECREF( old );
                    }

                }
                branch_no_8:;
            }
            {
                PyObject *tmp_assign_source_19;
                CHECK_OBJECT( PyCell_GET( par_obj ) );
                tmp_assign_source_19 = PyCell_GET( par_obj );
                assert( PyCell_GET( var_func ) == NULL );
                Py_INCREF( tmp_assign_source_19 );
                PyCell_SET( var_func, tmp_assign_source_19 );

            }
            {
                PyObject *tmp_assign_source_20;
                int tmp_or_left_truth_3;
                PyObject *tmp_or_left_value_3;
                PyObject *tmp_or_right_value_3;
                PyObject *tmp_source_name_11;
                CHECK_OBJECT( par_name );
                tmp_or_left_value_3 = par_name;
                tmp_or_left_truth_3 = CHECK_IF_TRUE( tmp_or_left_value_3 );
                if ( tmp_or_left_truth_3 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "coooooocooccoNocc";
                    goto frame_exception_exit_1;
                }
                if ( tmp_or_left_truth_3 == 1 )
                {
                    goto or_left_3;
                }
                else
                {
                    goto or_right_3;
                }
                or_right_3:;
                CHECK_OBJECT( PyCell_GET( par_obj ) );
                tmp_source_name_11 = PyCell_GET( par_obj );
                tmp_or_right_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain___name__ );
                if ( tmp_or_right_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "coooooocooccoNocc";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_20 = tmp_or_right_value_3;
                goto or_end_3;
                or_left_3:;
                Py_INCREF( tmp_or_left_value_3 );
                tmp_assign_source_20 = tmp_or_left_value_3;
                or_end_3:;
                {
                    PyObject *old = par_name;
                    assert( old != NULL );
                    par_name = tmp_assign_source_20;
                    Py_DECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_21;
                PyObject *tmp_source_name_12;
                CHECK_OBJECT( PyCell_GET( var_func ) );
                tmp_source_name_12 = PyCell_GET( var_func );
                tmp_assign_source_21 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain___doc__ );
                if ( tmp_assign_source_21 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 235;
                    type_description_1 = "coooooocooccoNocc";
                    goto frame_exception_exit_1;
                }
                assert( var_old_doc == NULL );
                var_old_doc = tmp_assign_source_21;
            }
            {
                PyObject *tmp_assign_source_22;
                tmp_assign_source_22 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_6_finalize(  );

                ((struct Nuitka_FunctionObject *)tmp_assign_source_22)->m_closure[0] = var_func;
                Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_22)->m_closure[0] );


                assert( var_finalize == NULL );
                var_finalize = tmp_assign_source_22;
            }
            branch_end_3:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_3;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_kw_name_3;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__generate_deprecation_warning );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__generate_deprecation_warning );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_generate_deprecation_warning" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 242;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_1;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "since" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 243;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_4 = PyCell_GET( self->m_closure[1] );
        tmp_args_name_3 = PyTuple_New( 7 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_4 );
        CHECK_OBJECT( par_message );
        tmp_tuple_element_4 = par_message;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_4 );
        if ( par_name == NULL )
        {
            Py_DECREF( tmp_args_name_3 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 243;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_4 = par_name;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_args_name_3, 2, tmp_tuple_element_4 );
        CHECK_OBJECT( par_alternative );
        tmp_tuple_element_4 = par_alternative;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_args_name_3, 3, tmp_tuple_element_4 );
        CHECK_OBJECT( par_pending );
        tmp_tuple_element_4 = par_pending;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_args_name_3, 4, tmp_tuple_element_4 );
        if ( par_obj_type == NULL )
        {
            Py_DECREF( tmp_args_name_3 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "obj_type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 243;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_4 = par_obj_type;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_args_name_3, 5, tmp_tuple_element_4 );
        CHECK_OBJECT( par_addendum );
        tmp_tuple_element_4 = par_addendum;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_args_name_3, 6, tmp_tuple_element_4 );
        tmp_dict_key_1 = const_str_plain_removal;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_args_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "removal" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 244;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_1 = PyCell_GET( self->m_closure[0] );
        tmp_kw_name_3 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_7363cb49922ef5792c2d6b06382529ea->m_frame.f_lineno = 242;
        tmp_assign_source_23 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
        Py_DECREF( tmp_args_name_3 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 242;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_warning ) == NULL );
        PyCell_SET( var_warning, tmp_assign_source_23 );

    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_7_wrapper(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_24)->m_closure[0] = var_func;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_24)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_24)->m_closure[1] = var_warning;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_24)->m_closure[1] );


        assert( var_wrapper == NULL );
        var_wrapper = tmp_assign_source_24;
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_13;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        int tmp_or_left_truth_4;
        PyObject *tmp_or_left_value_4;
        PyObject *tmp_or_right_value_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_inspect );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_inspect );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "inspect" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 251;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_13 = tmp_mvar_value_2;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_cleandoc );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        if ( var_old_doc == NULL )
        {
            Py_DECREF( tmp_called_name_4 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "old_doc" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 251;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }

        tmp_or_left_value_4 = var_old_doc;
        tmp_or_left_truth_4 = CHECK_IF_TRUE( tmp_or_left_value_4 );
        if ( tmp_or_left_truth_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 251;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_4 == 1 )
        {
            goto or_left_4;
        }
        else
        {
            goto or_right_4;
        }
        or_right_4:;
        tmp_or_right_value_4 = const_str_empty;
        tmp_args_element_name_1 = tmp_or_right_value_4;
        goto or_end_4;
        or_left_4:;
        tmp_args_element_name_1 = tmp_or_left_value_4;
        or_end_4:;
        frame_7363cb49922ef5792c2d6b06382529ea->m_frame.f_lineno = 251;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_instance_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        frame_7363cb49922ef5792c2d6b06382529ea->m_frame.f_lineno = 251;
        tmp_assign_source_25 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_strip, &PyTuple_GET_ITEM( const_tuple_str_newline_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_old_doc;
            var_old_doc = tmp_assign_source_25;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_string_concat_values_1;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_format_value_1;
        PyObject *tmp_format_spec_1;
        PyObject *tmp_format_value_2;
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_format_spec_2;
        PyObject *tmp_format_value_3;
        PyObject *tmp_format_spec_3;
        PyObject *tmp_format_value_4;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_format_spec_4;
        tmp_tuple_element_5 = const_str_digest_2bb788eca591478fe8c4968926c3b184;
        tmp_string_concat_values_1 = PyTuple_New( 8 );
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_string_concat_values_1, 0, tmp_tuple_element_5 );
        CHECK_OBJECT( var_old_doc );
        tmp_format_value_1 = var_old_doc;
        tmp_format_spec_1 = const_str_empty;
        tmp_tuple_element_5 = BUILTIN_FORMAT( tmp_format_value_1, tmp_format_spec_1 );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_string_concat_values_1 );

            exception_lineno = 254;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_string_concat_values_1, 1, tmp_tuple_element_5 );
        tmp_tuple_element_5 = const_str_newline;
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_string_concat_values_1, 2, tmp_tuple_element_5 );
        tmp_compexpr_left_4 = const_str_digest_cc52526bc81fb768b83183d7748c8ba6;
        CHECK_OBJECT( var_old_doc );
        tmp_compexpr_right_4 = var_old_doc;
        tmp_res = PySequence_Contains( tmp_compexpr_right_4, tmp_compexpr_left_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_string_concat_values_1 );

            exception_lineno = 254;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        tmp_format_value_2 = const_str_digest_cc52526bc81fb768b83183d7748c8ba6;
        goto condexpr_end_3;
        condexpr_false_3:;
        tmp_format_value_2 = const_str_empty;
        condexpr_end_3:;
        tmp_format_spec_2 = const_str_empty;
        tmp_tuple_element_5 = BUILTIN_FORMAT( tmp_format_value_2, tmp_format_spec_2 );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_string_concat_values_1 );

            exception_lineno = 254;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_string_concat_values_1, 3, tmp_tuple_element_5 );
        tmp_tuple_element_5 = const_str_digest_9521915caf951ff5c39d8dd5f8a1fb93;
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_string_concat_values_1, 4, tmp_tuple_element_5 );
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_string_concat_values_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "since" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 254;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }

        tmp_format_value_3 = PyCell_GET( self->m_closure[1] );
        tmp_format_spec_3 = const_str_empty;
        tmp_tuple_element_5 = BUILTIN_FORMAT( tmp_format_value_3, tmp_format_spec_3 );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_string_concat_values_1 );

            exception_lineno = 254;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_string_concat_values_1, 5, tmp_tuple_element_5 );
        tmp_tuple_element_5 = const_str_digest_09349449244a66b7d1fc2b9fa7edf9d0;
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_string_concat_values_1, 6, tmp_tuple_element_5 );
        CHECK_OBJECT( par_message );
        tmp_called_instance_2 = par_message;
        frame_7363cb49922ef5792c2d6b06382529ea->m_frame.f_lineno = 254;
        tmp_format_value_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_strip );
        if ( tmp_format_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_string_concat_values_1 );

            exception_lineno = 254;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        tmp_format_spec_4 = const_str_empty;
        tmp_tuple_element_5 = BUILTIN_FORMAT( tmp_format_value_4, tmp_format_spec_4 );
        Py_DECREF( tmp_format_value_4 );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_string_concat_values_1 );

            exception_lineno = 254;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_string_concat_values_1, 7, tmp_tuple_element_5 );
        tmp_assign_source_26 = PyUnicode_Join( const_str_empty, tmp_string_concat_values_1 );
        Py_DECREF( tmp_string_concat_values_1 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 254;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        assert( var_new_doc == NULL );
        var_new_doc = tmp_assign_source_26;
    }
    {
        nuitka_bool tmp_condition_result_12;
        PyObject *tmp_operand_name_2;
        CHECK_OBJECT( var_old_doc );
        tmp_operand_name_2 = var_old_doc;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            CHECK_OBJECT( var_new_doc );
            tmp_left_name_2 = var_new_doc;
            tmp_right_name_2 = const_str_digest_4db78317356ed7c8749566230afe4ac3;
            tmp_result = BINARY_OPERATION_ADD_UNICODE_UNICODE_INPLACE( &tmp_left_name_2, tmp_right_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 262;
                type_description_1 = "coooooocooccoNocc";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_27 = tmp_left_name_2;
            var_new_doc = tmp_assign_source_27;

        }
        branch_no_9:;
    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        if ( var_finalize == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "finalize" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 264;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = var_finalize;
        CHECK_OBJECT( var_wrapper );
        tmp_args_element_name_2 = var_wrapper;
        CHECK_OBJECT( var_new_doc );
        tmp_args_element_name_3 = var_new_doc;
        frame_7363cb49922ef5792c2d6b06382529ea->m_frame.f_lineno = 264;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 264;
            type_description_1 = "coooooocooccoNocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7363cb49922ef5792c2d6b06382529ea );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7363cb49922ef5792c2d6b06382529ea );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7363cb49922ef5792c2d6b06382529ea );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7363cb49922ef5792c2d6b06382529ea, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7363cb49922ef5792c2d6b06382529ea->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7363cb49922ef5792c2d6b06382529ea, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7363cb49922ef5792c2d6b06382529ea,
        type_description_1,
        par_obj,
        par_message,
        par_name,
        par_alternative,
        par_pending,
        par_obj_type,
        par_addendum,
        var_func,
        var_old_doc,
        var_finalize,
        var__deprecated_property,
        var_warning,
        var_wrapper,
        NULL,
        var_new_doc,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_7363cb49922ef5792c2d6b06382529ea == cache_frame_7363cb49922ef5792c2d6b06382529ea )
    {
        Py_DECREF( frame_7363cb49922ef5792c2d6b06382529ea );
    }
    cache_frame_7363cb49922ef5792c2d6b06382529ea = NULL;

    assertFrameObject( frame_7363cb49922ef5792c2d6b06382529ea );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    Py_XDECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_alternative );
    Py_DECREF( par_alternative );
    par_alternative = NULL;

    CHECK_OBJECT( (PyObject *)par_pending );
    Py_DECREF( par_pending );
    par_pending = NULL;

    Py_XDECREF( par_obj_type );
    par_obj_type = NULL;

    CHECK_OBJECT( (PyObject *)par_addendum );
    Py_DECREF( par_addendum );
    par_addendum = NULL;

    CHECK_OBJECT( (PyObject *)var_func );
    Py_DECREF( var_func );
    var_func = NULL;

    CHECK_OBJECT( (PyObject *)var_old_doc );
    Py_DECREF( var_old_doc );
    var_old_doc = NULL;

    Py_XDECREF( var_finalize );
    var_finalize = NULL;

    CHECK_OBJECT( (PyObject *)var__deprecated_property );
    Py_DECREF( var__deprecated_property );
    var__deprecated_property = NULL;

    CHECK_OBJECT( (PyObject *)var_warning );
    Py_DECREF( var_warning );
    var_warning = NULL;

    CHECK_OBJECT( (PyObject *)var_wrapper );
    Py_DECREF( var_wrapper );
    var_wrapper = NULL;

    CHECK_OBJECT( (PyObject *)var_new_doc );
    Py_DECREF( var_new_doc );
    var_new_doc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    Py_XDECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_alternative );
    Py_DECREF( par_alternative );
    par_alternative = NULL;

    CHECK_OBJECT( (PyObject *)par_pending );
    Py_DECREF( par_pending );
    par_pending = NULL;

    Py_XDECREF( par_obj_type );
    par_obj_type = NULL;

    CHECK_OBJECT( (PyObject *)par_addendum );
    Py_DECREF( par_addendum );
    par_addendum = NULL;

    CHECK_OBJECT( (PyObject *)var_func );
    Py_DECREF( var_func );
    var_func = NULL;

    Py_XDECREF( var_old_doc );
    var_old_doc = NULL;

    Py_XDECREF( var_finalize );
    var_finalize = NULL;

    CHECK_OBJECT( (PyObject *)var__deprecated_property );
    Py_DECREF( var__deprecated_property );
    var__deprecated_property = NULL;

    CHECK_OBJECT( (PyObject *)var_warning );
    Py_DECREF( var_warning );
    var_warning = NULL;

    Py_XDECREF( var_wrapper );
    var_wrapper = NULL;

    Py_XDECREF( var_new_doc );
    var_new_doc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate );
    return NULL;

function_exception_exit:
    Py_XDECREF( locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207 );
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.
    Py_XDECREF( locals_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate_207 );


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_1_finalize( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_wrapper = python_pars[ 0 ];
    PyObject *par_new_doc = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_8fa74b6215bc1eaee564d2cb5e6e5d20;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_8fa74b6215bc1eaee564d2cb5e6e5d20 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8fa74b6215bc1eaee564d2cb5e6e5d20, codeobj_8fa74b6215bc1eaee564d2cb5e6e5d20, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8fa74b6215bc1eaee564d2cb5e6e5d20 = cache_frame_8fa74b6215bc1eaee564d2cb5e6e5d20;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8fa74b6215bc1eaee564d2cb5e6e5d20 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8fa74b6215bc1eaee564d2cb5e6e5d20 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_new_doc );
        tmp_assattr_name_1 = par_new_doc;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "obj" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 195;
            type_description_1 = "ooc";
            goto try_except_handler_2;
        }

        tmp_assattr_target_1 = PyCell_GET( self->m_closure[0] );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain___doc__, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 195;
            type_description_1 = "ooc";
            goto try_except_handler_2;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_8fa74b6215bc1eaee564d2cb5e6e5d20, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_8fa74b6215bc1eaee564d2cb5e6e5d20, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_AttributeError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_1 = "ooc";
            goto try_except_handler_3;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_1 = "ooc";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 194;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_8fa74b6215bc1eaee564d2cb5e6e5d20->m_frame) frame_8fa74b6215bc1eaee564d2cb5e6e5d20->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooc";
        goto try_except_handler_3;
        branch_no_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_1_finalize );
    return NULL;
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_wrapper );
        tmp_assattr_name_2 = par_wrapper;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "obj" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 198;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }

        tmp_assattr_target_2 = PyCell_GET( self->m_closure[0] );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain___init__, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
    }
    if ( PyCell_GET( self->m_closure[0] ) == NULL )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "obj" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 199;
        type_description_1 = "ooc";
        goto frame_exception_exit_1;
    }

    tmp_return_value = PyCell_GET( self->m_closure[0] );
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8fa74b6215bc1eaee564d2cb5e6e5d20 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8fa74b6215bc1eaee564d2cb5e6e5d20 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8fa74b6215bc1eaee564d2cb5e6e5d20 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8fa74b6215bc1eaee564d2cb5e6e5d20, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8fa74b6215bc1eaee564d2cb5e6e5d20->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8fa74b6215bc1eaee564d2cb5e6e5d20, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8fa74b6215bc1eaee564d2cb5e6e5d20,
        type_description_1,
        par_wrapper,
        par_new_doc,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_8fa74b6215bc1eaee564d2cb5e6e5d20 == cache_frame_8fa74b6215bc1eaee564d2cb5e6e5d20 )
    {
        Py_DECREF( frame_8fa74b6215bc1eaee564d2cb5e6e5d20 );
    }
    cache_frame_8fa74b6215bc1eaee564d2cb5e6e5d20 = NULL;

    assertFrameObject( frame_8fa74b6215bc1eaee564d2cb5e6e5d20 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_1_finalize );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_wrapper );
    Py_DECREF( par_wrapper );
    par_wrapper = NULL;

    CHECK_OBJECT( (PyObject *)par_new_doc );
    Py_DECREF( par_new_doc );
    par_new_doc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_wrapper );
    Py_DECREF( par_wrapper );
    par_wrapper = NULL;

    CHECK_OBJECT( (PyObject *)par_new_doc );
    Py_DECREF( par_new_doc );
    par_new_doc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_1_finalize );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_2___get__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_instance = python_pars[ 1 ];
    PyObject *par_owner = python_pars[ 2 ];
    PyObject *var__warn_external = NULL;
    struct Nuitka_FrameObject *frame_08c2622f35a3431ed3ee5a285ea3d111;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_08c2622f35a3431ed3ee5a285ea3d111 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_08c2622f35a3431ed3ee5a285ea3d111, codeobj_08c2622f35a3431ed3ee5a285ea3d111, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_08c2622f35a3431ed3ee5a285ea3d111 = cache_frame_08c2622f35a3431ed3ee5a285ea3d111;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_08c2622f35a3431ed3ee5a285ea3d111 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_08c2622f35a3431ed3ee5a285ea3d111 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_instance );
        tmp_compexpr_left_1 = par_instance;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_import_name_from_1;
            PyObject *tmp_name_name_1;
            PyObject *tmp_globals_name_1;
            PyObject *tmp_locals_name_1;
            PyObject *tmp_fromlist_name_1;
            PyObject *tmp_level_name_1;
            tmp_name_name_1 = const_str_empty;
            tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$cbook$deprecation;
            tmp_locals_name_1 = Py_None;
            tmp_fromlist_name_1 = const_tuple_str_plain__warn_external_tuple;
            tmp_level_name_1 = const_int_pos_1;
            frame_08c2622f35a3431ed3ee5a285ea3d111->m_frame.f_lineno = 210;
            tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
            if ( tmp_import_name_from_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;
                type_description_1 = "oooocc";
                goto frame_exception_exit_1;
            }
            if ( PyModule_Check( tmp_import_name_from_1 ) )
            {
               tmp_assign_source_1 = IMPORT_NAME_OR_MODULE(
                    tmp_import_name_from_1,
                    (PyObject *)moduledict_matplotlib$cbook$deprecation,
                    const_str_plain__warn_external,
                    const_int_pos_1
                );
            }
            else
            {
               tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain__warn_external );
            }

            Py_DECREF( tmp_import_name_from_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;
                type_description_1 = "oooocc";
                goto frame_exception_exit_1;
            }
            assert( var__warn_external == NULL );
            var__warn_external = tmp_assign_source_1;
        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var__warn_external );
            tmp_called_name_1 = var__warn_external;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "warning" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 211;
                type_description_1 = "oooocc";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = PyCell_GET( self->m_closure[1] );
            frame_08c2622f35a3431ed3ee5a285ea3d111->m_frame.f_lineno = 211;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "oooocc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 212;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_called_instance_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_instance );
        tmp_args_element_name_2 = par_instance;
        CHECK_OBJECT( par_owner );
        tmp_args_element_name_3 = par_owner;
        frame_08c2622f35a3431ed3ee5a285ea3d111->m_frame.f_lineno = 212;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain___get__, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_08c2622f35a3431ed3ee5a285ea3d111 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_08c2622f35a3431ed3ee5a285ea3d111 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_08c2622f35a3431ed3ee5a285ea3d111 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_08c2622f35a3431ed3ee5a285ea3d111, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_08c2622f35a3431ed3ee5a285ea3d111->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_08c2622f35a3431ed3ee5a285ea3d111, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_08c2622f35a3431ed3ee5a285ea3d111,
        type_description_1,
        par_self,
        par_instance,
        par_owner,
        var__warn_external,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_08c2622f35a3431ed3ee5a285ea3d111 == cache_frame_08c2622f35a3431ed3ee5a285ea3d111 )
    {
        Py_DECREF( frame_08c2622f35a3431ed3ee5a285ea3d111 );
    }
    cache_frame_08c2622f35a3431ed3ee5a285ea3d111 = NULL;

    assertFrameObject( frame_08c2622f35a3431ed3ee5a285ea3d111 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_2___get__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_instance );
    Py_DECREF( par_instance );
    par_instance = NULL;

    CHECK_OBJECT( (PyObject *)par_owner );
    Py_DECREF( par_owner );
    par_owner = NULL;

    Py_XDECREF( var__warn_external );
    var__warn_external = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_instance );
    Py_DECREF( par_instance );
    par_instance = NULL;

    CHECK_OBJECT( (PyObject *)par_owner );
    Py_DECREF( par_owner );
    par_owner = NULL;

    Py_XDECREF( var__warn_external );
    var__warn_external = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_2___get__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_3___set__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_instance = python_pars[ 1 ];
    PyObject *par_value = python_pars[ 2 ];
    PyObject *var__warn_external = NULL;
    struct Nuitka_FrameObject *frame_112af24ecb9a463e995654111c3f152a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_112af24ecb9a463e995654111c3f152a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_112af24ecb9a463e995654111c3f152a, codeobj_112af24ecb9a463e995654111c3f152a, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_112af24ecb9a463e995654111c3f152a = cache_frame_112af24ecb9a463e995654111c3f152a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_112af24ecb9a463e995654111c3f152a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_112af24ecb9a463e995654111c3f152a ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_instance );
        tmp_compexpr_left_1 = par_instance;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_import_name_from_1;
            PyObject *tmp_name_name_1;
            PyObject *tmp_globals_name_1;
            PyObject *tmp_locals_name_1;
            PyObject *tmp_fromlist_name_1;
            PyObject *tmp_level_name_1;
            tmp_name_name_1 = const_str_empty;
            tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$cbook$deprecation;
            tmp_locals_name_1 = Py_None;
            tmp_fromlist_name_1 = const_tuple_str_plain__warn_external_tuple;
            tmp_level_name_1 = const_int_pos_1;
            frame_112af24ecb9a463e995654111c3f152a->m_frame.f_lineno = 216;
            tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
            if ( tmp_import_name_from_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 216;
                type_description_1 = "oooocc";
                goto frame_exception_exit_1;
            }
            if ( PyModule_Check( tmp_import_name_from_1 ) )
            {
               tmp_assign_source_1 = IMPORT_NAME_OR_MODULE(
                    tmp_import_name_from_1,
                    (PyObject *)moduledict_matplotlib$cbook$deprecation,
                    const_str_plain__warn_external,
                    const_int_pos_1
                );
            }
            else
            {
               tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain__warn_external );
            }

            Py_DECREF( tmp_import_name_from_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 216;
                type_description_1 = "oooocc";
                goto frame_exception_exit_1;
            }
            assert( var__warn_external == NULL );
            var__warn_external = tmp_assign_source_1;
        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var__warn_external );
            tmp_called_name_1 = var__warn_external;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "warning" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 217;
                type_description_1 = "oooocc";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = PyCell_GET( self->m_closure[1] );
            frame_112af24ecb9a463e995654111c3f152a->m_frame.f_lineno = 217;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 217;
                type_description_1 = "oooocc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 218;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_called_instance_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_instance );
        tmp_args_element_name_2 = par_instance;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_3 = par_value;
        frame_112af24ecb9a463e995654111c3f152a->m_frame.f_lineno = 218;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain___set__, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_1 = "oooocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_112af24ecb9a463e995654111c3f152a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_112af24ecb9a463e995654111c3f152a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_112af24ecb9a463e995654111c3f152a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_112af24ecb9a463e995654111c3f152a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_112af24ecb9a463e995654111c3f152a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_112af24ecb9a463e995654111c3f152a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_112af24ecb9a463e995654111c3f152a,
        type_description_1,
        par_self,
        par_instance,
        par_value,
        var__warn_external,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_112af24ecb9a463e995654111c3f152a == cache_frame_112af24ecb9a463e995654111c3f152a )
    {
        Py_DECREF( frame_112af24ecb9a463e995654111c3f152a );
    }
    cache_frame_112af24ecb9a463e995654111c3f152a = NULL;

    assertFrameObject( frame_112af24ecb9a463e995654111c3f152a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_3___set__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_instance );
    Py_DECREF( par_instance );
    par_instance = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    Py_XDECREF( var__warn_external );
    var__warn_external = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_instance );
    Py_DECREF( par_instance );
    par_instance = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    Py_XDECREF( var__warn_external );
    var__warn_external = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_3___set__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_4___delete__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_instance = python_pars[ 1 ];
    PyObject *var__warn_external = NULL;
    struct Nuitka_FrameObject *frame_6bfb613eacf0e6932019da285c419273;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_6bfb613eacf0e6932019da285c419273 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6bfb613eacf0e6932019da285c419273, codeobj_6bfb613eacf0e6932019da285c419273, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6bfb613eacf0e6932019da285c419273 = cache_frame_6bfb613eacf0e6932019da285c419273;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6bfb613eacf0e6932019da285c419273 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6bfb613eacf0e6932019da285c419273 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_instance );
        tmp_compexpr_left_1 = par_instance;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_import_name_from_1;
            PyObject *tmp_name_name_1;
            PyObject *tmp_globals_name_1;
            PyObject *tmp_locals_name_1;
            PyObject *tmp_fromlist_name_1;
            PyObject *tmp_level_name_1;
            tmp_name_name_1 = const_str_empty;
            tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$cbook$deprecation;
            tmp_locals_name_1 = Py_None;
            tmp_fromlist_name_1 = const_tuple_str_plain__warn_external_tuple;
            tmp_level_name_1 = const_int_pos_1;
            frame_6bfb613eacf0e6932019da285c419273->m_frame.f_lineno = 222;
            tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
            if ( tmp_import_name_from_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 222;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            if ( PyModule_Check( tmp_import_name_from_1 ) )
            {
               tmp_assign_source_1 = IMPORT_NAME_OR_MODULE(
                    tmp_import_name_from_1,
                    (PyObject *)moduledict_matplotlib$cbook$deprecation,
                    const_str_plain__warn_external,
                    const_int_pos_1
                );
            }
            else
            {
               tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain__warn_external );
            }

            Py_DECREF( tmp_import_name_from_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 222;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            assert( var__warn_external == NULL );
            var__warn_external = tmp_assign_source_1;
        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var__warn_external );
            tmp_called_name_1 = var__warn_external;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "warning" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 223;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = PyCell_GET( self->m_closure[1] );
            frame_6bfb613eacf0e6932019da285c419273->m_frame.f_lineno = 223;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 223;
                type_description_1 = "ooocc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_args_element_name_2;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "__class__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 224;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_called_instance_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_instance );
        tmp_args_element_name_2 = par_instance;
        frame_6bfb613eacf0e6932019da285c419273->m_frame.f_lineno = 224;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain___delete__, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6bfb613eacf0e6932019da285c419273 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6bfb613eacf0e6932019da285c419273 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6bfb613eacf0e6932019da285c419273 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6bfb613eacf0e6932019da285c419273, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6bfb613eacf0e6932019da285c419273->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6bfb613eacf0e6932019da285c419273, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6bfb613eacf0e6932019da285c419273,
        type_description_1,
        par_self,
        par_instance,
        var__warn_external,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_6bfb613eacf0e6932019da285c419273 == cache_frame_6bfb613eacf0e6932019da285c419273 )
    {
        Py_DECREF( frame_6bfb613eacf0e6932019da285c419273 );
    }
    cache_frame_6bfb613eacf0e6932019da285c419273 = NULL;

    assertFrameObject( frame_6bfb613eacf0e6932019da285c419273 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_4___delete__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_instance );
    Py_DECREF( par_instance );
    par_instance = NULL;

    Py_XDECREF( var__warn_external );
    var__warn_external = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_instance );
    Py_DECREF( par_instance );
    par_instance = NULL;

    Py_XDECREF( var__warn_external );
    var__warn_external = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_4___delete__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_5_finalize( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par__ = python_pars[ 0 ];
    PyObject *par_new_doc = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_f5dbb84adc05a8b8017b07af877767e0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_f5dbb84adc05a8b8017b07af877767e0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f5dbb84adc05a8b8017b07af877767e0, codeobj_f5dbb84adc05a8b8017b07af877767e0, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f5dbb84adc05a8b8017b07af877767e0 = cache_frame_f5dbb84adc05a8b8017b07af877767e0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f5dbb84adc05a8b8017b07af877767e0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f5dbb84adc05a8b8017b07af877767e0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "_deprecated_property" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 227;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[0] );
        tmp_dict_key_1 = const_str_plain_fget;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "obj" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 228;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[1] );
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_fget );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_fset;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "obj" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 228;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = PyCell_GET( self->m_closure[1] );
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_fset );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 228;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_fdel;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "obj" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 228;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = PyCell_GET( self->m_closure[1] );
        tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_fdel );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 228;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_doc;
        CHECK_OBJECT( par_new_doc );
        tmp_dict_value_4 = par_new_doc;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_f5dbb84adc05a8b8017b07af877767e0->m_frame.f_lineno = 227;
        tmp_return_value = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f5dbb84adc05a8b8017b07af877767e0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f5dbb84adc05a8b8017b07af877767e0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f5dbb84adc05a8b8017b07af877767e0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f5dbb84adc05a8b8017b07af877767e0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f5dbb84adc05a8b8017b07af877767e0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f5dbb84adc05a8b8017b07af877767e0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f5dbb84adc05a8b8017b07af877767e0,
        type_description_1,
        par__,
        par_new_doc,
        self->m_closure[0],
        self->m_closure[1]
    );


    // Release cached frame.
    if ( frame_f5dbb84adc05a8b8017b07af877767e0 == cache_frame_f5dbb84adc05a8b8017b07af877767e0 )
    {
        Py_DECREF( frame_f5dbb84adc05a8b8017b07af877767e0 );
    }
    cache_frame_f5dbb84adc05a8b8017b07af877767e0 = NULL;

    assertFrameObject( frame_f5dbb84adc05a8b8017b07af877767e0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_5_finalize );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par__ );
    Py_DECREF( par__ );
    par__ = NULL;

    CHECK_OBJECT( (PyObject *)par_new_doc );
    Py_DECREF( par_new_doc );
    par_new_doc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par__ );
    Py_DECREF( par__ );
    par__ = NULL;

    CHECK_OBJECT( (PyObject *)par_new_doc );
    Py_DECREF( par_new_doc );
    par_new_doc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_5_finalize );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_6_finalize( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_wrapper = python_pars[ 0 ];
    PyObject *par_new_doc = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_eb66ce6a82024475054afab867381783;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_eb66ce6a82024475054afab867381783 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_eb66ce6a82024475054afab867381783, codeobj_eb66ce6a82024475054afab867381783, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_eb66ce6a82024475054afab867381783 = cache_frame_eb66ce6a82024475054afab867381783;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_eb66ce6a82024475054afab867381783 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_eb66ce6a82024475054afab867381783 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_functools );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 238;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_wraps );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 238;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[0] );
        frame_eb66ce6a82024475054afab867381783->m_frame.f_lineno = 238;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_called_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_wrapper );
        tmp_args_element_name_2 = par_wrapper;
        frame_eb66ce6a82024475054afab867381783->m_frame.f_lineno = 238;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_wrapper;
            assert( old != NULL );
            par_wrapper = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_new_doc );
        tmp_assattr_name_1 = par_new_doc;
        CHECK_OBJECT( par_wrapper );
        tmp_assattr_target_1 = par_wrapper;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain___doc__, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 239;
            type_description_1 = "ooc";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb66ce6a82024475054afab867381783 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb66ce6a82024475054afab867381783 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eb66ce6a82024475054afab867381783, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eb66ce6a82024475054afab867381783->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eb66ce6a82024475054afab867381783, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_eb66ce6a82024475054afab867381783,
        type_description_1,
        par_wrapper,
        par_new_doc,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_eb66ce6a82024475054afab867381783 == cache_frame_eb66ce6a82024475054afab867381783 )
    {
        Py_DECREF( frame_eb66ce6a82024475054afab867381783 );
    }
    cache_frame_eb66ce6a82024475054afab867381783 = NULL;

    assertFrameObject( frame_eb66ce6a82024475054afab867381783 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_wrapper );
    tmp_return_value = par_wrapper;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_6_finalize );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_wrapper );
    Py_DECREF( par_wrapper );
    par_wrapper = NULL;

    CHECK_OBJECT( (PyObject *)par_new_doc );
    Py_DECREF( par_new_doc );
    par_new_doc = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_wrapper );
    Py_DECREF( par_wrapper );
    par_wrapper = NULL;

    CHECK_OBJECT( (PyObject *)par_new_doc );
    Py_DECREF( par_new_doc );
    par_new_doc = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_6_finalize );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_7_wrapper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    PyObject *var__warn_external = NULL;
    struct Nuitka_FrameObject *frame_f861e5fed7f650f3780bd3601addbbda;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_f861e5fed7f650f3780bd3601addbbda = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f861e5fed7f650f3780bd3601addbbda, codeobj_f861e5fed7f650f3780bd3601addbbda, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f861e5fed7f650f3780bd3601addbbda = cache_frame_f861e5fed7f650f3780bd3601addbbda;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f861e5fed7f650f3780bd3601addbbda );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f861e5fed7f650f3780bd3601addbbda ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_empty;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$cbook$deprecation;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain__warn_external_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_f861e5fed7f650f3780bd3601addbbda->m_frame.f_lineno = 247;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_1 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_matplotlib$cbook$deprecation,
                const_str_plain__warn_external,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain__warn_external );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        assert( var__warn_external == NULL );
        var__warn_external = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var__warn_external );
        tmp_called_name_1 = var__warn_external;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "warning" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 248;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( self->m_closure[1] );
        frame_f861e5fed7f650f3780bd3601addbbda->m_frame.f_lineno = 248;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 249;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 249;
            type_description_1 = "ooocc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f861e5fed7f650f3780bd3601addbbda );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f861e5fed7f650f3780bd3601addbbda );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f861e5fed7f650f3780bd3601addbbda );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f861e5fed7f650f3780bd3601addbbda, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f861e5fed7f650f3780bd3601addbbda->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f861e5fed7f650f3780bd3601addbbda, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f861e5fed7f650f3780bd3601addbbda,
        type_description_1,
        par_args,
        par_kwargs,
        var__warn_external,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_f861e5fed7f650f3780bd3601addbbda == cache_frame_f861e5fed7f650f3780bd3601addbbda )
    {
        Py_DECREF( frame_f861e5fed7f650f3780bd3601addbbda );
    }
    cache_frame_f861e5fed7f650f3780bd3601addbbda = NULL;

    assertFrameObject( frame_f861e5fed7f650f3780bd3601addbbda );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_7_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var__warn_external );
    Py_DECREF( var__warn_external );
    var__warn_external = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var__warn_external );
    var__warn_external = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_7_wrapper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_4__rename_parameter( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_since = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_old = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_new = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *par_func = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *var_signature = NULL;
    PyObject *var_wrapper = NULL;
    struct Nuitka_FrameObject *frame_27e5cf7205556ea8e8b237cc9fb5b318;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_27e5cf7205556ea8e8b237cc9fb5b318 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_27e5cf7205556ea8e8b237cc9fb5b318, codeobj_27e5cf7205556ea8e8b237cc9fb5b318, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_27e5cf7205556ea8e8b237cc9fb5b318 = cache_frame_27e5cf7205556ea8e8b237cc9fb5b318;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_27e5cf7205556ea8e8b237cc9fb5b318 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_27e5cf7205556ea8e8b237cc9fb5b318 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_compexpr_left_1 = PyCell_GET( par_func );
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_functools );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 289;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_partial );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__rename_parameter );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__rename_parameter );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_rename_parameter" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 289;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = tmp_mvar_value_2;
            CHECK_OBJECT( PyCell_GET( par_since ) );
            tmp_args_element_name_2 = PyCell_GET( par_since );
            CHECK_OBJECT( PyCell_GET( par_old ) );
            tmp_args_element_name_3 = PyCell_GET( par_old );
            CHECK_OBJECT( PyCell_GET( par_new ) );
            tmp_args_element_name_4 = PyCell_GET( par_new );
            frame_27e5cf7205556ea8e8b237cc9fb5b318->m_frame.f_lineno = 289;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_inspect );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_inspect );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "inspect" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 291;
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_args_element_name_5 = PyCell_GET( par_func );
        frame_27e5cf7205556ea8e8b237cc9fb5b318->m_frame.f_lineno = 291;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_signature, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 291;
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }
        assert( var_signature == NULL );
        var_signature = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( PyCell_GET( par_old ) );
        tmp_compexpr_left_2 = PyCell_GET( par_old );
        CHECK_OBJECT( var_signature );
        tmp_source_name_2 = var_signature;
        tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_parameters );
        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 292;
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 292;
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_raise_value_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_string_concat_values_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_format_value_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_format_spec_1;
            PyObject *tmp_format_value_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_format_spec_2;
            tmp_raise_type_1 = PyExc_AssertionError;
            tmp_tuple_element_2 = const_str_digest_618c96c176b977c5f504ed0a642f72f7;
            tmp_string_concat_values_1 = PyTuple_New( 5 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( PyCell_GET( par_old ) );
            tmp_operand_name_1 = PyCell_GET( par_old );
            tmp_format_value_1 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_1 );
            if ( tmp_format_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 293;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_1 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_1, tmp_format_spec_1 );
            Py_DECREF( tmp_format_value_1 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 293;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 1, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_893a7ac8b87a8aa2fd33f50d9212578f;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 2, tmp_tuple_element_2 );
            CHECK_OBJECT( PyCell_GET( par_func ) );
            tmp_source_name_3 = PyCell_GET( par_func );
            tmp_format_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___name__ );
            if ( tmp_format_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 293;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_2 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_2, tmp_format_spec_2 );
            Py_DECREF( tmp_format_value_2 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 293;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 3, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_25731c733fd74e8333aa29126ce85686;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 4, tmp_tuple_element_2 );
            tmp_tuple_element_1 = PyUnicode_Join( const_str_empty, tmp_string_concat_values_1 );
            Py_DECREF( tmp_string_concat_values_1 );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 293;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            tmp_raise_value_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_raise_value_1, 0, tmp_tuple_element_1 );
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_value = tmp_raise_value_1;
            exception_lineno = 292;
            RAISE_EXCEPTION_WITH_VALUE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( PyCell_GET( par_new ) );
        tmp_compexpr_left_3 = PyCell_GET( par_new );
        CHECK_OBJECT( var_signature );
        tmp_source_name_4 = var_signature;
        tmp_compexpr_right_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_parameters );
        if ( tmp_compexpr_right_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 295;
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
        Py_DECREF( tmp_compexpr_right_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 295;
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_raise_value_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_string_concat_values_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_format_value_3;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_format_spec_3;
            PyObject *tmp_format_value_4;
            PyObject *tmp_source_name_5;
            PyObject *tmp_format_spec_4;
            tmp_raise_type_2 = PyExc_AssertionError;
            tmp_tuple_element_4 = const_str_digest_618c96c176b977c5f504ed0a642f72f7;
            tmp_string_concat_values_2 = PyTuple_New( 5 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_string_concat_values_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( PyCell_GET( par_new ) );
            tmp_operand_name_2 = PyCell_GET( par_new );
            tmp_format_value_3 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_2 );
            if ( tmp_format_value_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_2 );

                exception_lineno = 296;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_3 = const_str_empty;
            tmp_tuple_element_4 = BUILTIN_FORMAT( tmp_format_value_3, tmp_format_spec_3 );
            Py_DECREF( tmp_format_value_3 );
            if ( tmp_tuple_element_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_2 );

                exception_lineno = 296;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = const_str_digest_21397472cac24efd9c899754fc958a99;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_string_concat_values_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( PyCell_GET( par_func ) );
            tmp_source_name_5 = PyCell_GET( par_func );
            tmp_format_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain___name__ );
            if ( tmp_format_value_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_2 );

                exception_lineno = 296;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_4 = const_str_empty;
            tmp_tuple_element_4 = BUILTIN_FORMAT( tmp_format_value_4, tmp_format_spec_4 );
            Py_DECREF( tmp_format_value_4 );
            if ( tmp_tuple_element_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_2 );

                exception_lineno = 296;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_2, 3, tmp_tuple_element_4 );
            tmp_tuple_element_4 = const_str_digest_25731c733fd74e8333aa29126ce85686;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_string_concat_values_2, 4, tmp_tuple_element_4 );
            tmp_tuple_element_3 = PyUnicode_Join( const_str_empty, tmp_string_concat_values_2 );
            Py_DECREF( tmp_string_concat_values_2 );
            if ( tmp_tuple_element_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 296;
                type_description_1 = "ccccoo";
                goto frame_exception_exit_1;
            }
            tmp_raise_value_2 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_raise_value_2, 0, tmp_tuple_element_3 );
            exception_type = tmp_raise_type_2;
            Py_INCREF( tmp_raise_type_2 );
            exception_value = tmp_raise_value_2;
            exception_lineno = 295;
            RAISE_EXCEPTION_WITH_VALUE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }
        branch_no_3:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_functools );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 299;
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_4;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_args_element_name_6 = PyCell_GET( par_func );
        frame_27e5cf7205556ea8e8b237cc9fb5b318->m_frame.f_lineno = 299;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_called_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_wraps, call_args );
        }

        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_7 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_4__rename_parameter$$$function_1_wrapper(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[0] = par_func;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[1] = par_new;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[2] = par_old;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[3] = par_since;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[3] );


        frame_27e5cf7205556ea8e8b237cc9fb5b318->m_frame.f_lineno = 299;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_1 = "ccccoo";
            goto frame_exception_exit_1;
        }
        assert( var_wrapper == NULL );
        var_wrapper = tmp_assign_source_2;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27e5cf7205556ea8e8b237cc9fb5b318 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_27e5cf7205556ea8e8b237cc9fb5b318 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27e5cf7205556ea8e8b237cc9fb5b318 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_27e5cf7205556ea8e8b237cc9fb5b318, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_27e5cf7205556ea8e8b237cc9fb5b318->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_27e5cf7205556ea8e8b237cc9fb5b318, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_27e5cf7205556ea8e8b237cc9fb5b318,
        type_description_1,
        par_since,
        par_old,
        par_new,
        par_func,
        var_signature,
        var_wrapper
    );


    // Release cached frame.
    if ( frame_27e5cf7205556ea8e8b237cc9fb5b318 == cache_frame_27e5cf7205556ea8e8b237cc9fb5b318 )
    {
        Py_DECREF( frame_27e5cf7205556ea8e8b237cc9fb5b318 );
    }
    cache_frame_27e5cf7205556ea8e8b237cc9fb5b318 = NULL;

    assertFrameObject( frame_27e5cf7205556ea8e8b237cc9fb5b318 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_wrapper );
    tmp_return_value = var_wrapper;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_4__rename_parameter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    CHECK_OBJECT( (PyObject *)par_old );
    Py_DECREF( par_old );
    par_old = NULL;

    CHECK_OBJECT( (PyObject *)par_new );
    Py_DECREF( par_new );
    par_new = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    Py_XDECREF( var_signature );
    var_signature = NULL;

    Py_XDECREF( var_wrapper );
    var_wrapper = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    CHECK_OBJECT( (PyObject *)par_old );
    Py_DECREF( par_old );
    par_old = NULL;

    CHECK_OBJECT( (PyObject *)par_new );
    Py_DECREF( par_new );
    par_new = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    Py_XDECREF( var_signature );
    var_signature = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_4__rename_parameter );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_4__rename_parameter$$$function_1_wrapper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_5023d2a43be9bdf9a8b72aeeb1807ffe;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_5023d2a43be9bdf9a8b72aeeb1807ffe = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5023d2a43be9bdf9a8b72aeeb1807ffe, codeobj_5023d2a43be9bdf9a8b72aeeb1807ffe, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_5023d2a43be9bdf9a8b72aeeb1807ffe = cache_frame_5023d2a43be9bdf9a8b72aeeb1807ffe;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5023d2a43be9bdf9a8b72aeeb1807ffe );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5023d2a43be9bdf9a8b72aeeb1807ffe ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "old" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 301;
            type_description_1 = "oocccc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = PyCell_GET( self->m_closure[2] );
        CHECK_OBJECT( par_kwargs );
        tmp_compexpr_right_1 = par_kwargs;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 301;
            type_description_1 = "oocccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_string_concat_values_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_format_value_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_format_spec_1;
            PyObject *tmp_format_value_2;
            PyObject *tmp_source_name_1;
            PyObject *tmp_format_spec_2;
            PyObject *tmp_format_value_3;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_format_spec_3;
            PyObject *tmp_format_value_4;
            PyObject *tmp_format_spec_4;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_warn_deprecated );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warn_deprecated );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warn_deprecated" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 302;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            if ( PyCell_GET( self->m_closure[3] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "since" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_1 = PyCell_GET( self->m_closure[3] );
            tmp_args_name_1 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_dict_key_1 = const_str_plain_message;
            tmp_tuple_element_2 = const_str_digest_f13229e14be656569ff96e8edb661822;
            tmp_string_concat_values_1 = PyTuple_New( 9 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 0, tmp_tuple_element_2 );
            if ( PyCell_GET( self->m_closure[2] ) == NULL )
            {
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "old" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }

            tmp_operand_name_1 = PyCell_GET( self->m_closure[2] );
            tmp_format_value_1 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_1 );
            if ( tmp_format_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_1 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_1, tmp_format_spec_1 );
            Py_DECREF( tmp_format_value_1 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 1, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_7654d5c228106810e555b45def28becc;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 2, tmp_tuple_element_2 );
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = PyCell_GET( self->m_closure[0] );
            tmp_format_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
            if ( tmp_format_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_2 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_2, tmp_format_spec_2 );
            Py_DECREF( tmp_format_value_2 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 3, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_4dd76f99f7cda49e24f2ec6902453b96;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 4, tmp_tuple_element_2 );
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "new" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }

            tmp_operand_name_2 = PyCell_GET( self->m_closure[1] );
            tmp_format_value_3 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_2 );
            if ( tmp_format_value_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_3 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_3, tmp_format_spec_3 );
            Py_DECREF( tmp_format_value_3 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 5, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_91a49c69f4285f5d67501ac0224f36d6;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 6, tmp_tuple_element_2 );
            if ( PyCell_GET( self->m_closure[3] ) == NULL )
            {
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "since" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }

            tmp_format_value_4 = PyCell_GET( self->m_closure[3] );
            tmp_format_spec_4 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_4, tmp_format_spec_4 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 7, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_d8486a1b232bbac034ebcb89ee222341;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 8, tmp_tuple_element_2 );
            tmp_dict_value_1 = PyUnicode_Join( const_str_empty, tmp_string_concat_values_1 );
            Py_DECREF( tmp_string_concat_values_1 );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );

                exception_lineno = 303;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_5023d2a43be9bdf9a8b72aeeb1807ffe->m_frame.f_lineno = 302;
            tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 302;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( par_kwargs );
            tmp_source_name_2 = par_kwargs;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_pop );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 306;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            if ( PyCell_GET( self->m_closure[2] ) == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "old" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 306;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = PyCell_GET( self->m_closure[2] );
            frame_5023d2a43be9bdf9a8b72aeeb1807ffe->m_frame.f_lineno = 306;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_ass_subvalue_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            if ( tmp_ass_subvalue_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 306;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_kwargs );
            tmp_ass_subscribed_1 = par_kwargs;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_ass_subvalue_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "new" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 306;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }

            tmp_ass_subscript_1 = PyCell_GET( self->m_closure[1] );
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 306;
                type_description_1 = "oocccc";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 307;
            type_description_1 = "oocccc";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_1 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 307;
            type_description_1 = "oocccc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5023d2a43be9bdf9a8b72aeeb1807ffe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5023d2a43be9bdf9a8b72aeeb1807ffe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5023d2a43be9bdf9a8b72aeeb1807ffe );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5023d2a43be9bdf9a8b72aeeb1807ffe, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5023d2a43be9bdf9a8b72aeeb1807ffe->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5023d2a43be9bdf9a8b72aeeb1807ffe, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5023d2a43be9bdf9a8b72aeeb1807ffe,
        type_description_1,
        par_args,
        par_kwargs,
        self->m_closure[2],
        self->m_closure[3],
        self->m_closure[0],
        self->m_closure[1]
    );


    // Release cached frame.
    if ( frame_5023d2a43be9bdf9a8b72aeeb1807ffe == cache_frame_5023d2a43be9bdf9a8b72aeeb1807ffe )
    {
        Py_DECREF( frame_5023d2a43be9bdf9a8b72aeeb1807ffe );
    }
    cache_frame_5023d2a43be9bdf9a8b72aeeb1807ffe = NULL;

    assertFrameObject( frame_5023d2a43be9bdf9a8b72aeeb1807ffe );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_4__rename_parameter$$$function_1_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_4__rename_parameter$$$function_1_wrapper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_5___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = const_str_digest_e35b4ca67805dd83ae05fba930bdb412;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_5___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_5___repr__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_6__delete_parameter( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_since = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_name = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_func = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *var_signature = NULL;
    PyObject *var_wrapper = NULL;
    PyObject *outline_0_var_param = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_1317ce8661d54881623c1f54531120a4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    struct Nuitka_FrameObject *frame_d5e28f735b78dd2d76d51578bad34796_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_d5e28f735b78dd2d76d51578bad34796_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_1317ce8661d54881623c1f54531120a4 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1317ce8661d54881623c1f54531120a4, codeobj_1317ce8661d54881623c1f54531120a4, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1317ce8661d54881623c1f54531120a4 = cache_frame_1317ce8661d54881623c1f54531120a4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1317ce8661d54881623c1f54531120a4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1317ce8661d54881623c1f54531120a4 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_compexpr_left_1 = PyCell_GET( par_func );
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_functools );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 347;
                type_description_1 = "cccoo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_partial );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 347;
                type_description_1 = "cccoo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__delete_parameter );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__delete_parameter );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_delete_parameter" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 347;
                type_description_1 = "cccoo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = tmp_mvar_value_2;
            CHECK_OBJECT( PyCell_GET( par_since ) );
            tmp_args_element_name_2 = PyCell_GET( par_since );
            CHECK_OBJECT( PyCell_GET( par_name ) );
            tmp_args_element_name_3 = PyCell_GET( par_name );
            frame_1317ce8661d54881623c1f54531120a4->m_frame.f_lineno = 347;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 347;
                type_description_1 = "cccoo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_inspect );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_inspect );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "inspect" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 349;
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_args_element_name_4 = PyCell_GET( par_func );
        frame_1317ce8661d54881623c1f54531120a4->m_frame.f_lineno = 349;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_signature, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 349;
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }
        assert( var_signature == NULL );
        var_signature = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( PyCell_GET( par_name ) );
        tmp_compexpr_left_2 = PyCell_GET( par_name );
        CHECK_OBJECT( var_signature );
        tmp_source_name_2 = var_signature;
        tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_parameters );
        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 350;
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 350;
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_raise_value_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_string_concat_values_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_format_value_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_format_spec_1;
            PyObject *tmp_format_value_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_format_spec_2;
            tmp_raise_type_1 = PyExc_AssertionError;
            tmp_tuple_element_2 = const_str_digest_618c96c176b977c5f504ed0a642f72f7;
            tmp_string_concat_values_1 = PyTuple_New( 5 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( PyCell_GET( par_name ) );
            tmp_operand_name_1 = PyCell_GET( par_name );
            tmp_format_value_1 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_1 );
            if ( tmp_format_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 351;
                type_description_1 = "cccoo";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_1 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_1, tmp_format_spec_1 );
            Py_DECREF( tmp_format_value_1 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 351;
                type_description_1 = "cccoo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 1, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_21397472cac24efd9c899754fc958a99;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 2, tmp_tuple_element_2 );
            CHECK_OBJECT( PyCell_GET( par_func ) );
            tmp_source_name_3 = PyCell_GET( par_func );
            tmp_format_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___name__ );
            if ( tmp_format_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 351;
                type_description_1 = "cccoo";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_2 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_2, tmp_format_spec_2 );
            Py_DECREF( tmp_format_value_2 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 351;
                type_description_1 = "cccoo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 3, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_25731c733fd74e8333aa29126ce85686;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 4, tmp_tuple_element_2 );
            tmp_tuple_element_1 = PyUnicode_Join( const_str_empty, tmp_string_concat_values_1 );
            Py_DECREF( tmp_string_concat_values_1 );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 351;
                type_description_1 = "cccoo";
                goto frame_exception_exit_1;
            }
            tmp_raise_value_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_raise_value_1, 0, tmp_tuple_element_1 );
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_value = tmp_raise_value_1;
            exception_lineno = 350;
            RAISE_EXCEPTION_WITH_VALUE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( var_signature );
        tmp_source_name_4 = var_signature;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_replace );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 353;
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_parameters;
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( var_signature );
            tmp_source_name_5 = var_signature;
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_parameters );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 356;
                type_description_1 = "cccoo";
                goto try_except_handler_2;
            }
            frame_1317ce8661d54881623c1f54531120a4->m_frame.f_lineno = 356;
            tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_values );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 356;
                type_description_1 = "cccoo";
                goto try_except_handler_2;
            }
            tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_1 = "cccoo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_d5e28f735b78dd2d76d51578bad34796_2, codeobj_d5e28f735b78dd2d76d51578bad34796, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *) );
        frame_d5e28f735b78dd2d76d51578bad34796_2 = cache_frame_d5e28f735b78dd2d76d51578bad34796_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_d5e28f735b78dd2d76d51578bad34796_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_d5e28f735b78dd2d76d51578bad34796_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oc";
                    exception_lineno = 354;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_5 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_param;
                outline_0_var_param = tmp_assign_source_5;
                Py_INCREF( outline_0_var_param );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_7;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_mvar_value_4;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            CHECK_OBJECT( outline_0_var_param );
            tmp_source_name_6 = outline_0_var_param;
            tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_name );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( PyCell_GET( par_name ) );
            tmp_compexpr_right_3 = PyCell_GET( par_name );
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_1;
            }
            else
            {
                goto condexpr_false_1;
            }
            condexpr_true_1:;
            CHECK_OBJECT( outline_0_var_param );
            tmp_source_name_7 = outline_0_var_param;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_replace );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }
            tmp_dict_key_2 = const_str_plain_default;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__deprecated_parameter );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__deprecated_parameter );
            }

            if ( tmp_mvar_value_4 == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_deprecated_parameter" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 354;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }

            tmp_dict_value_2 = tmp_mvar_value_4;
            tmp_kw_name_2 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            frame_d5e28f735b78dd2d76d51578bad34796_2->m_frame.f_lineno = 354;
            tmp_append_value_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }
            goto condexpr_end_1;
            condexpr_false_1:;
            CHECK_OBJECT( outline_0_var_param );
            tmp_append_value_1 = outline_0_var_param;
            Py_INCREF( tmp_append_value_1 );
            condexpr_end_1:;
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_2 = "oc";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 354;
            type_description_2 = "oc";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_dict_value_1 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_dict_value_1 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_6__delete_parameter );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d5e28f735b78dd2d76d51578bad34796_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_d5e28f735b78dd2d76d51578bad34796_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_d5e28f735b78dd2d76d51578bad34796_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_d5e28f735b78dd2d76d51578bad34796_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_d5e28f735b78dd2d76d51578bad34796_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_d5e28f735b78dd2d76d51578bad34796_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_d5e28f735b78dd2d76d51578bad34796_2,
            type_description_2,
            outline_0_var_param,
            par_name
        );


        // Release cached frame.
        if ( frame_d5e28f735b78dd2d76d51578bad34796_2 == cache_frame_d5e28f735b78dd2d76d51578bad34796_2 )
        {
            Py_DECREF( frame_d5e28f735b78dd2d76d51578bad34796_2 );
        }
        cache_frame_d5e28f735b78dd2d76d51578bad34796_2 = NULL;

        assertFrameObject( frame_d5e28f735b78dd2d76d51578bad34796_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "cccoo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_6__delete_parameter );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_param );
        outline_0_var_param = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_param );
        outline_0_var_param = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_6__delete_parameter );
        return NULL;
        outline_exception_1:;
        exception_lineno = 354;
        goto frame_exception_exit_1;
        outline_result_1:;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_1317ce8661d54881623c1f54531120a4->m_frame.f_lineno = 353;
        tmp_assattr_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 353;
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_assattr_target_1 = PyCell_GET( par_func );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain___signature__, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 353;
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_4;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_functools );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 358;
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_5;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_args_element_name_5 = PyCell_GET( par_func );
        frame_1317ce8661d54881623c1f54531120a4->m_frame.f_lineno = 358;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_called_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_wraps, call_args );
        }

        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_6 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_6__delete_parameter$$$function_1_wrapper(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_6)->m_closure[0] = par_func;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_6)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_6)->m_closure[1] = par_name;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_6)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_6)->m_closure[2] = par_since;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_6)->m_closure[2] );


        frame_1317ce8661d54881623c1f54531120a4->m_frame.f_lineno = 358;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;
            type_description_1 = "cccoo";
            goto frame_exception_exit_1;
        }
        assert( var_wrapper == NULL );
        var_wrapper = tmp_assign_source_6;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1317ce8661d54881623c1f54531120a4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1317ce8661d54881623c1f54531120a4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1317ce8661d54881623c1f54531120a4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1317ce8661d54881623c1f54531120a4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1317ce8661d54881623c1f54531120a4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1317ce8661d54881623c1f54531120a4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1317ce8661d54881623c1f54531120a4,
        type_description_1,
        par_since,
        par_name,
        par_func,
        var_signature,
        var_wrapper
    );


    // Release cached frame.
    if ( frame_1317ce8661d54881623c1f54531120a4 == cache_frame_1317ce8661d54881623c1f54531120a4 )
    {
        Py_DECREF( frame_1317ce8661d54881623c1f54531120a4 );
    }
    cache_frame_1317ce8661d54881623c1f54531120a4 = NULL;

    assertFrameObject( frame_1317ce8661d54881623c1f54531120a4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    CHECK_OBJECT( var_wrapper );
    tmp_return_value = var_wrapper;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_6__delete_parameter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    Py_XDECREF( var_signature );
    var_signature = NULL;

    Py_XDECREF( var_wrapper );
    var_wrapper = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    Py_XDECREF( var_signature );
    var_signature = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_6__delete_parameter );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_6__delete_parameter$$$function_1_wrapper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    PyObject *var_arguments = NULL;
    struct Nuitka_FrameObject *frame_d8a9c1581f4e5683e4559f567d64b192;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_d8a9c1581f4e5683e4559f567d64b192 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d8a9c1581f4e5683e4559f567d64b192, codeobj_d8a9c1581f4e5683e4559f567d64b192, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d8a9c1581f4e5683e4559f567d64b192 = cache_frame_d8a9c1581f4e5683e4559f567d64b192;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d8a9c1581f4e5683e4559f567d64b192 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d8a9c1581f4e5683e4559f567d64b192 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 360;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = PyCell_GET( self->m_closure[0] );
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___signature__ );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 360;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_bind );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 360;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_source_name_1 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 360;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_arguments );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 360;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }
        assert( var_arguments == NULL );
        var_arguments = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_mvar_value_1;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 363;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = PyCell_GET( self->m_closure[1] );
        CHECK_OBJECT( var_arguments );
        tmp_compexpr_right_1 = var_arguments;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 363;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_arguments );
        tmp_subscribed_name_1 = var_arguments;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 363;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }

        tmp_subscript_name_1 = PyCell_GET( self->m_closure[1] );
        tmp_compexpr_left_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 363;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__deprecated_parameter );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__deprecated_parameter );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_compexpr_left_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_deprecated_parameter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 363;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_2 = tmp_mvar_value_1;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 363;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_string_concat_values_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_format_value_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_format_spec_1;
            PyObject *tmp_format_value_2;
            PyObject *tmp_source_name_4;
            PyObject *tmp_format_spec_2;
            PyObject *tmp_format_value_3;
            PyObject *tmp_format_spec_3;
            PyObject *tmp_format_value_4;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_format_spec_4;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_warn_deprecated );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warn_deprecated );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warn_deprecated" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 364;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_2;
            if ( PyCell_GET( self->m_closure[2] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "since" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_1 = PyCell_GET( self->m_closure[2] );
            tmp_args_name_1 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_dict_key_1 = const_str_plain_message;
            tmp_tuple_element_2 = const_str_digest_f13229e14be656569ff96e8edb661822;
            tmp_string_concat_values_1 = PyTuple_New( 9 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 0, tmp_tuple_element_2 );
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }

            tmp_operand_name_1 = PyCell_GET( self->m_closure[1] );
            tmp_format_value_1 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_1 );
            if ( tmp_format_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_1 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_1, tmp_format_spec_1 );
            Py_DECREF( tmp_format_value_1 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 1, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_7654d5c228106810e555b45def28becc;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 2, tmp_tuple_element_2 );
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }

            tmp_source_name_4 = PyCell_GET( self->m_closure[0] );
            tmp_format_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
            if ( tmp_format_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_2 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_2, tmp_format_spec_2 );
            Py_DECREF( tmp_format_value_2 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 3, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_4dc23b8033bde8f5594de299e9979616;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 4, tmp_tuple_element_2 );
            if ( PyCell_GET( self->m_closure[2] ) == NULL )
            {
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "since" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }

            tmp_format_value_3 = PyCell_GET( self->m_closure[2] );
            tmp_format_spec_3 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_3, tmp_format_spec_3 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 5, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_7351bd1520ee1e45bba392f411640a43;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 6, tmp_tuple_element_2 );
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }

            tmp_operand_name_2 = PyCell_GET( self->m_closure[1] );
            tmp_format_value_4 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_2 );
            if ( tmp_format_value_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_4 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_4, tmp_format_spec_4 );
            Py_DECREF( tmp_format_value_4 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 7, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_2d03aa0e4987d339d81bf09c043a8b47;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 8, tmp_tuple_element_2 );
            tmp_dict_value_1 = PyUnicode_Join( const_str_empty, tmp_string_concat_values_1 );
            Py_DECREF( tmp_string_concat_values_1 );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );

                exception_lineno = 365;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_d8a9c1581f4e5683e4559f567d64b192->m_frame.f_lineno = 364;
            tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 364;
                type_description_1 = "oooccc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_dircall_arg1_2;
        PyObject *tmp_dircall_arg2_2;
        PyObject *tmp_dircall_arg3_2;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 369;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_2 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_2 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_2 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_2 );
        Py_INCREF( tmp_dircall_arg2_2 );
        Py_INCREF( tmp_dircall_arg3_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2, tmp_dircall_arg3_2};
            tmp_return_value = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;
            type_description_1 = "oooccc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d8a9c1581f4e5683e4559f567d64b192 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d8a9c1581f4e5683e4559f567d64b192 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d8a9c1581f4e5683e4559f567d64b192 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d8a9c1581f4e5683e4559f567d64b192, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d8a9c1581f4e5683e4559f567d64b192->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d8a9c1581f4e5683e4559f567d64b192, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d8a9c1581f4e5683e4559f567d64b192,
        type_description_1,
        par_args,
        par_kwargs,
        var_arguments,
        self->m_closure[0],
        self->m_closure[1],
        self->m_closure[2]
    );


    // Release cached frame.
    if ( frame_d8a9c1581f4e5683e4559f567d64b192 == cache_frame_d8a9c1581f4e5683e4559f567d64b192 )
    {
        Py_DECREF( frame_d8a9c1581f4e5683e4559f567d64b192 );
    }
    cache_frame_d8a9c1581f4e5683e4559f567d64b192 = NULL;

    assertFrameObject( frame_d8a9c1581f4e5683e4559f567d64b192 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_6__delete_parameter$$$function_1_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_arguments );
    Py_DECREF( var_arguments );
    var_arguments = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_arguments );
    var_arguments = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_6__delete_parameter$$$function_1_wrapper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_7__make_keyword_only( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_since = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_name = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_func = PyCell_NEW1( python_pars[ 2 ] );
    struct Nuitka_CellObject *var_signature = PyCell_EMPTY();
    PyObject *var_POK = NULL;
    PyObject *var_KWO = NULL;
    PyObject *var_names = NULL;
    PyObject *var_kwonly = NULL;
    PyObject *var_wrapper = NULL;
    PyObject *outline_0_var_name = NULL;
    PyObject *outline_1_var_param = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_2__$0 = NULL;
    PyObject *tmp_listcomp_2__contraction = NULL;
    PyObject *tmp_listcomp_2__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_f69e57f244bdf4cd7fa01d4f158653f8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    struct Nuitka_FrameObject *frame_561302bb77fe5608dcb90b4fdd324ab2_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_561302bb77fe5608dcb90b4fdd324ab2_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    struct Nuitka_FrameObject *frame_cd7b2ffbb718252ce76ae8bf45e44893_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_cd7b2ffbb718252ce76ae8bf45e44893_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_f69e57f244bdf4cd7fa01d4f158653f8 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f69e57f244bdf4cd7fa01d4f158653f8, codeobj_f69e57f244bdf4cd7fa01d4f158653f8, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f69e57f244bdf4cd7fa01d4f158653f8 = cache_frame_f69e57f244bdf4cd7fa01d4f158653f8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f69e57f244bdf4cd7fa01d4f158653f8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f69e57f244bdf4cd7fa01d4f158653f8 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_compexpr_left_1 = PyCell_GET( par_func );
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_functools );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 386;
                type_description_1 = "ccccooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_1;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_partial );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 386;
                type_description_1 = "ccccooooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__make_keyword_only );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__make_keyword_only );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_make_keyword_only" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 386;
                type_description_1 = "ccccooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = tmp_mvar_value_2;
            CHECK_OBJECT( PyCell_GET( par_since ) );
            tmp_args_element_name_2 = PyCell_GET( par_since );
            CHECK_OBJECT( PyCell_GET( par_name ) );
            tmp_args_element_name_3 = PyCell_GET( par_name );
            frame_f69e57f244bdf4cd7fa01d4f158653f8->m_frame.f_lineno = 386;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 386;
                type_description_1 = "ccccooooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_inspect );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_inspect );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "inspect" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 388;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_args_element_name_4 = PyCell_GET( par_func );
        frame_f69e57f244bdf4cd7fa01d4f158653f8->m_frame.f_lineno = 388;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_signature, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 388;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_signature ) == NULL );
        PyCell_SET( var_signature, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_inspect );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_inspect );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "inspect" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 389;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_4;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_Parameter );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 389;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_POSITIONAL_OR_KEYWORD );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 389;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        assert( var_POK == NULL );
        var_POK = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_inspect );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_inspect );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "inspect" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 390;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_5;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_Parameter );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 390;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_KEYWORD_ONLY );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 390;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        assert( var_KWO == NULL );
        var_KWO = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_source_name_7;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_8;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( PyCell_GET( par_name ) );
        tmp_compexpr_left_2 = PyCell_GET( par_name );
        CHECK_OBJECT( PyCell_GET( var_signature ) );
        tmp_source_name_6 = PyCell_GET( var_signature );
        tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_parameters );
        if ( tmp_compexpr_right_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 391;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 391;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res == 1 ) ? Py_True : Py_False;
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( PyCell_GET( var_signature ) );
        tmp_source_name_8 = PyCell_GET( var_signature );
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_parameters );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 392;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_name ) );
        tmp_subscript_name_1 = PyCell_GET( par_name );
        tmp_source_name_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_source_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 392;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_kind );
        Py_DECREF( tmp_source_name_7 );
        if ( tmp_compexpr_left_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 392;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_POK );
        tmp_compexpr_right_3 = var_POK;
        tmp_and_right_value_1 = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        Py_DECREF( tmp_compexpr_left_3 );
        if ( tmp_and_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 392;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        Py_INCREF( tmp_and_left_value_1 );
        tmp_operand_name_1 = tmp_and_left_value_1;
        and_end_1:;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 391;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_raise_value_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_string_concat_values_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_format_value_1;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_format_spec_1;
            PyObject *tmp_format_value_2;
            PyObject *tmp_source_name_9;
            PyObject *tmp_format_spec_2;
            tmp_raise_type_1 = PyExc_AssertionError;
            tmp_tuple_element_2 = const_str_digest_618c96c176b977c5f504ed0a642f72f7;
            tmp_string_concat_values_1 = PyTuple_New( 5 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( PyCell_GET( par_name ) );
            tmp_operand_name_2 = PyCell_GET( par_name );
            tmp_format_value_1 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_2 );
            if ( tmp_format_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 393;
                type_description_1 = "ccccooooo";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_1 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_1, tmp_format_spec_1 );
            Py_DECREF( tmp_format_value_1 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 393;
                type_description_1 = "ccccooooo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 1, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_e25a340f340b508bcb2c9ba7a5c6dd60;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 2, tmp_tuple_element_2 );
            CHECK_OBJECT( PyCell_GET( par_func ) );
            tmp_source_name_9 = PyCell_GET( par_func );
            tmp_format_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain___name__ );
            if ( tmp_format_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 393;
                type_description_1 = "ccccooooo";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_2 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_2, tmp_format_spec_2 );
            Py_DECREF( tmp_format_value_2 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 393;
                type_description_1 = "ccccooooo";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 3, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_25731c733fd74e8333aa29126ce85686;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 4, tmp_tuple_element_2 );
            tmp_tuple_element_1 = PyUnicode_Join( const_str_empty, tmp_string_concat_values_1 );
            Py_DECREF( tmp_string_concat_values_1 );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 393;
                type_description_1 = "ccccooooo";
                goto frame_exception_exit_1;
            }
            tmp_raise_value_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_raise_value_1, 0, tmp_tuple_element_1 );
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_value = tmp_raise_value_1;
            exception_lineno = 391;
            RAISE_EXCEPTION_WITH_VALUE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_source_name_10;
        CHECK_OBJECT( PyCell_GET( var_signature ) );
        tmp_source_name_10 = PyCell_GET( var_signature );
        tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_parameters );
        if ( tmp_tuple_element_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 395;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_dircall_arg1_1, 0, tmp_tuple_element_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_4 = impl___internal__$$$function_17__unpack_list( dir_call_args );
        }
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 395;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        assert( var_names == NULL );
        var_names = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        // Tried code:
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_start_name_1;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_stop_name_1;
            PyObject *tmp_step_name_1;
            CHECK_OBJECT( var_names );
            tmp_subscribed_name_2 = var_names;
            CHECK_OBJECT( var_names );
            tmp_called_instance_2 = var_names;
            CHECK_OBJECT( PyCell_GET( par_name ) );
            tmp_args_element_name_5 = PyCell_GET( par_name );
            frame_f69e57f244bdf4cd7fa01d4f158653f8->m_frame.f_lineno = 396;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_start_name_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_index, call_args );
            }

            if ( tmp_start_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 396;
                type_description_1 = "ccccooooo";
                goto try_except_handler_2;
            }
            tmp_stop_name_1 = Py_None;
            tmp_step_name_1 = Py_None;
            tmp_subscript_name_2 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
            Py_DECREF( tmp_start_name_1 );
            assert( !(tmp_subscript_name_2 == NULL) );
            tmp_iter_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscript_name_2 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 396;
                type_description_1 = "ccccooooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 396;
                type_description_1 = "ccccooooo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_6;
        }
        {
            PyObject *tmp_assign_source_7;
            tmp_assign_source_7 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_561302bb77fe5608dcb90b4fdd324ab2_2, codeobj_561302bb77fe5608dcb90b4fdd324ab2, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *) );
        frame_561302bb77fe5608dcb90b4fdd324ab2_2 = cache_frame_561302bb77fe5608dcb90b4fdd324ab2_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_561302bb77fe5608dcb90b4fdd324ab2_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_561302bb77fe5608dcb90b4fdd324ab2_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oco";
                    exception_lineno = 396;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_9;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_9 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_name;
                outline_0_var_name = tmp_assign_source_9;
                Py_INCREF( outline_0_var_name );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            PyObject *tmp_source_name_11;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_source_name_12;
            PyObject *tmp_subscript_name_3;
            CHECK_OBJECT( PyCell_GET( var_signature ) );
            tmp_source_name_12 = PyCell_GET( var_signature );
            tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_parameters );
            if ( tmp_subscribed_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 397;
                type_description_2 = "oco";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( outline_0_var_name );
            tmp_subscript_name_3 = outline_0_var_name;
            tmp_source_name_11 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
            Py_DECREF( tmp_subscribed_name_3 );
            if ( tmp_source_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 397;
                type_description_2 = "oco";
                goto try_except_handler_3;
            }
            tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_kind );
            Py_DECREF( tmp_source_name_11 );
            if ( tmp_compexpr_left_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 397;
                type_description_2 = "oco";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_POK );
            tmp_compexpr_right_4 = var_POK;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            Py_DECREF( tmp_compexpr_left_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 397;
                type_description_2 = "oco";
                goto try_except_handler_3;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_append_list_1 = tmp_listcomp_1__contraction;
                CHECK_OBJECT( outline_0_var_name );
                tmp_append_value_1 = outline_0_var_name;
                assert( PyList_Check( tmp_append_list_1 ) );
                tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 396;
                    type_description_2 = "oco";
                    goto try_except_handler_3;
                }
            }
            branch_no_3:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 396;
            type_description_2 = "oco";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_5 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_5 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_7__make_keyword_only );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_561302bb77fe5608dcb90b4fdd324ab2_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_561302bb77fe5608dcb90b4fdd324ab2_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_561302bb77fe5608dcb90b4fdd324ab2_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_561302bb77fe5608dcb90b4fdd324ab2_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_561302bb77fe5608dcb90b4fdd324ab2_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_561302bb77fe5608dcb90b4fdd324ab2_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_561302bb77fe5608dcb90b4fdd324ab2_2,
            type_description_2,
            outline_0_var_name,
            var_signature,
            var_POK
        );


        // Release cached frame.
        if ( frame_561302bb77fe5608dcb90b4fdd324ab2_2 == cache_frame_561302bb77fe5608dcb90b4fdd324ab2_2 )
        {
            Py_DECREF( frame_561302bb77fe5608dcb90b4fdd324ab2_2 );
        }
        cache_frame_561302bb77fe5608dcb90b4fdd324ab2_2 = NULL;

        assertFrameObject( frame_561302bb77fe5608dcb90b4fdd324ab2_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ccccooooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_7__make_keyword_only );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_name );
        outline_0_var_name = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_name );
        outline_0_var_name = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_7__make_keyword_only );
        return NULL;
        outline_exception_1:;
        exception_lineno = 396;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( var_kwonly == NULL );
        var_kwonly = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_13;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( PyCell_GET( var_signature ) );
        tmp_source_name_13 = PyCell_GET( var_signature );
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_replace );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 398;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_parameters;
        // Tried code:
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_iter_arg_2;
            PyObject *tmp_called_instance_3;
            PyObject *tmp_source_name_14;
            CHECK_OBJECT( PyCell_GET( var_signature ) );
            tmp_source_name_14 = PyCell_GET( var_signature );
            tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_parameters );
            if ( tmp_called_instance_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 402;
                type_description_1 = "ccccooooo";
                goto try_except_handler_4;
            }
            frame_f69e57f244bdf4cd7fa01d4f158653f8->m_frame.f_lineno = 402;
            tmp_iter_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_values );
            Py_DECREF( tmp_called_instance_3 );
            if ( tmp_iter_arg_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 402;
                type_description_1 = "ccccooooo";
                goto try_except_handler_4;
            }
            tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_2 );
            Py_DECREF( tmp_iter_arg_2 );
            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 399;
                type_description_1 = "ccccooooo";
                goto try_except_handler_4;
            }
            assert( tmp_listcomp_2__$0 == NULL );
            tmp_listcomp_2__$0 = tmp_assign_source_10;
        }
        {
            PyObject *tmp_assign_source_11;
            tmp_assign_source_11 = PyList_New( 0 );
            assert( tmp_listcomp_2__contraction == NULL );
            tmp_listcomp_2__contraction = tmp_assign_source_11;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_cd7b2ffbb718252ce76ae8bf45e44893_3, codeobj_cd7b2ffbb718252ce76ae8bf45e44893, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *) );
        frame_cd7b2ffbb718252ce76ae8bf45e44893_3 = cache_frame_cd7b2ffbb718252ce76ae8bf45e44893_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_cd7b2ffbb718252ce76ae8bf45e44893_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_cd7b2ffbb718252ce76ae8bf45e44893_3 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_2:;
        {
            PyObject *tmp_next_source_2;
            PyObject *tmp_assign_source_12;
            CHECK_OBJECT( tmp_listcomp_2__$0 );
            tmp_next_source_2 = tmp_listcomp_2__$0;
            tmp_assign_source_12 = ITERATOR_NEXT( tmp_next_source_2 );
            if ( tmp_assign_source_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_2;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "oo";
                    exception_lineno = 399;
                    goto try_except_handler_5;
                }
            }

            {
                PyObject *old = tmp_listcomp_2__iter_value_0;
                tmp_listcomp_2__iter_value_0 = tmp_assign_source_12;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_13;
            CHECK_OBJECT( tmp_listcomp_2__iter_value_0 );
            tmp_assign_source_13 = tmp_listcomp_2__iter_value_0;
            {
                PyObject *old = outline_1_var_param;
                outline_1_var_param = tmp_assign_source_13;
                Py_INCREF( outline_1_var_param );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_2;
            PyObject *tmp_append_value_2;
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            PyObject *tmp_source_name_15;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_16;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_source_name_17;
            PyObject *tmp_source_name_18;
            PyObject *tmp_mvar_value_6;
            CHECK_OBJECT( tmp_listcomp_2__contraction );
            tmp_append_list_2 = tmp_listcomp_2__contraction;
            CHECK_OBJECT( outline_1_var_param );
            tmp_source_name_15 = outline_1_var_param;
            tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_name );
            if ( tmp_compexpr_left_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 400;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            CHECK_OBJECT( var_kwonly );
            tmp_compexpr_right_5 = var_kwonly;
            tmp_res = PySequence_Contains( tmp_compexpr_right_5, tmp_compexpr_left_5 );
            Py_DECREF( tmp_compexpr_left_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 400;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_1;
            }
            else
            {
                goto condexpr_false_1;
            }
            condexpr_true_1:;
            CHECK_OBJECT( outline_1_var_param );
            tmp_source_name_16 = outline_1_var_param;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_replace );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 399;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            tmp_dict_key_2 = const_str_plain_kind;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_inspect );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_inspect );
            }

            if ( tmp_mvar_value_6 == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "inspect" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 399;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }

            tmp_source_name_18 = tmp_mvar_value_6;
            tmp_source_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_Parameter );
            if ( tmp_source_name_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 399;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_KEYWORD_ONLY );
            Py_DECREF( tmp_source_name_17 );
            if ( tmp_dict_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 399;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            tmp_kw_name_2 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            frame_cd7b2ffbb718252ce76ae8bf45e44893_3->m_frame.f_lineno = 399;
            tmp_append_value_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_append_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 399;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
            goto condexpr_end_1;
            condexpr_false_1:;
            CHECK_OBJECT( outline_1_var_param );
            tmp_append_value_2 = outline_1_var_param;
            Py_INCREF( tmp_append_value_2 );
            condexpr_end_1:;
            assert( PyList_Check( tmp_append_list_2 ) );
            tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
            Py_DECREF( tmp_append_value_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 399;
                type_description_2 = "oo";
                goto try_except_handler_5;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 399;
            type_description_2 = "oo";
            goto try_except_handler_5;
        }
        goto loop_start_2;
        loop_end_2:;
        CHECK_OBJECT( tmp_listcomp_2__contraction );
        tmp_dict_value_1 = tmp_listcomp_2__contraction;
        Py_INCREF( tmp_dict_value_1 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_7__make_keyword_only );
        return NULL;
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        goto frame_return_exit_3;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_3;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_cd7b2ffbb718252ce76ae8bf45e44893_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_return_exit_3:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_cd7b2ffbb718252ce76ae8bf45e44893_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_4;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_cd7b2ffbb718252ce76ae8bf45e44893_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_cd7b2ffbb718252ce76ae8bf45e44893_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_cd7b2ffbb718252ce76ae8bf45e44893_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_cd7b2ffbb718252ce76ae8bf45e44893_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_cd7b2ffbb718252ce76ae8bf45e44893_3,
            type_description_2,
            outline_1_var_param,
            var_kwonly
        );


        // Release cached frame.
        if ( frame_cd7b2ffbb718252ce76ae8bf45e44893_3 == cache_frame_cd7b2ffbb718252ce76ae8bf45e44893_3 )
        {
            Py_DECREF( frame_cd7b2ffbb718252ce76ae8bf45e44893_3 );
        }
        cache_frame_cd7b2ffbb718252ce76ae8bf45e44893_3 = NULL;

        assertFrameObject( frame_cd7b2ffbb718252ce76ae8bf45e44893_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;
        type_description_1 = "ccccooooo";
        goto try_except_handler_4;
        skip_nested_handling_2:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_7__make_keyword_only );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        Py_XDECREF( outline_1_var_param );
        outline_1_var_param = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_1_var_param );
        outline_1_var_param = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_7__make_keyword_only );
        return NULL;
        outline_exception_2:;
        exception_lineno = 399;
        goto frame_exception_exit_1;
        outline_result_2:;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_f69e57f244bdf4cd7fa01d4f158653f8->m_frame.f_lineno = 398;
        tmp_assattr_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 398;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_assattr_target_1 = PyCell_GET( par_func );
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain___signature__, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 398;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_called_name_4;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_functools );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 404;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_7;
        CHECK_OBJECT( PyCell_GET( par_func ) );
        tmp_args_element_name_6 = PyCell_GET( par_func );
        frame_f69e57f244bdf4cd7fa01d4f158653f8->m_frame.f_lineno = 404;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_called_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_wraps, call_args );
        }

        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 404;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_7 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_7__make_keyword_only$$$function_1_wrapper(  );

        ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[0] = par_func;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[1] = par_name;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[2] = var_signature;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[3] = par_since;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_7)->m_closure[3] );


        frame_f69e57f244bdf4cd7fa01d4f158653f8->m_frame.f_lineno = 404;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_assign_source_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 404;
            type_description_1 = "ccccooooo";
            goto frame_exception_exit_1;
        }
        assert( var_wrapper == NULL );
        var_wrapper = tmp_assign_source_14;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f69e57f244bdf4cd7fa01d4f158653f8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_3;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f69e57f244bdf4cd7fa01d4f158653f8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f69e57f244bdf4cd7fa01d4f158653f8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f69e57f244bdf4cd7fa01d4f158653f8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f69e57f244bdf4cd7fa01d4f158653f8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f69e57f244bdf4cd7fa01d4f158653f8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f69e57f244bdf4cd7fa01d4f158653f8,
        type_description_1,
        par_since,
        par_name,
        par_func,
        var_signature,
        var_POK,
        var_KWO,
        var_names,
        var_kwonly,
        var_wrapper
    );


    // Release cached frame.
    if ( frame_f69e57f244bdf4cd7fa01d4f158653f8 == cache_frame_f69e57f244bdf4cd7fa01d4f158653f8 )
    {
        Py_DECREF( frame_f69e57f244bdf4cd7fa01d4f158653f8 );
    }
    cache_frame_f69e57f244bdf4cd7fa01d4f158653f8 = NULL;

    assertFrameObject( frame_f69e57f244bdf4cd7fa01d4f158653f8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_3:;
    CHECK_OBJECT( var_wrapper );
    tmp_return_value = var_wrapper;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_7__make_keyword_only );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    CHECK_OBJECT( (PyObject *)var_signature );
    Py_DECREF( var_signature );
    var_signature = NULL;

    Py_XDECREF( var_POK );
    var_POK = NULL;

    Py_XDECREF( var_KWO );
    var_KWO = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_kwonly );
    var_kwonly = NULL;

    Py_XDECREF( var_wrapper );
    var_wrapper = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_since );
    Py_DECREF( par_since );
    par_since = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_func );
    Py_DECREF( par_func );
    par_func = NULL;

    CHECK_OBJECT( (PyObject *)var_signature );
    Py_DECREF( var_signature );
    var_signature = NULL;

    Py_XDECREF( var_POK );
    var_POK = NULL;

    Py_XDECREF( var_KWO );
    var_KWO = NULL;

    Py_XDECREF( var_names );
    var_names = NULL;

    Py_XDECREF( var_kwonly );
    var_kwonly = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_7__make_keyword_only );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_7__make_keyword_only$$$function_1_wrapper( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_args = python_pars[ 0 ];
    PyObject *par_kwargs = python_pars[ 1 ];
    PyObject *var_bound = NULL;
    struct Nuitka_FrameObject *frame_b58bb223bc3f0f04560651ca8575ec15;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_b58bb223bc3f0f04560651ca8575ec15 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b58bb223bc3f0f04560651ca8575ec15, codeobj_b58bb223bc3f0f04560651ca8575ec15, module_matplotlib$cbook$deprecation, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_b58bb223bc3f0f04560651ca8575ec15 = cache_frame_b58bb223bc3f0f04560651ca8575ec15;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b58bb223bc3f0f04560651ca8575ec15 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b58bb223bc3f0f04560651ca8575ec15 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "signature" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 406;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[2] );
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_bind );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 406;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_1 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 406;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }
        assert( var_bound == NULL );
        var_bound = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 407;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = PyCell_GET( self->m_closure[1] );
        CHECK_OBJECT( var_bound );
        tmp_source_name_2 = var_bound;
        tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arguments );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 407;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 407;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 407;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_2 = PyCell_GET( self->m_closure[1] );
        CHECK_OBJECT( par_kwargs );
        tmp_compexpr_right_2 = par_kwargs;
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 407;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            PyObject *tmp_string_concat_values_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_format_value_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_format_spec_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_warn_deprecated );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warn_deprecated );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warn_deprecated" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 408;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            if ( PyCell_GET( self->m_closure[3] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "since" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 409;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_1 = PyCell_GET( self->m_closure[3] );
            tmp_args_name_1 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_dict_key_1 = const_str_plain_message;
            tmp_dict_value_1 = const_str_digest_c324388e8872f2163c30f519b2d58f8a;
            tmp_kw_name_1 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_2 = const_str_plain_name;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 412;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }

            tmp_dict_value_2 = PyCell_GET( self->m_closure[1] );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_3 = const_str_plain_obj_type;
            tmp_tuple_element_2 = const_str_digest_bcd6e8071a1b0f259058c810a8077cf3;
            tmp_string_concat_values_1 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 0, tmp_tuple_element_2 );
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 412;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }

            tmp_source_name_3 = PyCell_GET( self->m_closure[0] );
            tmp_format_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___name__ );
            if ( tmp_format_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 412;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }
            tmp_format_spec_1 = const_str_empty;
            tmp_tuple_element_2 = BUILTIN_FORMAT( tmp_format_value_1, tmp_format_spec_1 );
            Py_DECREF( tmp_format_value_1 );
            if ( tmp_tuple_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                Py_DECREF( tmp_string_concat_values_1 );

                exception_lineno = 412;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 1, tmp_tuple_element_2 );
            tmp_tuple_element_2 = const_str_digest_25731c733fd74e8333aa29126ce85686;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_string_concat_values_1, 2, tmp_tuple_element_2 );
            tmp_dict_value_3 = PyUnicode_Join( const_str_empty, tmp_string_concat_values_1 );
            Py_DECREF( tmp_string_concat_values_1 );
            if ( tmp_dict_value_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );

                exception_lineno = 412;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
            Py_DECREF( tmp_dict_value_3 );
            assert( !(tmp_res != 0) );
            frame_b58bb223bc3f0f04560651ca8575ec15->m_frame.f_lineno = 408;
            tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 408;
                type_description_1 = "ooocccc";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_dircall_arg1_2;
        PyObject *tmp_dircall_arg2_2;
        PyObject *tmp_dircall_arg3_2;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "func" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 413;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_2 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_2 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_2 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_2 );
        Py_INCREF( tmp_dircall_arg2_2 );
        Py_INCREF( tmp_dircall_arg3_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2, tmp_dircall_arg3_2};
            tmp_return_value = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 413;
            type_description_1 = "ooocccc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b58bb223bc3f0f04560651ca8575ec15 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b58bb223bc3f0f04560651ca8575ec15 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b58bb223bc3f0f04560651ca8575ec15 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b58bb223bc3f0f04560651ca8575ec15, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b58bb223bc3f0f04560651ca8575ec15->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b58bb223bc3f0f04560651ca8575ec15, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b58bb223bc3f0f04560651ca8575ec15,
        type_description_1,
        par_args,
        par_kwargs,
        var_bound,
        self->m_closure[2],
        self->m_closure[1],
        self->m_closure[3],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_b58bb223bc3f0f04560651ca8575ec15 == cache_frame_b58bb223bc3f0f04560651ca8575ec15 )
    {
        Py_DECREF( frame_b58bb223bc3f0f04560651ca8575ec15 );
    }
    cache_frame_b58bb223bc3f0f04560651ca8575ec15 = NULL;

    assertFrameObject( frame_b58bb223bc3f0f04560651ca8575ec15 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_7__make_keyword_only$$$function_1_wrapper );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_bound );
    Py_DECREF( var_bound );
    var_bound = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_bound );
    var_bound = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_7__make_keyword_only$$$function_1_wrapper );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning$$$genobj_1__suppress_matplotlib_deprecation_warning_maker();



    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning$$$genobj_1__suppress_matplotlib_deprecation_warning_locals {
    PyObject *tmp_with_1__enter;
    PyObject *tmp_with_1__exit;
    nuitka_bool tmp_with_1__indicator;
    PyObject *tmp_with_1__source;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning$$$genobj_1__suppress_matplotlib_deprecation_warning_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning$$$genobj_1__suppress_matplotlib_deprecation_warning_locals *generator_heap = (struct matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning$$$genobj_1__suppress_matplotlib_deprecation_warning_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->tmp_with_1__enter = NULL;
    generator_heap->tmp_with_1__exit = NULL;
    generator_heap->tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    generator_heap->tmp_with_1__source = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_5178978a9b5ece2f6d3ebd0363c6209a, module_matplotlib$cbook$deprecation, 0 );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_warnings );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warnings );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warnings" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 420;

            goto try_except_handler_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        generator->m_frame->m_frame.f_lineno = 420;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_catch_warnings );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 420;

            goto try_except_handler_1;
        }
        assert( generator_heap->tmp_with_1__source == NULL );
        generator_heap->tmp_with_1__source = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( generator_heap->tmp_with_1__source );
        tmp_source_name_1 = generator_heap->tmp_with_1__source;
        tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_1, const_str_plain___enter__ );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 420;

            goto try_except_handler_1;
        }
        generator->m_frame->m_frame.f_lineno = 420;
        tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 420;

            goto try_except_handler_1;
        }
        assert( generator_heap->tmp_with_1__enter == NULL );
        generator_heap->tmp_with_1__enter = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( generator_heap->tmp_with_1__source );
        tmp_source_name_2 = generator_heap->tmp_with_1__source;
        tmp_assign_source_3 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___exit__ );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 420;

            goto try_except_handler_1;
        }
        assert( generator_heap->tmp_with_1__exit == NULL );
        generator_heap->tmp_with_1__exit = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_assign_source_4;
        tmp_assign_source_4 = NUITKA_BOOL_TRUE;
        generator_heap->tmp_with_1__indicator = tmp_assign_source_4;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_warnings );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warnings );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warnings" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 421;

            goto try_except_handler_3;
        }

        tmp_source_name_3 = tmp_mvar_value_2;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_simplefilter );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 421;

            goto try_except_handler_3;
        }
        tmp_args_element_name_1 = const_str_plain_ignore;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_MatplotlibDeprecationWarning );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MatplotlibDeprecationWarning );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MatplotlibDeprecationWarning" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 421;

            goto try_except_handler_3;
        }

        tmp_args_element_name_2 = tmp_mvar_value_3;
        generator->m_frame->m_frame.f_lineno = 421;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 421;

            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_expression_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_expression_name_1 = Py_None;
        Py_INCREF( tmp_expression_name_1 );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 422;

            goto try_except_handler_3;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Preserve existing published exception.
    generator_heap->exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_type_1 );
    generator_heap->exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_value_1 );
    generator_heap->exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( generator_heap->exception_preserved_tb_1 );

    if ( generator_heap->exception_keeper_tb_1 == NULL )
    {
        generator_heap->exception_keeper_tb_1 = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }
    else if ( generator_heap->exception_keeper_lineno_1 != 0 )
    {
        generator_heap->exception_keeper_tb_1 = ADD_TRACEBACK( generator_heap->exception_keeper_tb_1, generator->m_frame, generator_heap->exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    PyException_SetTraceback( generator_heap->exception_keeper_value_1, (PyObject *)generator_heap->exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &generator_heap->exception_keeper_type_1, &generator_heap->exception_keeper_value_1, &generator_heap->exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_BaseException;
        generator_heap->tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 420;

            goto try_except_handler_4;
        }
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_assign_source_5;
            tmp_assign_source_5 = NUITKA_BOOL_FALSE;
            generator_heap->tmp_with_1__indicator = tmp_assign_source_5;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            CHECK_OBJECT( generator_heap->tmp_with_1__exit );
            tmp_called_name_3 = generator_heap->tmp_with_1__exit;
            tmp_args_element_name_3 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_4 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_5 = EXC_TRACEBACK(PyThreadState_GET());
            generator->m_frame->m_frame.f_lineno = 422;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 422;

                goto try_except_handler_4;
            }
            generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 422;

                goto try_except_handler_4;
            }
            tmp_condition_result_2 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            if (unlikely( generator_heap->tmp_result == false ))
            {
                generator_heap->exception_lineno = 422;
            }

            if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;

            goto try_except_handler_4;
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        generator_heap->tmp_result = RERAISE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
        if (unlikely( generator_heap->tmp_result == false ))
        {
            generator_heap->exception_lineno = 420;
        }

        if (generator_heap->exception_tb && generator_heap->exception_tb->tb_frame == &generator->m_frame->m_frame) generator->m_frame->m_frame.f_lineno = generator_heap->exception_tb->tb_lineno;

        goto try_except_handler_4;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( generator_heap->exception_preserved_type_1, generator_heap->exception_preserved_value_1, generator_heap->exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning$$$genobj_1__suppress_matplotlib_deprecation_warning );
    return NULL;
    // End of try:
    try_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( generator_heap->tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = generator_heap->tmp_with_1__indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( generator_heap->tmp_with_1__exit );
            tmp_called_name_4 = generator_heap->tmp_with_1__exit;
            generator->m_frame->m_frame.f_lineno = 422;
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                Py_DECREF( generator_heap->exception_keeper_type_3 );
                Py_XDECREF( generator_heap->exception_keeper_value_3 );
                Py_XDECREF( generator_heap->exception_keeper_tb_3 );

                generator_heap->exception_lineno = 422;

                goto try_except_handler_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_3:;
    }
    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto try_except_handler_1;
    // End of try:
    try_end_3:;
    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( generator_heap->tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = generator_heap->tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( generator_heap->tmp_with_1__exit );
            tmp_called_name_5 = generator_heap->tmp_with_1__exit;
            generator->m_frame->m_frame.f_lineno = 422;
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 422;

                goto try_except_handler_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_4:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_with_1__source );
    generator_heap->tmp_with_1__source = NULL;

    Py_XDECREF( generator_heap->tmp_with_1__enter );
    generator_heap->tmp_with_1__enter = NULL;

    Py_XDECREF( generator_heap->tmp_with_1__exit );
    generator_heap->tmp_with_1__exit = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_1__source );
    Py_DECREF( generator_heap->tmp_with_1__source );
    generator_heap->tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_1__enter );
    Py_DECREF( generator_heap->tmp_with_1__enter );
    generator_heap->tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_with_1__exit );
    Py_DECREF( generator_heap->tmp_with_1__exit );
    generator_heap->tmp_with_1__exit = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning$$$genobj_1__suppress_matplotlib_deprecation_warning_maker( void )
{
    return Nuitka_Generator_New(
        matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning$$$genobj_1__suppress_matplotlib_deprecation_warning_context,
        module_matplotlib$cbook$deprecation,
        const_str_plain__suppress_matplotlib_deprecation_warning,
#if PYTHON_VERSION >= 350
        NULL,
#endif
        codeobj_5178978a9b5ece2f6d3ebd0363c6209a,
        0,
        sizeof(struct matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning$$$genobj_1__suppress_matplotlib_deprecation_warning_locals)
    );
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_1__generate_deprecation_warning( PyObject *defaults, PyObject *kw_defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_1__generate_deprecation_warning,
        const_str_plain__generate_deprecation_warning,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c8d2e9bec54f9a47225eb0f6eca366a7,
        defaults,
#if PYTHON_VERSION >= 300
        kw_defaults,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_2_warn_deprecated( PyObject *kw_defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_2_warn_deprecated,
        const_str_plain_warn_deprecated,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e9d6ffae23703aacdce7f642eb17fba8,
        NULL,
#if PYTHON_VERSION >= 300
        kw_defaults,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        const_str_digest_b7663a8fc994f1116640c4cbdb8b5d0b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated( PyObject *kw_defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_3_deprecated,
        const_str_plain_deprecated,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6be97f9686a674b9178476de4ba08905,
        NULL,
#if PYTHON_VERSION >= 300
        kw_defaults,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        const_str_digest_564e3b63ea9764ae143ea40ab98b97f0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate,
        const_str_plain_deprecate,
#if PYTHON_VERSION >= 300
        const_str_digest_baa534a2c591faaa7f7304271d345845,
#endif
        codeobj_7363cb49922ef5792c2d6b06382529ea,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_1_finalize(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_1_finalize,
        const_str_plain_finalize,
#if PYTHON_VERSION >= 300
        const_str_digest_7e58ec12901653a99ccbe6c630247a0f,
#endif
        codeobj_8fa74b6215bc1eaee564d2cb5e6e5d20,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_2___get__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_2___get__,
        const_str_plain___get__,
#if PYTHON_VERSION >= 300
        const_str_digest_aef16e9fac7ee50acd7ddda112bc6970,
#endif
        codeobj_08c2622f35a3431ed3ee5a285ea3d111,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_3___set__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_3___set__,
        const_str_plain___set__,
#if PYTHON_VERSION >= 300
        const_str_digest_40b735750604d760d22884177c72c9b4,
#endif
        codeobj_112af24ecb9a463e995654111c3f152a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_4___delete__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_4___delete__,
        const_str_plain___delete__,
#if PYTHON_VERSION >= 300
        const_str_digest_c152cc6b1c2a07ae832a9da408c82333,
#endif
        codeobj_6bfb613eacf0e6932019da285c419273,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_5_finalize(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_5_finalize,
        const_str_plain_finalize,
#if PYTHON_VERSION >= 300
        const_str_digest_7e58ec12901653a99ccbe6c630247a0f,
#endif
        codeobj_f5dbb84adc05a8b8017b07af877767e0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_6_finalize(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_6_finalize,
        const_str_plain_finalize,
#if PYTHON_VERSION >= 300
        const_str_digest_7e58ec12901653a99ccbe6c630247a0f,
#endif
        codeobj_eb66ce6a82024475054afab867381783,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_7_wrapper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_3_deprecated$$$function_1_deprecate$$$function_7_wrapper,
        const_str_plain_wrapper,
#if PYTHON_VERSION >= 300
        const_str_digest_9a45f3fe2ce4bccb7e2deeab00592029,
#endif
        codeobj_f861e5fed7f650f3780bd3601addbbda,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_4__rename_parameter( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_4__rename_parameter,
        const_str_plain__rename_parameter,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_27e5cf7205556ea8e8b237cc9fb5b318,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        const_str_digest_668fe558c901f4408742633d58c2006e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_4__rename_parameter$$$function_1_wrapper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_4__rename_parameter$$$function_1_wrapper,
        const_str_plain_wrapper,
#if PYTHON_VERSION >= 300
        const_str_digest_1ed0834fb44639b6c86bc76dc2a7d4b6,
#endif
        codeobj_5023d2a43be9bdf9a8b72aeeb1807ffe,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        4
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_5___repr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_5___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_0e3811383b231b7c65c4082d4334c412,
#endif
        codeobj_9b0223749077f0347c73c340756f3f94,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_6__delete_parameter( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_6__delete_parameter,
        const_str_plain__delete_parameter,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1317ce8661d54881623c1f54531120a4,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        const_str_digest_f76bbb1cda8fc672dad73d81f6efb3b6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_6__delete_parameter$$$function_1_wrapper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_6__delete_parameter$$$function_1_wrapper,
        const_str_plain_wrapper,
#if PYTHON_VERSION >= 300
        const_str_digest_b5569535a1e20d95bba28c4f9579bc66,
#endif
        codeobj_d8a9c1581f4e5683e4559f567d64b192,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        3
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_7__make_keyword_only( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_7__make_keyword_only,
        const_str_plain__make_keyword_only,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f69e57f244bdf4cd7fa01d4f158653f8,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        const_str_digest_fdbb208f62d71b18577f0b1117f34570,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_7__make_keyword_only$$$function_1_wrapper(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_7__make_keyword_only$$$function_1_wrapper,
        const_str_plain_wrapper,
#if PYTHON_VERSION >= 300
        const_str_digest_6c04cf4e6fd1ae25a30c888eaeb5f4cc,
#endif
        codeobj_b58bb223bc3f0f04560651ca8575ec15,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        4
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning,
        const_str_plain__suppress_matplotlib_deprecation_warning,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5178978a9b5ece2f6d3ebd0363c6209a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_matplotlib$cbook$deprecation,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_matplotlib$cbook$deprecation =
{
    PyModuleDef_HEAD_INIT,
    "matplotlib.cbook.deprecation",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(matplotlib$cbook$deprecation)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(matplotlib$cbook$deprecation)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_matplotlib$cbook$deprecation );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("matplotlib.cbook.deprecation: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.cbook.deprecation: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("matplotlib.cbook.deprecation: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initmatplotlib$cbook$deprecation" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_matplotlib$cbook$deprecation = Py_InitModule4(
        "matplotlib.cbook.deprecation",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_matplotlib$cbook$deprecation = PyModule_Create( &mdef_matplotlib$cbook$deprecation );
#endif

    moduledict_matplotlib$cbook$deprecation = MODULE_DICT( module_matplotlib$cbook$deprecation );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_matplotlib$cbook$deprecation,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_matplotlib$cbook$deprecation,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$cbook$deprecation,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_matplotlib$cbook$deprecation,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_matplotlib$cbook$deprecation );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_72fd6686a1bcf90a38f58fc375cd915c, module_matplotlib$cbook$deprecation );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    struct Nuitka_FrameObject *frame_fc791ac736d18b32f8b916e8551290fc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_matplotlib$cbook$deprecation_7 = NULL;
    PyObject *tmp_dictset_value;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *locals_matplotlib$cbook$deprecation_317 = NULL;
    struct Nuitka_FrameObject *frame_970c410aa615c58e7f1b7a3220cbc778_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_970c410aa615c58e7f1b7a3220cbc778_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_fc791ac736d18b32f8b916e8551290fc = MAKE_MODULE_FRAME( codeobj_fc791ac736d18b32f8b916e8551290fc, module_matplotlib$cbook$deprecation );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_fc791ac736d18b32f8b916e8551290fc );
    assert( Py_REFCNT( frame_fc791ac736d18b32f8b916e8551290fc ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_contextlib;
        tmp_globals_name_1 = (PyObject *)moduledict_matplotlib$cbook$deprecation;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_fc791ac736d18b32f8b916e8551290fc->m_frame.f_lineno = 1;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_contextlib, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_functools;
        tmp_globals_name_2 = (PyObject *)moduledict_matplotlib$cbook$deprecation;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_fc791ac736d18b32f8b916e8551290fc->m_frame.f_lineno = 2;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_functools, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_inspect;
        tmp_globals_name_3 = (PyObject *)moduledict_matplotlib$cbook$deprecation;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_fc791ac736d18b32f8b916e8551290fc->m_frame.f_lineno = 3;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_inspect, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_warnings;
        tmp_globals_name_4 = (PyObject *)moduledict_matplotlib$cbook$deprecation;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_fc791ac736d18b32f8b916e8551290fc->m_frame.f_lineno = 4;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_warnings, tmp_assign_source_7 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_UserWarning );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UserWarning );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UserWarning" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 7;

            goto try_except_handler_1;
        }

        tmp_tuple_element_1 = tmp_mvar_value_3;
        tmp_assign_source_8 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_8, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_9 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_1;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_1;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_11 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_1;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_11;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_12;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 7;

                goto try_except_handler_1;
            }
            tmp_tuple_element_2 = const_str_plain_MatplotlibDeprecationWarning;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_fc791ac736d18b32f8b916e8551290fc->m_frame.f_lineno = 7;
            tmp_assign_source_12 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 7;

                goto try_except_handler_1;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_12;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 7;

                goto try_except_handler_1;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 7;

                    goto try_except_handler_1;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 7;

                    goto try_except_handler_1;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 7;

                    goto try_except_handler_1;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 7;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_1;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_13;
            tmp_assign_source_13 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_13;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_14;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_matplotlib$cbook$deprecation_7 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_72fd6686a1bcf90a38f58fc375cd915c;
        tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation_7, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_3;
        }
        tmp_dictset_value = const_str_digest_64b3bdb8b98db31bb95bfc994b2747d0;
        tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation_7, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_3;
        }
        tmp_dictset_value = const_str_plain_MatplotlibDeprecationWarning;
        tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation_7, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_3;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 7;

                goto try_except_handler_3;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation_7, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 7;

                goto try_except_handler_3;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_MatplotlibDeprecationWarning;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_matplotlib$cbook$deprecation_7;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_fc791ac736d18b32f8b916e8551290fc->m_frame.f_lineno = 7;
            tmp_assign_source_15 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 7;

                goto try_except_handler_3;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_15;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_14 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_14 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        Py_DECREF( locals_matplotlib$cbook$deprecation_7 );
        locals_matplotlib$cbook$deprecation_7 = NULL;
        goto try_return_handler_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$cbook$deprecation_7 );
        locals_matplotlib$cbook$deprecation_7 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 7;
        goto try_except_handler_1;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_MatplotlibDeprecationWarning, tmp_assign_source_14 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_MatplotlibDeprecationWarning );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MatplotlibDeprecationWarning );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MatplotlibDeprecationWarning" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 20;

            goto frame_exception_exit_1;
        }

        tmp_assign_source_16 = tmp_mvar_value_4;
        UPDATE_STRING_DICT0( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_mplDeprecation, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_defaults_1;
        PyObject *tmp_kw_defaults_1;
        tmp_defaults_1 = const_tuple_44f3e9a432120f85e825f4f40b4a8f27_tuple;
        tmp_kw_defaults_1 = PyDict_Copy( const_dict_8e618b9c8c01085ba9af4b4138d93a54 );
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_17 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_1__generate_deprecation_warning( tmp_defaults_1, tmp_kw_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__generate_deprecation_warning, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_kw_defaults_2;
        tmp_kw_defaults_2 = PyDict_Copy( const_dict_c3a87ba873c63ff91ba7b1a628042fcb );
        tmp_assign_source_18 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_2_warn_deprecated( tmp_kw_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_warn_deprecated, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_kw_defaults_3;
        tmp_kw_defaults_3 = PyDict_Copy( const_dict_288ceb0c92afcb06d9d92fe674369750 );
        tmp_assign_source_19 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_3_deprecated( tmp_kw_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_deprecated, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_20 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_4__rename_parameter( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__rename_parameter, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_21;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;

            goto try_except_handler_4;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;

            goto try_except_handler_4;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_3:;
        tmp_bases_name_2 = const_tuple_empty;
        tmp_assign_source_22 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_22;
    }
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;

            goto try_except_handler_4;
        }
        tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;

            goto try_except_handler_4;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;

                goto try_except_handler_4;
            }
            tmp_args_name_3 = const_tuple_str_plain__deprecated_parameter_class_tuple_empty_tuple;
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_fc791ac736d18b32f8b916e8551290fc->m_frame.f_lineno = 317;
            tmp_assign_source_23 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_3 );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_23;
        }
        {
            nuitka_bool tmp_condition_result_10;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;

                goto try_except_handler_4;
            }
            tmp_condition_result_10 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_5;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_3;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_5 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 317;

                    goto try_except_handler_4;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_5 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_3 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_3 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 317;

                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_5 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 317;

                    goto try_except_handler_4;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 317;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_4;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_24;
            tmp_assign_source_24 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_24;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_25;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_matplotlib$cbook$deprecation_317 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_72fd6686a1bcf90a38f58fc375cd915c;
        tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation_317, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_plain__deprecated_parameter_class;
        tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation_317, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;

            goto try_except_handler_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_970c410aa615c58e7f1b7a3220cbc778_2, codeobj_970c410aa615c58e7f1b7a3220cbc778, module_matplotlib$cbook$deprecation, sizeof(void *) );
        frame_970c410aa615c58e7f1b7a3220cbc778_2 = cache_frame_970c410aa615c58e7f1b7a3220cbc778_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_970c410aa615c58e7f1b7a3220cbc778_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_970c410aa615c58e7f1b7a3220cbc778_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_5___repr__(  );



        tmp_res = PyObject_SetItem( locals_matplotlib$cbook$deprecation_317, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 318;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_970c410aa615c58e7f1b7a3220cbc778_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_970c410aa615c58e7f1b7a3220cbc778_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_970c410aa615c58e7f1b7a3220cbc778_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_970c410aa615c58e7f1b7a3220cbc778_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_970c410aa615c58e7f1b7a3220cbc778_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_970c410aa615c58e7f1b7a3220cbc778_2,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_970c410aa615c58e7f1b7a3220cbc778_2 == cache_frame_970c410aa615c58e7f1b7a3220cbc778_2 )
        {
            Py_DECREF( frame_970c410aa615c58e7f1b7a3220cbc778_2 );
        }
        cache_frame_970c410aa615c58e7f1b7a3220cbc778_2 = NULL;

        assertFrameObject( frame_970c410aa615c58e7f1b7a3220cbc778_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_6;
        skip_nested_handling_1:;
        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_4 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_6 = const_str_plain__deprecated_parameter_class;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_6 );
            tmp_tuple_element_6 = const_tuple_empty;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_6 );
            tmp_tuple_element_6 = locals_matplotlib$cbook$deprecation_317;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_fc791ac736d18b32f8b916e8551290fc->m_frame.f_lineno = 317;
            tmp_assign_source_26 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 317;

                goto try_except_handler_6;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_26;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_25 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_25 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        Py_DECREF( locals_matplotlib$cbook$deprecation_317 );
        locals_matplotlib$cbook$deprecation_317 = NULL;
        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_matplotlib$cbook$deprecation_317 );
        locals_matplotlib$cbook$deprecation_317 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( matplotlib$cbook$deprecation );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 317;
        goto try_except_handler_4;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__deprecated_parameter_class, tmp_assign_source_25 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__deprecated_parameter_class );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__deprecated_parameter_class );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_deprecated_parameter_class" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 322;

            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_5;
        frame_fc791ac736d18b32f8b916e8551290fc->m_frame.f_lineno = 322;
        tmp_assign_source_27 = CALL_FUNCTION_NO_ARGS( tmp_called_name_5 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 322;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__deprecated_parameter, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_defaults_3;
        tmp_defaults_3 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_3 );
        tmp_assign_source_28 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_6__delete_parameter( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__delete_parameter, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_defaults_4;
        tmp_defaults_4 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_4 );
        tmp_assign_source_29 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_7__make_keyword_only( tmp_defaults_4 );



        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__make_keyword_only, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain_contextlib );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_contextlib );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "contextlib" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 418;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_6;
        tmp_args_element_name_1 = MAKE_FUNCTION_matplotlib$cbook$deprecation$$$function_8__suppress_matplotlib_deprecation_warning(  );



        frame_fc791ac736d18b32f8b916e8551290fc->m_frame.f_lineno = 418;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_30 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_contextmanager, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 418;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_matplotlib$cbook$deprecation, (Nuitka_StringObject *)const_str_plain__suppress_matplotlib_deprecation_warning, tmp_assign_source_30 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_fc791ac736d18b32f8b916e8551290fc );
#endif
    popFrameStack();

    assertFrameObject( frame_fc791ac736d18b32f8b916e8551290fc );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_fc791ac736d18b32f8b916e8551290fc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_fc791ac736d18b32f8b916e8551290fc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_fc791ac736d18b32f8b916e8551290fc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_fc791ac736d18b32f8b916e8551290fc, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_matplotlib$cbook$deprecation );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
