/* Generated code for Python module 'prompt_toolkit.formatted_text.ansi'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$formatted_text$ansi" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$formatted_text$ansi;
PyDictObject *moduledict_prompt_toolkit$formatted_text$ansi;

/* The declarations of module constants used, if any. */
extern PyObject *const_tuple_str_plain_self_str_plain_result_tuple;
extern PyObject *const_tuple_str_plain_FormattedText_tuple;
extern PyObject *const_str_plain_result;
static PyObject *const_str_digest_545a955e2a188ce6b5985eb1e36021e9;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_int_pos_24;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_colors;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
static PyObject *const_tuple_95483d9f7c705b7b89e737f24788ae44_tuple;
extern PyObject *const_int_pos_22;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain_formatted_text;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_color_str;
static PyObject *const_str_plain__bgcolor;
extern PyObject *const_str_plain_args;
static PyObject *const_str_plain__underline;
extern PyObject *const_tuple_str_plain_self_str_plain_args_str_plain_kwargs_tuple;
extern PyObject *const_str_plain__color;
extern PyObject *const_str_plain_a;
static PyObject *const_str_digest_7c1cb00676fa8ada43b7efa321b12d2b;
extern PyObject *const_str_plain_items;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain___pt_formatted_text__;
extern PyObject *const_str_plain_bold;
extern PyObject *const_str_plain_m;
extern PyObject *const_str_plain__hidden;
extern PyObject *const_int_pos_23;
static PyObject *const_tuple_str_chr_8_str_chr_63_tuple;
static PyObject *const_int_pos_9999;
extern PyObject *const_int_pos_6;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_FG_ANSI_COLORS;
extern PyObject *const_int_pos_25;
extern PyObject *const_int_pos_27;
static PyObject *const_str_plain__formatted_text;
static PyObject *const_str_digest_111b86be98ea03850a57e8268db68e1b;
extern PyObject *const_int_pos_5;
static PyObject *const_str_digest_2b36fb2749562fe8e8384bf4000a08a0;
static PyObject *const_str_plain__fg_colors;
extern PyObject *const_str_plain_join;
static PyObject *const_str_digest_860417bc1abf49c4ae304a86e872c7a9;
extern PyObject *const_str_chr_63;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_reverse;
extern PyObject *const_str_digest_3a832cd0dd716d9abfb3e37fb4302ef8;
static PyObject *const_str_plain__bg_colors;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_attrs;
static PyObject *const_str_plain__create_style_string;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain_BG_ANSI_COLORS;
static PyObject *const_str_digest_ba75d78e2eadb3e1308752ec44cdc66c;
extern PyObject *const_str_plain_ANSI;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_hidden;
extern PyObject *const_str_plain_n;
extern PyObject *const_str_plain_isdigit;
extern PyObject *const_int_pos_38;
extern PyObject *const_str_plain_min;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
static PyObject *const_tuple_str_plain_FG_ANSI_COLORS_str_plain_BG_ANSI_COLORS_tuple;
extern PyObject *const_str_plain_g;
extern PyObject *const_str_plain_value;
static PyObject *const_str_digest_067808a912a6c2aceef20fbd6ef0cd4c;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_style;
static PyObject *const_str_digest_8efe3d681c1fb16cef671b890a778cf7;
extern PyObject *const_str_chr_91;
extern PyObject *const_int_pos_8;
extern PyObject *const_str_chr_8;
static PyObject *const_tuple_str_plain_bold_tuple;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_6fe3509a405405eba42928ed2fd3f08d;
extern PyObject *const_str_plain_enumerate;
extern PyObject *const_str_space;
extern PyObject *const_str_chr_1;
extern PyObject *const_str_plain_append;
extern PyObject *const_int_pos_48;
static PyObject *const_tuple_str_plain_blink_tuple;
extern PyObject *const_str_plain_r;
static PyObject *const_tuple_str_plain__256_colors_tuple;
extern PyObject *const_str_plain_send;
extern PyObject *const_str_chr_2;
extern PyObject *const_str_plain_attr;
static PyObject *const_tuple_str_plain_hidden_tuple;
extern PyObject *const_str_plain_parser;
static PyObject *const_str_digest_5635386a497322015b85321566a95aa4;
static PyObject *const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple;
extern PyObject *const_str_plain_b;
extern PyObject *const_tuple_str_chr_27_str_chr_63_tuple;
extern PyObject *const_str_plain_underline;
extern PyObject *const_str_plain_False;
static PyObject *const_tuple_int_pos_38_int_pos_48_tuple;
extern PyObject *const_str_plain_k;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain___all__;
extern PyObject *const_str_plain_char;
static PyObject *const_tuple_str_plain_reverse_tuple;
extern PyObject *const_str_chr_27;
extern PyObject *const_str_plain_pop;
extern PyObject *const_int_0;
static PyObject *const_list_str_plain_ANSI_str_plain_ansi_escape_list;
extern PyObject *const_str_plain__256_colors;
static PyObject *const_str_plain__bold;
extern PyObject *const_str_plain_text;
extern PyObject *const_str_digest_741617e4cb060c7f988d39877ef870e3;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_current;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_tuple_str_plain_text_tuple;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_c;
static PyObject *const_str_plain_ansi_escape;
extern PyObject *const_str_plain_base;
static PyObject *const_tuple_str_plain_self_str_plain_value_str_plain_parser_str_plain_c_tuple;
extern PyObject *const_str_digest_301caf0732d954907c54da665f688542;
extern PyObject *const_int_pos_4;
static PyObject *const_str_digest_24f55768503de3087ed5e5f9644bfeaa;
static PyObject *const_str_digest_0ab5784fd7b2a64c58528a23715c7240;
extern PyObject *const_tuple_b0c00933e83a0151953f3a00c2178204_tuple;
extern PyObject *const_str_plain_italic;
static PyObject *const_str_digest_5e14602528d2a10b56554f4188c1bcb1;
extern PyObject *const_str_plain_type;
static PyObject *const_str_digest_961deea024e5668b00b2f477056197e9;
static PyObject *const_str_plain__select_graphic_rendition;
extern PyObject *const_slice_none_none_int_neg_1;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_v;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_digest_eec683951d1cdd1abf685d52499274e2;
static PyObject *const_str_plain_square_bracket;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_str_plain__parse_corot;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_chr_155;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain_params;
static PyObject *const_str_digest_4b361621cb6439b9d456df21d5789e2a;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_replace;
extern PyObject *const_list_int_0_list;
static PyObject *const_str_digest_ecb4aa274594bb5f8ad40169ad0bc512;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
static PyObject *const_str_plain_csi;
static PyObject *const_str_plain__italic;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain___repr__;
extern PyObject *const_tuple_str_plain_a_tuple;
extern PyObject *const_int_pos_7;
static PyObject *const_str_plain_escaped_text;
static PyObject *const_str_plain__reverse;
extern PyObject *const_str_chr_59;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_tuple_str_plain_italic_tuple;
static PyObject *const_str_plain__256_colors_table;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_format;
static PyObject *const_tuple_str_plain_underline_tuple;
extern PyObject *const_str_plain_FormattedText;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_blink;
static PyObject *const_str_plain__blink;
static PyObject *const_str_digest_ed06864af6677bd9d5d964732f4f512a;
extern PyObject *const_str_digest_b72cceb46cd0996b2283bd0bd2da08a0;
static PyObject *const_str_digest_61df946a8f6df740fc20a2078b408440;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_545a955e2a188ce6b5985eb1e36021e9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4654731 ], 34, 0 );
    const_tuple_95483d9f7c705b7b89e737f24788ae44_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_95483d9f7c705b7b89e737f24788ae44_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_95483d9f7c705b7b89e737f24788ae44_tuple, 1, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    PyTuple_SET_ITEM( const_tuple_95483d9f7c705b7b89e737f24788ae44_tuple, 2, const_str_plain_attr ); Py_INCREF( const_str_plain_attr );
    PyTuple_SET_ITEM( const_tuple_95483d9f7c705b7b89e737f24788ae44_tuple, 3, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    PyTuple_SET_ITEM( const_tuple_95483d9f7c705b7b89e737f24788ae44_tuple, 4, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    const_str_plain_color_str = UNSTREAM_STRING_ASCII( &constant_bin[ 4654765 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_95483d9f7c705b7b89e737f24788ae44_tuple, 5, const_str_plain_color_str ); Py_INCREF( const_str_plain_color_str );
    const_str_plain__bgcolor = UNSTREAM_STRING_ASCII( &constant_bin[ 4654774 ], 8, 1 );
    const_str_plain__underline = UNSTREAM_STRING_ASCII( &constant_bin[ 1434135 ], 10, 1 );
    const_str_digest_7c1cb00676fa8ada43b7efa321b12d2b = UNSTREAM_STRING_ASCII( &constant_bin[ 4654782 ], 134, 0 );
    const_tuple_str_chr_8_str_chr_63_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_chr_8_str_chr_63_tuple, 0, const_str_chr_8 ); Py_INCREF( const_str_chr_8 );
    PyTuple_SET_ITEM( const_tuple_str_chr_8_str_chr_63_tuple, 1, const_str_chr_63 ); Py_INCREF( const_str_chr_63 );
    const_int_pos_9999 = PyLong_FromUnsignedLong( 9999ul );
    const_str_plain__formatted_text = UNSTREAM_STRING_ASCII( &constant_bin[ 4654069 ], 15, 1 );
    const_str_digest_111b86be98ea03850a57e8268db68e1b = UNSTREAM_STRING_ASCII( &constant_bin[ 4654916 ], 30, 0 );
    const_str_digest_2b36fb2749562fe8e8384bf4000a08a0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4654946 ], 66, 0 );
    const_str_plain__fg_colors = UNSTREAM_STRING_ASCII( &constant_bin[ 4655012 ], 10, 1 );
    const_str_digest_860417bc1abf49c4ae304a86e872c7a9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4655022 ], 37, 0 );
    const_str_plain__bg_colors = UNSTREAM_STRING_ASCII( &constant_bin[ 4655059 ], 10, 1 );
    const_str_plain__create_style_string = UNSTREAM_STRING_ASCII( &constant_bin[ 4655069 ], 20, 1 );
    const_str_digest_ba75d78e2eadb3e1308752ec44cdc66c = UNSTREAM_STRING_ASCII( &constant_bin[ 4655089 ], 52, 0 );
    const_tuple_str_plain_FG_ANSI_COLORS_str_plain_BG_ANSI_COLORS_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_FG_ANSI_COLORS_str_plain_BG_ANSI_COLORS_tuple, 0, const_str_plain_FG_ANSI_COLORS ); Py_INCREF( const_str_plain_FG_ANSI_COLORS );
    PyTuple_SET_ITEM( const_tuple_str_plain_FG_ANSI_COLORS_str_plain_BG_ANSI_COLORS_tuple, 1, const_str_plain_BG_ANSI_COLORS ); Py_INCREF( const_str_plain_BG_ANSI_COLORS );
    const_str_digest_067808a912a6c2aceef20fbd6ef0cd4c = UNSTREAM_STRING_ASCII( &constant_bin[ 4655141 ], 13, 0 );
    const_str_digest_8efe3d681c1fb16cef671b890a778cf7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4655154 ], 87, 0 );
    const_tuple_str_plain_bold_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_bold_tuple, 0, const_str_plain_bold ); Py_INCREF( const_str_plain_bold );
    const_str_digest_6fe3509a405405eba42928ed2fd3f08d = UNSTREAM_STRING_ASCII( &constant_bin[ 4655241 ], 466, 0 );
    const_tuple_str_plain_blink_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_blink_tuple, 0, const_str_plain_blink ); Py_INCREF( const_str_plain_blink );
    const_tuple_str_plain__256_colors_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__256_colors_tuple, 0, const_str_plain__256_colors ); Py_INCREF( const_str_plain__256_colors );
    const_tuple_str_plain_hidden_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_hidden_tuple, 0, const_str_plain_hidden ); Py_INCREF( const_str_plain_hidden );
    const_str_digest_5635386a497322015b85321566a95aa4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4655707 ], 13, 0 );
    const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 1, const_str_plain_style ); Py_INCREF( const_str_plain_style );
    PyTuple_SET_ITEM( const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 2, const_str_plain_formatted_text ); Py_INCREF( const_str_plain_formatted_text );
    const_str_plain_csi = UNSTREAM_STRING_ASCII( &constant_bin[ 109846 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 3, const_str_plain_csi ); Py_INCREF( const_str_plain_csi );
    PyTuple_SET_ITEM( const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 4, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    const_str_plain_escaped_text = UNSTREAM_STRING_ASCII( &constant_bin[ 4655720 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 5, const_str_plain_escaped_text ); Py_INCREF( const_str_plain_escaped_text );
    const_str_plain_square_bracket = UNSTREAM_STRING_ASCII( &constant_bin[ 4655732 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 6, const_str_plain_square_bracket ); Py_INCREF( const_str_plain_square_bracket );
    PyTuple_SET_ITEM( const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 7, const_str_plain_current ); Py_INCREF( const_str_plain_current );
    PyTuple_SET_ITEM( const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 8, const_str_plain_params ); Py_INCREF( const_str_plain_params );
    PyTuple_SET_ITEM( const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 9, const_str_plain_char ); Py_INCREF( const_str_plain_char );
    const_tuple_int_pos_38_int_pos_48_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_38_int_pos_48_tuple, 0, const_int_pos_38 ); Py_INCREF( const_int_pos_38 );
    PyTuple_SET_ITEM( const_tuple_int_pos_38_int_pos_48_tuple, 1, const_int_pos_48 ); Py_INCREF( const_int_pos_48 );
    const_tuple_str_plain_reverse_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_reverse_tuple, 0, const_str_plain_reverse ); Py_INCREF( const_str_plain_reverse );
    const_list_str_plain_ANSI_str_plain_ansi_escape_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_ANSI_str_plain_ansi_escape_list, 0, const_str_plain_ANSI ); Py_INCREF( const_str_plain_ANSI );
    const_str_plain_ansi_escape = UNSTREAM_STRING_ASCII( &constant_bin[ 4655746 ], 11, 1 );
    PyList_SET_ITEM( const_list_str_plain_ANSI_str_plain_ansi_escape_list, 1, const_str_plain_ansi_escape ); Py_INCREF( const_str_plain_ansi_escape );
    const_str_plain__bold = UNSTREAM_STRING_ASCII( &constant_bin[ 4655757 ], 5, 1 );
    const_tuple_str_plain_self_str_plain_value_str_plain_parser_str_plain_c_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_parser_str_plain_c_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_parser_str_plain_c_tuple, 1, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_parser_str_plain_c_tuple, 2, const_str_plain_parser ); Py_INCREF( const_str_plain_parser );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_value_str_plain_parser_str_plain_c_tuple, 3, const_str_plain_c ); Py_INCREF( const_str_plain_c );
    const_str_digest_24f55768503de3087ed5e5f9644bfeaa = UNSTREAM_STRING_ASCII( &constant_bin[ 4655762 ], 26, 0 );
    const_str_digest_0ab5784fd7b2a64c58528a23715c7240 = UNSTREAM_STRING_ASCII( &constant_bin[ 4655788 ], 8, 0 );
    const_str_digest_5e14602528d2a10b56554f4188c1bcb1 = UNSTREAM_STRING_ASCII( &constant_bin[ 4655796 ], 25, 0 );
    const_str_digest_961deea024e5668b00b2f477056197e9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4655821 ], 17, 0 );
    const_str_plain__select_graphic_rendition = UNSTREAM_STRING_ASCII( &constant_bin[ 4655838 ], 25, 1 );
    const_str_digest_eec683951d1cdd1abf685d52499274e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4655863 ], 72, 0 );
    const_str_plain__parse_corot = UNSTREAM_STRING_ASCII( &constant_bin[ 4655826 ], 12, 1 );
    const_str_chr_155 = UNSTREAM_STRING( &constant_bin[ 4655935 ], 2, 0 );
    const_str_digest_4b361621cb6439b9d456df21d5789e2a = UNSTREAM_STRING_ASCII( &constant_bin[ 4655937 ], 43, 0 );
    const_str_digest_ecb4aa274594bb5f8ad40169ad0bc512 = UNSTREAM_STRING_ASCII( &constant_bin[ 4654916 ], 11, 0 );
    const_str_plain__italic = UNSTREAM_STRING_ASCII( &constant_bin[ 4655980 ], 7, 1 );
    const_str_plain__reverse = UNSTREAM_STRING_ASCII( &constant_bin[ 543256 ], 8, 1 );
    const_str_plain__256_colors_table = UNSTREAM_STRING_ASCII( &constant_bin[ 4655987 ], 17, 1 );
    const_tuple_str_plain_underline_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_underline_tuple, 0, const_str_plain_underline ); Py_INCREF( const_str_plain_underline );
    const_str_plain__blink = UNSTREAM_STRING_ASCII( &constant_bin[ 4656004 ], 6, 1 );
    const_str_digest_ed06864af6677bd9d5d964732f4f512a = UNSTREAM_STRING_ASCII( &constant_bin[ 4656010 ], 13, 0 );
    const_str_digest_61df946a8f6df740fc20a2078b408440 = UNSTREAM_STRING_ASCII( &constant_bin[ 4656023 ], 30, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$formatted_text$ansi( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_f7b7c615d0ae33f0a18541d673392a58;
static PyCodeObject *codeobj_89394e0f8616c4174399f1d93aca4498;
static PyCodeObject *codeobj_44f6dd968e481914872bf518a0090ac9;
static PyCodeObject *codeobj_03531dc0e520b39442b12a3759f6a75f;
static PyCodeObject *codeobj_4abdfa9cab80d381f439db22986f3336;
static PyCodeObject *codeobj_215eb4c3a7e1a21826bb183cee6c7d88;
static PyCodeObject *codeobj_eaf2cc95a9fee3905cb03fac33474473;
static PyCodeObject *codeobj_9a51e847a75393bdd72aca172d443461;
static PyCodeObject *codeobj_e6b1cc48a0f8640329ed1cf351efa147;
static PyCodeObject *codeobj_0c1cd473cb44c9d02130dab558906431;
static PyCodeObject *codeobj_7a19018b97e3050b7736fb578a18b9b0;
static PyCodeObject *codeobj_9f13e85c4c17356e845f140da8d2e279;
static PyCodeObject *codeobj_e4db8ab5ea7342b85318d9371058c6aa;
static PyCodeObject *codeobj_a854c917057514c96ce50e373f50ad14;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_860417bc1abf49c4ae304a86e872c7a9 );
    codeobj_f7b7c615d0ae33f0a18541d673392a58 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 220, const_tuple_b0c00933e83a0151953f3a00c2178204_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_89394e0f8616c4174399f1d93aca4498 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 226, const_tuple_b0c00933e83a0151953f3a00c2178204_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_44f6dd968e481914872bf518a0090ac9 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 227, const_tuple_b0c00933e83a0151953f3a00c2178204_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_03531dc0e520b39442b12a3759f6a75f = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 219, const_tuple_str_plain_a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_4abdfa9cab80d381f439db22986f3336 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_4b361621cb6439b9d456df21d5789e2a, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_215eb4c3a7e1a21826bb183cee6c7d88 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ANSI, 12, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_eaf2cc95a9fee3905cb03fac33474473 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 26, const_tuple_str_plain_self_str_plain_value_str_plain_parser_str_plain_c_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9a51e847a75393bdd72aca172d443461 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___pt_formatted_text__, 210, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e6b1cc48a0f8640329ed1cf351efa147 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 207, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0c1cd473cb44c9d02130dab558906431 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__create_style_string, 183, const_tuple_str_plain_self_str_plain_result_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7a19018b97e3050b7736fb578a18b9b0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__parse_corot, 46, const_tuple_f5e5a229c14faca35178c0dbc1a36738_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9f13e85c4c17356e845f140da8d2e279 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__select_graphic_rendition, 108, const_tuple_95483d9f7c705b7b89e737f24788ae44_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e4db8ab5ea7342b85318d9371058c6aa = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ansi_escape, 236, const_tuple_str_plain_text_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a854c917057514c96ce50e373f50ad14 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_format, 213, const_tuple_str_plain_self_str_plain_args_str_plain_kwargs_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot$$$genobj_1__parse_corot_maker( void );


static PyObject *prompt_toolkit$formatted_text$ansi$$$function_7_format$$$genexpr_1_genexpr_maker( void );


static PyObject *prompt_toolkit$formatted_text$ansi$$$genexpr_1_genexpr_maker( void );


static PyObject *prompt_toolkit$formatted_text$ansi$$$genexpr_2_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_1___init__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_3__select_graphic_rendition(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_4__create_style_string(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_5___repr__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_6___pt_formatted_text__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_7_format(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_8_ansi_escape(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$formatted_text$ansi$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    PyObject *var_parser = NULL;
    PyObject *var_c = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_eaf2cc95a9fee3905cb03fac33474473;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_eaf2cc95a9fee3905cb03fac33474473 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_eaf2cc95a9fee3905cb03fac33474473, codeobj_eaf2cc95a9fee3905cb03fac33474473, module_prompt_toolkit$formatted_text$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_eaf2cc95a9fee3905cb03fac33474473 = cache_frame_eaf2cc95a9fee3905cb03fac33474473;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_eaf2cc95a9fee3905cb03fac33474473 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_eaf2cc95a9fee3905cb03fac33474473 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_value );
        tmp_assattr_name_1 = par_value;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_value, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_assattr_target_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_FormattedText );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FormattedText );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FormattedText" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 28;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_eaf2cc95a9fee3905cb03fac33474473->m_frame.f_lineno = 28;
        tmp_assattr_name_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__formatted_text, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        tmp_assattr_name_3 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__color, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        tmp_assattr_name_4 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__bgcolor, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_assattr_target_5;
        tmp_assattr_name_5 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__bold, tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_assattr_target_6;
        tmp_assattr_name_6 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_6 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain__underline, tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_assattr_target_7;
        tmp_assattr_name_7 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_7 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain__italic, tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_8;
        PyObject *tmp_assattr_target_8;
        tmp_assattr_name_8 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_8 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain__blink, tmp_assattr_name_8 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_9;
        PyObject *tmp_assattr_target_9;
        tmp_assattr_name_9 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_9 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_9, const_str_plain__reverse, tmp_assattr_name_9 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_10;
        PyObject *tmp_assattr_target_10;
        tmp_assattr_name_10 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_10 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_10, const_str_plain__hidden, tmp_assattr_name_10 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_eaf2cc95a9fee3905cb03fac33474473->m_frame.f_lineno = 41;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__parse_corot );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_parser == NULL );
        var_parser = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_parser );
        tmp_called_instance_2 = var_parser;
        frame_eaf2cc95a9fee3905cb03fac33474473->m_frame.f_lineno = 42;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_send, &PyTuple_GET_ITEM( const_tuple_none_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_value );
        tmp_iter_arg_1 = par_value;
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 43;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 43;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_c;
            var_c = tmp_assign_source_4;
            Py_INCREF( var_c );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_parser );
        tmp_called_instance_3 = var_parser;
        CHECK_OBJECT( var_c );
        tmp_args_element_name_1 = var_c;
        frame_eaf2cc95a9fee3905cb03fac33474473->m_frame.f_lineno = 44;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_send, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 43;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eaf2cc95a9fee3905cb03fac33474473 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eaf2cc95a9fee3905cb03fac33474473 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eaf2cc95a9fee3905cb03fac33474473, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eaf2cc95a9fee3905cb03fac33474473->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eaf2cc95a9fee3905cb03fac33474473, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_eaf2cc95a9fee3905cb03fac33474473,
        type_description_1,
        par_self,
        par_value,
        var_parser,
        var_c
    );


    // Release cached frame.
    if ( frame_eaf2cc95a9fee3905cb03fac33474473 == cache_frame_eaf2cc95a9fee3905cb03fac33474473 )
    {
        Py_DECREF( frame_eaf2cc95a9fee3905cb03fac33474473 );
    }
    cache_frame_eaf2cc95a9fee3905cb03fac33474473 = NULL;

    assertFrameObject( frame_eaf2cc95a9fee3905cb03fac33474473 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    CHECK_OBJECT( (PyObject *)var_parser );
    Py_DECREF( var_parser );
    var_parser = NULL;

    Py_XDECREF( var_c );
    var_c = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    Py_XDECREF( var_parser );
    var_parser = NULL;

    Py_XDECREF( var_c );
    var_c = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot$$$genobj_1__parse_corot_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_self;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot$$$genobj_1__parse_corot_locals {
    PyObject *var_style;
    PyObject *var_formatted_text;
    PyObject *var_csi;
    PyObject *var_c;
    PyObject *var_escaped_text;
    PyObject *var_square_bracket;
    PyObject *var_current;
    PyObject *var_params;
    PyObject *var_char;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
};

static PyObject *prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot$$$genobj_1__parse_corot_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot$$$genobj_1__parse_corot_locals *generator_heap = (struct prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot$$$genobj_1__parse_corot_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 5: goto yield_return_5;
    case 4: goto yield_return_4;
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_style = NULL;
    generator_heap->var_formatted_text = NULL;
    generator_heap->var_csi = NULL;
    generator_heap->var_c = NULL;
    generator_heap->var_escaped_text = NULL;
    generator_heap->var_square_bracket = NULL;
    generator_heap->var_current = NULL;
    generator_heap->var_params = NULL;
    generator_heap->var_char = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_empty;
        assert( generator_heap->var_style == NULL );
        Py_INCREF( tmp_assign_source_1 );
        generator_heap->var_style = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_7a19018b97e3050b7736fb578a18b9b0, module_prompt_toolkit$formatted_text$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 51;
            generator_heap->type_description_1 = "cooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__formatted_text );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 51;
            generator_heap->type_description_1 = "cooooooooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->var_formatted_text == NULL );
        generator_heap->var_formatted_text = tmp_assign_source_2;
    }
    loop_start_1:;
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_False;
        {
            PyObject *old = generator_heap->var_csi;
            generator_heap->var_csi = tmp_assign_source_3;
            Py_INCREF( generator_heap->var_csi );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_expression_name_1;
        tmp_expression_name_1 = Py_None;
        Py_INCREF( tmp_expression_name_1 );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 55;
            generator_heap->type_description_1 = "cooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = yield_return_value;
        {
            PyObject *old = generator_heap->var_c;
            generator_heap->var_c = tmp_assign_source_4;
            Py_INCREF( generator_heap->var_c );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( generator_heap->var_c );
        tmp_compexpr_left_1 = generator_heap->var_c;
        tmp_compexpr_right_1 = const_str_chr_1;
        generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 58;
            generator_heap->type_description_1 = "cooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_5;
            tmp_assign_source_5 = const_str_empty;
            {
                PyObject *old = generator_heap->var_escaped_text;
                generator_heap->var_escaped_text = tmp_assign_source_5;
                Py_INCREF( generator_heap->var_escaped_text );
                Py_XDECREF( old );
            }

        }
        loop_start_2:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( generator_heap->var_c );
            tmp_compexpr_left_2 = generator_heap->var_c;
            tmp_compexpr_right_2 = const_str_chr_2;
            tmp_operand_name_1 = RICH_COMPARE_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 60;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            generator_heap->tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 60;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( generator_heap->tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            goto loop_end_1;
            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_expression_name_2;
            tmp_expression_name_2 = Py_None;
            Py_INCREF( tmp_expression_name_2 );
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), NULL );
            generator->m_yield_return_index = 2;
            return tmp_expression_name_2;
            yield_return_2:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 61;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_6 = yield_return_value;
            {
                PyObject *old = generator_heap->var_c;
                assert( old != NULL );
                generator_heap->var_c = tmp_assign_source_6;
                Py_INCREF( generator_heap->var_c );
                Py_DECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( generator_heap->var_c );
            tmp_compexpr_left_3 = generator_heap->var_c;
            tmp_compexpr_right_3 = const_str_chr_2;
            generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 62;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_2;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_tuple_element_1;
                CHECK_OBJECT( generator_heap->var_formatted_text );
                tmp_source_name_2 = generator_heap->var_formatted_text;
                tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 63;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_tuple_element_1 = const_str_digest_301caf0732d954907c54da665f688542;
                tmp_args_element_name_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_args_element_name_1, 0, tmp_tuple_element_1 );
                if ( generator_heap->var_escaped_text == NULL )
                {
                    Py_DECREF( tmp_called_name_1 );
                    Py_DECREF( tmp_args_element_name_1 );
                    generator_heap->exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "escaped_text" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 63;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_tuple_element_1 = generator_heap->var_escaped_text;
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_args_element_name_1, 1, tmp_tuple_element_1 );
                generator->m_frame->m_frame.f_lineno = 63;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1 };
                    tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_element_name_1 );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 63;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            {
                PyObject *tmp_assign_source_7;
                PyObject *tmp_expression_name_3;
                tmp_expression_name_3 = Py_None;
                Py_INCREF( tmp_expression_name_3 );
                Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), NULL );
                generator->m_yield_return_index = 3;
                return tmp_expression_name_3;
                yield_return_3:
                Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_condition_result_3, sizeof(nuitka_bool), &tmp_compexpr_left_3, sizeof(PyObject *), &tmp_compexpr_right_3, sizeof(PyObject *), NULL );
                if ( yield_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 64;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_7 = yield_return_value;
                {
                    PyObject *old = generator_heap->var_c;
                    assert( old != NULL );
                    generator_heap->var_c = tmp_assign_source_7;
                    Py_INCREF( generator_heap->var_c );
                    Py_DECREF( old );
                }

            }
            goto loop_end_1;
            goto branch_end_3;
            branch_no_3:;
            {
                PyObject *tmp_assign_source_8;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                if ( generator_heap->var_escaped_text == NULL )
                {

                    generator_heap->exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "escaped_text" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 67;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_1 = generator_heap->var_escaped_text;
                CHECK_OBJECT( generator_heap->var_c );
                tmp_right_name_1 = generator_heap->var_c;
                generator_heap->tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_1, tmp_right_name_1 );
                if ( generator_heap->tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 67;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_8 = tmp_left_name_1;
                generator_heap->var_escaped_text = tmp_assign_source_8;

            }
            branch_end_3:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 60;
            generator_heap->type_description_1 = "cooooooooo";
            goto frame_exception_exit_1;
        }
        goto loop_start_2;
        loop_end_1:;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        if ( generator_heap->var_c == NULL )
        {

            generator_heap->exception_type = PyExc_UnboundLocalError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "c" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 69;
            generator_heap->type_description_1 = "cooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_4 = generator_heap->var_c;
        tmp_compexpr_right_4 = const_str_chr_27;
        generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 69;
            generator_heap->type_description_1 = "cooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_expression_name_4;
            tmp_expression_name_4 = Py_None;
            Py_INCREF( tmp_expression_name_4 );
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), NULL );
            generator->m_yield_return_index = 4;
            return tmp_expression_name_4;
            yield_return_4:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_4, sizeof(nuitka_bool), &tmp_compexpr_left_4, sizeof(PyObject *), &tmp_compexpr_right_4, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 71;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_9 = yield_return_value;
            {
                PyObject *old = generator_heap->var_square_bracket;
                generator_heap->var_square_bracket = tmp_assign_source_9;
                Py_INCREF( generator_heap->var_square_bracket );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( generator_heap->var_square_bracket );
            tmp_compexpr_left_5 = generator_heap->var_square_bracket;
            tmp_compexpr_right_5 = const_str_chr_91;
            generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 72;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assign_source_10;
                tmp_assign_source_10 = Py_True;
                {
                    PyObject *old = generator_heap->var_csi;
                    assert( old != NULL );
                    generator_heap->var_csi = tmp_assign_source_10;
                    Py_INCREF( generator_heap->var_csi );
                    Py_DECREF( old );
                }

            }
            goto branch_end_5;
            branch_no_5:;
            goto loop_start_1;
            branch_end_5:;
        }
        goto branch_end_4;
        branch_no_4:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            if ( generator_heap->var_c == NULL )
            {

                generator_heap->exception_type = PyExc_UnboundLocalError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "c" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 76;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_left_6 = generator_heap->var_c;
            tmp_compexpr_right_6 = const_str_chr_155;
            generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 76;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_6 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_assign_source_11;
                tmp_assign_source_11 = Py_True;
                {
                    PyObject *old = generator_heap->var_csi;
                    assert( old != NULL );
                    generator_heap->var_csi = tmp_assign_source_11;
                    Py_INCREF( generator_heap->var_csi );
                    Py_DECREF( old );
                }

            }
            branch_no_6:;
        }
        branch_end_4:;
    }
    {
        nuitka_bool tmp_condition_result_7;
        int tmp_truth_name_1;
        if ( generator_heap->var_csi == NULL )
        {

            generator_heap->exception_type = PyExc_UnboundLocalError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "csi" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 79;
            generator_heap->type_description_1 = "cooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_1 = CHECK_IF_TRUE( generator_heap->var_csi );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 79;
            generator_heap->type_description_1 = "cooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_7 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_12;
            tmp_assign_source_12 = const_str_empty;
            {
                PyObject *old = generator_heap->var_current;
                generator_heap->var_current = tmp_assign_source_12;
                Py_INCREF( generator_heap->var_current );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_13;
            tmp_assign_source_13 = PyList_New( 0 );
            {
                PyObject *old = generator_heap->var_params;
                generator_heap->var_params = tmp_assign_source_13;
                Py_XDECREF( old );
            }

        }
        loop_start_3:;
        {
            PyObject *tmp_assign_source_14;
            PyObject *tmp_expression_name_5;
            tmp_expression_name_5 = Py_None;
            Py_INCREF( tmp_expression_name_5 );
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_7, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), NULL );
            generator->m_yield_return_index = 5;
            return tmp_expression_name_5;
            yield_return_5:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_7, sizeof(nuitka_bool), &tmp_truth_name_1, sizeof(int), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 84;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_14 = yield_return_value;
            {
                PyObject *old = generator_heap->var_char;
                generator_heap->var_char = tmp_assign_source_14;
                Py_INCREF( generator_heap->var_char );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_2;
            int tmp_truth_name_2;
            CHECK_OBJECT( generator_heap->var_char );
            tmp_called_instance_1 = generator_heap->var_char;
            generator->m_frame->m_frame.f_lineno = 85;
            tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_isdigit );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 85;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_2 );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                Py_DECREF( tmp_call_result_2 );

                generator_heap->exception_lineno = 85;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_2 );
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_assign_source_15;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                if ( generator_heap->var_current == NULL )
                {

                    generator_heap->exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "current" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 86;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_2 = generator_heap->var_current;
                CHECK_OBJECT( generator_heap->var_char );
                tmp_right_name_2 = generator_heap->var_char;
                generator_heap->tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_2, tmp_right_name_2 );
                if ( generator_heap->tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 86;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_15 = tmp_left_name_2;
                generator_heap->var_current = tmp_assign_source_15;

            }
            goto branch_end_8;
            branch_no_8:;
            {
                PyObject *tmp_called_name_2;
                PyObject *tmp_source_name_3;
                PyObject *tmp_call_result_3;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_called_name_3;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_int_arg_1;
                int tmp_or_left_truth_1;
                PyObject *tmp_or_left_value_1;
                PyObject *tmp_or_right_value_1;
                PyObject *tmp_args_element_name_4;
                CHECK_OBJECT( generator_heap->var_params );
                tmp_source_name_3 = generator_heap->var_params;
                tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_append );
                if ( tmp_called_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 88;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_min );
                assert( tmp_called_name_3 != NULL );
                if ( generator_heap->var_current == NULL )
                {
                    Py_DECREF( tmp_called_name_2 );
                    generator_heap->exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( generator_heap->exception_type );
                    generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "current" );
                    generator_heap->exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    CHAIN_EXCEPTION( generator_heap->exception_value );

                    generator_heap->exception_lineno = 88;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_or_left_value_1 = generator_heap->var_current;
                tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
                if ( tmp_or_left_truth_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    Py_DECREF( tmp_called_name_2 );

                    generator_heap->exception_lineno = 88;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                if ( tmp_or_left_truth_1 == 1 )
                {
                    goto or_left_1;
                }
                else
                {
                    goto or_right_1;
                }
                or_right_1:;
                tmp_or_right_value_1 = const_int_0;
                tmp_int_arg_1 = tmp_or_right_value_1;
                goto or_end_1;
                or_left_1:;
                tmp_int_arg_1 = tmp_or_left_value_1;
                or_end_1:;
                tmp_args_element_name_3 = PyNumber_Int( tmp_int_arg_1 );
                if ( tmp_args_element_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    Py_DECREF( tmp_called_name_2 );

                    generator_heap->exception_lineno = 88;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_args_element_name_4 = const_int_pos_9999;
                generator->m_frame->m_frame.f_lineno = 88;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                    tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
                }

                Py_DECREF( tmp_args_element_name_3 );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    Py_DECREF( tmp_called_name_2 );

                    generator_heap->exception_lineno = 88;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                generator->m_frame->m_frame.f_lineno = 88;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                }

                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 88;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            {
                nuitka_bool tmp_condition_result_9;
                PyObject *tmp_compexpr_left_7;
                PyObject *tmp_compexpr_right_7;
                CHECK_OBJECT( generator_heap->var_char );
                tmp_compexpr_left_7 = generator_heap->var_char;
                tmp_compexpr_right_7 = const_str_chr_59;
                generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                if ( generator_heap->tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                    generator_heap->exception_lineno = 89;
                    generator_heap->type_description_1 = "cooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_9 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_9;
                }
                else
                {
                    goto branch_no_9;
                }
                branch_yes_9:;
                {
                    PyObject *tmp_assign_source_16;
                    tmp_assign_source_16 = const_str_empty;
                    {
                        PyObject *old = generator_heap->var_current;
                        generator_heap->var_current = tmp_assign_source_16;
                        Py_INCREF( generator_heap->var_current );
                        Py_XDECREF( old );
                    }

                }
                goto branch_end_9;
                branch_no_9:;
                {
                    nuitka_bool tmp_condition_result_10;
                    PyObject *tmp_compexpr_left_8;
                    PyObject *tmp_compexpr_right_8;
                    CHECK_OBJECT( generator_heap->var_char );
                    tmp_compexpr_left_8 = generator_heap->var_char;
                    tmp_compexpr_right_8 = const_str_plain_m;
                    generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                    if ( generator_heap->tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                        generator_heap->exception_lineno = 91;
                        generator_heap->type_description_1 = "cooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_10 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_10;
                    }
                    else
                    {
                        goto branch_no_10;
                    }
                    branch_yes_10:;
                    {
                        PyObject *tmp_called_instance_2;
                        PyObject *tmp_call_result_4;
                        PyObject *tmp_args_element_name_5;
                        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
                        {

                            generator_heap->exception_type = PyExc_NameError;
                            Py_INCREF( generator_heap->exception_type );
                            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                            generator_heap->exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            CHAIN_EXCEPTION( generator_heap->exception_value );

                            generator_heap->exception_lineno = 93;
                            generator_heap->type_description_1 = "cooooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_instance_2 = PyCell_GET( generator->m_closure[0] );
                        CHECK_OBJECT( generator_heap->var_params );
                        tmp_args_element_name_5 = generator_heap->var_params;
                        generator->m_frame->m_frame.f_lineno = 93;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_5 };
                            tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain__select_graphic_rendition, call_args );
                        }

                        if ( tmp_call_result_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                            generator_heap->exception_lineno = 93;
                            generator_heap->type_description_1 = "cooooooooo";
                            goto frame_exception_exit_1;
                        }
                        Py_DECREF( tmp_call_result_4 );
                    }
                    {
                        PyObject *tmp_assign_source_17;
                        PyObject *tmp_called_instance_3;
                        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
                        {

                            generator_heap->exception_type = PyExc_NameError;
                            Py_INCREF( generator_heap->exception_type );
                            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                            generator_heap->exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                            CHAIN_EXCEPTION( generator_heap->exception_value );

                            generator_heap->exception_lineno = 94;
                            generator_heap->type_description_1 = "cooooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_instance_3 = PyCell_GET( generator->m_closure[0] );
                        generator->m_frame->m_frame.f_lineno = 94;
                        tmp_assign_source_17 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain__create_style_string );
                        if ( tmp_assign_source_17 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                            generator_heap->exception_lineno = 94;
                            generator_heap->type_description_1 = "cooooooooo";
                            goto frame_exception_exit_1;
                        }
                        {
                            PyObject *old = generator_heap->var_style;
                            generator_heap->var_style = tmp_assign_source_17;
                            Py_XDECREF( old );
                        }

                    }
                    goto loop_end_2;
                    goto branch_end_10;
                    branch_no_10:;
                    goto loop_end_2;
                    branch_end_10:;
                }
                branch_end_9:;
            }
            branch_end_8:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 83;
            generator_heap->type_description_1 = "cooooooooo";
            goto frame_exception_exit_1;
        }
        goto loop_start_3;
        loop_end_2:;
        goto branch_end_7;
        branch_no_7:;
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_4;
            PyObject *tmp_call_result_5;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_tuple_element_2;
            CHECK_OBJECT( generator_heap->var_formatted_text );
            tmp_source_name_4 = generator_heap->var_formatted_text;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_append );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 106;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            if ( generator_heap->var_style == NULL )
            {
                Py_DECREF( tmp_called_name_4 );
                generator_heap->exception_type = PyExc_UnboundLocalError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "style" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 106;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_2 = generator_heap->var_style;
            tmp_args_element_name_6 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_element_name_6, 0, tmp_tuple_element_2 );
            if ( generator_heap->var_c == NULL )
            {
                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_6 );
                generator_heap->exception_type = PyExc_UnboundLocalError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "c" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 106;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_2 = generator_heap->var_c;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_element_name_6, 1, tmp_tuple_element_2 );
            generator->m_frame->m_frame.f_lineno = 106;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 106;
                generator_heap->type_description_1 = "cooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_end_7:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 53;
        generator_heap->type_description_1 = "cooooooooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[0],
            generator_heap->var_style,
            generator_heap->var_formatted_text,
            generator_heap->var_csi,
            generator_heap->var_c,
            generator_heap->var_escaped_text,
            generator_heap->var_square_bracket,
            generator_heap->var_current,
            generator_heap->var_params,
            generator_heap->var_char
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot$$$genobj_1__parse_corot );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_style );
    generator_heap->var_style = NULL;

    Py_XDECREF( generator_heap->var_formatted_text );
    generator_heap->var_formatted_text = NULL;

    Py_XDECREF( generator_heap->var_csi );
    generator_heap->var_csi = NULL;

    Py_XDECREF( generator_heap->var_c );
    generator_heap->var_c = NULL;

    Py_XDECREF( generator_heap->var_escaped_text );
    generator_heap->var_escaped_text = NULL;

    Py_XDECREF( generator_heap->var_square_bracket );
    generator_heap->var_square_bracket = NULL;

    Py_XDECREF( generator_heap->var_current );
    generator_heap->var_current = NULL;

    Py_XDECREF( generator_heap->var_params );
    generator_heap->var_params = NULL;

    Py_XDECREF( generator_heap->var_char );
    generator_heap->var_char = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:


    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot$$$genobj_1__parse_corot_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot$$$genobj_1__parse_corot_context,
        module_prompt_toolkit$formatted_text$ansi,
        const_str_plain__parse_corot,
#if PYTHON_VERSION >= 350
        const_str_digest_961deea024e5668b00b2f477056197e9,
#endif
        codeobj_7a19018b97e3050b7736fb578a18b9b0,
        1,
        sizeof(struct prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot$$$genobj_1__parse_corot_locals)
    );
}


static PyObject *impl_prompt_toolkit$formatted_text$ansi$$$function_3__select_graphic_rendition( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attrs = python_pars[ 1 ];
    PyObject *var_attr = NULL;
    PyObject *var_n = NULL;
    PyObject *var_m = NULL;
    PyObject *var_color_str = NULL;
    PyObject *tmp_try_except_1__unhandled_indicator = NULL;
    struct Nuitka_FrameObject *frame_9f13e85c4c17356e845f140da8d2e279;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_9f13e85c4c17356e845f140da8d2e279 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9f13e85c4c17356e845f140da8d2e279, codeobj_9f13e85c4c17356e845f140da8d2e279, module_prompt_toolkit$formatted_text$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9f13e85c4c17356e845f140da8d2e279 = cache_frame_9f13e85c4c17356e845f140da8d2e279;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9f13e85c4c17356e845f140da8d2e279 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9f13e85c4c17356e845f140da8d2e279 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( par_attrs );
        tmp_operand_name_1 = par_attrs;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            tmp_assign_source_1 = LIST_COPY( const_list_int_0_list );
            {
                PyObject *old = par_attrs;
                assert( old != NULL );
                par_attrs = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_list_arg_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( par_attrs );
            tmp_subscribed_name_1 = par_attrs;
            tmp_subscript_name_1 = const_slice_none_none_int_neg_1;
            tmp_list_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_list_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_2 = PySequence_List( tmp_list_arg_1 );
            Py_DECREF( tmp_list_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_attrs;
                assert( old != NULL );
                par_attrs = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        branch_end_1:;
    }
    loop_start_1:;
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_2;
        CHECK_OBJECT( par_attrs );
        tmp_operand_name_2 = par_attrs;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        goto loop_end_1;
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_attrs );
        tmp_called_instance_1 = par_attrs;
        frame_9f13e85c4c17356e845f140da8d2e279->m_frame.f_lineno = 118;
        tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_pop );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_attr;
            var_attr = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( var_attr );
        tmp_compexpr_left_1 = var_attr;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__fg_colors );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__fg_colors );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_fg_colors" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_1;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_assattr_target_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__fg_colors );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__fg_colors );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_fg_colors" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 121;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_2 = tmp_mvar_value_2;
            CHECK_OBJECT( var_attr );
            tmp_subscript_name_2 = var_attr;
            tmp_assattr_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 121;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_assattr_target_1 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__color, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 121;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_3;
        branch_no_3:;
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_mvar_value_3;
            CHECK_OBJECT( var_attr );
            tmp_compexpr_left_2 = var_attr;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__bg_colors );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__bg_colors );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_bg_colors" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 122;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_right_2 = tmp_mvar_value_3;
            tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 122;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assattr_name_2;
                PyObject *tmp_subscribed_name_3;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_subscript_name_3;
                PyObject *tmp_assattr_target_2;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__bg_colors );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__bg_colors );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_bg_colors" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 123;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }

                tmp_subscribed_name_3 = tmp_mvar_value_4;
                CHECK_OBJECT( var_attr );
                tmp_subscript_name_3 = var_attr;
                tmp_assattr_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
                if ( tmp_assattr_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 123;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_self );
                tmp_assattr_target_2 = par_self;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__bgcolor, tmp_assattr_name_2 );
                Py_DECREF( tmp_assattr_name_2 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 123;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
            }
            goto branch_end_4;
            branch_no_4:;
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                CHECK_OBJECT( var_attr );
                tmp_compexpr_left_3 = var_attr;
                tmp_compexpr_right_3 = const_int_pos_1;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 124;
                    type_description_1 = "oooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_assattr_name_3;
                    PyObject *tmp_assattr_target_3;
                    tmp_assattr_name_3 = Py_True;
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_3 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__bold, tmp_assattr_name_3 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 125;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                }
                goto branch_end_5;
                branch_no_5:;
                {
                    nuitka_bool tmp_condition_result_6;
                    PyObject *tmp_compexpr_left_4;
                    PyObject *tmp_compexpr_right_4;
                    CHECK_OBJECT( var_attr );
                    tmp_compexpr_left_4 = var_attr;
                    tmp_compexpr_right_4 = const_int_pos_3;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 126;
                        type_description_1 = "oooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_6;
                    }
                    else
                    {
                        goto branch_no_6;
                    }
                    branch_yes_6:;
                    {
                        PyObject *tmp_assattr_name_4;
                        PyObject *tmp_assattr_target_4;
                        tmp_assattr_name_4 = Py_True;
                        CHECK_OBJECT( par_self );
                        tmp_assattr_target_4 = par_self;
                        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__italic, tmp_assattr_name_4 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 127;
                            type_description_1 = "oooooo";
                            goto frame_exception_exit_1;
                        }
                    }
                    goto branch_end_6;
                    branch_no_6:;
                    {
                        nuitka_bool tmp_condition_result_7;
                        PyObject *tmp_compexpr_left_5;
                        PyObject *tmp_compexpr_right_5;
                        CHECK_OBJECT( var_attr );
                        tmp_compexpr_left_5 = var_attr;
                        tmp_compexpr_right_5 = const_int_pos_4;
                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 128;
                            type_description_1 = "oooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_7;
                        }
                        else
                        {
                            goto branch_no_7;
                        }
                        branch_yes_7:;
                        {
                            PyObject *tmp_assattr_name_5;
                            PyObject *tmp_assattr_target_5;
                            tmp_assattr_name_5 = Py_True;
                            CHECK_OBJECT( par_self );
                            tmp_assattr_target_5 = par_self;
                            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain__underline, tmp_assattr_name_5 );
                            if ( tmp_result == false )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 129;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                        }
                        goto branch_end_7;
                        branch_no_7:;
                        {
                            nuitka_bool tmp_condition_result_8;
                            PyObject *tmp_compexpr_left_6;
                            PyObject *tmp_compexpr_right_6;
                            CHECK_OBJECT( var_attr );
                            tmp_compexpr_left_6 = var_attr;
                            tmp_compexpr_right_6 = const_int_pos_5;
                            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                            if ( tmp_res == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 130;
                                type_description_1 = "oooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                            {
                                goto branch_yes_8;
                            }
                            else
                            {
                                goto branch_no_8;
                            }
                            branch_yes_8:;
                            {
                                PyObject *tmp_assattr_name_6;
                                PyObject *tmp_assattr_target_6;
                                tmp_assattr_name_6 = Py_True;
                                CHECK_OBJECT( par_self );
                                tmp_assattr_target_6 = par_self;
                                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain__blink, tmp_assattr_name_6 );
                                if ( tmp_result == false )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 131;
                                    type_description_1 = "oooooo";
                                    goto frame_exception_exit_1;
                                }
                            }
                            goto branch_end_8;
                            branch_no_8:;
                            {
                                nuitka_bool tmp_condition_result_9;
                                PyObject *tmp_compexpr_left_7;
                                PyObject *tmp_compexpr_right_7;
                                CHECK_OBJECT( var_attr );
                                tmp_compexpr_left_7 = var_attr;
                                tmp_compexpr_right_7 = const_int_pos_6;
                                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                                if ( tmp_res == -1 )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 132;
                                    type_description_1 = "oooooo";
                                    goto frame_exception_exit_1;
                                }
                                tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                                {
                                    goto branch_yes_9;
                                }
                                else
                                {
                                    goto branch_no_9;
                                }
                                branch_yes_9:;
                                {
                                    PyObject *tmp_assattr_name_7;
                                    PyObject *tmp_assattr_target_7;
                                    tmp_assattr_name_7 = Py_True;
                                    CHECK_OBJECT( par_self );
                                    tmp_assattr_target_7 = par_self;
                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain__blink, tmp_assattr_name_7 );
                                    if ( tmp_result == false )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 133;
                                        type_description_1 = "oooooo";
                                        goto frame_exception_exit_1;
                                    }
                                }
                                goto branch_end_9;
                                branch_no_9:;
                                {
                                    nuitka_bool tmp_condition_result_10;
                                    PyObject *tmp_compexpr_left_8;
                                    PyObject *tmp_compexpr_right_8;
                                    CHECK_OBJECT( var_attr );
                                    tmp_compexpr_left_8 = var_attr;
                                    tmp_compexpr_right_8 = const_int_pos_7;
                                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
                                    if ( tmp_res == -1 )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 134;
                                        type_description_1 = "oooooo";
                                        goto frame_exception_exit_1;
                                    }
                                    tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                    if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                                    {
                                        goto branch_yes_10;
                                    }
                                    else
                                    {
                                        goto branch_no_10;
                                    }
                                    branch_yes_10:;
                                    {
                                        PyObject *tmp_assattr_name_8;
                                        PyObject *tmp_assattr_target_8;
                                        tmp_assattr_name_8 = Py_True;
                                        CHECK_OBJECT( par_self );
                                        tmp_assattr_target_8 = par_self;
                                        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain__reverse, tmp_assattr_name_8 );
                                        if ( tmp_result == false )
                                        {
                                            assert( ERROR_OCCURRED() );

                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                            exception_lineno = 135;
                                            type_description_1 = "oooooo";
                                            goto frame_exception_exit_1;
                                        }
                                    }
                                    goto branch_end_10;
                                    branch_no_10:;
                                    {
                                        nuitka_bool tmp_condition_result_11;
                                        PyObject *tmp_compexpr_left_9;
                                        PyObject *tmp_compexpr_right_9;
                                        CHECK_OBJECT( var_attr );
                                        tmp_compexpr_left_9 = var_attr;
                                        tmp_compexpr_right_9 = const_int_pos_8;
                                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                                        if ( tmp_res == -1 )
                                        {
                                            assert( ERROR_OCCURRED() );

                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                            exception_lineno = 136;
                                            type_description_1 = "oooooo";
                                            goto frame_exception_exit_1;
                                        }
                                        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                                        {
                                            goto branch_yes_11;
                                        }
                                        else
                                        {
                                            goto branch_no_11;
                                        }
                                        branch_yes_11:;
                                        {
                                            PyObject *tmp_assattr_name_9;
                                            PyObject *tmp_assattr_target_9;
                                            tmp_assattr_name_9 = Py_True;
                                            CHECK_OBJECT( par_self );
                                            tmp_assattr_target_9 = par_self;
                                            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_9, const_str_plain__hidden, tmp_assattr_name_9 );
                                            if ( tmp_result == false )
                                            {
                                                assert( ERROR_OCCURRED() );

                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                exception_lineno = 137;
                                                type_description_1 = "oooooo";
                                                goto frame_exception_exit_1;
                                            }
                                        }
                                        goto branch_end_11;
                                        branch_no_11:;
                                        {
                                            nuitka_bool tmp_condition_result_12;
                                            PyObject *tmp_compexpr_left_10;
                                            PyObject *tmp_compexpr_right_10;
                                            CHECK_OBJECT( var_attr );
                                            tmp_compexpr_left_10 = var_attr;
                                            tmp_compexpr_right_10 = const_int_pos_22;
                                            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
                                            if ( tmp_res == -1 )
                                            {
                                                assert( ERROR_OCCURRED() );

                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                exception_lineno = 138;
                                                type_description_1 = "oooooo";
                                                goto frame_exception_exit_1;
                                            }
                                            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                                            {
                                                goto branch_yes_12;
                                            }
                                            else
                                            {
                                                goto branch_no_12;
                                            }
                                            branch_yes_12:;
                                            {
                                                PyObject *tmp_assattr_name_10;
                                                PyObject *tmp_assattr_target_10;
                                                tmp_assattr_name_10 = Py_False;
                                                CHECK_OBJECT( par_self );
                                                tmp_assattr_target_10 = par_self;
                                                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_10, const_str_plain__bold, tmp_assattr_name_10 );
                                                if ( tmp_result == false )
                                                {
                                                    assert( ERROR_OCCURRED() );

                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                    exception_lineno = 139;
                                                    type_description_1 = "oooooo";
                                                    goto frame_exception_exit_1;
                                                }
                                            }
                                            goto branch_end_12;
                                            branch_no_12:;
                                            {
                                                nuitka_bool tmp_condition_result_13;
                                                PyObject *tmp_compexpr_left_11;
                                                PyObject *tmp_compexpr_right_11;
                                                CHECK_OBJECT( var_attr );
                                                tmp_compexpr_left_11 = var_attr;
                                                tmp_compexpr_right_11 = const_int_pos_23;
                                                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
                                                if ( tmp_res == -1 )
                                                {
                                                    assert( ERROR_OCCURRED() );

                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                    exception_lineno = 140;
                                                    type_description_1 = "oooooo";
                                                    goto frame_exception_exit_1;
                                                }
                                                tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                                                {
                                                    goto branch_yes_13;
                                                }
                                                else
                                                {
                                                    goto branch_no_13;
                                                }
                                                branch_yes_13:;
                                                {
                                                    PyObject *tmp_assattr_name_11;
                                                    PyObject *tmp_assattr_target_11;
                                                    tmp_assattr_name_11 = Py_False;
                                                    CHECK_OBJECT( par_self );
                                                    tmp_assattr_target_11 = par_self;
                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_11, const_str_plain__italic, tmp_assattr_name_11 );
                                                    if ( tmp_result == false )
                                                    {
                                                        assert( ERROR_OCCURRED() );

                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                        exception_lineno = 141;
                                                        type_description_1 = "oooooo";
                                                        goto frame_exception_exit_1;
                                                    }
                                                }
                                                goto branch_end_13;
                                                branch_no_13:;
                                                {
                                                    nuitka_bool tmp_condition_result_14;
                                                    PyObject *tmp_compexpr_left_12;
                                                    PyObject *tmp_compexpr_right_12;
                                                    CHECK_OBJECT( var_attr );
                                                    tmp_compexpr_left_12 = var_attr;
                                                    tmp_compexpr_right_12 = const_int_pos_24;
                                                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_12, tmp_compexpr_right_12 );
                                                    if ( tmp_res == -1 )
                                                    {
                                                        assert( ERROR_OCCURRED() );

                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                        exception_lineno = 142;
                                                        type_description_1 = "oooooo";
                                                        goto frame_exception_exit_1;
                                                    }
                                                    tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                    if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                                                    {
                                                        goto branch_yes_14;
                                                    }
                                                    else
                                                    {
                                                        goto branch_no_14;
                                                    }
                                                    branch_yes_14:;
                                                    {
                                                        PyObject *tmp_assattr_name_12;
                                                        PyObject *tmp_assattr_target_12;
                                                        tmp_assattr_name_12 = Py_False;
                                                        CHECK_OBJECT( par_self );
                                                        tmp_assattr_target_12 = par_self;
                                                        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_12, const_str_plain__underline, tmp_assattr_name_12 );
                                                        if ( tmp_result == false )
                                                        {
                                                            assert( ERROR_OCCURRED() );

                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                            exception_lineno = 143;
                                                            type_description_1 = "oooooo";
                                                            goto frame_exception_exit_1;
                                                        }
                                                    }
                                                    goto branch_end_14;
                                                    branch_no_14:;
                                                    {
                                                        nuitka_bool tmp_condition_result_15;
                                                        PyObject *tmp_compexpr_left_13;
                                                        PyObject *tmp_compexpr_right_13;
                                                        CHECK_OBJECT( var_attr );
                                                        tmp_compexpr_left_13 = var_attr;
                                                        tmp_compexpr_right_13 = const_int_pos_25;
                                                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_13, tmp_compexpr_right_13 );
                                                        if ( tmp_res == -1 )
                                                        {
                                                            assert( ERROR_OCCURRED() );

                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                            exception_lineno = 144;
                                                            type_description_1 = "oooooo";
                                                            goto frame_exception_exit_1;
                                                        }
                                                        tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
                                                        {
                                                            goto branch_yes_15;
                                                        }
                                                        else
                                                        {
                                                            goto branch_no_15;
                                                        }
                                                        branch_yes_15:;
                                                        {
                                                            PyObject *tmp_assattr_name_13;
                                                            PyObject *tmp_assattr_target_13;
                                                            tmp_assattr_name_13 = Py_False;
                                                            CHECK_OBJECT( par_self );
                                                            tmp_assattr_target_13 = par_self;
                                                            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_13, const_str_plain__blink, tmp_assattr_name_13 );
                                                            if ( tmp_result == false )
                                                            {
                                                                assert( ERROR_OCCURRED() );

                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                exception_lineno = 145;
                                                                type_description_1 = "oooooo";
                                                                goto frame_exception_exit_1;
                                                            }
                                                        }
                                                        goto branch_end_15;
                                                        branch_no_15:;
                                                        {
                                                            nuitka_bool tmp_condition_result_16;
                                                            PyObject *tmp_compexpr_left_14;
                                                            PyObject *tmp_compexpr_right_14;
                                                            CHECK_OBJECT( var_attr );
                                                            tmp_compexpr_left_14 = var_attr;
                                                            tmp_compexpr_right_14 = const_int_pos_27;
                                                            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_14, tmp_compexpr_right_14 );
                                                            if ( tmp_res == -1 )
                                                            {
                                                                assert( ERROR_OCCURRED() );

                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                exception_lineno = 146;
                                                                type_description_1 = "oooooo";
                                                                goto frame_exception_exit_1;
                                                            }
                                                            tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                            if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
                                                            {
                                                                goto branch_yes_16;
                                                            }
                                                            else
                                                            {
                                                                goto branch_no_16;
                                                            }
                                                            branch_yes_16:;
                                                            {
                                                                PyObject *tmp_assattr_name_14;
                                                                PyObject *tmp_assattr_target_14;
                                                                tmp_assattr_name_14 = Py_False;
                                                                CHECK_OBJECT( par_self );
                                                                tmp_assattr_target_14 = par_self;
                                                                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_14, const_str_plain__reverse, tmp_assattr_name_14 );
                                                                if ( tmp_result == false )
                                                                {
                                                                    assert( ERROR_OCCURRED() );

                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                    exception_lineno = 147;
                                                                    type_description_1 = "oooooo";
                                                                    goto frame_exception_exit_1;
                                                                }
                                                            }
                                                            goto branch_end_16;
                                                            branch_no_16:;
                                                            {
                                                                nuitka_bool tmp_condition_result_17;
                                                                PyObject *tmp_operand_name_3;
                                                                CHECK_OBJECT( var_attr );
                                                                tmp_operand_name_3 = var_attr;
                                                                tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
                                                                if ( tmp_res == -1 )
                                                                {
                                                                    assert( ERROR_OCCURRED() );

                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                    exception_lineno = 148;
                                                                    type_description_1 = "oooooo";
                                                                    goto frame_exception_exit_1;
                                                                }
                                                                tmp_condition_result_17 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
                                                                {
                                                                    goto branch_yes_17;
                                                                }
                                                                else
                                                                {
                                                                    goto branch_no_17;
                                                                }
                                                                branch_yes_17:;
                                                                {
                                                                    PyObject *tmp_assattr_name_15;
                                                                    PyObject *tmp_assattr_target_15;
                                                                    tmp_assattr_name_15 = Py_None;
                                                                    CHECK_OBJECT( par_self );
                                                                    tmp_assattr_target_15 = par_self;
                                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_15, const_str_plain__color, tmp_assattr_name_15 );
                                                                    if ( tmp_result == false )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 149;
                                                                        type_description_1 = "oooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                }
                                                                {
                                                                    PyObject *tmp_assattr_name_16;
                                                                    PyObject *tmp_assattr_target_16;
                                                                    tmp_assattr_name_16 = Py_None;
                                                                    CHECK_OBJECT( par_self );
                                                                    tmp_assattr_target_16 = par_self;
                                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_16, const_str_plain__bgcolor, tmp_assattr_name_16 );
                                                                    if ( tmp_result == false )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 150;
                                                                        type_description_1 = "oooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                }
                                                                {
                                                                    PyObject *tmp_assattr_name_17;
                                                                    PyObject *tmp_assattr_target_17;
                                                                    tmp_assattr_name_17 = Py_False;
                                                                    CHECK_OBJECT( par_self );
                                                                    tmp_assattr_target_17 = par_self;
                                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_17, const_str_plain__bold, tmp_assattr_name_17 );
                                                                    if ( tmp_result == false )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 151;
                                                                        type_description_1 = "oooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                }
                                                                {
                                                                    PyObject *tmp_assattr_name_18;
                                                                    PyObject *tmp_assattr_target_18;
                                                                    tmp_assattr_name_18 = Py_False;
                                                                    CHECK_OBJECT( par_self );
                                                                    tmp_assattr_target_18 = par_self;
                                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_18, const_str_plain__underline, tmp_assattr_name_18 );
                                                                    if ( tmp_result == false )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 152;
                                                                        type_description_1 = "oooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                }
                                                                {
                                                                    PyObject *tmp_assattr_name_19;
                                                                    PyObject *tmp_assattr_target_19;
                                                                    tmp_assattr_name_19 = Py_False;
                                                                    CHECK_OBJECT( par_self );
                                                                    tmp_assattr_target_19 = par_self;
                                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_19, const_str_plain__italic, tmp_assattr_name_19 );
                                                                    if ( tmp_result == false )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 153;
                                                                        type_description_1 = "oooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                }
                                                                {
                                                                    PyObject *tmp_assattr_name_20;
                                                                    PyObject *tmp_assattr_target_20;
                                                                    tmp_assattr_name_20 = Py_False;
                                                                    CHECK_OBJECT( par_self );
                                                                    tmp_assattr_target_20 = par_self;
                                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_20, const_str_plain__blink, tmp_assattr_name_20 );
                                                                    if ( tmp_result == false )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 154;
                                                                        type_description_1 = "oooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                }
                                                                {
                                                                    PyObject *tmp_assattr_name_21;
                                                                    PyObject *tmp_assattr_target_21;
                                                                    tmp_assattr_name_21 = Py_False;
                                                                    CHECK_OBJECT( par_self );
                                                                    tmp_assattr_target_21 = par_self;
                                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_21, const_str_plain__reverse, tmp_assattr_name_21 );
                                                                    if ( tmp_result == false )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 155;
                                                                        type_description_1 = "oooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                }
                                                                {
                                                                    PyObject *tmp_assattr_name_22;
                                                                    PyObject *tmp_assattr_target_22;
                                                                    tmp_assattr_name_22 = Py_False;
                                                                    CHECK_OBJECT( par_self );
                                                                    tmp_assattr_target_22 = par_self;
                                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_22, const_str_plain__hidden, tmp_assattr_name_22 );
                                                                    if ( tmp_result == false )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 156;
                                                                        type_description_1 = "oooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                }
                                                                goto branch_end_17;
                                                                branch_no_17:;
                                                                {
                                                                    nuitka_bool tmp_condition_result_18;
                                                                    int tmp_and_left_truth_1;
                                                                    nuitka_bool tmp_and_left_value_1;
                                                                    nuitka_bool tmp_and_right_value_1;
                                                                    PyObject *tmp_compexpr_left_15;
                                                                    PyObject *tmp_compexpr_right_15;
                                                                    PyObject *tmp_compexpr_left_16;
                                                                    PyObject *tmp_compexpr_right_16;
                                                                    PyObject *tmp_len_arg_1;
                                                                    CHECK_OBJECT( var_attr );
                                                                    tmp_compexpr_left_15 = var_attr;
                                                                    tmp_compexpr_right_15 = const_tuple_int_pos_38_int_pos_48_tuple;
                                                                    tmp_res = PySequence_Contains( tmp_compexpr_right_15, tmp_compexpr_left_15 );
                                                                    if ( tmp_res == -1 )
                                                                    {
                                                                        assert( ERROR_OCCURRED() );

                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                        exception_lineno = 158;
                                                                        type_description_1 = "oooooo";
                                                                        goto frame_exception_exit_1;
                                                                    }
                                                                    tmp_and_left_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                    tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
                                                                    if ( tmp_and_left_truth_1 == 1 )
                                                                    {
                                                                        goto and_right_1;
                                                                    }
                                                                    else
                                                                    {
                                                                        goto and_left_1;
                                                                    }
                                                                    and_right_1:;
                                                                    CHECK_OBJECT( par_attrs );
                                                                    tmp_len_arg_1 = par_attrs;
                                                                    tmp_compexpr_left_16 = BUILTIN_LEN( tmp_len_arg_1 );
                                                                    assert( !(tmp_compexpr_left_16 == NULL) );
                                                                    tmp_compexpr_right_16 = const_int_pos_1;
                                                                    tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_16, tmp_compexpr_right_16 );
                                                                    Py_DECREF( tmp_compexpr_left_16 );
                                                                    assert( !(tmp_res == -1) );
                                                                    tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                    tmp_condition_result_18 = tmp_and_right_value_1;
                                                                    goto and_end_1;
                                                                    and_left_1:;
                                                                    tmp_condition_result_18 = tmp_and_left_value_1;
                                                                    and_end_1:;
                                                                    if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
                                                                    {
                                                                        goto branch_yes_18;
                                                                    }
                                                                    else
                                                                    {
                                                                        goto branch_no_18;
                                                                    }
                                                                    branch_yes_18:;
                                                                    {
                                                                        PyObject *tmp_assign_source_4;
                                                                        PyObject *tmp_called_instance_2;
                                                                        CHECK_OBJECT( par_attrs );
                                                                        tmp_called_instance_2 = par_attrs;
                                                                        frame_9f13e85c4c17356e845f140da8d2e279->m_frame.f_lineno = 159;
                                                                        tmp_assign_source_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_pop );
                                                                        if ( tmp_assign_source_4 == NULL )
                                                                        {
                                                                            assert( ERROR_OCCURRED() );

                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                            exception_lineno = 159;
                                                                            type_description_1 = "oooooo";
                                                                            goto frame_exception_exit_1;
                                                                        }
                                                                        {
                                                                            PyObject *old = var_n;
                                                                            var_n = tmp_assign_source_4;
                                                                            Py_XDECREF( old );
                                                                        }

                                                                    }
                                                                    {
                                                                        nuitka_bool tmp_condition_result_19;
                                                                        int tmp_and_left_truth_2;
                                                                        nuitka_bool tmp_and_left_value_2;
                                                                        nuitka_bool tmp_and_right_value_2;
                                                                        PyObject *tmp_compexpr_left_17;
                                                                        PyObject *tmp_compexpr_right_17;
                                                                        PyObject *tmp_compexpr_left_18;
                                                                        PyObject *tmp_compexpr_right_18;
                                                                        PyObject *tmp_len_arg_2;
                                                                        CHECK_OBJECT( var_n );
                                                                        tmp_compexpr_left_17 = var_n;
                                                                        tmp_compexpr_right_17 = const_int_pos_5;
                                                                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_17, tmp_compexpr_right_17 );
                                                                        if ( tmp_res == -1 )
                                                                        {
                                                                            assert( ERROR_OCCURRED() );

                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                            exception_lineno = 162;
                                                                            type_description_1 = "oooooo";
                                                                            goto frame_exception_exit_1;
                                                                        }
                                                                        tmp_and_left_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                        tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
                                                                        if ( tmp_and_left_truth_2 == 1 )
                                                                        {
                                                                            goto and_right_2;
                                                                        }
                                                                        else
                                                                        {
                                                                            goto and_left_2;
                                                                        }
                                                                        and_right_2:;
                                                                        CHECK_OBJECT( par_attrs );
                                                                        tmp_len_arg_2 = par_attrs;
                                                                        tmp_compexpr_left_18 = BUILTIN_LEN( tmp_len_arg_2 );
                                                                        assert( !(tmp_compexpr_left_18 == NULL) );
                                                                        tmp_compexpr_right_18 = const_int_pos_1;
                                                                        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_18, tmp_compexpr_right_18 );
                                                                        Py_DECREF( tmp_compexpr_left_18 );
                                                                        assert( !(tmp_res == -1) );
                                                                        tmp_and_right_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                        tmp_condition_result_19 = tmp_and_right_value_2;
                                                                        goto and_end_2;
                                                                        and_left_2:;
                                                                        tmp_condition_result_19 = tmp_and_left_value_2;
                                                                        and_end_2:;
                                                                        if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
                                                                        {
                                                                            goto branch_yes_19;
                                                                        }
                                                                        else
                                                                        {
                                                                            goto branch_no_19;
                                                                        }
                                                                        branch_yes_19:;
                                                                        {
                                                                            nuitka_bool tmp_condition_result_20;
                                                                            PyObject *tmp_compexpr_left_19;
                                                                            PyObject *tmp_compexpr_right_19;
                                                                            CHECK_OBJECT( var_attr );
                                                                            tmp_compexpr_left_19 = var_attr;
                                                                            tmp_compexpr_right_19 = const_int_pos_38;
                                                                            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_19, tmp_compexpr_right_19 );
                                                                            if ( tmp_res == -1 )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                exception_lineno = 163;
                                                                                type_description_1 = "oooooo";
                                                                                goto frame_exception_exit_1;
                                                                            }
                                                                            tmp_condition_result_20 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                            if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
                                                                            {
                                                                                goto branch_yes_20;
                                                                            }
                                                                            else
                                                                            {
                                                                                goto branch_no_20;
                                                                            }
                                                                            branch_yes_20:;
                                                                            {
                                                                                PyObject *tmp_assign_source_5;
                                                                                PyObject *tmp_called_instance_3;
                                                                                CHECK_OBJECT( par_attrs );
                                                                                tmp_called_instance_3 = par_attrs;
                                                                                frame_9f13e85c4c17356e845f140da8d2e279->m_frame.f_lineno = 164;
                                                                                tmp_assign_source_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_pop );
                                                                                if ( tmp_assign_source_5 == NULL )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                    exception_lineno = 164;
                                                                                    type_description_1 = "oooooo";
                                                                                    goto frame_exception_exit_1;
                                                                                }
                                                                                {
                                                                                    PyObject *old = var_m;
                                                                                    var_m = tmp_assign_source_5;
                                                                                    Py_XDECREF( old );
                                                                                }

                                                                            }
                                                                            {
                                                                                PyObject *tmp_assattr_name_23;
                                                                                PyObject *tmp_called_instance_4;
                                                                                PyObject *tmp_mvar_value_5;
                                                                                PyObject *tmp_args_element_name_1;
                                                                                PyObject *tmp_assattr_target_23;
                                                                                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__256_colors );

                                                                                if (unlikely( tmp_mvar_value_5 == NULL ))
                                                                                {
                                                                                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__256_colors );
                                                                                }

                                                                                if ( tmp_mvar_value_5 == NULL )
                                                                                {

                                                                                    exception_type = PyExc_NameError;
                                                                                    Py_INCREF( exception_type );
                                                                                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_256_colors" );
                                                                                    exception_tb = NULL;
                                                                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                                    CHAIN_EXCEPTION( exception_value );

                                                                                    exception_lineno = 165;
                                                                                    type_description_1 = "oooooo";
                                                                                    goto frame_exception_exit_1;
                                                                                }

                                                                                tmp_called_instance_4 = tmp_mvar_value_5;
                                                                                CHECK_OBJECT( var_m );
                                                                                tmp_args_element_name_1 = var_m;
                                                                                frame_9f13e85c4c17356e845f140da8d2e279->m_frame.f_lineno = 165;
                                                                                {
                                                                                    PyObject *call_args[] = { tmp_args_element_name_1 };
                                                                                    tmp_assattr_name_23 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_get, call_args );
                                                                                }

                                                                                if ( tmp_assattr_name_23 == NULL )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                    exception_lineno = 165;
                                                                                    type_description_1 = "oooooo";
                                                                                    goto frame_exception_exit_1;
                                                                                }
                                                                                CHECK_OBJECT( par_self );
                                                                                tmp_assattr_target_23 = par_self;
                                                                                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_23, const_str_plain__color, tmp_assattr_name_23 );
                                                                                Py_DECREF( tmp_assattr_name_23 );
                                                                                if ( tmp_result == false )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                    exception_lineno = 165;
                                                                                    type_description_1 = "oooooo";
                                                                                    goto frame_exception_exit_1;
                                                                                }
                                                                            }
                                                                            goto branch_end_20;
                                                                            branch_no_20:;
                                                                            {
                                                                                nuitka_bool tmp_condition_result_21;
                                                                                PyObject *tmp_compexpr_left_20;
                                                                                PyObject *tmp_compexpr_right_20;
                                                                                CHECK_OBJECT( var_attr );
                                                                                tmp_compexpr_left_20 = var_attr;
                                                                                tmp_compexpr_right_20 = const_int_pos_48;
                                                                                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_20, tmp_compexpr_right_20 );
                                                                                if ( tmp_res == -1 )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                    exception_lineno = 166;
                                                                                    type_description_1 = "oooooo";
                                                                                    goto frame_exception_exit_1;
                                                                                }
                                                                                tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                                if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
                                                                                {
                                                                                    goto branch_yes_21;
                                                                                }
                                                                                else
                                                                                {
                                                                                    goto branch_no_21;
                                                                                }
                                                                                branch_yes_21:;
                                                                                {
                                                                                    PyObject *tmp_assign_source_6;
                                                                                    PyObject *tmp_called_instance_5;
                                                                                    CHECK_OBJECT( par_attrs );
                                                                                    tmp_called_instance_5 = par_attrs;
                                                                                    frame_9f13e85c4c17356e845f140da8d2e279->m_frame.f_lineno = 167;
                                                                                    tmp_assign_source_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_pop );
                                                                                    if ( tmp_assign_source_6 == NULL )
                                                                                    {
                                                                                        assert( ERROR_OCCURRED() );

                                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                        exception_lineno = 167;
                                                                                        type_description_1 = "oooooo";
                                                                                        goto frame_exception_exit_1;
                                                                                    }
                                                                                    {
                                                                                        PyObject *old = var_m;
                                                                                        var_m = tmp_assign_source_6;
                                                                                        Py_XDECREF( old );
                                                                                    }

                                                                                }
                                                                                {
                                                                                    PyObject *tmp_assattr_name_24;
                                                                                    PyObject *tmp_called_instance_6;
                                                                                    PyObject *tmp_mvar_value_6;
                                                                                    PyObject *tmp_args_element_name_2;
                                                                                    PyObject *tmp_assattr_target_24;
                                                                                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__256_colors );

                                                                                    if (unlikely( tmp_mvar_value_6 == NULL ))
                                                                                    {
                                                                                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__256_colors );
                                                                                    }

                                                                                    if ( tmp_mvar_value_6 == NULL )
                                                                                    {

                                                                                        exception_type = PyExc_NameError;
                                                                                        Py_INCREF( exception_type );
                                                                                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_256_colors" );
                                                                                        exception_tb = NULL;
                                                                                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                                        CHAIN_EXCEPTION( exception_value );

                                                                                        exception_lineno = 168;
                                                                                        type_description_1 = "oooooo";
                                                                                        goto frame_exception_exit_1;
                                                                                    }

                                                                                    tmp_called_instance_6 = tmp_mvar_value_6;
                                                                                    CHECK_OBJECT( var_m );
                                                                                    tmp_args_element_name_2 = var_m;
                                                                                    frame_9f13e85c4c17356e845f140da8d2e279->m_frame.f_lineno = 168;
                                                                                    {
                                                                                        PyObject *call_args[] = { tmp_args_element_name_2 };
                                                                                        tmp_assattr_name_24 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_get, call_args );
                                                                                    }

                                                                                    if ( tmp_assattr_name_24 == NULL )
                                                                                    {
                                                                                        assert( ERROR_OCCURRED() );

                                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                        exception_lineno = 168;
                                                                                        type_description_1 = "oooooo";
                                                                                        goto frame_exception_exit_1;
                                                                                    }
                                                                                    CHECK_OBJECT( par_self );
                                                                                    tmp_assattr_target_24 = par_self;
                                                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_24, const_str_plain__bgcolor, tmp_assattr_name_24 );
                                                                                    Py_DECREF( tmp_assattr_name_24 );
                                                                                    if ( tmp_result == false )
                                                                                    {
                                                                                        assert( ERROR_OCCURRED() );

                                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                        exception_lineno = 168;
                                                                                        type_description_1 = "oooooo";
                                                                                        goto frame_exception_exit_1;
                                                                                    }
                                                                                }
                                                                                branch_no_21:;
                                                                            }
                                                                            branch_end_20:;
                                                                        }
                                                                        branch_no_19:;
                                                                    }
                                                                    {
                                                                        nuitka_bool tmp_condition_result_22;
                                                                        int tmp_and_left_truth_3;
                                                                        nuitka_bool tmp_and_left_value_3;
                                                                        nuitka_bool tmp_and_right_value_3;
                                                                        PyObject *tmp_compexpr_left_21;
                                                                        PyObject *tmp_compexpr_right_21;
                                                                        PyObject *tmp_compexpr_left_22;
                                                                        PyObject *tmp_compexpr_right_22;
                                                                        PyObject *tmp_len_arg_3;
                                                                        CHECK_OBJECT( var_n );
                                                                        tmp_compexpr_left_21 = var_n;
                                                                        tmp_compexpr_right_21 = const_int_pos_2;
                                                                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_21, tmp_compexpr_right_21 );
                                                                        if ( tmp_res == -1 )
                                                                        {
                                                                            assert( ERROR_OCCURRED() );

                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                            exception_lineno = 171;
                                                                            type_description_1 = "oooooo";
                                                                            goto frame_exception_exit_1;
                                                                        }
                                                                        tmp_and_left_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                        tmp_and_left_truth_3 = tmp_and_left_value_3 == NUITKA_BOOL_TRUE ? 1 : 0;
                                                                        if ( tmp_and_left_truth_3 == 1 )
                                                                        {
                                                                            goto and_right_3;
                                                                        }
                                                                        else
                                                                        {
                                                                            goto and_left_3;
                                                                        }
                                                                        and_right_3:;
                                                                        CHECK_OBJECT( par_attrs );
                                                                        tmp_len_arg_3 = par_attrs;
                                                                        tmp_compexpr_left_22 = BUILTIN_LEN( tmp_len_arg_3 );
                                                                        assert( !(tmp_compexpr_left_22 == NULL) );
                                                                        tmp_compexpr_right_22 = const_int_pos_3;
                                                                        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_22, tmp_compexpr_right_22 );
                                                                        Py_DECREF( tmp_compexpr_left_22 );
                                                                        assert( !(tmp_res == -1) );
                                                                        tmp_and_right_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                        tmp_condition_result_22 = tmp_and_right_value_3;
                                                                        goto and_end_3;
                                                                        and_left_3:;
                                                                        tmp_condition_result_22 = tmp_and_left_value_3;
                                                                        and_end_3:;
                                                                        if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
                                                                        {
                                                                            goto branch_yes_22;
                                                                        }
                                                                        else
                                                                        {
                                                                            goto branch_no_22;
                                                                        }
                                                                        branch_yes_22:;
                                                                        {
                                                                            PyObject *tmp_assign_source_7;
                                                                            tmp_assign_source_7 = Py_True;
                                                                            {
                                                                                PyObject *old = tmp_try_except_1__unhandled_indicator;
                                                                                tmp_try_except_1__unhandled_indicator = tmp_assign_source_7;
                                                                                Py_INCREF( tmp_try_except_1__unhandled_indicator );
                                                                                Py_XDECREF( old );
                                                                            }

                                                                        }
                                                                        // Tried code:
                                                                        // Tried code:
                                                                        {
                                                                            PyObject *tmp_assign_source_8;
                                                                            PyObject *tmp_left_name_1;
                                                                            PyObject *tmp_right_name_1;
                                                                            PyObject *tmp_tuple_element_1;
                                                                            PyObject *tmp_called_instance_7;
                                                                            PyObject *tmp_called_instance_8;
                                                                            PyObject *tmp_called_instance_9;
                                                                            tmp_left_name_1 = const_str_digest_b72cceb46cd0996b2283bd0bd2da08a0;
                                                                            CHECK_OBJECT( par_attrs );
                                                                            tmp_called_instance_7 = par_attrs;
                                                                            frame_9f13e85c4c17356e845f140da8d2e279->m_frame.f_lineno = 174;
                                                                            tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_pop );
                                                                            if ( tmp_tuple_element_1 == NULL )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                exception_lineno = 174;
                                                                                type_description_1 = "oooooo";
                                                                                goto try_except_handler_3;
                                                                            }
                                                                            tmp_right_name_1 = PyTuple_New( 3 );
                                                                            PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
                                                                            CHECK_OBJECT( par_attrs );
                                                                            tmp_called_instance_8 = par_attrs;
                                                                            frame_9f13e85c4c17356e845f140da8d2e279->m_frame.f_lineno = 174;
                                                                            tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_pop );
                                                                            if ( tmp_tuple_element_1 == NULL )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                                Py_DECREF( tmp_right_name_1 );

                                                                                exception_lineno = 174;
                                                                                type_description_1 = "oooooo";
                                                                                goto try_except_handler_3;
                                                                            }
                                                                            PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
                                                                            CHECK_OBJECT( par_attrs );
                                                                            tmp_called_instance_9 = par_attrs;
                                                                            frame_9f13e85c4c17356e845f140da8d2e279->m_frame.f_lineno = 174;
                                                                            tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_pop );
                                                                            if ( tmp_tuple_element_1 == NULL )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                                                                Py_DECREF( tmp_right_name_1 );

                                                                                exception_lineno = 174;
                                                                                type_description_1 = "oooooo";
                                                                                goto try_except_handler_3;
                                                                            }
                                                                            PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
                                                                            tmp_assign_source_8 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                                                                            Py_DECREF( tmp_right_name_1 );
                                                                            if ( tmp_assign_source_8 == NULL )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                exception_lineno = 173;
                                                                                type_description_1 = "oooooo";
                                                                                goto try_except_handler_3;
                                                                            }
                                                                            {
                                                                                PyObject *old = var_color_str;
                                                                                var_color_str = tmp_assign_source_8;
                                                                                Py_XDECREF( old );
                                                                            }

                                                                        }
                                                                        goto try_end_1;
                                                                        // Exception handler code:
                                                                        try_except_handler_3:;
                                                                        exception_keeper_type_1 = exception_type;
                                                                        exception_keeper_value_1 = exception_value;
                                                                        exception_keeper_tb_1 = exception_tb;
                                                                        exception_keeper_lineno_1 = exception_lineno;
                                                                        exception_type = NULL;
                                                                        exception_value = NULL;
                                                                        exception_tb = NULL;
                                                                        exception_lineno = 0;

                                                                        {
                                                                            PyObject *tmp_assign_source_9;
                                                                            tmp_assign_source_9 = Py_False;
                                                                            {
                                                                                PyObject *old = tmp_try_except_1__unhandled_indicator;
                                                                                assert( old != NULL );
                                                                                tmp_try_except_1__unhandled_indicator = tmp_assign_source_9;
                                                                                Py_INCREF( tmp_try_except_1__unhandled_indicator );
                                                                                Py_DECREF( old );
                                                                            }

                                                                        }
                                                                        // Preserve existing published exception.
                                                                        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
                                                                        Py_XINCREF( exception_preserved_type_1 );
                                                                        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
                                                                        Py_XINCREF( exception_preserved_value_1 );
                                                                        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
                                                                        Py_XINCREF( exception_preserved_tb_1 );

                                                                        if ( exception_keeper_tb_1 == NULL )
                                                                        {
                                                                            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_9f13e85c4c17356e845f140da8d2e279, exception_keeper_lineno_1 );
                                                                        }
                                                                        else if ( exception_keeper_lineno_1 != 0 )
                                                                        {
                                                                            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_9f13e85c4c17356e845f140da8d2e279, exception_keeper_lineno_1 );
                                                                        }

                                                                        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
                                                                        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
                                                                        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
                                                                        // Tried code:
                                                                        {
                                                                            nuitka_bool tmp_condition_result_23;
                                                                            PyObject *tmp_operand_name_4;
                                                                            PyObject *tmp_compexpr_left_23;
                                                                            PyObject *tmp_compexpr_right_23;
                                                                            tmp_compexpr_left_23 = EXC_TYPE(PyThreadState_GET());
                                                                            tmp_compexpr_right_23 = PyExc_IndexError;
                                                                            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_23, tmp_compexpr_right_23 );
                                                                            if ( tmp_res == -1 )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                exception_lineno = 175;
                                                                                type_description_1 = "oooooo";
                                                                                goto try_except_handler_4;
                                                                            }
                                                                            tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
                                                                            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
                                                                            if ( tmp_res == -1 )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                exception_lineno = 175;
                                                                                type_description_1 = "oooooo";
                                                                                goto try_except_handler_4;
                                                                            }
                                                                            tmp_condition_result_23 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                            if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
                                                                            {
                                                                                goto branch_yes_23;
                                                                            }
                                                                            else
                                                                            {
                                                                                goto branch_no_23;
                                                                            }
                                                                            branch_yes_23:;
                                                                            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                            if (unlikely( tmp_result == false ))
                                                                            {
                                                                                exception_lineno = 172;
                                                                            }

                                                                            if (exception_tb && exception_tb->tb_frame == &frame_9f13e85c4c17356e845f140da8d2e279->m_frame) frame_9f13e85c4c17356e845f140da8d2e279->m_frame.f_lineno = exception_tb->tb_lineno;
                                                                            type_description_1 = "oooooo";
                                                                            goto try_except_handler_4;
                                                                            branch_no_23:;
                                                                        }
                                                                        goto try_end_2;
                                                                        // Exception handler code:
                                                                        try_except_handler_4:;
                                                                        exception_keeper_type_2 = exception_type;
                                                                        exception_keeper_value_2 = exception_value;
                                                                        exception_keeper_tb_2 = exception_tb;
                                                                        exception_keeper_lineno_2 = exception_lineno;
                                                                        exception_type = NULL;
                                                                        exception_value = NULL;
                                                                        exception_tb = NULL;
                                                                        exception_lineno = 0;

                                                                        // Restore previous exception.
                                                                        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
                                                                        // Re-raise.
                                                                        exception_type = exception_keeper_type_2;
                                                                        exception_value = exception_keeper_value_2;
                                                                        exception_tb = exception_keeper_tb_2;
                                                                        exception_lineno = exception_keeper_lineno_2;

                                                                        goto try_except_handler_2;
                                                                        // End of try:
                                                                        try_end_2:;
                                                                        // Restore previous exception.
                                                                        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
                                                                        goto try_end_1;
                                                                        // exception handler codes exits in all cases
                                                                        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_3__select_graphic_rendition );
                                                                        return NULL;
                                                                        // End of try:
                                                                        try_end_1:;
                                                                        {
                                                                            nuitka_bool tmp_condition_result_24;
                                                                            nuitka_bool tmp_compexpr_left_24;
                                                                            nuitka_bool tmp_compexpr_right_24;
                                                                            int tmp_truth_name_1;
                                                                            CHECK_OBJECT( tmp_try_except_1__unhandled_indicator );
                                                                            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_try_except_1__unhandled_indicator );
                                                                            if ( tmp_truth_name_1 == -1 )
                                                                            {
                                                                                assert( ERROR_OCCURRED() );

                                                                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                exception_lineno = 172;
                                                                                type_description_1 = "oooooo";
                                                                                goto try_except_handler_2;
                                                                            }
                                                                            tmp_compexpr_left_24 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                            tmp_compexpr_right_24 = NUITKA_BOOL_TRUE;
                                                                            tmp_condition_result_24 = ( tmp_compexpr_left_24 == tmp_compexpr_right_24 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                            if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
                                                                            {
                                                                                goto branch_yes_24;
                                                                            }
                                                                            else
                                                                            {
                                                                                goto branch_no_24;
                                                                            }
                                                                            branch_yes_24:;
                                                                            {
                                                                                nuitka_bool tmp_condition_result_25;
                                                                                PyObject *tmp_compexpr_left_25;
                                                                                PyObject *tmp_compexpr_right_25;
                                                                                CHECK_OBJECT( var_attr );
                                                                                tmp_compexpr_left_25 = var_attr;
                                                                                tmp_compexpr_right_25 = const_int_pos_38;
                                                                                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_25, tmp_compexpr_right_25 );
                                                                                if ( tmp_res == -1 )
                                                                                {
                                                                                    assert( ERROR_OCCURRED() );

                                                                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                    exception_lineno = 178;
                                                                                    type_description_1 = "oooooo";
                                                                                    goto try_except_handler_2;
                                                                                }
                                                                                tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                                if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
                                                                                {
                                                                                    goto branch_yes_25;
                                                                                }
                                                                                else
                                                                                {
                                                                                    goto branch_no_25;
                                                                                }
                                                                                branch_yes_25:;
                                                                                {
                                                                                    PyObject *tmp_assattr_name_25;
                                                                                    PyObject *tmp_assattr_target_25;
                                                                                    if ( var_color_str == NULL )
                                                                                    {

                                                                                        exception_type = PyExc_UnboundLocalError;
                                                                                        Py_INCREF( exception_type );
                                                                                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "color_str" );
                                                                                        exception_tb = NULL;
                                                                                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                                        CHAIN_EXCEPTION( exception_value );

                                                                                        exception_lineno = 179;
                                                                                        type_description_1 = "oooooo";
                                                                                        goto try_except_handler_2;
                                                                                    }

                                                                                    tmp_assattr_name_25 = var_color_str;
                                                                                    CHECK_OBJECT( par_self );
                                                                                    tmp_assattr_target_25 = par_self;
                                                                                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_25, const_str_plain__color, tmp_assattr_name_25 );
                                                                                    if ( tmp_result == false )
                                                                                    {
                                                                                        assert( ERROR_OCCURRED() );

                                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                        exception_lineno = 179;
                                                                                        type_description_1 = "oooooo";
                                                                                        goto try_except_handler_2;
                                                                                    }
                                                                                }
                                                                                goto branch_end_25;
                                                                                branch_no_25:;
                                                                                {
                                                                                    nuitka_bool tmp_condition_result_26;
                                                                                    PyObject *tmp_compexpr_left_26;
                                                                                    PyObject *tmp_compexpr_right_26;
                                                                                    CHECK_OBJECT( var_attr );
                                                                                    tmp_compexpr_left_26 = var_attr;
                                                                                    tmp_compexpr_right_26 = const_int_pos_48;
                                                                                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_26, tmp_compexpr_right_26 );
                                                                                    if ( tmp_res == -1 )
                                                                                    {
                                                                                        assert( ERROR_OCCURRED() );

                                                                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                        exception_lineno = 180;
                                                                                        type_description_1 = "oooooo";
                                                                                        goto try_except_handler_2;
                                                                                    }
                                                                                    tmp_condition_result_26 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                                                                    if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
                                                                                    {
                                                                                        goto branch_yes_26;
                                                                                    }
                                                                                    else
                                                                                    {
                                                                                        goto branch_no_26;
                                                                                    }
                                                                                    branch_yes_26:;
                                                                                    {
                                                                                        PyObject *tmp_assattr_name_26;
                                                                                        PyObject *tmp_assattr_target_26;
                                                                                        if ( var_color_str == NULL )
                                                                                        {

                                                                                            exception_type = PyExc_UnboundLocalError;
                                                                                            Py_INCREF( exception_type );
                                                                                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "color_str" );
                                                                                            exception_tb = NULL;
                                                                                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                                                                            CHAIN_EXCEPTION( exception_value );

                                                                                            exception_lineno = 181;
                                                                                            type_description_1 = "oooooo";
                                                                                            goto try_except_handler_2;
                                                                                        }

                                                                                        tmp_assattr_name_26 = var_color_str;
                                                                                        CHECK_OBJECT( par_self );
                                                                                        tmp_assattr_target_26 = par_self;
                                                                                        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_26, const_str_plain__bgcolor, tmp_assattr_name_26 );
                                                                                        if ( tmp_result == false )
                                                                                        {
                                                                                            assert( ERROR_OCCURRED() );

                                                                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                                                                            exception_lineno = 181;
                                                                                            type_description_1 = "oooooo";
                                                                                            goto try_except_handler_2;
                                                                                        }
                                                                                    }
                                                                                    branch_no_26:;
                                                                                }
                                                                                branch_end_25:;
                                                                            }
                                                                            branch_no_24:;
                                                                        }
                                                                        goto try_end_3;
                                                                        // Exception handler code:
                                                                        try_except_handler_2:;
                                                                        exception_keeper_type_3 = exception_type;
                                                                        exception_keeper_value_3 = exception_value;
                                                                        exception_keeper_tb_3 = exception_tb;
                                                                        exception_keeper_lineno_3 = exception_lineno;
                                                                        exception_type = NULL;
                                                                        exception_value = NULL;
                                                                        exception_tb = NULL;
                                                                        exception_lineno = 0;

                                                                        Py_XDECREF( tmp_try_except_1__unhandled_indicator );
                                                                        tmp_try_except_1__unhandled_indicator = NULL;

                                                                        // Re-raise.
                                                                        exception_type = exception_keeper_type_3;
                                                                        exception_value = exception_keeper_value_3;
                                                                        exception_tb = exception_keeper_tb_3;
                                                                        exception_lineno = exception_keeper_lineno_3;

                                                                        goto frame_exception_exit_1;
                                                                        // End of try:
                                                                        try_end_3:;
                                                                        CHECK_OBJECT( (PyObject *)tmp_try_except_1__unhandled_indicator );
                                                                        Py_DECREF( tmp_try_except_1__unhandled_indicator );
                                                                        tmp_try_except_1__unhandled_indicator = NULL;

                                                                        branch_no_22:;
                                                                    }
                                                                    branch_no_18:;
                                                                }
                                                                branch_end_17:;
                                                            }
                                                            branch_end_16:;
                                                        }
                                                        branch_end_15:;
                                                    }
                                                    branch_end_14:;
                                                }
                                                branch_end_13:;
                                            }
                                            branch_end_12:;
                                        }
                                        branch_end_11:;
                                    }
                                    branch_end_10:;
                                }
                                branch_end_9:;
                            }
                            branch_end_8:;
                        }
                        branch_end_7:;
                    }
                    branch_end_6:;
                }
                branch_end_5:;
            }
            branch_end_4:;
        }
        branch_end_3:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 117;
        type_description_1 = "oooooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9f13e85c4c17356e845f140da8d2e279 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9f13e85c4c17356e845f140da8d2e279 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9f13e85c4c17356e845f140da8d2e279, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9f13e85c4c17356e845f140da8d2e279->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9f13e85c4c17356e845f140da8d2e279, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9f13e85c4c17356e845f140da8d2e279,
        type_description_1,
        par_self,
        par_attrs,
        var_attr,
        var_n,
        var_m,
        var_color_str
    );


    // Release cached frame.
    if ( frame_9f13e85c4c17356e845f140da8d2e279 == cache_frame_9f13e85c4c17356e845f140da8d2e279 )
    {
        Py_DECREF( frame_9f13e85c4c17356e845f140da8d2e279 );
    }
    cache_frame_9f13e85c4c17356e845f140da8d2e279 = NULL;

    assertFrameObject( frame_9f13e85c4c17356e845f140da8d2e279 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_3__select_graphic_rendition );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attrs );
    Py_DECREF( par_attrs );
    par_attrs = NULL;

    Py_XDECREF( var_attr );
    var_attr = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_color_str );
    var_color_str = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_attrs );
    par_attrs = NULL;

    Py_XDECREF( var_attr );
    var_attr = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_color_str );
    var_color_str = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_3__select_graphic_rendition );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$formatted_text$ansi$$$function_4__create_style_string( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_result = NULL;
    struct Nuitka_FrameObject *frame_0c1cd473cb44c9d02130dab558906431;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_0c1cd473cb44c9d02130dab558906431 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_result == NULL );
        var_result = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0c1cd473cb44c9d02130dab558906431, codeobj_0c1cd473cb44c9d02130dab558906431, module_prompt_toolkit$formatted_text$ansi, sizeof(void *)+sizeof(void *) );
    frame_0c1cd473cb44c9d02130dab558906431 = cache_frame_0c1cd473cb44c9d02130dab558906431;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0c1cd473cb44c9d02130dab558906431 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0c1cd473cb44c9d02130dab558906431 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__color );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 188;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( var_result );
            tmp_source_name_2 = var_result;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_append );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__color );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 189;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_0c1cd473cb44c9d02130dab558906431->m_frame.f_lineno = 189;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_attribute_value_2;
        int tmp_truth_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_attribute_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__bgcolor );
        if ( tmp_attribute_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 190;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_2 );

            exception_lineno = 190;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( var_result );
            tmp_source_name_5 = var_result;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_append );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 191;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_1 = const_str_digest_3a832cd0dd716d9abfb3e37fb4302ef8;
            CHECK_OBJECT( par_self );
            tmp_source_name_6 = par_self;
            tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain__bgcolor );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 191;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_2 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 191;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_0c1cd473cb44c9d02130dab558906431->m_frame.f_lineno = 191;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 191;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_source_name_7;
        PyObject *tmp_attribute_value_3;
        int tmp_truth_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_attribute_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain__bold );
        if ( tmp_attribute_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_attribute_value_3 );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_3 );

            exception_lineno = 192;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_3 );
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_3;
            CHECK_OBJECT( var_result );
            tmp_called_instance_1 = var_result;
            frame_0c1cd473cb44c9d02130dab558906431->m_frame.f_lineno = 193;
            tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_plain_bold_tuple, 0 ) );

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 193;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_8;
        PyObject *tmp_attribute_value_4;
        int tmp_truth_name_4;
        CHECK_OBJECT( par_self );
        tmp_source_name_8 = par_self;
        tmp_attribute_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain__underline );
        if ( tmp_attribute_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_attribute_value_4 );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_4 );

            exception_lineno = 194;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_4 );
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_4;
            CHECK_OBJECT( var_result );
            tmp_called_instance_2 = var_result;
            frame_0c1cd473cb44c9d02130dab558906431->m_frame.f_lineno = 195;
            tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_plain_underline_tuple, 0 ) );

            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 195;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        branch_no_4:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_9;
        PyObject *tmp_attribute_value_5;
        int tmp_truth_name_5;
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_attribute_value_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain__italic );
        if ( tmp_attribute_value_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_5 = CHECK_IF_TRUE( tmp_attribute_value_5 );
        if ( tmp_truth_name_5 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_5 );

            exception_lineno = 196;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_5 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_5 );
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_call_result_5;
            CHECK_OBJECT( var_result );
            tmp_called_instance_3 = var_result;
            frame_0c1cd473cb44c9d02130dab558906431->m_frame.f_lineno = 197;
            tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_plain_italic_tuple, 0 ) );

            if ( tmp_call_result_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 197;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_5 );
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_source_name_10;
        PyObject *tmp_attribute_value_6;
        int tmp_truth_name_6;
        CHECK_OBJECT( par_self );
        tmp_source_name_10 = par_self;
        tmp_attribute_value_6 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain__blink );
        if ( tmp_attribute_value_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_6 = CHECK_IF_TRUE( tmp_attribute_value_6 );
        if ( tmp_truth_name_6 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_6 );

            exception_lineno = 198;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_6 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_6 );
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_called_instance_4;
            PyObject *tmp_call_result_6;
            CHECK_OBJECT( var_result );
            tmp_called_instance_4 = var_result;
            frame_0c1cd473cb44c9d02130dab558906431->m_frame.f_lineno = 199;
            tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_plain_blink_tuple, 0 ) );

            if ( tmp_call_result_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 199;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_6 );
        }
        branch_no_6:;
    }
    {
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_source_name_11;
        PyObject *tmp_attribute_value_7;
        int tmp_truth_name_7;
        CHECK_OBJECT( par_self );
        tmp_source_name_11 = par_self;
        tmp_attribute_value_7 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain__reverse );
        if ( tmp_attribute_value_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 200;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_7 = CHECK_IF_TRUE( tmp_attribute_value_7 );
        if ( tmp_truth_name_7 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_7 );

            exception_lineno = 200;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_7 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_7 );
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_called_instance_5;
            PyObject *tmp_call_result_7;
            CHECK_OBJECT( var_result );
            tmp_called_instance_5 = var_result;
            frame_0c1cd473cb44c9d02130dab558906431->m_frame.f_lineno = 201;
            tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_plain_reverse_tuple, 0 ) );

            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 201;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        branch_no_7:;
    }
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_source_name_12;
        PyObject *tmp_attribute_value_8;
        int tmp_truth_name_8;
        CHECK_OBJECT( par_self );
        tmp_source_name_12 = par_self;
        tmp_attribute_value_8 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain__hidden );
        if ( tmp_attribute_value_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_8 = CHECK_IF_TRUE( tmp_attribute_value_8 );
        if ( tmp_truth_name_8 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_8 );

            exception_lineno = 202;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = tmp_truth_name_8 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_8 );
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            PyObject *tmp_called_instance_6;
            PyObject *tmp_call_result_8;
            CHECK_OBJECT( var_result );
            tmp_called_instance_6 = var_result;
            frame_0c1cd473cb44c9d02130dab558906431->m_frame.f_lineno = 203;
            tmp_call_result_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_plain_hidden_tuple, 0 ) );

            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 203;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        branch_no_8:;
    }
    {
        PyObject *tmp_called_instance_7;
        PyObject *tmp_args_element_name_3;
        tmp_called_instance_7 = const_str_space;
        CHECK_OBJECT( var_result );
        tmp_args_element_name_3 = var_result;
        frame_0c1cd473cb44c9d02130dab558906431->m_frame.f_lineno = 205;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_join, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 205;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c1cd473cb44c9d02130dab558906431 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c1cd473cb44c9d02130dab558906431 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0c1cd473cb44c9d02130dab558906431 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0c1cd473cb44c9d02130dab558906431, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0c1cd473cb44c9d02130dab558906431->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0c1cd473cb44c9d02130dab558906431, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0c1cd473cb44c9d02130dab558906431,
        type_description_1,
        par_self,
        var_result
    );


    // Release cached frame.
    if ( frame_0c1cd473cb44c9d02130dab558906431 == cache_frame_0c1cd473cb44c9d02130dab558906431 )
    {
        Py_DECREF( frame_0c1cd473cb44c9d02130dab558906431 );
    }
    cache_frame_0c1cd473cb44c9d02130dab558906431 = NULL;

    assertFrameObject( frame_0c1cd473cb44c9d02130dab558906431 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_4__create_style_string );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_result );
    Py_DECREF( var_result );
    var_result = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_4__create_style_string );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$formatted_text$ansi$$$function_5___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_e6b1cc48a0f8640329ed1cf351efa147;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e6b1cc48a0f8640329ed1cf351efa147 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e6b1cc48a0f8640329ed1cf351efa147, codeobj_e6b1cc48a0f8640329ed1cf351efa147, module_prompt_toolkit$formatted_text$ansi, sizeof(void *) );
    frame_e6b1cc48a0f8640329ed1cf351efa147 = cache_frame_e6b1cc48a0f8640329ed1cf351efa147;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e6b1cc48a0f8640329ed1cf351efa147 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e6b1cc48a0f8640329ed1cf351efa147 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        tmp_left_name_1 = const_str_digest_0ab5784fd7b2a64c58528a23715c7240;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_value );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e6b1cc48a0f8640329ed1cf351efa147 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e6b1cc48a0f8640329ed1cf351efa147 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e6b1cc48a0f8640329ed1cf351efa147 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e6b1cc48a0f8640329ed1cf351efa147, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e6b1cc48a0f8640329ed1cf351efa147->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e6b1cc48a0f8640329ed1cf351efa147, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e6b1cc48a0f8640329ed1cf351efa147,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_e6b1cc48a0f8640329ed1cf351efa147 == cache_frame_e6b1cc48a0f8640329ed1cf351efa147 )
    {
        Py_DECREF( frame_e6b1cc48a0f8640329ed1cf351efa147 );
    }
    cache_frame_e6b1cc48a0f8640329ed1cf351efa147 = NULL;

    assertFrameObject( frame_e6b1cc48a0f8640329ed1cf351efa147 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_5___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_5___repr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$formatted_text$ansi$$$function_6___pt_formatted_text__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9a51e847a75393bdd72aca172d443461;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9a51e847a75393bdd72aca172d443461 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9a51e847a75393bdd72aca172d443461, codeobj_9a51e847a75393bdd72aca172d443461, module_prompt_toolkit$formatted_text$ansi, sizeof(void *) );
    frame_9a51e847a75393bdd72aca172d443461 = cache_frame_9a51e847a75393bdd72aca172d443461;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9a51e847a75393bdd72aca172d443461 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9a51e847a75393bdd72aca172d443461 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__formatted_text );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 211;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a51e847a75393bdd72aca172d443461 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a51e847a75393bdd72aca172d443461 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a51e847a75393bdd72aca172d443461 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9a51e847a75393bdd72aca172d443461, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9a51e847a75393bdd72aca172d443461->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9a51e847a75393bdd72aca172d443461, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9a51e847a75393bdd72aca172d443461,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_9a51e847a75393bdd72aca172d443461 == cache_frame_9a51e847a75393bdd72aca172d443461 )
    {
        Py_DECREF( frame_9a51e847a75393bdd72aca172d443461 );
    }
    cache_frame_9a51e847a75393bdd72aca172d443461 = NULL;

    assertFrameObject( frame_9a51e847a75393bdd72aca172d443461 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_6___pt_formatted_text__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_6___pt_formatted_text__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$formatted_text$ansi$$$function_7_format( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *outline_0_var_a = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_a854c917057514c96ce50e373f50ad14;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_03531dc0e520b39442b12a3759f6a75f_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_03531dc0e520b39442b12a3759f6a75f_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_a854c917057514c96ce50e373f50ad14 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a854c917057514c96ce50e373f50ad14, codeobj_a854c917057514c96ce50e373f50ad14, module_prompt_toolkit$formatted_text$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a854c917057514c96ce50e373f50ad14 = cache_frame_a854c917057514c96ce50e373f50ad14;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a854c917057514c96ce50e373f50ad14 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a854c917057514c96ce50e373f50ad14 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_args );
            tmp_iter_arg_1 = par_args;
            tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 219;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            assert( tmp_listcomp_1__$0 == NULL );
            tmp_listcomp_1__$0 = tmp_assign_source_2;
        }
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = PyList_New( 0 );
            assert( tmp_listcomp_1__contraction == NULL );
            tmp_listcomp_1__contraction = tmp_assign_source_3;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_03531dc0e520b39442b12a3759f6a75f_2, codeobj_03531dc0e520b39442b12a3759f6a75f, module_prompt_toolkit$formatted_text$ansi, sizeof(void *) );
        frame_03531dc0e520b39442b12a3759f6a75f_2 = cache_frame_03531dc0e520b39442b12a3759f6a75f_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_03531dc0e520b39442b12a3759f6a75f_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_03531dc0e520b39442b12a3759f6a75f_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( tmp_listcomp_1__$0 );
            tmp_next_source_1 = tmp_listcomp_1__$0;
            tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 219;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = tmp_listcomp_1__iter_value_0;
                tmp_listcomp_1__iter_value_0 = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_5 = tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = outline_0_var_a;
                outline_0_var_a = tmp_assign_source_5;
                Py_INCREF( outline_0_var_a );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_append_list_1 = tmp_listcomp_1__contraction;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_ansi_escape );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ansi_escape );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ansi_escape" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 219;
                type_description_2 = "o";
                goto try_except_handler_3;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( outline_0_var_a );
            tmp_args_element_name_1 = outline_0_var_a;
            frame_03531dc0e520b39442b12a3759f6a75f_2->m_frame.f_lineno = 219;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 219;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 219;
                type_description_2 = "o";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;
            type_description_2 = "o";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_assign_source_1 = tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_1 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_7_format );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
        Py_DECREF( tmp_listcomp_1__$0 );
        tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
        Py_DECREF( tmp_listcomp_1__contraction );
        tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( tmp_listcomp_1__iter_value_0 );
        tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_03531dc0e520b39442b12a3759f6a75f_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_2:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_03531dc0e520b39442b12a3759f6a75f_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_03531dc0e520b39442b12a3759f6a75f_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_03531dc0e520b39442b12a3759f6a75f_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_03531dc0e520b39442b12a3759f6a75f_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_03531dc0e520b39442b12a3759f6a75f_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_03531dc0e520b39442b12a3759f6a75f_2,
            type_description_2,
            outline_0_var_a
        );


        // Release cached frame.
        if ( frame_03531dc0e520b39442b12a3759f6a75f_2 == cache_frame_03531dc0e520b39442b12a3759f6a75f_2 )
        {
            Py_DECREF( frame_03531dc0e520b39442b12a3759f6a75f_2 );
        }
        cache_frame_03531dc0e520b39442b12a3759f6a75f_2 = NULL;

        assertFrameObject( frame_03531dc0e520b39442b12a3759f6a75f_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        type_description_1 = "ooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_7_format );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( outline_0_var_a );
        outline_0_var_a = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_0_var_a );
        outline_0_var_a = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_7_format );
        return NULL;
        outline_exception_1:;
        exception_lineno = 219;
        goto frame_exception_exit_1;
        outline_result_1:;
        {
            PyObject *old = par_args;
            assert( old != NULL );
            par_args = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_dict_seq_1;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_iter_arg_2;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_kwargs );
            tmp_called_instance_1 = par_kwargs;
            frame_a854c917057514c96ce50e373f50ad14->m_frame.f_lineno = 220;
            tmp_iter_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
            if ( tmp_iter_arg_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 220;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_2 );
            Py_DECREF( tmp_iter_arg_2 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 220;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_7;
        }
        // Tried code:
        tmp_dict_seq_1 = prompt_toolkit$formatted_text$ansi$$$function_7_format$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_dict_seq_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_7_format );
        return NULL;
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_2;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_7_format );
        return NULL;
        outline_result_2:;
        tmp_assign_source_6 = TO_DICT( tmp_dict_seq_1, NULL );
        Py_DECREF( tmp_dict_seq_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 220;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_kwargs;
            assert( old != NULL );
            par_kwargs = tmp_assign_source_6;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_ANSI );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ANSI );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ANSI" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 222;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_value );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_args_element_name_2 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_a854c917057514c96ce50e373f50ad14->m_frame.f_lineno = 222;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 222;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a854c917057514c96ce50e373f50ad14 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a854c917057514c96ce50e373f50ad14 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a854c917057514c96ce50e373f50ad14 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a854c917057514c96ce50e373f50ad14, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a854c917057514c96ce50e373f50ad14->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a854c917057514c96ce50e373f50ad14, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a854c917057514c96ce50e373f50ad14,
        type_description_1,
        par_self,
        par_args,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_a854c917057514c96ce50e373f50ad14 == cache_frame_a854c917057514c96ce50e373f50ad14 )
    {
        Py_DECREF( frame_a854c917057514c96ce50e373f50ad14 );
    }
    cache_frame_a854c917057514c96ce50e373f50ad14 = NULL;

    assertFrameObject( frame_a854c917057514c96ce50e373f50ad14 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_7_format );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_7_format );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct prompt_toolkit$formatted_text$ansi$$$function_7_format$$$genexpr_1_genexpr_locals {
    PyObject *var_k;
    PyObject *var_v;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *prompt_toolkit$formatted_text$ansi$$$function_7_format$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$formatted_text$ansi$$$function_7_format$$$genexpr_1_genexpr_locals *generator_heap = (struct prompt_toolkit$formatted_text$ansi$$$function_7_format$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_k = NULL;
    generator_heap->var_v = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_f7b7c615d0ae33f0a18541d673392a58, module_prompt_toolkit$formatted_text$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noo";
                generator_heap->exception_lineno = 220;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_iter_arg_1 = generator_heap->tmp_iter_value_0;
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 220;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 220;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 220;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "Noo";
                    generator_heap->exception_lineno = 220;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 220;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_k;
            generator_heap->var_k = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_k );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_v;
            generator_heap->var_v = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_v );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_k );
        tmp_tuple_element_1 = generator_heap->var_k;
        tmp_expression_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_ansi_escape );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ansi_escape );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ansi_escape" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 220;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_v );
        tmp_args_element_name_1 = generator_heap->var_v;
        generator->m_frame->m_frame.f_lineno = 220;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_expression_name_1 );

            generator_heap->exception_lineno = 220;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 220;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 220;
        generator_heap->type_description_1 = "Noo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_k,
            generator_heap->var_v
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    Py_XDECREF( generator_heap->var_v );
    generator_heap->var_v = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    Py_XDECREF( generator_heap->var_v );
    generator_heap->var_v = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *prompt_toolkit$formatted_text$ansi$$$function_7_format$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$formatted_text$ansi$$$function_7_format$$$genexpr_1_genexpr_context,
        module_prompt_toolkit$formatted_text$ansi,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_111b86be98ea03850a57e8268db68e1b,
#endif
        codeobj_f7b7c615d0ae33f0a18541d673392a58,
        1,
        sizeof(struct prompt_toolkit$formatted_text$ansi$$$function_7_format$$$genexpr_1_genexpr_locals)
    );
}



struct prompt_toolkit$formatted_text$ansi$$$genexpr_1_genexpr_locals {
    PyObject *var_k;
    PyObject *var_v;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *prompt_toolkit$formatted_text$ansi$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$formatted_text$ansi$$$genexpr_1_genexpr_locals *generator_heap = (struct prompt_toolkit$formatted_text$ansi$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_k = NULL;
    generator_heap->var_v = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_89394e0f8616c4174399f1d93aca4498, module_prompt_toolkit$formatted_text$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noo";
                generator_heap->exception_lineno = 226;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_iter_arg_1 = generator_heap->tmp_iter_value_0;
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 226;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 226;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 226;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "Noo";
                    generator_heap->exception_lineno = 226;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 226;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_k;
            generator_heap->var_k = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_k );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_v;
            generator_heap->var_v = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_v );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_v );
        tmp_tuple_element_1 = generator_heap->var_v;
        tmp_expression_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( generator_heap->var_k );
        tmp_tuple_element_1 = generator_heap->var_k;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 226;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 226;
        generator_heap->type_description_1 = "Noo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_k,
            generator_heap->var_v
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    Py_XDECREF( generator_heap->var_v );
    generator_heap->var_v = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    Py_XDECREF( generator_heap->var_v );
    generator_heap->var_v = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *prompt_toolkit$formatted_text$ansi$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$formatted_text$ansi$$$genexpr_1_genexpr_context,
        module_prompt_toolkit$formatted_text$ansi,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        NULL,
#endif
        codeobj_89394e0f8616c4174399f1d93aca4498,
        1,
        sizeof(struct prompt_toolkit$formatted_text$ansi$$$genexpr_1_genexpr_locals)
    );
}



struct prompt_toolkit$formatted_text$ansi$$$genexpr_2_genexpr_locals {
    PyObject *var_k;
    PyObject *var_v;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *prompt_toolkit$formatted_text$ansi$$$genexpr_2_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct prompt_toolkit$formatted_text$ansi$$$genexpr_2_genexpr_locals *generator_heap = (struct prompt_toolkit$formatted_text$ansi$$$genexpr_2_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_k = NULL;
    generator_heap->var_v = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_44f6dd968e481914872bf518a0090ac9, module_prompt_toolkit$formatted_text$ansi, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noo";
                generator_heap->exception_lineno = 227;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_iter_arg_1 = generator_heap->tmp_iter_value_0;
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 227;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 227;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 227;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "Noo";
                    generator_heap->exception_lineno = 227;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "Noo";
            generator_heap->exception_lineno = 227;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_k;
            generator_heap->var_k = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_k );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_v;
            generator_heap->var_v = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_v );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_v );
        tmp_tuple_element_1 = generator_heap->var_v;
        tmp_expression_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( generator_heap->var_k );
        tmp_tuple_element_1 = generator_heap->var_k;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 227;
            generator_heap->type_description_1 = "Noo";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 227;
        generator_heap->type_description_1 = "Noo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_k,
            generator_heap->var_v
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    Py_XDECREF( generator_heap->var_v );
    generator_heap->var_v = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_k );
    generator_heap->var_k = NULL;

    Py_XDECREF( generator_heap->var_v );
    generator_heap->var_v = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *prompt_toolkit$formatted_text$ansi$$$genexpr_2_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        prompt_toolkit$formatted_text$ansi$$$genexpr_2_genexpr_context,
        module_prompt_toolkit$formatted_text$ansi,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        NULL,
#endif
        codeobj_44f6dd968e481914872bf518a0090ac9,
        1,
        sizeof(struct prompt_toolkit$formatted_text$ansi$$$genexpr_2_genexpr_locals)
    );
}


static PyObject *impl_prompt_toolkit$formatted_text$ansi$$$function_8_ansi_escape( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_text = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_e4db8ab5ea7342b85318d9371058c6aa;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e4db8ab5ea7342b85318d9371058c6aa = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e4db8ab5ea7342b85318d9371058c6aa, codeobj_e4db8ab5ea7342b85318d9371058c6aa, module_prompt_toolkit$formatted_text$ansi, sizeof(void *) );
    frame_e4db8ab5ea7342b85318d9371058c6aa = cache_frame_e4db8ab5ea7342b85318d9371058c6aa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e4db8ab5ea7342b85318d9371058c6aa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e4db8ab5ea7342b85318d9371058c6aa ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( par_text );
        tmp_called_instance_2 = par_text;
        frame_e4db8ab5ea7342b85318d9371058c6aa->m_frame.f_lineno = 240;
        tmp_called_instance_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_chr_27_str_chr_63_tuple, 0 ) );

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_e4db8ab5ea7342b85318d9371058c6aa->m_frame.f_lineno = 240;
        tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_chr_8_str_chr_63_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4db8ab5ea7342b85318d9371058c6aa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4db8ab5ea7342b85318d9371058c6aa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e4db8ab5ea7342b85318d9371058c6aa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e4db8ab5ea7342b85318d9371058c6aa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e4db8ab5ea7342b85318d9371058c6aa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e4db8ab5ea7342b85318d9371058c6aa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e4db8ab5ea7342b85318d9371058c6aa,
        type_description_1,
        par_text
    );


    // Release cached frame.
    if ( frame_e4db8ab5ea7342b85318d9371058c6aa == cache_frame_e4db8ab5ea7342b85318d9371058c6aa )
    {
        Py_DECREF( frame_e4db8ab5ea7342b85318d9371058c6aa );
    }
    cache_frame_e4db8ab5ea7342b85318d9371058c6aa = NULL;

    assertFrameObject( frame_e4db8ab5ea7342b85318d9371058c6aa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_8_ansi_escape );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_text );
    Py_DECREF( par_text );
    par_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi$$$function_8_ansi_escape );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_1___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$formatted_text$ansi$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_067808a912a6c2aceef20fbd6ef0cd4c,
#endif
        codeobj_eaf2cc95a9fee3905cb03fac33474473,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$formatted_text$ansi,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot,
        const_str_plain__parse_corot,
#if PYTHON_VERSION >= 300
        const_str_digest_961deea024e5668b00b2f477056197e9,
#endif
        codeobj_7a19018b97e3050b7736fb578a18b9b0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$formatted_text$ansi,
        const_str_digest_2b36fb2749562fe8e8384bf4000a08a0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_3__select_graphic_rendition(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$formatted_text$ansi$$$function_3__select_graphic_rendition,
        const_str_plain__select_graphic_rendition,
#if PYTHON_VERSION >= 300
        const_str_digest_61df946a8f6df740fc20a2078b408440,
#endif
        codeobj_9f13e85c4c17356e845f140da8d2e279,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$formatted_text$ansi,
        const_str_digest_eec683951d1cdd1abf685d52499274e2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_4__create_style_string(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$formatted_text$ansi$$$function_4__create_style_string,
        const_str_plain__create_style_string,
#if PYTHON_VERSION >= 300
        const_str_digest_5e14602528d2a10b56554f4188c1bcb1,
#endif
        codeobj_0c1cd473cb44c9d02130dab558906431,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$formatted_text$ansi,
        const_str_digest_8efe3d681c1fb16cef671b890a778cf7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_5___repr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$formatted_text$ansi$$$function_5___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_5635386a497322015b85321566a95aa4,
#endif
        codeobj_e6b1cc48a0f8640329ed1cf351efa147,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$formatted_text$ansi,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_6___pt_formatted_text__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$formatted_text$ansi$$$function_6___pt_formatted_text__,
        const_str_plain___pt_formatted_text__,
#if PYTHON_VERSION >= 300
        const_str_digest_24f55768503de3087ed5e5f9644bfeaa,
#endif
        codeobj_9a51e847a75393bdd72aca172d443461,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$formatted_text$ansi,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_7_format(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$formatted_text$ansi$$$function_7_format,
        const_str_plain_format,
#if PYTHON_VERSION >= 300
        const_str_digest_ecb4aa274594bb5f8ad40169ad0bc512,
#endif
        codeobj_a854c917057514c96ce50e373f50ad14,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$formatted_text$ansi,
        const_str_digest_7c1cb00676fa8ada43b7efa321b12d2b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_8_ansi_escape(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$formatted_text$ansi$$$function_8_ansi_escape,
        const_str_plain_ansi_escape,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e4db8ab5ea7342b85318d9371058c6aa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$formatted_text$ansi,
        const_str_digest_ba75d78e2eadb3e1308752ec44cdc66c,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$formatted_text$ansi =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.formatted_text.ansi",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$formatted_text$ansi)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$formatted_text$ansi)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$formatted_text$ansi );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.formatted_text.ansi: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.formatted_text.ansi: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.formatted_text.ansi: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$formatted_text$ansi" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$formatted_text$ansi = Py_InitModule4(
        "prompt_toolkit.formatted_text.ansi",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$formatted_text$ansi = PyModule_Create( &mdef_prompt_toolkit$formatted_text$ansi );
#endif

    moduledict_prompt_toolkit$formatted_text$ansi = MODULE_DICT( module_prompt_toolkit$formatted_text$ansi );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$formatted_text$ansi,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$formatted_text$ansi,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$formatted_text$ansi,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$formatted_text$ansi,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$formatted_text$ansi );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_545a955e2a188ce6b5985eb1e36021e9, module_prompt_toolkit$formatted_text$ansi );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_genexpr_2__$0 = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__element_3 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    struct Nuitka_FrameObject *frame_4abdfa9cab80d381f439db22986f3336;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_prompt_toolkit$formatted_text$ansi_12 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_215eb4c3a7e1a21826bb183cee6c7d88_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_215eb4c3a7e1a21826bb183cee6c7d88_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_4abdfa9cab80d381f439db22986f3336 = MAKE_MODULE_FRAME( codeobj_4abdfa9cab80d381f439db22986f3336, module_prompt_toolkit$formatted_text$ansi );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_4abdfa9cab80d381f439db22986f3336 );
    assert( Py_REFCNT( frame_4abdfa9cab80d381f439db22986f3336 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_4abdfa9cab80d381f439db22986f3336->m_frame.f_lineno = 1;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_741617e4cb060c7f988d39877ef870e3;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$formatted_text$ansi;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_FG_ANSI_COLORS_str_plain_BG_ANSI_COLORS_tuple;
        tmp_level_name_1 = const_int_0;
        frame_4abdfa9cab80d381f439db22986f3336->m_frame.f_lineno = 2;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_FG_ANSI_COLORS );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_FG_ANSI_COLORS, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_BG_ANSI_COLORS );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_BG_ANSI_COLORS, tmp_assign_source_7 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_741617e4cb060c7f988d39877ef870e3;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$formatted_text$ansi;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain__256_colors_tuple;
        tmp_level_name_2 = const_int_0;
        frame_4abdfa9cab80d381f439db22986f3336->m_frame.f_lineno = 3;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain__256_colors );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__256_colors_table, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_base;
        tmp_globals_name_3 = (PyObject *)moduledict_prompt_toolkit$formatted_text$ansi;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_FormattedText_tuple;
        tmp_level_name_3 = const_int_pos_1;
        frame_4abdfa9cab80d381f439db22986f3336->m_frame.f_lineno = 4;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_5 ) )
        {
           tmp_assign_source_9 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_5,
                (PyObject *)moduledict_prompt_toolkit$formatted_text$ansi,
                const_str_plain_FormattedText,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_FormattedText );
        }

        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_FormattedText, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = LIST_COPY( const_list_str_plain_ANSI_str_plain_ansi_escape_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_10 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_11 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_11;
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_12;
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_13 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_13;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_14;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 12;

                goto try_except_handler_2;
            }
            tmp_tuple_element_1 = const_str_plain_ANSI;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_4abdfa9cab80d381f439db22986f3336->m_frame.f_lineno = 12;
            tmp_assign_source_14 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 12;

                goto try_except_handler_2;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_14;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 12;

                goto try_except_handler_2;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 12;

                    goto try_except_handler_2;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 12;

                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 12;

                    goto try_except_handler_2;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 12;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_2;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_15;
            tmp_assign_source_15 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_15;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_16;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_prompt_toolkit$formatted_text$ansi_12 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_545a955e2a188ce6b5985eb1e36021e9;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_digest_6fe3509a405405eba42928ed2fd3f08d;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_plain_ANSI;
        tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_215eb4c3a7e1a21826bb183cee6c7d88_2, codeobj_215eb4c3a7e1a21826bb183cee6c7d88, module_prompt_toolkit$formatted_text$ansi, sizeof(void *) );
        frame_215eb4c3a7e1a21826bb183cee6c7d88_2 = cache_frame_215eb4c3a7e1a21826bb183cee6c7d88_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_215eb4c3a7e1a21826bb183cee6c7d88_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_215eb4c3a7e1a21826bb183cee6c7d88_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_1___init__(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_2__parse_corot(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain__parse_corot, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_3__select_graphic_rendition(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain__select_graphic_rendition, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_4__create_style_string(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain__create_style_string, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_5___repr__(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_6___pt_formatted_text__(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain___pt_formatted_text__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 210;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_7_format(  );



        tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain_format, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_215eb4c3a7e1a21826bb183cee6c7d88_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_215eb4c3a7e1a21826bb183cee6c7d88_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_215eb4c3a7e1a21826bb183cee6c7d88_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_215eb4c3a7e1a21826bb183cee6c7d88_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_215eb4c3a7e1a21826bb183cee6c7d88_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_215eb4c3a7e1a21826bb183cee6c7d88_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_215eb4c3a7e1a21826bb183cee6c7d88_2 == cache_frame_215eb4c3a7e1a21826bb183cee6c7d88_2 )
        {
            Py_DECREF( frame_215eb4c3a7e1a21826bb183cee6c7d88_2 );
        }
        cache_frame_215eb4c3a7e1a21826bb183cee6c7d88_2 = NULL;

        assertFrameObject( frame_215eb4c3a7e1a21826bb183cee6c7d88_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_4;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 12;

                goto try_except_handler_4;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_prompt_toolkit$formatted_text$ansi_12, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 12;

                goto try_except_handler_4;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_ANSI;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_prompt_toolkit$formatted_text$ansi_12;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_4abdfa9cab80d381f439db22986f3336->m_frame.f_lineno = 12;
            tmp_assign_source_17 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 12;

                goto try_except_handler_4;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_17;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_16 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_16 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        Py_DECREF( locals_prompt_toolkit$formatted_text$ansi_12 );
        locals_prompt_toolkit$formatted_text$ansi_12 = NULL;
        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_prompt_toolkit$formatted_text$ansi_12 );
        locals_prompt_toolkit$formatted_text$ansi_12 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 12;
        goto try_except_handler_2;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_ANSI, tmp_assign_source_16 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_dict_seq_1;
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_3;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_FG_ANSI_COLORS );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FG_ANSI_COLORS );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FG_ANSI_COLORS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 226;

                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_3;
            frame_4abdfa9cab80d381f439db22986f3336->m_frame.f_lineno = 226;
            tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 226;

                goto frame_exception_exit_1;
            }
            tmp_assign_source_19 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 226;

                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_19;
        }
        // Tried code:
        tmp_dict_seq_1 = prompt_toolkit$formatted_text$ansi$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_dict_seq_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_2;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi );
        return MOD_RETURN_VALUE( NULL );
        outline_result_2:;
        tmp_assign_source_18 = TO_DICT( tmp_dict_seq_1, NULL );
        Py_DECREF( tmp_dict_seq_1 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__fg_colors, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_dict_seq_2;
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_iter_arg_2;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_4;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_BG_ANSI_COLORS );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BG_ANSI_COLORS );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BG_ANSI_COLORS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 227;

                goto frame_exception_exit_1;
            }

            tmp_called_instance_2 = tmp_mvar_value_4;
            frame_4abdfa9cab80d381f439db22986f3336->m_frame.f_lineno = 227;
            tmp_iter_arg_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_items );
            if ( tmp_iter_arg_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 227;

                goto frame_exception_exit_1;
            }
            tmp_assign_source_21 = MAKE_ITERATOR( tmp_iter_arg_2 );
            Py_DECREF( tmp_iter_arg_2 );
            if ( tmp_assign_source_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 227;

                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_2__$0 == NULL );
            tmp_genexpr_2__$0 = tmp_assign_source_21;
        }
        // Tried code:
        tmp_dict_seq_2 = prompt_toolkit$formatted_text$ansi$$$genexpr_2_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_dict_seq_2)->m_closure[0] = PyCell_NEW0( tmp_genexpr_2__$0 );


        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
        Py_DECREF( tmp_genexpr_2__$0 );
        tmp_genexpr_2__$0 = NULL;

        goto outline_result_3;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
        Py_DECREF( tmp_genexpr_2__$0 );
        tmp_genexpr_2__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( prompt_toolkit$formatted_text$ansi );
        return MOD_RETURN_VALUE( NULL );
        outline_result_3:;
        tmp_assign_source_20 = TO_DICT( tmp_dict_seq_2, NULL );
        Py_DECREF( tmp_dict_seq_2 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__bg_colors, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = PyDict_New();
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__256_colors, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_5;
        tmp_called_name_3 = (PyObject *)&PyEnum_Type;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__256_colors_table );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__256_colors_table );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_256_colors_table" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 232;

            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_5;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_colors );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;

            goto frame_exception_exit_1;
        }
        frame_4abdfa9cab80d381f439db22986f3336->m_frame.f_lineno = 232;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_iter_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_23 = MAKE_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;

            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_23;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_24;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_24 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_24 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                exception_lineno = 232;
                goto try_except_handler_7;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_24;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_iter_arg_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_4 = tmp_for_loop_1__iter_value;
        tmp_assign_source_25 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;

            goto try_except_handler_8;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_25;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_26 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_26 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 232;
            goto try_except_handler_9;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_26;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_27 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_27 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 232;
            goto try_except_handler_9;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_27;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 232;
                    goto try_except_handler_9;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            goto try_except_handler_9;
        }
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_8;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_28;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_28 = tmp_tuple_unpack_1__element_1;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_i, tmp_assign_source_28 );
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_iter_arg_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_iter_arg_5 = tmp_tuple_unpack_1__element_2;
        tmp_assign_source_29 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_5 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;

            goto try_except_handler_10;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__source_iter;
            tmp_tuple_unpack_2__source_iter = tmp_assign_source_29;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_30 = UNPACK_NEXT( tmp_unpack_3, 0, 3 );
        if ( tmp_assign_source_30 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 232;
            goto try_except_handler_11;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_1;
            tmp_tuple_unpack_2__element_1 = tmp_assign_source_30;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_31 = UNPACK_NEXT( tmp_unpack_4, 1, 3 );
        if ( tmp_assign_source_31 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 232;
            goto try_except_handler_11;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_2;
            tmp_tuple_unpack_2__element_2 = tmp_assign_source_31;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_unpack_5;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_5 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_32 = UNPACK_NEXT( tmp_unpack_5, 2, 3 );
        if ( tmp_assign_source_32 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }



            exception_lineno = 232;
            goto try_except_handler_11;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_3;
            tmp_tuple_unpack_2__element_3 = tmp_assign_source_32;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 232;
                    goto try_except_handler_11;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 232;
            goto try_except_handler_11;
        }
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto try_except_handler_10;
    // End of try:
    try_end_4:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_3 );
    tmp_tuple_unpack_2__element_3 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto try_except_handler_8;
    // End of try:
    try_end_5:;
    goto try_end_6;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto try_except_handler_7;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_33;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_33 = tmp_tuple_unpack_2__element_1;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_r, tmp_assign_source_33 );
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_34;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_34 = tmp_tuple_unpack_2__element_2;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_g, tmp_assign_source_34 );
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    {
        PyObject *tmp_assign_source_35;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_3 );
        tmp_assign_source_35 = tmp_tuple_unpack_2__element_3;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_b, tmp_assign_source_35 );
    }
    Py_XDECREF( tmp_tuple_unpack_2__element_3 );
    tmp_tuple_unpack_2__element_3 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_ass_subscript_1;
        PyObject *tmp_mvar_value_10;
        tmp_left_name_2 = const_str_digest_ed06864af6677bd9d5d964732f4f512a;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_r );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_r );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "r" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 233;

            goto try_except_handler_7;
        }

        tmp_tuple_element_4 = tmp_mvar_value_6;
        tmp_right_name_2 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_4 );
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_g );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_g );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_right_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "g" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 233;

            goto try_except_handler_7;
        }

        tmp_tuple_element_4 = tmp_mvar_value_7;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_4 );
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_b );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_b );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_right_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "b" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 233;

            goto try_except_handler_7;
        }

        tmp_tuple_element_4 = tmp_mvar_value_8;
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_right_name_2, 2, tmp_tuple_element_4 );
        tmp_ass_subvalue_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_ass_subvalue_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 233;

            goto try_except_handler_7;
        }
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain__256_colors );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__256_colors );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_ass_subvalue_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_256_colors" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 233;

            goto try_except_handler_7;
        }

        tmp_ass_subscribed_1 = tmp_mvar_value_9;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_i );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_ass_subvalue_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 233;

            goto try_except_handler_7;
        }

        tmp_ass_subscript_1 = tmp_mvar_value_10;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        Py_DECREF( tmp_ass_subvalue_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 233;

            goto try_except_handler_7;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 232;

        goto try_except_handler_7;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_7;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4abdfa9cab80d381f439db22986f3336 );
#endif
    popFrameStack();

    assertFrameObject( frame_4abdfa9cab80d381f439db22986f3336 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4abdfa9cab80d381f439db22986f3336 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4abdfa9cab80d381f439db22986f3336, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4abdfa9cab80d381f439db22986f3336->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4abdfa9cab80d381f439db22986f3336, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_36;
        tmp_assign_source_36 = MAKE_FUNCTION_prompt_toolkit$formatted_text$ansi$$$function_8_ansi_escape(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$formatted_text$ansi, (Nuitka_StringObject *)const_str_plain_ansi_escape, tmp_assign_source_36 );
    }

    return MOD_RETURN_VALUE( module_prompt_toolkit$formatted_text$ansi );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
