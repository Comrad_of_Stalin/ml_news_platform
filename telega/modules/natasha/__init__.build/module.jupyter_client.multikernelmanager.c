/* Generated code for Python module 'jupyter_client.multikernelmanager'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jupyter_client$multikernelmanager" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jupyter_client$multikernelmanager;
PyDictObject *moduledict_jupyter_client$multikernelmanager;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_signum;
static PyObject *const_tuple_f2395a3d4926b02c306d5e63b4fee427_tuple;
extern PyObject *const_str_plain_restart;
static PyObject *const_str_plain_constructor_kwargs;
extern PyObject *const_str_plain_finish_shutdown;
static PyObject *const_str_digest_7f33257446ecdc09da27efa8b1b5eabf;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_KernelSpecManager;
static PyObject *const_str_digest_2c016138d0cf1800dae1903e4fd03ca6;
static PyObject *const_str_digest_b3f6913fa34a3cc3d53a5e53ae57ff60;
extern PyObject *const_str_plain___name__;
static PyObject *const_str_digest_ce2e49a6736035532a56a8e3416e08c6;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_connect_shell;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_km;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_fa77bd8a182b92708dbc44f1692368a3;
static PyObject *const_str_digest_88986eb061eb9515676d3901c72de268;
extern PyObject *const_str_plain_uuid4;
static PyObject *const_str_digest_c2bd822880a7f32d2afc88e4242f01d4;
extern PyObject *const_tuple_str_plain_f_str_plain_wrapped_tuple;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain_kernel_manager_class;
extern PyObject *const_str_plain_restart_kernel;
extern PyObject *const_str_plain__kernels;
static PyObject *const_str_digest_cdc338ffebf21598b6b047c452af9ad7;
extern PyObject *const_tuple_false_false_tuple;
static PyObject *const_str_digest_2abb1e6f0a71a3ed587155acbea20aad;
static PyObject *const_str_digest_e712fe2b9fa57988fcb2a3aab9f85080;
static PyObject *const_str_plain_default_kernel_name;
static PyObject *const_str_digest_416ab4e9c294a0bf35209fcddba938c5;
static PyObject *const_str_plain_DuplicateKernelError;
static PyObject *const_str_digest_e947ff718933c357fc56cbc61f9b7130;
extern PyObject *const_str_plain_now;
extern PyObject *const_str_plain_instance;
static PyObject *const_str_digest_71acf10d398406a11e9af6c497875bd7;
static PyObject *const_str_digest_59a7a93a22f3fc467128e61ce88ee3cb;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_Dict;
extern PyObject *const_str_plain_None;
static PyObject *const_dict_0d183126925e861e63208132384d6690;
extern PyObject *const_tuple_none_float_0_1_tuple;
static PyObject *const_str_plain_kernel_manager_factory;
static PyObject *const_str_digest_5ee1d9ec14389b82cae29d99ac4344f6;
extern PyObject *const_str_plain_List;
static PyObject *const_str_digest_7848684c32bc8895da7055d1e340d42a;
extern PyObject *const_str_plain_cleanup;
static PyObject *const_str_digest_17e8a030a8a53b34719766669bea3f99;
static PyObject *const_str_digest_47253d84ce90ad61fe62e851b2670fb4;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_digest_b1de2f4e7d17f11eaeb3062e52aa9890;
extern PyObject *const_str_plain_connect_hb;
extern PyObject *const_str_plain_request_shutdown;
static PyObject *const_str_digest_4fa8f7b0e58fbec1c69b7afce7431494;
static PyObject *const_str_digest_43cb031700d556e6e3708de5b2a7adec;
extern PyObject *const_tuple_true_tuple;
static PyObject *const_str_digest_43b52cf86e191cb5b069631d56f54d11;
extern PyObject *const_str_plain_waittime;
extern PyObject *const_str_plain_wrapped;
extern PyObject *const_str_plain_absolute_import;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_kernelspec;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_digest_4e83119e4807363e8041fe90f23e9902;
extern PyObject *const_str_plain_start_kernel;
static PyObject *const_str_digest_d7198416b586a9fd11e4c1be02223718;
static PyObject *const_str_digest_b6e93d45ba9131909659533c46aa542f;
extern PyObject *const_str_plain_allow_none;
static PyObject *const_str_digest_8fa820fd86a703d81845291a1d9b2c36;
extern PyObject *const_str_plain___len__;
extern PyObject *const_float_0_1;
extern PyObject *const_str_plain_Context;
extern PyObject *const_str_plain_DottedObjectName;
extern PyObject *const_str_plain_connect_stdin;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_plain_remove_kernel;
extern PyObject *const_str_plain_kernel_name;
extern PyObject *const_str_digest_187926d0ae58a603c01bbdd47a78211c;
extern PyObject *const_str_plain_signal_kernel;
static PyObject *const_str_digest_78ebd725b917784d162be40d4f13d612;
static PyObject *const_str_digest_5f5eef8a4f2775a5a6ebd47e7e7c625d;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_plain_connection_dir;
static PyObject *const_str_digest_11ef1cb4e1d1e4671ef1e217b69bd3f5;
static PyObject *const_str_digest_a90a9c4fa1533a2736b5bb095ebee9e3;
extern PyObject *const_str_plain_LoggingConfigurable;
extern PyObject *const_tuple_str_plain_self_tuple;
static PyObject *const_str_plain_list_kernel_ids;
static PyObject *const_str_digest_22861c41e2217e918abd7dc10f46cbd7;
extern PyObject *const_str_plain_parent;
static PyObject *const_str_digest_a84fe30dba4618157be829ad273ee7ab;
extern PyObject *const_str_plain_interrupt_kernel;
extern PyObject *const_str_plain_MultiKernelManager;
extern PyObject *const_str_plain_keys;
extern PyObject *const_str_plain_context;
static PyObject *const_dict_1ab8db4bc0dac411ceb46d7af52254d7;
extern PyObject *const_str_plain_old;
static PyObject *const_str_digest_07f1c830eeb0e5eac10974520e863a57;
extern PyObject *const_str_plain_connection_file;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_event;
static PyObject *const_str_digest_11a5b6f991aea211ac82cc8a784e8a99;
static PyObject *const_str_digest_8f750d9ab041f2063a8a20b100915376;
static PyObject *const_tuple_str_plain_self_str_plain_kernel_id_str_plain_now_tuple;
extern PyObject *const_str_plain_new;
extern PyObject *const_str_plain_get_connection_info;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain_r;
static PyObject *const_str_digest_8978421be8f91781af7ce2289b2adef6;
extern PyObject *const_str_plain_Any;
static PyObject *const_str_digest_26284e431a918fbc2da2865fc52e946a;
static PyObject *const_str_digest_656a811c105fc57fb43e14e6d24ac599;
static PyObject *const_str_digest_21216dfe9d6a01d51632cfcd62bfaaec;
static PyObject *const_str_plain__kernel_manager_class_changed;
extern PyObject *const_str_plain_traitlets;
static PyObject *const_str_digest_2e3d5abd33b85e0d308c46cf1b55b27a;
static PyObject *const_str_digest_4e08e4df4147c3abba1fb891c8c05185;
extern PyObject *const_str_plain_config;
static PyObject *const_str_digest_0e0ddded2119d6a179c05dd7e937cb13;
extern PyObject *const_str_plain_False;
extern PyObject *const_tuple_str_plain_unicode_type_tuple;
static PyObject *const_str_digest_42865928c7a79d7944586f09ad478b0a;
extern PyObject *const_tuple_str_empty_tuple;
static PyObject *const_str_digest_7add0b9f6c1a0a1dd90320a3c05e3399;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain___contains__;
static PyObject *const_str_digest_24eb43422c2b998699a14f7d1b94f9be;
extern PyObject *const_str_plain_f;
extern PyObject *const_str_plain_kernel_spec_manager;
extern PyObject *const_str_plain_connect_iopub;
extern PyObject *const_str_plain_pop;
extern PyObject *const_str_plain_zmq;
extern PyObject *const_int_0;
extern PyObject *const_tuple_str_plain_import_item_tuple;
static PyObject *const_tuple_str_digest_b6e93d45ba9131909659533c46aa542f_tuple;
static PyObject *const_str_digest_6aa63f4692615b1480969936aeb13257;
static PyObject *const_str_digest_84f941612cf3c2b03790c47b7f21ab83;
static PyObject *const_str_plain__kernel_manager_factory_default;
extern PyObject *const_tuple_str_plain_restart_tuple;
static PyObject *const_str_digest_8e86094e04988490a8703f89546292cf;
static PyObject *const_tuple_str_plain_self_str_plain_kernel_id_str_plain_restart_tuple;
static PyObject *const_str_digest_0216d87a24d8d6decebf25026a725e14;
extern PyObject *const_str_plain_origin;
static PyObject *const_tuple_str_plain_self_str_plain_kernel_id_str_plain_identity_tuple;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_log;
extern PyObject *const_tuple_str_plain_self_str_plain_name_str_plain_old_str_plain_new_tuple;
static PyObject *const_str_digest_f26321dfb8e094ece8128da687882017;
extern PyObject *const_str_plain_import_item;
extern PyObject *const_str_plain_Instance;
extern PyObject *const_str_plain_identity;
extern PyObject *const_dict_d9034e00b80a03cb8bb3a26cd519b4c0;
extern PyObject *const_str_plain_is_alive;
extern PyObject *const_str_plain_callback;
static PyObject *const_str_plain_kernel_id;
static PyObject *const_str_digest_90456e7f24418e0fe671eeb1b317f6e4;
static PyObject *const_str_digest_8c51b352f892ef1c11cfd4bb72a6de84;
static PyObject *const_str_digest_de79b2f753a09f78e2ec63dbde59a8c6;
static PyObject *const_tuple_str_plain_self_str_plain_now_str_plain_kids_str_plain_kid_tuple;
extern PyObject *const_str_plain_type;
static PyObject *const_str_plain_kid;
static PyObject *const_str_digest_3c84c6eb44f362defbe50b8d6ebcfe2a;
static PyObject *const_str_digest_633bf33f696495f3934d819048299943;
extern PyObject *const_str_plain_pollinterval;
static PyObject *const_str_digest_17502559f98cdbd5004e19315c169fd1;
static PyObject *const_tuple_4078bacaa22d7a8fbbcc081c1cfbb1ca_tuple;
extern PyObject *const_str_plain_shutdown_kernel;
extern PyObject *const_str_plain_unicode_type;
static PyObject *const_str_digest_c0aa17bc5807bd203ea556397e070832;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
static PyObject *const_tuple_7281746f2ebd1816fe3573228d2ac44c_tuple;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_tuple_81afae4e236238f27144093deb2d6765_tuple;
extern PyObject *const_tuple_type_Exception_tuple;
static PyObject *const_str_plain_get_kernel;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain__context_default;
static PyObject *const_str_plain__check_kernel_id;
static PyObject *const_tuple_str_plain_self_str_plain_kernel_id_str_plain_signum_tuple;
extern PyObject *const_str_digest_1f09be22b4de59cb51b5667b622d5584;
static PyObject *const_str_plain_shutdown_all;
static PyObject *const_str_digest_8a230c74237862fe95fa7386df4b52a6;
static PyObject *const_tuple_90b7e25eff814a61b8b1f575e8611c09_tuple;
extern PyObject *const_tuple_str_plain_LoggingConfigurable_tuple;
extern PyObject *const_str_digest_34e3966f9679fde95e40f5931481245c;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_942d63b2480dc86fc4906ec5e73f51f5;
static PyObject *const_str_digest_735de15a0f9cc7ceae2d0ccd8cf0012c;
static PyObject *const_str_digest_7f311be033ffce5fac41a48d46ea1da3;
static PyObject *const_tuple_str_plain_NATIVE_KERNEL_NAME_str_plain_KernelSpecManager_tuple;
static PyObject *const_dict_706716995e98c3113acb1b849e043636;
extern PyObject *const_str_plain_uuid;
extern PyObject *const_tuple_str_digest_1f09be22b4de59cb51b5667b622d5584_tuple;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_remove_restart_callback;
static PyObject *const_tuple_str_plain_self_str_plain_kernel_id_str_plain_connection_file_tuple;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_NATIVE_KERNEL_NAME;
static PyObject *const_str_plain_kernel_method;
extern PyObject *const_str_plain_method;
extern PyObject *const_str_digest_c4d698c24b299b0741495b4b2af4b51a;
static PyObject *const_str_digest_1023dc1ed61ea43a8b0708aa8f3e4d60;
extern PyObject *const_str_plain_kwargs;
static PyObject *const_str_digest_2756d4df6b0b2df7aaa48752692ba594;
extern PyObject *const_str_plain_add_restart_callback;
static PyObject *const_str_plain_kids;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_2139417fa6f6500d7d7b32b0bcc5ef25;
extern PyObject *const_str_plain_Unicode;
static PyObject *const_str_digest_008e1a9bff1b3a1b2a9a675f37a1654b;
static PyObject *const_tuple_str_plain_self_str_plain_kernel_id_tuple;
extern PyObject *const_str_plain_info;
extern PyObject *const_str_empty;
static PyObject *const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple;
extern PyObject *const_str_plain_help;
static PyObject *const_str_digest_e8611e6ebae2f702245f8ba2b74a208e;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_f2395a3d4926b02c306d5e63b4fee427_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_f2395a3d4926b02c306d5e63b4fee427_tuple, 0, const_str_plain_Instance ); Py_INCREF( const_str_plain_Instance );
    PyTuple_SET_ITEM( const_tuple_f2395a3d4926b02c306d5e63b4fee427_tuple, 1, const_str_plain_Dict ); Py_INCREF( const_str_plain_Dict );
    PyTuple_SET_ITEM( const_tuple_f2395a3d4926b02c306d5e63b4fee427_tuple, 2, const_str_plain_List ); Py_INCREF( const_str_plain_List );
    PyTuple_SET_ITEM( const_tuple_f2395a3d4926b02c306d5e63b4fee427_tuple, 3, const_str_plain_Unicode ); Py_INCREF( const_str_plain_Unicode );
    PyTuple_SET_ITEM( const_tuple_f2395a3d4926b02c306d5e63b4fee427_tuple, 4, const_str_plain_Any ); Py_INCREF( const_str_plain_Any );
    PyTuple_SET_ITEM( const_tuple_f2395a3d4926b02c306d5e63b4fee427_tuple, 5, const_str_plain_DottedObjectName ); Py_INCREF( const_str_plain_DottedObjectName );
    const_str_plain_constructor_kwargs = UNSTREAM_STRING_ASCII( &constant_bin[ 1211129 ], 18, 1 );
    const_str_digest_7f33257446ecdc09da27efa8b1b5eabf = UNSTREAM_STRING_ASCII( &constant_bin[ 1211147 ], 307, 0 );
    const_str_digest_2c016138d0cf1800dae1903e4fd03ca6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1211454 ], 22, 0 );
    const_str_digest_b3f6913fa34a3cc3d53a5e53ae57ff60 = UNSTREAM_STRING_ASCII( &constant_bin[ 1211476 ], 310, 0 );
    const_str_digest_ce2e49a6736035532a56a8e3416e08c6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1211786 ], 34, 0 );
    const_str_digest_fa77bd8a182b92708dbc44f1692368a3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1211820 ], 20, 0 );
    const_str_digest_88986eb061eb9515676d3901c72de268 = UNSTREAM_STRING_ASCII( &constant_bin[ 1211840 ], 41, 0 );
    const_str_digest_c2bd822880a7f32d2afc88e4242f01d4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1211881 ], 38, 0 );
    const_str_digest_cdc338ffebf21598b6b047c452af9ad7 = UNSTREAM_STRING_ASCII( &constant_bin[ 1211919 ], 37, 0 );
    const_str_digest_2abb1e6f0a71a3ed587155acbea20aad = UNSTREAM_STRING_ASCII( &constant_bin[ 1211956 ], 36, 0 );
    const_str_digest_e712fe2b9fa57988fcb2a3aab9f85080 = UNSTREAM_STRING_ASCII( &constant_bin[ 1211992 ], 42, 0 );
    const_str_plain_default_kernel_name = UNSTREAM_STRING_ASCII( &constant_bin[ 1199451 ], 19, 1 );
    const_str_digest_416ab4e9c294a0bf35209fcddba938c5 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212034 ], 32, 0 );
    const_str_plain_DuplicateKernelError = UNSTREAM_STRING_ASCII( &constant_bin[ 1212066 ], 20, 1 );
    const_str_digest_e947ff718933c357fc56cbc61f9b7130 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212086 ], 32, 0 );
    const_str_digest_71acf10d398406a11e9af6c497875bd7 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212118 ], 28, 0 );
    const_str_digest_59a7a93a22f3fc467128e61ce88ee3cb = UNSTREAM_STRING_ASCII( &constant_bin[ 1212146 ], 162, 0 );
    const_dict_0d183126925e861e63208132384d6690 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_0d183126925e861e63208132384d6690, const_str_plain_config, Py_True );
    const_str_digest_4fa8f7b0e58fbec1c69b7afce7431494 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212308 ], 131, 0 );
    PyDict_SetItem( const_dict_0d183126925e861e63208132384d6690, const_str_plain_help, const_str_digest_4fa8f7b0e58fbec1c69b7afce7431494 );
    assert( PyDict_Size( const_dict_0d183126925e861e63208132384d6690 ) == 2 );
    const_str_plain_kernel_manager_factory = UNSTREAM_STRING_ASCII( &constant_bin[ 1212439 ], 22, 1 );
    const_str_digest_5ee1d9ec14389b82cae29d99ac4344f6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212461 ], 39, 0 );
    const_str_digest_7848684c32bc8895da7055d1e340d42a = UNSTREAM_STRING_ASCII( &constant_bin[ 1212500 ], 31, 0 );
    const_str_digest_17e8a030a8a53b34719766669bea3f99 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212531 ], 54, 0 );
    const_str_digest_47253d84ce90ad61fe62e851b2670fb4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212585 ], 30, 0 );
    const_str_digest_43cb031700d556e6e3708de5b2a7adec = UNSTREAM_STRING_ASCII( &constant_bin[ 1212615 ], 26, 0 );
    const_str_digest_43b52cf86e191cb5b069631d56f54d11 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212641 ], 31, 0 );
    const_str_digest_4e83119e4807363e8041fe90f23e9902 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212672 ], 19, 0 );
    const_str_digest_d7198416b586a9fd11e4c1be02223718 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212691 ], 168, 0 );
    const_str_digest_b6e93d45ba9131909659533c46aa542f = UNSTREAM_STRING_ASCII( &constant_bin[ 1212859 ], 41, 0 );
    const_str_digest_8fa820fd86a703d81845291a1d9b2c36 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212900 ], 48, 0 );
    const_str_plain_remove_kernel = UNSTREAM_STRING_ASCII( &constant_bin[ 1200431 ], 13, 1 );
    const_str_digest_78ebd725b917784d162be40d4f13d612 = UNSTREAM_STRING_ASCII( &constant_bin[ 1212948 ], 275, 0 );
    const_str_digest_5f5eef8a4f2775a5a6ebd47e7e7c625d = UNSTREAM_STRING_ASCII( &constant_bin[ 1213223 ], 25, 0 );
    const_str_digest_11ef1cb4e1d1e4671ef1e217b69bd3f5 = UNSTREAM_STRING_ASCII( &constant_bin[ 1213248 ], 31, 0 );
    const_str_digest_a90a9c4fa1533a2736b5bb095ebee9e3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1213279 ], 35, 0 );
    const_str_plain_list_kernel_ids = UNSTREAM_STRING_ASCII( &constant_bin[ 1213314 ], 15, 1 );
    const_str_digest_22861c41e2217e918abd7dc10f46cbd7 = UNSTREAM_STRING_ASCII( &constant_bin[ 1213329 ], 44, 0 );
    const_str_digest_a84fe30dba4618157be829ad273ee7ab = UNSTREAM_STRING_ASCII( &constant_bin[ 1213373 ], 21, 0 );
    const_dict_1ab8db4bc0dac411ceb46d7af52254d7 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_1ab8db4bc0dac411ceb46d7af52254d7, const_str_plain_help, const_str_digest_88986eb061eb9515676d3901c72de268 );
    assert( PyDict_Size( const_dict_1ab8db4bc0dac411ceb46d7af52254d7 ) == 1 );
    const_str_digest_07f1c830eeb0e5eac10974520e863a57 = UNSTREAM_STRING_ASCII( &constant_bin[ 1213394 ], 29, 0 );
    const_str_digest_11a5b6f991aea211ac82cc8a784e8a99 = UNSTREAM_STRING_ASCII( &constant_bin[ 1213423 ], 37, 0 );
    const_str_digest_8f750d9ab041f2063a8a20b100915376 = UNSTREAM_STRING_ASCII( &constant_bin[ 1213460 ], 34, 0 );
    const_tuple_str_plain_self_str_plain_kernel_id_str_plain_now_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_now_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_kernel_id = UNSTREAM_STRING_ASCII( &constant_bin[ 1211243 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_now_tuple, 1, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_now_tuple, 2, const_str_plain_now ); Py_INCREF( const_str_plain_now );
    const_str_digest_8978421be8f91781af7ce2289b2adef6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1213494 ], 29, 0 );
    const_str_digest_26284e431a918fbc2da2865fc52e946a = UNSTREAM_STRING_ASCII( &constant_bin[ 1213523 ], 173, 0 );
    const_str_digest_656a811c105fc57fb43e14e6d24ac599 = UNSTREAM_STRING_ASCII( &constant_bin[ 1213696 ], 26, 0 );
    const_str_digest_21216dfe9d6a01d51632cfcd62bfaaec = UNSTREAM_STRING_ASCII( &constant_bin[ 1213722 ], 459, 0 );
    const_str_plain__kernel_manager_class_changed = UNSTREAM_STRING_ASCII( &constant_bin[ 1212919 ], 29, 1 );
    const_str_digest_2e3d5abd33b85e0d308c46cf1b55b27a = UNSTREAM_STRING_ASCII( &constant_bin[ 1214181 ], 42, 0 );
    const_str_digest_4e08e4df4147c3abba1fb891c8c05185 = UNSTREAM_STRING_ASCII( &constant_bin[ 1214223 ], 27, 0 );
    const_str_digest_0e0ddded2119d6a179c05dd7e937cb13 = UNSTREAM_STRING_ASCII( &constant_bin[ 1214250 ], 31, 0 );
    const_str_digest_42865928c7a79d7944586f09ad478b0a = UNSTREAM_STRING_ASCII( &constant_bin[ 1214281 ], 38, 0 );
    const_str_digest_7add0b9f6c1a0a1dd90320a3c05e3399 = UNSTREAM_STRING_ASCII( &constant_bin[ 1214319 ], 26, 0 );
    const_str_digest_24eb43422c2b998699a14f7d1b94f9be = UNSTREAM_STRING_ASCII( &constant_bin[ 1214345 ], 77, 0 );
    const_tuple_str_digest_b6e93d45ba9131909659533c46aa542f_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_b6e93d45ba9131909659533c46aa542f_tuple, 0, const_str_digest_b6e93d45ba9131909659533c46aa542f ); Py_INCREF( const_str_digest_b6e93d45ba9131909659533c46aa542f );
    const_str_digest_6aa63f4692615b1480969936aeb13257 = UNSTREAM_STRING_ASCII( &constant_bin[ 1214422 ], 35, 0 );
    const_str_digest_84f941612cf3c2b03790c47b7f21ab83 = UNSTREAM_STRING_ASCII( &constant_bin[ 1214457 ], 310, 0 );
    const_str_plain__kernel_manager_factory_default = UNSTREAM_STRING_ASCII( &constant_bin[ 1214767 ], 31, 1 );
    const_str_digest_8e86094e04988490a8703f89546292cf = UNSTREAM_STRING_ASCII( &constant_bin[ 1214798 ], 32, 0 );
    const_tuple_str_plain_self_str_plain_kernel_id_str_plain_restart_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_restart_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_restart_tuple, 1, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_restart_tuple, 2, const_str_plain_restart ); Py_INCREF( const_str_plain_restart );
    const_str_digest_0216d87a24d8d6decebf25026a725e14 = UNSTREAM_STRING_ASCII( &constant_bin[ 1214830 ], 221, 0 );
    const_tuple_str_plain_self_str_plain_kernel_id_str_plain_identity_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_identity_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_identity_tuple, 1, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_identity_tuple, 2, const_str_plain_identity ); Py_INCREF( const_str_plain_identity );
    const_str_digest_f26321dfb8e094ece8128da687882017 = UNSTREAM_STRING_ASCII( &constant_bin[ 1215051 ], 41, 0 );
    const_str_digest_90456e7f24418e0fe671eeb1b317f6e4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1215092 ], 68, 0 );
    const_str_digest_8c51b352f892ef1c11cfd4bb72a6de84 = UNSTREAM_STRING_ASCII( &constant_bin[ 1215160 ], 32, 0 );
    const_str_digest_de79b2f753a09f78e2ec63dbde59a8c6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1215192 ], 201, 0 );
    const_tuple_str_plain_self_str_plain_now_str_plain_kids_str_plain_kid_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_now_str_plain_kids_str_plain_kid_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_now_str_plain_kids_str_plain_kid_tuple, 1, const_str_plain_now ); Py_INCREF( const_str_plain_now );
    const_str_plain_kids = UNSTREAM_STRING_ASCII( &constant_bin[ 1215393 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_now_str_plain_kids_str_plain_kid_tuple, 2, const_str_plain_kids ); Py_INCREF( const_str_plain_kids );
    const_str_plain_kid = UNSTREAM_STRING_ASCII( &constant_bin[ 1215393 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_now_str_plain_kids_str_plain_kid_tuple, 3, const_str_plain_kid ); Py_INCREF( const_str_plain_kid );
    const_str_digest_3c84c6eb44f362defbe50b8d6ebcfe2a = UNSTREAM_STRING_ASCII( &constant_bin[ 1215397 ], 35, 0 );
    const_str_digest_633bf33f696495f3934d819048299943 = UNSTREAM_STRING_ASCII( &constant_bin[ 1215432 ], 38, 0 );
    const_str_digest_17502559f98cdbd5004e19315c169fd1 = UNSTREAM_STRING_ASCII( &constant_bin[ 1215470 ], 307, 0 );
    const_tuple_4078bacaa22d7a8fbbcc081c1cfbb1ca_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_4078bacaa22d7a8fbbcc081c1cfbb1ca_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_4078bacaa22d7a8fbbcc081c1cfbb1ca_tuple, 1, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    PyTuple_SET_ITEM( const_tuple_4078bacaa22d7a8fbbcc081c1cfbb1ca_tuple, 2, const_str_plain_now ); Py_INCREF( const_str_plain_now );
    PyTuple_SET_ITEM( const_tuple_4078bacaa22d7a8fbbcc081c1cfbb1ca_tuple, 3, const_str_plain_restart ); Py_INCREF( const_str_plain_restart );
    const_str_digest_c0aa17bc5807bd203ea556397e070832 = UNSTREAM_STRING_ASCII( &constant_bin[ 1215777 ], 34, 0 );
    const_tuple_7281746f2ebd1816fe3573228d2ac44c_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_7281746f2ebd1816fe3573228d2ac44c_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_7281746f2ebd1816fe3573228d2ac44c_tuple, 1, const_str_plain_kernel_name ); Py_INCREF( const_str_plain_kernel_name );
    PyTuple_SET_ITEM( const_tuple_7281746f2ebd1816fe3573228d2ac44c_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_7281746f2ebd1816fe3573228d2ac44c_tuple, 3, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    PyTuple_SET_ITEM( const_tuple_7281746f2ebd1816fe3573228d2ac44c_tuple, 4, const_str_plain_constructor_kwargs ); Py_INCREF( const_str_plain_constructor_kwargs );
    PyTuple_SET_ITEM( const_tuple_7281746f2ebd1816fe3573228d2ac44c_tuple, 5, const_str_plain_km ); Py_INCREF( const_str_plain_km );
    const_tuple_81afae4e236238f27144093deb2d6765_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_81afae4e236238f27144093deb2d6765_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_81afae4e236238f27144093deb2d6765_tuple, 1, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    PyTuple_SET_ITEM( const_tuple_81afae4e236238f27144093deb2d6765_tuple, 2, const_str_plain_callback ); Py_INCREF( const_str_plain_callback );
    PyTuple_SET_ITEM( const_tuple_81afae4e236238f27144093deb2d6765_tuple, 3, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    const_str_plain_get_kernel = UNSTREAM_STRING_ASCII( &constant_bin[ 1198828 ], 10, 1 );
    const_str_plain__check_kernel_id = UNSTREAM_STRING_ASCII( &constant_bin[ 1215811 ], 16, 1 );
    const_tuple_str_plain_self_str_plain_kernel_id_str_plain_signum_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_signum_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_signum_tuple, 1, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_signum_tuple, 2, const_str_plain_signum ); Py_INCREF( const_str_plain_signum );
    const_str_plain_shutdown_all = UNSTREAM_STRING_ASCII( &constant_bin[ 1212519 ], 12, 1 );
    const_str_digest_8a230c74237862fe95fa7386df4b52a6 = UNSTREAM_STRING_ASCII( &constant_bin[ 1215827 ], 39, 0 );
    const_tuple_90b7e25eff814a61b8b1f575e8611c09_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_90b7e25eff814a61b8b1f575e8611c09_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_90b7e25eff814a61b8b1f575e8611c09_tuple, 1, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    PyTuple_SET_ITEM( const_tuple_90b7e25eff814a61b8b1f575e8611c09_tuple, 2, const_str_plain_waittime ); Py_INCREF( const_str_plain_waittime );
    PyTuple_SET_ITEM( const_tuple_90b7e25eff814a61b8b1f575e8611c09_tuple, 3, const_str_plain_pollinterval ); Py_INCREF( const_str_plain_pollinterval );
    const_str_digest_942d63b2480dc86fc4906ec5e73f51f5 = UNSTREAM_STRING_ASCII( &constant_bin[ 1215866 ], 236, 0 );
    const_str_digest_735de15a0f9cc7ceae2d0ccd8cf0012c = UNSTREAM_STRING_ASCII( &constant_bin[ 1216102 ], 29, 0 );
    const_str_digest_7f311be033ffce5fac41a48d46ea1da3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1216131 ], 35, 0 );
    const_tuple_str_plain_NATIVE_KERNEL_NAME_str_plain_KernelSpecManager_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_NATIVE_KERNEL_NAME_str_plain_KernelSpecManager_tuple, 0, const_str_plain_NATIVE_KERNEL_NAME ); Py_INCREF( const_str_plain_NATIVE_KERNEL_NAME );
    PyTuple_SET_ITEM( const_tuple_str_plain_NATIVE_KERNEL_NAME_str_plain_KernelSpecManager_tuple, 1, const_str_plain_KernelSpecManager ); Py_INCREF( const_str_plain_KernelSpecManager );
    const_dict_706716995e98c3113acb1b849e043636 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_706716995e98c3113acb1b849e043636, const_str_plain_config, Py_True );
    PyDict_SetItem( const_dict_706716995e98c3113acb1b849e043636, const_str_plain_help, const_str_digest_5ee1d9ec14389b82cae29d99ac4344f6 );
    assert( PyDict_Size( const_dict_706716995e98c3113acb1b849e043636 ) == 2 );
    const_tuple_str_plain_self_str_plain_kernel_id_str_plain_connection_file_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_connection_file_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_connection_file_tuple, 1, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_str_plain_connection_file_tuple, 2, const_str_plain_connection_file ); Py_INCREF( const_str_plain_connection_file );
    const_str_plain_kernel_method = UNSTREAM_STRING_ASCII( &constant_bin[ 1212585 ], 13, 1 );
    const_str_digest_1023dc1ed61ea43a8b0708aa8f3e4d60 = UNSTREAM_STRING_ASCII( &constant_bin[ 1216166 ], 32, 0 );
    const_str_digest_2756d4df6b0b2df7aaa48752692ba594 = UNSTREAM_STRING_ASCII( &constant_bin[ 1216198 ], 33, 0 );
    const_str_digest_2139417fa6f6500d7d7b32b0bcc5ef25 = UNSTREAM_STRING_ASCII( &constant_bin[ 1214189 ], 33, 0 );
    const_str_digest_008e1a9bff1b3a1b2a9a675f37a1654b = UNSTREAM_STRING_ASCII( &constant_bin[ 1216231 ], 50, 0 );
    const_tuple_str_plain_self_str_plain_kernel_id_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_kernel_id_tuple, 1, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple, 1, const_str_plain_kernel_id ); Py_INCREF( const_str_plain_kernel_id );
    PyTuple_SET_ITEM( const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple, 2, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple, 3, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple, 4, const_str_plain_km ); Py_INCREF( const_str_plain_km );
    PyTuple_SET_ITEM( const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple, 5, const_str_plain_method ); Py_INCREF( const_str_plain_method );
    PyTuple_SET_ITEM( const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple, 6, const_str_plain_r ); Py_INCREF( const_str_plain_r );
    PyTuple_SET_ITEM( const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple, 7, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    const_str_digest_e8611e6ebae2f702245f8ba2b74a208e = UNSTREAM_STRING_ASCII( &constant_bin[ 1216281 ], 310, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jupyter_client$multikernelmanager( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_91ccbcb52cbcabb69b899a938ea905e1;
static PyCodeObject *codeobj_29eb16db1d6922f0ab18dd0af5d7452b;
static PyCodeObject *codeobj_745e33e47b918bdea2ada8903cf9f9e3;
static PyCodeObject *codeobj_4c41c571d19bcda30fbaf8e875096608;
static PyCodeObject *codeobj_09a700113408f79bd78883ae54953e42;
static PyCodeObject *codeobj_68e6b1c95c9b164a68a9a231b167e4cb;
static PyCodeObject *codeobj_9cb0ac5f8d8a16dd941d66d35dd990ce;
static PyCodeObject *codeobj_206c93c6a9c6970c037f0b760bc76ef5;
static PyCodeObject *codeobj_0f5127f2ac035122bd36eaa78f7cf76e;
static PyCodeObject *codeobj_64bb76649e50f0c8ed12e4efae021afc;
static PyCodeObject *codeobj_ec9cbe4a9bf09d46a743f803fc567af3;
static PyCodeObject *codeobj_a5a1e43f357589d446cf6d2f3a662414;
static PyCodeObject *codeobj_742428932fc41889ecec61a637aae234;
static PyCodeObject *codeobj_d48311f42f90b0fc8987ab6ce1c1e8d9;
static PyCodeObject *codeobj_6469ed134d824cdd1d8832cfbf217989;
static PyCodeObject *codeobj_99627676e86da22dc261797102b500be;
static PyCodeObject *codeobj_a44c5262b57b510c3386083cc64b01fd;
static PyCodeObject *codeobj_0d08f4f80c987c1986443a34d615e5e5;
static PyCodeObject *codeobj_7e36d6ec9db270007aad0ffd8c8cdd32;
static PyCodeObject *codeobj_2664207bff25b0f478ed20473be20ae3;
static PyCodeObject *codeobj_ed13d83aff49c106da38fa36cbf3fb60;
static PyCodeObject *codeobj_106d79cc7953f9b0459a6b5d8ba1725b;
static PyCodeObject *codeobj_f1ac5a464e278cc04934b779e94220c9;
static PyCodeObject *codeobj_710e9ec8f085e19a804b9827ab1dd424;
static PyCodeObject *codeobj_990c3b871a70f83038000474009046cb;
static PyCodeObject *codeobj_6d0ce4fd8e542168fad75aa9e5033624;
static PyCodeObject *codeobj_1ea163f6d2cb33193954fb9c9e02855e;
static PyCodeObject *codeobj_54e56d3a3f639351e1f35c21ec2306b5;
static PyCodeObject *codeobj_eb7db7b6021c9b064a50c251fbed9ce2;
static PyCodeObject *codeobj_1ac81d3332e6a5391b4705fc1386961e;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_2abb1e6f0a71a3ed587155acbea20aad );
    codeobj_91ccbcb52cbcabb69b899a938ea905e1 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_2e3d5abd33b85e0d308c46cf1b55b27a, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_29eb16db1d6922f0ab18dd0af5d7452b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_MultiKernelManager, 42, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_745e33e47b918bdea2ada8903cf9f9e3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___contains__, 82, const_tuple_str_plain_self_str_plain_kernel_id_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4c41c571d19bcda30fbaf8e875096608 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___len__, 78, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_09a700113408f79bd78883ae54953e42 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__check_kernel_id, 213, const_tuple_str_plain_self_str_plain_kernel_id_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_68e6b1c95c9b164a68a9a231b167e4cb = MAKE_CODEOBJ( module_filename_obj, const_str_plain__context_default, 65, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9cb0ac5f8d8a16dd941d66d35dd990ce = MAKE_CODEOBJ( module_filename_obj, const_str_plain__kernel_manager_class_changed, 57, const_tuple_str_plain_self_str_plain_name_str_plain_old_str_plain_new_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_206c93c6a9c6970c037f0b760bc76ef5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__kernel_manager_factory_default, 61, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0f5127f2ac035122bd36eaa78f7cf76e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_add_restart_callback, 229, const_tuple_81afae4e236238f27144093deb2d6765_tuple, 4, 0, CO_NOFREE );
    codeobj_64bb76649e50f0c8ed12e4efae021afc = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cleanup, 140, const_tuple_str_plain_self_str_plain_kernel_id_str_plain_connection_file_tuple, 3, 0, CO_NOFREE );
    codeobj_ec9cbe4a9bf09d46a743f803fc567af3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_connect_hb, 303, const_tuple_str_plain_self_str_plain_kernel_id_str_plain_identity_tuple, 3, 0, CO_NOFREE );
    codeobj_a5a1e43f357589d446cf6d2f3a662414 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_connect_iopub, 255, const_tuple_str_plain_self_str_plain_kernel_id_str_plain_identity_tuple, 3, 0, CO_NOFREE );
    codeobj_742428932fc41889ecec61a637aae234 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_connect_shell, 271, const_tuple_str_plain_self_str_plain_kernel_id_str_plain_identity_tuple, 3, 0, CO_NOFREE );
    codeobj_d48311f42f90b0fc8987ab6ce1c1e8d9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_connect_stdin, 287, const_tuple_str_plain_self_str_plain_kernel_id_str_plain_identity_tuple, 3, 0, CO_NOFREE );
    codeobj_6469ed134d824cdd1d8832cfbf217989 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_finish_shutdown, 134, const_tuple_90b7e25eff814a61b8b1f575e8611c09_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_99627676e86da22dc261797102b500be = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_connection_info, 237, const_tuple_str_plain_self_str_plain_kernel_id_tuple, 2, 0, CO_NOFREE );
    codeobj_a44c5262b57b510c3386083cc64b01fd = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_kernel, 218, const_tuple_str_plain_self_str_plain_kernel_id_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0d08f4f80c987c1986443a34d615e5e5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_interrupt_kernel, 164, const_tuple_str_plain_self_str_plain_kernel_id_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7e36d6ec9db270007aad0ffd8c8cdd32 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_is_alive, 200, const_tuple_str_plain_self_str_plain_kernel_id_tuple, 2, 0, CO_NOFREE );
    codeobj_2664207bff25b0f478ed20473be20ae3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_kernel_method, 26, const_tuple_str_plain_f_str_plain_wrapped_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ed13d83aff49c106da38fa36cbf3fb60 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_list_kernel_ids, 72, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_106d79cc7953f9b0459a6b5d8ba1725b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_remove_kernel, 144, const_tuple_str_plain_self_str_plain_kernel_id_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f1ac5a464e278cc04934b779e94220c9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_remove_restart_callback, 233, const_tuple_81afae4e236238f27144093deb2d6765_tuple, 4, 0, CO_NOFREE );
    codeobj_710e9ec8f085e19a804b9827ab1dd424 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_request_shutdown, 130, const_tuple_str_plain_self_str_plain_kernel_id_str_plain_restart_tuple, 3, 0, CO_NOFREE );
    codeobj_990c3b871a70f83038000474009046cb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_restart_kernel, 189, const_tuple_str_plain_self_str_plain_kernel_id_str_plain_now_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6d0ce4fd8e542168fad75aa9e5033624 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_shutdown_all, 154, const_tuple_str_plain_self_str_plain_now_str_plain_kids_str_plain_kid_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1ea163f6d2cb33193954fb9c9e02855e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_shutdown_kernel, 114, const_tuple_4078bacaa22d7a8fbbcc081c1cfbb1ca_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_54e56d3a3f639351e1f35c21ec2306b5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_signal_kernel, 175, const_tuple_str_plain_self_str_plain_kernel_id_str_plain_signum_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_eb7db7b6021c9b064a50c251fbed9ce2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_start_kernel, 85, const_tuple_7281746f2ebd1816fe3573228d2ac44c_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_1ac81d3332e6a5391b4705fc1386961e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_wrapped, 28, const_tuple_fe738379e34f590ebaa26cf055d7fb20_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_7_complex_call_helper_pos_star_list_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_10_complex_call_helper_keywords_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_2_complex_call_helper_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_10_request_shutdown( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_11_finish_shutdown( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_12_cleanup( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_13_remove_kernel(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_14_shutdown_all( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_15_interrupt_kernel(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_16_signal_kernel(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_17_restart_kernel( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_18_is_alive(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_19__check_kernel_id(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_1_kernel_method(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_1_kernel_method$$$function_1_wrapped(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_20_get_kernel(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_21_add_restart_callback( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_22_remove_restart_callback( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_23_get_connection_info(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_24_connect_iopub( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_25_connect_shell( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_26_connect_stdin( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_27_connect_hb( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_2__kernel_manager_class_changed(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_3__kernel_manager_factory_default(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_4__context_default(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_5_list_kernel_ids(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_6___len__(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_7___contains__(  );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_8_start_kernel( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_9_shutdown_kernel( PyObject *defaults );


// The module function definitions.
static PyObject *impl_jupyter_client$multikernelmanager$$$function_1_kernel_method( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_f = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_wrapped = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_1_kernel_method$$$function_1_wrapped(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_f;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var_wrapped == NULL );
        var_wrapped = tmp_assign_source_1;
    }
    // Tried code:
    CHECK_OBJECT( var_wrapped );
    tmp_return_value = var_wrapped;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_1_kernel_method );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_f );
    Py_DECREF( par_f );
    par_f = NULL;

    CHECK_OBJECT( (PyObject *)var_wrapped );
    Py_DECREF( var_wrapped );
    var_wrapped = NULL;

    goto function_return_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_1_kernel_method );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_1_kernel_method$$$function_1_wrapped( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_args = python_pars[ 2 ];
    PyObject *par_kwargs = python_pars[ 3 ];
    PyObject *var_km = NULL;
    PyObject *var_method = NULL;
    PyObject *var_r = NULL;
    struct Nuitka_FrameObject *frame_1ac81d3332e6a5391b4705fc1386961e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1ac81d3332e6a5391b4705fc1386961e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1ac81d3332e6a5391b4705fc1386961e, codeobj_1ac81d3332e6a5391b4705fc1386961e, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1ac81d3332e6a5391b4705fc1386961e = cache_frame_1ac81d3332e6a5391b4705fc1386961e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1ac81d3332e6a5391b4705fc1386961e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1ac81d3332e6a5391b4705fc1386961e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_kernel_id );
        tmp_args_element_name_1 = par_kernel_id;
        frame_1ac81d3332e6a5391b4705fc1386961e->m_frame.f_lineno = 30;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get_kernel, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "oooooooc";
            goto frame_exception_exit_1;
        }
        assert( var_km == NULL );
        var_km = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( var_km );
        tmp_getattr_target_1 = var_km;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "f" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 31;
            type_description_1 = "oooooooc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[0] );
        tmp_getattr_attr_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
        if ( tmp_getattr_attr_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "oooooooc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        Py_DECREF( tmp_getattr_attr_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "oooooooc";
            goto frame_exception_exit_1;
        }
        assert( var_method == NULL );
        var_method = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( var_method );
        tmp_dircall_arg1_1 = var_method;
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_3 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "oooooooc";
            goto frame_exception_exit_1;
        }
        assert( var_r == NULL );
        var_r = tmp_assign_source_3;
    }
    {
        PyObject *tmp_dircall_arg1_2;
        PyObject *tmp_dircall_arg2_2;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_2;
        PyObject *tmp_dircall_arg4_1;
        PyObject *tmp_call_result_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "f" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;
            type_description_1 = "oooooooc";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_2 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_self );
        tmp_tuple_element_1 = par_self;
        tmp_dircall_arg2_2 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_2, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kernel_id );
        tmp_tuple_element_1 = par_kernel_id;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_2, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_args );
        tmp_dircall_arg3_2 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg4_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg1_2 );
        Py_INCREF( tmp_dircall_arg3_2 );
        Py_INCREF( tmp_dircall_arg4_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2, tmp_dircall_arg3_2, tmp_dircall_arg4_1};
            tmp_call_result_1 = impl___internal__$$$function_7_complex_call_helper_pos_star_list_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "oooooooc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1ac81d3332e6a5391b4705fc1386961e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1ac81d3332e6a5391b4705fc1386961e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1ac81d3332e6a5391b4705fc1386961e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1ac81d3332e6a5391b4705fc1386961e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1ac81d3332e6a5391b4705fc1386961e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1ac81d3332e6a5391b4705fc1386961e,
        type_description_1,
        par_self,
        par_kernel_id,
        par_args,
        par_kwargs,
        var_km,
        var_method,
        var_r,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_1ac81d3332e6a5391b4705fc1386961e == cache_frame_1ac81d3332e6a5391b4705fc1386961e )
    {
        Py_DECREF( frame_1ac81d3332e6a5391b4705fc1386961e );
    }
    cache_frame_1ac81d3332e6a5391b4705fc1386961e = NULL;

    assertFrameObject( frame_1ac81d3332e6a5391b4705fc1386961e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_r );
    tmp_return_value = var_r;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_1_kernel_method$$$function_1_wrapped );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_km );
    Py_DECREF( var_km );
    var_km = NULL;

    CHECK_OBJECT( (PyObject *)var_method );
    Py_DECREF( var_method );
    var_method = NULL;

    CHECK_OBJECT( (PyObject *)var_r );
    Py_DECREF( var_r );
    var_r = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_km );
    var_km = NULL;

    Py_XDECREF( var_method );
    var_method = NULL;

    Py_XDECREF( var_r );
    var_r = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_1_kernel_method$$$function_1_wrapped );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_2__kernel_manager_class_changed( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *par_old = python_pars[ 2 ];
    PyObject *par_new = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_9cb0ac5f8d8a16dd941d66d35dd990ce;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_9cb0ac5f8d8a16dd941d66d35dd990ce = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9cb0ac5f8d8a16dd941d66d35dd990ce, codeobj_9cb0ac5f8d8a16dd941d66d35dd990ce, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9cb0ac5f8d8a16dd941d66d35dd990ce = cache_frame_9cb0ac5f8d8a16dd941d66d35dd990ce;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9cb0ac5f8d8a16dd941d66d35dd990ce );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9cb0ac5f8d8a16dd941d66d35dd990ce ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_import_item );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_import_item );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "import_item" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_new );
        tmp_args_element_name_1 = par_new;
        frame_9cb0ac5f8d8a16dd941d66d35dd990ce->m_frame.f_lineno = 58;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_kernel_manager_factory, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9cb0ac5f8d8a16dd941d66d35dd990ce );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9cb0ac5f8d8a16dd941d66d35dd990ce );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9cb0ac5f8d8a16dd941d66d35dd990ce, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9cb0ac5f8d8a16dd941d66d35dd990ce->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9cb0ac5f8d8a16dd941d66d35dd990ce, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9cb0ac5f8d8a16dd941d66d35dd990ce,
        type_description_1,
        par_self,
        par_name,
        par_old,
        par_new
    );


    // Release cached frame.
    if ( frame_9cb0ac5f8d8a16dd941d66d35dd990ce == cache_frame_9cb0ac5f8d8a16dd941d66d35dd990ce )
    {
        Py_DECREF( frame_9cb0ac5f8d8a16dd941d66d35dd990ce );
    }
    cache_frame_9cb0ac5f8d8a16dd941d66d35dd990ce = NULL;

    assertFrameObject( frame_9cb0ac5f8d8a16dd941d66d35dd990ce );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_2__kernel_manager_class_changed );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_old );
    Py_DECREF( par_old );
    par_old = NULL;

    CHECK_OBJECT( (PyObject *)par_new );
    Py_DECREF( par_new );
    par_new = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_old );
    Py_DECREF( par_old );
    par_old = NULL;

    CHECK_OBJECT( (PyObject *)par_new );
    Py_DECREF( par_new );
    par_new = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_2__kernel_manager_class_changed );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_3__kernel_manager_factory_default( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_206c93c6a9c6970c037f0b760bc76ef5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_206c93c6a9c6970c037f0b760bc76ef5 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_206c93c6a9c6970c037f0b760bc76ef5, codeobj_206c93c6a9c6970c037f0b760bc76ef5, module_jupyter_client$multikernelmanager, sizeof(void *) );
    frame_206c93c6a9c6970c037f0b760bc76ef5 = cache_frame_206c93c6a9c6970c037f0b760bc76ef5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_206c93c6a9c6970c037f0b760bc76ef5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_206c93c6a9c6970c037f0b760bc76ef5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_import_item );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_import_item );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "import_item" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_kernel_manager_class );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_206c93c6a9c6970c037f0b760bc76ef5->m_frame.f_lineno = 62;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 62;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_206c93c6a9c6970c037f0b760bc76ef5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_206c93c6a9c6970c037f0b760bc76ef5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_206c93c6a9c6970c037f0b760bc76ef5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_206c93c6a9c6970c037f0b760bc76ef5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_206c93c6a9c6970c037f0b760bc76ef5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_206c93c6a9c6970c037f0b760bc76ef5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_206c93c6a9c6970c037f0b760bc76ef5,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_206c93c6a9c6970c037f0b760bc76ef5 == cache_frame_206c93c6a9c6970c037f0b760bc76ef5 )
    {
        Py_DECREF( frame_206c93c6a9c6970c037f0b760bc76ef5 );
    }
    cache_frame_206c93c6a9c6970c037f0b760bc76ef5 = NULL;

    assertFrameObject( frame_206c93c6a9c6970c037f0b760bc76ef5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_3__kernel_manager_factory_default );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_3__kernel_manager_factory_default );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_4__context_default( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_68e6b1c95c9b164a68a9a231b167e4cb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_68e6b1c95c9b164a68a9a231b167e4cb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_68e6b1c95c9b164a68a9a231b167e4cb, codeobj_68e6b1c95c9b164a68a9a231b167e4cb, module_jupyter_client$multikernelmanager, sizeof(void *) );
    frame_68e6b1c95c9b164a68a9a231b167e4cb = cache_frame_68e6b1c95c9b164a68a9a231b167e4cb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_68e6b1c95c9b164a68a9a231b167e4cb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_68e6b1c95c9b164a68a9a231b167e4cb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_zmq );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_zmq );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "zmq" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 66;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Context );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_68e6b1c95c9b164a68a9a231b167e4cb->m_frame.f_lineno = 66;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_instance );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_68e6b1c95c9b164a68a9a231b167e4cb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_68e6b1c95c9b164a68a9a231b167e4cb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_68e6b1c95c9b164a68a9a231b167e4cb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_68e6b1c95c9b164a68a9a231b167e4cb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_68e6b1c95c9b164a68a9a231b167e4cb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_68e6b1c95c9b164a68a9a231b167e4cb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_68e6b1c95c9b164a68a9a231b167e4cb,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_68e6b1c95c9b164a68a9a231b167e4cb == cache_frame_68e6b1c95c9b164a68a9a231b167e4cb )
    {
        Py_DECREF( frame_68e6b1c95c9b164a68a9a231b167e4cb );
    }
    cache_frame_68e6b1c95c9b164a68a9a231b167e4cb = NULL;

    assertFrameObject( frame_68e6b1c95c9b164a68a9a231b167e4cb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_4__context_default );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_4__context_default );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_5_list_kernel_ids( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_ed13d83aff49c106da38fa36cbf3fb60;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ed13d83aff49c106da38fa36cbf3fb60 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ed13d83aff49c106da38fa36cbf3fb60, codeobj_ed13d83aff49c106da38fa36cbf3fb60, module_jupyter_client$multikernelmanager, sizeof(void *) );
    frame_ed13d83aff49c106da38fa36cbf3fb60 = cache_frame_ed13d83aff49c106da38fa36cbf3fb60;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ed13d83aff49c106da38fa36cbf3fb60 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ed13d83aff49c106da38fa36cbf3fb60 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__kernels );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_ed13d83aff49c106da38fa36cbf3fb60->m_frame.f_lineno = 76;
        tmp_list_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_keys );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed13d83aff49c106da38fa36cbf3fb60 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed13d83aff49c106da38fa36cbf3fb60 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ed13d83aff49c106da38fa36cbf3fb60 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ed13d83aff49c106da38fa36cbf3fb60, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ed13d83aff49c106da38fa36cbf3fb60->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ed13d83aff49c106da38fa36cbf3fb60, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ed13d83aff49c106da38fa36cbf3fb60,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_ed13d83aff49c106da38fa36cbf3fb60 == cache_frame_ed13d83aff49c106da38fa36cbf3fb60 )
    {
        Py_DECREF( frame_ed13d83aff49c106da38fa36cbf3fb60 );
    }
    cache_frame_ed13d83aff49c106da38fa36cbf3fb60 = NULL;

    assertFrameObject( frame_ed13d83aff49c106da38fa36cbf3fb60 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_5_list_kernel_ids );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_5_list_kernel_ids );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_6___len__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_4c41c571d19bcda30fbaf8e875096608;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_4c41c571d19bcda30fbaf8e875096608 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4c41c571d19bcda30fbaf8e875096608, codeobj_4c41c571d19bcda30fbaf8e875096608, module_jupyter_client$multikernelmanager, sizeof(void *) );
    frame_4c41c571d19bcda30fbaf8e875096608 = cache_frame_4c41c571d19bcda30fbaf8e875096608;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4c41c571d19bcda30fbaf8e875096608 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4c41c571d19bcda30fbaf8e875096608 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_len_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_4c41c571d19bcda30fbaf8e875096608->m_frame.f_lineno = 80;
        tmp_len_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_list_kernel_ids );
        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4c41c571d19bcda30fbaf8e875096608 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4c41c571d19bcda30fbaf8e875096608 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4c41c571d19bcda30fbaf8e875096608 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4c41c571d19bcda30fbaf8e875096608, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4c41c571d19bcda30fbaf8e875096608->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4c41c571d19bcda30fbaf8e875096608, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4c41c571d19bcda30fbaf8e875096608,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_4c41c571d19bcda30fbaf8e875096608 == cache_frame_4c41c571d19bcda30fbaf8e875096608 )
    {
        Py_DECREF( frame_4c41c571d19bcda30fbaf8e875096608 );
    }
    cache_frame_4c41c571d19bcda30fbaf8e875096608 = NULL;

    assertFrameObject( frame_4c41c571d19bcda30fbaf8e875096608 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_6___len__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_6___len__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_7___contains__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_745e33e47b918bdea2ada8903cf9f9e3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_745e33e47b918bdea2ada8903cf9f9e3 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_745e33e47b918bdea2ada8903cf9f9e3, codeobj_745e33e47b918bdea2ada8903cf9f9e3, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *) );
    frame_745e33e47b918bdea2ada8903cf9f9e3 = cache_frame_745e33e47b918bdea2ada8903cf9f9e3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_745e33e47b918bdea2ada8903cf9f9e3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_745e33e47b918bdea2ada8903cf9f9e3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_kernel_id );
        tmp_compexpr_left_1 = par_kernel_id;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__kernels );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res == 1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_745e33e47b918bdea2ada8903cf9f9e3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_745e33e47b918bdea2ada8903cf9f9e3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_745e33e47b918bdea2ada8903cf9f9e3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_745e33e47b918bdea2ada8903cf9f9e3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_745e33e47b918bdea2ada8903cf9f9e3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_745e33e47b918bdea2ada8903cf9f9e3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_745e33e47b918bdea2ada8903cf9f9e3,
        type_description_1,
        par_self,
        par_kernel_id
    );


    // Release cached frame.
    if ( frame_745e33e47b918bdea2ada8903cf9f9e3 == cache_frame_745e33e47b918bdea2ada8903cf9f9e3 )
    {
        Py_DECREF( frame_745e33e47b918bdea2ada8903cf9f9e3 );
    }
    cache_frame_745e33e47b918bdea2ada8903cf9f9e3 = NULL;

    assertFrameObject( frame_745e33e47b918bdea2ada8903cf9f9e3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_7___contains__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_7___contains__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_8_start_kernel( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_name = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_kernel_id = NULL;
    PyObject *var_constructor_kwargs = NULL;
    PyObject *var_km = NULL;
    struct Nuitka_FrameObject *frame_eb7db7b6021c9b064a50c251fbed9ce2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_eb7db7b6021c9b064a50c251fbed9ce2 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_eb7db7b6021c9b064a50c251fbed9ce2, codeobj_eb7db7b6021c9b064a50c251fbed9ce2, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_eb7db7b6021c9b064a50c251fbed9ce2 = cache_frame_eb7db7b6021c9b064a50c251fbed9ce2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_eb7db7b6021c9b064a50c251fbed9ce2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_eb7db7b6021c9b064a50c251fbed9ce2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_kwargs );
        tmp_source_name_1 = par_kwargs;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_pop );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = const_str_plain_kernel_id;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_unicode_type );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode_type );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode_type" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 93;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_uuid );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_uuid );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "uuid" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 93;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_2;
        frame_eb7db7b6021c9b064a50c251fbed9ce2->m_frame.f_lineno = 93;
        tmp_args_element_name_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_uuid4 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 93;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_eb7db7b6021c9b064a50c251fbed9ce2->m_frame.f_lineno = 93;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 93;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_eb7db7b6021c9b064a50c251fbed9ce2->m_frame.f_lineno = 93;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_kernel_id == NULL );
        var_kernel_id = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_kernel_id );
        tmp_compexpr_left_1 = var_kernel_id;
        CHECK_OBJECT( par_self );
        tmp_compexpr_right_1 = par_self;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_DuplicateKernelError );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DuplicateKernelError );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DuplicateKernelError" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 95;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_3 = tmp_mvar_value_3;
            tmp_left_name_1 = const_str_digest_5f5eef8a4f2775a5a6ebd47e7e7c625d;
            CHECK_OBJECT( var_kernel_id );
            tmp_right_name_1 = var_kernel_id;
            tmp_args_element_name_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 95;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            frame_eb7db7b6021c9b064a50c251fbed9ce2->m_frame.f_lineno = 95;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 95;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 95;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_kernel_name );
        tmp_compexpr_left_2 = par_kernel_name;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_default_kernel_name );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 98;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_kernel_name;
                assert( old != NULL );
                par_kernel_name = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = PyDict_New();
        assert( var_constructor_kwargs == NULL );
        var_constructor_kwargs = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_kernel_spec_manager );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 103;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_dictset_value = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_kernel_spec_manager );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_constructor_kwargs );
            tmp_dictset_dict = var_constructor_kwargs;
            tmp_dictset_key = const_str_plain_kernel_spec_manager;
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            assert( !(tmp_res != 0) );
        }
        branch_no_3:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_6;
        PyObject *tmp_source_name_7;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_source_name_8;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_source_name_9;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_kernel_manager_factory );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_connection_file;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_dircall_arg1_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 105;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_7 = tmp_mvar_value_4;
        tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_path );
        if ( tmp_source_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_dircall_arg1_1 );

            exception_lineno = 105;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_join );
        Py_DECREF( tmp_source_name_6 );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_dircall_arg1_1 );

            exception_lineno = 105;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_8 = par_self;
        tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_connection_dir );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_dircall_arg1_1 );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 106;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_2 = const_str_digest_187926d0ae58a603c01bbdd47a78211c;
        CHECK_OBJECT( var_kernel_id );
        tmp_right_name_2 = var_kernel_id;
        tmp_args_element_name_6 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_dircall_arg1_1 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_5 );

            exception_lineno = 106;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_eb7db7b6021c9b064a50c251fbed9ce2->m_frame.f_lineno = 105;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_5 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_dircall_arg1_1 );

            exception_lineno = 105;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg2_1 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_dircall_arg2_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_parent;
        CHECK_OBJECT( par_self );
        tmp_dict_value_2 = par_self;
        tmp_res = PyDict_SetItem( tmp_dircall_arg2_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_log;
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_log );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_dircall_arg1_1 );
            Py_DECREF( tmp_dircall_arg2_1 );

            exception_lineno = 107;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_dircall_arg2_1, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_kernel_name;
        CHECK_OBJECT( par_kernel_name );
        tmp_dict_value_4 = par_kernel_name;
        tmp_res = PyDict_SetItem( tmp_dircall_arg2_1, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        CHECK_OBJECT( var_constructor_kwargs );
        tmp_dircall_arg3_1 = var_constructor_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_4 = impl___internal__$$$function_10_complex_call_helper_keywords_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_km == NULL );
        var_km = tmp_assign_source_4;
    }
    {
        PyObject *tmp_dircall_arg1_2;
        PyObject *tmp_source_name_10;
        PyObject *tmp_dircall_arg2_2;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_km );
        tmp_source_name_10 = var_km;
        tmp_dircall_arg1_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_start_kernel );
        if ( tmp_dircall_arg1_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg2_2 = par_kwargs;
        Py_INCREF( tmp_dircall_arg2_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2};
            tmp_call_result_1 = impl___internal__$$$function_2_complex_call_helper_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_source_name_11;
        PyObject *tmp_ass_subscript_1;
        CHECK_OBJECT( var_km );
        tmp_ass_subvalue_1 = var_km;
        CHECK_OBJECT( par_self );
        tmp_source_name_11 = par_self;
        tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain__kernels );
        if ( tmp_ass_subscribed_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_kernel_id );
        tmp_ass_subscript_1 = var_kernel_id;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        Py_DECREF( tmp_ass_subscribed_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb7db7b6021c9b064a50c251fbed9ce2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_eb7db7b6021c9b064a50c251fbed9ce2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_eb7db7b6021c9b064a50c251fbed9ce2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_eb7db7b6021c9b064a50c251fbed9ce2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_eb7db7b6021c9b064a50c251fbed9ce2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_eb7db7b6021c9b064a50c251fbed9ce2,
        type_description_1,
        par_self,
        par_kernel_name,
        par_kwargs,
        var_kernel_id,
        var_constructor_kwargs,
        var_km
    );


    // Release cached frame.
    if ( frame_eb7db7b6021c9b064a50c251fbed9ce2 == cache_frame_eb7db7b6021c9b064a50c251fbed9ce2 )
    {
        Py_DECREF( frame_eb7db7b6021c9b064a50c251fbed9ce2 );
    }
    cache_frame_eb7db7b6021c9b064a50c251fbed9ce2 = NULL;

    assertFrameObject( frame_eb7db7b6021c9b064a50c251fbed9ce2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_kernel_id );
    tmp_return_value = var_kernel_id;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_8_start_kernel );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_name );
    Py_DECREF( par_kernel_name );
    par_kernel_name = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_kernel_id );
    Py_DECREF( var_kernel_id );
    var_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)var_constructor_kwargs );
    Py_DECREF( var_constructor_kwargs );
    var_constructor_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_km );
    Py_DECREF( var_km );
    var_km = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_kernel_name );
    par_kernel_name = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_kernel_id );
    var_kernel_id = NULL;

    Py_XDECREF( var_constructor_kwargs );
    var_constructor_kwargs = NULL;

    Py_XDECREF( var_km );
    var_km = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_8_start_kernel );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_9_shutdown_kernel( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_now = python_pars[ 2 ];
    PyObject *par_restart = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_1ea163f6d2cb33193954fb9c9e02855e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1ea163f6d2cb33193954fb9c9e02855e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1ea163f6d2cb33193954fb9c9e02855e, codeobj_1ea163f6d2cb33193954fb9c9e02855e, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1ea163f6d2cb33193954fb9c9e02855e = cache_frame_1ea163f6d2cb33193954fb9c9e02855e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1ea163f6d2cb33193954fb9c9e02855e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1ea163f6d2cb33193954fb9c9e02855e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_log );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_info );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = const_str_digest_4e83119e4807363e8041fe90f23e9902;
        CHECK_OBJECT( par_kernel_id );
        tmp_right_name_1 = par_kernel_id;
        tmp_args_element_name_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 127;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_1ea163f6d2cb33193954fb9c9e02855e->m_frame.f_lineno = 127;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_kernel_id );
        tmp_args_element_name_2 = par_kernel_id;
        frame_1ea163f6d2cb33193954fb9c9e02855e->m_frame.f_lineno = 128;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_remove_kernel, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1ea163f6d2cb33193954fb9c9e02855e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1ea163f6d2cb33193954fb9c9e02855e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1ea163f6d2cb33193954fb9c9e02855e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1ea163f6d2cb33193954fb9c9e02855e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1ea163f6d2cb33193954fb9c9e02855e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1ea163f6d2cb33193954fb9c9e02855e,
        type_description_1,
        par_self,
        par_kernel_id,
        par_now,
        par_restart
    );


    // Release cached frame.
    if ( frame_1ea163f6d2cb33193954fb9c9e02855e == cache_frame_1ea163f6d2cb33193954fb9c9e02855e )
    {
        Py_DECREF( frame_1ea163f6d2cb33193954fb9c9e02855e );
    }
    cache_frame_1ea163f6d2cb33193954fb9c9e02855e = NULL;

    assertFrameObject( frame_1ea163f6d2cb33193954fb9c9e02855e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_9_shutdown_kernel );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_now );
    Py_DECREF( par_now );
    par_now = NULL;

    CHECK_OBJECT( (PyObject *)par_restart );
    Py_DECREF( par_restart );
    par_restart = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_now );
    Py_DECREF( par_now );
    par_now = NULL;

    CHECK_OBJECT( (PyObject *)par_restart );
    Py_DECREF( par_restart );
    par_restart = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_9_shutdown_kernel );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_10_request_shutdown( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_restart = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_10_request_shutdown );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_restart );
    Py_DECREF( par_restart );
    par_restart = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_restart );
    Py_DECREF( par_restart );
    par_restart = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_10_request_shutdown );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_11_finish_shutdown( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_waittime = python_pars[ 2 ];
    PyObject *par_pollinterval = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_6469ed134d824cdd1d8832cfbf217989;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6469ed134d824cdd1d8832cfbf217989 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6469ed134d824cdd1d8832cfbf217989, codeobj_6469ed134d824cdd1d8832cfbf217989, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6469ed134d824cdd1d8832cfbf217989 = cache_frame_6469ed134d824cdd1d8832cfbf217989;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6469ed134d824cdd1d8832cfbf217989 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6469ed134d824cdd1d8832cfbf217989 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_log );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_info );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = const_str_digest_4e83119e4807363e8041fe90f23e9902;
        CHECK_OBJECT( par_kernel_id );
        tmp_right_name_1 = par_kernel_id;
        tmp_args_element_name_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 138;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_6469ed134d824cdd1d8832cfbf217989->m_frame.f_lineno = 138;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6469ed134d824cdd1d8832cfbf217989 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6469ed134d824cdd1d8832cfbf217989 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6469ed134d824cdd1d8832cfbf217989, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6469ed134d824cdd1d8832cfbf217989->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6469ed134d824cdd1d8832cfbf217989, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6469ed134d824cdd1d8832cfbf217989,
        type_description_1,
        par_self,
        par_kernel_id,
        par_waittime,
        par_pollinterval
    );


    // Release cached frame.
    if ( frame_6469ed134d824cdd1d8832cfbf217989 == cache_frame_6469ed134d824cdd1d8832cfbf217989 )
    {
        Py_DECREF( frame_6469ed134d824cdd1d8832cfbf217989 );
    }
    cache_frame_6469ed134d824cdd1d8832cfbf217989 = NULL;

    assertFrameObject( frame_6469ed134d824cdd1d8832cfbf217989 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_11_finish_shutdown );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_waittime );
    Py_DECREF( par_waittime );
    par_waittime = NULL;

    CHECK_OBJECT( (PyObject *)par_pollinterval );
    Py_DECREF( par_pollinterval );
    par_pollinterval = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_waittime );
    Py_DECREF( par_waittime );
    par_waittime = NULL;

    CHECK_OBJECT( (PyObject *)par_pollinterval );
    Py_DECREF( par_pollinterval );
    par_pollinterval = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_11_finish_shutdown );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_12_cleanup( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_connection_file = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_12_cleanup );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_connection_file );
    Py_DECREF( par_connection_file );
    par_connection_file = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_connection_file );
    Py_DECREF( par_connection_file );
    par_connection_file = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_12_cleanup );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_13_remove_kernel( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_106d79cc7953f9b0459a6b5d8ba1725b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_106d79cc7953f9b0459a6b5d8ba1725b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_106d79cc7953f9b0459a6b5d8ba1725b, codeobj_106d79cc7953f9b0459a6b5d8ba1725b, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *) );
    frame_106d79cc7953f9b0459a6b5d8ba1725b = cache_frame_106d79cc7953f9b0459a6b5d8ba1725b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_106d79cc7953f9b0459a6b5d8ba1725b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_106d79cc7953f9b0459a6b5d8ba1725b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__kernels );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_kernel_id );
        tmp_args_element_name_1 = par_kernel_id;
        frame_106d79cc7953f9b0459a6b5d8ba1725b->m_frame.f_lineno = 152;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_pop, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_106d79cc7953f9b0459a6b5d8ba1725b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_106d79cc7953f9b0459a6b5d8ba1725b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_106d79cc7953f9b0459a6b5d8ba1725b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_106d79cc7953f9b0459a6b5d8ba1725b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_106d79cc7953f9b0459a6b5d8ba1725b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_106d79cc7953f9b0459a6b5d8ba1725b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_106d79cc7953f9b0459a6b5d8ba1725b,
        type_description_1,
        par_self,
        par_kernel_id
    );


    // Release cached frame.
    if ( frame_106d79cc7953f9b0459a6b5d8ba1725b == cache_frame_106d79cc7953f9b0459a6b5d8ba1725b )
    {
        Py_DECREF( frame_106d79cc7953f9b0459a6b5d8ba1725b );
    }
    cache_frame_106d79cc7953f9b0459a6b5d8ba1725b = NULL;

    assertFrameObject( frame_106d79cc7953f9b0459a6b5d8ba1725b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_13_remove_kernel );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_13_remove_kernel );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_14_shutdown_all( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_now = python_pars[ 1 ];
    PyObject *var_kids = NULL;
    PyObject *var_kid = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    struct Nuitka_FrameObject *frame_6d0ce4fd8e542168fad75aa9e5033624;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_6d0ce4fd8e542168fad75aa9e5033624 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6d0ce4fd8e542168fad75aa9e5033624, codeobj_6d0ce4fd8e542168fad75aa9e5033624, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6d0ce4fd8e542168fad75aa9e5033624 = cache_frame_6d0ce4fd8e542168fad75aa9e5033624;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6d0ce4fd8e542168fad75aa9e5033624 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6d0ce4fd8e542168fad75aa9e5033624 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_6d0ce4fd8e542168fad75aa9e5033624->m_frame.f_lineno = 156;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_list_kernel_ids );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_kids == NULL );
        var_kids = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( var_kids );
        tmp_iter_arg_1 = var_kids;
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 157;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_kid;
            var_kid = tmp_assign_source_4;
            Py_INCREF( var_kid );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        CHECK_OBJECT( var_kid );
        tmp_args_element_name_1 = var_kid;
        frame_6d0ce4fd8e542168fad75aa9e5033624->m_frame.f_lineno = 158;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_request_shutdown, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 157;
        type_description_1 = "oooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( var_kids );
        tmp_iter_arg_2 = var_kids;
        tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_2__for_iterator == NULL );
        tmp_for_loop_2__for_iterator = tmp_assign_source_5;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooo";
                exception_lineno = 159;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_7 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_kid;
            var_kid = tmp_assign_source_7;
            Py_INCREF( var_kid );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( par_self );
        tmp_called_instance_3 = par_self;
        CHECK_OBJECT( var_kid );
        tmp_args_element_name_2 = var_kid;
        frame_6d0ce4fd8e542168fad75aa9e5033624->m_frame.f_lineno = 160;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_finish_shutdown, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_instance_4;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( par_self );
        tmp_called_instance_4 = par_self;
        CHECK_OBJECT( var_kid );
        tmp_args_element_name_3 = var_kid;
        frame_6d0ce4fd8e542168fad75aa9e5033624->m_frame.f_lineno = 161;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_cleanup, call_args );
        }

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_instance_5;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( par_self );
        tmp_called_instance_5 = par_self;
        CHECK_OBJECT( var_kid );
        tmp_args_element_name_4 = var_kid;
        frame_6d0ce4fd8e542168fad75aa9e5033624->m_frame.f_lineno = 162;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_remove_kernel, call_args );
        }

        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 159;
        type_description_1 = "oooo";
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6d0ce4fd8e542168fad75aa9e5033624 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6d0ce4fd8e542168fad75aa9e5033624 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6d0ce4fd8e542168fad75aa9e5033624, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6d0ce4fd8e542168fad75aa9e5033624->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6d0ce4fd8e542168fad75aa9e5033624, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6d0ce4fd8e542168fad75aa9e5033624,
        type_description_1,
        par_self,
        par_now,
        var_kids,
        var_kid
    );


    // Release cached frame.
    if ( frame_6d0ce4fd8e542168fad75aa9e5033624 == cache_frame_6d0ce4fd8e542168fad75aa9e5033624 )
    {
        Py_DECREF( frame_6d0ce4fd8e542168fad75aa9e5033624 );
    }
    cache_frame_6d0ce4fd8e542168fad75aa9e5033624 = NULL;

    assertFrameObject( frame_6d0ce4fd8e542168fad75aa9e5033624 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_14_shutdown_all );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_now );
    Py_DECREF( par_now );
    par_now = NULL;

    CHECK_OBJECT( (PyObject *)var_kids );
    Py_DECREF( var_kids );
    var_kids = NULL;

    Py_XDECREF( var_kid );
    var_kid = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_now );
    Py_DECREF( par_now );
    par_now = NULL;

    Py_XDECREF( var_kids );
    var_kids = NULL;

    Py_XDECREF( var_kid );
    var_kid = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_14_shutdown_all );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_15_interrupt_kernel( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_0d08f4f80c987c1986443a34d615e5e5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0d08f4f80c987c1986443a34d615e5e5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0d08f4f80c987c1986443a34d615e5e5, codeobj_0d08f4f80c987c1986443a34d615e5e5, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *) );
    frame_0d08f4f80c987c1986443a34d615e5e5 = cache_frame_0d08f4f80c987c1986443a34d615e5e5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0d08f4f80c987c1986443a34d615e5e5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0d08f4f80c987c1986443a34d615e5e5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_log );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_info );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = const_str_digest_2c016138d0cf1800dae1903e4fd03ca6;
        CHECK_OBJECT( par_kernel_id );
        tmp_right_name_1 = par_kernel_id;
        tmp_args_element_name_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 173;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_0d08f4f80c987c1986443a34d615e5e5->m_frame.f_lineno = 173;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0d08f4f80c987c1986443a34d615e5e5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0d08f4f80c987c1986443a34d615e5e5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0d08f4f80c987c1986443a34d615e5e5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0d08f4f80c987c1986443a34d615e5e5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0d08f4f80c987c1986443a34d615e5e5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0d08f4f80c987c1986443a34d615e5e5,
        type_description_1,
        par_self,
        par_kernel_id
    );


    // Release cached frame.
    if ( frame_0d08f4f80c987c1986443a34d615e5e5 == cache_frame_0d08f4f80c987c1986443a34d615e5e5 )
    {
        Py_DECREF( frame_0d08f4f80c987c1986443a34d615e5e5 );
    }
    cache_frame_0d08f4f80c987c1986443a34d615e5e5 = NULL;

    assertFrameObject( frame_0d08f4f80c987c1986443a34d615e5e5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_15_interrupt_kernel );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_15_interrupt_kernel );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_16_signal_kernel( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_signum = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_54e56d3a3f639351e1f35c21ec2306b5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_54e56d3a3f639351e1f35c21ec2306b5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_54e56d3a3f639351e1f35c21ec2306b5, codeobj_54e56d3a3f639351e1f35c21ec2306b5, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_54e56d3a3f639351e1f35c21ec2306b5 = cache_frame_54e56d3a3f639351e1f35c21ec2306b5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_54e56d3a3f639351e1f35c21ec2306b5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_54e56d3a3f639351e1f35c21ec2306b5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_log );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_info );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = const_str_digest_656a811c105fc57fb43e14e6d24ac599;
        CHECK_OBJECT( par_kernel_id );
        tmp_tuple_element_1 = par_kernel_id;
        tmp_right_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_signum );
        tmp_tuple_element_1 = par_signum;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_args_element_name_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 187;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_54e56d3a3f639351e1f35c21ec2306b5->m_frame.f_lineno = 187;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_54e56d3a3f639351e1f35c21ec2306b5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_54e56d3a3f639351e1f35c21ec2306b5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_54e56d3a3f639351e1f35c21ec2306b5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_54e56d3a3f639351e1f35c21ec2306b5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_54e56d3a3f639351e1f35c21ec2306b5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_54e56d3a3f639351e1f35c21ec2306b5,
        type_description_1,
        par_self,
        par_kernel_id,
        par_signum
    );


    // Release cached frame.
    if ( frame_54e56d3a3f639351e1f35c21ec2306b5 == cache_frame_54e56d3a3f639351e1f35c21ec2306b5 )
    {
        Py_DECREF( frame_54e56d3a3f639351e1f35c21ec2306b5 );
    }
    cache_frame_54e56d3a3f639351e1f35c21ec2306b5 = NULL;

    assertFrameObject( frame_54e56d3a3f639351e1f35c21ec2306b5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_16_signal_kernel );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_signum );
    Py_DECREF( par_signum );
    par_signum = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_signum );
    Py_DECREF( par_signum );
    par_signum = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_16_signal_kernel );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_17_restart_kernel( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_now = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_990c3b871a70f83038000474009046cb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_990c3b871a70f83038000474009046cb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_990c3b871a70f83038000474009046cb, codeobj_990c3b871a70f83038000474009046cb, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_990c3b871a70f83038000474009046cb = cache_frame_990c3b871a70f83038000474009046cb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_990c3b871a70f83038000474009046cb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_990c3b871a70f83038000474009046cb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_log );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_info );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = const_str_digest_fa77bd8a182b92708dbc44f1692368a3;
        CHECK_OBJECT( par_kernel_id );
        tmp_right_name_1 = par_kernel_id;
        tmp_args_element_name_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 198;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_990c3b871a70f83038000474009046cb->m_frame.f_lineno = 198;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_990c3b871a70f83038000474009046cb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_990c3b871a70f83038000474009046cb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_990c3b871a70f83038000474009046cb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_990c3b871a70f83038000474009046cb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_990c3b871a70f83038000474009046cb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_990c3b871a70f83038000474009046cb,
        type_description_1,
        par_self,
        par_kernel_id,
        par_now
    );


    // Release cached frame.
    if ( frame_990c3b871a70f83038000474009046cb == cache_frame_990c3b871a70f83038000474009046cb )
    {
        Py_DECREF( frame_990c3b871a70f83038000474009046cb );
    }
    cache_frame_990c3b871a70f83038000474009046cb = NULL;

    assertFrameObject( frame_990c3b871a70f83038000474009046cb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_17_restart_kernel );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_now );
    Py_DECREF( par_now );
    par_now = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_now );
    Py_DECREF( par_now );
    par_now = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_17_restart_kernel );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_18_is_alive( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_18_is_alive );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_18_is_alive );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_19__check_kernel_id( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_09a700113408f79bd78883ae54953e42;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_09a700113408f79bd78883ae54953e42 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_09a700113408f79bd78883ae54953e42, codeobj_09a700113408f79bd78883ae54953e42, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *) );
    frame_09a700113408f79bd78883ae54953e42 = cache_frame_09a700113408f79bd78883ae54953e42;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_09a700113408f79bd78883ae54953e42 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_09a700113408f79bd78883ae54953e42 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_kernel_id );
        tmp_compexpr_left_1 = par_kernel_id;
        CHECK_OBJECT( par_self );
        tmp_compexpr_right_1 = par_self;
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 215;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            tmp_left_name_1 = const_str_digest_71acf10d398406a11e9af6c497875bd7;
            CHECK_OBJECT( par_kernel_id );
            tmp_right_name_1 = par_kernel_id;
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 216;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_09a700113408f79bd78883ae54953e42->m_frame.f_lineno = 216;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_KeyError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 216;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_09a700113408f79bd78883ae54953e42 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_09a700113408f79bd78883ae54953e42 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_09a700113408f79bd78883ae54953e42, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_09a700113408f79bd78883ae54953e42->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_09a700113408f79bd78883ae54953e42, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_09a700113408f79bd78883ae54953e42,
        type_description_1,
        par_self,
        par_kernel_id
    );


    // Release cached frame.
    if ( frame_09a700113408f79bd78883ae54953e42 == cache_frame_09a700113408f79bd78883ae54953e42 )
    {
        Py_DECREF( frame_09a700113408f79bd78883ae54953e42 );
    }
    cache_frame_09a700113408f79bd78883ae54953e42 = NULL;

    assertFrameObject( frame_09a700113408f79bd78883ae54953e42 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_19__check_kernel_id );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_19__check_kernel_id );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_20_get_kernel( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_a44c5262b57b510c3386083cc64b01fd;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_a44c5262b57b510c3386083cc64b01fd = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a44c5262b57b510c3386083cc64b01fd, codeobj_a44c5262b57b510c3386083cc64b01fd, module_jupyter_client$multikernelmanager, sizeof(void *)+sizeof(void *) );
    frame_a44c5262b57b510c3386083cc64b01fd = cache_frame_a44c5262b57b510c3386083cc64b01fd;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a44c5262b57b510c3386083cc64b01fd );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a44c5262b57b510c3386083cc64b01fd ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        CHECK_OBJECT( par_kernel_id );
        tmp_args_element_name_1 = par_kernel_id;
        frame_a44c5262b57b510c3386083cc64b01fd->m_frame.f_lineno = 226;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain__check_kernel_id, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 226;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__kernels );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_kernel_id );
        tmp_subscript_name_1 = par_kernel_id;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a44c5262b57b510c3386083cc64b01fd );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a44c5262b57b510c3386083cc64b01fd );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a44c5262b57b510c3386083cc64b01fd );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a44c5262b57b510c3386083cc64b01fd, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a44c5262b57b510c3386083cc64b01fd->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a44c5262b57b510c3386083cc64b01fd, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a44c5262b57b510c3386083cc64b01fd,
        type_description_1,
        par_self,
        par_kernel_id
    );


    // Release cached frame.
    if ( frame_a44c5262b57b510c3386083cc64b01fd == cache_frame_a44c5262b57b510c3386083cc64b01fd )
    {
        Py_DECREF( frame_a44c5262b57b510c3386083cc64b01fd );
    }
    cache_frame_a44c5262b57b510c3386083cc64b01fd = NULL;

    assertFrameObject( frame_a44c5262b57b510c3386083cc64b01fd );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_20_get_kernel );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_20_get_kernel );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_21_add_restart_callback( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_callback = python_pars[ 2 ];
    PyObject *par_event = python_pars[ 3 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_21_add_restart_callback );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_callback );
    Py_DECREF( par_callback );
    par_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_callback );
    Py_DECREF( par_callback );
    par_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_21_add_restart_callback );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_22_remove_restart_callback( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_callback = python_pars[ 2 ];
    PyObject *par_event = python_pars[ 3 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_22_remove_restart_callback );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_callback );
    Py_DECREF( par_callback );
    par_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_callback );
    Py_DECREF( par_callback );
    par_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_22_remove_restart_callback );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_23_get_connection_info( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_23_get_connection_info );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_23_get_connection_info );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_24_connect_iopub( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_identity = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_24_connect_iopub );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_identity );
    Py_DECREF( par_identity );
    par_identity = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_identity );
    Py_DECREF( par_identity );
    par_identity = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_24_connect_iopub );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_25_connect_shell( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_identity = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_25_connect_shell );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_identity );
    Py_DECREF( par_identity );
    par_identity = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_identity );
    Py_DECREF( par_identity );
    par_identity = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_25_connect_shell );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_26_connect_stdin( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_identity = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_26_connect_stdin );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_identity );
    Py_DECREF( par_identity );
    par_identity = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_identity );
    Py_DECREF( par_identity );
    par_identity = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_26_connect_stdin );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jupyter_client$multikernelmanager$$$function_27_connect_hb( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_kernel_id = python_pars[ 1 ];
    PyObject *par_identity = python_pars[ 2 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_27_connect_hb );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_identity );
    Py_DECREF( par_identity );
    par_identity = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_kernel_id );
    Py_DECREF( par_kernel_id );
    par_kernel_id = NULL;

    CHECK_OBJECT( (PyObject *)par_identity );
    Py_DECREF( par_identity );
    par_identity = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager$$$function_27_connect_hb );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_10_request_shutdown( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_10_request_shutdown,
        const_str_plain_request_shutdown,
#if PYTHON_VERSION >= 300
        const_str_digest_3c84c6eb44f362defbe50b8d6ebcfe2a,
#endif
        codeobj_710e9ec8f085e19a804b9827ab1dd424,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_22861c41e2217e918abd7dc10f46cbd7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_11_finish_shutdown( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_11_finish_shutdown,
        const_str_plain_finish_shutdown,
#if PYTHON_VERSION >= 300
        const_str_digest_8f750d9ab041f2063a8a20b100915376,
#endif
        codeobj_6469ed134d824cdd1d8832cfbf217989,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_24eb43422c2b998699a14f7d1b94f9be,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_12_cleanup( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_12_cleanup,
        const_str_plain_cleanup,
#if PYTHON_VERSION >= 300
        const_str_digest_43cb031700d556e6e3708de5b2a7adec,
#endif
        codeobj_64bb76649e50f0c8ed12e4efae021afc,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_735de15a0f9cc7ceae2d0ccd8cf0012c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_13_remove_kernel(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_13_remove_kernel,
        const_str_plain_remove_kernel,
#if PYTHON_VERSION >= 300
        const_str_digest_416ab4e9c294a0bf35209fcddba938c5,
#endif
        codeobj_106d79cc7953f9b0459a6b5d8ba1725b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_de79b2f753a09f78e2ec63dbde59a8c6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_14_shutdown_all( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_14_shutdown_all,
        const_str_plain_shutdown_all,
#if PYTHON_VERSION >= 300
        const_str_digest_7848684c32bc8895da7055d1e340d42a,
#endif
        codeobj_6d0ce4fd8e542168fad75aa9e5033624,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_a84fe30dba4618157be829ad273ee7ab,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_15_interrupt_kernel(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_15_interrupt_kernel,
        const_str_plain_interrupt_kernel,
#if PYTHON_VERSION >= 300
        const_str_digest_a90a9c4fa1533a2736b5bb095ebee9e3,
#endif
        codeobj_0d08f4f80c987c1986443a34d615e5e5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_59a7a93a22f3fc467128e61ce88ee3cb,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_16_signal_kernel(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_16_signal_kernel,
        const_str_plain_signal_kernel,
#if PYTHON_VERSION >= 300
        const_str_digest_e947ff718933c357fc56cbc61f9b7130,
#endif
        codeobj_54e56d3a3f639351e1f35c21ec2306b5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_78ebd725b917784d162be40d4f13d612,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_17_restart_kernel( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_17_restart_kernel,
        const_str_plain_restart_kernel,
#if PYTHON_VERSION >= 300
        const_str_digest_2756d4df6b0b2df7aaa48752692ba594,
#endif
        codeobj_990c3b871a70f83038000474009046cb,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_26284e431a918fbc2da2865fc52e946a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_18_is_alive(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_18_is_alive,
        const_str_plain_is_alive,
#if PYTHON_VERSION >= 300
        const_str_digest_4e08e4df4147c3abba1fb891c8c05185,
#endif
        codeobj_7e36d6ec9db270007aad0ffd8c8cdd32,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_942d63b2480dc86fc4906ec5e73f51f5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_19__check_kernel_id(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_19__check_kernel_id,
        const_str_plain__check_kernel_id,
#if PYTHON_VERSION >= 300
        const_str_digest_7f311be033ffce5fac41a48d46ea1da3,
#endif
        codeobj_09a700113408f79bd78883ae54953e42,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_11ef1cb4e1d1e4671ef1e217b69bd3f5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_1_kernel_method(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_1_kernel_method,
        const_str_plain_kernel_method,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2664207bff25b0f478ed20473be20ae3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_90456e7f24418e0fe671eeb1b317f6e4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_1_kernel_method$$$function_1_wrapped(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_1_kernel_method$$$function_1_wrapped,
        const_str_plain_wrapped,
#if PYTHON_VERSION >= 300
        const_str_digest_47253d84ce90ad61fe62e851b2670fb4,
#endif
        codeobj_1ac81d3332e6a5391b4705fc1386961e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_20_get_kernel(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_20_get_kernel,
        const_str_plain_get_kernel,
#if PYTHON_VERSION >= 300
        const_str_digest_8978421be8f91781af7ce2289b2adef6,
#endif
        codeobj_a44c5262b57b510c3386083cc64b01fd,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_d7198416b586a9fd11e4c1be02223718,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_21_add_restart_callback( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_21_add_restart_callback,
        const_str_plain_add_restart_callback,
#if PYTHON_VERSION >= 300
        const_str_digest_8a230c74237862fe95fa7386df4b52a6,
#endif
        codeobj_0f5127f2ac035122bd36eaa78f7cf76e,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_42865928c7a79d7944586f09ad478b0a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_22_remove_restart_callback( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_22_remove_restart_callback,
        const_str_plain_remove_restart_callback,
#if PYTHON_VERSION >= 300
        const_str_digest_e712fe2b9fa57988fcb2a3aab9f85080,
#endif
        codeobj_f1ac5a464e278cc04934b779e94220c9,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_f26321dfb8e094ece8128da687882017,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_23_get_connection_info(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_23_get_connection_info,
        const_str_plain_get_connection_info,
#if PYTHON_VERSION >= 300
        const_str_digest_633bf33f696495f3934d819048299943,
#endif
        codeobj_99627676e86da22dc261797102b500be,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_21216dfe9d6a01d51632cfcd62bfaaec,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_24_connect_iopub( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_24_connect_iopub,
        const_str_plain_connect_iopub,
#if PYTHON_VERSION >= 300
        const_str_digest_1023dc1ed61ea43a8b0708aa8f3e4d60,
#endif
        codeobj_a5a1e43f357589d446cf6d2f3a662414,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_e8611e6ebae2f702245f8ba2b74a208e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_25_connect_shell( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_25_connect_shell,
        const_str_plain_connect_shell,
#if PYTHON_VERSION >= 300
        const_str_digest_8e86094e04988490a8703f89546292cf,
#endif
        codeobj_742428932fc41889ecec61a637aae234,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_84f941612cf3c2b03790c47b7f21ab83,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_26_connect_stdin( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_26_connect_stdin,
        const_str_plain_connect_stdin,
#if PYTHON_VERSION >= 300
        const_str_digest_8c51b352f892ef1c11cfd4bb72a6de84,
#endif
        codeobj_d48311f42f90b0fc8987ab6ce1c1e8d9,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_b3f6913fa34a3cc3d53a5e53ae57ff60,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_27_connect_hb( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_27_connect_hb,
        const_str_plain_connect_hb,
#if PYTHON_VERSION >= 300
        const_str_digest_07f1c830eeb0e5eac10974520e863a57,
#endif
        codeobj_ec9cbe4a9bf09d46a743f803fc567af3,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_7f33257446ecdc09da27efa8b1b5eabf,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_2__kernel_manager_class_changed(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_2__kernel_manager_class_changed,
        const_str_plain__kernel_manager_class_changed,
#if PYTHON_VERSION >= 300
        const_str_digest_8fa820fd86a703d81845291a1d9b2c36,
#endif
        codeobj_9cb0ac5f8d8a16dd941d66d35dd990ce,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_3__kernel_manager_factory_default(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_3__kernel_manager_factory_default,
        const_str_plain__kernel_manager_factory_default,
#if PYTHON_VERSION >= 300
        const_str_digest_008e1a9bff1b3a1b2a9a675f37a1654b,
#endif
        codeobj_206c93c6a9c6970c037f0b760bc76ef5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_4__context_default(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_4__context_default,
        const_str_plain__context_default,
#if PYTHON_VERSION >= 300
        const_str_digest_6aa63f4692615b1480969936aeb13257,
#endif
        codeobj_68e6b1c95c9b164a68a9a231b167e4cb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_5_list_kernel_ids(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_5_list_kernel_ids,
        const_str_plain_list_kernel_ids,
#if PYTHON_VERSION >= 300
        const_str_digest_c0aa17bc5807bd203ea556397e070832,
#endif
        codeobj_ed13d83aff49c106da38fa36cbf3fb60,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_17e8a030a8a53b34719766669bea3f99,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_6___len__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_6___len__,
        const_str_plain___len__,
#if PYTHON_VERSION >= 300
        const_str_digest_7add0b9f6c1a0a1dd90320a3c05e3399,
#endif
        codeobj_4c41c571d19bcda30fbaf8e875096608,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_cdc338ffebf21598b6b047c452af9ad7,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_7___contains__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_7___contains__,
        const_str_plain___contains__,
#if PYTHON_VERSION >= 300
        const_str_digest_0e0ddded2119d6a179c05dd7e937cb13,
#endif
        codeobj_745e33e47b918bdea2ada8903cf9f9e3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_8_start_kernel( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_8_start_kernel,
        const_str_plain_start_kernel,
#if PYTHON_VERSION >= 300
        const_str_digest_43b52cf86e191cb5b069631d56f54d11,
#endif
        codeobj_eb7db7b6021c9b064a50c251fbed9ce2,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_0216d87a24d8d6decebf25026a725e14,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_9_shutdown_kernel( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jupyter_client$multikernelmanager$$$function_9_shutdown_kernel,
        const_str_plain_shutdown_kernel,
#if PYTHON_VERSION >= 300
        const_str_digest_ce2e49a6736035532a56a8e3416e08c6,
#endif
        codeobj_1ea163f6d2cb33193954fb9c9e02855e,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jupyter_client$multikernelmanager,
        const_str_digest_17502559f98cdbd5004e19315c169fd1,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jupyter_client$multikernelmanager =
{
    PyModuleDef_HEAD_INIT,
    "jupyter_client.multikernelmanager",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jupyter_client$multikernelmanager)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jupyter_client$multikernelmanager)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jupyter_client$multikernelmanager );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jupyter_client.multikernelmanager: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jupyter_client.multikernelmanager: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jupyter_client.multikernelmanager: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjupyter_client$multikernelmanager" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jupyter_client$multikernelmanager = Py_InitModule4(
        "jupyter_client.multikernelmanager",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jupyter_client$multikernelmanager = PyModule_Create( &mdef_jupyter_client$multikernelmanager );
#endif

    moduledict_jupyter_client$multikernelmanager = MODULE_DICT( module_jupyter_client$multikernelmanager );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jupyter_client$multikernelmanager,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jupyter_client$multikernelmanager,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jupyter_client$multikernelmanager,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jupyter_client$multikernelmanager,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jupyter_client$multikernelmanager );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_2139417fa6f6500d7d7b32b0bcc5ef25, module_jupyter_client$multikernelmanager );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_91ccbcb52cbcabb69b899a938ea905e1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_jupyter_client$multikernelmanager_22 = NULL;
    PyObject *tmp_dictset_value;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *locals_jupyter_client$multikernelmanager_42 = NULL;
    struct Nuitka_FrameObject *frame_29eb16db1d6922f0ab18dd0af5d7452b_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_29eb16db1d6922f0ab18dd0af5d7452b_2 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_11a5b6f991aea211ac82cc8a784e8a99;
        UPDATE_STRING_DICT0( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_91ccbcb52cbcabb69b899a938ea905e1 = MAKE_MODULE_FRAME( codeobj_91ccbcb52cbcabb69b899a938ea905e1, module_jupyter_client$multikernelmanager );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_91ccbcb52cbcabb69b899a938ea905e1 );
    assert( Py_REFCNT( frame_91ccbcb52cbcabb69b899a938ea905e1 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 6;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_absolute_import );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_absolute_import, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_os;
        tmp_globals_name_1 = (PyObject *)moduledict_jupyter_client$multikernelmanager;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 8;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_uuid;
        tmp_globals_name_2 = (PyObject *)moduledict_jupyter_client$multikernelmanager;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 9;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_uuid, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_zmq;
        tmp_globals_name_3 = (PyObject *)moduledict_jupyter_client$multikernelmanager;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 11;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_zmq, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_c4d698c24b299b0741495b4b2af4b51a;
        tmp_globals_name_4 = (PyObject *)moduledict_jupyter_client$multikernelmanager;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_LoggingConfigurable_tuple;
        tmp_level_name_4 = const_int_0;
        frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 13;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_LoggingConfigurable );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_LoggingConfigurable, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_b1de2f4e7d17f11eaeb3062e52aa9890;
        tmp_globals_name_5 = (PyObject *)moduledict_jupyter_client$multikernelmanager;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_import_item_tuple;
        tmp_level_name_5 = const_int_0;
        frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 14;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_import_item );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_import_item, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_traitlets;
        tmp_globals_name_6 = (PyObject *)moduledict_jupyter_client$multikernelmanager;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_f2395a3d4926b02c306d5e63b4fee427_tuple;
        tmp_level_name_6 = const_int_0;
        frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 15;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_10;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_Instance );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_Instance, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_Dict );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_Dict, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_6 = tmp_import_from_1__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_List );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_List, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_7 = tmp_import_from_1__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_Unicode );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_Unicode, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_8 = tmp_import_from_1__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_Any );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_Any, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_9 = tmp_import_from_1__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_DottedObjectName );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_DottedObjectName, tmp_assign_source_16 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_10;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_34e3966f9679fde95e40f5931481245c;
        tmp_globals_name_7 = (PyObject *)moduledict_jupyter_client$multikernelmanager;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_unicode_type_tuple;
        tmp_level_name_7 = const_int_0;
        frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 18;
        tmp_import_name_from_10 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_unicode_type );
        Py_DECREF( tmp_import_name_from_10 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_unicode_type, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_plain_kernelspec;
        tmp_globals_name_8 = (PyObject *)moduledict_jupyter_client$multikernelmanager;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_NATIVE_KERNEL_NAME_str_plain_KernelSpecManager_tuple;
        tmp_level_name_8 = const_int_pos_1;
        frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 20;
        tmp_assign_source_18 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_18;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_11 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_11 ) )
        {
           tmp_assign_source_19 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_11,
                (PyObject *)moduledict_jupyter_client$multikernelmanager,
                const_str_plain_NATIVE_KERNEL_NAME,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_NATIVE_KERNEL_NAME );
        }

        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_NATIVE_KERNEL_NAME, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_12 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_12 ) )
        {
           tmp_assign_source_20 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_12,
                (PyObject *)moduledict_jupyter_client$multikernelmanager,
                const_str_plain_KernelSpecManager,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_KernelSpecManager );
        }

        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_KernelSpecManager, tmp_assign_source_20 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_Exception_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_21 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_21;
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_22;
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_3;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_3;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_3;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_3;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_23 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_23;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_3;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_3;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_24;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_3;
            }
            tmp_tuple_element_1 = const_str_plain_DuplicateKernelError;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 22;
            tmp_assign_source_24 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_3;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_24;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_3;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 22;

                    goto try_except_handler_3;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 22;

                    goto try_except_handler_3;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 22;

                    goto try_except_handler_3;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 22;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_3;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_25;
            tmp_assign_source_25 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_25;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_26;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_jupyter_client$multikernelmanager_22 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_2139417fa6f6500d7d7b32b0bcc5ef25;
        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_22, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_plain_DuplicateKernelError;
        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_22, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_5;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_Exception_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_5;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_Exception_tuple;
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_22, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_5;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_DuplicateKernelError;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_jupyter_client$multikernelmanager_22;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 22;
            tmp_assign_source_27 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_5;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_27;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_26 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_26 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        Py_DECREF( locals_jupyter_client$multikernelmanager_22 );
        locals_jupyter_client$multikernelmanager_22 = NULL;
        goto try_return_handler_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jupyter_client$multikernelmanager_22 );
        locals_jupyter_client$multikernelmanager_22 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 22;
        goto try_except_handler_3;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_DuplicateKernelError, tmp_assign_source_26 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_1_kernel_method(  );



        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method, tmp_assign_source_28 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_LoggingConfigurable );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LoggingConfigurable );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LoggingConfigurable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;

            goto try_except_handler_6;
        }

        tmp_tuple_element_4 = tmp_mvar_value_3;
        tmp_assign_source_29 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_assign_source_29, 0, tmp_tuple_element_4 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_29;
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_30 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_30;
    }
    {
        PyObject *tmp_assign_source_31;
        tmp_assign_source_31 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_32 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_32;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_6;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_6;
            }
            tmp_tuple_element_5 = const_str_plain_MultiKernelManager;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_5 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 42;
            tmp_assign_source_33 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_6;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_33;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_6;
            }
            tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_6;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_6 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 42;

                    goto try_except_handler_6;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_6 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 42;

                    goto try_except_handler_6;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_6 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 42;

                    goto try_except_handler_6;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 42;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_6;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_34;
            tmp_assign_source_34 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_34;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_35;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_jupyter_client$multikernelmanager_42 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_2139417fa6f6500d7d7b32b0bcc5ef25;
        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_8;
        }
        tmp_dictset_value = const_str_digest_c2bd822880a7f32d2afc88e4242f01d4;
        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_8;
        }
        tmp_dictset_value = const_str_plain_MultiKernelManager;
        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto try_except_handler_8;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_29eb16db1d6922f0ab18dd0af5d7452b_2, codeobj_29eb16db1d6922f0ab18dd0af5d7452b, module_jupyter_client$multikernelmanager, sizeof(void *) );
        frame_29eb16db1d6922f0ab18dd0af5d7452b_2 = cache_frame_29eb16db1d6922f0ab18dd0af5d7452b_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_29eb16db1d6922f0ab18dd0af5d7452b_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_29eb16db1d6922f0ab18dd0af5d7452b_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_kw_name_4;
            tmp_called_name_4 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_Unicode );

            if ( tmp_called_name_4 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_Unicode );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Unicode );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Unicode" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 45;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_4 = tmp_mvar_value_4;
                Py_INCREF( tmp_called_name_4 );
                }
            }

            tmp_tuple_element_7 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_NATIVE_KERNEL_NAME );

            if ( tmp_tuple_element_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_NATIVE_KERNEL_NAME );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NATIVE_KERNEL_NAME );
                }

                if ( tmp_mvar_value_5 == NULL )
                {
                    Py_DECREF( tmp_called_name_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NATIVE_KERNEL_NAME" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 45;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_tuple_element_7 = tmp_mvar_value_5;
                Py_INCREF( tmp_tuple_element_7 );
                }
            }

            tmp_args_name_4 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_7 );
            tmp_kw_name_4 = PyDict_Copy( const_dict_706716995e98c3113acb1b849e043636 );
            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 45;
            tmp_dictset_value = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_name_4 );
            Py_DECREF( tmp_kw_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_default_kernel_name, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_kw_name_5;
            tmp_called_name_5 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_Instance );

            if ( tmp_called_name_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_Instance );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Instance );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Instance" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 49;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_5 = tmp_mvar_value_6;
                Py_INCREF( tmp_called_name_5 );
                }
            }

            tmp_tuple_element_8 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_KernelSpecManager );

            if ( tmp_tuple_element_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_KernelSpecManager );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KernelSpecManager );
                }

                if ( tmp_mvar_value_7 == NULL )
                {
                    Py_DECREF( tmp_called_name_5 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "KernelSpecManager" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 49;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_tuple_element_8 = tmp_mvar_value_7;
                Py_INCREF( tmp_tuple_element_8 );
                }
            }

            tmp_args_name_5 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_8 );
            tmp_kw_name_5 = PyDict_Copy( const_dict_d9034e00b80a03cb8bb3a26cd519b4c0 );
            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 49;
            tmp_dictset_value = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_5 );
            Py_DECREF( tmp_kw_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_spec_manager, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_6;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_args_name_6;
            PyObject *tmp_kw_name_6;
            tmp_called_name_6 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_DottedObjectName );

            if ( tmp_called_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_DottedObjectName );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DottedObjectName );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DottedObjectName" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 51;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_6 = tmp_mvar_value_8;
                Py_INCREF( tmp_called_name_6 );
                }
            }

            tmp_args_name_6 = const_tuple_str_digest_b6e93d45ba9131909659533c46aa542f_tuple;
            tmp_kw_name_6 = PyDict_Copy( const_dict_0d183126925e861e63208132384d6690 );
            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 51;
            tmp_dictset_value = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_kw_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 51;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_manager_class, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 51;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_2__kernel_manager_class_changed(  );



        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain__kernel_manager_class_changed, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_called_name_7;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_kw_name_7;
            tmp_called_name_7 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_Any );

            if ( tmp_called_name_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_9 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 60;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_7 = tmp_mvar_value_9;
                Py_INCREF( tmp_called_name_7 );
                }
            }

            tmp_kw_name_7 = PyDict_Copy( const_dict_1ab8db4bc0dac411ceb46d7af52254d7 );
            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 60;
            tmp_dictset_value = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_7, tmp_kw_name_7 );
            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_kw_name_7 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_manager_factory, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_3__kernel_manager_factory_default(  );



        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain__kernel_manager_factory_default, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_called_name_8;
            PyObject *tmp_mvar_value_10;
            tmp_called_name_8 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_Instance );

            if ( tmp_called_name_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_Instance );

                if (unlikely( tmp_mvar_value_10 == NULL ))
                {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Instance );
                }

                if ( tmp_mvar_value_10 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Instance" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 64;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_8 = tmp_mvar_value_10;
                Py_INCREF( tmp_called_name_8 );
                }
            }

            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 64;
            tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_str_digest_1f09be22b4de59cb51b5667b622d5584_tuple, 0 ) );

            Py_DECREF( tmp_called_name_8 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_context, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_4__context_default(  );



        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain__context_default, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_called_name_9;
            PyObject *tmp_mvar_value_11;
            tmp_called_name_9 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_Unicode );

            if ( tmp_called_name_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_Unicode );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Unicode );
                }

                if ( tmp_mvar_value_11 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Unicode" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 68;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_9 = tmp_mvar_value_11;
                Py_INCREF( tmp_called_name_9 );
                }
            }

            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 68;
            tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_str_empty_tuple, 0 ) );

            Py_DECREF( tmp_called_name_9 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_connection_dir, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_10;
            PyObject *tmp_mvar_value_12;
            tmp_called_name_10 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_Dict );

            if ( tmp_called_name_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_12 == NULL ))
                {
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_12 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 70;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_10 = tmp_mvar_value_12;
                Py_INCREF( tmp_called_name_10 );
                }
            }

            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 70;
            tmp_dictset_value = CALL_FUNCTION_NO_ARGS( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_10 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain__kernels, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_5_list_kernel_ids(  );



        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_list_kernel_ids, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_6___len__(  );



        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain___len__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_7___contains__(  );



        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain___contains__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_8_start_kernel( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_start_kernel, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_11;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_defaults_2;
            tmp_called_name_11 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_13 == NULL ))
                {
                    tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_13 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 114;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_11 = tmp_mvar_value_13;
                Py_INCREF( tmp_called_name_11 );
                }
            }

            tmp_defaults_2 = const_tuple_false_false_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_args_element_name_1 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_9_shutdown_kernel( tmp_defaults_2 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 114;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
            }

            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 114;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_shutdown_kernel, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 114;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_12;
            PyObject *tmp_mvar_value_14;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_defaults_3;
            tmp_called_name_12 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_14 == NULL ))
                {
                    tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_14 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 130;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_12 = tmp_mvar_value_14;
                Py_INCREF( tmp_called_name_12 );
                }
            }

            tmp_defaults_3 = const_tuple_false_tuple;
            Py_INCREF( tmp_defaults_3 );
            tmp_args_element_name_2 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_10_request_shutdown( tmp_defaults_3 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 130;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
            }

            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_request_shutdown, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_13;
            PyObject *tmp_mvar_value_15;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_defaults_4;
            tmp_called_name_13 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_15 == NULL ))
                {
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_15 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 134;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_13 = tmp_mvar_value_15;
                Py_INCREF( tmp_called_name_13 );
                }
            }

            tmp_defaults_4 = const_tuple_none_float_0_1_tuple;
            Py_INCREF( tmp_defaults_4 );
            tmp_args_element_name_3 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_11_finish_shutdown( tmp_defaults_4 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 134;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
            }

            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_finish_shutdown, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_14;
            PyObject *tmp_mvar_value_16;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_defaults_5;
            tmp_called_name_14 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_16 == NULL ))
                {
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_16 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 140;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_14 = tmp_mvar_value_16;
                Py_INCREF( tmp_called_name_14 );
                }
            }

            tmp_defaults_5 = const_tuple_true_tuple;
            Py_INCREF( tmp_defaults_5 );
            tmp_args_element_name_4 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_12_cleanup( tmp_defaults_5 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 140;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
            }

            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_cleanup, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_13_remove_kernel(  );



        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_remove_kernel, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_6;
            tmp_defaults_6 = const_tuple_false_tuple;
            Py_INCREF( tmp_defaults_6 );
            tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_14_shutdown_all( tmp_defaults_6 );



            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_shutdown_all, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 154;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_15;
            PyObject *tmp_mvar_value_17;
            PyObject *tmp_args_element_name_5;
            tmp_called_name_15 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_17 == NULL ))
                {
                    tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_17 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 164;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_15 = tmp_mvar_value_17;
                Py_INCREF( tmp_called_name_15 );
                }
            }

            tmp_args_element_name_5 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_15_interrupt_kernel(  );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 164;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
            }

            Py_DECREF( tmp_called_name_15 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 164;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_interrupt_kernel, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 164;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_16;
            PyObject *tmp_mvar_value_18;
            PyObject *tmp_args_element_name_6;
            tmp_called_name_16 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_18 == NULL ))
                {
                    tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_18 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 175;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_16 = tmp_mvar_value_18;
                Py_INCREF( tmp_called_name_16 );
                }
            }

            tmp_args_element_name_6 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_16_signal_kernel(  );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 175;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
            }

            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_signal_kernel, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_17;
            PyObject *tmp_mvar_value_19;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_defaults_7;
            tmp_called_name_17 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_19 == NULL ))
                {
                    tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_19 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 189;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_17 = tmp_mvar_value_19;
                Py_INCREF( tmp_called_name_17 );
                }
            }

            tmp_defaults_7 = const_tuple_false_tuple;
            Py_INCREF( tmp_defaults_7 );
            tmp_args_element_name_7 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_17_restart_kernel( tmp_defaults_7 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 189;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, call_args );
            }

            Py_DECREF( tmp_called_name_17 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_restart_kernel, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_18;
            PyObject *tmp_mvar_value_20;
            PyObject *tmp_args_element_name_8;
            tmp_called_name_18 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_18 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_20 == NULL ))
                {
                    tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_20 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 200;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_18 = tmp_mvar_value_20;
                Py_INCREF( tmp_called_name_18 );
                }
            }

            tmp_args_element_name_8 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_18_is_alive(  );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 200;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
            }

            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 200;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_is_alive, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 200;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_19__check_kernel_id(  );



        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain__check_kernel_id, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_20_get_kernel(  );



        tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_get_kernel, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_called_name_19;
            PyObject *tmp_mvar_value_21;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_defaults_8;
            tmp_called_name_19 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_19 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_21 == NULL ))
                {
                    tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_21 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 229;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_19 = tmp_mvar_value_21;
                Py_INCREF( tmp_called_name_19 );
                }
            }

            tmp_defaults_8 = const_tuple_str_plain_restart_tuple;
            Py_INCREF( tmp_defaults_8 );
            tmp_args_element_name_9 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_21_add_restart_callback( tmp_defaults_8 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 229;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
            }

            Py_DECREF( tmp_called_name_19 );
            Py_DECREF( tmp_args_element_name_9 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 229;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_add_restart_callback, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 229;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_20;
            PyObject *tmp_mvar_value_22;
            PyObject *tmp_args_element_name_10;
            PyObject *tmp_defaults_9;
            tmp_called_name_20 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_22 == NULL ))
                {
                    tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_22 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 233;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_20 = tmp_mvar_value_22;
                Py_INCREF( tmp_called_name_20 );
                }
            }

            tmp_defaults_9 = const_tuple_str_plain_restart_tuple;
            Py_INCREF( tmp_defaults_9 );
            tmp_args_element_name_10 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_22_remove_restart_callback( tmp_defaults_9 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 233;
            {
                PyObject *call_args[] = { tmp_args_element_name_10 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
            }

            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_args_element_name_10 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 233;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_remove_restart_callback, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 233;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_21;
            PyObject *tmp_mvar_value_23;
            PyObject *tmp_args_element_name_11;
            tmp_called_name_21 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_21 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_23 == NULL ))
                {
                    tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_23 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 237;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_21 = tmp_mvar_value_23;
                Py_INCREF( tmp_called_name_21 );
                }
            }

            tmp_args_element_name_11 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_23_get_connection_info(  );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 237;
            {
                PyObject *call_args[] = { tmp_args_element_name_11 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, call_args );
            }

            Py_DECREF( tmp_called_name_21 );
            Py_DECREF( tmp_args_element_name_11 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 237;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_get_connection_info, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 237;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_22;
            PyObject *tmp_mvar_value_24;
            PyObject *tmp_args_element_name_12;
            PyObject *tmp_defaults_10;
            tmp_called_name_22 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_24 == NULL ))
                {
                    tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_24 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 255;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_22 = tmp_mvar_value_24;
                Py_INCREF( tmp_called_name_22 );
                }
            }

            tmp_defaults_10 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_10 );
            tmp_args_element_name_12 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_24_connect_iopub( tmp_defaults_10 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 255;
            {
                PyObject *call_args[] = { tmp_args_element_name_12 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
            }

            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_element_name_12 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 255;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_connect_iopub, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 255;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_23;
            PyObject *tmp_mvar_value_25;
            PyObject *tmp_args_element_name_13;
            PyObject *tmp_defaults_11;
            tmp_called_name_23 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_23 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_25 == NULL ))
                {
                    tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_25 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 271;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_23 = tmp_mvar_value_25;
                Py_INCREF( tmp_called_name_23 );
                }
            }

            tmp_defaults_11 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_11 );
            tmp_args_element_name_13 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_25_connect_shell( tmp_defaults_11 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 271;
            {
                PyObject *call_args[] = { tmp_args_element_name_13 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, call_args );
            }

            Py_DECREF( tmp_called_name_23 );
            Py_DECREF( tmp_args_element_name_13 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 271;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_connect_shell, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 271;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_24;
            PyObject *tmp_mvar_value_26;
            PyObject *tmp_args_element_name_14;
            PyObject *tmp_defaults_12;
            tmp_called_name_24 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_26 == NULL ))
                {
                    tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_26 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 287;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_24 = tmp_mvar_value_26;
                Py_INCREF( tmp_called_name_24 );
                }
            }

            tmp_defaults_12 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_12 );
            tmp_args_element_name_14 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_26_connect_stdin( tmp_defaults_12 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 287;
            {
                PyObject *call_args[] = { tmp_args_element_name_14 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, call_args );
            }

            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_args_element_name_14 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 287;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_connect_stdin, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 287;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_called_name_25;
            PyObject *tmp_mvar_value_27;
            PyObject *tmp_args_element_name_15;
            PyObject *tmp_defaults_13;
            tmp_called_name_25 = PyObject_GetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_kernel_method );

            if ( tmp_called_name_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_kernel_method );

                if (unlikely( tmp_mvar_value_27 == NULL ))
                {
                    tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_kernel_method );
                }

                if ( tmp_mvar_value_27 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "kernel_method" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 303;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_25 = tmp_mvar_value_27;
                Py_INCREF( tmp_called_name_25 );
                }
            }

            tmp_defaults_13 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_13 );
            tmp_args_element_name_15 = MAKE_FUNCTION_jupyter_client$multikernelmanager$$$function_27_connect_hb( tmp_defaults_13 );



            frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame.f_lineno = 303;
            {
                PyObject *call_args[] = { tmp_args_element_name_15 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, call_args );
            }

            Py_DECREF( tmp_called_name_25 );
            Py_DECREF( tmp_args_element_name_15 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 303;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain_connect_hb, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 303;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_29eb16db1d6922f0ab18dd0af5d7452b_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_29eb16db1d6922f0ab18dd0af5d7452b_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_29eb16db1d6922f0ab18dd0af5d7452b_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_29eb16db1d6922f0ab18dd0af5d7452b_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_29eb16db1d6922f0ab18dd0af5d7452b_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_29eb16db1d6922f0ab18dd0af5d7452b_2,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_29eb16db1d6922f0ab18dd0af5d7452b_2 == cache_frame_29eb16db1d6922f0ab18dd0af5d7452b_2 )
        {
            Py_DECREF( frame_29eb16db1d6922f0ab18dd0af5d7452b_2 );
        }
        cache_frame_29eb16db1d6922f0ab18dd0af5d7452b_2 = NULL;

        assertFrameObject( frame_29eb16db1d6922f0ab18dd0af5d7452b_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_8;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_8;
            }
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_jupyter_client$multikernelmanager_42, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_8;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_36;
            PyObject *tmp_called_name_26;
            PyObject *tmp_args_name_7;
            PyObject *tmp_tuple_element_9;
            PyObject *tmp_kw_name_8;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_26 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_9 = const_str_plain_MultiKernelManager;
            tmp_args_name_7 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_9 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_9 );
            tmp_tuple_element_9 = locals_jupyter_client$multikernelmanager_42;
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_7, 2, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_8 = tmp_class_creation_2__class_decl_dict;
            frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame.f_lineno = 42;
            tmp_assign_source_36 = CALL_FUNCTION( tmp_called_name_26, tmp_args_name_7, tmp_kw_name_8 );
            Py_DECREF( tmp_args_name_7 );
            if ( tmp_assign_source_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 42;

                goto try_except_handler_8;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_36;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_35 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_35 );
        goto try_return_handler_8;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        Py_DECREF( locals_jupyter_client$multikernelmanager_42 );
        locals_jupyter_client$multikernelmanager_42 = NULL;
        goto try_return_handler_7;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jupyter_client$multikernelmanager_42 );
        locals_jupyter_client$multikernelmanager_42 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_7;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jupyter_client$multikernelmanager );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 42;
        goto try_except_handler_6;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_jupyter_client$multikernelmanager, (Nuitka_StringObject *)const_str_plain_MultiKernelManager, tmp_assign_source_35 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_91ccbcb52cbcabb69b899a938ea905e1 );
#endif
    popFrameStack();

    assertFrameObject( frame_91ccbcb52cbcabb69b899a938ea905e1 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_91ccbcb52cbcabb69b899a938ea905e1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_91ccbcb52cbcabb69b899a938ea905e1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_91ccbcb52cbcabb69b899a938ea905e1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_91ccbcb52cbcabb69b899a938ea905e1, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;


    return MOD_RETURN_VALUE( module_jupyter_client$multikernelmanager );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
