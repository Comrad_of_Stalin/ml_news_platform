/* Generated code for Python module 'nbformat.v3.nbpy'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_nbformat$v3$nbpy" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_nbformat$v3$nbpy;
PyDictObject *moduledict_nbformat$v3$nbpy;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_nb;
extern PyObject *const_tuple_str_digest_215283e090e28401efd32b2cf0bf29ae_tuple;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_digest_0ef8517a2e0443cae5679ec66d74a519;
extern PyObject *const_str_digest_aab696e9dc4dd50c44cf2f1c57967b68;
extern PyObject *const_tuple_str_plain_input_tuple;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_new_text_cell;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_tuple_str_plain_line_tuple;
extern PyObject *const_str_plain_rwbase;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_group;
extern PyObject *const_tuple_str_plain_markdown_tuple;
extern PyObject *const_tuple_str_plain_raw_tuple;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_reads;
extern PyObject *const_str_plain_m;
extern PyObject *const_tuple_str_chr_35_tuple;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_input;
static PyObject *const_tuple_f25cbd52acbf24776e032c042f114b49_tuple;
extern PyObject *const_str_plain_strip;
extern PyObject *const_str_plain_NotebookReader;
extern PyObject *const_str_plain_splitlines;
extern PyObject *const_str_plain_codecell;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain_body;
extern PyObject *const_str_plain_new_worksheet;
extern PyObject *const_str_plain_read;
static PyObject *const_str_digest_08690a2008ca83674f75fd9195f70b7f;
extern PyObject *const_str_digest_bd2398fc2b513d07b5e5840b7bb6c7c9;
extern PyObject *const_str_digest_ff85db009e3011b9c6a1f745923678a5;
extern PyObject *const_list_str_digest_783cb5b32947f6074d02faf31f87f24e_str_empty_list;
extern PyObject *const_str_digest_43d123edad7181f56494a8200b84a35c;
static PyObject *const_str_digest_2e399016eb375b57f3c9a7ec17f9281d;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_tuple_str_plain_html_tuple;
extern PyObject *const_str_digest_215283e090e28401efd32b2cf0bf29ae;
extern PyObject *const_str_plain_extend;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_tuple_str_digest_efdace01816915012691e5b49febfd69_tuple;
extern PyObject *const_str_plain_nbbase;
extern PyObject *const_str_plain_s;
extern PyObject *const_tuple_str_plain_level_int_pos_1_tuple;
static PyObject *const_tuple_str_digest_08690a2008ca83674f75fd9195f70b7f_tuple;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_digest_6566768af533234b42110c37c6c377fd;
extern PyObject *const_str_plain_NotebookWriter;
static PyObject *const_str_digest_3a548363f730fd88b910bc5cb4e29675;
static PyObject *const_str_digest_49a2f4a86b9b100a47c479fad7247d4b;
extern PyObject *const_str_digest_f86266a3d8a5127d8277b95971ab8d55;
extern PyObject *const_str_chr_35;
extern PyObject *const_str_plain__encoding_declaration_re;
extern PyObject *const_str_plain_line;
extern PyObject *const_str_digest_b7a68a90094c6ec9dcd6f55b6e1f128e;
extern PyObject *const_str_plain_PyWriter;
extern PyObject *const_tuple_str_newline_tuple;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_digest_6b42a3b9404b51c5be86e5b0a3c164a9;
extern PyObject *const_str_plain_new_cell;
extern PyObject *const_str_plain_append;
static PyObject *const_tuple_458ee026cceff7f4c2727919faba6d0d_tuple;
extern PyObject *const_str_plain_raw;
extern PyObject *const_str_digest_057da886ac33ee92c36c3be89967b4ee;
extern PyObject *const_tuple_str_digest_783cb5b32947f6074d02faf31f87f24e_tuple;
extern PyObject *const_str_plain_PyReaderError;
extern PyObject *const_str_plain_nbformat_minor;
extern PyObject *const_tuple_str_plain_x_tuple;
extern PyObject *const_str_plain_compile;
extern PyObject *const_list_str_digest_0ef8517a2e0443cae5679ec66d74a519_str_empty_list;
extern PyObject *const_str_plain_match;
extern PyObject *const_list_str_digest_b7a68a90094c6ec9dcd6f55b6e1f128e_str_empty_list;
static PyObject *const_tuple_str_digest_43d74b3c7f8a8c18835b37ee35a1e6bc_tuple;
extern PyObject *const_str_plain_split_lines_into_blocks;
extern PyObject *const_tuple_str_empty_tuple;
extern PyObject *const_tuple_str_plain_NotebookReader_str_plain_NotebookWriter_tuple;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_lineno;
extern PyObject *const_tuple_str_plain_level_tuple;
static PyObject *const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple;
extern PyObject *const_tuple_str_digest_0ef8517a2e0443cae5679ec66d74a519_tuple;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_heading;
static PyObject *const_str_digest_43d74b3c7f8a8c18835b37ee35a1e6bc;
extern PyObject *const_tuple_str_plain_self_str_plain_s_str_plain_kwargs_tuple;
extern PyObject *const_str_plain_code;
static PyObject *const_list_str_digest_efdace01816915012691e5b49febfd69_str_empty_list;
static PyObject *const_str_digest_fc7467a3720075c2bc06bb415a7982bb;
extern PyObject *const_str_plain_cell_type;
extern PyObject *const_str_plain_text;
extern PyObject *const_str_plain_new_heading_cell;
extern PyObject *const_str_plain_new_lines;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_htmlcell;
extern PyObject *const_str_plain_ws;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_html;
extern PyObject *const_str_plain_x;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_cell;
extern PyObject *const_str_plain_cell_lines;
extern PyObject *const_str_plain__reader;
extern PyObject *const_str_digest_0c4e933b6de95532d7ad0f14039a6e76;
extern PyObject *const_str_plain_type;
static PyObject *const_str_digest_a1ccde01d3ada8afb4b652f98ef1467b;
extern PyObject *const_str_plain_cells;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_ast;
extern PyObject *const_tuple_type_Exception_tuple;
extern PyObject *const_tuple_str_digest_aab696e9dc4dd50c44cf2f1c57967b68_tuple;
static PyObject *const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_source;
extern PyObject *const_str_plain__remove_comments;
extern PyObject *const_str_digest_eab93e76d9a19da3152998dbdc1780a9;
static PyObject *const_str_digest_efdace01816915012691e5b49febfd69;
extern PyObject *const_str_plain_nbformat;
extern PyObject *const_tuple_str_plain_source_tuple;
extern PyObject *const_list_str_digest_bd2398fc2b513d07b5e5840b7bb6c7c9_list;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_writes;
static PyObject *const_str_plain_headingcell;
extern PyObject *const_str_plain_worksheets;
extern PyObject *const_str_newline;
extern PyObject *const_str_plain_PyReader;
extern PyObject *const_str_plain_state;
extern PyObject *const_slice_int_pos_2_none_none;
extern PyObject *const_tuple_de33fc8d485e350dddf3e8614527e071_tuple;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_level;
extern PyObject *const_str_plain_to_notebook;
extern PyObject *const_str_plain_parse;
extern PyObject *const_str_plain_self;
static PyObject *const_str_digest_8a80611d57ef5e7ff5b3544d5b831791;
extern PyObject *const_str_plain_lines;
extern PyObject *const_str_plain_write;
extern PyObject *const_tuple_9eb4762e2735231377088987105a5e17_tuple;
extern PyObject *const_str_digest_783cb5b32947f6074d02faf31f87f24e;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_new_notebook;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_new_code_cell;
extern PyObject *const_str_plain_markdowncell;
extern PyObject *const_str_plain__writer;
extern PyObject *const_str_plain_startswith;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_starts;
static PyObject *const_str_plain_rawcell;
extern PyObject *const_str_plain_markdown;
extern PyObject *const_tuple_str_digest_b7a68a90094c6ec9dcd6f55b6e1f128e_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_f25cbd52acbf24776e032c042f114b49_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_f25cbd52acbf24776e032c042f114b49_tuple, 0, const_str_plain_new_code_cell ); Py_INCREF( const_str_plain_new_code_cell );
    PyTuple_SET_ITEM( const_tuple_f25cbd52acbf24776e032c042f114b49_tuple, 1, const_str_plain_new_text_cell ); Py_INCREF( const_str_plain_new_text_cell );
    PyTuple_SET_ITEM( const_tuple_f25cbd52acbf24776e032c042f114b49_tuple, 2, const_str_plain_new_worksheet ); Py_INCREF( const_str_plain_new_worksheet );
    PyTuple_SET_ITEM( const_tuple_f25cbd52acbf24776e032c042f114b49_tuple, 3, const_str_plain_new_notebook ); Py_INCREF( const_str_plain_new_notebook );
    PyTuple_SET_ITEM( const_tuple_f25cbd52acbf24776e032c042f114b49_tuple, 4, const_str_plain_new_heading_cell ); Py_INCREF( const_str_plain_new_heading_cell );
    PyTuple_SET_ITEM( const_tuple_f25cbd52acbf24776e032c042f114b49_tuple, 5, const_str_plain_nbformat ); Py_INCREF( const_str_plain_nbformat );
    PyTuple_SET_ITEM( const_tuple_f25cbd52acbf24776e032c042f114b49_tuple, 6, const_str_plain_nbformat_minor ); Py_INCREF( const_str_plain_nbformat_minor );
    const_str_digest_08690a2008ca83674f75fd9195f70b7f = UNSTREAM_STRING_ASCII( &constant_bin[ 2802941 ], 14, 0 );
    const_str_digest_2e399016eb375b57f3c9a7ec17f9281d = UNSTREAM_STRING_ASCII( &constant_bin[ 2802955 ], 19, 0 );
    const_tuple_str_digest_efdace01816915012691e5b49febfd69_tuple = PyTuple_New( 1 );
    const_str_digest_efdace01816915012691e5b49febfd69 = UNSTREAM_STRING_ASCII( &constant_bin[ 2802974 ], 11, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_efdace01816915012691e5b49febfd69_tuple, 0, const_str_digest_efdace01816915012691e5b49febfd69 ); Py_INCREF( const_str_digest_efdace01816915012691e5b49febfd69 );
    const_tuple_str_digest_08690a2008ca83674f75fd9195f70b7f_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_08690a2008ca83674f75fd9195f70b7f_tuple, 0, const_str_digest_08690a2008ca83674f75fd9195f70b7f ); Py_INCREF( const_str_digest_08690a2008ca83674f75fd9195f70b7f );
    const_str_digest_3a548363f730fd88b910bc5cb4e29675 = UNSTREAM_STRING_ASCII( &constant_bin[ 2802985 ], 35, 0 );
    const_str_digest_49a2f4a86b9b100a47c479fad7247d4b = UNSTREAM_STRING_ASCII( &constant_bin[ 2803020 ], 28, 0 );
    const_tuple_458ee026cceff7f4c2727919faba6d0d_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_458ee026cceff7f4c2727919faba6d0d_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_458ee026cceff7f4c2727919faba6d0d_tuple, 1, const_str_plain_state ); Py_INCREF( const_str_plain_state );
    PyTuple_SET_ITEM( const_tuple_458ee026cceff7f4c2727919faba6d0d_tuple, 2, const_str_plain_lines ); Py_INCREF( const_str_plain_lines );
    PyTuple_SET_ITEM( const_tuple_458ee026cceff7f4c2727919faba6d0d_tuple, 3, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_458ee026cceff7f4c2727919faba6d0d_tuple, 4, const_str_plain_input ); Py_INCREF( const_str_plain_input );
    PyTuple_SET_ITEM( const_tuple_458ee026cceff7f4c2727919faba6d0d_tuple, 5, const_str_plain_text ); Py_INCREF( const_str_plain_text );
    PyTuple_SET_ITEM( const_tuple_458ee026cceff7f4c2727919faba6d0d_tuple, 6, const_str_plain_level ); Py_INCREF( const_str_plain_level );
    const_tuple_str_digest_43d74b3c7f8a8c18835b37ee35a1e6bc_tuple = PyTuple_New( 1 );
    const_str_digest_43d74b3c7f8a8c18835b37ee35a1e6bc = UNSTREAM_STRING_ASCII( &constant_bin[ 2803048 ], 17, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_43d74b3c7f8a8c18835b37ee35a1e6bc_tuple, 0, const_str_digest_43d74b3c7f8a8c18835b37ee35a1e6bc ); Py_INCREF( const_str_digest_43d74b3c7f8a8c18835b37ee35a1e6bc );
    const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple, 1, const_str_plain_nb ); Py_INCREF( const_str_plain_nb );
    PyTuple_SET_ITEM( const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple, 3, const_str_plain_lines ); Py_INCREF( const_str_plain_lines );
    PyTuple_SET_ITEM( const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple, 4, const_str_plain_ws ); Py_INCREF( const_str_plain_ws );
    PyTuple_SET_ITEM( const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple, 5, const_str_plain_cell ); Py_INCREF( const_str_plain_cell );
    PyTuple_SET_ITEM( const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple, 6, const_str_plain_input ); Py_INCREF( const_str_plain_input );
    PyTuple_SET_ITEM( const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple, 7, const_str_plain_level ); Py_INCREF( const_str_plain_level );
    const_list_str_digest_efdace01816915012691e5b49febfd69_str_empty_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_digest_efdace01816915012691e5b49febfd69_str_empty_list, 0, const_str_digest_efdace01816915012691e5b49febfd69 ); Py_INCREF( const_str_digest_efdace01816915012691e5b49febfd69 );
    PyList_SET_ITEM( const_list_str_digest_efdace01816915012691e5b49febfd69_str_empty_list, 1, const_str_empty ); Py_INCREF( const_str_empty );
    const_str_digest_fc7467a3720075c2bc06bb415a7982bb = UNSTREAM_STRING_ASCII( &constant_bin[ 2803065 ], 24, 0 );
    const_str_digest_a1ccde01d3ada8afb4b652f98ef1467b = UNSTREAM_STRING_ASCII( &constant_bin[ 2803089 ], 16, 0 );
    const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 1, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 3, const_str_plain_lines ); Py_INCREF( const_str_plain_lines );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 4, const_str_plain_cells ); Py_INCREF( const_str_plain_cells );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 5, const_str_plain_cell_lines ); Py_INCREF( const_str_plain_cell_lines );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 6, const_str_plain_state ); Py_INCREF( const_str_plain_state );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 7, const_str_plain_line ); Py_INCREF( const_str_plain_line );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 8, const_str_plain_cell ); Py_INCREF( const_str_plain_cell );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 9, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 10, const_str_plain_ws ); Py_INCREF( const_str_plain_ws );
    PyTuple_SET_ITEM( const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 11, const_str_plain_nb ); Py_INCREF( const_str_plain_nb );
    const_str_plain_headingcell = UNSTREAM_STRING_ASCII( &constant_bin[ 2802944 ], 11, 1 );
    const_str_digest_8a80611d57ef5e7ff5b3544d5b831791 = UNSTREAM_STRING_ASCII( &constant_bin[ 2803105 ], 25, 0 );
    const_str_plain_rawcell = UNSTREAM_STRING_ASCII( &constant_bin[ 2802977 ], 7, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_nbformat$v3$nbpy( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_eb26b3964260d822c88a7fae45c3d049;
static PyCodeObject *codeobj_d2728b061cb2bcfa5fded6401f5ec7d3;
static PyCodeObject *codeobj_c2a8dffdae9b7d568e4654a49f86edf8;
static PyCodeObject *codeobj_e2b6c5150ad64bf469296b400d92c9ff;
static PyCodeObject *codeobj_db67969e0889f1e67a65348b6afbf76d;
static PyCodeObject *codeobj_172af58c58bc83062beaf4406bd4dbd5;
static PyCodeObject *codeobj_731c89bd0fef47837207780129665e0e;
static PyCodeObject *codeobj_571b126b46a578699a7b55a1776dfdb7;
static PyCodeObject *codeobj_caf610dc5992c62a0682b1140ac23345;
static PyCodeObject *codeobj_3c7155fddb695906582167c0e7e21eb8;
static PyCodeObject *codeobj_e294c983e3fbdad42247274db5767661;
static PyCodeObject *codeobj_9c9c9b47c83ccf8873a3073eb6ba3519;
static PyCodeObject *codeobj_8a351f86f2e046cbffb082b3b91827f5;
static PyCodeObject *codeobj_32003efde0d7f7755fbe30fab89f2b4f;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_2e399016eb375b57f3c9a7ec17f9281d );
    codeobj_eb26b3964260d822c88a7fae45c3d049 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 171, const_tuple_str_plain_line_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d2728b061cb2bcfa5fded6401f5ec7d3 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 177, const_tuple_str_plain_line_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c2a8dffdae9b7d568e4654a49f86edf8 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 183, const_tuple_str_plain_line_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e2b6c5150ad64bf469296b400d92c9ff = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 190, const_tuple_str_plain_line_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_db67969e0889f1e67a65348b6afbf76d = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 145, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_172af58c58bc83062beaf4406bd4dbd5 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_8a80611d57ef5e7ff5b3544d5b831791, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_731c89bd0fef47837207780129665e0e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_PyReader, 36, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_571b126b46a578699a7b55a1776dfdb7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_PyWriter, 151, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_caf610dc5992c62a0682b1140ac23345 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__remove_comments, 127, const_tuple_de33fc8d485e350dddf3e8614527e071_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3c7155fddb695906582167c0e7e21eb8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_new_cell, 103, const_tuple_458ee026cceff7f4c2727919faba6d0d_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_e294c983e3fbdad42247274db5767661 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_reads, 38, const_tuple_str_plain_self_str_plain_s_str_plain_kwargs_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_9c9c9b47c83ccf8873a3073eb6ba3519 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_split_lines_into_blocks, 138, const_tuple_9eb4762e2735231377088987105a5e17_tuple, 2, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8a351f86f2e046cbffb082b3b91827f5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_to_notebook, 41, const_tuple_1d8f2020ac6eb597ff183ce9a70cad2a_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_32003efde0d7f7755fbe30fab89f2b4f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_writes, 153, const_tuple_fe25bb24cfcdf1a6272e70a0bee7b042_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
}

// The module function declarations.
static PyObject *nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_1_reads(  );


static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_2_to_notebook(  );


static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_3_new_cell(  );


static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_4__remove_comments(  );


static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_5_split_lines_into_blocks(  );


static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_6_writes(  );


// The module function definitions.
static PyObject *impl_nbformat$v3$nbpy$$$function_1_reads( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_s = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_e294c983e3fbdad42247274db5767661;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e294c983e3fbdad42247274db5767661 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e294c983e3fbdad42247274db5767661, codeobj_e294c983e3fbdad42247274db5767661, module_nbformat$v3$nbpy, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e294c983e3fbdad42247274db5767661 = cache_frame_e294c983e3fbdad42247274db5767661;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e294c983e3fbdad42247274db5767661 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e294c983e3fbdad42247274db5767661 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_to_notebook );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_s );
        tmp_tuple_element_1 = par_s;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_return_value = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e294c983e3fbdad42247274db5767661 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e294c983e3fbdad42247274db5767661 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e294c983e3fbdad42247274db5767661 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e294c983e3fbdad42247274db5767661, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e294c983e3fbdad42247274db5767661->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e294c983e3fbdad42247274db5767661, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e294c983e3fbdad42247274db5767661,
        type_description_1,
        par_self,
        par_s,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_e294c983e3fbdad42247274db5767661 == cache_frame_e294c983e3fbdad42247274db5767661 )
    {
        Py_DECREF( frame_e294c983e3fbdad42247274db5767661 );
    }
    cache_frame_e294c983e3fbdad42247274db5767661 = NULL;

    assertFrameObject( frame_e294c983e3fbdad42247274db5767661 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_1_reads );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_1_reads );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v3$nbpy$$$function_2_to_notebook( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_s = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_lines = NULL;
    PyObject *var_cells = NULL;
    PyObject *var_cell_lines = NULL;
    PyObject *var_state = NULL;
    PyObject *var_line = NULL;
    PyObject *var_cell = NULL;
    PyObject *var_m = NULL;
    PyObject *var_ws = NULL;
    PyObject *var_nb = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_8a351f86f2e046cbffb082b3b91827f5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_8a351f86f2e046cbffb082b3b91827f5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8a351f86f2e046cbffb082b3b91827f5, codeobj_8a351f86f2e046cbffb082b3b91827f5, module_nbformat$v3$nbpy, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8a351f86f2e046cbffb082b3b91827f5 = cache_frame_8a351f86f2e046cbffb082b3b91827f5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8a351f86f2e046cbffb082b3b91827f5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8a351f86f2e046cbffb082b3b91827f5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_s );
        tmp_called_instance_1 = par_s;
        frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 42;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_splitlines );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_lines == NULL );
        var_lines = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( var_cells == NULL );
        var_cells = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = PyList_New( 0 );
        assert( var_cell_lines == NULL );
        var_cell_lines = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = PyDict_New();
        {
            PyObject *old = par_kwargs;
            assert( old != NULL );
            par_kwargs = tmp_assign_source_4;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = const_str_plain_codecell;
        assert( var_state == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_state = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( var_lines );
        tmp_iter_arg_1 = var_lines;
        tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_6;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooo";
                exception_lineno = 47;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_8 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_line;
            var_line = tmp_assign_source_8;
            Py_INCREF( var_line );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_line );
        tmp_called_instance_2 = var_line;
        frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 48;
        tmp_or_left_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_aab696e9dc4dd50c44cf2f1c57967b68_tuple, 0 ) );

        if ( tmp_or_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_2;
        }
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_or_left_value_1 );

            exception_lineno = 48;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_2;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        Py_DECREF( tmp_or_left_value_1 );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain__encoding_declaration_re );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__encoding_declaration_re );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_encoding_declaration_re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 48;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_2;
        }

        tmp_called_instance_3 = tmp_mvar_value_1;
        CHECK_OBJECT( var_line );
        tmp_args_element_name_1 = var_line;
        frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_or_right_value_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_match, call_args );
        }

        if ( tmp_or_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_2;
        }
        tmp_operand_name_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_operand_name_1 = tmp_or_left_value_1;
        or_end_1:;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oooooooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_called_instance_4;
            PyObject *tmp_call_result_1;
            int tmp_truth_name_1;
            CHECK_OBJECT( var_line );
            tmp_called_instance_4 = var_line;
            frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 50;
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_0ef8517a2e0443cae5679ec66d74a519_tuple, 0 ) );

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_2;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_call_result_1 );

                exception_lineno = 50;
                type_description_1 = "oooooooooooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_1 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_dircall_arg1_1;
                PyObject *tmp_source_name_1;
                PyObject *tmp_dircall_arg2_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_dircall_arg3_1;
                CHECK_OBJECT( par_self );
                tmp_source_name_1 = par_self;
                tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_new_cell );
                if ( tmp_dircall_arg1_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 51;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_2;
                }
                if ( var_state == NULL )
                {
                    Py_DECREF( tmp_dircall_arg1_1 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "state" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 51;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_2;
                }

                tmp_tuple_element_1 = var_state;
                tmp_dircall_arg2_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
                if ( var_cell_lines == NULL )
                {
                    Py_DECREF( tmp_dircall_arg1_1 );
                    Py_DECREF( tmp_dircall_arg2_1 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cell_lines" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 51;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_2;
                }

                tmp_tuple_element_1 = var_cell_lines;
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_dircall_arg2_1, 1, tmp_tuple_element_1 );
                if ( par_kwargs == NULL )
                {
                    Py_DECREF( tmp_dircall_arg1_1 );
                    Py_DECREF( tmp_dircall_arg2_1 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 51;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_2;
                }

                tmp_dircall_arg3_1 = par_kwargs;
                Py_INCREF( tmp_dircall_arg3_1 );

                {
                    PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
                    tmp_assign_source_9 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
                }
                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 51;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = var_cell;
                    var_cell = tmp_assign_source_9;
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_1;
                PyObject *tmp_compexpr_right_1;
                CHECK_OBJECT( var_cell );
                tmp_compexpr_left_1 = var_cell;
                tmp_compexpr_right_1 = Py_None;
                tmp_condition_result_3 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_called_instance_5;
                    PyObject *tmp_call_result_2;
                    PyObject *tmp_args_element_name_2;
                    CHECK_OBJECT( var_cells );
                    tmp_called_instance_5 = var_cells;
                    CHECK_OBJECT( var_cell );
                    tmp_args_element_name_2 = var_cell;
                    frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 53;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_2 };
                        tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_append, call_args );
                    }

                    if ( tmp_call_result_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 53;
                        type_description_1 = "oooooooooooo";
                        goto try_except_handler_2;
                    }
                    Py_DECREF( tmp_call_result_2 );
                }
                branch_no_3:;
            }
            {
                PyObject *tmp_assign_source_10;
                tmp_assign_source_10 = const_str_plain_codecell;
                {
                    PyObject *old = var_state;
                    var_state = tmp_assign_source_10;
                    Py_INCREF( var_state );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_11;
                tmp_assign_source_11 = PyList_New( 0 );
                {
                    PyObject *old = var_cell_lines;
                    var_cell_lines = tmp_assign_source_11;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_12;
                tmp_assign_source_12 = PyDict_New();
                {
                    PyObject *old = par_kwargs;
                    par_kwargs = tmp_assign_source_12;
                    Py_XDECREF( old );
                }

            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_called_instance_6;
                PyObject *tmp_call_result_3;
                int tmp_truth_name_2;
                CHECK_OBJECT( var_line );
                tmp_called_instance_6 = var_line;
                frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 57;
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_783cb5b32947f6074d02faf31f87f24e_tuple, 0 ) );

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 57;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_3 );
                if ( tmp_truth_name_2 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_call_result_3 );

                    exception_lineno = 57;
                    type_description_1 = "oooooooooooo";
                    goto try_except_handler_2;
                }
                tmp_condition_result_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_call_result_3 );
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_13;
                    PyObject *tmp_dircall_arg1_2;
                    PyObject *tmp_source_name_2;
                    PyObject *tmp_dircall_arg2_2;
                    PyObject *tmp_tuple_element_2;
                    PyObject *tmp_dircall_arg3_2;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_2 = par_self;
                    tmp_dircall_arg1_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_new_cell );
                    if ( tmp_dircall_arg1_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 58;
                        type_description_1 = "oooooooooooo";
                        goto try_except_handler_2;
                    }
                    if ( var_state == NULL )
                    {
                        Py_DECREF( tmp_dircall_arg1_2 );
                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "state" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 58;
                        type_description_1 = "oooooooooooo";
                        goto try_except_handler_2;
                    }

                    tmp_tuple_element_2 = var_state;
                    tmp_dircall_arg2_2 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_2 );
                    PyTuple_SET_ITEM( tmp_dircall_arg2_2, 0, tmp_tuple_element_2 );
                    if ( var_cell_lines == NULL )
                    {
                        Py_DECREF( tmp_dircall_arg1_2 );
                        Py_DECREF( tmp_dircall_arg2_2 );
                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cell_lines" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 58;
                        type_description_1 = "oooooooooooo";
                        goto try_except_handler_2;
                    }

                    tmp_tuple_element_2 = var_cell_lines;
                    Py_INCREF( tmp_tuple_element_2 );
                    PyTuple_SET_ITEM( tmp_dircall_arg2_2, 1, tmp_tuple_element_2 );
                    if ( par_kwargs == NULL )
                    {
                        Py_DECREF( tmp_dircall_arg1_2 );
                        Py_DECREF( tmp_dircall_arg2_2 );
                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 58;
                        type_description_1 = "oooooooooooo";
                        goto try_except_handler_2;
                    }

                    tmp_dircall_arg3_2 = par_kwargs;
                    Py_INCREF( tmp_dircall_arg3_2 );

                    {
                        PyObject *dir_call_args[] = {tmp_dircall_arg1_2, tmp_dircall_arg2_2, tmp_dircall_arg3_2};
                        tmp_assign_source_13 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
                    }
                    if ( tmp_assign_source_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 58;
                        type_description_1 = "oooooooooooo";
                        goto try_except_handler_2;
                    }
                    {
                        PyObject *old = var_cell;
                        var_cell = tmp_assign_source_13;
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_2;
                    PyObject *tmp_compexpr_right_2;
                    CHECK_OBJECT( var_cell );
                    tmp_compexpr_left_2 = var_cell;
                    tmp_compexpr_right_2 = Py_None;
                    tmp_condition_result_5 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_called_instance_7;
                        PyObject *tmp_call_result_4;
                        PyObject *tmp_args_element_name_3;
                        CHECK_OBJECT( var_cells );
                        tmp_called_instance_7 = var_cells;
                        CHECK_OBJECT( var_cell );
                        tmp_args_element_name_3 = var_cell;
                        frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 60;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_3 };
                            tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_append, call_args );
                        }

                        if ( tmp_call_result_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 60;
                            type_description_1 = "oooooooooooo";
                            goto try_except_handler_2;
                        }
                        Py_DECREF( tmp_call_result_4 );
                    }
                    branch_no_5:;
                }
                {
                    PyObject *tmp_assign_source_14;
                    tmp_assign_source_14 = const_str_plain_htmlcell;
                    {
                        PyObject *old = var_state;
                        var_state = tmp_assign_source_14;
                        Py_INCREF( var_state );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_15;
                    tmp_assign_source_15 = PyList_New( 0 );
                    {
                        PyObject *old = var_cell_lines;
                        var_cell_lines = tmp_assign_source_15;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_16;
                    tmp_assign_source_16 = PyDict_New();
                    {
                        PyObject *old = par_kwargs;
                        par_kwargs = tmp_assign_source_16;
                        Py_XDECREF( old );
                    }

                }
                goto branch_end_4;
                branch_no_4:;
                {
                    nuitka_bool tmp_condition_result_6;
                    PyObject *tmp_called_instance_8;
                    PyObject *tmp_call_result_5;
                    int tmp_truth_name_3;
                    CHECK_OBJECT( var_line );
                    tmp_called_instance_8 = var_line;
                    frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 64;
                    tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_b7a68a90094c6ec9dcd6f55b6e1f128e_tuple, 0 ) );

                    if ( tmp_call_result_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 64;
                        type_description_1 = "oooooooooooo";
                        goto try_except_handler_2;
                    }
                    tmp_truth_name_3 = CHECK_IF_TRUE( tmp_call_result_5 );
                    if ( tmp_truth_name_3 == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_call_result_5 );

                        exception_lineno = 64;
                        type_description_1 = "oooooooooooo";
                        goto try_except_handler_2;
                    }
                    tmp_condition_result_6 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    Py_DECREF( tmp_call_result_5 );
                    if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_6;
                    }
                    else
                    {
                        goto branch_no_6;
                    }
                    branch_yes_6:;
                    {
                        PyObject *tmp_assign_source_17;
                        PyObject *tmp_dircall_arg1_3;
                        PyObject *tmp_source_name_3;
                        PyObject *tmp_dircall_arg2_3;
                        PyObject *tmp_tuple_element_3;
                        PyObject *tmp_dircall_arg3_3;
                        CHECK_OBJECT( par_self );
                        tmp_source_name_3 = par_self;
                        tmp_dircall_arg1_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_new_cell );
                        if ( tmp_dircall_arg1_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 65;
                            type_description_1 = "oooooooooooo";
                            goto try_except_handler_2;
                        }
                        if ( var_state == NULL )
                        {
                            Py_DECREF( tmp_dircall_arg1_3 );
                            exception_type = PyExc_UnboundLocalError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "state" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 65;
                            type_description_1 = "oooooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_tuple_element_3 = var_state;
                        tmp_dircall_arg2_3 = PyTuple_New( 2 );
                        Py_INCREF( tmp_tuple_element_3 );
                        PyTuple_SET_ITEM( tmp_dircall_arg2_3, 0, tmp_tuple_element_3 );
                        if ( var_cell_lines == NULL )
                        {
                            Py_DECREF( tmp_dircall_arg1_3 );
                            Py_DECREF( tmp_dircall_arg2_3 );
                            exception_type = PyExc_UnboundLocalError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cell_lines" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 65;
                            type_description_1 = "oooooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_tuple_element_3 = var_cell_lines;
                        Py_INCREF( tmp_tuple_element_3 );
                        PyTuple_SET_ITEM( tmp_dircall_arg2_3, 1, tmp_tuple_element_3 );
                        if ( par_kwargs == NULL )
                        {
                            Py_DECREF( tmp_dircall_arg1_3 );
                            Py_DECREF( tmp_dircall_arg2_3 );
                            exception_type = PyExc_UnboundLocalError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 65;
                            type_description_1 = "oooooooooooo";
                            goto try_except_handler_2;
                        }

                        tmp_dircall_arg3_3 = par_kwargs;
                        Py_INCREF( tmp_dircall_arg3_3 );

                        {
                            PyObject *dir_call_args[] = {tmp_dircall_arg1_3, tmp_dircall_arg2_3, tmp_dircall_arg3_3};
                            tmp_assign_source_17 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
                        }
                        if ( tmp_assign_source_17 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 65;
                            type_description_1 = "oooooooooooo";
                            goto try_except_handler_2;
                        }
                        {
                            PyObject *old = var_cell;
                            var_cell = tmp_assign_source_17;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        nuitka_bool tmp_condition_result_7;
                        PyObject *tmp_compexpr_left_3;
                        PyObject *tmp_compexpr_right_3;
                        CHECK_OBJECT( var_cell );
                        tmp_compexpr_left_3 = var_cell;
                        tmp_compexpr_right_3 = Py_None;
                        tmp_condition_result_7 = ( tmp_compexpr_left_3 != tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_7;
                        }
                        else
                        {
                            goto branch_no_7;
                        }
                        branch_yes_7:;
                        {
                            PyObject *tmp_called_instance_9;
                            PyObject *tmp_call_result_6;
                            PyObject *tmp_args_element_name_4;
                            CHECK_OBJECT( var_cells );
                            tmp_called_instance_9 = var_cells;
                            CHECK_OBJECT( var_cell );
                            tmp_args_element_name_4 = var_cell;
                            frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 67;
                            {
                                PyObject *call_args[] = { tmp_args_element_name_4 };
                                tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_append, call_args );
                            }

                            if ( tmp_call_result_6 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 67;
                                type_description_1 = "oooooooooooo";
                                goto try_except_handler_2;
                            }
                            Py_DECREF( tmp_call_result_6 );
                        }
                        branch_no_7:;
                    }
                    {
                        PyObject *tmp_assign_source_18;
                        tmp_assign_source_18 = const_str_plain_markdowncell;
                        {
                            PyObject *old = var_state;
                            var_state = tmp_assign_source_18;
                            Py_INCREF( var_state );
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_19;
                        tmp_assign_source_19 = PyList_New( 0 );
                        {
                            PyObject *old = var_cell_lines;
                            var_cell_lines = tmp_assign_source_19;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_20;
                        tmp_assign_source_20 = PyDict_New();
                        {
                            PyObject *old = par_kwargs;
                            par_kwargs = tmp_assign_source_20;
                            Py_XDECREF( old );
                        }

                    }
                    goto branch_end_6;
                    branch_no_6:;
                    {
                        nuitka_bool tmp_condition_result_8;
                        int tmp_or_left_truth_2;
                        nuitka_bool tmp_or_left_value_2;
                        nuitka_bool tmp_or_right_value_2;
                        PyObject *tmp_called_instance_10;
                        PyObject *tmp_call_result_7;
                        int tmp_truth_name_4;
                        PyObject *tmp_called_instance_11;
                        PyObject *tmp_call_result_8;
                        int tmp_truth_name_5;
                        CHECK_OBJECT( var_line );
                        tmp_called_instance_10 = var_line;
                        frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 72;
                        tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_efdace01816915012691e5b49febfd69_tuple, 0 ) );

                        if ( tmp_call_result_7 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 72;
                            type_description_1 = "oooooooooooo";
                            goto try_except_handler_2;
                        }
                        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_call_result_7 );
                        if ( tmp_truth_name_4 == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_call_result_7 );

                            exception_lineno = 72;
                            type_description_1 = "oooooooooooo";
                            goto try_except_handler_2;
                        }
                        tmp_or_left_value_2 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        Py_DECREF( tmp_call_result_7 );
                        tmp_or_left_truth_2 = tmp_or_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
                        if ( tmp_or_left_truth_2 == 1 )
                        {
                            goto or_left_2;
                        }
                        else
                        {
                            goto or_right_2;
                        }
                        or_right_2:;
                        CHECK_OBJECT( var_line );
                        tmp_called_instance_11 = var_line;
                        frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 72;
                        tmp_call_result_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_11, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_43d74b3c7f8a8c18835b37ee35a1e6bc_tuple, 0 ) );

                        if ( tmp_call_result_8 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 72;
                            type_description_1 = "oooooooooooo";
                            goto try_except_handler_2;
                        }
                        tmp_truth_name_5 = CHECK_IF_TRUE( tmp_call_result_8 );
                        if ( tmp_truth_name_5 == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_call_result_8 );

                            exception_lineno = 72;
                            type_description_1 = "oooooooooooo";
                            goto try_except_handler_2;
                        }
                        tmp_or_right_value_2 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        Py_DECREF( tmp_call_result_8 );
                        tmp_condition_result_8 = tmp_or_right_value_2;
                        goto or_end_2;
                        or_left_2:;
                        tmp_condition_result_8 = tmp_or_left_value_2;
                        or_end_2:;
                        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_8;
                        }
                        else
                        {
                            goto branch_no_8;
                        }
                        branch_yes_8:;
                        {
                            PyObject *tmp_assign_source_21;
                            PyObject *tmp_dircall_arg1_4;
                            PyObject *tmp_source_name_4;
                            PyObject *tmp_dircall_arg2_4;
                            PyObject *tmp_tuple_element_4;
                            PyObject *tmp_dircall_arg3_4;
                            CHECK_OBJECT( par_self );
                            tmp_source_name_4 = par_self;
                            tmp_dircall_arg1_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_new_cell );
                            if ( tmp_dircall_arg1_4 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 73;
                                type_description_1 = "oooooooooooo";
                                goto try_except_handler_2;
                            }
                            if ( var_state == NULL )
                            {
                                Py_DECREF( tmp_dircall_arg1_4 );
                                exception_type = PyExc_UnboundLocalError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "state" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 73;
                                type_description_1 = "oooooooooooo";
                                goto try_except_handler_2;
                            }

                            tmp_tuple_element_4 = var_state;
                            tmp_dircall_arg2_4 = PyTuple_New( 2 );
                            Py_INCREF( tmp_tuple_element_4 );
                            PyTuple_SET_ITEM( tmp_dircall_arg2_4, 0, tmp_tuple_element_4 );
                            if ( var_cell_lines == NULL )
                            {
                                Py_DECREF( tmp_dircall_arg1_4 );
                                Py_DECREF( tmp_dircall_arg2_4 );
                                exception_type = PyExc_UnboundLocalError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cell_lines" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 73;
                                type_description_1 = "oooooooooooo";
                                goto try_except_handler_2;
                            }

                            tmp_tuple_element_4 = var_cell_lines;
                            Py_INCREF( tmp_tuple_element_4 );
                            PyTuple_SET_ITEM( tmp_dircall_arg2_4, 1, tmp_tuple_element_4 );
                            if ( par_kwargs == NULL )
                            {
                                Py_DECREF( tmp_dircall_arg1_4 );
                                Py_DECREF( tmp_dircall_arg2_4 );
                                exception_type = PyExc_UnboundLocalError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 73;
                                type_description_1 = "oooooooooooo";
                                goto try_except_handler_2;
                            }

                            tmp_dircall_arg3_4 = par_kwargs;
                            Py_INCREF( tmp_dircall_arg3_4 );

                            {
                                PyObject *dir_call_args[] = {tmp_dircall_arg1_4, tmp_dircall_arg2_4, tmp_dircall_arg3_4};
                                tmp_assign_source_21 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
                            }
                            if ( tmp_assign_source_21 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 73;
                                type_description_1 = "oooooooooooo";
                                goto try_except_handler_2;
                            }
                            {
                                PyObject *old = var_cell;
                                var_cell = tmp_assign_source_21;
                                Py_XDECREF( old );
                            }

                        }
                        {
                            nuitka_bool tmp_condition_result_9;
                            PyObject *tmp_compexpr_left_4;
                            PyObject *tmp_compexpr_right_4;
                            CHECK_OBJECT( var_cell );
                            tmp_compexpr_left_4 = var_cell;
                            tmp_compexpr_right_4 = Py_None;
                            tmp_condition_result_9 = ( tmp_compexpr_left_4 != tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                            {
                                goto branch_yes_9;
                            }
                            else
                            {
                                goto branch_no_9;
                            }
                            branch_yes_9:;
                            {
                                PyObject *tmp_called_instance_12;
                                PyObject *tmp_call_result_9;
                                PyObject *tmp_args_element_name_5;
                                CHECK_OBJECT( var_cells );
                                tmp_called_instance_12 = var_cells;
                                CHECK_OBJECT( var_cell );
                                tmp_args_element_name_5 = var_cell;
                                frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 75;
                                {
                                    PyObject *call_args[] = { tmp_args_element_name_5 };
                                    tmp_call_result_9 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_12, const_str_plain_append, call_args );
                                }

                                if ( tmp_call_result_9 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 75;
                                    type_description_1 = "oooooooooooo";
                                    goto try_except_handler_2;
                                }
                                Py_DECREF( tmp_call_result_9 );
                            }
                            branch_no_9:;
                        }
                        {
                            PyObject *tmp_assign_source_22;
                            tmp_assign_source_22 = const_str_plain_rawcell;
                            {
                                PyObject *old = var_state;
                                var_state = tmp_assign_source_22;
                                Py_INCREF( var_state );
                                Py_XDECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_assign_source_23;
                            tmp_assign_source_23 = PyList_New( 0 );
                            {
                                PyObject *old = var_cell_lines;
                                var_cell_lines = tmp_assign_source_23;
                                Py_XDECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_assign_source_24;
                            tmp_assign_source_24 = PyDict_New();
                            {
                                PyObject *old = par_kwargs;
                                par_kwargs = tmp_assign_source_24;
                                Py_XDECREF( old );
                            }

                        }
                        goto branch_end_8;
                        branch_no_8:;
                        {
                            nuitka_bool tmp_condition_result_10;
                            PyObject *tmp_called_instance_13;
                            PyObject *tmp_call_result_10;
                            int tmp_truth_name_6;
                            CHECK_OBJECT( var_line );
                            tmp_called_instance_13 = var_line;
                            frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 79;
                            tmp_call_result_10 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_13, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_digest_08690a2008ca83674f75fd9195f70b7f_tuple, 0 ) );

                            if ( tmp_call_result_10 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 79;
                                type_description_1 = "oooooooooooo";
                                goto try_except_handler_2;
                            }
                            tmp_truth_name_6 = CHECK_IF_TRUE( tmp_call_result_10 );
                            if ( tmp_truth_name_6 == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                Py_DECREF( tmp_call_result_10 );

                                exception_lineno = 79;
                                type_description_1 = "oooooooooooo";
                                goto try_except_handler_2;
                            }
                            tmp_condition_result_10 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            Py_DECREF( tmp_call_result_10 );
                            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                            {
                                goto branch_yes_10;
                            }
                            else
                            {
                                goto branch_no_10;
                            }
                            branch_yes_10:;
                            {
                                PyObject *tmp_assign_source_25;
                                PyObject *tmp_dircall_arg1_5;
                                PyObject *tmp_source_name_5;
                                PyObject *tmp_dircall_arg2_5;
                                PyObject *tmp_tuple_element_5;
                                PyObject *tmp_dircall_arg3_5;
                                CHECK_OBJECT( par_self );
                                tmp_source_name_5 = par_self;
                                tmp_dircall_arg1_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_new_cell );
                                if ( tmp_dircall_arg1_5 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 80;
                                    type_description_1 = "oooooooooooo";
                                    goto try_except_handler_2;
                                }
                                if ( var_state == NULL )
                                {
                                    Py_DECREF( tmp_dircall_arg1_5 );
                                    exception_type = PyExc_UnboundLocalError;
                                    Py_INCREF( exception_type );
                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "state" );
                                    exception_tb = NULL;
                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                    CHAIN_EXCEPTION( exception_value );

                                    exception_lineno = 80;
                                    type_description_1 = "oooooooooooo";
                                    goto try_except_handler_2;
                                }

                                tmp_tuple_element_5 = var_state;
                                tmp_dircall_arg2_5 = PyTuple_New( 2 );
                                Py_INCREF( tmp_tuple_element_5 );
                                PyTuple_SET_ITEM( tmp_dircall_arg2_5, 0, tmp_tuple_element_5 );
                                if ( var_cell_lines == NULL )
                                {
                                    Py_DECREF( tmp_dircall_arg1_5 );
                                    Py_DECREF( tmp_dircall_arg2_5 );
                                    exception_type = PyExc_UnboundLocalError;
                                    Py_INCREF( exception_type );
                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cell_lines" );
                                    exception_tb = NULL;
                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                    CHAIN_EXCEPTION( exception_value );

                                    exception_lineno = 80;
                                    type_description_1 = "oooooooooooo";
                                    goto try_except_handler_2;
                                }

                                tmp_tuple_element_5 = var_cell_lines;
                                Py_INCREF( tmp_tuple_element_5 );
                                PyTuple_SET_ITEM( tmp_dircall_arg2_5, 1, tmp_tuple_element_5 );
                                if ( par_kwargs == NULL )
                                {
                                    Py_DECREF( tmp_dircall_arg1_5 );
                                    Py_DECREF( tmp_dircall_arg2_5 );
                                    exception_type = PyExc_UnboundLocalError;
                                    Py_INCREF( exception_type );
                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "kwargs" );
                                    exception_tb = NULL;
                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                    CHAIN_EXCEPTION( exception_value );

                                    exception_lineno = 80;
                                    type_description_1 = "oooooooooooo";
                                    goto try_except_handler_2;
                                }

                                tmp_dircall_arg3_5 = par_kwargs;
                                Py_INCREF( tmp_dircall_arg3_5 );

                                {
                                    PyObject *dir_call_args[] = {tmp_dircall_arg1_5, tmp_dircall_arg2_5, tmp_dircall_arg3_5};
                                    tmp_assign_source_25 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
                                }
                                if ( tmp_assign_source_25 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 80;
                                    type_description_1 = "oooooooooooo";
                                    goto try_except_handler_2;
                                }
                                {
                                    PyObject *old = var_cell;
                                    var_cell = tmp_assign_source_25;
                                    Py_XDECREF( old );
                                }

                            }
                            {
                                nuitka_bool tmp_condition_result_11;
                                PyObject *tmp_compexpr_left_5;
                                PyObject *tmp_compexpr_right_5;
                                CHECK_OBJECT( var_cell );
                                tmp_compexpr_left_5 = var_cell;
                                tmp_compexpr_right_5 = Py_None;
                                tmp_condition_result_11 = ( tmp_compexpr_left_5 != tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                                {
                                    goto branch_yes_11;
                                }
                                else
                                {
                                    goto branch_no_11;
                                }
                                branch_yes_11:;
                                {
                                    PyObject *tmp_called_instance_14;
                                    PyObject *tmp_call_result_11;
                                    PyObject *tmp_args_element_name_6;
                                    CHECK_OBJECT( var_cells );
                                    tmp_called_instance_14 = var_cells;
                                    CHECK_OBJECT( var_cell );
                                    tmp_args_element_name_6 = var_cell;
                                    frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 82;
                                    {
                                        PyObject *call_args[] = { tmp_args_element_name_6 };
                                        tmp_call_result_11 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_14, const_str_plain_append, call_args );
                                    }

                                    if ( tmp_call_result_11 == NULL )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 82;
                                        type_description_1 = "oooooooooooo";
                                        goto try_except_handler_2;
                                    }
                                    Py_DECREF( tmp_call_result_11 );
                                }
                                {
                                    PyObject *tmp_assign_source_26;
                                    tmp_assign_source_26 = PyList_New( 0 );
                                    {
                                        PyObject *old = var_cell_lines;
                                        var_cell_lines = tmp_assign_source_26;
                                        Py_XDECREF( old );
                                    }

                                }
                                branch_no_11:;
                            }
                            {
                                PyObject *tmp_assign_source_27;
                                PyObject *tmp_called_instance_15;
                                PyObject *tmp_mvar_value_2;
                                PyObject *tmp_args_element_name_7;
                                PyObject *tmp_args_element_name_8;
                                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_re );

                                if (unlikely( tmp_mvar_value_2 == NULL ))
                                {
                                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
                                }

                                if ( tmp_mvar_value_2 == NULL )
                                {

                                    exception_type = PyExc_NameError;
                                    Py_INCREF( exception_type );
                                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
                                    exception_tb = NULL;
                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                    CHAIN_EXCEPTION( exception_value );

                                    exception_lineno = 84;
                                    type_description_1 = "oooooooooooo";
                                    goto try_except_handler_2;
                                }

                                tmp_called_instance_15 = tmp_mvar_value_2;
                                tmp_args_element_name_7 = const_str_digest_3a548363f730fd88b910bc5cb4e29675;
                                CHECK_OBJECT( var_line );
                                tmp_args_element_name_8 = var_line;
                                frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 84;
                                {
                                    PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
                                    tmp_assign_source_27 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_15, const_str_plain_match, call_args );
                                }

                                if ( tmp_assign_source_27 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 84;
                                    type_description_1 = "oooooooooooo";
                                    goto try_except_handler_2;
                                }
                                {
                                    PyObject *old = var_m;
                                    var_m = tmp_assign_source_27;
                                    Py_XDECREF( old );
                                }

                            }
                            {
                                nuitka_bool tmp_condition_result_12;
                                PyObject *tmp_compexpr_left_6;
                                PyObject *tmp_compexpr_right_6;
                                CHECK_OBJECT( var_m );
                                tmp_compexpr_left_6 = var_m;
                                tmp_compexpr_right_6 = Py_None;
                                tmp_condition_result_12 = ( tmp_compexpr_left_6 != tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                                if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                                {
                                    goto branch_yes_12;
                                }
                                else
                                {
                                    goto branch_no_12;
                                }
                                branch_yes_12:;
                                {
                                    PyObject *tmp_assign_source_28;
                                    tmp_assign_source_28 = const_str_plain_headingcell;
                                    {
                                        PyObject *old = var_state;
                                        var_state = tmp_assign_source_28;
                                        Py_INCREF( var_state );
                                        Py_XDECREF( old );
                                    }

                                }
                                {
                                    PyObject *tmp_assign_source_29;
                                    tmp_assign_source_29 = PyDict_New();
                                    {
                                        PyObject *old = par_kwargs;
                                        par_kwargs = tmp_assign_source_29;
                                        Py_XDECREF( old );
                                    }

                                }
                                {
                                    PyObject *tmp_int_arg_1;
                                    PyObject *tmp_called_instance_16;
                                    CHECK_OBJECT( var_m );
                                    tmp_called_instance_16 = var_m;
                                    frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 88;
                                    tmp_int_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_16, const_str_plain_group, &PyTuple_GET_ITEM( const_tuple_str_plain_level_tuple, 0 ) );

                                    if ( tmp_int_arg_1 == NULL )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 88;
                                        type_description_1 = "oooooooooooo";
                                        goto try_except_handler_2;
                                    }
                                    tmp_dictset_value = PyNumber_Int( tmp_int_arg_1 );
                                    Py_DECREF( tmp_int_arg_1 );
                                    if ( tmp_dictset_value == NULL )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 88;
                                        type_description_1 = "oooooooooooo";
                                        goto try_except_handler_2;
                                    }
                                    CHECK_OBJECT( par_kwargs );
                                    tmp_dictset_dict = par_kwargs;
                                    tmp_dictset_key = const_str_plain_level;
                                    tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                                    Py_DECREF( tmp_dictset_value );
                                    assert( !(tmp_res != 0) );
                                }
                                goto branch_end_12;
                                branch_no_12:;
                                {
                                    PyObject *tmp_assign_source_30;
                                    tmp_assign_source_30 = const_str_plain_codecell;
                                    {
                                        PyObject *old = var_state;
                                        var_state = tmp_assign_source_30;
                                        Py_INCREF( var_state );
                                        Py_XDECREF( old );
                                    }

                                }
                                {
                                    PyObject *tmp_assign_source_31;
                                    tmp_assign_source_31 = PyDict_New();
                                    {
                                        PyObject *old = par_kwargs;
                                        par_kwargs = tmp_assign_source_31;
                                        Py_XDECREF( old );
                                    }

                                }
                                {
                                    PyObject *tmp_assign_source_32;
                                    tmp_assign_source_32 = PyList_New( 0 );
                                    {
                                        PyObject *old = var_cell_lines;
                                        var_cell_lines = tmp_assign_source_32;
                                        Py_XDECREF( old );
                                    }

                                }
                                branch_end_12:;
                            }
                            goto branch_end_10;
                            branch_no_10:;
                            {
                                PyObject *tmp_called_instance_17;
                                PyObject *tmp_call_result_12;
                                PyObject *tmp_args_element_name_9;
                                if ( var_cell_lines == NULL )
                                {

                                    exception_type = PyExc_UnboundLocalError;
                                    Py_INCREF( exception_type );
                                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cell_lines" );
                                    exception_tb = NULL;
                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                    CHAIN_EXCEPTION( exception_value );

                                    exception_lineno = 94;
                                    type_description_1 = "oooooooooooo";
                                    goto try_except_handler_2;
                                }

                                tmp_called_instance_17 = var_cell_lines;
                                CHECK_OBJECT( var_line );
                                tmp_args_element_name_9 = var_line;
                                frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 94;
                                {
                                    PyObject *call_args[] = { tmp_args_element_name_9 };
                                    tmp_call_result_12 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_17, const_str_plain_append, call_args );
                                }

                                if ( tmp_call_result_12 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 94;
                                    type_description_1 = "oooooooooooo";
                                    goto try_except_handler_2;
                                }
                                Py_DECREF( tmp_call_result_12 );
                            }
                            branch_end_10:;
                        }
                        branch_end_8:;
                    }
                    branch_end_6:;
                }
                branch_end_4:;
            }
            branch_end_2:;
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 47;
        type_description_1 = "oooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        nuitka_bool tmp_condition_result_13;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_7;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        if ( var_cell_lines == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cell_lines" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 95;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_7 = CHECK_IF_TRUE( var_cell_lines );
        if ( tmp_truth_name_7 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        if ( var_state == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "state" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 95;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_7 = var_state;
        tmp_compexpr_right_7 = const_str_plain_codecell;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_13 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_13 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_13;
        }
        else
        {
            goto branch_no_13;
        }
        branch_yes_13:;
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_element_name_10;
            PyObject *tmp_args_element_name_11;
            CHECK_OBJECT( par_self );
            tmp_source_name_6 = par_self;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_new_cell );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 96;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }
            if ( var_state == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "state" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 96;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_10 = var_state;
            if ( var_cell_lines == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "cell_lines" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 96;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_11 = var_cell_lines;
            frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 96;
            {
                PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11 };
                tmp_assign_source_33 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            if ( tmp_assign_source_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 96;
                type_description_1 = "oooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_cell;
                var_cell = tmp_assign_source_33;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_14;
            PyObject *tmp_compexpr_left_8;
            PyObject *tmp_compexpr_right_8;
            CHECK_OBJECT( var_cell );
            tmp_compexpr_left_8 = var_cell;
            tmp_compexpr_right_8 = Py_None;
            tmp_condition_result_14 = ( tmp_compexpr_left_8 != tmp_compexpr_right_8 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_14;
            }
            else
            {
                goto branch_no_14;
            }
            branch_yes_14:;
            {
                PyObject *tmp_called_instance_18;
                PyObject *tmp_call_result_13;
                PyObject *tmp_args_element_name_12;
                CHECK_OBJECT( var_cells );
                tmp_called_instance_18 = var_cells;
                CHECK_OBJECT( var_cell );
                tmp_args_element_name_12 = var_cell;
                frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 98;
                {
                    PyObject *call_args[] = { tmp_args_element_name_12 };
                    tmp_call_result_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_18, const_str_plain_append, call_args );
                }

                if ( tmp_call_result_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 98;
                    type_description_1 = "oooooooooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_13 );
            }
            branch_no_14:;
        }
        branch_no_13:;
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_worksheet );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_worksheet );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_worksheet" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        tmp_dict_key_1 = const_str_plain_cells;
        CHECK_OBJECT( var_cells );
        tmp_dict_value_1 = var_cells;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 99;
        tmp_assign_source_34 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_ws == NULL );
        var_ws = tmp_assign_source_34;
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_list_element_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_notebook );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_notebook );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_notebook" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 100;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_4;
        tmp_dict_key_2 = const_str_plain_worksheets;
        CHECK_OBJECT( var_ws );
        tmp_list_element_1 = var_ws;
        tmp_dict_value_2 = PyList_New( 1 );
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_dict_value_2, 0, tmp_list_element_1 );
        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_8a351f86f2e046cbffb082b3b91827f5->m_frame.f_lineno = 100;
        tmp_assign_source_35 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "oooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_nb == NULL );
        var_nb = tmp_assign_source_35;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8a351f86f2e046cbffb082b3b91827f5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8a351f86f2e046cbffb082b3b91827f5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8a351f86f2e046cbffb082b3b91827f5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8a351f86f2e046cbffb082b3b91827f5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8a351f86f2e046cbffb082b3b91827f5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8a351f86f2e046cbffb082b3b91827f5,
        type_description_1,
        par_self,
        par_s,
        par_kwargs,
        var_lines,
        var_cells,
        var_cell_lines,
        var_state,
        var_line,
        var_cell,
        var_m,
        var_ws,
        var_nb
    );


    // Release cached frame.
    if ( frame_8a351f86f2e046cbffb082b3b91827f5 == cache_frame_8a351f86f2e046cbffb082b3b91827f5 )
    {
        Py_DECREF( frame_8a351f86f2e046cbffb082b3b91827f5 );
    }
    cache_frame_8a351f86f2e046cbffb082b3b91827f5 = NULL;

    assertFrameObject( frame_8a351f86f2e046cbffb082b3b91827f5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_nb );
    tmp_return_value = var_nb;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_2_to_notebook );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_lines );
    Py_DECREF( var_lines );
    var_lines = NULL;

    CHECK_OBJECT( (PyObject *)var_cells );
    Py_DECREF( var_cells );
    var_cells = NULL;

    Py_XDECREF( var_cell_lines );
    var_cell_lines = NULL;

    Py_XDECREF( var_state );
    var_state = NULL;

    Py_XDECREF( var_line );
    var_line = NULL;

    Py_XDECREF( var_cell );
    var_cell = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    CHECK_OBJECT( (PyObject *)var_ws );
    Py_DECREF( var_ws );
    var_ws = NULL;

    CHECK_OBJECT( (PyObject *)var_nb );
    Py_DECREF( var_nb );
    var_nb = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    Py_XDECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_lines );
    var_lines = NULL;

    Py_XDECREF( var_cells );
    var_cells = NULL;

    Py_XDECREF( var_cell_lines );
    var_cell_lines = NULL;

    Py_XDECREF( var_state );
    var_state = NULL;

    Py_XDECREF( var_line );
    var_line = NULL;

    Py_XDECREF( var_cell );
    var_cell = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_ws );
    var_ws = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_2_to_notebook );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v3$nbpy$$$function_3_new_cell( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_state = python_pars[ 1 ];
    PyObject *par_lines = python_pars[ 2 ];
    PyObject *par_kwargs = python_pars[ 3 ];
    PyObject *var_input = NULL;
    PyObject *var_text = NULL;
    PyObject *var_level = NULL;
    struct Nuitka_FrameObject *frame_3c7155fddb695906582167c0e7e21eb8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_3c7155fddb695906582167c0e7e21eb8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3c7155fddb695906582167c0e7e21eb8, codeobj_3c7155fddb695906582167c0e7e21eb8, module_nbformat$v3$nbpy, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_3c7155fddb695906582167c0e7e21eb8 = cache_frame_3c7155fddb695906582167c0e7e21eb8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3c7155fddb695906582167c0e7e21eb8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3c7155fddb695906582167c0e7e21eb8 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_state );
        tmp_compexpr_left_1 = par_state;
        tmp_compexpr_right_1 = const_str_plain_codecell;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            tmp_called_instance_1 = const_str_newline;
            CHECK_OBJECT( par_lines );
            tmp_args_element_name_1 = par_lines;
            frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 105;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_join, call_args );
            }

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_input == NULL );
            var_input = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( var_input );
            tmp_called_instance_2 = var_input;
            frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 106;
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_strip, &PyTuple_GET_ITEM( const_tuple_str_newline_tuple, 0 ) );

            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_input;
                assert( old != NULL );
                var_input = tmp_assign_source_2;
                Py_DECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_truth_name_1;
            CHECK_OBJECT( var_input );
            tmp_truth_name_1 = CHECK_IF_TRUE( var_input );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 107;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_kw_name_1;
                PyObject *tmp_dict_key_1;
                PyObject *tmp_dict_value_1;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_code_cell );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_code_cell );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_code_cell" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 108;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_1 = tmp_mvar_value_1;
                tmp_dict_key_1 = const_str_plain_input;
                CHECK_OBJECT( var_input );
                tmp_dict_value_1 = var_input;
                tmp_kw_name_1 = _PyDict_NewPresized( 1 );
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
                assert( !(tmp_res != 0) );
                frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 108;
                tmp_return_value = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 108;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( par_state );
            tmp_compexpr_left_2 = par_state;
            tmp_compexpr_right_2 = const_str_plain_htmlcell;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 109;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_args_element_name_2;
                CHECK_OBJECT( par_self );
                tmp_called_instance_3 = par_self;
                CHECK_OBJECT( par_lines );
                tmp_args_element_name_2 = par_lines;
                frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 110;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain__remove_comments, call_args );
                }

                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 110;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_text == NULL );
                var_text = tmp_assign_source_3;
            }
            {
                nuitka_bool tmp_condition_result_4;
                int tmp_truth_name_2;
                CHECK_OBJECT( var_text );
                tmp_truth_name_2 = CHECK_IF_TRUE( var_text );
                if ( tmp_truth_name_2 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 111;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_mvar_value_2;
                    PyObject *tmp_args_name_1;
                    PyObject *tmp_kw_name_2;
                    PyObject *tmp_dict_key_2;
                    PyObject *tmp_dict_value_2;
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_text_cell );

                    if (unlikely( tmp_mvar_value_2 == NULL ))
                    {
                        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_text_cell );
                    }

                    if ( tmp_mvar_value_2 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_text_cell" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 112;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_2 = tmp_mvar_value_2;
                    tmp_args_name_1 = const_tuple_str_plain_html_tuple;
                    tmp_dict_key_2 = const_str_plain_source;
                    CHECK_OBJECT( var_text );
                    tmp_dict_value_2 = var_text;
                    tmp_kw_name_2 = _PyDict_NewPresized( 1 );
                    tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
                    assert( !(tmp_res != 0) );
                    frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 112;
                    tmp_return_value = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_2 );
                    Py_DECREF( tmp_kw_name_2 );
                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 112;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                branch_no_4:;
            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                CHECK_OBJECT( par_state );
                tmp_compexpr_left_3 = par_state;
                tmp_compexpr_right_3 = const_str_plain_markdowncell;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 113;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_assign_source_4;
                    PyObject *tmp_called_instance_4;
                    PyObject *tmp_args_element_name_3;
                    CHECK_OBJECT( par_self );
                    tmp_called_instance_4 = par_self;
                    CHECK_OBJECT( par_lines );
                    tmp_args_element_name_3 = par_lines;
                    frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 114;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_3 };
                        tmp_assign_source_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain__remove_comments, call_args );
                    }

                    if ( tmp_assign_source_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 114;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_text == NULL );
                    var_text = tmp_assign_source_4;
                }
                {
                    nuitka_bool tmp_condition_result_6;
                    int tmp_truth_name_3;
                    CHECK_OBJECT( var_text );
                    tmp_truth_name_3 = CHECK_IF_TRUE( var_text );
                    if ( tmp_truth_name_3 == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 115;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_6 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_6;
                    }
                    else
                    {
                        goto branch_no_6;
                    }
                    branch_yes_6:;
                    {
                        PyObject *tmp_called_name_3;
                        PyObject *tmp_mvar_value_3;
                        PyObject *tmp_args_name_2;
                        PyObject *tmp_kw_name_3;
                        PyObject *tmp_dict_key_3;
                        PyObject *tmp_dict_value_3;
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_text_cell );

                        if (unlikely( tmp_mvar_value_3 == NULL ))
                        {
                            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_text_cell );
                        }

                        if ( tmp_mvar_value_3 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_text_cell" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 116;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_name_3 = tmp_mvar_value_3;
                        tmp_args_name_2 = const_tuple_str_plain_markdown_tuple;
                        tmp_dict_key_3 = const_str_plain_source;
                        CHECK_OBJECT( var_text );
                        tmp_dict_value_3 = var_text;
                        tmp_kw_name_3 = _PyDict_NewPresized( 1 );
                        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_3, tmp_dict_value_3 );
                        assert( !(tmp_res != 0) );
                        frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 116;
                        tmp_return_value = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_2, tmp_kw_name_3 );
                        Py_DECREF( tmp_kw_name_3 );
                        if ( tmp_return_value == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 116;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        goto frame_return_exit_1;
                    }
                    branch_no_6:;
                }
                goto branch_end_5;
                branch_no_5:;
                {
                    nuitka_bool tmp_condition_result_7;
                    PyObject *tmp_compexpr_left_4;
                    PyObject *tmp_compexpr_right_4;
                    CHECK_OBJECT( par_state );
                    tmp_compexpr_left_4 = par_state;
                    tmp_compexpr_right_4 = const_str_plain_rawcell;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 117;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_7;
                    }
                    else
                    {
                        goto branch_no_7;
                    }
                    branch_yes_7:;
                    {
                        PyObject *tmp_assign_source_5;
                        PyObject *tmp_called_instance_5;
                        PyObject *tmp_args_element_name_4;
                        CHECK_OBJECT( par_self );
                        tmp_called_instance_5 = par_self;
                        CHECK_OBJECT( par_lines );
                        tmp_args_element_name_4 = par_lines;
                        frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 118;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_4 };
                            tmp_assign_source_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain__remove_comments, call_args );
                        }

                        if ( tmp_assign_source_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 118;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        assert( var_text == NULL );
                        var_text = tmp_assign_source_5;
                    }
                    {
                        nuitka_bool tmp_condition_result_8;
                        int tmp_truth_name_4;
                        CHECK_OBJECT( var_text );
                        tmp_truth_name_4 = CHECK_IF_TRUE( var_text );
                        if ( tmp_truth_name_4 == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 119;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_8 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_8;
                        }
                        else
                        {
                            goto branch_no_8;
                        }
                        branch_yes_8:;
                        {
                            PyObject *tmp_called_name_4;
                            PyObject *tmp_mvar_value_4;
                            PyObject *tmp_args_name_3;
                            PyObject *tmp_kw_name_4;
                            PyObject *tmp_dict_key_4;
                            PyObject *tmp_dict_value_4;
                            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_text_cell );

                            if (unlikely( tmp_mvar_value_4 == NULL ))
                            {
                                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_text_cell );
                            }

                            if ( tmp_mvar_value_4 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_text_cell" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 120;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }

                            tmp_called_name_4 = tmp_mvar_value_4;
                            tmp_args_name_3 = const_tuple_str_plain_raw_tuple;
                            tmp_dict_key_4 = const_str_plain_source;
                            CHECK_OBJECT( var_text );
                            tmp_dict_value_4 = var_text;
                            tmp_kw_name_4 = _PyDict_NewPresized( 1 );
                            tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_4, tmp_dict_value_4 );
                            assert( !(tmp_res != 0) );
                            frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 120;
                            tmp_return_value = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_3, tmp_kw_name_4 );
                            Py_DECREF( tmp_kw_name_4 );
                            if ( tmp_return_value == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 120;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            goto frame_return_exit_1;
                        }
                        branch_no_8:;
                    }
                    goto branch_end_7;
                    branch_no_7:;
                    {
                        nuitka_bool tmp_condition_result_9;
                        PyObject *tmp_compexpr_left_5;
                        PyObject *tmp_compexpr_right_5;
                        CHECK_OBJECT( par_state );
                        tmp_compexpr_left_5 = par_state;
                        tmp_compexpr_right_5 = const_str_plain_headingcell;
                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 121;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_9;
                        }
                        else
                        {
                            goto branch_no_9;
                        }
                        branch_yes_9:;
                        {
                            PyObject *tmp_assign_source_6;
                            PyObject *tmp_called_instance_6;
                            PyObject *tmp_args_element_name_5;
                            CHECK_OBJECT( par_self );
                            tmp_called_instance_6 = par_self;
                            CHECK_OBJECT( par_lines );
                            tmp_args_element_name_5 = par_lines;
                            frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 122;
                            {
                                PyObject *call_args[] = { tmp_args_element_name_5 };
                                tmp_assign_source_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain__remove_comments, call_args );
                            }

                            if ( tmp_assign_source_6 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 122;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            assert( var_text == NULL );
                            var_text = tmp_assign_source_6;
                        }
                        {
                            PyObject *tmp_assign_source_7;
                            PyObject *tmp_called_instance_7;
                            CHECK_OBJECT( par_kwargs );
                            tmp_called_instance_7 = par_kwargs;
                            frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 123;
                            tmp_assign_source_7 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_7, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_level_int_pos_1_tuple, 0 ) );

                            if ( tmp_assign_source_7 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 123;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            assert( var_level == NULL );
                            var_level = tmp_assign_source_7;
                        }
                        {
                            nuitka_bool tmp_condition_result_10;
                            int tmp_truth_name_5;
                            CHECK_OBJECT( var_text );
                            tmp_truth_name_5 = CHECK_IF_TRUE( var_text );
                            if ( tmp_truth_name_5 == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 124;
                                type_description_1 = "ooooooo";
                                goto frame_exception_exit_1;
                            }
                            tmp_condition_result_10 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                            {
                                goto branch_yes_10;
                            }
                            else
                            {
                                goto branch_no_10;
                            }
                            branch_yes_10:;
                            {
                                PyObject *tmp_called_name_5;
                                PyObject *tmp_mvar_value_5;
                                PyObject *tmp_kw_name_5;
                                PyObject *tmp_dict_key_5;
                                PyObject *tmp_dict_value_5;
                                PyObject *tmp_dict_key_6;
                                PyObject *tmp_dict_value_6;
                                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_heading_cell );

                                if (unlikely( tmp_mvar_value_5 == NULL ))
                                {
                                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_new_heading_cell );
                                }

                                if ( tmp_mvar_value_5 == NULL )
                                {

                                    exception_type = PyExc_NameError;
                                    Py_INCREF( exception_type );
                                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "new_heading_cell" );
                                    exception_tb = NULL;
                                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                    CHAIN_EXCEPTION( exception_value );

                                    exception_lineno = 125;
                                    type_description_1 = "ooooooo";
                                    goto frame_exception_exit_1;
                                }

                                tmp_called_name_5 = tmp_mvar_value_5;
                                tmp_dict_key_5 = const_str_plain_source;
                                CHECK_OBJECT( var_text );
                                tmp_dict_value_5 = var_text;
                                tmp_kw_name_5 = _PyDict_NewPresized( 2 );
                                tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_5, tmp_dict_value_5 );
                                assert( !(tmp_res != 0) );
                                tmp_dict_key_6 = const_str_plain_level;
                                CHECK_OBJECT( var_level );
                                tmp_dict_value_6 = var_level;
                                tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_6, tmp_dict_value_6 );
                                assert( !(tmp_res != 0) );
                                frame_3c7155fddb695906582167c0e7e21eb8->m_frame.f_lineno = 125;
                                tmp_return_value = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_5, tmp_kw_name_5 );
                                Py_DECREF( tmp_kw_name_5 );
                                if ( tmp_return_value == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 125;
                                    type_description_1 = "ooooooo";
                                    goto frame_exception_exit_1;
                                }
                                goto frame_return_exit_1;
                            }
                            branch_no_10:;
                        }
                        branch_no_9:;
                    }
                    branch_end_7:;
                }
                branch_end_5:;
            }
            branch_end_3:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3c7155fddb695906582167c0e7e21eb8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3c7155fddb695906582167c0e7e21eb8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3c7155fddb695906582167c0e7e21eb8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3c7155fddb695906582167c0e7e21eb8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3c7155fddb695906582167c0e7e21eb8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3c7155fddb695906582167c0e7e21eb8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3c7155fddb695906582167c0e7e21eb8,
        type_description_1,
        par_self,
        par_state,
        par_lines,
        par_kwargs,
        var_input,
        var_text,
        var_level
    );


    // Release cached frame.
    if ( frame_3c7155fddb695906582167c0e7e21eb8 == cache_frame_3c7155fddb695906582167c0e7e21eb8 )
    {
        Py_DECREF( frame_3c7155fddb695906582167c0e7e21eb8 );
    }
    cache_frame_3c7155fddb695906582167c0e7e21eb8 = NULL;

    assertFrameObject( frame_3c7155fddb695906582167c0e7e21eb8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_3_new_cell );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_state );
    Py_DECREF( par_state );
    par_state = NULL;

    CHECK_OBJECT( (PyObject *)par_lines );
    Py_DECREF( par_lines );
    par_lines = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_input );
    var_input = NULL;

    Py_XDECREF( var_text );
    var_text = NULL;

    Py_XDECREF( var_level );
    var_level = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_state );
    Py_DECREF( par_state );
    par_state = NULL;

    CHECK_OBJECT( (PyObject *)par_lines );
    Py_DECREF( par_lines );
    par_lines = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_input );
    var_input = NULL;

    Py_XDECREF( var_text );
    var_text = NULL;

    Py_XDECREF( var_level );
    var_level = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_3_new_cell );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v3$nbpy$$$function_4__remove_comments( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_lines = python_pars[ 1 ];
    PyObject *var_new_lines = NULL;
    PyObject *var_line = NULL;
    PyObject *var_text = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_caf610dc5992c62a0682b1140ac23345;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_caf610dc5992c62a0682b1140ac23345 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_new_lines == NULL );
        var_new_lines = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_caf610dc5992c62a0682b1140ac23345, codeobj_caf610dc5992c62a0682b1140ac23345, module_nbformat$v3$nbpy, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_caf610dc5992c62a0682b1140ac23345 = cache_frame_caf610dc5992c62a0682b1140ac23345;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_caf610dc5992c62a0682b1140ac23345 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_caf610dc5992c62a0682b1140ac23345 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( par_lines );
        tmp_iter_arg_1 = par_lines;
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                exception_lineno = 129;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_line;
            var_line = tmp_assign_source_4;
            Py_INCREF( var_line );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_line );
        tmp_called_instance_1 = var_line;
        frame_caf610dc5992c62a0682b1140ac23345->m_frame.f_lineno = 130;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_chr_35_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 130;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 130;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( var_new_lines );
            tmp_source_name_1 = var_new_lines;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_1 = "ooooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( var_line );
            tmp_subscribed_name_1 = var_line;
            tmp_subscript_name_1 = const_slice_int_pos_2_none_none;
            tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 131;
                type_description_1 = "ooooo";
                goto try_except_handler_2;
            }
            frame_caf610dc5992c62a0682b1140ac23345->m_frame.f_lineno = 131;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_1 = "ooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( var_new_lines );
            tmp_called_instance_2 = var_new_lines;
            CHECK_OBJECT( var_line );
            tmp_args_element_name_2 = var_line;
            frame_caf610dc5992c62a0682b1140ac23345->m_frame.f_lineno = 133;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;
                type_description_1 = "ooooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_end_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 129;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_args_element_name_3;
        tmp_called_instance_3 = const_str_newline;
        CHECK_OBJECT( var_new_lines );
        tmp_args_element_name_3 = var_new_lines;
        frame_caf610dc5992c62a0682b1140ac23345->m_frame.f_lineno = 134;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_join, call_args );
        }

        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_text == NULL );
        var_text = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_instance_4;
        CHECK_OBJECT( var_text );
        tmp_called_instance_4 = var_text;
        frame_caf610dc5992c62a0682b1140ac23345->m_frame.f_lineno = 135;
        tmp_assign_source_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_strip, &PyTuple_GET_ITEM( const_tuple_str_newline_tuple, 0 ) );

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_text;
            assert( old != NULL );
            var_text = tmp_assign_source_6;
            Py_DECREF( old );
        }

    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_caf610dc5992c62a0682b1140ac23345 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_caf610dc5992c62a0682b1140ac23345 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_caf610dc5992c62a0682b1140ac23345, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_caf610dc5992c62a0682b1140ac23345->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_caf610dc5992c62a0682b1140ac23345, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_caf610dc5992c62a0682b1140ac23345,
        type_description_1,
        par_self,
        par_lines,
        var_new_lines,
        var_line,
        var_text
    );


    // Release cached frame.
    if ( frame_caf610dc5992c62a0682b1140ac23345 == cache_frame_caf610dc5992c62a0682b1140ac23345 )
    {
        Py_DECREF( frame_caf610dc5992c62a0682b1140ac23345 );
    }
    cache_frame_caf610dc5992c62a0682b1140ac23345 = NULL;

    assertFrameObject( frame_caf610dc5992c62a0682b1140ac23345 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_text );
    tmp_return_value = var_text;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_4__remove_comments );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_lines );
    Py_DECREF( par_lines );
    par_lines = NULL;

    CHECK_OBJECT( (PyObject *)var_new_lines );
    Py_DECREF( var_new_lines );
    var_new_lines = NULL;

    Py_XDECREF( var_line );
    var_line = NULL;

    CHECK_OBJECT( (PyObject *)var_text );
    Py_DECREF( var_text );
    var_text = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_lines );
    Py_DECREF( par_lines );
    par_lines = NULL;

    CHECK_OBJECT( (PyObject *)var_new_lines );
    Py_DECREF( var_new_lines );
    var_new_lines = NULL;

    Py_XDECREF( var_line );
    var_line = NULL;

    Py_XDECREF( var_text );
    var_text = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_4__remove_comments );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$v3$nbpy$$$function_5_split_lines_into_blocks( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_CellObject *par_lines = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks_maker();

    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] = par_lines;
    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_GeneratorObject *)tmp_return_value)->m_closure[1] = PyCell_NEW0( par_self );


    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_5_split_lines_into_blocks );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_lines );
    Py_DECREF( par_lines );
    par_lines = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_lines );
    Py_DECREF( par_lines );
    par_lines = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_5_split_lines_into_blocks );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks_locals {
    PyObject *var_ast;
    PyObject *var_source;
    PyObject *var_code;
    PyObject *var_starts;
    PyObject *var_i;
    PyObject *outline_0_var_x;
    PyObject *tmp_for_loop_1__for_iterator;
    PyObject *tmp_for_loop_1__iter_value;
    PyObject *tmp_listcomp_1__$0;
    PyObject *tmp_listcomp_1__contraction;
    PyObject *tmp_listcomp_1__iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    struct Nuitka_FrameObject *frame_db67969e0889f1e67a65348b6afbf76d_2;
    char const *type_description_2;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks_locals *generator_heap = (struct nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 3: goto yield_return_3;
    case 2: goto yield_return_2;
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_frame_db67969e0889f1e67a65348b6afbf76d_2 = NULL;
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_ast = NULL;
    generator_heap->var_source = NULL;
    generator_heap->var_code = NULL;
    generator_heap->var_starts = NULL;
    generator_heap->var_i = NULL;
    generator_heap->outline_0_var_x = NULL;
    generator_heap->tmp_for_loop_1__for_iterator = NULL;
    generator_heap->tmp_for_loop_1__iter_value = NULL;
    generator_heap->tmp_listcomp_1__$0 = NULL;
    generator_heap->tmp_listcomp_1__contraction = NULL;
    generator_heap->tmp_listcomp_1__iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;
    generator_heap->type_description_2 = NULL;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_9c9c9b47c83ccf8873a3073eb6ba3519, module_nbformat$v3$nbpy, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "lines" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 139;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }

        tmp_len_arg_1 = PyCell_GET( generator->m_closure[0] );
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 139;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_1;
        generator_heap->tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        assert( !(generator_heap->tmp_res == -1) );
        tmp_condition_result_1 = ( generator_heap->tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_expression_name_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
            if ( PyCell_GET( generator->m_closure[0] ) == NULL )
            {

                generator_heap->exception_type = PyExc_NameError;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "lines" );
                generator_heap->exception_tb = NULL;
                NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                CHAIN_EXCEPTION( generator_heap->exception_value );

                generator_heap->exception_lineno = 140;
                generator_heap->type_description_1 = "ccooooo";
                goto frame_exception_exit_1;
            }

            tmp_subscribed_name_1 = PyCell_GET( generator->m_closure[0] );
            tmp_subscript_name_1 = const_int_0;
            tmp_expression_name_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_expression_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 140;
                generator_heap->type_description_1 = "ccooooo";
                goto frame_exception_exit_1;
            }
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), NULL );
            generator->m_yield_return_index = 1;
            return tmp_expression_name_1;
            yield_return_1:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_len_arg_1, sizeof(PyObject *), &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 140;
                generator_heap->type_description_1 = "ccooooo";
                goto frame_exception_exit_1;
            }
            tmp_yield_result_1 = yield_return_value;
        }
        {
            PyObject *tmp_raise_type_1;
            generator->m_frame->m_frame.f_lineno = 141;
            tmp_raise_type_1 = CALL_FUNCTION_NO_ARGS( PyExc_StopIteration );
            assert( !(tmp_raise_type_1 == NULL) );
            generator_heap->exception_type = tmp_raise_type_1;
            generator_heap->exception_lineno = 141;
            RAISE_EXCEPTION_WITH_TYPE( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_ast;
        tmp_globals_name_1 = (PyObject *)moduledict_nbformat$v3$nbpy;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        generator->m_frame->m_frame.f_lineno = 142;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 142;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->var_ast == NULL );
        generator_heap->var_ast = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        tmp_source_name_1 = const_str_newline;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        assert( !(tmp_called_name_1 == NULL) );
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "lines" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 143;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = PyCell_GET( generator->m_closure[0] );
        generator->m_frame->m_frame.f_lineno = 143;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 143;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->var_source == NULL );
        generator_heap->var_source = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( generator_heap->var_ast );
        tmp_called_instance_1 = generator_heap->var_ast;
        CHECK_OBJECT( generator_heap->var_source );
        tmp_args_element_name_2 = generator_heap->var_source;
        generator->m_frame->m_frame.f_lineno = 144;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_parse, call_args );
        }

        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 144;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->var_code == NULL );
        generator_heap->var_code = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        // Tried code:
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( generator_heap->var_code );
            tmp_source_name_2 = generator_heap->var_code;
            tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_body );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 145;
                generator_heap->type_description_1 = "ccooooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 145;
                generator_heap->type_description_1 = "ccooooo";
                goto try_except_handler_2;
            }
            assert( generator_heap->tmp_listcomp_1__$0 == NULL );
            generator_heap->tmp_listcomp_1__$0 = tmp_assign_source_5;
        }
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = PyList_New( 0 );
            assert( generator_heap->tmp_listcomp_1__contraction == NULL );
            generator_heap->tmp_listcomp_1__contraction = tmp_assign_source_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_db67969e0889f1e67a65348b6afbf76d_2, codeobj_db67969e0889f1e67a65348b6afbf76d, module_nbformat$v3$nbpy, sizeof(void *) );
        generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2 = cache_frame_db67969e0889f1e67a65348b6afbf76d_2;

        // Push the new frame as the currently active one.
        pushFrameStack( generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( generator_heap->tmp_listcomp_1__$0 );
            tmp_next_source_1 = generator_heap->tmp_listcomp_1__$0;
            tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                    generator_heap->type_description_2 = "o";
                    generator_heap->exception_lineno = 145;
                    goto try_except_handler_3;
                }
            }

            {
                PyObject *old = generator_heap->tmp_listcomp_1__iter_value_0;
                generator_heap->tmp_listcomp_1__iter_value_0 = tmp_assign_source_7;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_8;
            CHECK_OBJECT( generator_heap->tmp_listcomp_1__iter_value_0 );
            tmp_assign_source_8 = generator_heap->tmp_listcomp_1__iter_value_0;
            {
                PyObject *old = generator_heap->outline_0_var_x;
                generator_heap->outline_0_var_x = tmp_assign_source_8;
                Py_INCREF( generator_heap->outline_0_var_x );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_1;
            PyObject *tmp_append_value_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_right_name_1;
            CHECK_OBJECT( generator_heap->tmp_listcomp_1__contraction );
            tmp_append_list_1 = generator_heap->tmp_listcomp_1__contraction;
            CHECK_OBJECT( generator_heap->outline_0_var_x );
            tmp_source_name_3 = generator_heap->outline_0_var_x;
            tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_lineno );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 145;
                generator_heap->type_description_2 = "o";
                goto try_except_handler_3;
            }
            tmp_right_name_1 = const_int_pos_1;
            tmp_append_value_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_append_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 145;
                generator_heap->type_description_2 = "o";
                goto try_except_handler_3;
            }
            assert( PyList_Check( tmp_append_list_1 ) );
            generator_heap->tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
            Py_DECREF( tmp_append_value_1 );
            if ( generator_heap->tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 145;
                generator_heap->type_description_2 = "o";
                goto try_except_handler_3;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 145;
            generator_heap->type_description_2 = "o";
            goto try_except_handler_3;
        }
        goto loop_start_1;
        loop_end_1:;
        CHECK_OBJECT( generator_heap->tmp_listcomp_1__contraction );
        tmp_assign_source_4 = generator_heap->tmp_listcomp_1__contraction;
        Py_INCREF( tmp_assign_source_4 );
        goto try_return_handler_3;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_1__$0 );
        Py_DECREF( generator_heap->tmp_listcomp_1__$0 );
        generator_heap->tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_1__contraction );
        Py_DECREF( generator_heap->tmp_listcomp_1__contraction );
        generator_heap->tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( generator_heap->tmp_listcomp_1__iter_value_0 );
        generator_heap->tmp_listcomp_1__iter_value_0 = NULL;

        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_3:;
        generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
        generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
        generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
        generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
        generator_heap->exception_type = NULL;
        generator_heap->exception_value = NULL;
        generator_heap->exception_tb = NULL;
        generator_heap->exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_1__$0 );
        Py_DECREF( generator_heap->tmp_listcomp_1__$0 );
        generator_heap->tmp_listcomp_1__$0 = NULL;

        CHECK_OBJECT( (PyObject *)generator_heap->tmp_listcomp_1__contraction );
        Py_DECREF( generator_heap->tmp_listcomp_1__contraction );
        generator_heap->tmp_listcomp_1__contraction = NULL;

        Py_XDECREF( generator_heap->tmp_listcomp_1__iter_value_0 );
        generator_heap->tmp_listcomp_1__iter_value_0 = NULL;

        // Re-raise.
        generator_heap->exception_type = generator_heap->exception_keeper_type_1;
        generator_heap->exception_value = generator_heap->exception_keeper_value_1;
        generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
        generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

        goto frame_exception_exit_2;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_return_exit_1:;
#if 0
        RESTORE_FRAME_EXCEPTION( generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_2;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2 );
#endif

        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2, generator_heap->exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2,
            generator_heap->type_description_2,
            generator_heap->outline_0_var_x
        );


        // Release cached frame.
        if ( generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2 == cache_frame_db67969e0889f1e67a65348b6afbf76d_2 )
        {
            Py_DECREF( generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2 );
        }
        cache_frame_db67969e0889f1e67a65348b6afbf76d_2 = NULL;

        assertFrameObject( generator_heap->frame_db67969e0889f1e67a65348b6afbf76d_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;
        generator_heap->type_description_1 = "ccooooo";
        goto try_except_handler_2;
        skip_nested_handling_1:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        Py_XDECREF( generator_heap->outline_0_var_x );
        generator_heap->outline_0_var_x = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_2:;
        generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
        generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
        generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
        generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
        generator_heap->exception_type = NULL;
        generator_heap->exception_value = NULL;
        generator_heap->exception_tb = NULL;
        generator_heap->exception_lineno = 0;

        Py_XDECREF( generator_heap->outline_0_var_x );
        generator_heap->outline_0_var_x = NULL;

        // Re-raise.
        generator_heap->exception_type = generator_heap->exception_keeper_type_2;
        generator_heap->exception_value = generator_heap->exception_keeper_value_2;
        generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
        generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks );
        return NULL;
        outline_exception_1:;
        generator_heap->exception_lineno = 145;
        goto frame_exception_exit_1;
        outline_result_1:;
        assert( generator_heap->var_starts == NULL );
        generator_heap->var_starts = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_xrange_low_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_len_arg_2;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( generator_heap->var_starts );
        tmp_len_arg_2 = generator_heap->var_starts;
        tmp_left_name_2 = BUILTIN_LEN( tmp_len_arg_2 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 146;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_1;
        tmp_xrange_low_1 = BINARY_OPERATION_SUB_LONG_LONG( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        assert( !(tmp_xrange_low_1 == NULL) );
        tmp_iter_arg_2 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
        Py_DECREF( tmp_xrange_low_1 );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 146;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 146;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        assert( generator_heap->tmp_for_loop_1__for_iterator == NULL );
        generator_heap->tmp_for_loop_1__for_iterator = tmp_assign_source_9;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__for_iterator );
        tmp_next_source_2 = generator_heap->tmp_for_loop_1__for_iterator;
        tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_10 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "ccooooo";
                generator_heap->exception_lineno = 146;
                goto try_except_handler_4;
            }
        }

        {
            PyObject *old = generator_heap->tmp_for_loop_1__iter_value;
            generator_heap->tmp_for_loop_1__iter_value = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( generator_heap->tmp_for_loop_1__iter_value );
        tmp_assign_source_11 = generator_heap->tmp_for_loop_1__iter_value;
        {
            PyObject *old = generator_heap->var_i;
            generator_heap->var_i = tmp_assign_source_11;
            Py_INCREF( generator_heap->var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_start_name_1;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_step_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_2;
        tmp_source_name_4 = const_str_newline;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_join );
        assert( !(tmp_called_name_2 == NULL) );
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "lines" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 147;
            generator_heap->type_description_1 = "ccooooo";
            goto try_except_handler_4;
        }

        tmp_subscribed_name_2 = PyCell_GET( generator->m_closure[0] );
        CHECK_OBJECT( generator_heap->var_starts );
        tmp_subscribed_name_3 = generator_heap->var_starts;
        CHECK_OBJECT( generator_heap->var_i );
        tmp_subscript_name_3 = generator_heap->var_i;
        tmp_start_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
        if ( tmp_start_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );

            generator_heap->exception_lineno = 147;
            generator_heap->type_description_1 = "ccooooo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( generator_heap->var_starts );
        tmp_subscribed_name_4 = generator_heap->var_starts;
        CHECK_OBJECT( generator_heap->var_i );
        tmp_left_name_3 = generator_heap->var_i;
        tmp_right_name_3 = const_int_pos_1;
        tmp_subscript_name_4 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_3, tmp_right_name_3 );
        if ( tmp_subscript_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_start_name_1 );

            generator_heap->exception_lineno = 147;
            generator_heap->type_description_1 = "ccooooo";
            goto try_except_handler_4;
        }
        tmp_stop_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
        Py_DECREF( tmp_subscript_name_4 );
        if ( tmp_stop_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_start_name_1 );

            generator_heap->exception_lineno = 147;
            generator_heap->type_description_1 = "ccooooo";
            goto try_except_handler_4;
        }
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_2 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        Py_DECREF( tmp_start_name_1 );
        Py_DECREF( tmp_stop_name_1 );
        assert( !(tmp_subscript_name_2 == NULL) );
        tmp_args_element_name_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        Py_DECREF( tmp_subscript_name_2 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_2 );

            generator_heap->exception_lineno = 147;
            generator_heap->type_description_1 = "ccooooo";
            goto try_except_handler_4;
        }
        generator->m_frame->m_frame.f_lineno = 147;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_called_instance_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 147;
            generator_heap->type_description_1 = "ccooooo";
            goto try_except_handler_4;
        }
        generator->m_frame->m_frame.f_lineno = 147;
        tmp_expression_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_strip, &PyTuple_GET_ITEM( const_tuple_str_newline_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 147;
            generator_heap->type_description_1 = "ccooooo";
            goto try_except_handler_4;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_instance_2, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_source_name_4, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_subscribed_name_2, sizeof(PyObject *), &tmp_subscript_name_2, sizeof(PyObject *), &tmp_start_name_1, sizeof(PyObject *), &tmp_subscribed_name_3, sizeof(PyObject *), &tmp_subscript_name_3, sizeof(PyObject *), &tmp_stop_name_1, sizeof(PyObject *), &tmp_subscribed_name_4, sizeof(PyObject *), &tmp_subscript_name_4, sizeof(PyObject *), &tmp_left_name_3, sizeof(PyObject *), &tmp_right_name_3, sizeof(PyObject *), &tmp_step_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 2;
        return tmp_expression_name_2;
        yield_return_2:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_instance_2, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_source_name_4, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_subscribed_name_2, sizeof(PyObject *), &tmp_subscript_name_2, sizeof(PyObject *), &tmp_start_name_1, sizeof(PyObject *), &tmp_subscribed_name_3, sizeof(PyObject *), &tmp_subscript_name_3, sizeof(PyObject *), &tmp_stop_name_1, sizeof(PyObject *), &tmp_subscribed_name_4, sizeof(PyObject *), &tmp_subscript_name_4, sizeof(PyObject *), &tmp_left_name_3, sizeof(PyObject *), &tmp_right_name_3, sizeof(PyObject *), &tmp_step_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 147;
            generator_heap->type_description_1 = "ccooooo";
            goto try_except_handler_4;
        }
        tmp_yield_result_2 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 146;
        generator_heap->type_description_1 = "ccooooo";
        goto try_except_handler_4;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( generator_heap->tmp_for_loop_1__iter_value );
    generator_heap->tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_for_loop_1__for_iterator );
    Py_DECREF( generator_heap->tmp_for_loop_1__for_iterator );
    generator_heap->tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_expression_name_3;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_start_name_2;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_subscript_name_6;
        PyObject *tmp_stop_name_2;
        PyObject *tmp_step_name_2;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_3;
        tmp_source_name_5 = const_str_newline;
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_join );
        assert( !(tmp_called_name_3 == NULL) );
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "lines" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 148;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_5 = PyCell_GET( generator->m_closure[0] );
        CHECK_OBJECT( generator_heap->var_starts );
        tmp_subscribed_name_6 = generator_heap->var_starts;
        tmp_subscript_name_6 = const_int_neg_1;
        tmp_start_name_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, -1 );
        if ( tmp_start_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_3 );

            generator_heap->exception_lineno = 148;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        tmp_stop_name_2 = Py_None;
        tmp_step_name_2 = Py_None;
        tmp_subscript_name_5 = MAKE_SLICEOBJ3( tmp_start_name_2, tmp_stop_name_2, tmp_step_name_2 );
        Py_DECREF( tmp_start_name_2 );
        assert( !(tmp_subscript_name_5 == NULL) );
        tmp_args_element_name_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
        Py_DECREF( tmp_subscript_name_5 );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_called_name_3 );

            generator_heap->exception_lineno = 148;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        generator->m_frame->m_frame.f_lineno = 148;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_called_instance_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 148;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        generator->m_frame->m_frame.f_lineno = 148;
        tmp_expression_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_strip, &PyTuple_GET_ITEM( const_tuple_str_newline_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_expression_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 148;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_instance_3, sizeof(PyObject *), &tmp_called_name_3, sizeof(PyObject *), &tmp_source_name_5, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), &tmp_subscribed_name_5, sizeof(PyObject *), &tmp_subscript_name_5, sizeof(PyObject *), &tmp_start_name_2, sizeof(PyObject *), &tmp_subscribed_name_6, sizeof(PyObject *), &tmp_subscript_name_6, sizeof(PyObject *), &tmp_stop_name_2, sizeof(PyObject *), &tmp_step_name_2, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 3;
        return tmp_expression_name_3;
        yield_return_3:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_instance_3, sizeof(PyObject *), &tmp_called_name_3, sizeof(PyObject *), &tmp_source_name_5, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), &tmp_subscribed_name_5, sizeof(PyObject *), &tmp_subscript_name_5, sizeof(PyObject *), &tmp_start_name_2, sizeof(PyObject *), &tmp_subscribed_name_6, sizeof(PyObject *), &tmp_subscript_name_6, sizeof(PyObject *), &tmp_stop_name_2, sizeof(PyObject *), &tmp_step_name_2, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 148;
            generator_heap->type_description_1 = "ccooooo";
            goto frame_exception_exit_1;
        }
        tmp_yield_result_3 = yield_return_value;
    }

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_2;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            generator->m_closure[1],
            generator->m_closure[0],
            generator_heap->var_ast,
            generator_heap->var_source,
            generator_heap->var_code,
            generator_heap->var_starts,
            generator_heap->var_i
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_ast );
    generator_heap->var_ast = NULL;

    Py_XDECREF( generator_heap->var_source );
    generator_heap->var_source = NULL;

    Py_XDECREF( generator_heap->var_code );
    generator_heap->var_code = NULL;

    Py_XDECREF( generator_heap->var_starts );
    generator_heap->var_starts = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->var_ast );
    Py_DECREF( generator_heap->var_ast );
    generator_heap->var_ast = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->var_source );
    Py_DECREF( generator_heap->var_source );
    generator_heap->var_source = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->var_code );
    Py_DECREF( generator_heap->var_code );
    generator_heap->var_code = NULL;

    CHECK_OBJECT( (PyObject *)generator_heap->var_starts );
    Py_DECREF( generator_heap->var_starts );
    generator_heap->var_starts = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks_maker( void )
{
    return Nuitka_Generator_New(
        nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks_context,
        module_nbformat$v3$nbpy,
        const_str_plain_split_lines_into_blocks,
#if PYTHON_VERSION >= 350
        const_str_digest_43d123edad7181f56494a8200b84a35c,
#endif
        codeobj_9c9c9b47c83ccf8873a3073eb6ba3519,
        2,
        sizeof(struct nbformat$v3$nbpy$$$function_5_split_lines_into_blocks$$$genobj_1_split_lines_into_blocks_locals)
    );
}


static PyObject *impl_nbformat$v3$nbpy$$$function_6_writes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_nb = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_lines = NULL;
    PyObject *var_ws = NULL;
    PyObject *var_cell = NULL;
    PyObject *var_input = NULL;
    PyObject *var_level = NULL;
    PyObject *outline_0_var_line = NULL;
    PyObject *outline_1_var_line = NULL;
    PyObject *outline_2_var_line = NULL;
    PyObject *outline_3_var_line = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_2__$0 = NULL;
    PyObject *tmp_listcomp_2__contraction = NULL;
    PyObject *tmp_listcomp_2__iter_value_0 = NULL;
    PyObject *tmp_listcomp_3__$0 = NULL;
    PyObject *tmp_listcomp_3__contraction = NULL;
    PyObject *tmp_listcomp_3__iter_value_0 = NULL;
    PyObject *tmp_listcomp_4__$0 = NULL;
    PyObject *tmp_listcomp_4__contraction = NULL;
    PyObject *tmp_listcomp_4__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_32003efde0d7f7755fbe30fab89f2b4f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    struct Nuitka_FrameObject *frame_eb26b3964260d822c88a7fae45c3d049_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_eb26b3964260d822c88a7fae45c3d049_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    struct Nuitka_FrameObject *frame_d2728b061cb2bcfa5fded6401f5ec7d3_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    struct Nuitka_FrameObject *frame_c2a8dffdae9b7d568e4654a49f86edf8_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_c2a8dffdae9b7d568e4654a49f86edf8_4 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    struct Nuitka_FrameObject *frame_e2b6c5150ad64bf469296b400d92c9ff_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    static struct Nuitka_FrameObject *cache_frame_e2b6c5150ad64bf469296b400d92c9ff_5 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_32003efde0d7f7755fbe30fab89f2b4f = NULL;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = LIST_COPY( const_list_str_digest_bd2398fc2b513d07b5e5840b7bb6c7c9_list );
        assert( var_lines == NULL );
        var_lines = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_32003efde0d7f7755fbe30fab89f2b4f, codeobj_32003efde0d7f7755fbe30fab89f2b4f, module_nbformat$v3$nbpy, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_32003efde0d7f7755fbe30fab89f2b4f = cache_frame_32003efde0d7f7755fbe30fab89f2b4f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_32003efde0d7f7755fbe30fab89f2b4f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_32003efde0d7f7755fbe30fab89f2b4f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_list_element_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( var_lines );
        tmp_source_name_1 = var_lines;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_extend );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = const_str_digest_49a2f4a86b9b100a47c479fad7247d4b;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_nbformat );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_nbformat );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "nbformat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = tmp_mvar_value_1;
        tmp_right_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_nbformat_minor );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_nbformat_minor );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_right_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "nbformat_minor" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = tmp_mvar_value_2;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_list_element_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 156;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = PyList_New( 2 );
        PyList_SET_ITEM( tmp_args_element_name_1, 0, tmp_list_element_1 );
        tmp_list_element_1 = const_str_empty;
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_1, 1, tmp_list_element_1 );
        frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 155;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 155;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_nb );
        tmp_source_name_2 = par_nb;
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_worksheets );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 159;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_ws;
            var_ws = tmp_assign_source_4;
            Py_INCREF( var_ws );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_ws );
        tmp_source_name_3 = var_ws;
        tmp_iter_arg_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_cells );
        if ( tmp_iter_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_2 );
        Py_DECREF( tmp_iter_arg_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 160;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = tmp_for_loop_2__for_iterator;
            tmp_for_loop_2__for_iterator = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 160;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_7 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_cell;
            var_cell = tmp_assign_source_7;
            Py_INCREF( var_cell );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( var_cell );
        tmp_source_name_4 = var_cell;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_cell_type );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_compexpr_right_1 = const_str_plain_code;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( var_cell );
            tmp_called_instance_1 = var_cell;
            frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 162;
            tmp_assign_source_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_input_tuple, 0 ) );

            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_input;
                var_input = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( var_input );
            tmp_compexpr_left_2 = var_input;
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_instance_2;
                PyObject *tmp_call_result_2;
                PyObject *tmp_call_arg_element_1;
                CHECK_OBJECT( var_lines );
                tmp_called_instance_2 = var_lines;
                tmp_call_arg_element_1 = LIST_COPY( const_list_str_digest_0ef8517a2e0443cae5679ec66d74a519_str_empty_list );
                frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 164;
                {
                    PyObject *call_args[] = { tmp_call_arg_element_1 };
                    tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_extend, call_args );
                }

                Py_DECREF( tmp_call_arg_element_1 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 164;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_3;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            {
                PyObject *tmp_called_name_2;
                PyObject *tmp_source_name_5;
                PyObject *tmp_call_result_3;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_called_instance_3;
                CHECK_OBJECT( var_lines );
                tmp_source_name_5 = var_lines;
                tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_extend );
                if ( tmp_called_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 165;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_3;
                }
                CHECK_OBJECT( var_input );
                tmp_called_instance_3 = var_input;
                frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 165;
                tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_splitlines );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_2 );

                    exception_lineno = 165;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_3;
                }
                frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 165;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                }

                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 165;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_3;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            {
                PyObject *tmp_called_instance_4;
                PyObject *tmp_call_result_4;
                CHECK_OBJECT( var_lines );
                tmp_called_instance_4 = var_lines;
                frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 166;
                tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_empty_tuple, 0 ) );

                if ( tmp_call_result_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 166;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_3;
                }
                Py_DECREF( tmp_call_result_4 );
            }
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( var_cell );
            tmp_source_name_6 = var_cell;
            tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_cell_type );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 167;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            tmp_compexpr_right_3 = const_str_plain_html;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 167;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_called_instance_5;
                CHECK_OBJECT( var_cell );
                tmp_called_instance_5 = var_cell;
                frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 168;
                tmp_assign_source_9 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_source_tuple, 0 ) );

                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 168;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = var_input;
                    var_input = tmp_assign_source_9;
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                CHECK_OBJECT( var_input );
                tmp_compexpr_left_4 = var_input;
                tmp_compexpr_right_4 = Py_None;
                tmp_condition_result_4 = ( tmp_compexpr_left_4 != tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_called_instance_6;
                    PyObject *tmp_call_result_5;
                    PyObject *tmp_call_arg_element_2;
                    CHECK_OBJECT( var_lines );
                    tmp_called_instance_6 = var_lines;
                    tmp_call_arg_element_2 = LIST_COPY( const_list_str_digest_783cb5b32947f6074d02faf31f87f24e_str_empty_list );
                    frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 170;
                    {
                        PyObject *call_args[] = { tmp_call_arg_element_2 };
                        tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_extend, call_args );
                    }

                    Py_DECREF( tmp_call_arg_element_2 );
                    if ( tmp_call_result_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 170;
                        type_description_1 = "oooooooo";
                        goto try_except_handler_3;
                    }
                    Py_DECREF( tmp_call_result_5 );
                }
                {
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_source_name_7;
                    PyObject *tmp_call_result_6;
                    PyObject *tmp_args_element_name_3;
                    CHECK_OBJECT( var_lines );
                    tmp_source_name_7 = var_lines;
                    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_extend );
                    if ( tmp_called_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 171;
                        type_description_1 = "oooooooo";
                        goto try_except_handler_3;
                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_10;
                        PyObject *tmp_iter_arg_3;
                        PyObject *tmp_called_instance_7;
                        CHECK_OBJECT( var_input );
                        tmp_called_instance_7 = var_input;
                        frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 171;
                        tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_7, const_str_plain_splitlines );
                        if ( tmp_iter_arg_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 171;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_4;
                        }
                        tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_3 );
                        Py_DECREF( tmp_iter_arg_3 );
                        if ( tmp_assign_source_10 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 171;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_4;
                        }
                        {
                            PyObject *old = tmp_listcomp_1__$0;
                            tmp_listcomp_1__$0 = tmp_assign_source_10;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_11;
                        tmp_assign_source_11 = PyList_New( 0 );
                        {
                            PyObject *old = tmp_listcomp_1__contraction;
                            tmp_listcomp_1__contraction = tmp_assign_source_11;
                            Py_XDECREF( old );
                        }

                    }
                    MAKE_OR_REUSE_FRAME( cache_frame_eb26b3964260d822c88a7fae45c3d049_2, codeobj_eb26b3964260d822c88a7fae45c3d049, module_nbformat$v3$nbpy, sizeof(void *) );
                    frame_eb26b3964260d822c88a7fae45c3d049_2 = cache_frame_eb26b3964260d822c88a7fae45c3d049_2;

                    // Push the new frame as the currently active one.
                    pushFrameStack( frame_eb26b3964260d822c88a7fae45c3d049_2 );

                    // Mark the frame object as in use, ref count 1 will be up for reuse.
                    assert( Py_REFCNT( frame_eb26b3964260d822c88a7fae45c3d049_2 ) == 2 ); // Frame stack

                    // Framed code:
                    // Tried code:
                    loop_start_3:;
                    {
                        PyObject *tmp_next_source_3;
                        PyObject *tmp_assign_source_12;
                        CHECK_OBJECT( tmp_listcomp_1__$0 );
                        tmp_next_source_3 = tmp_listcomp_1__$0;
                        tmp_assign_source_12 = ITERATOR_NEXT( tmp_next_source_3 );
                        if ( tmp_assign_source_12 == NULL )
                        {
                            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                            {

                                goto loop_end_3;
                            }
                            else
                            {

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                type_description_2 = "o";
                                exception_lineno = 171;
                                goto try_except_handler_5;
                            }
                        }

                        {
                            PyObject *old = tmp_listcomp_1__iter_value_0;
                            tmp_listcomp_1__iter_value_0 = tmp_assign_source_12;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_13;
                        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                        tmp_assign_source_13 = tmp_listcomp_1__iter_value_0;
                        {
                            PyObject *old = outline_0_var_line;
                            outline_0_var_line = tmp_assign_source_13;
                            Py_INCREF( outline_0_var_line );
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_append_list_1;
                        PyObject *tmp_append_value_1;
                        PyObject *tmp_left_name_2;
                        PyObject *tmp_right_name_2;
                        CHECK_OBJECT( tmp_listcomp_1__contraction );
                        tmp_append_list_1 = tmp_listcomp_1__contraction;
                        tmp_left_name_2 = const_str_digest_6566768af533234b42110c37c6c377fd;
                        CHECK_OBJECT( outline_0_var_line );
                        tmp_right_name_2 = outline_0_var_line;
                        tmp_append_value_1 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_2, tmp_right_name_2 );
                        if ( tmp_append_value_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 171;
                            type_description_2 = "o";
                            goto try_except_handler_5;
                        }
                        assert( PyList_Check( tmp_append_list_1 ) );
                        tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                        Py_DECREF( tmp_append_value_1 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 171;
                            type_description_2 = "o";
                            goto try_except_handler_5;
                        }
                    }
                    if ( CONSIDER_THREADING() == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 171;
                        type_description_2 = "o";
                        goto try_except_handler_5;
                    }
                    goto loop_start_3;
                    loop_end_3:;
                    CHECK_OBJECT( tmp_listcomp_1__contraction );
                    tmp_args_element_name_3 = tmp_listcomp_1__contraction;
                    Py_INCREF( tmp_args_element_name_3 );
                    goto try_return_handler_5;
                    // tried codes exits in all cases
                    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                    return NULL;
                    // Return handler code:
                    try_return_handler_5:;
                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                    Py_DECREF( tmp_listcomp_1__$0 );
                    tmp_listcomp_1__$0 = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                    Py_DECREF( tmp_listcomp_1__contraction );
                    tmp_listcomp_1__contraction = NULL;

                    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                    tmp_listcomp_1__iter_value_0 = NULL;

                    goto frame_return_exit_2;
                    // Exception handler code:
                    try_except_handler_5:;
                    exception_keeper_type_1 = exception_type;
                    exception_keeper_value_1 = exception_value;
                    exception_keeper_tb_1 = exception_tb;
                    exception_keeper_lineno_1 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                    Py_DECREF( tmp_listcomp_1__$0 );
                    tmp_listcomp_1__$0 = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                    Py_DECREF( tmp_listcomp_1__contraction );
                    tmp_listcomp_1__contraction = NULL;

                    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                    tmp_listcomp_1__iter_value_0 = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_1;
                    exception_value = exception_keeper_value_1;
                    exception_tb = exception_keeper_tb_1;
                    exception_lineno = exception_keeper_lineno_1;

                    goto frame_exception_exit_2;
                    // End of try:

#if 0
                    RESTORE_FRAME_EXCEPTION( frame_eb26b3964260d822c88a7fae45c3d049_2 );
#endif

                    // Put the previous frame back on top.
                    popFrameStack();

                    goto frame_no_exception_1;

                    frame_return_exit_2:;
#if 0
                    RESTORE_FRAME_EXCEPTION( frame_eb26b3964260d822c88a7fae45c3d049_2 );
#endif

                    // Put the previous frame back on top.
                    popFrameStack();

                    goto try_return_handler_4;

                    frame_exception_exit_2:;

#if 0
                    RESTORE_FRAME_EXCEPTION( frame_eb26b3964260d822c88a7fae45c3d049_2 );
#endif

                    if ( exception_tb == NULL )
                    {
                        exception_tb = MAKE_TRACEBACK( frame_eb26b3964260d822c88a7fae45c3d049_2, exception_lineno );
                    }
                    else if ( exception_tb->tb_frame != &frame_eb26b3964260d822c88a7fae45c3d049_2->m_frame )
                    {
                        exception_tb = ADD_TRACEBACK( exception_tb, frame_eb26b3964260d822c88a7fae45c3d049_2, exception_lineno );
                    }

                    // Attachs locals to frame if any.
                    Nuitka_Frame_AttachLocals(
                        (struct Nuitka_FrameObject *)frame_eb26b3964260d822c88a7fae45c3d049_2,
                        type_description_2,
                        outline_0_var_line
                    );


                    // Release cached frame.
                    if ( frame_eb26b3964260d822c88a7fae45c3d049_2 == cache_frame_eb26b3964260d822c88a7fae45c3d049_2 )
                    {
                        Py_DECREF( frame_eb26b3964260d822c88a7fae45c3d049_2 );
                    }
                    cache_frame_eb26b3964260d822c88a7fae45c3d049_2 = NULL;

                    assertFrameObject( frame_eb26b3964260d822c88a7fae45c3d049_2 );

                    // Put the previous frame back on top.
                    popFrameStack();

                    // Return the error.
                    goto nested_frame_exit_1;

                    frame_no_exception_1:;
                    goto skip_nested_handling_1;
                    nested_frame_exit_1:;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_4;
                    skip_nested_handling_1:;
                    // tried codes exits in all cases
                    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                    return NULL;
                    // Return handler code:
                    try_return_handler_4:;
                    Py_XDECREF( outline_0_var_line );
                    outline_0_var_line = NULL;

                    goto outline_result_1;
                    // Exception handler code:
                    try_except_handler_4:;
                    exception_keeper_type_2 = exception_type;
                    exception_keeper_value_2 = exception_value;
                    exception_keeper_tb_2 = exception_tb;
                    exception_keeper_lineno_2 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( outline_0_var_line );
                    outline_0_var_line = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_2;
                    exception_value = exception_keeper_value_2;
                    exception_tb = exception_keeper_tb_2;
                    exception_lineno = exception_keeper_lineno_2;

                    goto outline_exception_1;
                    // End of try:
                    // Return statement must have exited already.
                    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                    return NULL;
                    outline_exception_1:;
                    exception_lineno = 171;
                    goto try_except_handler_3;
                    outline_result_1:;
                    frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 171;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_3 };
                        tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                    }

                    Py_DECREF( tmp_called_name_3 );
                    Py_DECREF( tmp_args_element_name_3 );
                    if ( tmp_call_result_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 171;
                        type_description_1 = "oooooooo";
                        goto try_except_handler_3;
                    }
                    Py_DECREF( tmp_call_result_6 );
                }
                {
                    PyObject *tmp_called_instance_8;
                    PyObject *tmp_call_result_7;
                    CHECK_OBJECT( var_lines );
                    tmp_called_instance_8 = var_lines;
                    frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 172;
                    tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_8, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_empty_tuple, 0 ) );

                    if ( tmp_call_result_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 172;
                        type_description_1 = "oooooooo";
                        goto try_except_handler_3;
                    }
                    Py_DECREF( tmp_call_result_7 );
                }
                branch_no_4:;
            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_compexpr_left_5;
                PyObject *tmp_compexpr_right_5;
                PyObject *tmp_source_name_8;
                CHECK_OBJECT( var_cell );
                tmp_source_name_8 = var_cell;
                tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_cell_type );
                if ( tmp_compexpr_left_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 173;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_3;
                }
                tmp_compexpr_right_5 = const_str_plain_markdown;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                Py_DECREF( tmp_compexpr_left_5 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 173;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_3;
                }
                tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_assign_source_14;
                    PyObject *tmp_called_instance_9;
                    CHECK_OBJECT( var_cell );
                    tmp_called_instance_9 = var_cell;
                    frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 174;
                    tmp_assign_source_14 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_9, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_source_tuple, 0 ) );

                    if ( tmp_assign_source_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 174;
                        type_description_1 = "oooooooo";
                        goto try_except_handler_3;
                    }
                    {
                        PyObject *old = var_input;
                        var_input = tmp_assign_source_14;
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_6;
                    PyObject *tmp_compexpr_left_6;
                    PyObject *tmp_compexpr_right_6;
                    CHECK_OBJECT( var_input );
                    tmp_compexpr_left_6 = var_input;
                    tmp_compexpr_right_6 = Py_None;
                    tmp_condition_result_6 = ( tmp_compexpr_left_6 != tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_6;
                    }
                    else
                    {
                        goto branch_no_6;
                    }
                    branch_yes_6:;
                    {
                        PyObject *tmp_called_instance_10;
                        PyObject *tmp_call_result_8;
                        PyObject *tmp_call_arg_element_3;
                        CHECK_OBJECT( var_lines );
                        tmp_called_instance_10 = var_lines;
                        tmp_call_arg_element_3 = LIST_COPY( const_list_str_digest_b7a68a90094c6ec9dcd6f55b6e1f128e_str_empty_list );
                        frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 176;
                        {
                            PyObject *call_args[] = { tmp_call_arg_element_3 };
                            tmp_call_result_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_10, const_str_plain_extend, call_args );
                        }

                        Py_DECREF( tmp_call_arg_element_3 );
                        if ( tmp_call_result_8 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 176;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        Py_DECREF( tmp_call_result_8 );
                    }
                    {
                        PyObject *tmp_called_name_4;
                        PyObject *tmp_source_name_9;
                        PyObject *tmp_call_result_9;
                        PyObject *tmp_args_element_name_4;
                        CHECK_OBJECT( var_lines );
                        tmp_source_name_9 = var_lines;
                        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_extend );
                        if ( tmp_called_name_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 177;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        // Tried code:
                        {
                            PyObject *tmp_assign_source_15;
                            PyObject *tmp_iter_arg_4;
                            PyObject *tmp_called_instance_11;
                            CHECK_OBJECT( var_input );
                            tmp_called_instance_11 = var_input;
                            frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 177;
                            tmp_iter_arg_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_11, const_str_plain_splitlines );
                            if ( tmp_iter_arg_4 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 177;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_6;
                            }
                            tmp_assign_source_15 = MAKE_ITERATOR( tmp_iter_arg_4 );
                            Py_DECREF( tmp_iter_arg_4 );
                            if ( tmp_assign_source_15 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 177;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_6;
                            }
                            {
                                PyObject *old = tmp_listcomp_2__$0;
                                tmp_listcomp_2__$0 = tmp_assign_source_15;
                                Py_XDECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_assign_source_16;
                            tmp_assign_source_16 = PyList_New( 0 );
                            {
                                PyObject *old = tmp_listcomp_2__contraction;
                                tmp_listcomp_2__contraction = tmp_assign_source_16;
                                Py_XDECREF( old );
                            }

                        }
                        MAKE_OR_REUSE_FRAME( cache_frame_d2728b061cb2bcfa5fded6401f5ec7d3_3, codeobj_d2728b061cb2bcfa5fded6401f5ec7d3, module_nbformat$v3$nbpy, sizeof(void *) );
                        frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 = cache_frame_d2728b061cb2bcfa5fded6401f5ec7d3_3;

                        // Push the new frame as the currently active one.
                        pushFrameStack( frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 );

                        // Mark the frame object as in use, ref count 1 will be up for reuse.
                        assert( Py_REFCNT( frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 ) == 2 ); // Frame stack

                        // Framed code:
                        // Tried code:
                        loop_start_4:;
                        {
                            PyObject *tmp_next_source_4;
                            PyObject *tmp_assign_source_17;
                            CHECK_OBJECT( tmp_listcomp_2__$0 );
                            tmp_next_source_4 = tmp_listcomp_2__$0;
                            tmp_assign_source_17 = ITERATOR_NEXT( tmp_next_source_4 );
                            if ( tmp_assign_source_17 == NULL )
                            {
                                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                                {

                                    goto loop_end_4;
                                }
                                else
                                {

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                    type_description_2 = "o";
                                    exception_lineno = 177;
                                    goto try_except_handler_7;
                                }
                            }

                            {
                                PyObject *old = tmp_listcomp_2__iter_value_0;
                                tmp_listcomp_2__iter_value_0 = tmp_assign_source_17;
                                Py_XDECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_assign_source_18;
                            CHECK_OBJECT( tmp_listcomp_2__iter_value_0 );
                            tmp_assign_source_18 = tmp_listcomp_2__iter_value_0;
                            {
                                PyObject *old = outline_1_var_line;
                                outline_1_var_line = tmp_assign_source_18;
                                Py_INCREF( outline_1_var_line );
                                Py_XDECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_append_list_2;
                            PyObject *tmp_append_value_2;
                            PyObject *tmp_left_name_3;
                            PyObject *tmp_right_name_3;
                            CHECK_OBJECT( tmp_listcomp_2__contraction );
                            tmp_append_list_2 = tmp_listcomp_2__contraction;
                            tmp_left_name_3 = const_str_digest_6566768af533234b42110c37c6c377fd;
                            CHECK_OBJECT( outline_1_var_line );
                            tmp_right_name_3 = outline_1_var_line;
                            tmp_append_value_2 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_3, tmp_right_name_3 );
                            if ( tmp_append_value_2 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 177;
                                type_description_2 = "o";
                                goto try_except_handler_7;
                            }
                            assert( PyList_Check( tmp_append_list_2 ) );
                            tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
                            Py_DECREF( tmp_append_value_2 );
                            if ( tmp_res == -1 )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 177;
                                type_description_2 = "o";
                                goto try_except_handler_7;
                            }
                        }
                        if ( CONSIDER_THREADING() == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 177;
                            type_description_2 = "o";
                            goto try_except_handler_7;
                        }
                        goto loop_start_4;
                        loop_end_4:;
                        CHECK_OBJECT( tmp_listcomp_2__contraction );
                        tmp_args_element_name_4 = tmp_listcomp_2__contraction;
                        Py_INCREF( tmp_args_element_name_4 );
                        goto try_return_handler_7;
                        // tried codes exits in all cases
                        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                        return NULL;
                        // Return handler code:
                        try_return_handler_7:;
                        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
                        Py_DECREF( tmp_listcomp_2__$0 );
                        tmp_listcomp_2__$0 = NULL;

                        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
                        Py_DECREF( tmp_listcomp_2__contraction );
                        tmp_listcomp_2__contraction = NULL;

                        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
                        tmp_listcomp_2__iter_value_0 = NULL;

                        goto frame_return_exit_3;
                        // Exception handler code:
                        try_except_handler_7:;
                        exception_keeper_type_3 = exception_type;
                        exception_keeper_value_3 = exception_value;
                        exception_keeper_tb_3 = exception_tb;
                        exception_keeper_lineno_3 = exception_lineno;
                        exception_type = NULL;
                        exception_value = NULL;
                        exception_tb = NULL;
                        exception_lineno = 0;

                        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
                        Py_DECREF( tmp_listcomp_2__$0 );
                        tmp_listcomp_2__$0 = NULL;

                        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
                        Py_DECREF( tmp_listcomp_2__contraction );
                        tmp_listcomp_2__contraction = NULL;

                        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
                        tmp_listcomp_2__iter_value_0 = NULL;

                        // Re-raise.
                        exception_type = exception_keeper_type_3;
                        exception_value = exception_keeper_value_3;
                        exception_tb = exception_keeper_tb_3;
                        exception_lineno = exception_keeper_lineno_3;

                        goto frame_exception_exit_3;
                        // End of try:

#if 0
                        RESTORE_FRAME_EXCEPTION( frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 );
#endif

                        // Put the previous frame back on top.
                        popFrameStack();

                        goto frame_no_exception_2;

                        frame_return_exit_3:;
#if 0
                        RESTORE_FRAME_EXCEPTION( frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 );
#endif

                        // Put the previous frame back on top.
                        popFrameStack();

                        goto try_return_handler_6;

                        frame_exception_exit_3:;

#if 0
                        RESTORE_FRAME_EXCEPTION( frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 );
#endif

                        if ( exception_tb == NULL )
                        {
                            exception_tb = MAKE_TRACEBACK( frame_d2728b061cb2bcfa5fded6401f5ec7d3_3, exception_lineno );
                        }
                        else if ( exception_tb->tb_frame != &frame_d2728b061cb2bcfa5fded6401f5ec7d3_3->m_frame )
                        {
                            exception_tb = ADD_TRACEBACK( exception_tb, frame_d2728b061cb2bcfa5fded6401f5ec7d3_3, exception_lineno );
                        }

                        // Attachs locals to frame if any.
                        Nuitka_Frame_AttachLocals(
                            (struct Nuitka_FrameObject *)frame_d2728b061cb2bcfa5fded6401f5ec7d3_3,
                            type_description_2,
                            outline_1_var_line
                        );


                        // Release cached frame.
                        if ( frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 == cache_frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 )
                        {
                            Py_DECREF( frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 );
                        }
                        cache_frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 = NULL;

                        assertFrameObject( frame_d2728b061cb2bcfa5fded6401f5ec7d3_3 );

                        // Put the previous frame back on top.
                        popFrameStack();

                        // Return the error.
                        goto nested_frame_exit_2;

                        frame_no_exception_2:;
                        goto skip_nested_handling_2;
                        nested_frame_exit_2:;
                        type_description_1 = "oooooooo";
                        goto try_except_handler_6;
                        skip_nested_handling_2:;
                        // tried codes exits in all cases
                        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                        return NULL;
                        // Return handler code:
                        try_return_handler_6:;
                        Py_XDECREF( outline_1_var_line );
                        outline_1_var_line = NULL;

                        goto outline_result_2;
                        // Exception handler code:
                        try_except_handler_6:;
                        exception_keeper_type_4 = exception_type;
                        exception_keeper_value_4 = exception_value;
                        exception_keeper_tb_4 = exception_tb;
                        exception_keeper_lineno_4 = exception_lineno;
                        exception_type = NULL;
                        exception_value = NULL;
                        exception_tb = NULL;
                        exception_lineno = 0;

                        Py_XDECREF( outline_1_var_line );
                        outline_1_var_line = NULL;

                        // Re-raise.
                        exception_type = exception_keeper_type_4;
                        exception_value = exception_keeper_value_4;
                        exception_tb = exception_keeper_tb_4;
                        exception_lineno = exception_keeper_lineno_4;

                        goto outline_exception_2;
                        // End of try:
                        // Return statement must have exited already.
                        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                        return NULL;
                        outline_exception_2:;
                        exception_lineno = 177;
                        goto try_except_handler_3;
                        outline_result_2:;
                        frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 177;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_4 };
                            tmp_call_result_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                        }

                        Py_DECREF( tmp_called_name_4 );
                        Py_DECREF( tmp_args_element_name_4 );
                        if ( tmp_call_result_9 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 177;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        Py_DECREF( tmp_call_result_9 );
                    }
                    {
                        PyObject *tmp_called_instance_12;
                        PyObject *tmp_call_result_10;
                        CHECK_OBJECT( var_lines );
                        tmp_called_instance_12 = var_lines;
                        frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 178;
                        tmp_call_result_10 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_12, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_empty_tuple, 0 ) );

                        if ( tmp_call_result_10 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 178;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        Py_DECREF( tmp_call_result_10 );
                    }
                    branch_no_6:;
                }
                goto branch_end_5;
                branch_no_5:;
                {
                    nuitka_bool tmp_condition_result_7;
                    PyObject *tmp_compexpr_left_7;
                    PyObject *tmp_compexpr_right_7;
                    PyObject *tmp_source_name_10;
                    CHECK_OBJECT( var_cell );
                    tmp_source_name_10 = var_cell;
                    tmp_compexpr_left_7 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_cell_type );
                    if ( tmp_compexpr_left_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 179;
                        type_description_1 = "oooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_compexpr_right_7 = const_str_plain_raw;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                    Py_DECREF( tmp_compexpr_left_7 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 179;
                        type_description_1 = "oooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_7;
                    }
                    else
                    {
                        goto branch_no_7;
                    }
                    branch_yes_7:;
                    {
                        PyObject *tmp_assign_source_19;
                        PyObject *tmp_called_instance_13;
                        CHECK_OBJECT( var_cell );
                        tmp_called_instance_13 = var_cell;
                        frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 180;
                        tmp_assign_source_19 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_13, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_source_tuple, 0 ) );

                        if ( tmp_assign_source_19 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 180;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        {
                            PyObject *old = var_input;
                            var_input = tmp_assign_source_19;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        nuitka_bool tmp_condition_result_8;
                        PyObject *tmp_compexpr_left_8;
                        PyObject *tmp_compexpr_right_8;
                        CHECK_OBJECT( var_input );
                        tmp_compexpr_left_8 = var_input;
                        tmp_compexpr_right_8 = Py_None;
                        tmp_condition_result_8 = ( tmp_compexpr_left_8 != tmp_compexpr_right_8 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_8;
                        }
                        else
                        {
                            goto branch_no_8;
                        }
                        branch_yes_8:;
                        {
                            PyObject *tmp_called_instance_14;
                            PyObject *tmp_call_result_11;
                            PyObject *tmp_call_arg_element_4;
                            CHECK_OBJECT( var_lines );
                            tmp_called_instance_14 = var_lines;
                            tmp_call_arg_element_4 = LIST_COPY( const_list_str_digest_efdace01816915012691e5b49febfd69_str_empty_list );
                            frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 182;
                            {
                                PyObject *call_args[] = { tmp_call_arg_element_4 };
                                tmp_call_result_11 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_14, const_str_plain_extend, call_args );
                            }

                            Py_DECREF( tmp_call_arg_element_4 );
                            if ( tmp_call_result_11 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 182;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_3;
                            }
                            Py_DECREF( tmp_call_result_11 );
                        }
                        {
                            PyObject *tmp_called_name_5;
                            PyObject *tmp_source_name_11;
                            PyObject *tmp_call_result_12;
                            PyObject *tmp_args_element_name_5;
                            CHECK_OBJECT( var_lines );
                            tmp_source_name_11 = var_lines;
                            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_extend );
                            if ( tmp_called_name_5 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 183;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_3;
                            }
                            // Tried code:
                            {
                                PyObject *tmp_assign_source_20;
                                PyObject *tmp_iter_arg_5;
                                PyObject *tmp_called_instance_15;
                                CHECK_OBJECT( var_input );
                                tmp_called_instance_15 = var_input;
                                frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 183;
                                tmp_iter_arg_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_15, const_str_plain_splitlines );
                                if ( tmp_iter_arg_5 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 183;
                                    type_description_1 = "oooooooo";
                                    goto try_except_handler_8;
                                }
                                tmp_assign_source_20 = MAKE_ITERATOR( tmp_iter_arg_5 );
                                Py_DECREF( tmp_iter_arg_5 );
                                if ( tmp_assign_source_20 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 183;
                                    type_description_1 = "oooooooo";
                                    goto try_except_handler_8;
                                }
                                {
                                    PyObject *old = tmp_listcomp_3__$0;
                                    tmp_listcomp_3__$0 = tmp_assign_source_20;
                                    Py_XDECREF( old );
                                }

                            }
                            {
                                PyObject *tmp_assign_source_21;
                                tmp_assign_source_21 = PyList_New( 0 );
                                {
                                    PyObject *old = tmp_listcomp_3__contraction;
                                    tmp_listcomp_3__contraction = tmp_assign_source_21;
                                    Py_XDECREF( old );
                                }

                            }
                            MAKE_OR_REUSE_FRAME( cache_frame_c2a8dffdae9b7d568e4654a49f86edf8_4, codeobj_c2a8dffdae9b7d568e4654a49f86edf8, module_nbformat$v3$nbpy, sizeof(void *) );
                            frame_c2a8dffdae9b7d568e4654a49f86edf8_4 = cache_frame_c2a8dffdae9b7d568e4654a49f86edf8_4;

                            // Push the new frame as the currently active one.
                            pushFrameStack( frame_c2a8dffdae9b7d568e4654a49f86edf8_4 );

                            // Mark the frame object as in use, ref count 1 will be up for reuse.
                            assert( Py_REFCNT( frame_c2a8dffdae9b7d568e4654a49f86edf8_4 ) == 2 ); // Frame stack

                            // Framed code:
                            // Tried code:
                            loop_start_5:;
                            {
                                PyObject *tmp_next_source_5;
                                PyObject *tmp_assign_source_22;
                                CHECK_OBJECT( tmp_listcomp_3__$0 );
                                tmp_next_source_5 = tmp_listcomp_3__$0;
                                tmp_assign_source_22 = ITERATOR_NEXT( tmp_next_source_5 );
                                if ( tmp_assign_source_22 == NULL )
                                {
                                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                                    {

                                        goto loop_end_5;
                                    }
                                    else
                                    {

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                        type_description_2 = "o";
                                        exception_lineno = 183;
                                        goto try_except_handler_9;
                                    }
                                }

                                {
                                    PyObject *old = tmp_listcomp_3__iter_value_0;
                                    tmp_listcomp_3__iter_value_0 = tmp_assign_source_22;
                                    Py_XDECREF( old );
                                }

                            }
                            {
                                PyObject *tmp_assign_source_23;
                                CHECK_OBJECT( tmp_listcomp_3__iter_value_0 );
                                tmp_assign_source_23 = tmp_listcomp_3__iter_value_0;
                                {
                                    PyObject *old = outline_2_var_line;
                                    outline_2_var_line = tmp_assign_source_23;
                                    Py_INCREF( outline_2_var_line );
                                    Py_XDECREF( old );
                                }

                            }
                            {
                                PyObject *tmp_append_list_3;
                                PyObject *tmp_append_value_3;
                                PyObject *tmp_left_name_4;
                                PyObject *tmp_right_name_4;
                                CHECK_OBJECT( tmp_listcomp_3__contraction );
                                tmp_append_list_3 = tmp_listcomp_3__contraction;
                                tmp_left_name_4 = const_str_digest_6566768af533234b42110c37c6c377fd;
                                CHECK_OBJECT( outline_2_var_line );
                                tmp_right_name_4 = outline_2_var_line;
                                tmp_append_value_3 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_4, tmp_right_name_4 );
                                if ( tmp_append_value_3 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 183;
                                    type_description_2 = "o";
                                    goto try_except_handler_9;
                                }
                                assert( PyList_Check( tmp_append_list_3 ) );
                                tmp_res = PyList_Append( tmp_append_list_3, tmp_append_value_3 );
                                Py_DECREF( tmp_append_value_3 );
                                if ( tmp_res == -1 )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 183;
                                    type_description_2 = "o";
                                    goto try_except_handler_9;
                                }
                            }
                            if ( CONSIDER_THREADING() == false )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 183;
                                type_description_2 = "o";
                                goto try_except_handler_9;
                            }
                            goto loop_start_5;
                            loop_end_5:;
                            CHECK_OBJECT( tmp_listcomp_3__contraction );
                            tmp_args_element_name_5 = tmp_listcomp_3__contraction;
                            Py_INCREF( tmp_args_element_name_5 );
                            goto try_return_handler_9;
                            // tried codes exits in all cases
                            NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                            return NULL;
                            // Return handler code:
                            try_return_handler_9:;
                            CHECK_OBJECT( (PyObject *)tmp_listcomp_3__$0 );
                            Py_DECREF( tmp_listcomp_3__$0 );
                            tmp_listcomp_3__$0 = NULL;

                            CHECK_OBJECT( (PyObject *)tmp_listcomp_3__contraction );
                            Py_DECREF( tmp_listcomp_3__contraction );
                            tmp_listcomp_3__contraction = NULL;

                            Py_XDECREF( tmp_listcomp_3__iter_value_0 );
                            tmp_listcomp_3__iter_value_0 = NULL;

                            goto frame_return_exit_4;
                            // Exception handler code:
                            try_except_handler_9:;
                            exception_keeper_type_5 = exception_type;
                            exception_keeper_value_5 = exception_value;
                            exception_keeper_tb_5 = exception_tb;
                            exception_keeper_lineno_5 = exception_lineno;
                            exception_type = NULL;
                            exception_value = NULL;
                            exception_tb = NULL;
                            exception_lineno = 0;

                            CHECK_OBJECT( (PyObject *)tmp_listcomp_3__$0 );
                            Py_DECREF( tmp_listcomp_3__$0 );
                            tmp_listcomp_3__$0 = NULL;

                            CHECK_OBJECT( (PyObject *)tmp_listcomp_3__contraction );
                            Py_DECREF( tmp_listcomp_3__contraction );
                            tmp_listcomp_3__contraction = NULL;

                            Py_XDECREF( tmp_listcomp_3__iter_value_0 );
                            tmp_listcomp_3__iter_value_0 = NULL;

                            // Re-raise.
                            exception_type = exception_keeper_type_5;
                            exception_value = exception_keeper_value_5;
                            exception_tb = exception_keeper_tb_5;
                            exception_lineno = exception_keeper_lineno_5;

                            goto frame_exception_exit_4;
                            // End of try:

#if 0
                            RESTORE_FRAME_EXCEPTION( frame_c2a8dffdae9b7d568e4654a49f86edf8_4 );
#endif

                            // Put the previous frame back on top.
                            popFrameStack();

                            goto frame_no_exception_3;

                            frame_return_exit_4:;
#if 0
                            RESTORE_FRAME_EXCEPTION( frame_c2a8dffdae9b7d568e4654a49f86edf8_4 );
#endif

                            // Put the previous frame back on top.
                            popFrameStack();

                            goto try_return_handler_8;

                            frame_exception_exit_4:;

#if 0
                            RESTORE_FRAME_EXCEPTION( frame_c2a8dffdae9b7d568e4654a49f86edf8_4 );
#endif

                            if ( exception_tb == NULL )
                            {
                                exception_tb = MAKE_TRACEBACK( frame_c2a8dffdae9b7d568e4654a49f86edf8_4, exception_lineno );
                            }
                            else if ( exception_tb->tb_frame != &frame_c2a8dffdae9b7d568e4654a49f86edf8_4->m_frame )
                            {
                                exception_tb = ADD_TRACEBACK( exception_tb, frame_c2a8dffdae9b7d568e4654a49f86edf8_4, exception_lineno );
                            }

                            // Attachs locals to frame if any.
                            Nuitka_Frame_AttachLocals(
                                (struct Nuitka_FrameObject *)frame_c2a8dffdae9b7d568e4654a49f86edf8_4,
                                type_description_2,
                                outline_2_var_line
                            );


                            // Release cached frame.
                            if ( frame_c2a8dffdae9b7d568e4654a49f86edf8_4 == cache_frame_c2a8dffdae9b7d568e4654a49f86edf8_4 )
                            {
                                Py_DECREF( frame_c2a8dffdae9b7d568e4654a49f86edf8_4 );
                            }
                            cache_frame_c2a8dffdae9b7d568e4654a49f86edf8_4 = NULL;

                            assertFrameObject( frame_c2a8dffdae9b7d568e4654a49f86edf8_4 );

                            // Put the previous frame back on top.
                            popFrameStack();

                            // Return the error.
                            goto nested_frame_exit_3;

                            frame_no_exception_3:;
                            goto skip_nested_handling_3;
                            nested_frame_exit_3:;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_8;
                            skip_nested_handling_3:;
                            // tried codes exits in all cases
                            NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                            return NULL;
                            // Return handler code:
                            try_return_handler_8:;
                            Py_XDECREF( outline_2_var_line );
                            outline_2_var_line = NULL;

                            goto outline_result_3;
                            // Exception handler code:
                            try_except_handler_8:;
                            exception_keeper_type_6 = exception_type;
                            exception_keeper_value_6 = exception_value;
                            exception_keeper_tb_6 = exception_tb;
                            exception_keeper_lineno_6 = exception_lineno;
                            exception_type = NULL;
                            exception_value = NULL;
                            exception_tb = NULL;
                            exception_lineno = 0;

                            Py_XDECREF( outline_2_var_line );
                            outline_2_var_line = NULL;

                            // Re-raise.
                            exception_type = exception_keeper_type_6;
                            exception_value = exception_keeper_value_6;
                            exception_tb = exception_keeper_tb_6;
                            exception_lineno = exception_keeper_lineno_6;

                            goto outline_exception_3;
                            // End of try:
                            // Return statement must have exited already.
                            NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                            return NULL;
                            outline_exception_3:;
                            exception_lineno = 183;
                            goto try_except_handler_3;
                            outline_result_3:;
                            frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 183;
                            {
                                PyObject *call_args[] = { tmp_args_element_name_5 };
                                tmp_call_result_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
                            }

                            Py_DECREF( tmp_called_name_5 );
                            Py_DECREF( tmp_args_element_name_5 );
                            if ( tmp_call_result_12 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 183;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_3;
                            }
                            Py_DECREF( tmp_call_result_12 );
                        }
                        {
                            PyObject *tmp_called_instance_16;
                            PyObject *tmp_call_result_13;
                            CHECK_OBJECT( var_lines );
                            tmp_called_instance_16 = var_lines;
                            frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 184;
                            tmp_call_result_13 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_16, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_empty_tuple, 0 ) );

                            if ( tmp_call_result_13 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 184;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_3;
                            }
                            Py_DECREF( tmp_call_result_13 );
                        }
                        branch_no_8:;
                    }
                    goto branch_end_7;
                    branch_no_7:;
                    {
                        nuitka_bool tmp_condition_result_9;
                        PyObject *tmp_compexpr_left_9;
                        PyObject *tmp_compexpr_right_9;
                        PyObject *tmp_source_name_12;
                        CHECK_OBJECT( var_cell );
                        tmp_source_name_12 = var_cell;
                        tmp_compexpr_left_9 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_cell_type );
                        if ( tmp_compexpr_left_9 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 185;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        tmp_compexpr_right_9 = const_str_plain_heading;
                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                        Py_DECREF( tmp_compexpr_left_9 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 185;
                            type_description_1 = "oooooooo";
                            goto try_except_handler_3;
                        }
                        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_9;
                        }
                        else
                        {
                            goto branch_no_9;
                        }
                        branch_yes_9:;
                        {
                            PyObject *tmp_assign_source_24;
                            PyObject *tmp_called_instance_17;
                            CHECK_OBJECT( var_cell );
                            tmp_called_instance_17 = var_cell;
                            frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 186;
                            tmp_assign_source_24 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_17, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_source_tuple, 0 ) );

                            if ( tmp_assign_source_24 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 186;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_3;
                            }
                            {
                                PyObject *old = var_input;
                                var_input = tmp_assign_source_24;
                                Py_XDECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_assign_source_25;
                            PyObject *tmp_called_instance_18;
                            CHECK_OBJECT( var_cell );
                            tmp_called_instance_18 = var_cell;
                            frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 187;
                            tmp_assign_source_25 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_18, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_level_int_pos_1_tuple, 0 ) );

                            if ( tmp_assign_source_25 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 187;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_3;
                            }
                            {
                                PyObject *old = var_level;
                                var_level = tmp_assign_source_25;
                                Py_XDECREF( old );
                            }

                        }
                        {
                            nuitka_bool tmp_condition_result_10;
                            PyObject *tmp_compexpr_left_10;
                            PyObject *tmp_compexpr_right_10;
                            CHECK_OBJECT( var_input );
                            tmp_compexpr_left_10 = var_input;
                            tmp_compexpr_right_10 = Py_None;
                            tmp_condition_result_10 = ( tmp_compexpr_left_10 != tmp_compexpr_right_10 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                            if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                            {
                                goto branch_yes_10;
                            }
                            else
                            {
                                goto branch_no_10;
                            }
                            branch_yes_10:;
                            {
                                PyObject *tmp_called_name_6;
                                PyObject *tmp_source_name_13;
                                PyObject *tmp_call_result_14;
                                PyObject *tmp_args_element_name_6;
                                PyObject *tmp_list_element_2;
                                PyObject *tmp_left_name_5;
                                PyObject *tmp_right_name_5;
                                CHECK_OBJECT( var_lines );
                                tmp_source_name_13 = var_lines;
                                tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_extend );
                                if ( tmp_called_name_6 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 189;
                                    type_description_1 = "oooooooo";
                                    goto try_except_handler_3;
                                }
                                tmp_left_name_5 = const_str_digest_fc7467a3720075c2bc06bb415a7982bb;
                                CHECK_OBJECT( var_level );
                                tmp_right_name_5 = var_level;
                                tmp_list_element_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                                if ( tmp_list_element_2 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                    Py_DECREF( tmp_called_name_6 );

                                    exception_lineno = 189;
                                    type_description_1 = "oooooooo";
                                    goto try_except_handler_3;
                                }
                                tmp_args_element_name_6 = PyList_New( 2 );
                                PyList_SET_ITEM( tmp_args_element_name_6, 0, tmp_list_element_2 );
                                tmp_list_element_2 = const_str_empty;
                                Py_INCREF( tmp_list_element_2 );
                                PyList_SET_ITEM( tmp_args_element_name_6, 1, tmp_list_element_2 );
                                frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 189;
                                {
                                    PyObject *call_args[] = { tmp_args_element_name_6 };
                                    tmp_call_result_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
                                }

                                Py_DECREF( tmp_called_name_6 );
                                Py_DECREF( tmp_args_element_name_6 );
                                if ( tmp_call_result_14 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 189;
                                    type_description_1 = "oooooooo";
                                    goto try_except_handler_3;
                                }
                                Py_DECREF( tmp_call_result_14 );
                            }
                            {
                                PyObject *tmp_called_name_7;
                                PyObject *tmp_source_name_14;
                                PyObject *tmp_call_result_15;
                                PyObject *tmp_args_element_name_7;
                                CHECK_OBJECT( var_lines );
                                tmp_source_name_14 = var_lines;
                                tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_extend );
                                if ( tmp_called_name_7 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 190;
                                    type_description_1 = "oooooooo";
                                    goto try_except_handler_3;
                                }
                                // Tried code:
                                {
                                    PyObject *tmp_assign_source_26;
                                    PyObject *tmp_iter_arg_6;
                                    PyObject *tmp_called_instance_19;
                                    CHECK_OBJECT( var_input );
                                    tmp_called_instance_19 = var_input;
                                    frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 190;
                                    tmp_iter_arg_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_19, const_str_plain_splitlines );
                                    if ( tmp_iter_arg_6 == NULL )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 190;
                                        type_description_1 = "oooooooo";
                                        goto try_except_handler_10;
                                    }
                                    tmp_assign_source_26 = MAKE_ITERATOR( tmp_iter_arg_6 );
                                    Py_DECREF( tmp_iter_arg_6 );
                                    if ( tmp_assign_source_26 == NULL )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 190;
                                        type_description_1 = "oooooooo";
                                        goto try_except_handler_10;
                                    }
                                    {
                                        PyObject *old = tmp_listcomp_4__$0;
                                        tmp_listcomp_4__$0 = tmp_assign_source_26;
                                        Py_XDECREF( old );
                                    }

                                }
                                {
                                    PyObject *tmp_assign_source_27;
                                    tmp_assign_source_27 = PyList_New( 0 );
                                    {
                                        PyObject *old = tmp_listcomp_4__contraction;
                                        tmp_listcomp_4__contraction = tmp_assign_source_27;
                                        Py_XDECREF( old );
                                    }

                                }
                                MAKE_OR_REUSE_FRAME( cache_frame_e2b6c5150ad64bf469296b400d92c9ff_5, codeobj_e2b6c5150ad64bf469296b400d92c9ff, module_nbformat$v3$nbpy, sizeof(void *) );
                                frame_e2b6c5150ad64bf469296b400d92c9ff_5 = cache_frame_e2b6c5150ad64bf469296b400d92c9ff_5;

                                // Push the new frame as the currently active one.
                                pushFrameStack( frame_e2b6c5150ad64bf469296b400d92c9ff_5 );

                                // Mark the frame object as in use, ref count 1 will be up for reuse.
                                assert( Py_REFCNT( frame_e2b6c5150ad64bf469296b400d92c9ff_5 ) == 2 ); // Frame stack

                                // Framed code:
                                // Tried code:
                                loop_start_6:;
                                {
                                    PyObject *tmp_next_source_6;
                                    PyObject *tmp_assign_source_28;
                                    CHECK_OBJECT( tmp_listcomp_4__$0 );
                                    tmp_next_source_6 = tmp_listcomp_4__$0;
                                    tmp_assign_source_28 = ITERATOR_NEXT( tmp_next_source_6 );
                                    if ( tmp_assign_source_28 == NULL )
                                    {
                                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                                        {

                                            goto loop_end_6;
                                        }
                                        else
                                        {

                                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                            type_description_2 = "o";
                                            exception_lineno = 190;
                                            goto try_except_handler_11;
                                        }
                                    }

                                    {
                                        PyObject *old = tmp_listcomp_4__iter_value_0;
                                        tmp_listcomp_4__iter_value_0 = tmp_assign_source_28;
                                        Py_XDECREF( old );
                                    }

                                }
                                {
                                    PyObject *tmp_assign_source_29;
                                    CHECK_OBJECT( tmp_listcomp_4__iter_value_0 );
                                    tmp_assign_source_29 = tmp_listcomp_4__iter_value_0;
                                    {
                                        PyObject *old = outline_3_var_line;
                                        outline_3_var_line = tmp_assign_source_29;
                                        Py_INCREF( outline_3_var_line );
                                        Py_XDECREF( old );
                                    }

                                }
                                {
                                    PyObject *tmp_append_list_4;
                                    PyObject *tmp_append_value_4;
                                    PyObject *tmp_left_name_6;
                                    PyObject *tmp_right_name_6;
                                    CHECK_OBJECT( tmp_listcomp_4__contraction );
                                    tmp_append_list_4 = tmp_listcomp_4__contraction;
                                    tmp_left_name_6 = const_str_digest_6566768af533234b42110c37c6c377fd;
                                    CHECK_OBJECT( outline_3_var_line );
                                    tmp_right_name_6 = outline_3_var_line;
                                    tmp_append_value_4 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_6, tmp_right_name_6 );
                                    if ( tmp_append_value_4 == NULL )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 190;
                                        type_description_2 = "o";
                                        goto try_except_handler_11;
                                    }
                                    assert( PyList_Check( tmp_append_list_4 ) );
                                    tmp_res = PyList_Append( tmp_append_list_4, tmp_append_value_4 );
                                    Py_DECREF( tmp_append_value_4 );
                                    if ( tmp_res == -1 )
                                    {
                                        assert( ERROR_OCCURRED() );

                                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                        exception_lineno = 190;
                                        type_description_2 = "o";
                                        goto try_except_handler_11;
                                    }
                                }
                                if ( CONSIDER_THREADING() == false )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 190;
                                    type_description_2 = "o";
                                    goto try_except_handler_11;
                                }
                                goto loop_start_6;
                                loop_end_6:;
                                CHECK_OBJECT( tmp_listcomp_4__contraction );
                                tmp_args_element_name_7 = tmp_listcomp_4__contraction;
                                Py_INCREF( tmp_args_element_name_7 );
                                goto try_return_handler_11;
                                // tried codes exits in all cases
                                NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                                return NULL;
                                // Return handler code:
                                try_return_handler_11:;
                                CHECK_OBJECT( (PyObject *)tmp_listcomp_4__$0 );
                                Py_DECREF( tmp_listcomp_4__$0 );
                                tmp_listcomp_4__$0 = NULL;

                                CHECK_OBJECT( (PyObject *)tmp_listcomp_4__contraction );
                                Py_DECREF( tmp_listcomp_4__contraction );
                                tmp_listcomp_4__contraction = NULL;

                                Py_XDECREF( tmp_listcomp_4__iter_value_0 );
                                tmp_listcomp_4__iter_value_0 = NULL;

                                goto frame_return_exit_5;
                                // Exception handler code:
                                try_except_handler_11:;
                                exception_keeper_type_7 = exception_type;
                                exception_keeper_value_7 = exception_value;
                                exception_keeper_tb_7 = exception_tb;
                                exception_keeper_lineno_7 = exception_lineno;
                                exception_type = NULL;
                                exception_value = NULL;
                                exception_tb = NULL;
                                exception_lineno = 0;

                                CHECK_OBJECT( (PyObject *)tmp_listcomp_4__$0 );
                                Py_DECREF( tmp_listcomp_4__$0 );
                                tmp_listcomp_4__$0 = NULL;

                                CHECK_OBJECT( (PyObject *)tmp_listcomp_4__contraction );
                                Py_DECREF( tmp_listcomp_4__contraction );
                                tmp_listcomp_4__contraction = NULL;

                                Py_XDECREF( tmp_listcomp_4__iter_value_0 );
                                tmp_listcomp_4__iter_value_0 = NULL;

                                // Re-raise.
                                exception_type = exception_keeper_type_7;
                                exception_value = exception_keeper_value_7;
                                exception_tb = exception_keeper_tb_7;
                                exception_lineno = exception_keeper_lineno_7;

                                goto frame_exception_exit_5;
                                // End of try:

#if 0
                                RESTORE_FRAME_EXCEPTION( frame_e2b6c5150ad64bf469296b400d92c9ff_5 );
#endif

                                // Put the previous frame back on top.
                                popFrameStack();

                                goto frame_no_exception_4;

                                frame_return_exit_5:;
#if 0
                                RESTORE_FRAME_EXCEPTION( frame_e2b6c5150ad64bf469296b400d92c9ff_5 );
#endif

                                // Put the previous frame back on top.
                                popFrameStack();

                                goto try_return_handler_10;

                                frame_exception_exit_5:;

#if 0
                                RESTORE_FRAME_EXCEPTION( frame_e2b6c5150ad64bf469296b400d92c9ff_5 );
#endif

                                if ( exception_tb == NULL )
                                {
                                    exception_tb = MAKE_TRACEBACK( frame_e2b6c5150ad64bf469296b400d92c9ff_5, exception_lineno );
                                }
                                else if ( exception_tb->tb_frame != &frame_e2b6c5150ad64bf469296b400d92c9ff_5->m_frame )
                                {
                                    exception_tb = ADD_TRACEBACK( exception_tb, frame_e2b6c5150ad64bf469296b400d92c9ff_5, exception_lineno );
                                }

                                // Attachs locals to frame if any.
                                Nuitka_Frame_AttachLocals(
                                    (struct Nuitka_FrameObject *)frame_e2b6c5150ad64bf469296b400d92c9ff_5,
                                    type_description_2,
                                    outline_3_var_line
                                );


                                // Release cached frame.
                                if ( frame_e2b6c5150ad64bf469296b400d92c9ff_5 == cache_frame_e2b6c5150ad64bf469296b400d92c9ff_5 )
                                {
                                    Py_DECREF( frame_e2b6c5150ad64bf469296b400d92c9ff_5 );
                                }
                                cache_frame_e2b6c5150ad64bf469296b400d92c9ff_5 = NULL;

                                assertFrameObject( frame_e2b6c5150ad64bf469296b400d92c9ff_5 );

                                // Put the previous frame back on top.
                                popFrameStack();

                                // Return the error.
                                goto nested_frame_exit_4;

                                frame_no_exception_4:;
                                goto skip_nested_handling_4;
                                nested_frame_exit_4:;
                                type_description_1 = "oooooooo";
                                goto try_except_handler_10;
                                skip_nested_handling_4:;
                                // tried codes exits in all cases
                                NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                                return NULL;
                                // Return handler code:
                                try_return_handler_10:;
                                Py_XDECREF( outline_3_var_line );
                                outline_3_var_line = NULL;

                                goto outline_result_4;
                                // Exception handler code:
                                try_except_handler_10:;
                                exception_keeper_type_8 = exception_type;
                                exception_keeper_value_8 = exception_value;
                                exception_keeper_tb_8 = exception_tb;
                                exception_keeper_lineno_8 = exception_lineno;
                                exception_type = NULL;
                                exception_value = NULL;
                                exception_tb = NULL;
                                exception_lineno = 0;

                                Py_XDECREF( outline_3_var_line );
                                outline_3_var_line = NULL;

                                // Re-raise.
                                exception_type = exception_keeper_type_8;
                                exception_value = exception_keeper_value_8;
                                exception_tb = exception_keeper_tb_8;
                                exception_lineno = exception_keeper_lineno_8;

                                goto outline_exception_4;
                                // End of try:
                                // Return statement must have exited already.
                                NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
                                return NULL;
                                outline_exception_4:;
                                exception_lineno = 190;
                                goto try_except_handler_3;
                                outline_result_4:;
                                frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 190;
                                {
                                    PyObject *call_args[] = { tmp_args_element_name_7 };
                                    tmp_call_result_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
                                }

                                Py_DECREF( tmp_called_name_7 );
                                Py_DECREF( tmp_args_element_name_7 );
                                if ( tmp_call_result_15 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 190;
                                    type_description_1 = "oooooooo";
                                    goto try_except_handler_3;
                                }
                                Py_DECREF( tmp_call_result_15 );
                            }
                            {
                                PyObject *tmp_called_instance_20;
                                PyObject *tmp_call_result_16;
                                CHECK_OBJECT( var_lines );
                                tmp_called_instance_20 = var_lines;
                                frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 191;
                                tmp_call_result_16 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_20, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_empty_tuple, 0 ) );

                                if ( tmp_call_result_16 == NULL )
                                {
                                    assert( ERROR_OCCURRED() );

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                    exception_lineno = 191;
                                    type_description_1 = "oooooooo";
                                    goto try_except_handler_3;
                                }
                                Py_DECREF( tmp_call_result_16 );
                            }
                            branch_no_10:;
                        }
                        branch_no_9:;
                    }
                    branch_end_7:;
                }
                branch_end_5:;
            }
            branch_end_3:;
        }
        branch_end_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 160;
        type_description_1 = "oooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 159;
        type_description_1 = "oooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_instance_21;
        PyObject *tmp_call_result_17;
        CHECK_OBJECT( var_lines );
        tmp_called_instance_21 = var_lines;
        frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 192;
        tmp_call_result_17 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_21, const_str_plain_append, &PyTuple_GET_ITEM( const_tuple_str_empty_tuple, 0 ) );

        if ( tmp_call_result_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_17 );
    }
    {
        PyObject *tmp_called_instance_22;
        PyObject *tmp_args_element_name_8;
        tmp_called_instance_22 = const_str_newline;
        CHECK_OBJECT( var_lines );
        tmp_args_element_name_8 = var_lines;
        frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame.f_lineno = 193;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_22, const_str_plain_join, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_32003efde0d7f7755fbe30fab89f2b4f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_5;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_32003efde0d7f7755fbe30fab89f2b4f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_32003efde0d7f7755fbe30fab89f2b4f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_32003efde0d7f7755fbe30fab89f2b4f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_32003efde0d7f7755fbe30fab89f2b4f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_32003efde0d7f7755fbe30fab89f2b4f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_32003efde0d7f7755fbe30fab89f2b4f,
        type_description_1,
        par_self,
        par_nb,
        par_kwargs,
        var_lines,
        var_ws,
        var_cell,
        var_input,
        var_level
    );


    // Release cached frame.
    if ( frame_32003efde0d7f7755fbe30fab89f2b4f == cache_frame_32003efde0d7f7755fbe30fab89f2b4f )
    {
        Py_DECREF( frame_32003efde0d7f7755fbe30fab89f2b4f );
    }
    cache_frame_32003efde0d7f7755fbe30fab89f2b4f = NULL;

    assertFrameObject( frame_32003efde0d7f7755fbe30fab89f2b4f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_5:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_nb );
    Py_DECREF( par_nb );
    par_nb = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_lines );
    Py_DECREF( var_lines );
    var_lines = NULL;

    Py_XDECREF( var_ws );
    var_ws = NULL;

    Py_XDECREF( var_cell );
    var_cell = NULL;

    Py_XDECREF( var_input );
    var_input = NULL;

    Py_XDECREF( var_level );
    var_level = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_nb );
    Py_DECREF( par_nb );
    par_nb = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_lines );
    Py_DECREF( var_lines );
    var_lines = NULL;

    Py_XDECREF( var_ws );
    var_ws = NULL;

    Py_XDECREF( var_cell );
    var_cell = NULL;

    Py_XDECREF( var_input );
    var_input = NULL;

    Py_XDECREF( var_level );
    var_level = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy$$$function_6_writes );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_1_reads(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v3$nbpy$$$function_1_reads,
        const_str_plain_reads,
#if PYTHON_VERSION >= 300
        const_str_digest_6b42a3b9404b51c5be86e5b0a3c164a9,
#endif
        codeobj_e294c983e3fbdad42247274db5767661,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v3$nbpy,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_2_to_notebook(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v3$nbpy$$$function_2_to_notebook,
        const_str_plain_to_notebook,
#if PYTHON_VERSION >= 300
        const_str_digest_0c4e933b6de95532d7ad0f14039a6e76,
#endif
        codeobj_8a351f86f2e046cbffb082b3b91827f5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v3$nbpy,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_3_new_cell(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v3$nbpy$$$function_3_new_cell,
        const_str_plain_new_cell,
#if PYTHON_VERSION >= 300
        const_str_digest_057da886ac33ee92c36c3be89967b4ee,
#endif
        codeobj_3c7155fddb695906582167c0e7e21eb8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v3$nbpy,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_4__remove_comments(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v3$nbpy$$$function_4__remove_comments,
        const_str_plain__remove_comments,
#if PYTHON_VERSION >= 300
        const_str_digest_eab93e76d9a19da3152998dbdc1780a9,
#endif
        codeobj_caf610dc5992c62a0682b1140ac23345,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v3$nbpy,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_5_split_lines_into_blocks(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v3$nbpy$$$function_5_split_lines_into_blocks,
        const_str_plain_split_lines_into_blocks,
#if PYTHON_VERSION >= 300
        const_str_digest_43d123edad7181f56494a8200b84a35c,
#endif
        codeobj_9c9c9b47c83ccf8873a3073eb6ba3519,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v3$nbpy,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$v3$nbpy$$$function_6_writes(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$v3$nbpy$$$function_6_writes,
        const_str_plain_writes,
#if PYTHON_VERSION >= 300
        const_str_digest_f86266a3d8a5127d8277b95971ab8d55,
#endif
        codeobj_32003efde0d7f7755fbe30fab89f2b4f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$v3$nbpy,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_nbformat$v3$nbpy =
{
    PyModuleDef_HEAD_INIT,
    "nbformat.v3.nbpy",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(nbformat$v3$nbpy)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(nbformat$v3$nbpy)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_nbformat$v3$nbpy );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("nbformat.v3.nbpy: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbformat.v3.nbpy: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbformat.v3.nbpy: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initnbformat$v3$nbpy" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_nbformat$v3$nbpy = Py_InitModule4(
        "nbformat.v3.nbpy",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_nbformat$v3$nbpy = PyModule_Create( &mdef_nbformat$v3$nbpy );
#endif

    moduledict_nbformat$v3$nbpy = MODULE_DICT( module_nbformat$v3$nbpy );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_nbformat$v3$nbpy,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_nbformat$v3$nbpy,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbformat$v3$nbpy,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbformat$v3$nbpy,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_nbformat$v3$nbpy );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_a1ccde01d3ada8afb4b652f98ef1467b, module_nbformat$v3$nbpy );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_172af58c58bc83062beaf4406bd4dbd5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_nbformat$v3$nbpy_32 = NULL;
    PyObject *tmp_dictset_value;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *locals_nbformat$v3$nbpy_36 = NULL;
    struct Nuitka_FrameObject *frame_731c89bd0fef47837207780129665e0e_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_731c89bd0fef47837207780129665e0e_2 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *locals_nbformat$v3$nbpy_151 = NULL;
    struct Nuitka_FrameObject *frame_571b126b46a578699a7b55a1776dfdb7_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_571b126b46a578699a7b55a1776dfdb7_3 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_ff85db009e3011b9c6a1f745923678a5;
        UPDATE_STRING_DICT0( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_172af58c58bc83062beaf4406bd4dbd5 = MAKE_MODULE_FRAME( codeobj_172af58c58bc83062beaf4406bd4dbd5, module_nbformat$v3$nbpy );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_172af58c58bc83062beaf4406bd4dbd5 );
    assert( Py_REFCNT( frame_172af58c58bc83062beaf4406bd4dbd5 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_re;
        tmp_globals_name_1 = (PyObject *)moduledict_nbformat$v3$nbpy;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 19;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_rwbase;
        tmp_globals_name_2 = (PyObject *)moduledict_nbformat$v3$nbpy;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_NotebookReader_str_plain_NotebookWriter_tuple;
        tmp_level_name_2 = const_int_pos_1;
        frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 20;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_6 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_nbformat$v3$nbpy,
                const_str_plain_NotebookReader,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_NotebookReader );
        }

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_NotebookReader, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_2 ) )
        {
           tmp_assign_source_7 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_2,
                (PyObject *)moduledict_nbformat$v3$nbpy,
                const_str_plain_NotebookWriter,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_NotebookWriter );
        }

        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_NotebookWriter, tmp_assign_source_7 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_nbbase;
        tmp_globals_name_3 = (PyObject *)moduledict_nbformat$v3$nbpy;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_f25cbd52acbf24776e032c042f114b49_tuple;
        tmp_level_name_3 = const_int_pos_1;
        frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 21;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_8;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_3 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_3 ) )
        {
           tmp_assign_source_9 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_3,
                (PyObject *)moduledict_nbformat$v3$nbpy,
                const_str_plain_new_code_cell,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_new_code_cell );
        }

        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_code_cell, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_4 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_4 ) )
        {
           tmp_assign_source_10 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_4,
                (PyObject *)moduledict_nbformat$v3$nbpy,
                const_str_plain_new_text_cell,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_new_text_cell );
        }

        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_text_cell, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_5 ) )
        {
           tmp_assign_source_11 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_5,
                (PyObject *)moduledict_nbformat$v3$nbpy,
                const_str_plain_new_worksheet,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_new_worksheet );
        }

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_worksheet, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_6 ) )
        {
           tmp_assign_source_12 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_6,
                (PyObject *)moduledict_nbformat$v3$nbpy,
                const_str_plain_new_notebook,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_new_notebook );
        }

        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_notebook, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_7 ) )
        {
           tmp_assign_source_13 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_7,
                (PyObject *)moduledict_nbformat$v3$nbpy,
                const_str_plain_new_heading_cell,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_new_heading_cell );
        }

        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_new_heading_cell, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_8 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_8 ) )
        {
           tmp_assign_source_14 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_8,
                (PyObject *)moduledict_nbformat$v3$nbpy,
                const_str_plain_nbformat,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_nbformat );
        }

        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_nbformat, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_9 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_9 ) )
        {
           tmp_assign_source_15 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_9,
                (PyObject *)moduledict_nbformat$v3$nbpy,
                const_str_plain_nbformat_minor,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_nbformat_minor );
        }

        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 21;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_nbformat_minor, tmp_assign_source_15 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 30;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 30;
        tmp_assign_source_16 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_compile, &PyTuple_GET_ITEM( const_tuple_str_digest_215283e090e28401efd32b2cf0bf29ae_tuple, 0 ) );

        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain__encoding_declaration_re, tmp_assign_source_16 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_Exception_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_17 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_17;
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_18;
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_3;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_3;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_3;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_3;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_19 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_19;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_3;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_3;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 32;

                goto try_except_handler_3;
            }
            tmp_tuple_element_1 = const_str_plain_PyReaderError;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 32;
            tmp_assign_source_20 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 32;

                goto try_except_handler_3;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_20;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 32;

                goto try_except_handler_3;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 32;

                    goto try_except_handler_3;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 32;

                    goto try_except_handler_3;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 32;

                    goto try_except_handler_3;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 32;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_3;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_21;
            tmp_assign_source_21 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_21;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_22;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_nbformat$v3$nbpy_32 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_a1ccde01d3ada8afb4b652f98ef1467b;
        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_32, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_plain_PyReaderError;
        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_32, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto try_except_handler_5;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_Exception_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 32;

                goto try_except_handler_5;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_Exception_tuple;
            tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_32, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 32;

                goto try_except_handler_5;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_PyReaderError;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_nbformat$v3$nbpy_32;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 32;
            tmp_assign_source_23 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 32;

                goto try_except_handler_5;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_23;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_22 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_22 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        Py_DECREF( locals_nbformat$v3$nbpy_32 );
        locals_nbformat$v3$nbpy_32 = NULL;
        goto try_return_handler_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbformat$v3$nbpy_32 );
        locals_nbformat$v3$nbpy_32 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 32;
        goto try_except_handler_3;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_PyReaderError, tmp_assign_source_22 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_tuple_element_4;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_NotebookReader );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NotebookReader );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NotebookReader" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;

            goto try_except_handler_6;
        }

        tmp_tuple_element_4 = tmp_mvar_value_4;
        tmp_assign_source_24 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_4 );
        PyTuple_SET_ITEM( tmp_assign_source_24, 0, tmp_tuple_element_4 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_24;
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_25 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_25;
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_26;
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_6;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_6;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_6;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_6;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_6;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_27 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_27;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_6;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_6;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_6;
            }
            tmp_tuple_element_5 = const_str_plain_PyReader;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_5 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_5 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 36;
            tmp_assign_source_28 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_6;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_28;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_6;
            }
            tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_6;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_6 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 36;

                    goto try_except_handler_6;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_6 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 36;

                    goto try_except_handler_6;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_6 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 36;

                    goto try_except_handler_6;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 36;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_6;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_29;
            tmp_assign_source_29 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_29;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_30;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_nbformat$v3$nbpy_36 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_a1ccde01d3ada8afb4b652f98ef1467b;
        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_36, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_8;
        }
        tmp_dictset_value = const_str_plain_PyReader;
        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_36, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_8;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_731c89bd0fef47837207780129665e0e_2, codeobj_731c89bd0fef47837207780129665e0e, module_nbformat$v3$nbpy, sizeof(void *) );
        frame_731c89bd0fef47837207780129665e0e_2 = cache_frame_731c89bd0fef47837207780129665e0e_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_731c89bd0fef47837207780129665e0e_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_731c89bd0fef47837207780129665e0e_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_nbformat$v3$nbpy$$$function_1_reads(  );



        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_36, const_str_plain_reads, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbformat$v3$nbpy$$$function_2_to_notebook(  );



        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_36, const_str_plain_to_notebook, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbformat$v3$nbpy$$$function_3_new_cell(  );



        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_36, const_str_plain_new_cell, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbformat$v3$nbpy$$$function_4__remove_comments(  );



        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_36, const_str_plain__remove_comments, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbformat$v3$nbpy$$$function_5_split_lines_into_blocks(  );



        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_36, const_str_plain_split_lines_into_blocks, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_731c89bd0fef47837207780129665e0e_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_731c89bd0fef47837207780129665e0e_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_731c89bd0fef47837207780129665e0e_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_731c89bd0fef47837207780129665e0e_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_731c89bd0fef47837207780129665e0e_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_731c89bd0fef47837207780129665e0e_2,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_731c89bd0fef47837207780129665e0e_2 == cache_frame_731c89bd0fef47837207780129665e0e_2 )
        {
            Py_DECREF( frame_731c89bd0fef47837207780129665e0e_2 );
        }
        cache_frame_731c89bd0fef47837207780129665e0e_2 = NULL;

        assertFrameObject( frame_731c89bd0fef47837207780129665e0e_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_8;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_8;
            }
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_36, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_8;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_7;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_4 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_7 = const_str_plain_PyReader;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_7 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_7 );
            tmp_tuple_element_7 = locals_nbformat$v3$nbpy_36;
            Py_INCREF( tmp_tuple_element_7 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_7 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 36;
            tmp_assign_source_31 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_8;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_31;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_30 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_30 );
        goto try_return_handler_8;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        Py_DECREF( locals_nbformat$v3$nbpy_36 );
        locals_nbformat$v3$nbpy_36 = NULL;
        goto try_return_handler_7;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbformat$v3$nbpy_36 );
        locals_nbformat$v3$nbpy_36 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_7;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 36;
        goto try_except_handler_6;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_PyReader, tmp_assign_source_30 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_tuple_element_8;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_NotebookWriter );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NotebookWriter );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NotebookWriter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 151;

            goto try_except_handler_9;
        }

        tmp_tuple_element_8 = tmp_mvar_value_5;
        tmp_assign_source_32 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_8 );
        PyTuple_SET_ITEM( tmp_assign_source_32, 0, tmp_tuple_element_8 );
        assert( tmp_class_creation_3__bases_orig == NULL );
        tmp_class_creation_3__bases_orig = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_dircall_arg1_3;
        CHECK_OBJECT( tmp_class_creation_3__bases_orig );
        tmp_dircall_arg1_3 = tmp_class_creation_3__bases_orig;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_33 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_33;
    }
    {
        PyObject *tmp_assign_source_34;
        tmp_assign_source_34 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_34;
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_13;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_14;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_9;
        }
        tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_9;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_9;
        }
        tmp_condition_result_14 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_3 = tmp_class_creation_3__bases;
        tmp_subscript_name_3 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_9;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_9;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_6:;
        condexpr_end_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_35 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_35;
    }
    {
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_9;
        }
        tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_9;
        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_16;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_9 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___prepare__ );
        tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_36;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_10;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_9;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_10 = tmp_class_creation_3__metaclass;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___prepare__ );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;

                goto try_except_handler_9;
            }
            tmp_tuple_element_9 = const_str_plain_PyWriter;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_9 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_9 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 151;
            tmp_assign_source_36 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;

                goto try_except_handler_9;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_36;
        }
        {
            nuitka_bool tmp_condition_result_17;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_11 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_11, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;

                goto try_except_handler_9;
            }
            tmp_condition_result_17 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_10;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_12;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_10 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 151;

                    goto try_except_handler_9;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_10 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_12 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_12 == NULL) );
                tmp_tuple_element_10 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_12 );
                if ( tmp_tuple_element_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 151;

                    goto try_except_handler_9;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_10 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 151;

                    goto try_except_handler_9;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 151;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_9;
            }
            branch_no_11:;
        }
        goto branch_end_10;
        branch_no_10:;
        {
            PyObject *tmp_assign_source_37;
            tmp_assign_source_37 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_37;
        }
        branch_end_10:;
    }
    {
        PyObject *tmp_assign_source_38;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_nbformat$v3$nbpy_151 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_a1ccde01d3ada8afb4b652f98ef1467b;
        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_151, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_11;
        }
        tmp_dictset_value = const_str_plain_PyWriter;
        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_151, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 151;

            goto try_except_handler_11;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_571b126b46a578699a7b55a1776dfdb7_3, codeobj_571b126b46a578699a7b55a1776dfdb7, module_nbformat$v3$nbpy, sizeof(void *) );
        frame_571b126b46a578699a7b55a1776dfdb7_3 = cache_frame_571b126b46a578699a7b55a1776dfdb7_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_571b126b46a578699a7b55a1776dfdb7_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_571b126b46a578699a7b55a1776dfdb7_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_nbformat$v3$nbpy$$$function_6_writes(  );



        tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_151, const_str_plain_writes, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_571b126b46a578699a7b55a1776dfdb7_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_571b126b46a578699a7b55a1776dfdb7_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_571b126b46a578699a7b55a1776dfdb7_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_571b126b46a578699a7b55a1776dfdb7_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_571b126b46a578699a7b55a1776dfdb7_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_571b126b46a578699a7b55a1776dfdb7_3,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_571b126b46a578699a7b55a1776dfdb7_3 == cache_frame_571b126b46a578699a7b55a1776dfdb7_3 )
        {
            Py_DECREF( frame_571b126b46a578699a7b55a1776dfdb7_3 );
        }
        cache_frame_571b126b46a578699a7b55a1776dfdb7_3 = NULL;

        assertFrameObject( frame_571b126b46a578699a7b55a1776dfdb7_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_11;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_3 = tmp_class_creation_3__bases;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_compexpr_right_3 = tmp_class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;

                goto try_except_handler_11;
            }
            tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_dictset_value = tmp_class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_nbformat$v3$nbpy_151, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;

                goto try_except_handler_11;
            }
            branch_no_12:;
        }
        {
            PyObject *tmp_assign_source_39;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_11;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_6 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_11 = const_str_plain_PyWriter;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_11 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_11 );
            tmp_tuple_element_11 = locals_nbformat$v3$nbpy_151;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_11 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 151;
            tmp_assign_source_39 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_39 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;

                goto try_except_handler_11;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_39;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_38 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_38 );
        goto try_return_handler_11;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_11:;
        Py_DECREF( locals_nbformat$v3$nbpy_151 );
        locals_nbformat$v3$nbpy_151 = NULL;
        goto try_return_handler_10;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbformat$v3$nbpy_151 );
        locals_nbformat$v3$nbpy_151 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_10;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbformat$v3$nbpy );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 151;
        goto try_except_handler_9;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_PyWriter, tmp_assign_source_38 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
    Py_DECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_PyReader );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PyReader );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PyReader" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 196;

            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_6;
        frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 196;
        tmp_assign_source_40 = CALL_FUNCTION_NO_ARGS( tmp_called_name_7 );
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 196;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain__reader, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_PyWriter );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PyWriter );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PyWriter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 197;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_7;
        frame_172af58c58bc83062beaf4406bd4dbd5->m_frame.f_lineno = 197;
        tmp_assign_source_41 = CALL_FUNCTION_NO_ARGS( tmp_called_name_8 );
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain__writer, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_source_name_13;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain__reader );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__reader );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_reader" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 199;

            goto frame_exception_exit_1;
        }

        tmp_source_name_13 = tmp_mvar_value_8;
        tmp_assign_source_42 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_reads );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_reads, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain__reader );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__reader );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_reader" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 200;

            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = tmp_mvar_value_9;
        tmp_assign_source_43 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_read );
        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 200;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_read, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_source_name_15;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain__reader );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__reader );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_reader" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 201;

            goto frame_exception_exit_1;
        }

        tmp_source_name_15 = tmp_mvar_value_10;
        tmp_assign_source_44 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_to_notebook );
        if ( tmp_assign_source_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 201;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_to_notebook, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_source_name_16;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain__writer );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__writer );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_writer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 202;

            goto frame_exception_exit_1;
        }

        tmp_source_name_16 = tmp_mvar_value_11;
        tmp_assign_source_45 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_write );
        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_write, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_source_name_17;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain__writer );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__writer );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_writer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 203;

            goto frame_exception_exit_1;
        }

        tmp_source_name_17 = tmp_mvar_value_12;
        tmp_assign_source_46 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_writes );
        if ( tmp_assign_source_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$v3$nbpy, (Nuitka_StringObject *)const_str_plain_writes, tmp_assign_source_46 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_172af58c58bc83062beaf4406bd4dbd5 );
#endif
    popFrameStack();

    assertFrameObject( frame_172af58c58bc83062beaf4406bd4dbd5 );

    goto frame_no_exception_3;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_172af58c58bc83062beaf4406bd4dbd5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_172af58c58bc83062beaf4406bd4dbd5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_172af58c58bc83062beaf4406bd4dbd5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_172af58c58bc83062beaf4406bd4dbd5, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_3:;

    return MOD_RETURN_VALUE( module_nbformat$v3$nbpy );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
