/* Generated code for Python module 'cffi.commontypes'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_cffi$commontypes" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_cffi$commontypes;
PyDictObject *moduledict_cffi$commontypes;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_result;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_digest_58a42555babfe1d800c615f0c5ec11f2;
extern PyObject *const_str_plain_unknown_type;
extern PyObject *const_tuple_str_plain_model_tuple;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_PTCHAR;
static PyObject *const_str_plain_PCTSTR;
extern PyObject *const_str_plain_bool;
extern PyObject *const_str_plain_ALL_PRIMITIVE_TYPES;
static PyObject *const_str_plain__UNICODE_STRING;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_resolve_common_type;
static PyObject *const_str_plain__get_common_types;
extern PyObject *const_str_plain__Bool;
extern PyObject *const_str_plain_FFIError;
extern PyObject *const_str_plain_quals;
extern PyObject *const_str_plain_PointerType;
extern PyObject *const_str_plain_platform;
extern PyObject *const_str_plain___doc__;
static PyObject *const_tuple_str_digest_92dfa96e420126c66052ca0989a42b38_tuple;
static PyObject *const_str_plain_LPTSTR;
static PyObject *const_str_plain_LPCTSTR;
static PyObject *const_str_plain_PTSTR;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_plain__t;
extern PyObject *const_str_plain_parse_type_and_quals;
static PyObject *const_str_digest_126e5f2fa37362bc1210c2e514308243;
extern PyObject *const_str_plain_FILE;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_52240561c730b878f0ada2f8394ecb25;
static PyObject *const_str_plain_MaximumLength;
extern PyObject *const_str_plain_BaseTypeByIdentity;
extern PyObject *const_str_plain_endswith;
static PyObject *const_str_digest_c73d1190325a3cb35f485e081dbb1ae5;
extern PyObject *const_str_plain_parser;
extern PyObject *const_str_plain_error;
static PyObject *const_str_plain_win_common_types;
static PyObject *const_str_digest_2c9aaada0bc7f5027c3c847f30a4dc7c;
static PyObject *const_tuple_str_plain_FFIError_tuple;
static PyObject *const_str_plain_UNICODE_STRING;
static PyObject *const_str_plain_PTBYTE;
extern PyObject *const_str_plain_PrimitiveType;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_Buffer;
static PyObject *const_str_plain_PCUNICODE_STRING;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_PUNICODE_STRING;
extern PyObject *const_str_digest_92dfa96e420126c66052ca0989a42b38;
extern PyObject *const_str_plain_COMMON_TYPES;
static PyObject *const_list_str_plain_Length_str_plain_MaximumLength_str_plain_Buffer_list;
static PyObject *const_tuple_str_plain__t_tuple;
static PyObject *const_str_plain_commontype;
static PyObject *const_tuple_str_plain__get_common_types_tuple;
extern PyObject *const_str_plain__type;
extern PyObject *const_str_plain_StructType;
static PyObject *const_str_plain_TCHAR;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_cbe451655beeedcb9db9f28d182f551d;
extern PyObject *const_str_plain_sys;
static PyObject *const_tuple_str_plain_wchar_t_tuple;
extern PyObject *const_str_plain_update;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain__IO_FILE;
static PyObject *const_str_digest_7cceb741be998c6be0db098f025ad776;
static PyObject *const_list_int_neg_1_int_neg_1_int_neg_1_list;
static PyObject *const_str_digest_c2a6ff702e6709f9d5ede4f522684700;
extern PyObject *const_str_plain_wchar_t;
extern PyObject *const_str_plain__cffi_backend;
static PyObject *const_str_plain__CACHE;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_has_location;
static PyObject *const_tuple_str_plain_FILE_str_plain__IO_FILE_tuple;
static PyObject *const_str_plain_TBYTE;
static PyObject *const_tuple_b316d758ae1e83223b5899ed0002bca3_tuple;
extern PyObject *const_str_plain_cdecl;
extern PyObject *const_str_empty;
static PyObject *const_str_plain_Length;
extern PyObject *const_str_plain_win32;
extern PyObject *const_str_plain_model;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_58a42555babfe1d800c615f0c5ec11f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 195808 ], 16, 0 );
    const_str_plain_PTCHAR = UNSTREAM_STRING_ASCII( &constant_bin[ 184567 ], 6, 1 );
    const_str_plain_PCTSTR = UNSTREAM_STRING_ASCII( &constant_bin[ 184446 ], 6, 1 );
    const_str_plain__UNICODE_STRING = UNSTREAM_STRING_ASCII( &constant_bin[ 195824 ], 15, 1 );
    const_str_plain__get_common_types = UNSTREAM_STRING_ASCII( &constant_bin[ 195839 ], 17, 1 );
    const_tuple_str_digest_92dfa96e420126c66052ca0989a42b38_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_92dfa96e420126c66052ca0989a42b38_tuple, 0, const_str_digest_92dfa96e420126c66052ca0989a42b38 ); Py_INCREF( const_str_digest_92dfa96e420126c66052ca0989a42b38 );
    const_str_plain_LPTSTR = UNSTREAM_STRING_ASCII( &constant_bin[ 184500 ], 6, 1 );
    const_str_plain_LPCTSTR = UNSTREAM_STRING_ASCII( &constant_bin[ 184445 ], 7, 1 );
    const_str_plain_PTSTR = UNSTREAM_STRING_ASCII( &constant_bin[ 184501 ], 5, 1 );
    const_str_digest_126e5f2fa37362bc1210c2e514308243 = UNSTREAM_STRING_ASCII( &constant_bin[ 195856 ], 25, 0 );
    const_str_digest_52240561c730b878f0ada2f8394ecb25 = UNSTREAM_STRING_ASCII( &constant_bin[ 195881 ], 22, 0 );
    const_str_plain_MaximumLength = UNSTREAM_STRING_ASCII( &constant_bin[ 195903 ], 13, 1 );
    const_str_digest_c73d1190325a3cb35f485e081dbb1ae5 = UNSTREAM_STRING_ASCII( &constant_bin[ 195916 ], 174, 0 );
    const_str_plain_win_common_types = UNSTREAM_STRING_ASCII( &constant_bin[ 196090 ], 16, 1 );
    const_str_digest_2c9aaada0bc7f5027c3c847f30a4dc7c = UNSTREAM_STRING_ASCII( &constant_bin[ 195864 ], 16, 0 );
    const_tuple_str_plain_FFIError_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_FFIError_tuple, 0, const_str_plain_FFIError ); Py_INCREF( const_str_plain_FFIError );
    const_str_plain_UNICODE_STRING = UNSTREAM_STRING_ASCII( &constant_bin[ 195808 ], 14, 1 );
    const_str_plain_PTBYTE = UNSTREAM_STRING_ASCII( &constant_bin[ 184545 ], 6, 1 );
    const_str_plain_PCUNICODE_STRING = UNSTREAM_STRING_ASCII( &constant_bin[ 196106 ], 16, 1 );
    const_str_plain_PUNICODE_STRING = UNSTREAM_STRING_ASCII( &constant_bin[ 196122 ], 15, 1 );
    const_list_str_plain_Length_str_plain_MaximumLength_str_plain_Buffer_list = PyList_New( 3 );
    const_str_plain_Length = UNSTREAM_STRING_ASCII( &constant_bin[ 106827 ], 6, 1 );
    PyList_SET_ITEM( const_list_str_plain_Length_str_plain_MaximumLength_str_plain_Buffer_list, 0, const_str_plain_Length ); Py_INCREF( const_str_plain_Length );
    PyList_SET_ITEM( const_list_str_plain_Length_str_plain_MaximumLength_str_plain_Buffer_list, 1, const_str_plain_MaximumLength ); Py_INCREF( const_str_plain_MaximumLength );
    PyList_SET_ITEM( const_list_str_plain_Length_str_plain_MaximumLength_str_plain_Buffer_list, 2, const_str_plain_Buffer ); Py_INCREF( const_str_plain_Buffer );
    const_tuple_str_plain__t_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__t_tuple, 0, const_str_plain__t ); Py_INCREF( const_str_plain__t );
    const_str_plain_commontype = UNSTREAM_STRING_ASCII( &constant_bin[ 195869 ], 10, 1 );
    const_tuple_str_plain__get_common_types_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain__get_common_types_tuple, 0, const_str_plain__get_common_types ); Py_INCREF( const_str_plain__get_common_types );
    const_str_plain_TCHAR = UNSTREAM_STRING_ASCII( &constant_bin[ 179968 ], 5, 1 );
    const_str_digest_cbe451655beeedcb9db9f28d182f551d = UNSTREAM_STRING_ASCII( &constant_bin[ 196137 ], 19, 0 );
    const_tuple_str_plain_wchar_t_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_wchar_t_tuple, 0, const_str_plain_wchar_t ); Py_INCREF( const_str_plain_wchar_t );
    const_str_digest_7cceb741be998c6be0db098f025ad776 = UNSTREAM_STRING_ASCII( &constant_bin[ 196156 ], 18, 0 );
    const_list_int_neg_1_int_neg_1_int_neg_1_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_int_neg_1_int_neg_1_int_neg_1_list, 0, const_int_neg_1 ); Py_INCREF( const_int_neg_1 );
    PyList_SET_ITEM( const_list_int_neg_1_int_neg_1_int_neg_1_list, 1, const_int_neg_1 ); Py_INCREF( const_int_neg_1 );
    PyList_SET_ITEM( const_list_int_neg_1_int_neg_1_int_neg_1_list, 2, const_int_neg_1 ); Py_INCREF( const_int_neg_1 );
    const_str_digest_c2a6ff702e6709f9d5ede4f522684700 = UNSTREAM_STRING_ASCII( &constant_bin[ 196174 ], 70, 0 );
    const_str_plain__CACHE = UNSTREAM_STRING_ASCII( &constant_bin[ 196244 ], 6, 1 );
    const_tuple_str_plain_FILE_str_plain__IO_FILE_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_FILE_str_plain__IO_FILE_tuple, 0, const_str_plain_FILE ); Py_INCREF( const_str_plain_FILE );
    PyTuple_SET_ITEM( const_tuple_str_plain_FILE_str_plain__IO_FILE_tuple, 1, const_str_plain__IO_FILE ); Py_INCREF( const_str_plain__IO_FILE );
    const_str_plain_TBYTE = UNSTREAM_STRING_ASCII( &constant_bin[ 184394 ], 5, 1 );
    const_tuple_b316d758ae1e83223b5899ed0002bca3_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b316d758ae1e83223b5899ed0002bca3_tuple, 0, const_str_plain_parser ); Py_INCREF( const_str_plain_parser );
    PyTuple_SET_ITEM( const_tuple_b316d758ae1e83223b5899ed0002bca3_tuple, 1, const_str_plain_commontype ); Py_INCREF( const_str_plain_commontype );
    PyTuple_SET_ITEM( const_tuple_b316d758ae1e83223b5899ed0002bca3_tuple, 2, const_str_plain_cdecl ); Py_INCREF( const_str_plain_cdecl );
    PyTuple_SET_ITEM( const_tuple_b316d758ae1e83223b5899ed0002bca3_tuple, 3, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    PyTuple_SET_ITEM( const_tuple_b316d758ae1e83223b5899ed0002bca3_tuple, 4, const_str_plain_quals ); Py_INCREF( const_str_plain_quals );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_cffi$commontypes( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_70b441863670e67e2d14c2f7a3e65be3;
static PyCodeObject *codeobj_1f5960881092eed119ebf150a7fd2eac;
static PyCodeObject *codeobj_35cf3411ce9b7245b41126c939a811d8;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_cbe451655beeedcb9db9f28d182f551d );
    codeobj_70b441863670e67e2d14c2f7a3e65be3 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_126e5f2fa37362bc1210c2e514308243, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_1f5960881092eed119ebf150a7fd2eac = MAKE_CODEOBJ( module_filename_obj, const_str_plain_resolve_common_type, 25, const_tuple_b316d758ae1e83223b5899ed0002bca3_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_35cf3411ce9b7245b41126c939a811d8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_win_common_types, 55, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_cffi$commontypes$$$function_1_resolve_common_type(  );


static PyObject *MAKE_FUNCTION_cffi$commontypes$$$function_2_win_common_types(  );


// The module function definitions.
static PyObject *impl_cffi$commontypes$$$function_1_resolve_common_type( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_parser = python_pars[ 0 ];
    PyObject *par_commontype = python_pars[ 1 ];
    PyObject *var_cdecl = NULL;
    PyObject *var_result = NULL;
    PyObject *var_quals = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    struct Nuitka_FrameObject *frame_1f5960881092eed119ebf150a7fd2eac;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    bool tmp_result;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    static struct Nuitka_FrameObject *cache_frame_1f5960881092eed119ebf150a7fd2eac = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1f5960881092eed119ebf150a7fd2eac, codeobj_1f5960881092eed119ebf150a7fd2eac, module_cffi$commontypes, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1f5960881092eed119ebf150a7fd2eac = cache_frame_1f5960881092eed119ebf150a7fd2eac;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1f5960881092eed119ebf150a7fd2eac );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1f5960881092eed119ebf150a7fd2eac ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain__CACHE );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__CACHE );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_CACHE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 27;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_commontype );
        tmp_subscript_name_1 = par_commontype;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$commontypes$$$function_1_resolve_common_type );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_1f5960881092eed119ebf150a7fd2eac, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_1f5960881092eed119ebf150a7fd2eac, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "ooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COMMON_TYPES" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 29;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_called_instance_1 = tmp_mvar_value_2;
            CHECK_OBJECT( par_commontype );
            tmp_args_element_name_1 = par_commontype;
            CHECK_OBJECT( par_commontype );
            tmp_args_element_name_2 = par_commontype;
            frame_1f5960881092eed119ebf150a7fd2eac->m_frame.f_lineno = 29;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
            }

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 29;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            assert( var_cdecl == NULL );
            var_cdecl = tmp_assign_source_1;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            CHECK_OBJECT( var_cdecl );
            tmp_isinstance_inst_1 = var_cdecl;
            tmp_isinstance_cls_1 = (PyObject *)&PyUnicode_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 30;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 30;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_iter_arg_1;
                PyObject *tmp_tuple_element_1;
                CHECK_OBJECT( var_cdecl );
                tmp_tuple_element_1 = var_cdecl;
                tmp_iter_arg_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_iter_arg_1, 0, tmp_tuple_element_1 );
                tmp_tuple_element_1 = const_int_0;
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_iter_arg_1, 1, tmp_tuple_element_1 );
                tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
                Py_DECREF( tmp_iter_arg_1 );
                assert( !(tmp_assign_source_2 == NULL) );
                assert( tmp_tuple_unpack_1__source_iter == NULL );
                tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
            }
            // Tried code:
            // Tried code:
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_unpack_1;
                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
                tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
                if ( tmp_assign_source_3 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooo";
                    exception_lineno = 31;
                    goto try_except_handler_5;
                }
                assert( tmp_tuple_unpack_1__element_1 == NULL );
                tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
            }
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_unpack_2;
                CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
                tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
                if ( tmp_assign_source_4 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooo";
                    exception_lineno = 31;
                    goto try_except_handler_5;
                }
                assert( tmp_tuple_unpack_1__element_2 == NULL );
                tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
            }
            goto try_end_1;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
            Py_DECREF( tmp_tuple_unpack_1__source_iter );
            tmp_tuple_unpack_1__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto try_except_handler_4;
            // End of try:
            try_end_1:;
            goto try_end_2;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_tuple_unpack_1__element_1 );
            tmp_tuple_unpack_1__element_1 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto try_except_handler_3;
            // End of try:
            try_end_2:;
            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
            Py_DECREF( tmp_tuple_unpack_1__source_iter );
            tmp_tuple_unpack_1__source_iter = NULL;

            {
                PyObject *tmp_assign_source_5;
                CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
                tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;
                assert( var_result == NULL );
                Py_INCREF( tmp_assign_source_5 );
                var_result = tmp_assign_source_5;
            }
            Py_XDECREF( tmp_tuple_unpack_1__element_1 );
            tmp_tuple_unpack_1__element_1 = NULL;

            {
                PyObject *tmp_assign_source_6;
                CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
                tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;
                assert( var_quals == NULL );
                Py_INCREF( tmp_assign_source_6 );
                var_quals = tmp_assign_source_6;
            }
            Py_XDECREF( tmp_tuple_unpack_1__element_2 );
            tmp_tuple_unpack_1__element_2 = NULL;

            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                PyObject *tmp_source_name_1;
                PyObject *tmp_source_name_2;
                PyObject *tmp_mvar_value_3;
                CHECK_OBJECT( var_cdecl );
                tmp_compexpr_left_2 = var_cdecl;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_model );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "model" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 32;
                    type_description_1 = "ooooo";
                    goto try_except_handler_3;
                }

                tmp_source_name_2 = tmp_mvar_value_3;
                tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PrimitiveType );
                if ( tmp_source_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 32;
                    type_description_1 = "ooooo";
                    goto try_except_handler_3;
                }
                tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ALL_PRIMITIVE_TYPES );
                Py_DECREF( tmp_source_name_1 );
                if ( tmp_compexpr_right_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 32;
                    type_description_1 = "ooooo";
                    goto try_except_handler_3;
                }
                tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
                Py_DECREF( tmp_compexpr_right_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 32;
                    type_description_1 = "ooooo";
                    goto try_except_handler_3;
                }
                tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                // Tried code:
                {
                    PyObject *tmp_assign_source_7;
                    PyObject *tmp_iter_arg_2;
                    PyObject *tmp_tuple_element_2;
                    PyObject *tmp_called_instance_2;
                    PyObject *tmp_mvar_value_4;
                    PyObject *tmp_args_element_name_3;
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model );

                    if (unlikely( tmp_mvar_value_4 == NULL ))
                    {
                        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_model );
                    }

                    if ( tmp_mvar_value_4 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "model" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 33;
                        type_description_1 = "ooooo";
                        goto try_except_handler_6;
                    }

                    tmp_called_instance_2 = tmp_mvar_value_4;
                    CHECK_OBJECT( var_cdecl );
                    tmp_args_element_name_3 = var_cdecl;
                    frame_1f5960881092eed119ebf150a7fd2eac->m_frame.f_lineno = 33;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_3 };
                        tmp_tuple_element_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_PrimitiveType, call_args );
                    }

                    if ( tmp_tuple_element_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 33;
                        type_description_1 = "ooooo";
                        goto try_except_handler_6;
                    }
                    tmp_iter_arg_2 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_iter_arg_2, 0, tmp_tuple_element_2 );
                    tmp_tuple_element_2 = const_int_0;
                    Py_INCREF( tmp_tuple_element_2 );
                    PyTuple_SET_ITEM( tmp_iter_arg_2, 1, tmp_tuple_element_2 );
                    tmp_assign_source_7 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
                    Py_DECREF( tmp_iter_arg_2 );
                    if ( tmp_assign_source_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 33;
                        type_description_1 = "ooooo";
                        goto try_except_handler_6;
                    }
                    assert( tmp_tuple_unpack_2__source_iter == NULL );
                    tmp_tuple_unpack_2__source_iter = tmp_assign_source_7;
                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_8;
                    PyObject *tmp_unpack_3;
                    CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                    tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
                    tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
                    if ( tmp_assign_source_8 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooo";
                        exception_lineno = 33;
                        goto try_except_handler_7;
                    }
                    assert( tmp_tuple_unpack_2__element_1 == NULL );
                    tmp_tuple_unpack_2__element_1 = tmp_assign_source_8;
                }
                {
                    PyObject *tmp_assign_source_9;
                    PyObject *tmp_unpack_4;
                    CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                    tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
                    tmp_assign_source_9 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
                    if ( tmp_assign_source_9 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooo";
                        exception_lineno = 33;
                        goto try_except_handler_7;
                    }
                    assert( tmp_tuple_unpack_2__element_2 == NULL );
                    tmp_tuple_unpack_2__element_2 = tmp_assign_source_9;
                }
                goto try_end_3;
                // Exception handler code:
                try_except_handler_7:;
                exception_keeper_type_4 = exception_type;
                exception_keeper_value_4 = exception_value;
                exception_keeper_tb_4 = exception_tb;
                exception_keeper_lineno_4 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
                Py_DECREF( tmp_tuple_unpack_2__source_iter );
                tmp_tuple_unpack_2__source_iter = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_4;
                exception_value = exception_keeper_value_4;
                exception_tb = exception_keeper_tb_4;
                exception_lineno = exception_keeper_lineno_4;

                goto try_except_handler_6;
                // End of try:
                try_end_3:;
                goto try_end_4;
                // Exception handler code:
                try_except_handler_6:;
                exception_keeper_type_5 = exception_type;
                exception_keeper_value_5 = exception_value;
                exception_keeper_tb_5 = exception_tb;
                exception_keeper_lineno_5 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_tuple_unpack_2__element_1 );
                tmp_tuple_unpack_2__element_1 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_5;
                exception_value = exception_keeper_value_5;
                exception_tb = exception_keeper_tb_5;
                exception_lineno = exception_keeper_lineno_5;

                goto try_except_handler_3;
                // End of try:
                try_end_4:;
                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
                Py_DECREF( tmp_tuple_unpack_2__source_iter );
                tmp_tuple_unpack_2__source_iter = NULL;

                {
                    PyObject *tmp_assign_source_10;
                    CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
                    tmp_assign_source_10 = tmp_tuple_unpack_2__element_1;
                    assert( var_result == NULL );
                    Py_INCREF( tmp_assign_source_10 );
                    var_result = tmp_assign_source_10;
                }
                Py_XDECREF( tmp_tuple_unpack_2__element_1 );
                tmp_tuple_unpack_2__element_1 = NULL;

                {
                    PyObject *tmp_assign_source_11;
                    CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
                    tmp_assign_source_11 = tmp_tuple_unpack_2__element_2;
                    assert( var_quals == NULL );
                    Py_INCREF( tmp_assign_source_11 );
                    var_quals = tmp_assign_source_11;
                }
                Py_XDECREF( tmp_tuple_unpack_2__element_2 );
                tmp_tuple_unpack_2__element_2 = NULL;

                goto branch_end_3;
                branch_no_3:;
                {
                    nuitka_bool tmp_condition_result_4;
                    PyObject *tmp_compexpr_left_3;
                    PyObject *tmp_compexpr_right_3;
                    CHECK_OBJECT( var_cdecl );
                    tmp_compexpr_left_3 = var_cdecl;
                    tmp_compexpr_right_3 = const_str_digest_7cceb741be998c6be0db098f025ad776;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 34;
                        type_description_1 = "ooooo";
                        goto try_except_handler_3;
                    }
                    tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    {
                        PyObject *tmp_raise_type_1;
                        PyObject *tmp_called_name_1;
                        PyObject *tmp_mvar_value_5;
                        PyObject *tmp_args_element_name_4;
                        PyObject *tmp_left_name_1;
                        PyObject *tmp_right_name_1;
                        PyObject *tmp_tuple_element_3;
                        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_FFIError );

                        if (unlikely( tmp_mvar_value_5 == NULL ))
                        {
                            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FFIError );
                        }

                        if ( tmp_mvar_value_5 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FFIError" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 35;
                            type_description_1 = "ooooo";
                            goto try_except_handler_3;
                        }

                        tmp_called_name_1 = tmp_mvar_value_5;
                        tmp_left_name_1 = const_str_digest_c2a6ff702e6709f9d5ede4f522684700;
                        CHECK_OBJECT( par_commontype );
                        tmp_tuple_element_3 = par_commontype;
                        tmp_right_name_1 = PyTuple_New( 1 );
                        Py_INCREF( tmp_tuple_element_3 );
                        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                        tmp_args_element_name_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                        Py_DECREF( tmp_right_name_1 );
                        if ( tmp_args_element_name_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 35;
                            type_description_1 = "ooooo";
                            goto try_except_handler_3;
                        }
                        frame_1f5960881092eed119ebf150a7fd2eac->m_frame.f_lineno = 35;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_4 };
                            tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                        }

                        Py_DECREF( tmp_args_element_name_4 );
                        if ( tmp_raise_type_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 35;
                            type_description_1 = "ooooo";
                            goto try_except_handler_3;
                        }
                        exception_type = tmp_raise_type_1;
                        exception_lineno = 35;
                        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "ooooo";
                        goto try_except_handler_3;
                    }
                    goto branch_end_4;
                    branch_no_4:;
                    {
                        nuitka_bool tmp_condition_result_5;
                        PyObject *tmp_compexpr_left_4;
                        PyObject *tmp_compexpr_right_4;
                        CHECK_OBJECT( par_commontype );
                        tmp_compexpr_left_4 = par_commontype;
                        CHECK_OBJECT( var_cdecl );
                        tmp_compexpr_right_4 = var_cdecl;
                        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 38;
                            type_description_1 = "ooooo";
                            goto try_except_handler_3;
                        }
                        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_5;
                        }
                        else
                        {
                            goto branch_no_5;
                        }
                        branch_yes_5:;
                        {
                            PyObject *tmp_raise_type_2;
                            PyObject *tmp_called_name_2;
                            PyObject *tmp_mvar_value_6;
                            PyObject *tmp_args_element_name_5;
                            PyObject *tmp_left_name_2;
                            PyObject *tmp_right_name_2;
                            PyObject *tmp_tuple_element_4;
                            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_FFIError );

                            if (unlikely( tmp_mvar_value_6 == NULL ))
                            {
                                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FFIError );
                            }

                            if ( tmp_mvar_value_6 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FFIError" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 39;
                                type_description_1 = "ooooo";
                                goto try_except_handler_3;
                            }

                            tmp_called_name_2 = tmp_mvar_value_6;
                            tmp_left_name_2 = const_str_digest_c73d1190325a3cb35f485e081dbb1ae5;
                            CHECK_OBJECT( par_commontype );
                            tmp_tuple_element_4 = par_commontype;
                            tmp_right_name_2 = PyTuple_New( 1 );
                            Py_INCREF( tmp_tuple_element_4 );
                            PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_4 );
                            tmp_args_element_name_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                            Py_DECREF( tmp_right_name_2 );
                            if ( tmp_args_element_name_5 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 40;
                                type_description_1 = "ooooo";
                                goto try_except_handler_3;
                            }
                            frame_1f5960881092eed119ebf150a7fd2eac->m_frame.f_lineno = 39;
                            {
                                PyObject *call_args[] = { tmp_args_element_name_5 };
                                tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                            }

                            Py_DECREF( tmp_args_element_name_5 );
                            if ( tmp_raise_type_2 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 39;
                                type_description_1 = "ooooo";
                                goto try_except_handler_3;
                            }
                            exception_type = tmp_raise_type_2;
                            exception_lineno = 39;
                            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "ooooo";
                            goto try_except_handler_3;
                        }
                        branch_no_5:;
                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_12;
                        PyObject *tmp_iter_arg_3;
                        PyObject *tmp_called_instance_3;
                        PyObject *tmp_args_element_name_6;
                        CHECK_OBJECT( par_parser );
                        tmp_called_instance_3 = par_parser;
                        CHECK_OBJECT( var_cdecl );
                        tmp_args_element_name_6 = var_cdecl;
                        frame_1f5960881092eed119ebf150a7fd2eac->m_frame.f_lineno = 44;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_6 };
                            tmp_iter_arg_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_parse_type_and_quals, call_args );
                        }

                        if ( tmp_iter_arg_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 44;
                            type_description_1 = "ooooo";
                            goto try_except_handler_8;
                        }
                        tmp_assign_source_12 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_3 );
                        Py_DECREF( tmp_iter_arg_3 );
                        if ( tmp_assign_source_12 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 44;
                            type_description_1 = "ooooo";
                            goto try_except_handler_8;
                        }
                        assert( tmp_tuple_unpack_3__source_iter == NULL );
                        tmp_tuple_unpack_3__source_iter = tmp_assign_source_12;
                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_13;
                        PyObject *tmp_unpack_5;
                        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                        tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;
                        tmp_assign_source_13 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
                        if ( tmp_assign_source_13 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooo";
                            exception_lineno = 44;
                            goto try_except_handler_9;
                        }
                        assert( tmp_tuple_unpack_3__element_1 == NULL );
                        tmp_tuple_unpack_3__element_1 = tmp_assign_source_13;
                    }
                    {
                        PyObject *tmp_assign_source_14;
                        PyObject *tmp_unpack_6;
                        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                        tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;
                        tmp_assign_source_14 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
                        if ( tmp_assign_source_14 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooo";
                            exception_lineno = 44;
                            goto try_except_handler_9;
                        }
                        assert( tmp_tuple_unpack_3__element_2 == NULL );
                        tmp_tuple_unpack_3__element_2 = tmp_assign_source_14;
                    }
                    {
                        PyObject *tmp_iterator_name_1;
                        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                        tmp_iterator_name_1 = tmp_tuple_unpack_3__source_iter;
                        // Check if iterator has left-over elements.
                        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

                        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

                        if (likely( tmp_iterator_attempt == NULL ))
                        {
                            PyObject *error = GET_ERROR_OCCURRED();

                            if ( error != NULL )
                            {
                                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                                {
                                    CLEAR_ERROR_OCCURRED();
                                }
                                else
                                {
                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                    type_description_1 = "ooooo";
                                    exception_lineno = 44;
                                    goto try_except_handler_9;
                                }
                            }
                        }
                        else
                        {
                            Py_DECREF( tmp_iterator_attempt );

                            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "ooooo";
                            exception_lineno = 44;
                            goto try_except_handler_9;
                        }
                    }
                    goto try_end_5;
                    // Exception handler code:
                    try_except_handler_9:;
                    exception_keeper_type_6 = exception_type;
                    exception_keeper_value_6 = exception_value;
                    exception_keeper_tb_6 = exception_tb;
                    exception_keeper_lineno_6 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
                    Py_DECREF( tmp_tuple_unpack_3__source_iter );
                    tmp_tuple_unpack_3__source_iter = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_6;
                    exception_value = exception_keeper_value_6;
                    exception_tb = exception_keeper_tb_6;
                    exception_lineno = exception_keeper_lineno_6;

                    goto try_except_handler_8;
                    // End of try:
                    try_end_5:;
                    goto try_end_6;
                    // Exception handler code:
                    try_except_handler_8:;
                    exception_keeper_type_7 = exception_type;
                    exception_keeper_value_7 = exception_value;
                    exception_keeper_tb_7 = exception_tb;
                    exception_keeper_lineno_7 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
                    tmp_tuple_unpack_3__element_1 = NULL;

                    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
                    tmp_tuple_unpack_3__element_2 = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_7;
                    exception_value = exception_keeper_value_7;
                    exception_tb = exception_keeper_tb_7;
                    exception_lineno = exception_keeper_lineno_7;

                    goto try_except_handler_3;
                    // End of try:
                    try_end_6:;
                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
                    Py_DECREF( tmp_tuple_unpack_3__source_iter );
                    tmp_tuple_unpack_3__source_iter = NULL;

                    {
                        PyObject *tmp_assign_source_15;
                        CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
                        tmp_assign_source_15 = tmp_tuple_unpack_3__element_1;
                        assert( var_result == NULL );
                        Py_INCREF( tmp_assign_source_15 );
                        var_result = tmp_assign_source_15;
                    }
                    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
                    tmp_tuple_unpack_3__element_1 = NULL;

                    {
                        PyObject *tmp_assign_source_16;
                        CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
                        tmp_assign_source_16 = tmp_tuple_unpack_3__element_2;
                        assert( var_quals == NULL );
                        Py_INCREF( tmp_assign_source_16 );
                        var_quals = tmp_assign_source_16;
                    }
                    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
                    tmp_tuple_unpack_3__element_2 = NULL;

                    branch_end_4:;
                }
                branch_end_3:;
            }
            branch_end_2:;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_mvar_value_7;
            if ( var_result == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "result" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 46;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_isinstance_inst_2 = var_result;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_model );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "model" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 46;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_source_name_3 = tmp_mvar_value_7;
            tmp_isinstance_cls_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_BaseTypeByIdentity );
            if ( tmp_isinstance_cls_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            Py_DECREF( tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_raise_type_3;
                tmp_raise_type_3 = PyExc_AssertionError;
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_lineno = 46;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
            branch_no_6:;
        }
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_tuple_element_5;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_ass_subscript_1;
            if ( var_result == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "result" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 47;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_tuple_element_5 = var_result;
            tmp_ass_subvalue_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_ass_subvalue_1, 0, tmp_tuple_element_5 );
            if ( var_quals == NULL )
            {
                Py_DECREF( tmp_ass_subvalue_1 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "quals" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 47;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_tuple_element_5 = var_quals;
            Py_INCREF( tmp_tuple_element_5 );
            PyTuple_SET_ITEM( tmp_ass_subvalue_1, 1, tmp_tuple_element_5 );
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain__CACHE );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__CACHE );
            }

            if ( tmp_mvar_value_8 == NULL )
            {
                Py_DECREF( tmp_ass_subvalue_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_CACHE" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 47;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_ass_subscribed_1 = tmp_mvar_value_8;
            CHECK_OBJECT( par_commontype );
            tmp_ass_subscript_1 = par_commontype;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 47;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }
        }
        {
            PyObject *tmp_tuple_element_6;
            if ( var_result == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "result" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 48;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_tuple_element_6 = var_result;
            tmp_return_value = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_6 );
            if ( var_quals == NULL )
            {
                Py_DECREF( tmp_return_value );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "quals" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 48;
                type_description_1 = "ooooo";
                goto try_except_handler_3;
            }

            tmp_tuple_element_6 = var_quals;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_6 );
            goto try_return_handler_3;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 26;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_1f5960881092eed119ebf150a7fd2eac->m_frame) frame_1f5960881092eed119ebf150a7fd2eac->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "ooooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$commontypes$$$function_1_resolve_common_type );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1f5960881092eed119ebf150a7fd2eac );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1f5960881092eed119ebf150a7fd2eac );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1f5960881092eed119ebf150a7fd2eac );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1f5960881092eed119ebf150a7fd2eac, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1f5960881092eed119ebf150a7fd2eac->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1f5960881092eed119ebf150a7fd2eac, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1f5960881092eed119ebf150a7fd2eac,
        type_description_1,
        par_parser,
        par_commontype,
        var_cdecl,
        var_result,
        var_quals
    );


    // Release cached frame.
    if ( frame_1f5960881092eed119ebf150a7fd2eac == cache_frame_1f5960881092eed119ebf150a7fd2eac )
    {
        Py_DECREF( frame_1f5960881092eed119ebf150a7fd2eac );
    }
    cache_frame_1f5960881092eed119ebf150a7fd2eac = NULL;

    assertFrameObject( frame_1f5960881092eed119ebf150a7fd2eac );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$commontypes$$$function_1_resolve_common_type );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_parser );
    Py_DECREF( par_parser );
    par_parser = NULL;

    CHECK_OBJECT( (PyObject *)par_commontype );
    Py_DECREF( par_commontype );
    par_commontype = NULL;

    Py_XDECREF( var_cdecl );
    var_cdecl = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_quals );
    var_quals = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_parser );
    Py_DECREF( par_parser );
    par_parser = NULL;

    CHECK_OBJECT( (PyObject *)par_commontype );
    Py_DECREF( par_commontype );
    par_commontype = NULL;

    Py_XDECREF( var_cdecl );
    var_cdecl = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    Py_XDECREF( var_quals );
    var_quals = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$commontypes$$$function_1_resolve_common_type );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_cffi$commontypes$$$function_2_win_common_types( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_35cf3411ce9b7245b41126c939a811d8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_35cf3411ce9b7245b41126c939a811d8 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_35cf3411ce9b7245b41126c939a811d8, codeobj_35cf3411ce9b7245b41126c939a811d8, module_cffi$commontypes, 0 );
    frame_35cf3411ce9b7245b41126c939a811d8 = cache_frame_35cf3411ce9b7245b41126c939a811d8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_35cf3411ce9b7245b41126c939a811d8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_35cf3411ce9b7245b41126c939a811d8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        tmp_dict_key_1 = const_str_plain_UNICODE_STRING;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_model );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "model" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 57;

            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_StructType );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = const_str_plain__UNICODE_STRING;
        tmp_args_element_name_2 = LIST_COPY( const_list_str_plain_Length_str_plain_MaximumLength_str_plain_Buffer_list );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_model );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "model" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 62;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_2;
        frame_35cf3411ce9b7245b41126c939a811d8->m_frame.f_lineno = 62;
        tmp_list_element_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_PrimitiveType, &PyTuple_GET_ITEM( const_tuple_str_digest_92dfa96e420126c66052ca0989a42b38_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 62;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = PyList_New( 3 );
        PyList_SET_ITEM( tmp_args_element_name_3, 0, tmp_list_element_1 );
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_model );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "model" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_3;
        frame_35cf3411ce9b7245b41126c939a811d8->m_frame.f_lineno = 63;
        tmp_list_element_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_PrimitiveType, &PyTuple_GET_ITEM( const_tuple_str_digest_92dfa96e420126c66052ca0989a42b38_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_3, 1, tmp_list_element_1 );
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_model );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "model" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_4;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PointerType );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_model );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "model" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_5;
        frame_35cf3411ce9b7245b41126c939a811d8->m_frame.f_lineno = 64;
        tmp_args_element_name_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_PrimitiveType, &PyTuple_GET_ITEM( const_tuple_str_plain_wchar_t_tuple, 0 ) );

        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }
        frame_35cf3411ce9b7245b41126c939a811d8->m_frame.f_lineno = 64;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 64;

            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_3, 2, tmp_list_element_1 );
        tmp_args_element_name_5 = LIST_COPY( const_list_int_neg_1_int_neg_1_int_neg_1_list );
        frame_35cf3411ce9b7245b41126c939a811d8->m_frame.f_lineno = 57;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_5 };
            tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;

            goto frame_exception_exit_1;
        }
        tmp_return_value = _PyDict_NewPresized( 11 );
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_PUNICODE_STRING;
        tmp_dict_value_2 = const_str_digest_58a42555babfe1d800c615f0c5ec11f2;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_PCUNICODE_STRING;
        tmp_dict_value_3 = const_str_digest_52240561c730b878f0ada2f8394ecb25;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_TBYTE;
        tmp_dict_value_4 = const_str_digest_7cceb741be998c6be0db098f025ad776;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_5 = const_str_plain_TCHAR;
        tmp_dict_value_5 = const_str_digest_7cceb741be998c6be0db098f025ad776;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_LPCTSTR;
        tmp_dict_value_6 = const_str_digest_7cceb741be998c6be0db098f025ad776;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_7 = const_str_plain_PCTSTR;
        tmp_dict_value_7 = const_str_digest_7cceb741be998c6be0db098f025ad776;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_LPTSTR;
        tmp_dict_value_8 = const_str_digest_7cceb741be998c6be0db098f025ad776;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_9 = const_str_plain_PTSTR;
        tmp_dict_value_9 = const_str_digest_7cceb741be998c6be0db098f025ad776;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_PTBYTE;
        tmp_dict_value_10 = const_str_digest_7cceb741be998c6be0db098f025ad776;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_11 = const_str_plain_PTCHAR;
        tmp_dict_value_11 = const_str_digest_7cceb741be998c6be0db098f025ad776;
        tmp_res = PyDict_SetItem( tmp_return_value, tmp_dict_key_11, tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_35cf3411ce9b7245b41126c939a811d8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_35cf3411ce9b7245b41126c939a811d8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_35cf3411ce9b7245b41126c939a811d8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_35cf3411ce9b7245b41126c939a811d8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_35cf3411ce9b7245b41126c939a811d8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_35cf3411ce9b7245b41126c939a811d8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_35cf3411ce9b7245b41126c939a811d8,
        type_description_1
    );


    // Release cached frame.
    if ( frame_35cf3411ce9b7245b41126c939a811d8 == cache_frame_35cf3411ce9b7245b41126c939a811d8 )
    {
        Py_DECREF( frame_35cf3411ce9b7245b41126c939a811d8 );
    }
    cache_frame_35cf3411ce9b7245b41126c939a811d8 = NULL;

    assertFrameObject( frame_35cf3411ce9b7245b41126c939a811d8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( cffi$commontypes$$$function_2_win_common_types );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_cffi$commontypes$$$function_1_resolve_common_type(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$commontypes$$$function_1_resolve_common_type,
        const_str_plain_resolve_common_type,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1f5960881092eed119ebf150a7fd2eac,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$commontypes,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_cffi$commontypes$$$function_2_win_common_types(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_cffi$commontypes$$$function_2_win_common_types,
        const_str_plain_win_common_types,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_35cf3411ce9b7245b41126c939a811d8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_cffi$commontypes,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_cffi$commontypes =
{
    PyModuleDef_HEAD_INIT,
    "cffi.commontypes",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(cffi$commontypes)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(cffi$commontypes)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_cffi$commontypes );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("cffi.commontypes: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("cffi.commontypes: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("cffi.commontypes: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initcffi$commontypes" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_cffi$commontypes = Py_InitModule4(
        "cffi.commontypes",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_cffi$commontypes = PyModule_Create( &mdef_cffi$commontypes );
#endif

    moduledict_cffi$commontypes = MODULE_DICT( module_cffi$commontypes );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_cffi$commontypes,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_cffi$commontypes,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_cffi$commontypes,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_cffi$commontypes,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_cffi$commontypes );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_2c9aaada0bc7f5027c3c847f30a4dc7c, module_cffi$commontypes );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_70b441863670e67e2d14c2f7a3e65be3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_70b441863670e67e2d14c2f7a3e65be3 = MAKE_MODULE_FRAME( codeobj_70b441863670e67e2d14c2f7a3e65be3, module_cffi$commontypes );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_70b441863670e67e2d14c2f7a3e65be3 );
    assert( Py_REFCNT( frame_70b441863670e67e2d14c2f7a3e65be3 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_sys;
        tmp_globals_name_1 = (PyObject *)moduledict_cffi$commontypes;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_70b441863670e67e2d14c2f7a3e65be3->m_frame.f_lineno = 1;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        assert( !(tmp_assign_source_4 == NULL) );
        UPDATE_STRING_DICT1( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_empty;
        tmp_globals_name_2 = (PyObject *)moduledict_cffi$commontypes;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_model_tuple;
        tmp_level_name_2 = const_int_pos_1;
        frame_70b441863670e67e2d14c2f7a3e65be3->m_frame.f_lineno = 2;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_5 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_cffi$commontypes,
                const_str_plain_model,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_model );
        }

        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_error;
        tmp_globals_name_3 = (PyObject *)moduledict_cffi$commontypes;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_FFIError_tuple;
        tmp_level_name_3 = const_int_pos_1;
        frame_70b441863670e67e2d14c2f7a3e65be3->m_frame.f_lineno = 3;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_2 ) )
        {
           tmp_assign_source_6 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_2,
                (PyObject *)moduledict_cffi$commontypes,
                const_str_plain_FFIError,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_FFIError );
        }

        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_FFIError, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = PyDict_New();
        UPDATE_STRING_DICT1( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES, tmp_assign_source_7 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain__cffi_backend;
        tmp_globals_name_4 = (PyObject *)moduledict_cffi$commontypes;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain__get_common_types_tuple;
        tmp_level_name_4 = const_int_0;
        frame_70b441863670e67e2d14c2f7a3e65be3->m_frame.f_lineno = 10;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain__get_common_types );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain__get_common_types, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain__get_common_types );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_common_types );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COMMON_TYPES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 11;

            goto try_except_handler_1;
        }

        tmp_args_element_name_1 = tmp_mvar_value_4;
        frame_70b441863670e67e2d14c2f7a3e65be3->m_frame.f_lineno = 11;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_70b441863670e67e2d14c2f7a3e65be3, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_70b441863670e67e2d14c2f7a3e65be3, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 8;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_70b441863670e67e2d14c2f7a3e65be3->m_frame) frame_70b441863670e67e2d14c2f7a3e65be3->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_2;
        branch_no_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( cffi$commontypes );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_ass_subscript_1;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_model );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "model" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 15;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_5;
        frame_70b441863670e67e2d14c2f7a3e65be3->m_frame.f_lineno = 15;
        tmp_ass_subvalue_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_unknown_type, &PyTuple_GET_ITEM( const_tuple_str_plain_FILE_str_plain__IO_FILE_tuple, 0 ) );

        if ( tmp_ass_subvalue_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_ass_subvalue_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COMMON_TYPES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 15;

            goto frame_exception_exit_1;
        }

        tmp_ass_subscribed_1 = tmp_mvar_value_6;
        tmp_ass_subscript_1 = const_str_plain_FILE;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        Py_DECREF( tmp_ass_subvalue_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_ass_subvalue_2;
        PyObject *tmp_ass_subscribed_2;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_ass_subscript_2;
        tmp_ass_subvalue_2 = const_str_plain__Bool;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COMMON_TYPES" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 16;

            goto frame_exception_exit_1;
        }

        tmp_ass_subscribed_2 = tmp_mvar_value_7;
        tmp_ass_subscript_2 = const_str_plain_bool;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_model );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_model );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "model" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 18;

            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_8;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_PrimitiveType );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ALL_PRIMITIVE_TYPES );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_9;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_10 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                exception_lineno = 18;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_11 = tmp_for_loop_1__iter_value;
        UPDATE_STRING_DICT0( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain__type, tmp_assign_source_11 );
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_call_result_2;
        int tmp_truth_name_1;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain__type );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__type );
        }

        CHECK_OBJECT( tmp_mvar_value_9 );
        tmp_called_instance_2 = tmp_mvar_value_9;
        frame_70b441863670e67e2d14c2f7a3e65be3->m_frame.f_lineno = 19;
        tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_str_plain__t_tuple, 0 ) );

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_3;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_2 );

            exception_lineno = 19;

            goto try_except_handler_3;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_ass_subvalue_3;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_ass_subscribed_3;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_ass_subscript_3;
            PyObject *tmp_mvar_value_12;
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain__type );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__type );
            }

            if ( tmp_mvar_value_10 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_type" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 20;

                goto try_except_handler_3;
            }

            tmp_ass_subvalue_3 = tmp_mvar_value_10;
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );

            if (unlikely( tmp_mvar_value_11 == NULL ))
            {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );
            }

            if ( tmp_mvar_value_11 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COMMON_TYPES" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 20;

                goto try_except_handler_3;
            }

            tmp_ass_subscribed_3 = tmp_mvar_value_11;
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain__type );

            if (unlikely( tmp_mvar_value_12 == NULL ))
            {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__type );
            }

            if ( tmp_mvar_value_12 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_type" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 20;

                goto try_except_handler_3;
            }

            tmp_ass_subscript_3 = tmp_mvar_value_12;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;

                goto try_except_handler_3;
            }
        }
        branch_no_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 18;

        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_res = PyDict_DelItem( (PyObject *)moduledict_cffi$commontypes, const_str_plain__type );
    tmp_result = tmp_res != -1;
    if ( tmp_result == false ) CLEAR_ERROR_OCCURRED();

    if ( tmp_result == false )
    {

        exception_type = PyExc_NameError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_type" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 21;

        goto frame_exception_exit_1;
    }

    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = PyDict_New();
        UPDATE_STRING_DICT1( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain__CACHE, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_cffi$commontypes$$$function_1_resolve_common_type(  );



        UPDATE_STRING_DICT1( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_resolve_common_type, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_cffi$commontypes$$$function_2_win_common_types(  );



        UPDATE_STRING_DICT1( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_win_common_types, tmp_assign_source_14 );
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_13;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;

            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_13;
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_platform );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_str_plain_win32;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT_NORECURSE( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_4;
            PyObject *tmp_mvar_value_14;
            PyObject *tmp_call_result_3;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_15;
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );

            if (unlikely( tmp_mvar_value_14 == NULL ))
            {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COMMON_TYPES );
            }

            if ( tmp_mvar_value_14 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COMMON_TYPES" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 80;

                goto frame_exception_exit_1;
            }

            tmp_source_name_4 = tmp_mvar_value_14;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_update );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 80;

                goto frame_exception_exit_1;
            }
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_cffi$commontypes, (Nuitka_StringObject *)const_str_plain_win_common_types );

            if (unlikely( tmp_mvar_value_15 == NULL ))
            {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_win_common_types );
            }

            if ( tmp_mvar_value_15 == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "win_common_types" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 80;

                goto frame_exception_exit_1;
            }

            tmp_called_name_3 = tmp_mvar_value_15;
            frame_70b441863670e67e2d14c2f7a3e65be3->m_frame.f_lineno = 80;
            tmp_args_element_name_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 80;

                goto frame_exception_exit_1;
            }
            frame_70b441863670e67e2d14c2f7a3e65be3->m_frame.f_lineno = 80;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 80;

                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_3 );
        }
        branch_no_3:;
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_70b441863670e67e2d14c2f7a3e65be3 );
#endif
    popFrameStack();

    assertFrameObject( frame_70b441863670e67e2d14c2f7a3e65be3 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_70b441863670e67e2d14c2f7a3e65be3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_70b441863670e67e2d14c2f7a3e65be3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_70b441863670e67e2d14c2f7a3e65be3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_70b441863670e67e2d14c2f7a3e65be3, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_cffi$commontypes );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
