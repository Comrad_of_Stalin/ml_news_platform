/* Generated code for Python module 'attr._funcs'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_attr$_funcs" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_attr$_funcs;
PyDictObject *moduledict_attr$_funcs;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_assoc;
extern PyObject *const_str_plain_warn;
extern PyObject *const_str_plain_NOTHING;
extern PyObject *const_str_plain_set;
extern PyObject *const_str_plain_cl;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_dict;
static PyObject *const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple;
static PyObject *const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple;
extern PyObject *const_str_plain_i;
extern PyObject *const_slice_int_pos_1_none_none;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_plain_cls_tuple;
extern PyObject *const_str_plain_asdict;
extern PyObject *const_str_plain_astuple;
extern PyObject *const_str_plain_a;
extern PyObject *const_str_plain_division;
extern PyObject *const_str_plain_rv;
extern PyObject *const_str_plain_evolve;
extern PyObject *const_dict_f154c9a58c9419d7e391901d7b7fe49e;
static PyObject *const_str_plain_dict_factory;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_634a33e8654ffef560e62de1d34622e3;
extern PyObject *const_str_plain_filter;
static PyObject *const_str_digest_0cf0b36801d2e126e0c369f2885f747d;
extern PyObject *const_str_plain_cls;
extern PyObject *const_str_plain_attr_name;
static PyObject *const_tuple_8d09e7f0172198178b3c33c2fe1216ad_tuple;
static PyObject *const_tuple_str_plain_AttrsAttributeNotFoundError_tuple;
static PyObject *const_str_digest_e5dfe7e5fac48bd4bcde33eaf5a83dad;
extern PyObject *const_str_plain_absolute_import;
extern PyObject *const_str_plain___doc__;
static PyObject *const_tuple_9c32b305293d788de15e00786d911041_tuple;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_attrs;
extern PyObject *const_str_plain_cf;
extern PyObject *const_tuple_str_plain_iteritems_tuple;
extern PyObject *const_str_plain_warnings;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain_j;
static PyObject *const_str_digest_64e4ff9f09b44528b09bbe58cbebfc7a;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_DeprecationWarning;
extern PyObject *const_str_plain_iteritems;
extern PyObject *const_str_plain_init;
extern PyObject *const_str_plain_recurse;
extern PyObject *const_str_plain_has;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_append;
static PyObject *const_str_digest_8e2c97f4f26f8b58fe61145ca0cacfef;
extern PyObject *const_str_plain_new;
extern PyObject *const_str_plain_name;
static PyObject *const_str_plain__asdict_anything;
extern PyObject *const_str_plain___attrs_attrs__;
extern PyObject *const_str_plain_exceptions;
static PyObject *const_str_digest_0868412c7596b0aba8b59b68b966a79c;
extern PyObject *const_str_plain_stacklevel;
static PyObject *const_tuple_type_tuple_type_list_type_set_tuple;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_k;
static PyObject *const_str_digest_e77bb25df3fe1ff897e0dd855d2a2eb1;
static PyObject *const_tuple_895cc7c4f6a85e1967e2ccd73bec664d_tuple;
extern PyObject *const_str_plain_list;
extern PyObject *const_str_plain_df;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_val;
static PyObject *const_str_plain_init_name;
extern PyObject *const_str_plain_changes;
extern PyObject *const_str_plain__make;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain__compat;
static PyObject *const_str_plain_kk;
extern PyObject *const_str_angle_listcomp;
static PyObject *const_tuple_str_plain_NOTHING_str_plain__obj_setattr_str_plain_fields_tuple;
static PyObject *const_tuple_6fdaec00a661f1d0b18610153ca4364b_tuple;
static PyObject *const_tuple_158521dbe0e6b3d1f0584a4ce3940301_tuple;
extern PyObject *const_str_plain_vv;
static PyObject *const_str_digest_2d81ff510cc484c9f4116610439f0620;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_v;
extern PyObject *const_str_plain_print_function;
extern PyObject *const_str_plain__;
static PyObject *const_tuple_true_none_type_dict_false_tuple;
static PyObject *const_str_plain_tuple_factory;
extern PyObject *const_str_plain__obj_setattr;
static PyObject *const_str_plain_retain;
static PyObject *const_str_digest_6419a49fb159320db882dce19dfd2cd9;
extern PyObject *const_int_pos_1;
static PyObject *const_str_plain_retain_collection_types;
extern PyObject *const_str_plain_tuple;
extern PyObject *const_str_plain_copy;
static PyObject *const_tuple_true_none_type_tuple_false_tuple;
static PyObject *const_tuple_41448c61929f920e4be48551f6ff8763_tuple;
extern PyObject *const_str_plain_fields;
static PyObject *const_str_digest_98b8f42d0cc096bf55b9067d47c79e4c;
extern PyObject *const_str_plain_inst;
static PyObject *const_str_digest_ad9bccaaeffd83eec2914cae2e13b5b8;
static PyObject *const_str_digest_d6b568f7768e3849583ee28edbc76902;
extern PyObject *const_str_plain_AttrsAttributeNotFoundError;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_bf193d080c27b70d5950ee1f3b1ee064;
static PyObject *const_str_digest_f5cf11b1cb8b0d9853e12ab86322a6b3;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_format;
static PyObject *const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 0, const_str_plain_inst ); Py_INCREF( const_str_plain_inst );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 1, const_str_plain_recurse ); Py_INCREF( const_str_plain_recurse );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 2, const_str_plain_filter ); Py_INCREF( const_str_plain_filter );
    const_str_plain_dict_factory = UNSTREAM_STRING_ASCII( &constant_bin[ 114976 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 3, const_str_plain_dict_factory ); Py_INCREF( const_str_plain_dict_factory );
    const_str_plain_retain_collection_types = UNSTREAM_STRING_ASCII( &constant_bin[ 114988 ], 23, 1 );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 4, const_str_plain_retain_collection_types ); Py_INCREF( const_str_plain_retain_collection_types );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 5, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 6, const_str_plain_rv ); Py_INCREF( const_str_plain_rv );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 7, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 8, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 9, const_str_plain_cf ); Py_INCREF( const_str_plain_cf );
    PyTuple_SET_ITEM( const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 10, const_str_plain_df ); Py_INCREF( const_str_plain_df );
    const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 0, const_str_plain_inst ); Py_INCREF( const_str_plain_inst );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 1, const_str_plain_recurse ); Py_INCREF( const_str_plain_recurse );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 2, const_str_plain_filter ); Py_INCREF( const_str_plain_filter );
    const_str_plain_tuple_factory = UNSTREAM_STRING_ASCII( &constant_bin[ 115011 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 3, const_str_plain_tuple_factory ); Py_INCREF( const_str_plain_tuple_factory );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 4, const_str_plain_retain_collection_types ); Py_INCREF( const_str_plain_retain_collection_types );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 5, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 6, const_str_plain_rv ); Py_INCREF( const_str_plain_rv );
    const_str_plain_retain = UNSTREAM_STRING_ASCII( &constant_bin[ 46425 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 7, const_str_plain_retain ); Py_INCREF( const_str_plain_retain );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 8, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 9, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 10, const_str_plain_cf ); Py_INCREF( const_str_plain_cf );
    PyTuple_SET_ITEM( const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 11, const_str_plain_df ); Py_INCREF( const_str_plain_df );
    const_str_digest_634a33e8654ffef560e62de1d34622e3 = UNSTREAM_STRING_ASCII( &constant_bin[ 115024 ], 1080, 0 );
    const_str_digest_0cf0b36801d2e126e0c369f2885f747d = UNSTREAM_STRING_ASCII( &constant_bin[ 116104 ], 11, 0 );
    const_tuple_8d09e7f0172198178b3c33c2fe1216ad_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_8d09e7f0172198178b3c33c2fe1216ad_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    const_str_plain_kk = UNSTREAM_STRING_ASCII( &constant_bin[ 57096 ], 2, 1 );
    PyTuple_SET_ITEM( const_tuple_8d09e7f0172198178b3c33c2fe1216ad_tuple, 1, const_str_plain_kk ); Py_INCREF( const_str_plain_kk );
    PyTuple_SET_ITEM( const_tuple_8d09e7f0172198178b3c33c2fe1216ad_tuple, 2, const_str_plain_vv ); Py_INCREF( const_str_plain_vv );
    PyTuple_SET_ITEM( const_tuple_8d09e7f0172198178b3c33c2fe1216ad_tuple, 3, const_str_plain_filter ); Py_INCREF( const_str_plain_filter );
    PyTuple_SET_ITEM( const_tuple_8d09e7f0172198178b3c33c2fe1216ad_tuple, 4, const_str_plain_df ); Py_INCREF( const_str_plain_df );
    PyTuple_SET_ITEM( const_tuple_8d09e7f0172198178b3c33c2fe1216ad_tuple, 5, const_str_plain_retain_collection_types ); Py_INCREF( const_str_plain_retain_collection_types );
    const_tuple_str_plain_AttrsAttributeNotFoundError_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_AttrsAttributeNotFoundError_tuple, 0, const_str_plain_AttrsAttributeNotFoundError ); Py_INCREF( const_str_plain_AttrsAttributeNotFoundError );
    const_str_digest_e5dfe7e5fac48bd4bcde33eaf5a83dad = UNSTREAM_STRING_ASCII( &constant_bin[ 116115 ], 184, 0 );
    const_tuple_9c32b305293d788de15e00786d911041_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_9c32b305293d788de15e00786d911041_tuple, 0, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_9c32b305293d788de15e00786d911041_tuple, 1, const_str_plain_filter ); Py_INCREF( const_str_plain_filter );
    PyTuple_SET_ITEM( const_tuple_9c32b305293d788de15e00786d911041_tuple, 2, const_str_plain_dict_factory ); Py_INCREF( const_str_plain_dict_factory );
    PyTuple_SET_ITEM( const_tuple_9c32b305293d788de15e00786d911041_tuple, 3, const_str_plain_retain_collection_types ); Py_INCREF( const_str_plain_retain_collection_types );
    const_str_digest_64e4ff9f09b44528b09bbe58cbebfc7a = UNSTREAM_STRING_ASCII( &constant_bin[ 116299 ], 75, 0 );
    const_str_digest_8e2c97f4f26f8b58fe61145ca0cacfef = UNSTREAM_STRING_ASCII( &constant_bin[ 116374 ], 54, 0 );
    const_str_plain__asdict_anything = UNSTREAM_STRING_ASCII( &constant_bin[ 116428 ], 16, 1 );
    const_str_digest_0868412c7596b0aba8b59b68b966a79c = UNSTREAM_STRING_ASCII( &constant_bin[ 116444 ], 38, 0 );
    const_tuple_type_tuple_type_list_type_set_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_type_tuple_type_list_type_set_tuple, 0, (PyObject *)&PyTuple_Type ); Py_INCREF( (PyObject *)&PyTuple_Type );
    PyTuple_SET_ITEM( const_tuple_type_tuple_type_list_type_set_tuple, 1, (PyObject *)&PyList_Type ); Py_INCREF( (PyObject *)&PyList_Type );
    PyTuple_SET_ITEM( const_tuple_type_tuple_type_list_type_set_tuple, 2, (PyObject *)&PySet_Type ); Py_INCREF( (PyObject *)&PySet_Type );
    const_str_digest_e77bb25df3fe1ff897e0dd855d2a2eb1 = UNSTREAM_STRING_ASCII( &constant_bin[ 116482 ], 463, 0 );
    const_tuple_895cc7c4f6a85e1967e2ccd73bec664d_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_895cc7c4f6a85e1967e2ccd73bec664d_tuple, 0, const_str_plain_inst ); Py_INCREF( const_str_plain_inst );
    PyTuple_SET_ITEM( const_tuple_895cc7c4f6a85e1967e2ccd73bec664d_tuple, 1, const_str_plain_changes ); Py_INCREF( const_str_plain_changes );
    PyTuple_SET_ITEM( const_tuple_895cc7c4f6a85e1967e2ccd73bec664d_tuple, 2, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_895cc7c4f6a85e1967e2ccd73bec664d_tuple, 3, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    PyTuple_SET_ITEM( const_tuple_895cc7c4f6a85e1967e2ccd73bec664d_tuple, 4, const_str_plain_a ); Py_INCREF( const_str_plain_a );
    PyTuple_SET_ITEM( const_tuple_895cc7c4f6a85e1967e2ccd73bec664d_tuple, 5, const_str_plain_attr_name ); Py_INCREF( const_str_plain_attr_name );
    const_str_plain_init_name = UNSTREAM_STRING_ASCII( &constant_bin[ 116945 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_895cc7c4f6a85e1967e2ccd73bec664d_tuple, 6, const_str_plain_init_name ); Py_INCREF( const_str_plain_init_name );
    const_tuple_str_plain_NOTHING_str_plain__obj_setattr_str_plain_fields_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_NOTHING_str_plain__obj_setattr_str_plain_fields_tuple, 0, const_str_plain_NOTHING ); Py_INCREF( const_str_plain_NOTHING );
    PyTuple_SET_ITEM( const_tuple_str_plain_NOTHING_str_plain__obj_setattr_str_plain_fields_tuple, 1, const_str_plain__obj_setattr ); Py_INCREF( const_str_plain__obj_setattr );
    PyTuple_SET_ITEM( const_tuple_str_plain_NOTHING_str_plain__obj_setattr_str_plain_fields_tuple, 2, const_str_plain_fields ); Py_INCREF( const_str_plain_fields );
    const_tuple_6fdaec00a661f1d0b18610153ca4364b_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_6fdaec00a661f1d0b18610153ca4364b_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_6fdaec00a661f1d0b18610153ca4364b_tuple, 1, const_str_plain_kk ); Py_INCREF( const_str_plain_kk );
    PyTuple_SET_ITEM( const_tuple_6fdaec00a661f1d0b18610153ca4364b_tuple, 2, const_str_plain_vv ); Py_INCREF( const_str_plain_vv );
    PyTuple_SET_ITEM( const_tuple_6fdaec00a661f1d0b18610153ca4364b_tuple, 3, const_str_plain_tuple_factory ); Py_INCREF( const_str_plain_tuple_factory );
    PyTuple_SET_ITEM( const_tuple_6fdaec00a661f1d0b18610153ca4364b_tuple, 4, const_str_plain_retain ); Py_INCREF( const_str_plain_retain );
    const_tuple_158521dbe0e6b3d1f0584a4ce3940301_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_158521dbe0e6b3d1f0584a4ce3940301_tuple, 0, const_str_plain_j ); Py_INCREF( const_str_plain_j );
    PyTuple_SET_ITEM( const_tuple_158521dbe0e6b3d1f0584a4ce3940301_tuple, 1, const_str_plain_filter ); Py_INCREF( const_str_plain_filter );
    PyTuple_SET_ITEM( const_tuple_158521dbe0e6b3d1f0584a4ce3940301_tuple, 2, const_str_plain_tuple_factory ); Py_INCREF( const_str_plain_tuple_factory );
    PyTuple_SET_ITEM( const_tuple_158521dbe0e6b3d1f0584a4ce3940301_tuple, 3, const_str_plain_retain ); Py_INCREF( const_str_plain_retain );
    const_str_digest_2d81ff510cc484c9f4116610439f0620 = UNSTREAM_STRING_ASCII( &constant_bin[ 116954 ], 484, 0 );
    const_tuple_true_none_type_dict_false_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_true_none_type_dict_false_tuple, 0, Py_True ); Py_INCREF( Py_True );
    PyTuple_SET_ITEM( const_tuple_true_none_type_dict_false_tuple, 1, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_true_none_type_dict_false_tuple, 2, (PyObject *)&PyDict_Type ); Py_INCREF( (PyObject *)&PyDict_Type );
    PyTuple_SET_ITEM( const_tuple_true_none_type_dict_false_tuple, 3, Py_False ); Py_INCREF( Py_False );
    const_str_digest_6419a49fb159320db882dce19dfd2cd9 = UNSTREAM_STRING_ASCII( &constant_bin[ 117438 ], 26, 0 );
    const_tuple_true_none_type_tuple_false_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_true_none_type_tuple_false_tuple, 0, Py_True ); Py_INCREF( Py_True );
    PyTuple_SET_ITEM( const_tuple_true_none_type_tuple_false_tuple, 1, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_true_none_type_tuple_false_tuple, 2, (PyObject *)&PyTuple_Type ); Py_INCREF( (PyObject *)&PyTuple_Type );
    PyTuple_SET_ITEM( const_tuple_true_none_type_tuple_false_tuple, 3, Py_False ); Py_INCREF( Py_False );
    const_tuple_41448c61929f920e4be48551f6ff8763_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_41448c61929f920e4be48551f6ff8763_tuple, 0, const_str_plain_val ); Py_INCREF( const_str_plain_val );
    PyTuple_SET_ITEM( const_tuple_41448c61929f920e4be48551f6ff8763_tuple, 1, const_str_plain_filter ); Py_INCREF( const_str_plain_filter );
    PyTuple_SET_ITEM( const_tuple_41448c61929f920e4be48551f6ff8763_tuple, 2, const_str_plain_dict_factory ); Py_INCREF( const_str_plain_dict_factory );
    PyTuple_SET_ITEM( const_tuple_41448c61929f920e4be48551f6ff8763_tuple, 3, const_str_plain_retain_collection_types ); Py_INCREF( const_str_plain_retain_collection_types );
    PyTuple_SET_ITEM( const_tuple_41448c61929f920e4be48551f6ff8763_tuple, 4, const_str_plain_rv ); Py_INCREF( const_str_plain_rv );
    PyTuple_SET_ITEM( const_tuple_41448c61929f920e4be48551f6ff8763_tuple, 5, const_str_plain_cf ); Py_INCREF( const_str_plain_cf );
    PyTuple_SET_ITEM( const_tuple_41448c61929f920e4be48551f6ff8763_tuple, 6, const_str_plain_df ); Py_INCREF( const_str_plain_df );
    const_str_digest_98b8f42d0cc096bf55b9067d47c79e4c = UNSTREAM_STRING_ASCII( &constant_bin[ 117464 ], 1204, 0 );
    const_str_digest_ad9bccaaeffd83eec2914cae2e13b5b8 = UNSTREAM_STRING_ASCII( &constant_bin[ 118668 ], 14, 0 );
    const_str_digest_d6b568f7768e3849583ee28edbc76902 = UNSTREAM_STRING_ASCII( &constant_bin[ 118682 ], 25, 0 );
    const_str_digest_bf193d080c27b70d5950ee1f3b1ee064 = UNSTREAM_STRING_ASCII( &constant_bin[ 118707 ], 35, 0 );
    const_str_digest_f5cf11b1cb8b0d9853e12ab86322a6b3 = UNSTREAM_STRING_ASCII( &constant_bin[ 118742 ], 20, 0 );
    const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple, 0, const_str_plain_inst ); Py_INCREF( const_str_plain_inst );
    PyTuple_SET_ITEM( const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple, 1, const_str_plain_changes ); Py_INCREF( const_str_plain_changes );
    PyTuple_SET_ITEM( const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple, 2, const_str_plain_warnings ); Py_INCREF( const_str_plain_warnings );
    PyTuple_SET_ITEM( const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple, 3, const_str_plain_new ); Py_INCREF( const_str_plain_new );
    PyTuple_SET_ITEM( const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple, 4, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    PyTuple_SET_ITEM( const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple, 5, const_str_plain_k ); Py_INCREF( const_str_plain_k );
    PyTuple_SET_ITEM( const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple, 6, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    PyTuple_SET_ITEM( const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple, 7, const_str_plain_a ); Py_INCREF( const_str_plain_a );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_attr$_funcs( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_5619416fb724fbf3cd31b2a368569f82;
static PyCodeObject *codeobj_7b6fa8a333d1c91cb053395857592df1;
static PyCodeObject *codeobj_4b60d7e3227fc1c91079c373953e6021;
static PyCodeObject *codeobj_f82ffd4999ed656b8e7a64ff137d53c6;
static PyCodeObject *codeobj_d1dc5195ebe0ee1ddf532d3d9c328ca1;
static PyCodeObject *codeobj_e1f1293b3a5624800f12024e8955cc06;
static PyCodeObject *codeobj_c4106b694f9a60e8b66fc9818555f8e6;
static PyCodeObject *codeobj_980029849d5b392ef4a7e2137a31a6c9;
static PyCodeObject *codeobj_971fec919b0d12e93ae402a49ce447a9;
static PyCodeObject *codeobj_578fc6341ba7c2a7a428194177901142;
static PyCodeObject *codeobj_7d0311866830c038650d360f68da8fd3;
static PyCodeObject *codeobj_a46b62abc6e63351549890145355cab5;
static PyCodeObject *codeobj_0ef64ddd35fb82e9e752d760c7c29fc2;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_ad9bccaaeffd83eec2914cae2e13b5b8 );
    codeobj_5619416fb724fbf3cd31b2a368569f82 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 68, const_tuple_8d09e7f0172198178b3c33c2fe1216ad_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7b6fa8a333d1c91cb053395857592df1 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 105, const_tuple_8d09e7f0172198178b3c33c2fe1216ad_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4b60d7e3227fc1c91079c373953e6021 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 189, const_tuple_6fdaec00a661f1d0b18610153ca4364b_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f82ffd4999ed656b8e7a64ff137d53c6 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 59, const_tuple_9c32b305293d788de15e00786d911041_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d1dc5195ebe0ee1ddf532d3d9c328ca1 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 96, const_tuple_9c32b305293d788de15e00786d911041_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e1f1293b3a5624800f12024e8955cc06 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 172, const_tuple_158521dbe0e6b3d1f0584a4ce3940301_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c4106b694f9a60e8b66fc9818555f8e6 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_f5cf11b1cb8b0d9853e12ab86322a6b3, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_980029849d5b392ef4a7e2137a31a6c9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__asdict_anything, 85, const_tuple_41448c61929f920e4be48551f6ff8763_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_971fec919b0d12e93ae402a49ce447a9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_asdict, 10, const_tuple_02272d2fad32df4d9d965927a1bb73a7_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_578fc6341ba7c2a7a428194177901142 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_assoc, 227, const_tuple_a9ea09764f42e0e1d1b2a53430fcc3c9_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_7d0311866830c038650d360f68da8fd3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_astuple, 116, const_tuple_0e8ae63b960389a4645ff5071c30f872_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a46b62abc6e63351549890145355cab5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_evolve, 265, const_tuple_895cc7c4f6a85e1967e2ccd73bec664d_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_0ef64ddd35fb82e9e752d760c7c29fc2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_has, 215, const_tuple_str_plain_cls_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *attr$_funcs$$$function_1_asdict$$$genexpr_1_genexpr_maker( void );


static PyObject *attr$_funcs$$$function_2__asdict_anything$$$genexpr_1_genexpr_maker( void );


static PyObject *attr$_funcs$$$function_3_astuple$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_2_complex_call_helper_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_1_asdict( PyObject *defaults );


static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_2__asdict_anything(  );


static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_3_astuple( PyObject *defaults );


static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_4_has(  );


static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_5_assoc(  );


static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_6_evolve(  );


// The module function definitions.
static PyObject *impl_attr$_funcs$$$function_1_asdict( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_inst = python_pars[ 0 ];
    PyObject *par_recurse = python_pars[ 1 ];
    struct Nuitka_CellObject *par_filter = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *par_dict_factory = python_pars[ 3 ];
    struct Nuitka_CellObject *par_retain_collection_types = PyCell_NEW1( python_pars[ 4 ] );
    PyObject *var_attrs = NULL;
    PyObject *var_rv = NULL;
    PyObject *var_a = NULL;
    PyObject *var_v = NULL;
    PyObject *var_cf = NULL;
    struct Nuitka_CellObject *var_df = PyCell_EMPTY();
    PyObject *outline_0_var_i = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_971fec919b0d12e93ae402a49ce447a9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    struct Nuitka_FrameObject *frame_f82ffd4999ed656b8e7a64ff137d53c6_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_f82ffd4999ed656b8e7a64ff137d53c6_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_971fec919b0d12e93ae402a49ce447a9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_971fec919b0d12e93ae402a49ce447a9, codeobj_971fec919b0d12e93ae402a49ce447a9, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_971fec919b0d12e93ae402a49ce447a9 = cache_frame_971fec919b0d12e93ae402a49ce447a9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_971fec919b0d12e93ae402a49ce447a9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_971fec919b0d12e93ae402a49ce447a9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_fields );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fields );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fields" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 44;
            type_description_1 = "oococoooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_inst );
        tmp_source_name_1 = par_inst;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oococoooooc";
            goto frame_exception_exit_1;
        }
        frame_971fec919b0d12e93ae402a49ce447a9->m_frame.f_lineno = 44;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;
            type_description_1 = "oococoooooc";
            goto frame_exception_exit_1;
        }
        assert( var_attrs == NULL );
        var_attrs = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        CHECK_OBJECT( par_dict_factory );
        tmp_called_name_2 = par_dict_factory;
        frame_971fec919b0d12e93ae402a49ce447a9->m_frame.f_lineno = 45;
        tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "oococoooooc";
            goto frame_exception_exit_1;
        }
        assert( var_rv == NULL );
        var_rv = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( var_attrs );
        tmp_iter_arg_1 = var_attrs;
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "oococoooooc";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oococoooooc";
                exception_lineno = 46;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_a;
            var_a = tmp_assign_source_5;
            Py_INCREF( var_a );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_inst );
        tmp_getattr_target_1 = par_inst;
        CHECK_OBJECT( var_a );
        tmp_source_name_2 = var_a;
        tmp_getattr_attr_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
        if ( tmp_getattr_attr_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oococoooooc";
            goto try_except_handler_2;
        }
        tmp_assign_source_6 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        Py_DECREF( tmp_getattr_attr_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oococoooooc";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_v;
            var_v = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( PyCell_GET( par_filter ) );
        tmp_compexpr_left_1 = PyCell_GET( par_filter );
        tmp_compexpr_right_1 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( PyCell_GET( par_filter ) );
        tmp_called_name_3 = PyCell_GET( par_filter );
        CHECK_OBJECT( var_a );
        tmp_args_element_name_2 = var_a;
        CHECK_OBJECT( var_v );
        tmp_args_element_name_3 = var_v;
        frame_971fec919b0d12e93ae402a49ce447a9->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oococoooooc";
            goto try_except_handler_2;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oococoooooc";
            goto try_except_handler_2;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_start_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_recurse );
        tmp_compexpr_left_2 = par_recurse;
        tmp_compexpr_right_2 = Py_True;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_called_name_4;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_source_name_3;
            int tmp_truth_name_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_has );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 51;
                type_description_1 = "oococoooooc";
                goto try_except_handler_2;
            }

            tmp_called_name_4 = tmp_mvar_value_2;
            CHECK_OBJECT( var_v );
            tmp_source_name_3 = var_v;
            tmp_args_element_name_4 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_3 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 51;
                type_description_1 = "oococoooooc";
                goto try_except_handler_2;
            }
            frame_971fec919b0d12e93ae402a49ce447a9->m_frame.f_lineno = 51;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 51;
                type_description_1 = "oococoooooc";
                goto try_except_handler_2;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_call_result_1 );

                exception_lineno = 51;
                type_description_1 = "oococoooooc";
                goto try_except_handler_2;
            }
            tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_1 );
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_ass_subvalue_1;
                PyObject *tmp_called_name_5;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_args_element_name_5;
                PyObject *tmp_args_element_name_6;
                PyObject *tmp_args_element_name_7;
                PyObject *tmp_args_element_name_8;
                PyObject *tmp_args_element_name_9;
                PyObject *tmp_ass_subscribed_1;
                PyObject *tmp_ass_subscript_1;
                PyObject *tmp_source_name_4;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_asdict );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asdict );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asdict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 52;
                    type_description_1 = "oococoooooc";
                    goto try_except_handler_2;
                }

                tmp_called_name_5 = tmp_mvar_value_3;
                CHECK_OBJECT( var_v );
                tmp_args_element_name_5 = var_v;
                tmp_args_element_name_6 = Py_True;
                CHECK_OBJECT( PyCell_GET( par_filter ) );
                tmp_args_element_name_7 = PyCell_GET( par_filter );
                CHECK_OBJECT( par_dict_factory );
                tmp_args_element_name_8 = par_dict_factory;
                CHECK_OBJECT( PyCell_GET( par_retain_collection_types ) );
                tmp_args_element_name_9 = PyCell_GET( par_retain_collection_types );
                frame_971fec919b0d12e93ae402a49ce447a9->m_frame.f_lineno = 52;
                {
                    PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
                    tmp_ass_subvalue_1 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_5, call_args );
                }

                if ( tmp_ass_subvalue_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 52;
                    type_description_1 = "oococoooooc";
                    goto try_except_handler_2;
                }
                CHECK_OBJECT( var_rv );
                tmp_ass_subscribed_1 = var_rv;
                CHECK_OBJECT( var_a );
                tmp_source_name_4 = var_a;
                tmp_ass_subscript_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_name );
                if ( tmp_ass_subscript_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_ass_subvalue_1 );

                    exception_lineno = 52;
                    type_description_1 = "oococoooooc";
                    goto try_except_handler_2;
                }
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
                Py_DECREF( tmp_ass_subscript_1 );
                Py_DECREF( tmp_ass_subvalue_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 52;
                    type_description_1 = "oococoooooc";
                    goto try_except_handler_2;
                }
            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_isinstance_inst_1;
                PyObject *tmp_isinstance_cls_1;
                CHECK_OBJECT( var_v );
                tmp_isinstance_inst_1 = var_v;
                tmp_isinstance_cls_1 = const_tuple_type_tuple_type_list_type_set_tuple;
                tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 55;
                    type_description_1 = "oococoooooc";
                    goto try_except_handler_2;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_7;
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_3;
                    PyObject *tmp_compexpr_right_3;
                    PyObject *tmp_source_name_5;
                    CHECK_OBJECT( PyCell_GET( par_retain_collection_types ) );
                    tmp_compexpr_left_3 = PyCell_GET( par_retain_collection_types );
                    tmp_compexpr_right_3 = Py_True;
                    tmp_condition_result_5 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto condexpr_true_1;
                    }
                    else
                    {
                        goto condexpr_false_1;
                    }
                    condexpr_true_1:;
                    CHECK_OBJECT( var_v );
                    tmp_source_name_5 = var_v;
                    tmp_assign_source_7 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_5 );
                    if ( tmp_assign_source_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 56;
                        type_description_1 = "oococoooooc";
                        goto try_except_handler_2;
                    }
                    goto condexpr_end_1;
                    condexpr_false_1:;
                    tmp_assign_source_7 = (PyObject *)&PyList_Type;
                    Py_INCREF( tmp_assign_source_7 );
                    condexpr_end_1:;
                    {
                        PyObject *old = var_cf;
                        var_cf = tmp_assign_source_7;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_ass_subvalue_2;
                    PyObject *tmp_called_name_6;
                    PyObject *tmp_args_element_name_10;
                    PyObject *tmp_ass_subscribed_2;
                    PyObject *tmp_ass_subscript_2;
                    PyObject *tmp_source_name_6;
                    CHECK_OBJECT( var_cf );
                    tmp_called_name_6 = var_cf;
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_8;
                        PyObject *tmp_iter_arg_2;
                        CHECK_OBJECT( var_v );
                        tmp_iter_arg_2 = var_v;
                        tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_2 );
                        if ( tmp_assign_source_8 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 59;
                            type_description_1 = "oococoooooc";
                            goto try_except_handler_3;
                        }
                        {
                            PyObject *old = tmp_listcomp_1__$0;
                            tmp_listcomp_1__$0 = tmp_assign_source_8;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_9;
                        tmp_assign_source_9 = PyList_New( 0 );
                        {
                            PyObject *old = tmp_listcomp_1__contraction;
                            tmp_listcomp_1__contraction = tmp_assign_source_9;
                            Py_XDECREF( old );
                        }

                    }
                    MAKE_OR_REUSE_FRAME( cache_frame_f82ffd4999ed656b8e7a64ff137d53c6_2, codeobj_f82ffd4999ed656b8e7a64ff137d53c6, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
                    frame_f82ffd4999ed656b8e7a64ff137d53c6_2 = cache_frame_f82ffd4999ed656b8e7a64ff137d53c6_2;

                    // Push the new frame as the currently active one.
                    pushFrameStack( frame_f82ffd4999ed656b8e7a64ff137d53c6_2 );

                    // Mark the frame object as in use, ref count 1 will be up for reuse.
                    assert( Py_REFCNT( frame_f82ffd4999ed656b8e7a64ff137d53c6_2 ) == 2 ); // Frame stack

                    // Framed code:
                    // Tried code:
                    loop_start_2:;
                    {
                        PyObject *tmp_next_source_2;
                        PyObject *tmp_assign_source_10;
                        CHECK_OBJECT( tmp_listcomp_1__$0 );
                        tmp_next_source_2 = tmp_listcomp_1__$0;
                        tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_2 );
                        if ( tmp_assign_source_10 == NULL )
                        {
                            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                            {

                                goto loop_end_2;
                            }
                            else
                            {

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                type_description_2 = "ococ";
                                exception_lineno = 59;
                                goto try_except_handler_4;
                            }
                        }

                        {
                            PyObject *old = tmp_listcomp_1__iter_value_0;
                            tmp_listcomp_1__iter_value_0 = tmp_assign_source_10;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_11;
                        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                        tmp_assign_source_11 = tmp_listcomp_1__iter_value_0;
                        {
                            PyObject *old = outline_0_var_i;
                            outline_0_var_i = tmp_assign_source_11;
                            Py_INCREF( outline_0_var_i );
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_append_list_1;
                        PyObject *tmp_append_value_1;
                        PyObject *tmp_called_name_7;
                        PyObject *tmp_mvar_value_4;
                        PyObject *tmp_args_element_name_11;
                        PyObject *tmp_args_element_name_12;
                        PyObject *tmp_args_element_name_13;
                        PyObject *tmp_args_element_name_14;
                        CHECK_OBJECT( tmp_listcomp_1__contraction );
                        tmp_append_list_1 = tmp_listcomp_1__contraction;
                        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain__asdict_anything );

                        if (unlikely( tmp_mvar_value_4 == NULL ))
                        {
                            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__asdict_anything );
                        }

                        if ( tmp_mvar_value_4 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_asdict_anything" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 59;
                            type_description_2 = "ococ";
                            goto try_except_handler_4;
                        }

                        tmp_called_name_7 = tmp_mvar_value_4;
                        CHECK_OBJECT( outline_0_var_i );
                        tmp_args_element_name_11 = outline_0_var_i;
                        CHECK_OBJECT( PyCell_GET( par_filter ) );
                        tmp_args_element_name_12 = PyCell_GET( par_filter );
                        CHECK_OBJECT( par_dict_factory );
                        tmp_args_element_name_13 = par_dict_factory;
                        CHECK_OBJECT( PyCell_GET( par_retain_collection_types ) );
                        tmp_args_element_name_14 = PyCell_GET( par_retain_collection_types );
                        frame_f82ffd4999ed656b8e7a64ff137d53c6_2->m_frame.f_lineno = 59;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13, tmp_args_element_name_14 };
                            tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_7, call_args );
                        }

                        if ( tmp_append_value_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 59;
                            type_description_2 = "ococ";
                            goto try_except_handler_4;
                        }
                        assert( PyList_Check( tmp_append_list_1 ) );
                        tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                        Py_DECREF( tmp_append_value_1 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 59;
                            type_description_2 = "ococ";
                            goto try_except_handler_4;
                        }
                    }
                    if ( CONSIDER_THREADING() == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 59;
                        type_description_2 = "ococ";
                        goto try_except_handler_4;
                    }
                    goto loop_start_2;
                    loop_end_2:;
                    CHECK_OBJECT( tmp_listcomp_1__contraction );
                    tmp_args_element_name_10 = tmp_listcomp_1__contraction;
                    Py_INCREF( tmp_args_element_name_10 );
                    goto try_return_handler_4;
                    // tried codes exits in all cases
                    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_1_asdict );
                    return NULL;
                    // Return handler code:
                    try_return_handler_4:;
                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                    Py_DECREF( tmp_listcomp_1__$0 );
                    tmp_listcomp_1__$0 = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                    Py_DECREF( tmp_listcomp_1__contraction );
                    tmp_listcomp_1__contraction = NULL;

                    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                    tmp_listcomp_1__iter_value_0 = NULL;

                    goto frame_return_exit_1;
                    // Exception handler code:
                    try_except_handler_4:;
                    exception_keeper_type_1 = exception_type;
                    exception_keeper_value_1 = exception_value;
                    exception_keeper_tb_1 = exception_tb;
                    exception_keeper_lineno_1 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                    Py_DECREF( tmp_listcomp_1__$0 );
                    tmp_listcomp_1__$0 = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                    Py_DECREF( tmp_listcomp_1__contraction );
                    tmp_listcomp_1__contraction = NULL;

                    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                    tmp_listcomp_1__iter_value_0 = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_1;
                    exception_value = exception_keeper_value_1;
                    exception_tb = exception_keeper_tb_1;
                    exception_lineno = exception_keeper_lineno_1;

                    goto frame_exception_exit_2;
                    // End of try:

#if 0
                    RESTORE_FRAME_EXCEPTION( frame_f82ffd4999ed656b8e7a64ff137d53c6_2 );
#endif

                    // Put the previous frame back on top.
                    popFrameStack();

                    goto frame_no_exception_1;

                    frame_return_exit_1:;
#if 0
                    RESTORE_FRAME_EXCEPTION( frame_f82ffd4999ed656b8e7a64ff137d53c6_2 );
#endif

                    // Put the previous frame back on top.
                    popFrameStack();

                    goto try_return_handler_3;

                    frame_exception_exit_2:;

#if 0
                    RESTORE_FRAME_EXCEPTION( frame_f82ffd4999ed656b8e7a64ff137d53c6_2 );
#endif

                    if ( exception_tb == NULL )
                    {
                        exception_tb = MAKE_TRACEBACK( frame_f82ffd4999ed656b8e7a64ff137d53c6_2, exception_lineno );
                    }
                    else if ( exception_tb->tb_frame != &frame_f82ffd4999ed656b8e7a64ff137d53c6_2->m_frame )
                    {
                        exception_tb = ADD_TRACEBACK( exception_tb, frame_f82ffd4999ed656b8e7a64ff137d53c6_2, exception_lineno );
                    }

                    // Attachs locals to frame if any.
                    Nuitka_Frame_AttachLocals(
                        (struct Nuitka_FrameObject *)frame_f82ffd4999ed656b8e7a64ff137d53c6_2,
                        type_description_2,
                        outline_0_var_i,
                        par_filter,
                        par_dict_factory,
                        par_retain_collection_types
                    );


                    // Release cached frame.
                    if ( frame_f82ffd4999ed656b8e7a64ff137d53c6_2 == cache_frame_f82ffd4999ed656b8e7a64ff137d53c6_2 )
                    {
                        Py_DECREF( frame_f82ffd4999ed656b8e7a64ff137d53c6_2 );
                    }
                    cache_frame_f82ffd4999ed656b8e7a64ff137d53c6_2 = NULL;

                    assertFrameObject( frame_f82ffd4999ed656b8e7a64ff137d53c6_2 );

                    // Put the previous frame back on top.
                    popFrameStack();

                    // Return the error.
                    goto nested_frame_exit_1;

                    frame_no_exception_1:;
                    goto skip_nested_handling_1;
                    nested_frame_exit_1:;
                    type_description_1 = "oococoooooc";
                    goto try_except_handler_3;
                    skip_nested_handling_1:;
                    // tried codes exits in all cases
                    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_1_asdict );
                    return NULL;
                    // Return handler code:
                    try_return_handler_3:;
                    Py_XDECREF( outline_0_var_i );
                    outline_0_var_i = NULL;

                    goto outline_result_1;
                    // Exception handler code:
                    try_except_handler_3:;
                    exception_keeper_type_2 = exception_type;
                    exception_keeper_value_2 = exception_value;
                    exception_keeper_tb_2 = exception_tb;
                    exception_keeper_lineno_2 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( outline_0_var_i );
                    outline_0_var_i = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_2;
                    exception_value = exception_keeper_value_2;
                    exception_tb = exception_keeper_tb_2;
                    exception_lineno = exception_keeper_lineno_2;

                    goto outline_exception_1;
                    // End of try:
                    // Return statement must have exited already.
                    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_1_asdict );
                    return NULL;
                    outline_exception_1:;
                    exception_lineno = 59;
                    goto try_except_handler_2;
                    outline_result_1:;
                    frame_971fec919b0d12e93ae402a49ce447a9->m_frame.f_lineno = 57;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_10 };
                        tmp_ass_subvalue_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_10 );
                    if ( tmp_ass_subvalue_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 57;
                        type_description_1 = "oococoooooc";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( var_rv );
                    tmp_ass_subscribed_2 = var_rv;
                    CHECK_OBJECT( var_a );
                    tmp_source_name_6 = var_a;
                    tmp_ass_subscript_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_name );
                    if ( tmp_ass_subscript_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_ass_subvalue_2 );

                        exception_lineno = 57;
                        type_description_1 = "oococoooooc";
                        goto try_except_handler_2;
                    }
                    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
                    Py_DECREF( tmp_ass_subscript_2 );
                    Py_DECREF( tmp_ass_subvalue_2 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 57;
                        type_description_1 = "oococoooooc";
                        goto try_except_handler_2;
                    }
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    nuitka_bool tmp_condition_result_6;
                    PyObject *tmp_isinstance_inst_2;
                    PyObject *tmp_isinstance_cls_2;
                    CHECK_OBJECT( var_v );
                    tmp_isinstance_inst_2 = var_v;
                    tmp_isinstance_cls_2 = (PyObject *)&PyDict_Type;
                    tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 65;
                        type_description_1 = "oococoooooc";
                        goto try_except_handler_2;
                    }
                    tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_assign_source_12;
                        CHECK_OBJECT( par_dict_factory );
                        tmp_assign_source_12 = par_dict_factory;
                        {
                            PyObject *old = PyCell_GET( var_df );
                            PyCell_SET( var_df, tmp_assign_source_12 );
                            Py_INCREF( tmp_assign_source_12 );
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_ass_subvalue_3;
                        PyObject *tmp_called_name_8;
                        PyObject *tmp_args_element_name_15;
                        PyObject *tmp_ass_subscribed_3;
                        PyObject *tmp_ass_subscript_3;
                        PyObject *tmp_source_name_7;
                        CHECK_OBJECT( PyCell_GET( var_df ) );
                        tmp_called_name_8 = PyCell_GET( var_df );
                        {
                            PyObject *tmp_assign_source_13;
                            PyObject *tmp_iter_arg_3;
                            PyObject *tmp_called_name_9;
                            PyObject *tmp_mvar_value_5;
                            PyObject *tmp_args_element_name_16;
                            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_iteritems );

                            if (unlikely( tmp_mvar_value_5 == NULL ))
                            {
                                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iteritems );
                            }

                            if ( tmp_mvar_value_5 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iteritems" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 76;
                                type_description_1 = "oococoooooc";
                                goto try_except_handler_2;
                            }

                            tmp_called_name_9 = tmp_mvar_value_5;
                            CHECK_OBJECT( var_v );
                            tmp_args_element_name_16 = var_v;
                            frame_971fec919b0d12e93ae402a49ce447a9->m_frame.f_lineno = 76;
                            {
                                PyObject *call_args[] = { tmp_args_element_name_16 };
                                tmp_iter_arg_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
                            }

                            if ( tmp_iter_arg_3 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 76;
                                type_description_1 = "oococoooooc";
                                goto try_except_handler_2;
                            }
                            tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_3 );
                            Py_DECREF( tmp_iter_arg_3 );
                            if ( tmp_assign_source_13 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 68;
                                type_description_1 = "oococoooooc";
                                goto try_except_handler_2;
                            }
                            {
                                PyObject *old = tmp_genexpr_1__$0;
                                tmp_genexpr_1__$0 = tmp_assign_source_13;
                                Py_XDECREF( old );
                            }

                        }
                        // Tried code:
                        tmp_args_element_name_15 = attr$_funcs$$$function_1_asdict$$$genexpr_1_genexpr_maker();

                        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_15)->m_closure[0] = var_df;
                        Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_15)->m_closure[0] );
                        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_15)->m_closure[1] = par_filter;
                        Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_15)->m_closure[1] );
                        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_15)->m_closure[2] = PyCell_NEW0( tmp_genexpr_1__$0 );
                        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_15)->m_closure[3] = par_retain_collection_types;
                        Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_15)->m_closure[3] );


                        goto try_return_handler_5;
                        // tried codes exits in all cases
                        NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_1_asdict );
                        return NULL;
                        // Return handler code:
                        try_return_handler_5:;
                        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                        Py_DECREF( tmp_genexpr_1__$0 );
                        tmp_genexpr_1__$0 = NULL;

                        goto outline_result_2;
                        // End of try:
                        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                        Py_DECREF( tmp_genexpr_1__$0 );
                        tmp_genexpr_1__$0 = NULL;

                        // Return statement must have exited already.
                        NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_1_asdict );
                        return NULL;
                        outline_result_2:;
                        frame_971fec919b0d12e93ae402a49ce447a9->m_frame.f_lineno = 67;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_15 };
                            tmp_ass_subvalue_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
                        }

                        Py_DECREF( tmp_args_element_name_15 );
                        if ( tmp_ass_subvalue_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 67;
                            type_description_1 = "oococoooooc";
                            goto try_except_handler_2;
                        }
                        CHECK_OBJECT( var_rv );
                        tmp_ass_subscribed_3 = var_rv;
                        CHECK_OBJECT( var_a );
                        tmp_source_name_7 = var_a;
                        tmp_ass_subscript_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_name );
                        if ( tmp_ass_subscript_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_ass_subvalue_3 );

                            exception_lineno = 67;
                            type_description_1 = "oococoooooc";
                            goto try_except_handler_2;
                        }
                        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
                        Py_DECREF( tmp_ass_subscript_3 );
                        Py_DECREF( tmp_ass_subvalue_3 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 67;
                            type_description_1 = "oococoooooc";
                            goto try_except_handler_2;
                        }
                    }
                    goto branch_end_5;
                    branch_no_5:;
                    {
                        PyObject *tmp_ass_subvalue_4;
                        PyObject *tmp_ass_subscribed_4;
                        PyObject *tmp_ass_subscript_4;
                        PyObject *tmp_source_name_8;
                        CHECK_OBJECT( var_v );
                        tmp_ass_subvalue_4 = var_v;
                        CHECK_OBJECT( var_rv );
                        tmp_ass_subscribed_4 = var_rv;
                        CHECK_OBJECT( var_a );
                        tmp_source_name_8 = var_a;
                        tmp_ass_subscript_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_name );
                        if ( tmp_ass_subscript_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 79;
                            type_description_1 = "oococoooooc";
                            goto try_except_handler_2;
                        }
                        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_4, tmp_ass_subscript_4, tmp_ass_subvalue_4 );
                        Py_DECREF( tmp_ass_subscript_4 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 79;
                            type_description_1 = "oococoooooc";
                            goto try_except_handler_2;
                        }
                    }
                    branch_end_5:;
                }
                branch_end_4:;
            }
            branch_end_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_ass_subvalue_5;
            PyObject *tmp_ass_subscribed_5;
            PyObject *tmp_ass_subscript_5;
            PyObject *tmp_source_name_9;
            CHECK_OBJECT( var_v );
            tmp_ass_subvalue_5 = var_v;
            CHECK_OBJECT( var_rv );
            tmp_ass_subscribed_5 = var_rv;
            CHECK_OBJECT( var_a );
            tmp_source_name_9 = var_a;
            tmp_ass_subscript_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_name );
            if ( tmp_ass_subscript_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;
                type_description_1 = "oococoooooc";
                goto try_except_handler_2;
            }
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_5, tmp_ass_subscript_5, tmp_ass_subvalue_5 );
            Py_DECREF( tmp_ass_subscript_5 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;
                type_description_1 = "oococoooooc";
                goto try_except_handler_2;
            }
        }
        branch_end_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 46;
        type_description_1 = "oococoooooc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_971fec919b0d12e93ae402a49ce447a9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_971fec919b0d12e93ae402a49ce447a9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_971fec919b0d12e93ae402a49ce447a9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_971fec919b0d12e93ae402a49ce447a9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_971fec919b0d12e93ae402a49ce447a9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_971fec919b0d12e93ae402a49ce447a9,
        type_description_1,
        par_inst,
        par_recurse,
        par_filter,
        par_dict_factory,
        par_retain_collection_types,
        var_attrs,
        var_rv,
        var_a,
        var_v,
        var_cf,
        var_df
    );


    // Release cached frame.
    if ( frame_971fec919b0d12e93ae402a49ce447a9 == cache_frame_971fec919b0d12e93ae402a49ce447a9 )
    {
        Py_DECREF( frame_971fec919b0d12e93ae402a49ce447a9 );
    }
    cache_frame_971fec919b0d12e93ae402a49ce447a9 = NULL;

    assertFrameObject( frame_971fec919b0d12e93ae402a49ce447a9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_rv );
    tmp_return_value = var_rv;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_1_asdict );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_inst );
    Py_DECREF( par_inst );
    par_inst = NULL;

    CHECK_OBJECT( (PyObject *)par_recurse );
    Py_DECREF( par_recurse );
    par_recurse = NULL;

    CHECK_OBJECT( (PyObject *)par_filter );
    Py_DECREF( par_filter );
    par_filter = NULL;

    CHECK_OBJECT( (PyObject *)par_dict_factory );
    Py_DECREF( par_dict_factory );
    par_dict_factory = NULL;

    CHECK_OBJECT( (PyObject *)par_retain_collection_types );
    Py_DECREF( par_retain_collection_types );
    par_retain_collection_types = NULL;

    CHECK_OBJECT( (PyObject *)var_attrs );
    Py_DECREF( var_attrs );
    var_attrs = NULL;

    CHECK_OBJECT( (PyObject *)var_rv );
    Py_DECREF( var_rv );
    var_rv = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    Py_XDECREF( var_cf );
    var_cf = NULL;

    CHECK_OBJECT( (PyObject *)var_df );
    Py_DECREF( var_df );
    var_df = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_inst );
    Py_DECREF( par_inst );
    par_inst = NULL;

    CHECK_OBJECT( (PyObject *)par_recurse );
    Py_DECREF( par_recurse );
    par_recurse = NULL;

    CHECK_OBJECT( (PyObject *)par_filter );
    Py_DECREF( par_filter );
    par_filter = NULL;

    CHECK_OBJECT( (PyObject *)par_dict_factory );
    Py_DECREF( par_dict_factory );
    par_dict_factory = NULL;

    CHECK_OBJECT( (PyObject *)par_retain_collection_types );
    Py_DECREF( par_retain_collection_types );
    par_retain_collection_types = NULL;

    Py_XDECREF( var_attrs );
    var_attrs = NULL;

    Py_XDECREF( var_rv );
    var_rv = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    Py_XDECREF( var_cf );
    var_cf = NULL;

    CHECK_OBJECT( (PyObject *)var_df );
    Py_DECREF( var_df );
    var_df = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_1_asdict );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct attr$_funcs$$$function_1_asdict$$$genexpr_1_genexpr_locals {
    PyObject *var_kk;
    PyObject *var_vv;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *attr$_funcs$$$function_1_asdict$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct attr$_funcs$$$function_1_asdict$$$genexpr_1_genexpr_locals *generator_heap = (struct attr$_funcs$$$function_1_asdict$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_kk = NULL;
    generator_heap->var_vv = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_5619416fb724fbf3cd31b2a368569f82, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[2] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[2] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Nooccc";
                generator_heap->exception_lineno = 68;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_iter_arg_1 = generator_heap->tmp_iter_value_0;
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 68;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Nooccc";
            generator_heap->exception_lineno = 68;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Nooccc";
            generator_heap->exception_lineno = 68;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "Nooccc";
                    generator_heap->exception_lineno = 68;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "Nooccc";
            generator_heap->exception_lineno = 68;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_kk;
            generator_heap->var_kk = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_kk );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_vv;
            generator_heap->var_vv = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_vv );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_args_element_name_8;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain__asdict_anything );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__asdict_anything );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_asdict_anything" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 69;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_kk );
        tmp_args_element_name_1 = generator_heap->var_kk;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "filter" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 70;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_2 = PyCell_GET( generator->m_closure[1] );
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "df" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 70;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_3 = PyCell_GET( generator->m_closure[0] );
        if ( PyCell_GET( generator->m_closure[3] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "retain_collection_types" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 70;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_4 = PyCell_GET( generator->m_closure[3] );
        generator->m_frame->m_frame.f_lineno = 69;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 69;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain__asdict_anything );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__asdict_anything );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_asdict_anything" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 72;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( generator_heap->var_vv );
        tmp_args_element_name_5 = generator_heap->var_vv;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "filter" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 73;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_6 = PyCell_GET( generator->m_closure[1] );
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "df" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 73;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_7 = PyCell_GET( generator->m_closure[0] );
        if ( PyCell_GET( generator->m_closure[3] ) == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "retain_collection_types" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 73;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_8 = PyCell_GET( generator->m_closure[3] );
        generator->m_frame->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_2, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_expression_name_1 );

            generator_heap->exception_lineno = 72;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_5, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), &tmp_args_element_name_7, sizeof(PyObject *), &tmp_args_element_name_8, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_5, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), &tmp_args_element_name_7, sizeof(PyObject *), &tmp_args_element_name_8, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 68;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 68;
        generator_heap->type_description_1 = "Nooccc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_kk,
            generator_heap->var_vv,
            generator->m_closure[1],
            generator->m_closure[0],
            generator->m_closure[3]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_kk );
    generator_heap->var_kk = NULL;

    Py_XDECREF( generator_heap->var_vv );
    generator_heap->var_vv = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_kk );
    generator_heap->var_kk = NULL;

    Py_XDECREF( generator_heap->var_vv );
    generator_heap->var_vv = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *attr$_funcs$$$function_1_asdict$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        attr$_funcs$$$function_1_asdict$$$genexpr_1_genexpr_context,
        module_attr$_funcs,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_d6b568f7768e3849583ee28edbc76902,
#endif
        codeobj_5619416fb724fbf3cd31b2a368569f82,
        4,
        sizeof(struct attr$_funcs$$$function_1_asdict$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_attr$_funcs$$$function_2__asdict_anything( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_val = python_pars[ 0 ];
    struct Nuitka_CellObject *par_filter = PyCell_NEW1( python_pars[ 1 ] );
    PyObject *par_dict_factory = python_pars[ 2 ];
    struct Nuitka_CellObject *par_retain_collection_types = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *var_rv = NULL;
    PyObject *var_cf = NULL;
    struct Nuitka_CellObject *var_df = PyCell_EMPTY();
    PyObject *outline_0_var_i = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_980029849d5b392ef4a7e2137a31a6c9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    struct Nuitka_FrameObject *frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_980029849d5b392ef4a7e2137a31a6c9 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_980029849d5b392ef4a7e2137a31a6c9, codeobj_980029849d5b392ef4a7e2137a31a6c9, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_980029849d5b392ef4a7e2137a31a6c9 = cache_frame_980029849d5b392ef4a7e2137a31a6c9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_980029849d5b392ef4a7e2137a31a6c9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_980029849d5b392ef4a7e2137a31a6c9 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_getattr_default_1;
        CHECK_OBJECT( par_val );
        tmp_source_name_1 = par_val;
        tmp_getattr_target_1 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_1 );
        if ( tmp_getattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ococooc";
            goto frame_exception_exit_1;
        }
        tmp_getattr_attr_1 = const_str_plain___attrs_attrs__;
        tmp_getattr_default_1 = Py_None;
        tmp_compexpr_left_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
        Py_DECREF( tmp_getattr_target_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "ococooc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_asdict );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_asdict );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "asdict" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 91;
                type_description_1 = "ococooc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_val );
            tmp_args_element_name_1 = par_val;
            tmp_args_element_name_2 = Py_True;
            CHECK_OBJECT( PyCell_GET( par_filter ) );
            tmp_args_element_name_3 = PyCell_GET( par_filter );
            CHECK_OBJECT( par_dict_factory );
            tmp_args_element_name_4 = par_dict_factory;
            CHECK_OBJECT( PyCell_GET( par_retain_collection_types ) );
            tmp_args_element_name_5 = PyCell_GET( par_retain_collection_types );
            frame_980029849d5b392ef4a7e2137a31a6c9->m_frame.f_lineno = 91;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
                tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_1, call_args );
            }

            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 91;
                type_description_1 = "ococooc";
                goto frame_exception_exit_1;
            }
            assert( var_rv == NULL );
            var_rv = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            CHECK_OBJECT( par_val );
            tmp_isinstance_inst_1 = par_val;
            tmp_isinstance_cls_1 = const_tuple_type_tuple_type_list_type_set_tuple;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 92;
                type_description_1 = "ococooc";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                PyObject *tmp_source_name_2;
                CHECK_OBJECT( PyCell_GET( par_retain_collection_types ) );
                tmp_compexpr_left_2 = PyCell_GET( par_retain_collection_types );
                tmp_compexpr_right_2 = Py_True;
                tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_1;
                }
                else
                {
                    goto condexpr_false_1;
                }
                condexpr_true_1:;
                CHECK_OBJECT( par_val );
                tmp_source_name_2 = par_val;
                tmp_assign_source_2 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_2 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 93;
                    type_description_1 = "ococooc";
                    goto frame_exception_exit_1;
                }
                goto condexpr_end_1;
                condexpr_false_1:;
                tmp_assign_source_2 = (PyObject *)&PyList_Type;
                Py_INCREF( tmp_assign_source_2 );
                condexpr_end_1:;
                assert( var_cf == NULL );
                var_cf = tmp_assign_source_2;
            }
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_called_name_2;
                PyObject *tmp_args_element_name_6;
                CHECK_OBJECT( var_cf );
                tmp_called_name_2 = var_cf;
                // Tried code:
                {
                    PyObject *tmp_assign_source_4;
                    PyObject *tmp_iter_arg_1;
                    CHECK_OBJECT( par_val );
                    tmp_iter_arg_1 = par_val;
                    tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
                    if ( tmp_assign_source_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 96;
                        type_description_1 = "ococooc";
                        goto try_except_handler_2;
                    }
                    assert( tmp_listcomp_1__$0 == NULL );
                    tmp_listcomp_1__$0 = tmp_assign_source_4;
                }
                {
                    PyObject *tmp_assign_source_5;
                    tmp_assign_source_5 = PyList_New( 0 );
                    assert( tmp_listcomp_1__contraction == NULL );
                    tmp_listcomp_1__contraction = tmp_assign_source_5;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2, codeobj_d1dc5195ebe0ee1ddf532d3d9c328ca1, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
                frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 = cache_frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 ) == 2 ); // Frame stack

                // Framed code:
                // Tried code:
                loop_start_1:;
                {
                    PyObject *tmp_next_source_1;
                    PyObject *tmp_assign_source_6;
                    CHECK_OBJECT( tmp_listcomp_1__$0 );
                    tmp_next_source_1 = tmp_listcomp_1__$0;
                    tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
                    if ( tmp_assign_source_6 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_1;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_2 = "ococ";
                            exception_lineno = 96;
                            goto try_except_handler_3;
                        }
                    }

                    {
                        PyObject *old = tmp_listcomp_1__iter_value_0;
                        tmp_listcomp_1__iter_value_0 = tmp_assign_source_6;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_7;
                    CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                    tmp_assign_source_7 = tmp_listcomp_1__iter_value_0;
                    {
                        PyObject *old = outline_0_var_i;
                        outline_0_var_i = tmp_assign_source_7;
                        Py_INCREF( outline_0_var_i );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_append_list_1;
                    PyObject *tmp_append_value_1;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_mvar_value_2;
                    PyObject *tmp_args_element_name_7;
                    PyObject *tmp_args_element_name_8;
                    PyObject *tmp_args_element_name_9;
                    PyObject *tmp_args_element_name_10;
                    CHECK_OBJECT( tmp_listcomp_1__contraction );
                    tmp_append_list_1 = tmp_listcomp_1__contraction;
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain__asdict_anything );

                    if (unlikely( tmp_mvar_value_2 == NULL ))
                    {
                        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__asdict_anything );
                    }

                    if ( tmp_mvar_value_2 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_asdict_anything" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 96;
                        type_description_2 = "ococ";
                        goto try_except_handler_3;
                    }

                    tmp_called_name_3 = tmp_mvar_value_2;
                    CHECK_OBJECT( outline_0_var_i );
                    tmp_args_element_name_7 = outline_0_var_i;
                    CHECK_OBJECT( PyCell_GET( par_filter ) );
                    tmp_args_element_name_8 = PyCell_GET( par_filter );
                    CHECK_OBJECT( par_dict_factory );
                    tmp_args_element_name_9 = par_dict_factory;
                    CHECK_OBJECT( PyCell_GET( par_retain_collection_types ) );
                    tmp_args_element_name_10 = PyCell_GET( par_retain_collection_types );
                    frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2->m_frame.f_lineno = 96;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9, tmp_args_element_name_10 };
                        tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_3, call_args );
                    }

                    if ( tmp_append_value_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 96;
                        type_description_2 = "ococ";
                        goto try_except_handler_3;
                    }
                    assert( PyList_Check( tmp_append_list_1 ) );
                    tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                    Py_DECREF( tmp_append_value_1 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 96;
                        type_description_2 = "ococ";
                        goto try_except_handler_3;
                    }
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 96;
                    type_description_2 = "ococ";
                    goto try_except_handler_3;
                }
                goto loop_start_1;
                loop_end_1:;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_args_element_name_6 = tmp_listcomp_1__contraction;
                Py_INCREF( tmp_args_element_name_6 );
                goto try_return_handler_3;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_2__asdict_anything );
                return NULL;
                // Return handler code:
                try_return_handler_3:;
                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                goto frame_return_exit_2;
                // Exception handler code:
                try_except_handler_3:;
                exception_keeper_type_1 = exception_type;
                exception_keeper_value_1 = exception_value;
                exception_keeper_tb_1 = exception_tb;
                exception_keeper_lineno_1 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_1;
                exception_value = exception_keeper_value_1;
                exception_tb = exception_keeper_tb_1;
                exception_lineno = exception_keeper_lineno_1;

                goto frame_exception_exit_2;
                // End of try:

#if 0
                RESTORE_FRAME_EXCEPTION( frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_1;

                frame_return_exit_2:;
#if 0
                RESTORE_FRAME_EXCEPTION( frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto try_return_handler_2;

                frame_exception_exit_2:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2,
                    type_description_2,
                    outline_0_var_i,
                    par_filter,
                    par_dict_factory,
                    par_retain_collection_types
                );


                // Release cached frame.
                if ( frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 == cache_frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 )
                {
                    Py_DECREF( frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 );
                }
                cache_frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 = NULL;

                assertFrameObject( frame_d1dc5195ebe0ee1ddf532d3d9c328ca1_2 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_1;

                frame_no_exception_1:;
                goto skip_nested_handling_1;
                nested_frame_exit_1:;
                type_description_1 = "ococooc";
                goto try_except_handler_2;
                skip_nested_handling_1:;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_2__asdict_anything );
                return NULL;
                // Return handler code:
                try_return_handler_2:;
                Py_XDECREF( outline_0_var_i );
                outline_0_var_i = NULL;

                goto outline_result_1;
                // Exception handler code:
                try_except_handler_2:;
                exception_keeper_type_2 = exception_type;
                exception_keeper_value_2 = exception_value;
                exception_keeper_tb_2 = exception_tb;
                exception_keeper_lineno_2 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( outline_0_var_i );
                outline_0_var_i = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_2;
                exception_value = exception_keeper_value_2;
                exception_tb = exception_keeper_tb_2;
                exception_lineno = exception_keeper_lineno_2;

                goto outline_exception_1;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_2__asdict_anything );
                return NULL;
                outline_exception_1:;
                exception_lineno = 96;
                goto frame_exception_exit_1;
                outline_result_1:;
                frame_980029849d5b392ef4a7e2137a31a6c9->m_frame.f_lineno = 94;
                {
                    PyObject *call_args[] = { tmp_args_element_name_6 };
                    tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                }

                Py_DECREF( tmp_args_element_name_6 );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 94;
                    type_description_1 = "ococooc";
                    goto frame_exception_exit_1;
                }
                assert( var_rv == NULL );
                var_rv = tmp_assign_source_3;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_isinstance_inst_2;
                PyObject *tmp_isinstance_cls_2;
                CHECK_OBJECT( par_val );
                tmp_isinstance_inst_2 = par_val;
                tmp_isinstance_cls_2 = (PyObject *)&PyDict_Type;
                tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 102;
                    type_description_1 = "ococooc";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_8;
                    CHECK_OBJECT( par_dict_factory );
                    tmp_assign_source_8 = par_dict_factory;
                    assert( PyCell_GET( var_df ) == NULL );
                    Py_INCREF( tmp_assign_source_8 );
                    PyCell_SET( var_df, tmp_assign_source_8 );

                }
                {
                    PyObject *tmp_assign_source_9;
                    PyObject *tmp_called_name_4;
                    PyObject *tmp_args_element_name_11;
                    CHECK_OBJECT( PyCell_GET( var_df ) );
                    tmp_called_name_4 = PyCell_GET( var_df );
                    {
                        PyObject *tmp_assign_source_10;
                        PyObject *tmp_iter_arg_2;
                        PyObject *tmp_called_name_5;
                        PyObject *tmp_mvar_value_3;
                        PyObject *tmp_args_element_name_12;
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_iteritems );

                        if (unlikely( tmp_mvar_value_3 == NULL ))
                        {
                            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iteritems );
                        }

                        if ( tmp_mvar_value_3 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iteritems" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 109;
                            type_description_1 = "ococooc";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_name_5 = tmp_mvar_value_3;
                        CHECK_OBJECT( par_val );
                        tmp_args_element_name_12 = par_val;
                        frame_980029849d5b392ef4a7e2137a31a6c9->m_frame.f_lineno = 109;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_12 };
                            tmp_iter_arg_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
                        }

                        if ( tmp_iter_arg_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 109;
                            type_description_1 = "ococooc";
                            goto frame_exception_exit_1;
                        }
                        tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_2 );
                        Py_DECREF( tmp_iter_arg_2 );
                        if ( tmp_assign_source_10 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 105;
                            type_description_1 = "ococooc";
                            goto frame_exception_exit_1;
                        }
                        assert( tmp_genexpr_1__$0 == NULL );
                        tmp_genexpr_1__$0 = tmp_assign_source_10;
                    }
                    // Tried code:
                    tmp_args_element_name_11 = attr$_funcs$$$function_2__asdict_anything$$$genexpr_1_genexpr_maker();

                    ((struct Nuitka_GeneratorObject *)tmp_args_element_name_11)->m_closure[0] = var_df;
                    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_11)->m_closure[0] );
                    ((struct Nuitka_GeneratorObject *)tmp_args_element_name_11)->m_closure[1] = par_filter;
                    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_11)->m_closure[1] );
                    ((struct Nuitka_GeneratorObject *)tmp_args_element_name_11)->m_closure[2] = PyCell_NEW0( tmp_genexpr_1__$0 );
                    ((struct Nuitka_GeneratorObject *)tmp_args_element_name_11)->m_closure[3] = par_retain_collection_types;
                    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_11)->m_closure[3] );


                    goto try_return_handler_4;
                    // tried codes exits in all cases
                    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_2__asdict_anything );
                    return NULL;
                    // Return handler code:
                    try_return_handler_4:;
                    CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                    Py_DECREF( tmp_genexpr_1__$0 );
                    tmp_genexpr_1__$0 = NULL;

                    goto outline_result_2;
                    // End of try:
                    CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                    Py_DECREF( tmp_genexpr_1__$0 );
                    tmp_genexpr_1__$0 = NULL;

                    // Return statement must have exited already.
                    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_2__asdict_anything );
                    return NULL;
                    outline_result_2:;
                    frame_980029849d5b392ef4a7e2137a31a6c9->m_frame.f_lineno = 104;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_11 };
                        tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_11 );
                    if ( tmp_assign_source_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 104;
                        type_description_1 = "ococooc";
                        goto frame_exception_exit_1;
                    }
                    assert( var_rv == NULL );
                    var_rv = tmp_assign_source_9;
                }
                goto branch_end_3;
                branch_no_3:;
                {
                    PyObject *tmp_assign_source_11;
                    CHECK_OBJECT( par_val );
                    tmp_assign_source_11 = par_val;
                    assert( var_rv == NULL );
                    Py_INCREF( tmp_assign_source_11 );
                    var_rv = tmp_assign_source_11;
                }
                branch_end_3:;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }
    if ( var_rv == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "rv" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 113;
        type_description_1 = "ococooc";
        goto frame_exception_exit_1;
    }

    tmp_return_value = var_rv;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_980029849d5b392ef4a7e2137a31a6c9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_980029849d5b392ef4a7e2137a31a6c9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_980029849d5b392ef4a7e2137a31a6c9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_980029849d5b392ef4a7e2137a31a6c9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_980029849d5b392ef4a7e2137a31a6c9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_980029849d5b392ef4a7e2137a31a6c9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_980029849d5b392ef4a7e2137a31a6c9,
        type_description_1,
        par_val,
        par_filter,
        par_dict_factory,
        par_retain_collection_types,
        var_rv,
        var_cf,
        var_df
    );


    // Release cached frame.
    if ( frame_980029849d5b392ef4a7e2137a31a6c9 == cache_frame_980029849d5b392ef4a7e2137a31a6c9 )
    {
        Py_DECREF( frame_980029849d5b392ef4a7e2137a31a6c9 );
    }
    cache_frame_980029849d5b392ef4a7e2137a31a6c9 = NULL;

    assertFrameObject( frame_980029849d5b392ef4a7e2137a31a6c9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_2__asdict_anything );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_val );
    Py_DECREF( par_val );
    par_val = NULL;

    CHECK_OBJECT( (PyObject *)par_filter );
    Py_DECREF( par_filter );
    par_filter = NULL;

    CHECK_OBJECT( (PyObject *)par_dict_factory );
    Py_DECREF( par_dict_factory );
    par_dict_factory = NULL;

    CHECK_OBJECT( (PyObject *)par_retain_collection_types );
    Py_DECREF( par_retain_collection_types );
    par_retain_collection_types = NULL;

    Py_XDECREF( var_rv );
    var_rv = NULL;

    Py_XDECREF( var_cf );
    var_cf = NULL;

    CHECK_OBJECT( (PyObject *)var_df );
    Py_DECREF( var_df );
    var_df = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_val );
    Py_DECREF( par_val );
    par_val = NULL;

    CHECK_OBJECT( (PyObject *)par_filter );
    Py_DECREF( par_filter );
    par_filter = NULL;

    CHECK_OBJECT( (PyObject *)par_dict_factory );
    Py_DECREF( par_dict_factory );
    par_dict_factory = NULL;

    CHECK_OBJECT( (PyObject *)par_retain_collection_types );
    Py_DECREF( par_retain_collection_types );
    par_retain_collection_types = NULL;

    Py_XDECREF( var_rv );
    var_rv = NULL;

    Py_XDECREF( var_cf );
    var_cf = NULL;

    CHECK_OBJECT( (PyObject *)var_df );
    Py_DECREF( var_df );
    var_df = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_2__asdict_anything );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct attr$_funcs$$$function_2__asdict_anything$$$genexpr_1_genexpr_locals {
    PyObject *var_kk;
    PyObject *var_vv;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *attr$_funcs$$$function_2__asdict_anything$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct attr$_funcs$$$function_2__asdict_anything$$$genexpr_1_genexpr_locals *generator_heap = (struct attr$_funcs$$$function_2__asdict_anything$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_kk = NULL;
    generator_heap->var_vv = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_7b6fa8a333d1c91cb053395857592df1, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[2] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[2] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Nooccc";
                generator_heap->exception_lineno = 105;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_iter_arg_1 = generator_heap->tmp_iter_value_0;
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 105;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Nooccc";
            generator_heap->exception_lineno = 105;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Nooccc";
            generator_heap->exception_lineno = 105;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "Nooccc";
                    generator_heap->exception_lineno = 105;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "Nooccc";
            generator_heap->exception_lineno = 105;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_kk;
            generator_heap->var_kk = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_kk );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_vv;
            generator_heap->var_vv = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_vv );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_args_element_name_8;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain__asdict_anything );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__asdict_anything );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_asdict_anything" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 106;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_kk );
        tmp_args_element_name_1 = generator_heap->var_kk;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "filter" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 106;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_2 = PyCell_GET( generator->m_closure[1] );
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "df" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 106;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_3 = PyCell_GET( generator->m_closure[0] );
        if ( PyCell_GET( generator->m_closure[3] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "retain_collection_types" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 106;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_4 = PyCell_GET( generator->m_closure[3] );
        generator->m_frame->m_frame.f_lineno = 106;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 106;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain__asdict_anything );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__asdict_anything );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_asdict_anything" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 107;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( generator_heap->var_vv );
        tmp_args_element_name_5 = generator_heap->var_vv;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "filter" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 107;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_6 = PyCell_GET( generator->m_closure[1] );
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "df" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 107;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_7 = PyCell_GET( generator->m_closure[0] );
        if ( PyCell_GET( generator->m_closure[3] ) == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "retain_collection_types" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 107;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }

        tmp_args_element_name_8 = PyCell_GET( generator->m_closure[3] );
        generator->m_frame->m_frame.f_lineno = 107;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_2, call_args );
        }

        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_expression_name_1 );

            generator_heap->exception_lineno = 107;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_5, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), &tmp_args_element_name_7, sizeof(PyObject *), &tmp_args_element_name_8, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_args_element_name_3, sizeof(PyObject *), &tmp_args_element_name_4, sizeof(PyObject *), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_element_name_5, sizeof(PyObject *), &tmp_args_element_name_6, sizeof(PyObject *), &tmp_args_element_name_7, sizeof(PyObject *), &tmp_args_element_name_8, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 105;
            generator_heap->type_description_1 = "Nooccc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 105;
        generator_heap->type_description_1 = "Nooccc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_kk,
            generator_heap->var_vv,
            generator->m_closure[1],
            generator->m_closure[0],
            generator->m_closure[3]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_kk );
    generator_heap->var_kk = NULL;

    Py_XDECREF( generator_heap->var_vv );
    generator_heap->var_vv = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_kk );
    generator_heap->var_kk = NULL;

    Py_XDECREF( generator_heap->var_vv );
    generator_heap->var_vv = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *attr$_funcs$$$function_2__asdict_anything$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        attr$_funcs$$$function_2__asdict_anything$$$genexpr_1_genexpr_context,
        module_attr$_funcs,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_bf193d080c27b70d5950ee1f3b1ee064,
#endif
        codeobj_7b6fa8a333d1c91cb053395857592df1,
        4,
        sizeof(struct attr$_funcs$$$function_2__asdict_anything$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_attr$_funcs$$$function_3_astuple( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_inst = python_pars[ 0 ];
    PyObject *par_recurse = python_pars[ 1 ];
    PyObject *par_filter = python_pars[ 2 ];
    struct Nuitka_CellObject *par_tuple_factory = PyCell_NEW1( python_pars[ 3 ] );
    PyObject *par_retain_collection_types = python_pars[ 4 ];
    PyObject *var_attrs = NULL;
    PyObject *var_rv = NULL;
    struct Nuitka_CellObject *var_retain = PyCell_EMPTY();
    PyObject *var_a = NULL;
    PyObject *var_v = NULL;
    PyObject *var_cf = NULL;
    PyObject *var_df = NULL;
    PyObject *outline_0_var_j = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_7d0311866830c038650d360f68da8fd3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    struct Nuitka_FrameObject *frame_e1f1293b3a5624800f12024e8955cc06_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_e1f1293b3a5624800f12024e8955cc06_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_7d0311866830c038650d360f68da8fd3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7d0311866830c038650d360f68da8fd3, codeobj_7d0311866830c038650d360f68da8fd3, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7d0311866830c038650d360f68da8fd3 = cache_frame_7d0311866830c038650d360f68da8fd3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7d0311866830c038650d360f68da8fd3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7d0311866830c038650d360f68da8fd3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_fields );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fields );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fields" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;
            type_description_1 = "ooocooocoooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_inst );
        tmp_source_name_1 = par_inst;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooocooocoooo";
            goto frame_exception_exit_1;
        }
        frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 149;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooocooocoooo";
            goto frame_exception_exit_1;
        }
        assert( var_attrs == NULL );
        var_attrs = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( var_rv == NULL );
        var_rv = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( par_retain_collection_types );
        tmp_assign_source_3 = par_retain_collection_types;
        assert( PyCell_GET( var_retain ) == NULL );
        Py_INCREF( tmp_assign_source_3 );
        PyCell_SET( var_retain, tmp_assign_source_3 );

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( var_attrs );
        tmp_iter_arg_1 = var_attrs;
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "ooocooocoooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_4;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooocooocoooo";
                exception_lineno = 152;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_6 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_a;
            var_a = tmp_assign_source_6;
            Py_INCREF( var_a );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_inst );
        tmp_getattr_target_1 = par_inst;
        CHECK_OBJECT( var_a );
        tmp_source_name_2 = var_a;
        tmp_getattr_attr_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
        if ( tmp_getattr_attr_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooocooocoooo";
            goto try_except_handler_2;
        }
        tmp_assign_source_7 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        Py_DECREF( tmp_getattr_attr_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooocooocoooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_v;
            var_v = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        CHECK_OBJECT( par_filter );
        tmp_compexpr_left_1 = par_filter;
        tmp_compexpr_right_1 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( par_filter );
        tmp_called_name_2 = par_filter;
        CHECK_OBJECT( var_a );
        tmp_args_element_name_2 = var_a;
        CHECK_OBJECT( var_v );
        tmp_args_element_name_3 = var_v;
        frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 154;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "ooocooocoooo";
            goto try_except_handler_2;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "ooocooocoooo";
            goto try_except_handler_2;
        }
        tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_start_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_recurse );
        tmp_compexpr_left_2 = par_recurse;
        tmp_compexpr_right_2 = Py_True;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_source_name_3;
            int tmp_truth_name_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_has );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 157;
                type_description_1 = "ooocooocoooo";
                goto try_except_handler_2;
            }

            tmp_called_name_3 = tmp_mvar_value_2;
            CHECK_OBJECT( var_v );
            tmp_source_name_3 = var_v;
            tmp_args_element_name_4 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_3 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "ooocooocoooo";
                goto try_except_handler_2;
            }
            frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 157;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "ooocooocoooo";
                goto try_except_handler_2;
            }
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_call_result_1 );

                exception_lineno = 157;
                type_description_1 = "ooocooocoooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_1 );
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_4;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_5;
                PyObject *tmp_called_name_5;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_args_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_kw_name_1;
                PyObject *tmp_dict_key_1;
                PyObject *tmp_dict_value_1;
                PyObject *tmp_dict_key_2;
                PyObject *tmp_dict_value_2;
                PyObject *tmp_dict_key_3;
                PyObject *tmp_dict_value_3;
                PyObject *tmp_dict_key_4;
                PyObject *tmp_dict_value_4;
                CHECK_OBJECT( var_rv );
                tmp_source_name_4 = var_rv;
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_append );
                if ( tmp_called_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 158;
                    type_description_1 = "ooocooocoooo";
                    goto try_except_handler_2;
                }
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_astuple );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_astuple );
                }

                if ( tmp_mvar_value_3 == NULL )
                {
                    Py_DECREF( tmp_called_name_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "astuple" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 159;
                    type_description_1 = "ooocooocoooo";
                    goto try_except_handler_2;
                }

                tmp_called_name_5 = tmp_mvar_value_3;
                CHECK_OBJECT( var_v );
                tmp_tuple_element_1 = var_v;
                tmp_args_name_1 = PyTuple_New( 1 );
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
                tmp_dict_key_1 = const_str_plain_recurse;
                tmp_dict_value_1 = Py_True;
                tmp_kw_name_1 = _PyDict_NewPresized( 4 );
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
                assert( !(tmp_res != 0) );
                tmp_dict_key_2 = const_str_plain_filter;
                CHECK_OBJECT( par_filter );
                tmp_dict_value_2 = par_filter;
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
                assert( !(tmp_res != 0) );
                tmp_dict_key_3 = const_str_plain_tuple_factory;
                CHECK_OBJECT( PyCell_GET( par_tuple_factory ) );
                tmp_dict_value_3 = PyCell_GET( par_tuple_factory );
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_3, tmp_dict_value_3 );
                assert( !(tmp_res != 0) );
                tmp_dict_key_4 = const_str_plain_retain_collection_types;
                CHECK_OBJECT( PyCell_GET( var_retain ) );
                tmp_dict_value_4 = PyCell_GET( var_retain );
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_4, tmp_dict_value_4 );
                assert( !(tmp_res != 0) );
                frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 159;
                tmp_args_element_name_5 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                if ( tmp_args_element_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_4 );

                    exception_lineno = 159;
                    type_description_1 = "ooocooocoooo";
                    goto try_except_handler_2;
                }
                frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 158;
                {
                    PyObject *call_args[] = { tmp_args_element_name_5 };
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                }

                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_5 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 158;
                    type_description_1 = "ooocooocoooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            goto branch_end_3;
            branch_no_3:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_isinstance_inst_1;
                PyObject *tmp_isinstance_cls_1;
                CHECK_OBJECT( var_v );
                tmp_isinstance_inst_1 = var_v;
                tmp_isinstance_cls_1 = const_tuple_type_tuple_type_list_type_set_tuple;
                tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 167;
                    type_description_1 = "ooocooocoooo";
                    goto try_except_handler_2;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_8;
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_3;
                    PyObject *tmp_compexpr_right_3;
                    PyObject *tmp_source_name_5;
                    CHECK_OBJECT( PyCell_GET( var_retain ) );
                    tmp_compexpr_left_3 = PyCell_GET( var_retain );
                    tmp_compexpr_right_3 = Py_True;
                    tmp_condition_result_5 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto condexpr_true_1;
                    }
                    else
                    {
                        goto condexpr_false_1;
                    }
                    condexpr_true_1:;
                    CHECK_OBJECT( var_v );
                    tmp_source_name_5 = var_v;
                    tmp_assign_source_8 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_5 );
                    if ( tmp_assign_source_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 168;
                        type_description_1 = "ooocooocoooo";
                        goto try_except_handler_2;
                    }
                    goto condexpr_end_1;
                    condexpr_false_1:;
                    tmp_assign_source_8 = (PyObject *)&PyList_Type;
                    Py_INCREF( tmp_assign_source_8 );
                    condexpr_end_1:;
                    {
                        PyObject *old = var_cf;
                        var_cf = tmp_assign_source_8;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_called_name_6;
                    PyObject *tmp_source_name_6;
                    PyObject *tmp_call_result_3;
                    PyObject *tmp_args_element_name_6;
                    PyObject *tmp_called_name_7;
                    PyObject *tmp_args_element_name_7;
                    CHECK_OBJECT( var_rv );
                    tmp_source_name_6 = var_rv;
                    tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_append );
                    if ( tmp_called_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 169;
                        type_description_1 = "ooocooocoooo";
                        goto try_except_handler_2;
                    }
                    CHECK_OBJECT( var_cf );
                    tmp_called_name_7 = var_cf;
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_9;
                        PyObject *tmp_iter_arg_2;
                        CHECK_OBJECT( var_v );
                        tmp_iter_arg_2 = var_v;
                        tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_2 );
                        if ( tmp_assign_source_9 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 172;
                            type_description_1 = "ooocooocoooo";
                            goto try_except_handler_3;
                        }
                        {
                            PyObject *old = tmp_listcomp_1__$0;
                            tmp_listcomp_1__$0 = tmp_assign_source_9;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_10;
                        tmp_assign_source_10 = PyList_New( 0 );
                        {
                            PyObject *old = tmp_listcomp_1__contraction;
                            tmp_listcomp_1__contraction = tmp_assign_source_10;
                            Py_XDECREF( old );
                        }

                    }
                    MAKE_OR_REUSE_FRAME( cache_frame_e1f1293b3a5624800f12024e8955cc06_2, codeobj_e1f1293b3a5624800f12024e8955cc06, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
                    frame_e1f1293b3a5624800f12024e8955cc06_2 = cache_frame_e1f1293b3a5624800f12024e8955cc06_2;

                    // Push the new frame as the currently active one.
                    pushFrameStack( frame_e1f1293b3a5624800f12024e8955cc06_2 );

                    // Mark the frame object as in use, ref count 1 will be up for reuse.
                    assert( Py_REFCNT( frame_e1f1293b3a5624800f12024e8955cc06_2 ) == 2 ); // Frame stack

                    // Framed code:
                    // Tried code:
                    loop_start_2:;
                    {
                        PyObject *tmp_next_source_2;
                        PyObject *tmp_assign_source_11;
                        CHECK_OBJECT( tmp_listcomp_1__$0 );
                        tmp_next_source_2 = tmp_listcomp_1__$0;
                        tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_2 );
                        if ( tmp_assign_source_11 == NULL )
                        {
                            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                            {

                                goto loop_end_2;
                            }
                            else
                            {

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                type_description_2 = "oocc";
                                exception_lineno = 172;
                                goto try_except_handler_4;
                            }
                        }

                        {
                            PyObject *old = tmp_listcomp_1__iter_value_0;
                            tmp_listcomp_1__iter_value_0 = tmp_assign_source_11;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_12;
                        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                        tmp_assign_source_12 = tmp_listcomp_1__iter_value_0;
                        {
                            PyObject *old = outline_0_var_j;
                            outline_0_var_j = tmp_assign_source_12;
                            Py_INCREF( outline_0_var_j );
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_append_list_1;
                        PyObject *tmp_append_value_1;
                        nuitka_bool tmp_condition_result_6;
                        PyObject *tmp_called_name_8;
                        PyObject *tmp_mvar_value_4;
                        PyObject *tmp_call_result_4;
                        PyObject *tmp_args_element_name_8;
                        PyObject *tmp_source_name_7;
                        int tmp_truth_name_2;
                        PyObject *tmp_called_name_9;
                        PyObject *tmp_mvar_value_5;
                        PyObject *tmp_args_name_2;
                        PyObject *tmp_tuple_element_2;
                        PyObject *tmp_kw_name_2;
                        PyObject *tmp_dict_key_5;
                        PyObject *tmp_dict_value_5;
                        PyObject *tmp_dict_key_6;
                        PyObject *tmp_dict_value_6;
                        PyObject *tmp_dict_key_7;
                        PyObject *tmp_dict_value_7;
                        PyObject *tmp_dict_key_8;
                        PyObject *tmp_dict_value_8;
                        CHECK_OBJECT( tmp_listcomp_1__contraction );
                        tmp_append_list_1 = tmp_listcomp_1__contraction;
                        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_has );

                        if (unlikely( tmp_mvar_value_4 == NULL ))
                        {
                            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has );
                        }

                        if ( tmp_mvar_value_4 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 179;
                            type_description_2 = "oocc";
                            goto try_except_handler_4;
                        }

                        tmp_called_name_8 = tmp_mvar_value_4;
                        CHECK_OBJECT( outline_0_var_j );
                        tmp_source_name_7 = outline_0_var_j;
                        tmp_args_element_name_8 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_7 );
                        if ( tmp_args_element_name_8 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 179;
                            type_description_2 = "oocc";
                            goto try_except_handler_4;
                        }
                        frame_e1f1293b3a5624800f12024e8955cc06_2->m_frame.f_lineno = 179;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_8 };
                            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
                        }

                        Py_DECREF( tmp_args_element_name_8 );
                        if ( tmp_call_result_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 179;
                            type_description_2 = "oocc";
                            goto try_except_handler_4;
                        }
                        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_4 );
                        if ( tmp_truth_name_2 == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_call_result_4 );

                            exception_lineno = 179;
                            type_description_2 = "oocc";
                            goto try_except_handler_4;
                        }
                        tmp_condition_result_6 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        Py_DECREF( tmp_call_result_4 );
                        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                        {
                            goto condexpr_true_2;
                        }
                        else
                        {
                            goto condexpr_false_2;
                        }
                        condexpr_true_2:;
                        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_astuple );

                        if (unlikely( tmp_mvar_value_5 == NULL ))
                        {
                            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_astuple );
                        }

                        if ( tmp_mvar_value_5 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "astuple" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 172;
                            type_description_2 = "oocc";
                            goto try_except_handler_4;
                        }

                        tmp_called_name_9 = tmp_mvar_value_5;
                        CHECK_OBJECT( outline_0_var_j );
                        tmp_tuple_element_2 = outline_0_var_j;
                        tmp_args_name_2 = PyTuple_New( 1 );
                        Py_INCREF( tmp_tuple_element_2 );
                        PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
                        tmp_dict_key_5 = const_str_plain_recurse;
                        tmp_dict_value_5 = Py_True;
                        tmp_kw_name_2 = _PyDict_NewPresized( 4 );
                        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_5, tmp_dict_value_5 );
                        assert( !(tmp_res != 0) );
                        tmp_dict_key_6 = const_str_plain_filter;
                        CHECK_OBJECT( par_filter );
                        tmp_dict_value_6 = par_filter;
                        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_6, tmp_dict_value_6 );
                        assert( !(tmp_res != 0) );
                        tmp_dict_key_7 = const_str_plain_tuple_factory;
                        CHECK_OBJECT( PyCell_GET( par_tuple_factory ) );
                        tmp_dict_value_7 = PyCell_GET( par_tuple_factory );
                        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_7, tmp_dict_value_7 );
                        assert( !(tmp_res != 0) );
                        tmp_dict_key_8 = const_str_plain_retain_collection_types;
                        CHECK_OBJECT( PyCell_GET( var_retain ) );
                        tmp_dict_value_8 = PyCell_GET( var_retain );
                        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_8, tmp_dict_value_8 );
                        assert( !(tmp_res != 0) );
                        frame_e1f1293b3a5624800f12024e8955cc06_2->m_frame.f_lineno = 172;
                        tmp_append_value_1 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_2, tmp_kw_name_2 );
                        Py_DECREF( tmp_args_name_2 );
                        Py_DECREF( tmp_kw_name_2 );
                        if ( tmp_append_value_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 172;
                            type_description_2 = "oocc";
                            goto try_except_handler_4;
                        }
                        goto condexpr_end_2;
                        condexpr_false_2:;
                        CHECK_OBJECT( outline_0_var_j );
                        tmp_append_value_1 = outline_0_var_j;
                        Py_INCREF( tmp_append_value_1 );
                        condexpr_end_2:;
                        assert( PyList_Check( tmp_append_list_1 ) );
                        tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                        Py_DECREF( tmp_append_value_1 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 172;
                            type_description_2 = "oocc";
                            goto try_except_handler_4;
                        }
                    }
                    if ( CONSIDER_THREADING() == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 172;
                        type_description_2 = "oocc";
                        goto try_except_handler_4;
                    }
                    goto loop_start_2;
                    loop_end_2:;
                    CHECK_OBJECT( tmp_listcomp_1__contraction );
                    tmp_args_element_name_7 = tmp_listcomp_1__contraction;
                    Py_INCREF( tmp_args_element_name_7 );
                    goto try_return_handler_4;
                    // tried codes exits in all cases
                    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_3_astuple );
                    return NULL;
                    // Return handler code:
                    try_return_handler_4:;
                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                    Py_DECREF( tmp_listcomp_1__$0 );
                    tmp_listcomp_1__$0 = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                    Py_DECREF( tmp_listcomp_1__contraction );
                    tmp_listcomp_1__contraction = NULL;

                    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                    tmp_listcomp_1__iter_value_0 = NULL;

                    goto frame_return_exit_2;
                    // Exception handler code:
                    try_except_handler_4:;
                    exception_keeper_type_1 = exception_type;
                    exception_keeper_value_1 = exception_value;
                    exception_keeper_tb_1 = exception_tb;
                    exception_keeper_lineno_1 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                    Py_DECREF( tmp_listcomp_1__$0 );
                    tmp_listcomp_1__$0 = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                    Py_DECREF( tmp_listcomp_1__contraction );
                    tmp_listcomp_1__contraction = NULL;

                    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                    tmp_listcomp_1__iter_value_0 = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_1;
                    exception_value = exception_keeper_value_1;
                    exception_tb = exception_keeper_tb_1;
                    exception_lineno = exception_keeper_lineno_1;

                    goto frame_exception_exit_2;
                    // End of try:

#if 0
                    RESTORE_FRAME_EXCEPTION( frame_e1f1293b3a5624800f12024e8955cc06_2 );
#endif

                    // Put the previous frame back on top.
                    popFrameStack();

                    goto frame_no_exception_1;

                    frame_return_exit_2:;
#if 0
                    RESTORE_FRAME_EXCEPTION( frame_e1f1293b3a5624800f12024e8955cc06_2 );
#endif

                    // Put the previous frame back on top.
                    popFrameStack();

                    goto try_return_handler_3;

                    frame_exception_exit_2:;

#if 0
                    RESTORE_FRAME_EXCEPTION( frame_e1f1293b3a5624800f12024e8955cc06_2 );
#endif

                    if ( exception_tb == NULL )
                    {
                        exception_tb = MAKE_TRACEBACK( frame_e1f1293b3a5624800f12024e8955cc06_2, exception_lineno );
                    }
                    else if ( exception_tb->tb_frame != &frame_e1f1293b3a5624800f12024e8955cc06_2->m_frame )
                    {
                        exception_tb = ADD_TRACEBACK( exception_tb, frame_e1f1293b3a5624800f12024e8955cc06_2, exception_lineno );
                    }

                    // Attachs locals to frame if any.
                    Nuitka_Frame_AttachLocals(
                        (struct Nuitka_FrameObject *)frame_e1f1293b3a5624800f12024e8955cc06_2,
                        type_description_2,
                        outline_0_var_j,
                        par_filter,
                        par_tuple_factory,
                        var_retain
                    );


                    // Release cached frame.
                    if ( frame_e1f1293b3a5624800f12024e8955cc06_2 == cache_frame_e1f1293b3a5624800f12024e8955cc06_2 )
                    {
                        Py_DECREF( frame_e1f1293b3a5624800f12024e8955cc06_2 );
                    }
                    cache_frame_e1f1293b3a5624800f12024e8955cc06_2 = NULL;

                    assertFrameObject( frame_e1f1293b3a5624800f12024e8955cc06_2 );

                    // Put the previous frame back on top.
                    popFrameStack();

                    // Return the error.
                    goto nested_frame_exit_1;

                    frame_no_exception_1:;
                    goto skip_nested_handling_1;
                    nested_frame_exit_1:;
                    type_description_1 = "ooocooocoooo";
                    goto try_except_handler_3;
                    skip_nested_handling_1:;
                    // tried codes exits in all cases
                    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_3_astuple );
                    return NULL;
                    // Return handler code:
                    try_return_handler_3:;
                    Py_XDECREF( outline_0_var_j );
                    outline_0_var_j = NULL;

                    goto outline_result_1;
                    // Exception handler code:
                    try_except_handler_3:;
                    exception_keeper_type_2 = exception_type;
                    exception_keeper_value_2 = exception_value;
                    exception_keeper_tb_2 = exception_tb;
                    exception_keeper_lineno_2 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( outline_0_var_j );
                    outline_0_var_j = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_2;
                    exception_value = exception_keeper_value_2;
                    exception_tb = exception_keeper_tb_2;
                    exception_lineno = exception_keeper_lineno_2;

                    goto outline_exception_1;
                    // End of try:
                    // Return statement must have exited already.
                    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_3_astuple );
                    return NULL;
                    outline_exception_1:;
                    exception_lineno = 172;
                    goto try_except_handler_2;
                    outline_result_1:;
                    frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 170;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_7 };
                        tmp_args_element_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_7 );
                    if ( tmp_args_element_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_6 );

                        exception_lineno = 170;
                        type_description_1 = "ooocooocoooo";
                        goto try_except_handler_2;
                    }
                    frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 169;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_6 };
                        tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
                    }

                    Py_DECREF( tmp_called_name_6 );
                    Py_DECREF( tmp_args_element_name_6 );
                    if ( tmp_call_result_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 169;
                        type_description_1 = "ooocooocoooo";
                        goto try_except_handler_2;
                    }
                    Py_DECREF( tmp_call_result_3 );
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    nuitka_bool tmp_condition_result_7;
                    PyObject *tmp_isinstance_inst_2;
                    PyObject *tmp_isinstance_cls_2;
                    CHECK_OBJECT( var_v );
                    tmp_isinstance_inst_2 = var_v;
                    tmp_isinstance_cls_2 = (PyObject *)&PyDict_Type;
                    tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 185;
                        type_description_1 = "ooocooocoooo";
                        goto try_except_handler_2;
                    }
                    tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_assign_source_13;
                        nuitka_bool tmp_condition_result_8;
                        PyObject *tmp_compexpr_left_4;
                        PyObject *tmp_compexpr_right_4;
                        PyObject *tmp_source_name_8;
                        CHECK_OBJECT( PyCell_GET( var_retain ) );
                        tmp_compexpr_left_4 = PyCell_GET( var_retain );
                        tmp_compexpr_right_4 = Py_True;
                        tmp_condition_result_8 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                        {
                            goto condexpr_true_3;
                        }
                        else
                        {
                            goto condexpr_false_3;
                        }
                        condexpr_true_3:;
                        CHECK_OBJECT( var_v );
                        tmp_source_name_8 = var_v;
                        tmp_assign_source_13 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_8 );
                        if ( tmp_assign_source_13 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 186;
                            type_description_1 = "ooocooocoooo";
                            goto try_except_handler_2;
                        }
                        goto condexpr_end_3;
                        condexpr_false_3:;
                        tmp_assign_source_13 = (PyObject *)&PyDict_Type;
                        Py_INCREF( tmp_assign_source_13 );
                        condexpr_end_3:;
                        {
                            PyObject *old = var_df;
                            var_df = tmp_assign_source_13;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_called_name_10;
                        PyObject *tmp_source_name_9;
                        PyObject *tmp_call_result_5;
                        PyObject *tmp_args_element_name_9;
                        PyObject *tmp_called_name_11;
                        PyObject *tmp_args_element_name_10;
                        CHECK_OBJECT( var_rv );
                        tmp_source_name_9 = var_rv;
                        tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_append );
                        if ( tmp_called_name_10 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 187;
                            type_description_1 = "ooocooocoooo";
                            goto try_except_handler_2;
                        }
                        CHECK_OBJECT( var_df );
                        tmp_called_name_11 = var_df;
                        {
                            PyObject *tmp_assign_source_14;
                            PyObject *tmp_iter_arg_3;
                            PyObject *tmp_called_name_12;
                            PyObject *tmp_mvar_value_6;
                            PyObject *tmp_args_element_name_11;
                            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_iteritems );

                            if (unlikely( tmp_mvar_value_6 == NULL ))
                            {
                                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iteritems );
                            }

                            if ( tmp_mvar_value_6 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iteritems" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 205;
                                type_description_1 = "ooocooocoooo";
                                goto try_except_handler_2;
                            }

                            tmp_called_name_12 = tmp_mvar_value_6;
                            CHECK_OBJECT( var_v );
                            tmp_args_element_name_11 = var_v;
                            frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 205;
                            {
                                PyObject *call_args[] = { tmp_args_element_name_11 };
                                tmp_iter_arg_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
                            }

                            if ( tmp_iter_arg_3 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 205;
                                type_description_1 = "ooocooocoooo";
                                goto try_except_handler_2;
                            }
                            tmp_assign_source_14 = MAKE_ITERATOR( tmp_iter_arg_3 );
                            Py_DECREF( tmp_iter_arg_3 );
                            if ( tmp_assign_source_14 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 189;
                                type_description_1 = "ooocooocoooo";
                                goto try_except_handler_2;
                            }
                            {
                                PyObject *old = tmp_genexpr_1__$0;
                                tmp_genexpr_1__$0 = tmp_assign_source_14;
                                Py_XDECREF( old );
                            }

                        }
                        // Tried code:
                        tmp_args_element_name_10 = attr$_funcs$$$function_3_astuple$$$genexpr_1_genexpr_maker();

                        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_10)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );
                        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_10)->m_closure[1] = var_retain;
                        Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_10)->m_closure[1] );
                        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_10)->m_closure[2] = par_tuple_factory;
                        Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_10)->m_closure[2] );


                        goto try_return_handler_5;
                        // tried codes exits in all cases
                        NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_3_astuple );
                        return NULL;
                        // Return handler code:
                        try_return_handler_5:;
                        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                        Py_DECREF( tmp_genexpr_1__$0 );
                        tmp_genexpr_1__$0 = NULL;

                        goto outline_result_2;
                        // End of try:
                        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                        Py_DECREF( tmp_genexpr_1__$0 );
                        tmp_genexpr_1__$0 = NULL;

                        // Return statement must have exited already.
                        NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_3_astuple );
                        return NULL;
                        outline_result_2:;
                        frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 188;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_10 };
                            tmp_args_element_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
                        }

                        Py_DECREF( tmp_args_element_name_10 );
                        if ( tmp_args_element_name_9 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_called_name_10 );

                            exception_lineno = 188;
                            type_description_1 = "ooocooocoooo";
                            goto try_except_handler_2;
                        }
                        frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 187;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_9 };
                            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
                        }

                        Py_DECREF( tmp_called_name_10 );
                        Py_DECREF( tmp_args_element_name_9 );
                        if ( tmp_call_result_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 187;
                            type_description_1 = "ooocooocoooo";
                            goto try_except_handler_2;
                        }
                        Py_DECREF( tmp_call_result_5 );
                    }
                    goto branch_end_5;
                    branch_no_5:;
                    {
                        PyObject *tmp_called_instance_1;
                        PyObject *tmp_call_result_6;
                        PyObject *tmp_args_element_name_12;
                        CHECK_OBJECT( var_rv );
                        tmp_called_instance_1 = var_rv;
                        CHECK_OBJECT( var_v );
                        tmp_args_element_name_12 = var_v;
                        frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 209;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_12 };
                            tmp_call_result_6 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
                        }

                        if ( tmp_call_result_6 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 209;
                            type_description_1 = "ooocooocoooo";
                            goto try_except_handler_2;
                        }
                        Py_DECREF( tmp_call_result_6 );
                    }
                    branch_end_5:;
                }
                branch_end_4:;
            }
            branch_end_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_call_result_7;
            PyObject *tmp_args_element_name_13;
            CHECK_OBJECT( var_rv );
            tmp_called_instance_2 = var_rv;
            CHECK_OBJECT( var_v );
            tmp_args_element_name_13 = var_v;
            frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 211;
            {
                PyObject *call_args[] = { tmp_args_element_name_13 };
                tmp_call_result_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, call_args );
            }

            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "ooocooocoooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        branch_end_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 152;
        type_description_1 = "ooocooocoooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        PyObject *tmp_called_name_13;
        PyObject *tmp_args_element_name_14;
        CHECK_OBJECT( PyCell_GET( par_tuple_factory ) );
        tmp_compexpr_left_5 = PyCell_GET( par_tuple_factory );
        tmp_compexpr_right_5 = (PyObject *)&PyList_Type;
        tmp_condition_result_9 = ( tmp_compexpr_left_5 == tmp_compexpr_right_5 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( var_rv );
        tmp_return_value = var_rv;
        Py_INCREF( tmp_return_value );
        goto condexpr_end_4;
        condexpr_false_4:;
        CHECK_OBJECT( PyCell_GET( par_tuple_factory ) );
        tmp_called_name_13 = PyCell_GET( par_tuple_factory );
        CHECK_OBJECT( var_rv );
        tmp_args_element_name_14 = var_rv;
        frame_7d0311866830c038650d360f68da8fd3->m_frame.f_lineno = 212;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "ooocooocoooo";
            goto frame_exception_exit_1;
        }
        condexpr_end_4:;
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7d0311866830c038650d360f68da8fd3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7d0311866830c038650d360f68da8fd3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7d0311866830c038650d360f68da8fd3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7d0311866830c038650d360f68da8fd3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7d0311866830c038650d360f68da8fd3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7d0311866830c038650d360f68da8fd3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7d0311866830c038650d360f68da8fd3,
        type_description_1,
        par_inst,
        par_recurse,
        par_filter,
        par_tuple_factory,
        par_retain_collection_types,
        var_attrs,
        var_rv,
        var_retain,
        var_a,
        var_v,
        var_cf,
        var_df
    );


    // Release cached frame.
    if ( frame_7d0311866830c038650d360f68da8fd3 == cache_frame_7d0311866830c038650d360f68da8fd3 )
    {
        Py_DECREF( frame_7d0311866830c038650d360f68da8fd3 );
    }
    cache_frame_7d0311866830c038650d360f68da8fd3 = NULL;

    assertFrameObject( frame_7d0311866830c038650d360f68da8fd3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_3_astuple );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_inst );
    Py_DECREF( par_inst );
    par_inst = NULL;

    CHECK_OBJECT( (PyObject *)par_recurse );
    Py_DECREF( par_recurse );
    par_recurse = NULL;

    CHECK_OBJECT( (PyObject *)par_filter );
    Py_DECREF( par_filter );
    par_filter = NULL;

    CHECK_OBJECT( (PyObject *)par_tuple_factory );
    Py_DECREF( par_tuple_factory );
    par_tuple_factory = NULL;

    CHECK_OBJECT( (PyObject *)par_retain_collection_types );
    Py_DECREF( par_retain_collection_types );
    par_retain_collection_types = NULL;

    CHECK_OBJECT( (PyObject *)var_attrs );
    Py_DECREF( var_attrs );
    var_attrs = NULL;

    CHECK_OBJECT( (PyObject *)var_rv );
    Py_DECREF( var_rv );
    var_rv = NULL;

    CHECK_OBJECT( (PyObject *)var_retain );
    Py_DECREF( var_retain );
    var_retain = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    Py_XDECREF( var_cf );
    var_cf = NULL;

    Py_XDECREF( var_df );
    var_df = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_inst );
    Py_DECREF( par_inst );
    par_inst = NULL;

    CHECK_OBJECT( (PyObject *)par_recurse );
    Py_DECREF( par_recurse );
    par_recurse = NULL;

    CHECK_OBJECT( (PyObject *)par_filter );
    Py_DECREF( par_filter );
    par_filter = NULL;

    CHECK_OBJECT( (PyObject *)par_tuple_factory );
    Py_DECREF( par_tuple_factory );
    par_tuple_factory = NULL;

    CHECK_OBJECT( (PyObject *)par_retain_collection_types );
    Py_DECREF( par_retain_collection_types );
    par_retain_collection_types = NULL;

    Py_XDECREF( var_attrs );
    var_attrs = NULL;

    Py_XDECREF( var_rv );
    var_rv = NULL;

    CHECK_OBJECT( (PyObject *)var_retain );
    Py_DECREF( var_retain );
    var_retain = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    Py_XDECREF( var_cf );
    var_cf = NULL;

    Py_XDECREF( var_df );
    var_df = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_3_astuple );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct attr$_funcs$$$function_3_astuple$$$genexpr_1_genexpr_locals {
    PyObject *var_kk;
    PyObject *var_vv;
    PyObject *tmp_iter_value_0;
    PyObject *tmp_tuple_unpack_1__element_1;
    PyObject *tmp_tuple_unpack_1__element_2;
    PyObject *tmp_tuple_unpack_1__source_iter;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    int exception_keeper_lineno_4;
};

static PyObject *attr$_funcs$$$function_3_astuple$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct attr$_funcs$$$function_3_astuple$$$genexpr_1_genexpr_locals *generator_heap = (struct attr$_funcs$$$function_3_astuple$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_kk = NULL;
    generator_heap->var_vv = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_4b60d7e3227fc1c91079c373953e6021, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noocc";
                generator_heap->exception_lineno = 189;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_iter_arg_1 = generator_heap->tmp_iter_value_0;
        tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 189;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_3;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__source_iter;
            generator_heap->tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noocc";
            generator_heap->exception_lineno = 189;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_1;
            generator_heap->tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = generator_heap->tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                generator_heap->exception_type = PyExc_StopIteration;
                Py_INCREF( generator_heap->exception_type );
                generator_heap->exception_value = NULL;
                generator_heap->exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            }


            generator_heap->type_description_1 = "Noocc";
            generator_heap->exception_lineno = 189;
            goto try_except_handler_4;
        }
        {
            PyObject *old = generator_heap->tmp_tuple_unpack_1__element_2;
            generator_heap->tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = generator_heap->tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        generator_heap->tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( generator_heap->tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

                    generator_heap->type_description_1 = "Noocc";
                    generator_heap->exception_lineno = 189;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( generator_heap->tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );

            generator_heap->type_description_1 = "Noocc";
            generator_heap->exception_lineno = 189;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)generator_heap->tmp_tuple_unpack_1__source_iter );
    Py_DECREF( generator_heap->tmp_tuple_unpack_1__source_iter );
    generator_heap->tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = generator_heap->tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = generator_heap->var_kk;
            generator_heap->var_kk = tmp_assign_source_5;
            Py_INCREF( generator_heap->var_kk );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_1 );
    generator_heap->tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( generator_heap->tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = generator_heap->tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = generator_heap->var_vv;
            generator_heap->var_vv = tmp_assign_source_6;
            Py_INCREF( generator_heap->var_vv );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( generator_heap->tmp_tuple_unpack_1__element_2 );
    generator_heap->tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_tuple_element_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        int tmp_truth_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_2;
        int tmp_truth_name_2;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_name_2;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_has );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 195;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_kk );
        tmp_source_name_1 = generator_heap->var_kk;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 195;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        generator->m_frame->m_frame.f_lineno = 195;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 195;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_call_result_1 );

            generator_heap->exception_lineno = 195;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_astuple );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_astuple );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "astuple" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( generator_heap->var_kk );
        tmp_tuple_element_2 = generator_heap->var_kk;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
        tmp_dict_key_1 = const_str_plain_tuple_factory;
        if ( PyCell_GET( generator->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_args_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "tuple_factory" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 192;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_dict_value_1 = PyCell_GET( generator->m_closure[2] );
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(generator_heap->tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_retain_collection_types;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "retain" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 193;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_dict_value_2 = PyCell_GET( generator->m_closure[1] );
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(generator_heap->tmp_res != 0) );
        generator->m_frame->m_frame.f_lineno = 190;
        tmp_tuple_element_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 190;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( generator_heap->var_kk );
        tmp_tuple_element_1 = generator_heap->var_kk;
        Py_INCREF( tmp_tuple_element_1 );
        condexpr_end_1:;
        tmp_expression_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_expression_name_1, 0, tmp_tuple_element_1 );
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_has );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 202;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        CHECK_OBJECT( generator_heap->var_vv );
        tmp_source_name_2 = generator_heap->var_vv;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_2 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_expression_name_1 );

            generator_heap->exception_lineno = 202;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        generator->m_frame->m_frame.f_lineno = 202;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_expression_name_1 );

            generator_heap->exception_lineno = 202;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_expression_name_1 );
            Py_DECREF( tmp_call_result_2 );

            generator_heap->exception_lineno = 202;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_astuple );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_astuple );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "astuple" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 197;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_called_name_4 = tmp_mvar_value_4;
        CHECK_OBJECT( generator_heap->var_vv );
        tmp_tuple_element_3 = generator_heap->var_vv;
        tmp_args_name_2 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_3 );
        PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
        tmp_dict_key_3 = const_str_plain_tuple_factory;
        if ( PyCell_GET( generator->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            Py_DECREF( tmp_args_name_2 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "tuple_factory" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 199;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_dict_value_3 = PyCell_GET( generator->m_closure[2] );
        tmp_kw_name_2 = _PyDict_NewPresized( 2 );
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(generator_heap->tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_retain_collection_types;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_expression_name_1 );
            Py_DECREF( tmp_args_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "retain" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 200;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }

        tmp_dict_value_4 = PyCell_GET( generator->m_closure[1] );
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(generator_heap->tmp_res != 0) );
        generator->m_frame->m_frame.f_lineno = 197;
        tmp_tuple_element_1 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_args_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_expression_name_1 );

            generator_heap->exception_lineno = 197;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        CHECK_OBJECT( generator_heap->var_vv );
        tmp_tuple_element_1 = generator_heap->var_vv;
        Py_INCREF( tmp_tuple_element_1 );
        condexpr_end_2:;
        PyTuple_SET_ITEM( tmp_expression_name_1, 1, tmp_tuple_element_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_name_1, sizeof(PyObject *), &tmp_tuple_element_2, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), &tmp_dict_key_1, sizeof(PyObject *), &tmp_dict_value_1, sizeof(PyObject *), &tmp_dict_key_2, sizeof(PyObject *), &tmp_dict_value_2, sizeof(PyObject *), &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_called_name_3, sizeof(PyObject *), &tmp_mvar_value_3, sizeof(PyObject *), &tmp_call_result_2, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_truth_name_2, sizeof(int), &tmp_called_name_4, sizeof(PyObject *), &tmp_mvar_value_4, sizeof(PyObject *), &tmp_args_name_2, sizeof(PyObject *), &tmp_tuple_element_3, sizeof(PyObject *), &tmp_kw_name_2, sizeof(PyObject *), &tmp_dict_key_3, sizeof(PyObject *), &tmp_dict_value_3, sizeof(PyObject *), &tmp_dict_key_4, sizeof(PyObject *), &tmp_dict_value_4, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_tuple_element_1, sizeof(PyObject *), &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_call_result_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_called_name_2, sizeof(PyObject *), &tmp_mvar_value_2, sizeof(PyObject *), &tmp_args_name_1, sizeof(PyObject *), &tmp_tuple_element_2, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), &tmp_dict_key_1, sizeof(PyObject *), &tmp_dict_value_1, sizeof(PyObject *), &tmp_dict_key_2, sizeof(PyObject *), &tmp_dict_value_2, sizeof(PyObject *), &tmp_condition_result_2, sizeof(nuitka_bool), &tmp_called_name_3, sizeof(PyObject *), &tmp_mvar_value_3, sizeof(PyObject *), &tmp_call_result_2, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), &tmp_source_name_2, sizeof(PyObject *), &tmp_truth_name_2, sizeof(int), &tmp_called_name_4, sizeof(PyObject *), &tmp_mvar_value_4, sizeof(PyObject *), &tmp_args_name_2, sizeof(PyObject *), &tmp_tuple_element_3, sizeof(PyObject *), &tmp_kw_name_2, sizeof(PyObject *), &tmp_dict_key_3, sizeof(PyObject *), &tmp_dict_value_3, sizeof(PyObject *), &tmp_dict_key_4, sizeof(PyObject *), &tmp_dict_value_4, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 189;
            generator_heap->type_description_1 = "Noocc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 189;
        generator_heap->type_description_1 = "Noocc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_3 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_3 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_3 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_3 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_3;
    generator_heap->exception_value = generator_heap->exception_keeper_value_3;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_3;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_kk,
            generator_heap->var_vv,
            generator->m_closure[2],
            generator->m_closure[1]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_4 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_4 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_4 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_4 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_kk );
    generator_heap->var_kk = NULL;

    Py_XDECREF( generator_heap->var_vv );
    generator_heap->var_vv = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_4;
    generator_heap->exception_value = generator_heap->exception_keeper_value_4;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_4;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:
    try_end_4:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_kk );
    generator_heap->var_kk = NULL;

    Py_XDECREF( generator_heap->var_vv );
    generator_heap->var_vv = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *attr$_funcs$$$function_3_astuple$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        attr$_funcs$$$function_3_astuple$$$genexpr_1_genexpr_context,
        module_attr$_funcs,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_6419a49fb159320db882dce19dfd2cd9,
#endif
        codeobj_4b60d7e3227fc1c91079c373953e6021,
        3,
        sizeof(struct attr$_funcs$$$function_3_astuple$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_attr$_funcs$$$function_4_has( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0ef64ddd35fb82e9e752d760c7c29fc2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0ef64ddd35fb82e9e752d760c7c29fc2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0ef64ddd35fb82e9e752d760c7c29fc2, codeobj_0ef64ddd35fb82e9e752d760c7c29fc2, module_attr$_funcs, sizeof(void *) );
    frame_0ef64ddd35fb82e9e752d760c7c29fc2 = cache_frame_0ef64ddd35fb82e9e752d760c7c29fc2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0ef64ddd35fb82e9e752d760c7c29fc2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0ef64ddd35fb82e9e752d760c7c29fc2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_getattr_default_1;
        CHECK_OBJECT( par_cls );
        tmp_getattr_target_1 = par_cls;
        tmp_getattr_attr_1 = const_str_plain___attrs_attrs__;
        tmp_getattr_default_1 = Py_None;
        tmp_compexpr_left_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 224;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_return_value = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? Py_True : Py_False;
        Py_DECREF( tmp_compexpr_left_1 );
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0ef64ddd35fb82e9e752d760c7c29fc2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0ef64ddd35fb82e9e752d760c7c29fc2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0ef64ddd35fb82e9e752d760c7c29fc2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0ef64ddd35fb82e9e752d760c7c29fc2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0ef64ddd35fb82e9e752d760c7c29fc2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0ef64ddd35fb82e9e752d760c7c29fc2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0ef64ddd35fb82e9e752d760c7c29fc2,
        type_description_1,
        par_cls
    );


    // Release cached frame.
    if ( frame_0ef64ddd35fb82e9e752d760c7c29fc2 == cache_frame_0ef64ddd35fb82e9e752d760c7c29fc2 )
    {
        Py_DECREF( frame_0ef64ddd35fb82e9e752d760c7c29fc2 );
    }
    cache_frame_0ef64ddd35fb82e9e752d760c7c29fc2 = NULL;

    assertFrameObject( frame_0ef64ddd35fb82e9e752d760c7c29fc2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_4_has );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_4_has );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_attr$_funcs$$$function_5_assoc( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_inst = python_pars[ 0 ];
    PyObject *par_changes = python_pars[ 1 ];
    PyObject *var_warnings = NULL;
    PyObject *var_new = NULL;
    PyObject *var_attrs = NULL;
    PyObject *var_k = NULL;
    PyObject *var_v = NULL;
    PyObject *var_a = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_578fc6341ba7c2a7a428194177901142;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_578fc6341ba7c2a7a428194177901142 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_578fc6341ba7c2a7a428194177901142, codeobj_578fc6341ba7c2a7a428194177901142, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_578fc6341ba7c2a7a428194177901142 = cache_frame_578fc6341ba7c2a7a428194177901142;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_578fc6341ba7c2a7a428194177901142 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_578fc6341ba7c2a7a428194177901142 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_warnings;
        tmp_globals_name_1 = (PyObject *)moduledict_attr$_funcs;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_578fc6341ba7c2a7a428194177901142->m_frame.f_lineno = 244;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 244;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_warnings == NULL );
        var_warnings = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( var_warnings );
        tmp_source_name_1 = var_warnings;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = const_str_digest_8e2c97f4f26f8b58fe61145ca0cacfef;
        tmp_args_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DeprecationWarning );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DeprecationWarning" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 248;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_1 = tmp_mvar_value_1;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_f154c9a58c9419d7e391901d7b7fe49e );
        frame_578fc6341ba7c2a7a428194177901142->m_frame.f_lineno = 246;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_copy );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_copy );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "copy" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 251;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_inst );
        tmp_args_element_name_1 = par_inst;
        frame_578fc6341ba7c2a7a428194177901142->m_frame.f_lineno = 251;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_copy, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_new == NULL );
        var_new = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_fields );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fields );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fields" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 252;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        CHECK_OBJECT( par_inst );
        tmp_source_name_2 = par_inst;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_2 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 252;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_578fc6341ba7c2a7a428194177901142->m_frame.f_lineno = 252;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 252;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_attrs == NULL );
        var_attrs = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_iteritems );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iteritems );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iteritems" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 253;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_4;
        CHECK_OBJECT( par_changes );
        tmp_args_element_name_3 = par_changes;
        frame_578fc6341ba7c2a7a428194177901142->m_frame.f_lineno = 253;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 253;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 253;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_4;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_5 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 253;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_iter_arg_2;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
        tmp_assign_source_6 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 253;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_7 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 253;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_8 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooo";
            exception_lineno = 253;
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooo";
                    exception_lineno = 253;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooo";
            exception_lineno = 253;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_9 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_k;
            var_k = tmp_assign_source_9;
            Py_INCREF( var_k );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_10;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_10 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_v;
            var_v = tmp_assign_source_10;
            Py_INCREF( var_v );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_getattr_default_1;
        PyObject *tmp_mvar_value_5;
        CHECK_OBJECT( var_attrs );
        tmp_getattr_target_1 = var_attrs;
        CHECK_OBJECT( var_k );
        tmp_getattr_attr_1 = var_k;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_NOTHING );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NOTHING );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NOTHING" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 254;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }

        tmp_getattr_default_1 = tmp_mvar_value_5;
        tmp_assign_source_11 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 254;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_a;
            var_a = tmp_assign_source_11;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_6;
        CHECK_OBJECT( var_a );
        tmp_compexpr_left_1 = var_a;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_NOTHING );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NOTHING );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NOTHING" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 255;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_6;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_called_name_4;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_3;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_source_name_4;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_AttrsAttributeNotFoundError );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AttrsAttributeNotFoundError );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AttrsAttributeNotFoundError" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 256;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }

            tmp_called_name_4 = tmp_mvar_value_7;
            tmp_source_name_3 = const_str_digest_0868412c7596b0aba8b59b68b966a79c;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_format );
            assert( !(tmp_called_name_5 == NULL) );
            tmp_dict_key_1 = const_str_plain_k;
            CHECK_OBJECT( var_k );
            tmp_dict_value_1 = var_k;
            tmp_kw_name_2 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_1, tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_2 = const_str_plain_cl;
            CHECK_OBJECT( var_new );
            tmp_source_name_4 = var_new;
            tmp_dict_value_2 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_4 );
            if ( tmp_dict_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );
                Py_DECREF( tmp_kw_name_2 );

                exception_lineno = 258;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            frame_578fc6341ba7c2a7a428194177901142->m_frame.f_lineno = 257;
            tmp_args_element_name_4 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_5, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 257;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            frame_578fc6341ba7c2a7a428194177901142->m_frame.f_lineno = 256;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 256;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 256;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain__obj_setattr );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__obj_setattr );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_obj_setattr" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 261;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }

        tmp_called_name_6 = tmp_mvar_value_8;
        CHECK_OBJECT( var_new );
        tmp_args_element_name_5 = var_new;
        CHECK_OBJECT( var_k );
        tmp_args_element_name_6 = var_k;
        CHECK_OBJECT( var_v );
        tmp_args_element_name_7 = var_v;
        frame_578fc6341ba7c2a7a428194177901142->m_frame.f_lineno = 261;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 261;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 253;
        type_description_1 = "oooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_578fc6341ba7c2a7a428194177901142 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_578fc6341ba7c2a7a428194177901142 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_578fc6341ba7c2a7a428194177901142, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_578fc6341ba7c2a7a428194177901142->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_578fc6341ba7c2a7a428194177901142, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_578fc6341ba7c2a7a428194177901142,
        type_description_1,
        par_inst,
        par_changes,
        var_warnings,
        var_new,
        var_attrs,
        var_k,
        var_v,
        var_a
    );


    // Release cached frame.
    if ( frame_578fc6341ba7c2a7a428194177901142 == cache_frame_578fc6341ba7c2a7a428194177901142 )
    {
        Py_DECREF( frame_578fc6341ba7c2a7a428194177901142 );
    }
    cache_frame_578fc6341ba7c2a7a428194177901142 = NULL;

    assertFrameObject( frame_578fc6341ba7c2a7a428194177901142 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_new );
    tmp_return_value = var_new;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_5_assoc );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_inst );
    Py_DECREF( par_inst );
    par_inst = NULL;

    CHECK_OBJECT( (PyObject *)par_changes );
    Py_DECREF( par_changes );
    par_changes = NULL;

    CHECK_OBJECT( (PyObject *)var_warnings );
    Py_DECREF( var_warnings );
    var_warnings = NULL;

    CHECK_OBJECT( (PyObject *)var_new );
    Py_DECREF( var_new );
    var_new = NULL;

    CHECK_OBJECT( (PyObject *)var_attrs );
    Py_DECREF( var_attrs );
    var_attrs = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_inst );
    Py_DECREF( par_inst );
    par_inst = NULL;

    CHECK_OBJECT( (PyObject *)par_changes );
    Py_DECREF( par_changes );
    par_changes = NULL;

    Py_XDECREF( var_warnings );
    var_warnings = NULL;

    Py_XDECREF( var_new );
    var_new = NULL;

    Py_XDECREF( var_attrs );
    var_attrs = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_5_assoc );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_attr$_funcs$$$function_6_evolve( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_inst = python_pars[ 0 ];
    PyObject *par_changes = python_pars[ 1 ];
    PyObject *var_cls = NULL;
    PyObject *var_attrs = NULL;
    PyObject *var_a = NULL;
    PyObject *var_attr_name = NULL;
    PyObject *var_init_name = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_a46b62abc6e63351549890145355cab5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_a46b62abc6e63351549890145355cab5 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a46b62abc6e63351549890145355cab5, codeobj_a46b62abc6e63351549890145355cab5, module_attr$_funcs, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a46b62abc6e63351549890145355cab5 = cache_frame_a46b62abc6e63351549890145355cab5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a46b62abc6e63351549890145355cab5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a46b62abc6e63351549890145355cab5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_inst );
        tmp_source_name_1 = par_inst;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 281;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_cls == NULL );
        var_cls = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_fields );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_fields );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "fields" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 282;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_cls );
        tmp_args_element_name_1 = var_cls;
        frame_a46b62abc6e63351549890145355cab5->m_frame.f_lineno = 282;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 282;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_attrs == NULL );
        var_attrs = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( var_attrs );
        tmp_iter_arg_1 = var_attrs;
        tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 283;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_3;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooo";
                exception_lineno = 283;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_4;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_5 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_a;
            var_a = tmp_assign_source_5;
            Py_INCREF( var_a );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_a );
        tmp_source_name_2 = var_a;
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_init );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 284;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 284;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_start_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_a );
        tmp_source_name_3 = var_a;
        tmp_assign_source_6 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_name );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 286;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_attr_name;
            var_attr_name = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        CHECK_OBJECT( var_attr_name );
        tmp_subscribed_name_1 = var_attr_name;
        tmp_subscript_name_1 = const_int_0;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 287;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_compexpr_right_1 = const_str_plain__;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 287;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( var_attr_name );
        tmp_assign_source_7 = var_attr_name;
        Py_INCREF( tmp_assign_source_7 );
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( var_attr_name );
        tmp_subscribed_name_2 = var_attr_name;
        tmp_subscript_name_2 = const_slice_int_pos_1_none_none;
        tmp_assign_source_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 287;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        condexpr_end_1:;
        {
            PyObject *old = var_init_name;
            var_init_name = tmp_assign_source_7;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_init_name );
        tmp_compexpr_left_2 = var_init_name;
        CHECK_OBJECT( par_changes );
        tmp_compexpr_right_2 = par_changes;
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 288;
            type_description_1 = "ooooooo";
            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_getattr_target_1;
            PyObject *tmp_getattr_attr_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( par_inst );
            tmp_getattr_target_1 = par_inst;
            CHECK_OBJECT( var_attr_name );
            tmp_getattr_attr_1 = var_attr_name;
            tmp_ass_subvalue_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
            if ( tmp_ass_subvalue_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
            CHECK_OBJECT( par_changes );
            tmp_ass_subscribed_1 = par_changes;
            CHECK_OBJECT( var_init_name );
            tmp_ass_subscript_1 = var_init_name;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_1 = "ooooooo";
                goto try_except_handler_2;
            }
        }
        branch_no_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 283;
        type_description_1 = "ooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        CHECK_OBJECT( var_cls );
        tmp_dircall_arg1_1 = var_cls;
        CHECK_OBJECT( par_changes );
        tmp_dircall_arg2_1 = par_changes;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
            tmp_return_value = impl___internal__$$$function_2_complex_call_helper_star_dict( dir_call_args );
        }
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 290;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a46b62abc6e63351549890145355cab5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a46b62abc6e63351549890145355cab5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a46b62abc6e63351549890145355cab5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a46b62abc6e63351549890145355cab5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a46b62abc6e63351549890145355cab5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a46b62abc6e63351549890145355cab5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a46b62abc6e63351549890145355cab5,
        type_description_1,
        par_inst,
        par_changes,
        var_cls,
        var_attrs,
        var_a,
        var_attr_name,
        var_init_name
    );


    // Release cached frame.
    if ( frame_a46b62abc6e63351549890145355cab5 == cache_frame_a46b62abc6e63351549890145355cab5 )
    {
        Py_DECREF( frame_a46b62abc6e63351549890145355cab5 );
    }
    cache_frame_a46b62abc6e63351549890145355cab5 = NULL;

    assertFrameObject( frame_a46b62abc6e63351549890145355cab5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_6_evolve );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_inst );
    Py_DECREF( par_inst );
    par_inst = NULL;

    CHECK_OBJECT( (PyObject *)par_changes );
    Py_DECREF( par_changes );
    par_changes = NULL;

    CHECK_OBJECT( (PyObject *)var_cls );
    Py_DECREF( var_cls );
    var_cls = NULL;

    CHECK_OBJECT( (PyObject *)var_attrs );
    Py_DECREF( var_attrs );
    var_attrs = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    Py_XDECREF( var_attr_name );
    var_attr_name = NULL;

    Py_XDECREF( var_init_name );
    var_init_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_inst );
    Py_DECREF( par_inst );
    par_inst = NULL;

    CHECK_OBJECT( (PyObject *)par_changes );
    Py_DECREF( par_changes );
    par_changes = NULL;

    Py_XDECREF( var_cls );
    var_cls = NULL;

    Py_XDECREF( var_attrs );
    var_attrs = NULL;

    Py_XDECREF( var_a );
    var_a = NULL;

    Py_XDECREF( var_attr_name );
    var_attr_name = NULL;

    Py_XDECREF( var_init_name );
    var_init_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( attr$_funcs$$$function_6_evolve );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_1_asdict( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_attr$_funcs$$$function_1_asdict,
        const_str_plain_asdict,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_971fec919b0d12e93ae402a49ce447a9,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_attr$_funcs,
        const_str_digest_98b8f42d0cc096bf55b9067d47c79e4c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_2__asdict_anything(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_attr$_funcs$$$function_2__asdict_anything,
        const_str_plain__asdict_anything,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_980029849d5b392ef4a7e2137a31a6c9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_attr$_funcs,
        const_str_digest_64e4ff9f09b44528b09bbe58cbebfc7a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_3_astuple( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_attr$_funcs$$$function_3_astuple,
        const_str_plain_astuple,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7d0311866830c038650d360f68da8fd3,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_attr$_funcs,
        const_str_digest_634a33e8654ffef560e62de1d34622e3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_4_has(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_attr$_funcs$$$function_4_has,
        const_str_plain_has,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0ef64ddd35fb82e9e752d760c7c29fc2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_attr$_funcs,
        const_str_digest_e5dfe7e5fac48bd4bcde33eaf5a83dad,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_5_assoc(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_attr$_funcs$$$function_5_assoc,
        const_str_plain_assoc,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_578fc6341ba7c2a7a428194177901142,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_attr$_funcs,
        const_str_digest_2d81ff510cc484c9f4116610439f0620,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_attr$_funcs$$$function_6_evolve(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_attr$_funcs$$$function_6_evolve,
        const_str_plain_evolve,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a46b62abc6e63351549890145355cab5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_attr$_funcs,
        const_str_digest_e77bb25df3fe1ff897e0dd855d2a2eb1,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_attr$_funcs =
{
    PyModuleDef_HEAD_INIT,
    "attr._funcs",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(attr$_funcs)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(attr$_funcs)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_attr$_funcs );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("attr._funcs: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("attr._funcs: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("attr._funcs: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initattr$_funcs" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_attr$_funcs = Py_InitModule4(
        "attr._funcs",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_attr$_funcs = PyModule_Create( &mdef_attr$_funcs );
#endif

    moduledict_attr$_funcs = MODULE_DICT( module_attr$_funcs );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_attr$_funcs,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_attr$_funcs,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_attr$_funcs,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_attr$_funcs,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_attr$_funcs );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_0cf0b36801d2e126e0c369f2885f747d, module_attr$_funcs );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_c4106b694f9a60e8b66fc9818555f8e6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_c4106b694f9a60e8b66fc9818555f8e6 = MAKE_MODULE_FRAME( codeobj_c4106b694f9a60e8b66fc9818555f8e6, module_attr$_funcs );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_c4106b694f9a60e8b66fc9818555f8e6 );
    assert( Py_REFCNT( frame_c4106b694f9a60e8b66fc9818555f8e6 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        frame_c4106b694f9a60e8b66fc9818555f8e6->m_frame.f_lineno = 1;
        tmp_assign_source_4 = PyImport_ImportModule("__future__");
        assert( !(tmp_assign_source_4 == NULL) );
        assert( tmp_import_from_1__module == NULL );
        Py_INCREF( tmp_assign_source_4 );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_absolute_import );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_absolute_import, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_division );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_division, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_print_function );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_print_function, tmp_assign_source_7 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_copy;
        tmp_globals_name_1 = (PyObject *)moduledict_attr$_funcs;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_c4106b694f9a60e8b66fc9818555f8e6->m_frame.f_lineno = 3;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_copy, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain__compat;
        tmp_globals_name_2 = (PyObject *)moduledict_attr$_funcs;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_iteritems_tuple;
        tmp_level_name_2 = const_int_pos_1;
        frame_c4106b694f9a60e8b66fc9818555f8e6->m_frame.f_lineno = 5;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_4 ) )
        {
           tmp_assign_source_9 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_4,
                (PyObject *)moduledict_attr$_funcs,
                const_str_plain_iteritems,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_iteritems );
        }

        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_iteritems, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain__make;
        tmp_globals_name_3 = (PyObject *)moduledict_attr$_funcs;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_NOTHING_str_plain__obj_setattr_str_plain_fields_tuple;
        tmp_level_name_3 = const_int_pos_1;
        frame_c4106b694f9a60e8b66fc9818555f8e6->m_frame.f_lineno = 6;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_10;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_5 ) )
        {
           tmp_assign_source_11 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_5,
                (PyObject *)moduledict_attr$_funcs,
                const_str_plain_NOTHING,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_NOTHING );
        }

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_NOTHING, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_6 ) )
        {
           tmp_assign_source_12 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_6,
                (PyObject *)moduledict_attr$_funcs,
                const_str_plain__obj_setattr,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain__obj_setattr );
        }

        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain__obj_setattr, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_7 ) )
        {
           tmp_assign_source_13 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_7,
                (PyObject *)moduledict_attr$_funcs,
                const_str_plain_fields,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_fields );
        }

        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_fields, tmp_assign_source_13 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_exceptions;
        tmp_globals_name_4 = (PyObject *)moduledict_attr$_funcs;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_AttrsAttributeNotFoundError_tuple;
        tmp_level_name_4 = const_int_pos_1;
        frame_c4106b694f9a60e8b66fc9818555f8e6->m_frame.f_lineno = 7;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_8 ) )
        {
           tmp_assign_source_14 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_8,
                (PyObject *)moduledict_attr$_funcs,
                const_str_plain_AttrsAttributeNotFoundError,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_AttrsAttributeNotFoundError );
        }

        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_AttrsAttributeNotFoundError, tmp_assign_source_14 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c4106b694f9a60e8b66fc9818555f8e6 );
#endif
    popFrameStack();

    assertFrameObject( frame_c4106b694f9a60e8b66fc9818555f8e6 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c4106b694f9a60e8b66fc9818555f8e6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c4106b694f9a60e8b66fc9818555f8e6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c4106b694f9a60e8b66fc9818555f8e6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c4106b694f9a60e8b66fc9818555f8e6, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_true_none_type_dict_false_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_15 = MAKE_FUNCTION_attr$_funcs$$$function_1_asdict( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_asdict, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_attr$_funcs$$$function_2__asdict_anything(  );



        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain__asdict_anything, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_true_none_type_tuple_false_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_17 = MAKE_FUNCTION_attr$_funcs$$$function_3_astuple( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_astuple, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_attr$_funcs$$$function_4_has(  );



        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_has, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_attr$_funcs$$$function_5_assoc(  );



        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_assoc, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = MAKE_FUNCTION_attr$_funcs$$$function_6_evolve(  );



        UPDATE_STRING_DICT1( moduledict_attr$_funcs, (Nuitka_StringObject *)const_str_plain_evolve, tmp_assign_source_20 );
    }

    return MOD_RETURN_VALUE( module_attr$_funcs );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
