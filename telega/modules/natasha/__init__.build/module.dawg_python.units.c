/* Generated code for Python module 'dawg_python.units'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_dawg_python$units" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_dawg_python$units;
PyDictObject *moduledict_dawg_python$units;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_a6b92e40b51243703808ce6ae1609571;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_int_pos_4294967295;
static PyObject *const_str_digest_ed9413380dd5c74bf53a8301dd5841cc;
extern PyObject *const_str_plain_label;
static PyObject *const_str_plain_HAS_LEAF_BIT;
static PyObject *const_tuple_str_plain_base_tuple;
extern PyObject *const_int_pos_256;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_offset;
static PyObject *const_int_pos_2097152;
static PyObject *const_tuple_str_plain_base_str_plain__mask_tuple;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_PRECISION_MASK;
static PyObject *const_str_digest_8fa87a1f3e7e28b740e5095a341e0207;
static PyObject *const_str_digest_c396101cdc024a026ddf3273e9f33fe5;
extern PyObject *const_int_pos_2147483648;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain__mask;
extern PyObject *const_int_pos_255;
extern PyObject *const_str_plain_origin;
extern PyObject *const_int_pos_6;
static PyObject *const_str_digest_3f7fa152c87f08e5ca3a982ac829b6b1;
static PyObject *const_str_plain_IS_LEAF_BIT;
static PyObject *const_str_plain_EXTENSION_BIT;
static PyObject *const_str_digest_e66a3fe6cba10d4ddfebb586c8bdc7bc;
extern PyObject *const_tuple_empty;
extern PyObject *const_int_pos_512;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_base;
extern PyObject *const_str_plain_has_leaf;
static PyObject *const_str_digest_843f71d44fc11ae30951300243286951;
extern PyObject *const_int_pos_10;
static PyObject *const_str_digest_e00085e6f2e404eb0f43d93e484eb18f;
extern PyObject *const_str_plain_absolute_import;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_plain_OFFSET_MAX;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_a6b92e40b51243703808ce6ae1609571 = UNSTREAM_STRING_ASCII( &constant_bin[ 656773 ], 22, 0 );
    const_str_digest_ed9413380dd5c74bf53a8301dd5841cc = UNSTREAM_STRING_ASCII( &constant_bin[ 656795 ], 47, 0 );
    const_str_plain_HAS_LEAF_BIT = UNSTREAM_STRING_ASCII( &constant_bin[ 656842 ], 12, 1 );
    const_tuple_str_plain_base_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_base_tuple, 0, const_str_plain_base ); Py_INCREF( const_str_plain_base );
    const_int_pos_2097152 = PyLong_FromUnsignedLong( 2097152ul );
    const_tuple_str_plain_base_str_plain__mask_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_base_str_plain__mask_tuple, 0, const_str_plain_base ); Py_INCREF( const_str_plain_base );
    PyTuple_SET_ITEM( const_tuple_str_plain_base_str_plain__mask_tuple, 1, const_str_plain__mask ); Py_INCREF( const_str_plain__mask );
    const_str_digest_8fa87a1f3e7e28b740e5095a341e0207 = UNSTREAM_STRING_ASCII( &constant_bin[ 656854 ], 47, 0 );
    const_str_digest_c396101cdc024a026ddf3273e9f33fe5 = UNSTREAM_STRING_ASCII( &constant_bin[ 656901 ], 53, 0 );
    const_str_digest_3f7fa152c87f08e5ca3a982ac829b6b1 = UNSTREAM_STRING_ASCII( &constant_bin[ 656954 ], 20, 0 );
    const_str_plain_IS_LEAF_BIT = UNSTREAM_STRING_ASCII( &constant_bin[ 656974 ], 11, 1 );
    const_str_plain_EXTENSION_BIT = UNSTREAM_STRING_ASCII( &constant_bin[ 656985 ], 13, 1 );
    const_str_digest_e66a3fe6cba10d4ddfebb586c8bdc7bc = UNSTREAM_STRING_ASCII( &constant_bin[ 656998 ], 26, 0 );
    const_str_digest_843f71d44fc11ae30951300243286951 = UNSTREAM_STRING_ASCII( &constant_bin[ 657006 ], 17, 0 );
    const_str_digest_e00085e6f2e404eb0f43d93e484eb18f = UNSTREAM_STRING_ASCII( &constant_bin[ 657024 ], 53, 0 );
    const_str_plain_OFFSET_MAX = UNSTREAM_STRING_ASCII( &constant_bin[ 657077 ], 10, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_dawg_python$units( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_93a29da5e2bdbd950b21a0da81247b90;
static PyCodeObject *codeobj_ac156c5361a53f436b3e94e7196cfd0c;
static PyCodeObject *codeobj_d5c39737a73fbc5977ed3d937c550a0b;
static PyCodeObject *codeobj_6a5f971793cb73787847eda66b00493a;
static PyCodeObject *codeobj_211ad98ffdefe7d7add21be898d76aa2;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_3f7fa152c87f08e5ca3a982ac829b6b1 );
    codeobj_93a29da5e2bdbd950b21a0da81247b90 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_e66a3fe6cba10d4ddfebb586c8bdc7bc, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_ac156c5361a53f436b3e94e7196cfd0c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_has_leaf, 15, const_tuple_str_plain_base_str_plain__mask_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d5c39737a73fbc5977ed3d937c550a0b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_label, 25, const_tuple_str_plain_base_str_plain__mask_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6a5f971793cb73787847eda66b00493a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_offset, 30, const_tuple_str_plain_base_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_211ad98ffdefe7d7add21be898d76aa2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_value, 20, const_tuple_str_plain_base_str_plain__mask_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_dawg_python$units$$$function_1_has_leaf( PyObject *defaults );


static PyObject *MAKE_FUNCTION_dawg_python$units$$$function_2_value( PyObject *defaults );


static PyObject *MAKE_FUNCTION_dawg_python$units$$$function_3_label( PyObject *defaults );


static PyObject *MAKE_FUNCTION_dawg_python$units$$$function_4_offset(  );


// The module function definitions.
static PyObject *impl_dawg_python$units$$$function_1_has_leaf( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_base = python_pars[ 0 ];
    PyObject *par__mask = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_ac156c5361a53f436b3e94e7196cfd0c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_ac156c5361a53f436b3e94e7196cfd0c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ac156c5361a53f436b3e94e7196cfd0c, codeobj_ac156c5361a53f436b3e94e7196cfd0c, module_dawg_python$units, sizeof(void *)+sizeof(void *) );
    frame_ac156c5361a53f436b3e94e7196cfd0c = cache_frame_ac156c5361a53f436b3e94e7196cfd0c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ac156c5361a53f436b3e94e7196cfd0c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ac156c5361a53f436b3e94e7196cfd0c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_value_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_base );
        tmp_left_name_1 = par_base;
        CHECK_OBJECT( par__mask );
        tmp_right_name_1 = par__mask;
        tmp_value_name_1 = BINARY_OPERATION( PyNumber_And, tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_value_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_value_name_1 );
        Py_DECREF( tmp_value_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ac156c5361a53f436b3e94e7196cfd0c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ac156c5361a53f436b3e94e7196cfd0c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ac156c5361a53f436b3e94e7196cfd0c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ac156c5361a53f436b3e94e7196cfd0c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ac156c5361a53f436b3e94e7196cfd0c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ac156c5361a53f436b3e94e7196cfd0c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ac156c5361a53f436b3e94e7196cfd0c,
        type_description_1,
        par_base,
        par__mask
    );


    // Release cached frame.
    if ( frame_ac156c5361a53f436b3e94e7196cfd0c == cache_frame_ac156c5361a53f436b3e94e7196cfd0c )
    {
        Py_DECREF( frame_ac156c5361a53f436b3e94e7196cfd0c );
    }
    cache_frame_ac156c5361a53f436b3e94e7196cfd0c = NULL;

    assertFrameObject( frame_ac156c5361a53f436b3e94e7196cfd0c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( dawg_python$units$$$function_1_has_leaf );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    CHECK_OBJECT( (PyObject *)par__mask );
    Py_DECREF( par__mask );
    par__mask = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    CHECK_OBJECT( (PyObject *)par__mask );
    Py_DECREF( par__mask );
    par__mask = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( dawg_python$units$$$function_1_has_leaf );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_dawg_python$units$$$function_2_value( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_base = python_pars[ 0 ];
    PyObject *par__mask = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_211ad98ffdefe7d7add21be898d76aa2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_211ad98ffdefe7d7add21be898d76aa2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_211ad98ffdefe7d7add21be898d76aa2, codeobj_211ad98ffdefe7d7add21be898d76aa2, module_dawg_python$units, sizeof(void *)+sizeof(void *) );
    frame_211ad98ffdefe7d7add21be898d76aa2 = cache_frame_211ad98ffdefe7d7add21be898d76aa2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_211ad98ffdefe7d7add21be898d76aa2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_211ad98ffdefe7d7add21be898d76aa2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_base );
        tmp_left_name_1 = par_base;
        CHECK_OBJECT( par__mask );
        tmp_right_name_1 = par__mask;
        tmp_return_value = BINARY_OPERATION( PyNumber_And, tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_211ad98ffdefe7d7add21be898d76aa2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_211ad98ffdefe7d7add21be898d76aa2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_211ad98ffdefe7d7add21be898d76aa2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_211ad98ffdefe7d7add21be898d76aa2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_211ad98ffdefe7d7add21be898d76aa2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_211ad98ffdefe7d7add21be898d76aa2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_211ad98ffdefe7d7add21be898d76aa2,
        type_description_1,
        par_base,
        par__mask
    );


    // Release cached frame.
    if ( frame_211ad98ffdefe7d7add21be898d76aa2 == cache_frame_211ad98ffdefe7d7add21be898d76aa2 )
    {
        Py_DECREF( frame_211ad98ffdefe7d7add21be898d76aa2 );
    }
    cache_frame_211ad98ffdefe7d7add21be898d76aa2 = NULL;

    assertFrameObject( frame_211ad98ffdefe7d7add21be898d76aa2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( dawg_python$units$$$function_2_value );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    CHECK_OBJECT( (PyObject *)par__mask );
    Py_DECREF( par__mask );
    par__mask = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    CHECK_OBJECT( (PyObject *)par__mask );
    Py_DECREF( par__mask );
    par__mask = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( dawg_python$units$$$function_2_value );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_dawg_python$units$$$function_3_label( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_base = python_pars[ 0 ];
    PyObject *par__mask = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_d5c39737a73fbc5977ed3d937c550a0b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d5c39737a73fbc5977ed3d937c550a0b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d5c39737a73fbc5977ed3d937c550a0b, codeobj_d5c39737a73fbc5977ed3d937c550a0b, module_dawg_python$units, sizeof(void *)+sizeof(void *) );
    frame_d5c39737a73fbc5977ed3d937c550a0b = cache_frame_d5c39737a73fbc5977ed3d937c550a0b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d5c39737a73fbc5977ed3d937c550a0b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d5c39737a73fbc5977ed3d937c550a0b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( par_base );
        tmp_left_name_1 = par_base;
        CHECK_OBJECT( par__mask );
        tmp_right_name_1 = par__mask;
        tmp_return_value = BINARY_OPERATION( PyNumber_And, tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d5c39737a73fbc5977ed3d937c550a0b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d5c39737a73fbc5977ed3d937c550a0b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d5c39737a73fbc5977ed3d937c550a0b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d5c39737a73fbc5977ed3d937c550a0b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d5c39737a73fbc5977ed3d937c550a0b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d5c39737a73fbc5977ed3d937c550a0b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d5c39737a73fbc5977ed3d937c550a0b,
        type_description_1,
        par_base,
        par__mask
    );


    // Release cached frame.
    if ( frame_d5c39737a73fbc5977ed3d937c550a0b == cache_frame_d5c39737a73fbc5977ed3d937c550a0b )
    {
        Py_DECREF( frame_d5c39737a73fbc5977ed3d937c550a0b );
    }
    cache_frame_d5c39737a73fbc5977ed3d937c550a0b = NULL;

    assertFrameObject( frame_d5c39737a73fbc5977ed3d937c550a0b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( dawg_python$units$$$function_3_label );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    CHECK_OBJECT( (PyObject *)par__mask );
    Py_DECREF( par__mask );
    par__mask = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    CHECK_OBJECT( (PyObject *)par__mask );
    Py_DECREF( par__mask );
    par__mask = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( dawg_python$units$$$function_3_label );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_dawg_python$units$$$function_4_offset( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_base = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6a5f971793cb73787847eda66b00493a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6a5f971793cb73787847eda66b00493a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6a5f971793cb73787847eda66b00493a, codeobj_6a5f971793cb73787847eda66b00493a, module_dawg_python$units, sizeof(void *) );
    frame_6a5f971793cb73787847eda66b00493a = cache_frame_6a5f971793cb73787847eda66b00493a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6a5f971793cb73787847eda66b00493a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6a5f971793cb73787847eda66b00493a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_right_name_3;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_right_name_4;
        PyObject *tmp_right_name_5;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_base );
        tmp_left_name_3 = par_base;
        tmp_right_name_1 = const_int_pos_10;
        tmp_left_name_2 = BINARY_OPERATION( PyNumber_Rshift, tmp_left_name_3, tmp_right_name_1 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_base );
        tmp_left_name_5 = par_base;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_EXTENSION_BIT );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_EXTENSION_BIT );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_left_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "EXTENSION_BIT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_right_name_3 = tmp_mvar_value_1;
        tmp_left_name_4 = BINARY_OPERATION( PyNumber_And, tmp_left_name_5, tmp_right_name_3 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_2 );

            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_4 = const_int_pos_6;
        tmp_right_name_2 = BINARY_OPERATION( PyNumber_Rshift, tmp_left_name_4, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_2 );

            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BINARY_OPERATION( PyNumber_Lshift, tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_PRECISION_MASK );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRECISION_MASK );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PRECISION_MASK" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_right_name_5 = tmp_mvar_value_2;
        tmp_return_value = BINARY_OPERATION( PyNumber_And, tmp_left_name_1, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a5f971793cb73787847eda66b00493a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a5f971793cb73787847eda66b00493a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a5f971793cb73787847eda66b00493a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6a5f971793cb73787847eda66b00493a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6a5f971793cb73787847eda66b00493a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6a5f971793cb73787847eda66b00493a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6a5f971793cb73787847eda66b00493a,
        type_description_1,
        par_base
    );


    // Release cached frame.
    if ( frame_6a5f971793cb73787847eda66b00493a == cache_frame_6a5f971793cb73787847eda66b00493a )
    {
        Py_DECREF( frame_6a5f971793cb73787847eda66b00493a );
    }
    cache_frame_6a5f971793cb73787847eda66b00493a = NULL;

    assertFrameObject( frame_6a5f971793cb73787847eda66b00493a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( dawg_python$units$$$function_4_offset );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_base );
    Py_DECREF( par_base );
    par_base = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( dawg_python$units$$$function_4_offset );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_dawg_python$units$$$function_1_has_leaf( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_dawg_python$units$$$function_1_has_leaf,
        const_str_plain_has_leaf,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ac156c5361a53f436b3e94e7196cfd0c,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_dawg_python$units,
        const_str_digest_ed9413380dd5c74bf53a8301dd5841cc,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_dawg_python$units$$$function_2_value( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_dawg_python$units$$$function_2_value,
        const_str_plain_value,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_211ad98ffdefe7d7add21be898d76aa2,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_dawg_python$units,
        const_str_digest_8fa87a1f3e7e28b740e5095a341e0207,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_dawg_python$units$$$function_3_label( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_dawg_python$units$$$function_3_label,
        const_str_plain_label,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d5c39737a73fbc5977ed3d937c550a0b,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_dawg_python$units,
        const_str_digest_c396101cdc024a026ddf3273e9f33fe5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_dawg_python$units$$$function_4_offset(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_dawg_python$units$$$function_4_offset,
        const_str_plain_offset,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6a5f971793cb73787847eda66b00493a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_dawg_python$units,
        const_str_digest_e00085e6f2e404eb0f43d93e484eb18f,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_dawg_python$units =
{
    PyModuleDef_HEAD_INIT,
    "dawg_python.units",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(dawg_python$units)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(dawg_python$units)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_dawg_python$units );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("dawg_python.units: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("dawg_python.units: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("dawg_python.units: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initdawg_python$units" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_dawg_python$units = Py_InitModule4(
        "dawg_python.units",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_dawg_python$units = PyModule_Create( &mdef_dawg_python$units );
#endif

    moduledict_dawg_python$units = MODULE_DICT( module_dawg_python$units );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_dawg_python$units,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_dawg_python$units,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_dawg_python$units,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_dawg_python$units,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_dawg_python$units );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_843f71d44fc11ae30951300243286951, module_dawg_python$units );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_93a29da5e2bdbd950b21a0da81247b90;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_a6b92e40b51243703808ce6ae1609571;
        UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_93a29da5e2bdbd950b21a0da81247b90 = MAKE_MODULE_FRAME( codeobj_93a29da5e2bdbd950b21a0da81247b90, module_dawg_python$units );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_93a29da5e2bdbd950b21a0da81247b90 );
    assert( Py_REFCNT( frame_93a29da5e2bdbd950b21a0da81247b90 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_93a29da5e2bdbd950b21a0da81247b90->m_frame.f_lineno = 5;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_absolute_import );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_absolute_import, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = const_int_pos_4294967295;
        UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_PRECISION_MASK, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = const_int_pos_2097152;
        UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_OFFSET_MAX, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = const_int_pos_2147483648;
        UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_IS_LEAF_BIT, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = const_int_pos_256;
        UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_HAS_LEAF_BIT, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = const_int_pos_512;
        UPDATE_STRING_DICT0( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_EXTENSION_BIT, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_defaults_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_HAS_LEAF_BIT );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HAS_LEAF_BIT );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_tuple_element_1 = tmp_mvar_value_3;
        tmp_defaults_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_1 );
        tmp_assign_source_10 = MAKE_FUNCTION_dawg_python$units$$$function_1_has_leaf( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_has_leaf, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_defaults_2;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_right_name_1;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_IS_LEAF_BIT );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IS_LEAF_BIT );
        }

        CHECK_OBJECT( tmp_mvar_value_4 );
        tmp_operand_name_1 = tmp_mvar_value_4;
        tmp_left_name_1 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_PRECISION_MASK );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PRECISION_MASK );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PRECISION_MASK" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 20;

            goto frame_exception_exit_1;
        }

        tmp_right_name_1 = tmp_mvar_value_5;
        tmp_tuple_element_2 = BINARY_OPERATION( PyNumber_And, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        tmp_defaults_2 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_defaults_2, 0, tmp_tuple_element_2 );
        tmp_assign_source_11 = MAKE_FUNCTION_dawg_python$units$$$function_2_value( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_value, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_defaults_3;
        PyObject *tmp_tuple_element_3;
        PyObject *tmp_left_name_2;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_right_name_2;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_IS_LEAF_BIT );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IS_LEAF_BIT );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IS_LEAF_BIT" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 25;

            goto frame_exception_exit_1;
        }

        tmp_left_name_2 = tmp_mvar_value_6;
        tmp_right_name_2 = const_int_pos_255;
        tmp_tuple_element_3 = BINARY_OPERATION( PyNumber_Or, tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_tuple_element_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        tmp_defaults_3 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_defaults_3, 0, tmp_tuple_element_3 );
        tmp_assign_source_12 = MAKE_FUNCTION_dawg_python$units$$$function_3_label( tmp_defaults_3 );



        UPDATE_STRING_DICT1( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_label, tmp_assign_source_12 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_93a29da5e2bdbd950b21a0da81247b90 );
#endif
    popFrameStack();

    assertFrameObject( frame_93a29da5e2bdbd950b21a0da81247b90 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_93a29da5e2bdbd950b21a0da81247b90 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_93a29da5e2bdbd950b21a0da81247b90, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_93a29da5e2bdbd950b21a0da81247b90->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_93a29da5e2bdbd950b21a0da81247b90, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_dawg_python$units$$$function_4_offset(  );



        UPDATE_STRING_DICT1( moduledict_dawg_python$units, (Nuitka_StringObject *)const_str_plain_offset, tmp_assign_source_13 );
    }

    return MOD_RETURN_VALUE( module_dawg_python$units );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
