/* Generated code for Python module 'zmq.utils.strtypes'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_zmq$utils$strtypes" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_zmq$utils$strtypes;
PyDictObject *moduledict_zmq$utils$strtypes;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_b;
extern PyObject *const_str_plain_basestring;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_decode;
extern PyObject *const_str_plain_utf8;
static PyObject *const_str_digest_a7c9c2b316266c5029f371d0e195d8a2;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_plain_cast_unicode;
extern PyObject *const_tuple_str_plain_s_str_plain_encoding_str_plain_errors_tuple;
extern PyObject *const_str_plain___all__;
extern PyObject *const_str_plain_strict;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_encoding;
extern PyObject *const_str_plain_asbytes;
extern PyObject *const_str_plain_bytes;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_encode;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_unicode;
static PyObject *const_str_digest_832f411ee589314ecea047dedd6b8c72;
static PyObject *const_str_digest_085541838454789ba79269efe7977d37;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_version_info;
static PyObject *const_str_digest_92230b762285683545089684e86e41f1;
static PyObject *const_str_digest_1b1cf9909297aa42d90f5c28b6ebece3;
static PyObject *const_str_digest_e91c45aa63e453eb7e55d72eaafb1491;
static PyObject *const_tuple_str_plain_utf8_str_plain_strict_tuple;
extern PyObject *const_str_plain_u;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_digest_feebc3097fcd4b75a2f145474817a329;
extern PyObject *const_str_plain_errors;
extern PyObject *const_str_plain_cast_bytes;
static PyObject *const_list_9ed4d9646029b14cd9ae787d2982896f_list;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___cached__;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_a7c9c2b316266c5029f371d0e195d8a2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5853577 ], 21, 0 );
    const_str_digest_832f411ee589314ecea047dedd6b8c72 = UNSTREAM_STRING_ASCII( &constant_bin[ 5853598 ], 33, 0 );
    const_str_digest_085541838454789ba79269efe7977d37 = UNSTREAM_STRING_ASCII( &constant_bin[ 5853631 ], 95, 0 );
    const_str_digest_92230b762285683545089684e86e41f1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5853726 ], 30, 0 );
    const_str_digest_1b1cf9909297aa42d90f5c28b6ebece3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5853756 ], 32, 0 );
    const_str_digest_e91c45aa63e453eb7e55d72eaafb1491 = UNSTREAM_STRING_ASCII( &constant_bin[ 5853788 ], 27, 0 );
    const_tuple_str_plain_utf8_str_plain_strict_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_utf8_str_plain_strict_tuple, 0, const_str_plain_utf8 ); Py_INCREF( const_str_plain_utf8 );
    PyTuple_SET_ITEM( const_tuple_str_plain_utf8_str_plain_strict_tuple, 1, const_str_plain_strict ); Py_INCREF( const_str_plain_strict );
    const_list_9ed4d9646029b14cd9ae787d2982896f_list = PyList_New( 8 );
    PyList_SET_ITEM( const_list_9ed4d9646029b14cd9ae787d2982896f_list, 0, const_str_plain_asbytes ); Py_INCREF( const_str_plain_asbytes );
    PyList_SET_ITEM( const_list_9ed4d9646029b14cd9ae787d2982896f_list, 1, const_str_plain_bytes ); Py_INCREF( const_str_plain_bytes );
    PyList_SET_ITEM( const_list_9ed4d9646029b14cd9ae787d2982896f_list, 2, const_str_plain_unicode ); Py_INCREF( const_str_plain_unicode );
    PyList_SET_ITEM( const_list_9ed4d9646029b14cd9ae787d2982896f_list, 3, const_str_plain_basestring ); Py_INCREF( const_str_plain_basestring );
    PyList_SET_ITEM( const_list_9ed4d9646029b14cd9ae787d2982896f_list, 4, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyList_SET_ITEM( const_list_9ed4d9646029b14cd9ae787d2982896f_list, 5, const_str_plain_u ); Py_INCREF( const_str_plain_u );
    PyList_SET_ITEM( const_list_9ed4d9646029b14cd9ae787d2982896f_list, 6, const_str_plain_cast_bytes ); Py_INCREF( const_str_plain_cast_bytes );
    PyList_SET_ITEM( const_list_9ed4d9646029b14cd9ae787d2982896f_list, 7, const_str_plain_cast_unicode ); Py_INCREF( const_str_plain_cast_unicode );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_zmq$utils$strtypes( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_4df57a366a57b4ea12faa88a54a7f4ea;
static PyCodeObject *codeobj_1ba814f5e14c9b7c4ebadcc7dd9904e3;
static PyCodeObject *codeobj_8615d3ebbfc58f75f4d96826a5cdc06f;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_a7c9c2b316266c5029f371d0e195d8a2 );
    codeobj_4df57a366a57b4ea12faa88a54a7f4ea = MAKE_CODEOBJ( module_filename_obj, const_str_digest_e91c45aa63e453eb7e55d72eaafb1491, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_1ba814f5e14c9b7c4ebadcc7dd9904e3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cast_bytes, 22, const_tuple_str_plain_s_str_plain_encoding_str_plain_errors_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8615d3ebbfc58f75f4d96826a5cdc06f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_cast_unicode, 31, const_tuple_str_plain_s_str_plain_encoding_str_plain_errors_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_zmq$utils$strtypes$$$function_1_cast_bytes( PyObject *defaults );


static PyObject *MAKE_FUNCTION_zmq$utils$strtypes$$$function_2_cast_unicode( PyObject *defaults );


// The module function definitions.
static PyObject *impl_zmq$utils$strtypes$$$function_1_cast_bytes( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    PyObject *par_encoding = python_pars[ 1 ];
    PyObject *par_errors = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_1ba814f5e14c9b7c4ebadcc7dd9904e3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1ba814f5e14c9b7c4ebadcc7dd9904e3, codeobj_1ba814f5e14c9b7c4ebadcc7dd9904e3, module_zmq$utils$strtypes, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 = cache_frame_1ba814f5e14c9b7c4ebadcc7dd9904e3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_s );
        tmp_isinstance_inst_1 = par_s;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_bytes );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_bytes );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "bytes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 24;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( par_s );
        tmp_return_value = par_s;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            PyObject *tmp_mvar_value_2;
            CHECK_OBJECT( par_s );
            tmp_isinstance_inst_2 = par_s;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 26;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_isinstance_cls_2 = tmp_mvar_value_2;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 26;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_instance_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_args_element_name_2;
                CHECK_OBJECT( par_s );
                tmp_called_instance_1 = par_s;
                CHECK_OBJECT( par_encoding );
                tmp_args_element_name_1 = par_encoding;
                CHECK_OBJECT( par_errors );
                tmp_args_element_name_2 = par_errors;
                frame_1ba814f5e14c9b7c4ebadcc7dd9904e3->m_frame.f_lineno = 27;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                    tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_encode, call_args );
                }

                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 27;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_make_exception_arg_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_832f411ee589314ecea047dedd6b8c72;
                CHECK_OBJECT( par_s );
                tmp_right_name_1 = par_s;
                tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_make_exception_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 29;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                frame_1ba814f5e14c9b7c4ebadcc7dd9904e3->m_frame.f_lineno = 29;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_1 };
                    tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_1 );
                assert( !(tmp_raise_type_1 == NULL) );
                exception_type = tmp_raise_type_1;
                exception_lineno = 29;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1ba814f5e14c9b7c4ebadcc7dd9904e3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1ba814f5e14c9b7c4ebadcc7dd9904e3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1ba814f5e14c9b7c4ebadcc7dd9904e3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1ba814f5e14c9b7c4ebadcc7dd9904e3,
        type_description_1,
        par_s,
        par_encoding,
        par_errors
    );


    // Release cached frame.
    if ( frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 == cache_frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 )
    {
        Py_DECREF( frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 );
    }
    cache_frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 = NULL;

    assertFrameObject( frame_1ba814f5e14c9b7c4ebadcc7dd9904e3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( zmq$utils$strtypes$$$function_1_cast_bytes );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_errors );
    Py_DECREF( par_errors );
    par_errors = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_errors );
    Py_DECREF( par_errors );
    par_errors = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( zmq$utils$strtypes$$$function_1_cast_bytes );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_zmq$utils$strtypes$$$function_2_cast_unicode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    PyObject *par_encoding = python_pars[ 1 ];
    PyObject *par_errors = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_8615d3ebbfc58f75f4d96826a5cdc06f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_8615d3ebbfc58f75f4d96826a5cdc06f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8615d3ebbfc58f75f4d96826a5cdc06f, codeobj_8615d3ebbfc58f75f4d96826a5cdc06f, module_zmq$utils$strtypes, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8615d3ebbfc58f75f4d96826a5cdc06f = cache_frame_8615d3ebbfc58f75f4d96826a5cdc06f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8615d3ebbfc58f75f4d96826a5cdc06f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8615d3ebbfc58f75f4d96826a5cdc06f ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_s );
        tmp_isinstance_inst_1 = par_s;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_bytes );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_bytes );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "bytes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 33;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_s );
            tmp_called_instance_1 = par_s;
            CHECK_OBJECT( par_encoding );
            tmp_args_element_name_1 = par_encoding;
            CHECK_OBJECT( par_errors );
            tmp_args_element_name_2 = par_errors;
            frame_8615d3ebbfc58f75f4d96826a5cdc06f->m_frame.f_lineno = 34;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_decode, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            PyObject *tmp_mvar_value_2;
            CHECK_OBJECT( par_s );
            tmp_isinstance_inst_2 = par_s;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 35;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_isinstance_cls_2 = tmp_mvar_value_2;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 35;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            CHECK_OBJECT( par_s );
            tmp_return_value = par_s;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_make_exception_arg_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                tmp_left_name_1 = const_str_digest_832f411ee589314ecea047dedd6b8c72;
                CHECK_OBJECT( par_s );
                tmp_right_name_1 = par_s;
                tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_make_exception_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 38;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                frame_8615d3ebbfc58f75f4d96826a5cdc06f->m_frame.f_lineno = 38;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_1 };
                    tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_TypeError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_1 );
                assert( !(tmp_raise_type_1 == NULL) );
                exception_type = tmp_raise_type_1;
                exception_lineno = 38;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8615d3ebbfc58f75f4d96826a5cdc06f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8615d3ebbfc58f75f4d96826a5cdc06f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8615d3ebbfc58f75f4d96826a5cdc06f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8615d3ebbfc58f75f4d96826a5cdc06f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8615d3ebbfc58f75f4d96826a5cdc06f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8615d3ebbfc58f75f4d96826a5cdc06f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8615d3ebbfc58f75f4d96826a5cdc06f,
        type_description_1,
        par_s,
        par_encoding,
        par_errors
    );


    // Release cached frame.
    if ( frame_8615d3ebbfc58f75f4d96826a5cdc06f == cache_frame_8615d3ebbfc58f75f4d96826a5cdc06f )
    {
        Py_DECREF( frame_8615d3ebbfc58f75f4d96826a5cdc06f );
    }
    cache_frame_8615d3ebbfc58f75f4d96826a5cdc06f = NULL;

    assertFrameObject( frame_8615d3ebbfc58f75f4d96826a5cdc06f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( zmq$utils$strtypes$$$function_2_cast_unicode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_errors );
    Py_DECREF( par_errors );
    par_errors = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_errors );
    Py_DECREF( par_errors );
    par_errors = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( zmq$utils$strtypes$$$function_2_cast_unicode );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_zmq$utils$strtypes$$$function_1_cast_bytes( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_zmq$utils$strtypes$$$function_1_cast_bytes,
        const_str_plain_cast_bytes,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1ba814f5e14c9b7c4ebadcc7dd9904e3,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_zmq$utils$strtypes,
        const_str_digest_92230b762285683545089684e86e41f1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_zmq$utils$strtypes$$$function_2_cast_unicode( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_zmq$utils$strtypes$$$function_2_cast_unicode,
        const_str_plain_cast_unicode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8615d3ebbfc58f75f4d96826a5cdc06f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_zmq$utils$strtypes,
        const_str_digest_1b1cf9909297aa42d90f5c28b6ebece3,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_zmq$utils$strtypes =
{
    PyModuleDef_HEAD_INIT,
    "zmq.utils.strtypes",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(zmq$utils$strtypes)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(zmq$utils$strtypes)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_zmq$utils$strtypes );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("zmq.utils.strtypes: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("zmq.utils.strtypes: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("zmq.utils.strtypes: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initzmq$utils$strtypes" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_zmq$utils$strtypes = Py_InitModule4(
        "zmq.utils.strtypes",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_zmq$utils$strtypes = PyModule_Create( &mdef_zmq$utils$strtypes );
#endif

    moduledict_zmq$utils$strtypes = MODULE_DICT( module_zmq$utils$strtypes );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_zmq$utils$strtypes,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_zmq$utils$strtypes,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_zmq$utils$strtypes,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_zmq$utils$strtypes,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_zmq$utils$strtypes );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_feebc3097fcd4b75a2f145474817a329, module_zmq$utils$strtypes );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_assign_unpack_1__assign_source = NULL;
    struct Nuitka_FrameObject *frame_4df57a366a57b4ea12faa88a54a7f4ea;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_085541838454789ba79269efe7977d37;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_4df57a366a57b4ea12faa88a54a7f4ea = MAKE_MODULE_FRAME( codeobj_4df57a366a57b4ea12faa88a54a7f4ea, module_zmq$utils$strtypes );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_4df57a366a57b4ea12faa88a54a7f4ea );
    assert( Py_REFCNT( frame_4df57a366a57b4ea12faa88a54a7f4ea ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_sys;
        tmp_globals_name_1 = (PyObject *)moduledict_zmq$utils$strtypes;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_4df57a366a57b4ea12faa88a54a7f4ea->m_frame.f_lineno = 11;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        assert( !(tmp_assign_source_4 == NULL) );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_4 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_version_info );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_3;
        tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_mvar_value_4;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_bytes );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_bytes );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "bytes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 14;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_5 = tmp_mvar_value_4;
            UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_bytes, tmp_assign_source_5 );
        }
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = (PyObject *)&PyUnicode_Type;
            UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_unicode, tmp_assign_source_6 );
        }
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_mvar_value_6;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_bytes );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_bytes );
            }

            CHECK_OBJECT( tmp_mvar_value_5 );
            tmp_tuple_element_1 = tmp_mvar_value_5;
            tmp_assign_source_7 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_assign_source_7, 0, tmp_tuple_element_1 );
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            CHECK_OBJECT( tmp_mvar_value_6 );
            tmp_tuple_element_1 = tmp_mvar_value_6;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_assign_source_7, 1, tmp_tuple_element_1 );
            UPDATE_STRING_DICT1( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_basestring, tmp_assign_source_7 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_mvar_value_7;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_unicode );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unicode );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 18;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_8 = tmp_mvar_value_7;
            UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_unicode, tmp_assign_source_8 );
        }
        {
            PyObject *tmp_assign_source_9;
            tmp_assign_source_9 = (PyObject *)&PyUnicode_Type;
            UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_bytes, tmp_assign_source_9 );
        }
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_mvar_value_8;
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_basestring );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_basestring );
            }

            if ( tmp_mvar_value_8 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "basestring" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 20;

                goto frame_exception_exit_1;
            }

            tmp_assign_source_10 = tmp_mvar_value_8;
            UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_basestring, tmp_assign_source_10 );
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_str_plain_utf8_str_plain_strict_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_11 = MAKE_FUNCTION_zmq$utils$strtypes$$$function_1_cast_bytes( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_cast_bytes, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_str_plain_utf8_str_plain_strict_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_12 = MAKE_FUNCTION_zmq$utils$strtypes$$$function_2_cast_unicode( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_cast_unicode, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_cast_bytes );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cast_bytes );
        }

        CHECK_OBJECT( tmp_mvar_value_9 );
        tmp_assign_source_13 = tmp_mvar_value_9;
        assert( tmp_assign_unpack_1__assign_source == NULL );
        Py_INCREF( tmp_assign_source_13 );
        tmp_assign_unpack_1__assign_source = tmp_assign_source_13;
    }
    {
        PyObject *tmp_assign_source_14;
        CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
        tmp_assign_source_14 = tmp_assign_unpack_1__assign_source;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_b, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        CHECK_OBJECT( tmp_assign_unpack_1__assign_source );
        tmp_assign_source_15 = tmp_assign_unpack_1__assign_source;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_asbytes, tmp_assign_source_15 );
    }
    CHECK_OBJECT( (PyObject *)tmp_assign_unpack_1__assign_source );
    Py_DECREF( tmp_assign_unpack_1__assign_source );
    tmp_assign_unpack_1__assign_source = NULL;

    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_cast_unicode );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cast_unicode );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cast_unicode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 43;

            goto frame_exception_exit_1;
        }

        tmp_assign_source_16 = tmp_mvar_value_10;
        UPDATE_STRING_DICT0( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain_u, tmp_assign_source_16 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4df57a366a57b4ea12faa88a54a7f4ea );
#endif
    popFrameStack();

    assertFrameObject( frame_4df57a366a57b4ea12faa88a54a7f4ea );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4df57a366a57b4ea12faa88a54a7f4ea );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4df57a366a57b4ea12faa88a54a7f4ea, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4df57a366a57b4ea12faa88a54a7f4ea->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4df57a366a57b4ea12faa88a54a7f4ea, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = LIST_COPY( const_list_9ed4d9646029b14cd9ae787d2982896f_list );
        UPDATE_STRING_DICT1( moduledict_zmq$utils$strtypes, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_17 );
    }

    return MOD_RETURN_VALUE( module_zmq$utils$strtypes );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
