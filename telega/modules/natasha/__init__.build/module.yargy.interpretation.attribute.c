/* Generated code for Python module 'yargy.interpretation.attribute'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_yargy$interpretation$attribute" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_yargy$interpretation$attribute;
PyDictObject *moduledict_yargy$interpretation$attribute;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_CustomAttribute;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_tuple_str_plain_self_str_plain_attribute_str_plain_value_tuple;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_FunctionNormalizer;
extern PyObject *const_str_plain___name__;
static PyObject *const_str_digest_7491f710402f5d0dc9b4814c31baa037;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_digest_0d28743c6d4a6176d0a738c0dff250ee;
static PyObject *const_str_digest_6163f2b16059c34f8ae8737e7bad272b;
static PyObject *const_tuple_str_plain_self_str_plain_fact_str_plain_name_tuple;
extern PyObject *const_str_plain_default;
static PyObject *const_str_digest_853a65832d0419b09fdc3ef6ec01028d;
static PyObject *const_str_digest_2d793d513173ba76a38f197efd2ed905;
static PyObject *const_str_digest_ca4f688267c27f862eae033c74d642ce;
static PyObject *const_str_digest_a632d73533c1bd63d47bef31e64fe0ca;
static PyObject *const_str_digest_7b55f2737d0b2b2aa5c84b53ea7f56d5;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_assert_equals;
extern PyObject *const_str_plain_NormalizedNormalizer;
static PyObject *const_list_str_plain_fact_str_plain_name_list;
static PyObject *const_str_digest_c1d4b01647b9b1a2278b66d100279aac;
static PyObject *const_str_digest_c97a8355d9ca021b8804379f97daf7f2;
static PyObject *const_str_digest_a72b15aeef42e299f2a2b8e0aa059297;
extern PyObject *const_str_plain_second;
static PyObject *const_str_plain_NormalizedFunctionAttribute;
extern PyObject *const_str_plain_AttributeScheme;
static PyObject *const_str_plain_InflectedFunctionAttribute;
extern PyObject *const_tuple_str_plain_self_str_plain_fact_tuple;
static PyObject *const_str_digest_b967356cd4072140e69056d8374222b5;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_sing;
static PyObject *const_list_str_plain_attribute_str_plain_function_list;
static PyObject *const_str_digest_6f2779d5c1720dc47f5e747bbec4f6a1;
static PyObject *const_str_plain_FunctionAttribute;
extern PyObject *const_str_plain_label;
static PyObject *const_tuple_ecf9b742293e9258c8b5e430c35879c0_tuple;
extern PyObject *const_str_plain_MorphAttribute;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_d1709deb518aae85f65eaaaa7a08b16d;
static PyObject *const_str_plain_RepeatableAttributeScheme;
static PyObject *const_str_digest_a127e924374f013ecef11ac3fcbd241d;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_grams;
static PyObject *const_str_plain_ConstAttribute;
extern PyObject *const_tuple_str_plain_self_tuple;
static PyObject *const_str_digest_c2114acdae82779ff85a8720737afc44;
static PyObject *const_str_digest_222dff31d6cd80af4ad255b128ac778d;
extern PyObject *const_tuple_str_plain_self_str_plain_grams_tuple;
extern PyObject *const_str_plain_inflected;
extern PyObject *const_str_plain_nomn;
extern PyObject *const_str_plain_const;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_f2bde2926fe9591a8639769111ec375c;
extern PyObject *const_tuple_str_plain_self_str_plain_value_tuple;
extern PyObject *const_str_plain_attribute;
extern PyObject *const_str_plain_FunctionFunctionNormalizer;
extern PyObject *const_str_plain_name;
extern PyObject *const_tuple_str_plain_self_str_plain_attribute_tuple;
extern PyObject *const_set_9bc13a39912b4878e1e8af7feb45ece3;
static PyObject *const_str_digest_5ac455653ac1ebec592a152a8c3f3863;
static PyObject *const_list_str_plain_attribute_str_plain_grams_str_plain_function_list;
static PyObject *const_tuple_5948286f4f35f56c6c444cdb2aaf160e_tuple;
extern PyObject *const_str_plain_normalizer;
static PyObject *const_str_plain_FunctionFunctionAttribute;
static PyObject *const_tuple_ac426a172cf1122707a7c50f965d720e_tuple;
static PyObject *const_str_digest_50965a23d3f5f45c66baac781ee78172;
static PyObject *const_str_digest_0a5543437308f2967e0aa95dc44a1f95;
extern PyObject *const_str_plain_construct;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_RepeatableAttribute;
extern PyObject *const_str_plain_first;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_repeatable;
extern PyObject *const_str_plain_AttributeBase;
static PyObject *const_str_digest_0d63aefdd7fc17adca3283cd354fcfe1;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_118fb053f5a49ca387482736dae25fdc;
static PyObject *const_list_str_plain_attribute_str_plain_grams_list;
extern PyObject *const_str_plain_Record;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_str_digest_e8ca02d690905faf827174dde4874079;
static PyObject *const_str_digest_100baea6fcaba7f1540b7141f0e7f43e;
extern PyObject *const_str_plain_Attribute;
extern PyObject *const_tuple_str_plain_self_str_plain_name_str_plain_default_tuple;
static PyObject *const_str_digest_236bd9924ac41e9b56942586fa13a27e;
static PyObject *const_list_str_plain_name_str_plain_default_list;
static PyObject *const_tuple_str_plain_Record_str_plain_assert_equals_tuple;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_fact;
extern PyObject *const_list_str_plain_name_list;
extern PyObject *const_str_plain_normalized;
extern PyObject *const_str_digest_b0477ef6933a0579a6a546ba22c6beef;
static PyObject *const_tuple_str_plain_self_str_plain_attribute_str_plain_function_tuple;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_a34c47a9e100a181b869d7a27f3f789a;
extern PyObject *const_str_plain___class__;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_list_str_plain_attribute_str_plain_value_list;
static PyObject *const_str_plain_InflectedAttribute;
extern PyObject *const_str_plain___module__;
extern PyObject *const_list_str_plain_attribute_list;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_int_pos_1;
static PyObject *const_tuple_2739264cb44534b95ff0f6466089de55_tuple;
static PyObject *const_tuple_str_plain_self_str_plain_attribute_str_plain_grams_tuple;
static PyObject *const_str_digest_c77a64efe299a87609aa4b56e43af14a;
static PyObject *const_list_str_plain_attribute_str_plain_first_str_plain_second_list;
static PyObject *const_str_digest_8ba57af75f808f02e4ce84cbb2ad2951;
extern PyObject *const_tuple_str_plain_self_str_plain_function_tuple;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
static PyObject *const_str_digest_4d34a4de017d7f0b6ed60cd14767060b;
static PyObject *const_str_digest_cfe5bbf7f7a8ef89e6f7a49baf605bed;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_function;
extern PyObject *const_str_plain_InflectedNormalizer;
static PyObject *const_list_str_plain_fact_str_plain_name_str_plain_default_list;
extern PyObject *const_str_plain_ConstNormalizer;
extern PyObject *const_str_plain___attributes__;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_AttributeSchemeBase;
static PyObject *const_str_digest_3da057d2db6d13ae89e65a3b49a2b910;
extern PyObject *const_str_plain_format;
extern PyObject *const_str_plain_MorphFunctionNormalizer;
extern PyObject *const_str_plain_property;
extern PyObject *const_str_plain_custom;
static PyObject *const_str_digest_1c2ce381f4b17a34495da4a992bccd94;
static PyObject *const_str_plain_NormalizedAttribute;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_str_plain_self_str_plain_attribute_str_plain_value_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attribute_str_plain_value_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attribute_str_plain_value_tuple, 1, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attribute_str_plain_value_tuple, 2, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    const_str_digest_7491f710402f5d0dc9b4814c31baa037 = UNSTREAM_STRING_ASCII( &constant_bin[ 5760863 ], 27, 0 );
    const_str_digest_6163f2b16059c34f8ae8737e7bad272b = UNSTREAM_STRING_ASCII( &constant_bin[ 5760890 ], 25, 0 );
    const_tuple_str_plain_self_str_plain_fact_str_plain_name_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_fact_str_plain_name_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_fact_str_plain_name_tuple, 1, const_str_plain_fact ); Py_INCREF( const_str_plain_fact );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_fact_str_plain_name_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_digest_853a65832d0419b09fdc3ef6ec01028d = UNSTREAM_STRING_ASCII( &constant_bin[ 5760915 ], 23, 0 );
    const_str_digest_2d793d513173ba76a38f197efd2ed905 = UNSTREAM_STRING_ASCII( &constant_bin[ 5760938 ], 38, 0 );
    const_str_digest_ca4f688267c27f862eae033c74d642ce = UNSTREAM_STRING_ASCII( &constant_bin[ 5760976 ], 37, 0 );
    const_str_digest_a632d73533c1bd63d47bef31e64fe0ca = UNSTREAM_STRING_ASCII( &constant_bin[ 5761013 ], 29, 0 );
    const_str_digest_7b55f2737d0b2b2aa5c84b53ea7f56d5 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761042 ], 24, 0 );
    const_list_str_plain_fact_str_plain_name_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_fact_str_plain_name_list, 0, const_str_plain_fact ); Py_INCREF( const_str_plain_fact );
    PyList_SET_ITEM( const_list_str_plain_fact_str_plain_name_list, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_digest_c1d4b01647b9b1a2278b66d100279aac = UNSTREAM_STRING_ASCII( &constant_bin[ 5761066 ], 26, 0 );
    const_str_digest_c97a8355d9ca021b8804379f97daf7f2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761092 ], 20, 0 );
    const_str_digest_a72b15aeef42e299f2a2b8e0aa059297 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761112 ], 28, 0 );
    const_str_plain_NormalizedFunctionAttribute = UNSTREAM_STRING_ASCII( &constant_bin[ 5760938 ], 27, 1 );
    const_str_plain_InflectedFunctionAttribute = UNSTREAM_STRING_ASCII( &constant_bin[ 5760976 ], 26, 1 );
    const_str_digest_b967356cd4072140e69056d8374222b5 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761140 ], 26, 0 );
    const_list_str_plain_attribute_str_plain_function_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_function_list, 0, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_function_list, 1, const_str_plain_function ); Py_INCREF( const_str_plain_function );
    const_str_digest_6f2779d5c1720dc47f5e747bbec4f6a1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5760899 ], 16, 0 );
    const_str_plain_FunctionAttribute = UNSTREAM_STRING_ASCII( &constant_bin[ 5760948 ], 17, 1 );
    const_tuple_ecf9b742293e9258c8b5e430c35879c0_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_ecf9b742293e9258c8b5e430c35879c0_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_ecf9b742293e9258c8b5e430c35879c0_tuple, 1, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyTuple_SET_ITEM( const_tuple_ecf9b742293e9258c8b5e430c35879c0_tuple, 2, const_str_plain_first ); Py_INCREF( const_str_plain_first );
    PyTuple_SET_ITEM( const_tuple_ecf9b742293e9258c8b5e430c35879c0_tuple, 3, const_str_plain_second ); Py_INCREF( const_str_plain_second );
    const_str_digest_d1709deb518aae85f65eaaaa7a08b16d = UNSTREAM_STRING_ASCII( &constant_bin[ 5761166 ], 33, 0 );
    const_str_plain_RepeatableAttributeScheme = UNSTREAM_STRING_ASCII( &constant_bin[ 5761199 ], 25, 1 );
    const_str_digest_a127e924374f013ecef11ac3fcbd241d = UNSTREAM_STRING_ASCII( &constant_bin[ 5761224 ], 36, 0 );
    const_str_plain_ConstAttribute = UNSTREAM_STRING_ASCII( &constant_bin[ 5760915 ], 14, 1 );
    const_str_digest_c2114acdae82779ff85a8720737afc44 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761260 ], 25, 0 );
    const_str_digest_222dff31d6cd80af4ad255b128ac778d = UNSTREAM_STRING_ASCII( &constant_bin[ 5761285 ], 36, 0 );
    const_str_digest_f2bde2926fe9591a8639769111ec375c = UNSTREAM_STRING_ASCII( &constant_bin[ 5761321 ], 26, 0 );
    const_str_digest_5ac455653ac1ebec592a152a8c3f3863 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761347 ], 30, 0 );
    const_list_str_plain_attribute_str_plain_grams_str_plain_function_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_grams_str_plain_function_list, 0, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_grams_str_plain_function_list, 1, const_str_plain_grams ); Py_INCREF( const_str_plain_grams );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_grams_str_plain_function_list, 2, const_str_plain_function ); Py_INCREF( const_str_plain_function );
    const_tuple_5948286f4f35f56c6c444cdb2aaf160e_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_5948286f4f35f56c6c444cdb2aaf160e_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_5948286f4f35f56c6c444cdb2aaf160e_tuple, 1, const_str_plain_fact ); Py_INCREF( const_str_plain_fact );
    PyTuple_SET_ITEM( const_tuple_5948286f4f35f56c6c444cdb2aaf160e_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_5948286f4f35f56c6c444cdb2aaf160e_tuple, 3, const_str_plain_default ); Py_INCREF( const_str_plain_default );
    const_str_plain_FunctionFunctionAttribute = UNSTREAM_STRING_ASCII( &constant_bin[ 5761285 ], 25, 1 );
    const_tuple_ac426a172cf1122707a7c50f965d720e_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_ac426a172cf1122707a7c50f965d720e_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_ac426a172cf1122707a7c50f965d720e_tuple, 1, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyTuple_SET_ITEM( const_tuple_ac426a172cf1122707a7c50f965d720e_tuple, 2, const_str_plain_grams ); Py_INCREF( const_str_plain_grams );
    PyTuple_SET_ITEM( const_tuple_ac426a172cf1122707a7c50f965d720e_tuple, 3, const_str_plain_function ); Py_INCREF( const_str_plain_function );
    const_str_digest_50965a23d3f5f45c66baac781ee78172 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761377 ], 13, 0 );
    const_str_digest_0a5543437308f2967e0aa95dc44a1f95 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761390 ], 34, 0 );
    const_str_digest_0d63aefdd7fc17adca3283cd354fcfe1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761424 ], 24, 0 );
    const_str_digest_118fb053f5a49ca387482736dae25fdc = UNSTREAM_STRING_ASCII( &constant_bin[ 5761448 ], 22, 0 );
    const_list_str_plain_attribute_str_plain_grams_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_grams_list, 0, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_grams_list, 1, const_str_plain_grams ); Py_INCREF( const_str_plain_grams );
    const_str_digest_e8ca02d690905faf827174dde4874079 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761470 ], 34, 0 );
    const_str_digest_100baea6fcaba7f1540b7141f0e7f43e = UNSTREAM_STRING_ASCII( &constant_bin[ 5760948 ], 28, 0 );
    const_str_digest_236bd9924ac41e9b56942586fa13a27e = UNSTREAM_STRING_ASCII( &constant_bin[ 5761504 ], 19, 0 );
    const_list_str_plain_name_str_plain_default_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_name_str_plain_default_list, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyList_SET_ITEM( const_list_str_plain_name_str_plain_default_list, 1, const_str_plain_default ); Py_INCREF( const_str_plain_default );
    const_tuple_str_plain_Record_str_plain_assert_equals_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Record_str_plain_assert_equals_tuple, 0, const_str_plain_Record ); Py_INCREF( const_str_plain_Record );
    PyTuple_SET_ITEM( const_tuple_str_plain_Record_str_plain_assert_equals_tuple, 1, const_str_plain_assert_equals ); Py_INCREF( const_str_plain_assert_equals );
    const_tuple_str_plain_self_str_plain_attribute_str_plain_function_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attribute_str_plain_function_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attribute_str_plain_function_tuple, 1, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attribute_str_plain_function_tuple, 2, const_str_plain_function ); Py_INCREF( const_str_plain_function );
    const_str_digest_a34c47a9e100a181b869d7a27f3f789a = UNSTREAM_STRING_ASCII( &constant_bin[ 5761523 ], 19, 0 );
    const_list_str_plain_attribute_str_plain_value_list = PyList_New( 2 );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_value_list, 0, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_value_list, 1, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    const_str_plain_InflectedAttribute = UNSTREAM_STRING_ASCII( &constant_bin[ 5760863 ], 18, 1 );
    const_tuple_2739264cb44534b95ff0f6466089de55_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_2739264cb44534b95ff0f6466089de55_tuple, 0, const_str_plain_InflectedNormalizer ); Py_INCREF( const_str_plain_InflectedNormalizer );
    PyTuple_SET_ITEM( const_tuple_2739264cb44534b95ff0f6466089de55_tuple, 1, const_str_plain_NormalizedNormalizer ); Py_INCREF( const_str_plain_NormalizedNormalizer );
    PyTuple_SET_ITEM( const_tuple_2739264cb44534b95ff0f6466089de55_tuple, 2, const_str_plain_ConstNormalizer ); Py_INCREF( const_str_plain_ConstNormalizer );
    PyTuple_SET_ITEM( const_tuple_2739264cb44534b95ff0f6466089de55_tuple, 3, const_str_plain_FunctionNormalizer ); Py_INCREF( const_str_plain_FunctionNormalizer );
    PyTuple_SET_ITEM( const_tuple_2739264cb44534b95ff0f6466089de55_tuple, 4, const_str_plain_FunctionFunctionNormalizer ); Py_INCREF( const_str_plain_FunctionFunctionNormalizer );
    PyTuple_SET_ITEM( const_tuple_2739264cb44534b95ff0f6466089de55_tuple, 5, const_str_plain_MorphFunctionNormalizer ); Py_INCREF( const_str_plain_MorphFunctionNormalizer );
    const_tuple_str_plain_self_str_plain_attribute_str_plain_grams_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attribute_str_plain_grams_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attribute_str_plain_grams_tuple, 1, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_attribute_str_plain_grams_tuple, 2, const_str_plain_grams ); Py_INCREF( const_str_plain_grams );
    const_str_digest_c77a64efe299a87609aa4b56e43af14a = UNSTREAM_STRING_ASCII( &constant_bin[ 5761542 ], 30, 0 );
    const_list_str_plain_attribute_str_plain_first_str_plain_second_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_first_str_plain_second_list, 0, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_first_str_plain_second_list, 1, const_str_plain_first ); Py_INCREF( const_str_plain_first );
    PyList_SET_ITEM( const_list_str_plain_attribute_str_plain_first_str_plain_second_list, 2, const_str_plain_second ); Py_INCREF( const_str_plain_second );
    const_str_digest_8ba57af75f808f02e4ce84cbb2ad2951 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761572 ], 35, 0 );
    const_str_digest_4d34a4de017d7f0b6ed60cd14767060b = UNSTREAM_STRING_ASCII( &constant_bin[ 5761607 ], 25, 0 );
    const_str_digest_cfe5bbf7f7a8ef89e6f7a49baf605bed = UNSTREAM_STRING_ASCII( &constant_bin[ 5761632 ], 15, 0 );
    const_list_str_plain_fact_str_plain_name_str_plain_default_list = PyList_New( 3 );
    PyList_SET_ITEM( const_list_str_plain_fact_str_plain_name_str_plain_default_list, 0, const_str_plain_fact ); Py_INCREF( const_str_plain_fact );
    PyList_SET_ITEM( const_list_str_plain_fact_str_plain_name_str_plain_default_list, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyList_SET_ITEM( const_list_str_plain_fact_str_plain_name_str_plain_default_list, 2, const_str_plain_default ); Py_INCREF( const_str_plain_default );
    const_str_digest_3da057d2db6d13ae89e65a3b49a2b910 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761647 ], 35, 0 );
    const_str_digest_1c2ce381f4b17a34495da4a992bccd94 = UNSTREAM_STRING_ASCII( &constant_bin[ 5761682 ], 39, 0 );
    const_str_plain_NormalizedAttribute = UNSTREAM_STRING_ASCII( &constant_bin[ 5761112 ], 19, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_yargy$interpretation$attribute( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_7fd3f0c5b319b6e5d2adc1a0902611ec;
static PyCodeObject *codeobj_43b522f96e9464f6cf11e230d6f1ddf8;
static PyCodeObject *codeobj_3630f57b4057d3cd4ce7d87e41188fe7;
static PyCodeObject *codeobj_48e6a4d53ef38d2e801d687a6b9283f6;
static PyCodeObject *codeobj_8ee9ad83d06e66cd88718bb360dd670e;
static PyCodeObject *codeobj_e3c722f334a426b726df72ea4f4a5f29;
static PyCodeObject *codeobj_81b99ecbdba3360f3d8f8af995ec57e8;
static PyCodeObject *codeobj_115e07a0ba9198c2c27c90390c1c32eb;
static PyCodeObject *codeobj_bc871a960d0c66a3c34ec1b1d6280e9e;
static PyCodeObject *codeobj_81c89dc97b0264a85ff7fd1cf1c4772f;
static PyCodeObject *codeobj_57a7f75885b84ff2c6e77d1521f9f436;
static PyCodeObject *codeobj_5fe3f5462368f492373f952d5ca849b1;
static PyCodeObject *codeobj_42d34affc81cf5f2df6085fad214394f;
static PyCodeObject *codeobj_6334f04b56529bf541f0d4f7a241f913;
static PyCodeObject *codeobj_972738c02d6d24bda47e53fef740b12f;
static PyCodeObject *codeobj_22caa1100484c4c6c43dae68a7112bba;
static PyCodeObject *codeobj_d096f1b27c6b6d72b1cf379514ae9f71;
static PyCodeObject *codeobj_f2bc2d8fd60667be51ab7f02ba475480;
static PyCodeObject *codeobj_5e1a9cb2d74d6f308399178f7c805075;
static PyCodeObject *codeobj_29c505c15ed5f1491f38c79f0846bbd6;
static PyCodeObject *codeobj_bed9472a3bd0f7eeece3562e3d0c48f8;
static PyCodeObject *codeobj_0386004d739d706b8f0b0eb15ade4c4e;
static PyCodeObject *codeobj_ec0672c8a4ede01d802e42a4799ef8e3;
static PyCodeObject *codeobj_04cda988208708971fd5ba51bbbb6f4f;
static PyCodeObject *codeobj_e31d73f6aa8bf4b38e78bef6a2abb0ee;
static PyCodeObject *codeobj_5801d96729697de34ffa9d33b343e9d1;
static PyCodeObject *codeobj_da901750565b5755d2014afafe7cfd28;
static PyCodeObject *codeobj_7192cc387045a2dbe9b31751e37e5bde;
static PyCodeObject *codeobj_42ff0cd9ce1cecbb82b9e81eebfcbd5b;
static PyCodeObject *codeobj_422d061468a4b3e59e913b207a5f0531;
static PyCodeObject *codeobj_ee24f8670913e7a3237609b242682de0;
static PyCodeObject *codeobj_e417ad6c1df5480e7701eaf0e16e0862;
static PyCodeObject *codeobj_d5fa32dac1eb63f7ca11184a592d9b2b;
static PyCodeObject *codeobj_73599e6c23384c2767232198d48d6926;
static PyCodeObject *codeobj_6a044fed0f1372a9b47b0f99ad019441;
static PyCodeObject *codeobj_2153fc9a7f1500b6cf12b107933ca8c2;
static PyCodeObject *codeobj_5010b93be3e9347fe6780af5e3098fbb;
static PyCodeObject *codeobj_3d8d077220267591c1a8e90818da009b;
static PyCodeObject *codeobj_377b521676259273fdea78548061c369;
static PyCodeObject *codeobj_15fd467c7bcc6a9ed1c988bb98444ea1;
static PyCodeObject *codeobj_9dbaa6a4b97bbfe63b595fd448471668;
static PyCodeObject *codeobj_2c6d64b151e736dc75956c2292e5bbec;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_d1709deb518aae85f65eaaaa7a08b16d );
    codeobj_7fd3f0c5b319b6e5d2adc1a0902611ec = MAKE_CODEOBJ( module_filename_obj, const_str_digest_1c2ce381f4b17a34495da4a992bccd94, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_43b522f96e9464f6cf11e230d6f1ddf8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Attribute, 60, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3630f57b4057d3cd4ce7d87e41188fe7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AttributeBase, 45, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_48e6a4d53ef38d2e801d687a6b9283f6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AttributeScheme, 22, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_8ee9ad83d06e66cd88718bb360dd670e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AttributeSchemeBase, 18, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e3c722f334a426b726df72ea4f4a5f29 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ConstAttribute, 122, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_81b99ecbdba3360f3d8f8af995ec57e8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_FunctionAttribute, 134, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_115e07a0ba9198c2c27c90390c1c32eb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_FunctionFunctionAttribute, 153, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_bc871a960d0c66a3c34ec1b1d6280e9e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_InflectedAttribute, 89, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_81c89dc97b0264a85ff7fd1cf1c4772f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_InflectedFunctionAttribute, 166, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_57a7f75885b84ff2c6e77d1521f9f436 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_NormalizedAttribute, 104, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5fe3f5462368f492373f952d5ca849b1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_NormalizedFunctionAttribute, 182, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_42d34affc81cf5f2df6085fad214394f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_RepeatableAttributeScheme, 36, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6334f04b56529bf541f0d4f7a241f913 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 37, const_tuple_str_plain_self_str_plain_attribute_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_972738c02d6d24bda47e53fef740b12f = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 107, const_tuple_str_plain_self_str_plain_attribute_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_22caa1100484c4c6c43dae68a7112bba = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 156, const_tuple_ecf9b742293e9258c8b5e430c35879c0_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d096f1b27c6b6d72b1cf379514ae9f71 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 137, const_tuple_str_plain_self_str_plain_attribute_str_plain_function_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_f2bc2d8fd60667be51ab7f02ba475480 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 185, const_tuple_str_plain_self_str_plain_attribute_str_plain_function_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5e1a9cb2d74d6f308399178f7c805075 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 92, const_tuple_str_plain_self_str_plain_attribute_str_plain_grams_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_29c505c15ed5f1491f38c79f0846bbd6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 169, const_tuple_ac426a172cf1122707a7c50f965d720e_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_bed9472a3bd0f7eeece3562e3d0c48f8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 125, const_tuple_str_plain_self_str_plain_attribute_str_plain_value_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0386004d739d706b8f0b0eb15ade4c4e = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 48, const_tuple_str_plain_self_str_plain_fact_str_plain_name_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ec0672c8a4ede01d802e42a4799ef8e3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 63, const_tuple_5948286f4f35f56c6c444cdb2aaf160e_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_04cda988208708971fd5ba51bbbb6f4f = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 25, const_tuple_str_plain_self_str_plain_name_str_plain_default_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e31d73f6aa8bf4b38e78bef6a2abb0ee = MAKE_CODEOBJ( module_filename_obj, const_str_plain_const, 74, const_tuple_str_plain_self_str_plain_value_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5801d96729697de34ffa9d33b343e9d1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_construct, 32, const_tuple_str_plain_self_str_plain_fact_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_da901750565b5755d2014afafe7cfd28 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_construct, 41, const_tuple_str_plain_self_str_plain_fact_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7192cc387045a2dbe9b31751e37e5bde = MAKE_CODEOBJ( module_filename_obj, const_str_plain_custom, 77, const_tuple_str_plain_self_str_plain_function_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_42ff0cd9ce1cecbb82b9e81eebfcbd5b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_custom, 96, const_tuple_str_plain_self_str_plain_function_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_422d061468a4b3e59e913b207a5f0531 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_custom, 110, const_tuple_str_plain_self_str_plain_function_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ee24f8670913e7a3237609b242682de0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_custom, 141, const_tuple_str_plain_self_str_plain_function_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e417ad6c1df5480e7701eaf0e16e0862 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_inflected, 68, const_tuple_str_plain_self_str_plain_grams_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d5fa32dac1eb63f7ca11184a592d9b2b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_label, 52, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_73599e6c23384c2767232198d48d6926 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalized, 71, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6a044fed0f1372a9b47b0f99ad019441 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalizer, 99, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_2153fc9a7f1500b6cf12b107933ca8c2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalizer, 113, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5010b93be3e9347fe6780af5e3098fbb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalizer, 129, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3d8d077220267591c1a8e90818da009b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalizer, 148, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_377b521676259273fdea78548061c369 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalizer, 161, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_15fd467c7bcc6a9ed1c988bb98444ea1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalizer, 174, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9dbaa6a4b97bbfe63b595fd448471668 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_normalizer, 189, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_2c6d64b151e736dc75956c2292e5bbec = MAKE_CODEOBJ( module_filename_obj, const_str_plain_repeatable, 29, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_10_normalized(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_11_const(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_12_custom(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_13___init__(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_14_custom(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_15_normalizer(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_16___init__(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_17_custom(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_18_normalizer(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_19___init__(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_1___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_20_normalizer(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_21___init__(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_22_custom(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_23_normalizer(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_24___init__(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_25_normalizer(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_26___init__(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_27_normalizer(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_28___init__(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_29_normalizer(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_2_repeatable(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_3_construct(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_4___init__(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_5_construct(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_6___init__(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_7_label(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_8___init__(  );


static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_9_inflected( PyObject *defaults );


// The module function definitions.
static PyObject *impl_yargy$interpretation$attribute$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *par_default = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_04cda988208708971fd5ba51bbbb6f4f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_04cda988208708971fd5ba51bbbb6f4f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_04cda988208708971fd5ba51bbbb6f4f, codeobj_04cda988208708971fd5ba51bbbb6f4f, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_04cda988208708971fd5ba51bbbb6f4f = cache_frame_04cda988208708971fd5ba51bbbb6f4f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_04cda988208708971fd5ba51bbbb6f4f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_04cda988208708971fd5ba51bbbb6f4f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_name );
        tmp_assattr_name_1 = par_name;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_name, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_default );
        tmp_assattr_name_2 = par_default;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_default, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_04cda988208708971fd5ba51bbbb6f4f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_04cda988208708971fd5ba51bbbb6f4f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_04cda988208708971fd5ba51bbbb6f4f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_04cda988208708971fd5ba51bbbb6f4f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_04cda988208708971fd5ba51bbbb6f4f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_04cda988208708971fd5ba51bbbb6f4f,
        type_description_1,
        par_self,
        par_name,
        par_default
    );


    // Release cached frame.
    if ( frame_04cda988208708971fd5ba51bbbb6f4f == cache_frame_04cda988208708971fd5ba51bbbb6f4f )
    {
        Py_DECREF( frame_04cda988208708971fd5ba51bbbb6f4f );
    }
    cache_frame_04cda988208708971fd5ba51bbbb6f4f = NULL;

    assertFrameObject( frame_04cda988208708971fd5ba51bbbb6f4f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_2_repeatable( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2c6d64b151e736dc75956c2292e5bbec;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2c6d64b151e736dc75956c2292e5bbec = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2c6d64b151e736dc75956c2292e5bbec, codeobj_2c6d64b151e736dc75956c2292e5bbec, module_yargy$interpretation$attribute, sizeof(void *) );
    frame_2c6d64b151e736dc75956c2292e5bbec = cache_frame_2c6d64b151e736dc75956c2292e5bbec;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2c6d64b151e736dc75956c2292e5bbec );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2c6d64b151e736dc75956c2292e5bbec ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_RepeatableAttributeScheme );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RepeatableAttributeScheme );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "RepeatableAttributeScheme" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 30;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        frame_2c6d64b151e736dc75956c2292e5bbec->m_frame.f_lineno = 30;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2c6d64b151e736dc75956c2292e5bbec );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2c6d64b151e736dc75956c2292e5bbec );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2c6d64b151e736dc75956c2292e5bbec );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2c6d64b151e736dc75956c2292e5bbec, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2c6d64b151e736dc75956c2292e5bbec->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2c6d64b151e736dc75956c2292e5bbec, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2c6d64b151e736dc75956c2292e5bbec,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_2c6d64b151e736dc75956c2292e5bbec == cache_frame_2c6d64b151e736dc75956c2292e5bbec )
    {
        Py_DECREF( frame_2c6d64b151e736dc75956c2292e5bbec );
    }
    cache_frame_2c6d64b151e736dc75956c2292e5bbec = NULL;

    assertFrameObject( frame_2c6d64b151e736dc75956c2292e5bbec );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_2_repeatable );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_2_repeatable );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_3_construct( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_fact = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_5801d96729697de34ffa9d33b343e9d1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5801d96729697de34ffa9d33b343e9d1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5801d96729697de34ffa9d33b343e9d1, codeobj_5801d96729697de34ffa9d33b343e9d1, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *) );
    frame_5801d96729697de34ffa9d33b343e9d1 = cache_frame_5801d96729697de34ffa9d33b343e9d1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5801d96729697de34ffa9d33b343e9d1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5801d96729697de34ffa9d33b343e9d1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_Attribute );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Attribute );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Attribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 33;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_fact );
        tmp_args_element_name_1 = par_fact;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_name );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_default );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 33;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_5801d96729697de34ffa9d33b343e9d1->m_frame.f_lineno = 33;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5801d96729697de34ffa9d33b343e9d1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5801d96729697de34ffa9d33b343e9d1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5801d96729697de34ffa9d33b343e9d1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5801d96729697de34ffa9d33b343e9d1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5801d96729697de34ffa9d33b343e9d1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5801d96729697de34ffa9d33b343e9d1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5801d96729697de34ffa9d33b343e9d1,
        type_description_1,
        par_self,
        par_fact
    );


    // Release cached frame.
    if ( frame_5801d96729697de34ffa9d33b343e9d1 == cache_frame_5801d96729697de34ffa9d33b343e9d1 )
    {
        Py_DECREF( frame_5801d96729697de34ffa9d33b343e9d1 );
    }
    cache_frame_5801d96729697de34ffa9d33b343e9d1 = NULL;

    assertFrameObject( frame_5801d96729697de34ffa9d33b343e9d1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_3_construct );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fact );
    Py_DECREF( par_fact );
    par_fact = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fact );
    Py_DECREF( par_fact );
    par_fact = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_3_construct );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_4___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attribute = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_6334f04b56529bf541f0d4f7a241f913;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_6334f04b56529bf541f0d4f7a241f913 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6334f04b56529bf541f0d4f7a241f913, codeobj_6334f04b56529bf541f0d4f7a241f913, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *) );
    frame_6334f04b56529bf541f0d4f7a241f913 = cache_frame_6334f04b56529bf541f0d4f7a241f913;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6334f04b56529bf541f0d4f7a241f913 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6334f04b56529bf541f0d4f7a241f913 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_assert_equals );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_assert_equals );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "assert_equals" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 38;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_attribute );
        tmp_source_name_1 = par_attribute;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_default );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = Py_None;
        frame_6334f04b56529bf541f0d4f7a241f913->m_frame.f_lineno = 38;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_attribute );
        tmp_source_name_2 = par_attribute;
        tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_name, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6334f04b56529bf541f0d4f7a241f913 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6334f04b56529bf541f0d4f7a241f913 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6334f04b56529bf541f0d4f7a241f913, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6334f04b56529bf541f0d4f7a241f913->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6334f04b56529bf541f0d4f7a241f913, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6334f04b56529bf541f0d4f7a241f913,
        type_description_1,
        par_self,
        par_attribute
    );


    // Release cached frame.
    if ( frame_6334f04b56529bf541f0d4f7a241f913 == cache_frame_6334f04b56529bf541f0d4f7a241f913 )
    {
        Py_DECREF( frame_6334f04b56529bf541f0d4f7a241f913 );
    }
    cache_frame_6334f04b56529bf541f0d4f7a241f913 = NULL;

    assertFrameObject( frame_6334f04b56529bf541f0d4f7a241f913 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_4___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_4___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_5_construct( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_fact = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_da901750565b5755d2014afafe7cfd28;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_da901750565b5755d2014afafe7cfd28 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_da901750565b5755d2014afafe7cfd28, codeobj_da901750565b5755d2014afafe7cfd28, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *) );
    frame_da901750565b5755d2014afafe7cfd28 = cache_frame_da901750565b5755d2014afafe7cfd28;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_da901750565b5755d2014afafe7cfd28 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_da901750565b5755d2014afafe7cfd28 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_RepeatableAttribute );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_RepeatableAttribute );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "RepeatableAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 42;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_fact );
        tmp_args_element_name_1 = par_fact;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_name );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_da901750565b5755d2014afafe7cfd28->m_frame.f_lineno = 42;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_da901750565b5755d2014afafe7cfd28 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_da901750565b5755d2014afafe7cfd28 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_da901750565b5755d2014afafe7cfd28 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_da901750565b5755d2014afafe7cfd28, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_da901750565b5755d2014afafe7cfd28->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_da901750565b5755d2014afafe7cfd28, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_da901750565b5755d2014afafe7cfd28,
        type_description_1,
        par_self,
        par_fact
    );


    // Release cached frame.
    if ( frame_da901750565b5755d2014afafe7cfd28 == cache_frame_da901750565b5755d2014afafe7cfd28 )
    {
        Py_DECREF( frame_da901750565b5755d2014afafe7cfd28 );
    }
    cache_frame_da901750565b5755d2014afafe7cfd28 = NULL;

    assertFrameObject( frame_da901750565b5755d2014afafe7cfd28 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_5_construct );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fact );
    Py_DECREF( par_fact );
    par_fact = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fact );
    Py_DECREF( par_fact );
    par_fact = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_5_construct );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_6___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_fact = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_0386004d739d706b8f0b0eb15ade4c4e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0386004d739d706b8f0b0eb15ade4c4e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0386004d739d706b8f0b0eb15ade4c4e, codeobj_0386004d739d706b8f0b0eb15ade4c4e, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0386004d739d706b8f0b0eb15ade4c4e = cache_frame_0386004d739d706b8f0b0eb15ade4c4e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0386004d739d706b8f0b0eb15ade4c4e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0386004d739d706b8f0b0eb15ade4c4e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_fact );
        tmp_assattr_name_1 = par_fact;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_fact, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 49;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_name );
        tmp_assattr_name_2 = par_name;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_name, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0386004d739d706b8f0b0eb15ade4c4e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0386004d739d706b8f0b0eb15ade4c4e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0386004d739d706b8f0b0eb15ade4c4e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0386004d739d706b8f0b0eb15ade4c4e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0386004d739d706b8f0b0eb15ade4c4e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0386004d739d706b8f0b0eb15ade4c4e,
        type_description_1,
        par_self,
        par_fact,
        par_name
    );


    // Release cached frame.
    if ( frame_0386004d739d706b8f0b0eb15ade4c4e == cache_frame_0386004d739d706b8f0b0eb15ade4c4e )
    {
        Py_DECREF( frame_0386004d739d706b8f0b0eb15ade4c4e );
    }
    cache_frame_0386004d739d706b8f0b0eb15ade4c4e = NULL;

    assertFrameObject( frame_0386004d739d706b8f0b0eb15ade4c4e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_6___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fact );
    Py_DECREF( par_fact );
    par_fact = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fact );
    Py_DECREF( par_fact );
    par_fact = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_6___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_7_label( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d5fa32dac1eb63f7ca11184a592d9b2b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_d5fa32dac1eb63f7ca11184a592d9b2b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d5fa32dac1eb63f7ca11184a592d9b2b, codeobj_d5fa32dac1eb63f7ca11184a592d9b2b, module_yargy$interpretation$attribute, sizeof(void *) );
    frame_d5fa32dac1eb63f7ca11184a592d9b2b = cache_frame_d5fa32dac1eb63f7ca11184a592d9b2b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d5fa32dac1eb63f7ca11184a592d9b2b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d5fa32dac1eb63f7ca11184a592d9b2b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_4;
        tmp_source_name_1 = const_str_digest_50965a23d3f5f45c66baac781ee78172;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_format );
        assert( !(tmp_called_name_1 == NULL) );
        tmp_dict_key_1 = const_str_plain_fact;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_fact );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___name__ );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_name;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_name );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 56;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_d5fa32dac1eb63f7ca11184a592d9b2b->m_frame.f_lineno = 54;
        tmp_return_value = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d5fa32dac1eb63f7ca11184a592d9b2b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d5fa32dac1eb63f7ca11184a592d9b2b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d5fa32dac1eb63f7ca11184a592d9b2b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d5fa32dac1eb63f7ca11184a592d9b2b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d5fa32dac1eb63f7ca11184a592d9b2b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d5fa32dac1eb63f7ca11184a592d9b2b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d5fa32dac1eb63f7ca11184a592d9b2b,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_d5fa32dac1eb63f7ca11184a592d9b2b == cache_frame_d5fa32dac1eb63f7ca11184a592d9b2b )
    {
        Py_DECREF( frame_d5fa32dac1eb63f7ca11184a592d9b2b );
    }
    cache_frame_d5fa32dac1eb63f7ca11184a592d9b2b = NULL;

    assertFrameObject( frame_d5fa32dac1eb63f7ca11184a592d9b2b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_7_label );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_7_label );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_8___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_fact = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    PyObject *par_default = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_ec0672c8a4ede01d802e42a4799ef8e3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ec0672c8a4ede01d802e42a4799ef8e3 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ec0672c8a4ede01d802e42a4799ef8e3, codeobj_ec0672c8a4ede01d802e42a4799ef8e3, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ec0672c8a4ede01d802e42a4799ef8e3 = cache_frame_ec0672c8a4ede01d802e42a4799ef8e3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ec0672c8a4ede01d802e42a4799ef8e3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ec0672c8a4ede01d802e42a4799ef8e3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_fact );
        tmp_assattr_name_1 = par_fact;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_fact, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_name );
        tmp_assattr_name_2 = par_name;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_name, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_default );
        tmp_assattr_name_3 = par_default;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_default, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec0672c8a4ede01d802e42a4799ef8e3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec0672c8a4ede01d802e42a4799ef8e3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ec0672c8a4ede01d802e42a4799ef8e3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ec0672c8a4ede01d802e42a4799ef8e3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ec0672c8a4ede01d802e42a4799ef8e3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ec0672c8a4ede01d802e42a4799ef8e3,
        type_description_1,
        par_self,
        par_fact,
        par_name,
        par_default
    );


    // Release cached frame.
    if ( frame_ec0672c8a4ede01d802e42a4799ef8e3 == cache_frame_ec0672c8a4ede01d802e42a4799ef8e3 )
    {
        Py_DECREF( frame_ec0672c8a4ede01d802e42a4799ef8e3 );
    }
    cache_frame_ec0672c8a4ede01d802e42a4799ef8e3 = NULL;

    assertFrameObject( frame_ec0672c8a4ede01d802e42a4799ef8e3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_8___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fact );
    Py_DECREF( par_fact );
    par_fact = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_fact );
    Py_DECREF( par_fact );
    par_fact = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)par_default );
    Py_DECREF( par_default );
    par_default = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_8___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_9_inflected( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_grams = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_e417ad6c1df5480e7701eaf0e16e0862;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e417ad6c1df5480e7701eaf0e16e0862 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e417ad6c1df5480e7701eaf0e16e0862, codeobj_e417ad6c1df5480e7701eaf0e16e0862, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *) );
    frame_e417ad6c1df5480e7701eaf0e16e0862 = cache_frame_e417ad6c1df5480e7701eaf0e16e0862;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e417ad6c1df5480e7701eaf0e16e0862 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e417ad6c1df5480e7701eaf0e16e0862 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_InflectedAttribute );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_InflectedAttribute );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "InflectedAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 69;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        CHECK_OBJECT( par_grams );
        tmp_args_element_name_2 = par_grams;
        frame_e417ad6c1df5480e7701eaf0e16e0862->m_frame.f_lineno = 69;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e417ad6c1df5480e7701eaf0e16e0862 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e417ad6c1df5480e7701eaf0e16e0862 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e417ad6c1df5480e7701eaf0e16e0862 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e417ad6c1df5480e7701eaf0e16e0862, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e417ad6c1df5480e7701eaf0e16e0862->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e417ad6c1df5480e7701eaf0e16e0862, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e417ad6c1df5480e7701eaf0e16e0862,
        type_description_1,
        par_self,
        par_grams
    );


    // Release cached frame.
    if ( frame_e417ad6c1df5480e7701eaf0e16e0862 == cache_frame_e417ad6c1df5480e7701eaf0e16e0862 )
    {
        Py_DECREF( frame_e417ad6c1df5480e7701eaf0e16e0862 );
    }
    cache_frame_e417ad6c1df5480e7701eaf0e16e0862 = NULL;

    assertFrameObject( frame_e417ad6c1df5480e7701eaf0e16e0862 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_9_inflected );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_grams );
    Py_DECREF( par_grams );
    par_grams = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_grams );
    Py_DECREF( par_grams );
    par_grams = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_9_inflected );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_10_normalized( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_73599e6c23384c2767232198d48d6926;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_73599e6c23384c2767232198d48d6926 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_73599e6c23384c2767232198d48d6926, codeobj_73599e6c23384c2767232198d48d6926, module_yargy$interpretation$attribute, sizeof(void *) );
    frame_73599e6c23384c2767232198d48d6926 = cache_frame_73599e6c23384c2767232198d48d6926;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_73599e6c23384c2767232198d48d6926 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_73599e6c23384c2767232198d48d6926 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_NormalizedAttribute );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NormalizedAttribute );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NormalizedAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        frame_73599e6c23384c2767232198d48d6926->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_73599e6c23384c2767232198d48d6926 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_73599e6c23384c2767232198d48d6926 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_73599e6c23384c2767232198d48d6926 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_73599e6c23384c2767232198d48d6926, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_73599e6c23384c2767232198d48d6926->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_73599e6c23384c2767232198d48d6926, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_73599e6c23384c2767232198d48d6926,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_73599e6c23384c2767232198d48d6926 == cache_frame_73599e6c23384c2767232198d48d6926 )
    {
        Py_DECREF( frame_73599e6c23384c2767232198d48d6926 );
    }
    cache_frame_73599e6c23384c2767232198d48d6926 = NULL;

    assertFrameObject( frame_73599e6c23384c2767232198d48d6926 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_10_normalized );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_10_normalized );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_11_const( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_e31d73f6aa8bf4b38e78bef6a2abb0ee;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e31d73f6aa8bf4b38e78bef6a2abb0ee = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e31d73f6aa8bf4b38e78bef6a2abb0ee, codeobj_e31d73f6aa8bf4b38e78bef6a2abb0ee, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *) );
    frame_e31d73f6aa8bf4b38e78bef6a2abb0ee = cache_frame_e31d73f6aa8bf4b38e78bef6a2abb0ee;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e31d73f6aa8bf4b38e78bef6a2abb0ee );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e31d73f6aa8bf4b38e78bef6a2abb0ee ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_ConstAttribute );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ConstAttribute );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ConstAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 75;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_2 = par_value;
        frame_e31d73f6aa8bf4b38e78bef6a2abb0ee->m_frame.f_lineno = 75;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e31d73f6aa8bf4b38e78bef6a2abb0ee );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e31d73f6aa8bf4b38e78bef6a2abb0ee );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e31d73f6aa8bf4b38e78bef6a2abb0ee );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e31d73f6aa8bf4b38e78bef6a2abb0ee, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e31d73f6aa8bf4b38e78bef6a2abb0ee->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e31d73f6aa8bf4b38e78bef6a2abb0ee, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e31d73f6aa8bf4b38e78bef6a2abb0ee,
        type_description_1,
        par_self,
        par_value
    );


    // Release cached frame.
    if ( frame_e31d73f6aa8bf4b38e78bef6a2abb0ee == cache_frame_e31d73f6aa8bf4b38e78bef6a2abb0ee )
    {
        Py_DECREF( frame_e31d73f6aa8bf4b38e78bef6a2abb0ee );
    }
    cache_frame_e31d73f6aa8bf4b38e78bef6a2abb0ee = NULL;

    assertFrameObject( frame_e31d73f6aa8bf4b38e78bef6a2abb0ee );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_11_const );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_11_const );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_12_custom( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_function = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_7192cc387045a2dbe9b31751e37e5bde;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7192cc387045a2dbe9b31751e37e5bde = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7192cc387045a2dbe9b31751e37e5bde, codeobj_7192cc387045a2dbe9b31751e37e5bde, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *) );
    frame_7192cc387045a2dbe9b31751e37e5bde = cache_frame_7192cc387045a2dbe9b31751e37e5bde;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7192cc387045a2dbe9b31751e37e5bde );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7192cc387045a2dbe9b31751e37e5bde ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_FunctionAttribute );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FunctionAttribute );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FunctionAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 78;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        CHECK_OBJECT( par_function );
        tmp_args_element_name_2 = par_function;
        frame_7192cc387045a2dbe9b31751e37e5bde->m_frame.f_lineno = 78;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7192cc387045a2dbe9b31751e37e5bde );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7192cc387045a2dbe9b31751e37e5bde );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7192cc387045a2dbe9b31751e37e5bde );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7192cc387045a2dbe9b31751e37e5bde, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7192cc387045a2dbe9b31751e37e5bde->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7192cc387045a2dbe9b31751e37e5bde, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7192cc387045a2dbe9b31751e37e5bde,
        type_description_1,
        par_self,
        par_function
    );


    // Release cached frame.
    if ( frame_7192cc387045a2dbe9b31751e37e5bde == cache_frame_7192cc387045a2dbe9b31751e37e5bde )
    {
        Py_DECREF( frame_7192cc387045a2dbe9b31751e37e5bde );
    }
    cache_frame_7192cc387045a2dbe9b31751e37e5bde = NULL;

    assertFrameObject( frame_7192cc387045a2dbe9b31751e37e5bde );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_12_custom );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_12_custom );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_13___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attribute = python_pars[ 1 ];
    PyObject *par_grams = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_5e1a9cb2d74d6f308399178f7c805075;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5e1a9cb2d74d6f308399178f7c805075 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5e1a9cb2d74d6f308399178f7c805075, codeobj_5e1a9cb2d74d6f308399178f7c805075, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_5e1a9cb2d74d6f308399178f7c805075 = cache_frame_5e1a9cb2d74d6f308399178f7c805075;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5e1a9cb2d74d6f308399178f7c805075 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5e1a9cb2d74d6f308399178f7c805075 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_attribute );
        tmp_assattr_name_1 = par_attribute;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_attribute, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_grams );
        tmp_assattr_name_2 = par_grams;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_grams, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5e1a9cb2d74d6f308399178f7c805075 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5e1a9cb2d74d6f308399178f7c805075 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5e1a9cb2d74d6f308399178f7c805075, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5e1a9cb2d74d6f308399178f7c805075->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5e1a9cb2d74d6f308399178f7c805075, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5e1a9cb2d74d6f308399178f7c805075,
        type_description_1,
        par_self,
        par_attribute,
        par_grams
    );


    // Release cached frame.
    if ( frame_5e1a9cb2d74d6f308399178f7c805075 == cache_frame_5e1a9cb2d74d6f308399178f7c805075 )
    {
        Py_DECREF( frame_5e1a9cb2d74d6f308399178f7c805075 );
    }
    cache_frame_5e1a9cb2d74d6f308399178f7c805075 = NULL;

    assertFrameObject( frame_5e1a9cb2d74d6f308399178f7c805075 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_13___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_grams );
    Py_DECREF( par_grams );
    par_grams = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_grams );
    Py_DECREF( par_grams );
    par_grams = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_13___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_14_custom( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_function = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b, codeobj_42ff0cd9ce1cecbb82b9e81eebfcbd5b, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *) );
    frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b = cache_frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_InflectedFunctionAttribute );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_InflectedFunctionAttribute );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "InflectedFunctionAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_attribute );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_grams );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 97;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_function );
        tmp_args_element_name_3 = par_function;
        frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b,
        type_description_1,
        par_self,
        par_function
    );


    // Release cached frame.
    if ( frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b == cache_frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b )
    {
        Py_DECREF( frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b );
    }
    cache_frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b = NULL;

    assertFrameObject( frame_42ff0cd9ce1cecbb82b9e81eebfcbd5b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_14_custom );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_14_custom );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_15_normalizer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6a044fed0f1372a9b47b0f99ad019441;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6a044fed0f1372a9b47b0f99ad019441 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6a044fed0f1372a9b47b0f99ad019441, codeobj_6a044fed0f1372a9b47b0f99ad019441, module_yargy$interpretation$attribute, sizeof(void *) );
    frame_6a044fed0f1372a9b47b0f99ad019441 = cache_frame_6a044fed0f1372a9b47b0f99ad019441;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6a044fed0f1372a9b47b0f99ad019441 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6a044fed0f1372a9b47b0f99ad019441 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_InflectedNormalizer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_InflectedNormalizer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "InflectedNormalizer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_grams );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_6a044fed0f1372a9b47b0f99ad019441->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a044fed0f1372a9b47b0f99ad019441 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a044fed0f1372a9b47b0f99ad019441 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a044fed0f1372a9b47b0f99ad019441 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6a044fed0f1372a9b47b0f99ad019441, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6a044fed0f1372a9b47b0f99ad019441->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6a044fed0f1372a9b47b0f99ad019441, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6a044fed0f1372a9b47b0f99ad019441,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_6a044fed0f1372a9b47b0f99ad019441 == cache_frame_6a044fed0f1372a9b47b0f99ad019441 )
    {
        Py_DECREF( frame_6a044fed0f1372a9b47b0f99ad019441 );
    }
    cache_frame_6a044fed0f1372a9b47b0f99ad019441 = NULL;

    assertFrameObject( frame_6a044fed0f1372a9b47b0f99ad019441 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_15_normalizer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_15_normalizer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_16___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attribute = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_972738c02d6d24bda47e53fef740b12f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_972738c02d6d24bda47e53fef740b12f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_972738c02d6d24bda47e53fef740b12f, codeobj_972738c02d6d24bda47e53fef740b12f, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *) );
    frame_972738c02d6d24bda47e53fef740b12f = cache_frame_972738c02d6d24bda47e53fef740b12f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_972738c02d6d24bda47e53fef740b12f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_972738c02d6d24bda47e53fef740b12f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_attribute );
        tmp_assattr_name_1 = par_attribute;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_attribute, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_972738c02d6d24bda47e53fef740b12f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_972738c02d6d24bda47e53fef740b12f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_972738c02d6d24bda47e53fef740b12f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_972738c02d6d24bda47e53fef740b12f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_972738c02d6d24bda47e53fef740b12f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_972738c02d6d24bda47e53fef740b12f,
        type_description_1,
        par_self,
        par_attribute
    );


    // Release cached frame.
    if ( frame_972738c02d6d24bda47e53fef740b12f == cache_frame_972738c02d6d24bda47e53fef740b12f )
    {
        Py_DECREF( frame_972738c02d6d24bda47e53fef740b12f );
    }
    cache_frame_972738c02d6d24bda47e53fef740b12f = NULL;

    assertFrameObject( frame_972738c02d6d24bda47e53fef740b12f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_16___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_16___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_17_custom( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_function = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_422d061468a4b3e59e913b207a5f0531;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_422d061468a4b3e59e913b207a5f0531 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_422d061468a4b3e59e913b207a5f0531, codeobj_422d061468a4b3e59e913b207a5f0531, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *) );
    frame_422d061468a4b3e59e913b207a5f0531 = cache_frame_422d061468a4b3e59e913b207a5f0531;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_422d061468a4b3e59e913b207a5f0531 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_422d061468a4b3e59e913b207a5f0531 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_NormalizedFunctionAttribute );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NormalizedFunctionAttribute );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NormalizedFunctionAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 111;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_attribute );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_function );
        tmp_args_element_name_2 = par_function;
        frame_422d061468a4b3e59e913b207a5f0531->m_frame.f_lineno = 111;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_422d061468a4b3e59e913b207a5f0531 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_422d061468a4b3e59e913b207a5f0531 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_422d061468a4b3e59e913b207a5f0531 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_422d061468a4b3e59e913b207a5f0531, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_422d061468a4b3e59e913b207a5f0531->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_422d061468a4b3e59e913b207a5f0531, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_422d061468a4b3e59e913b207a5f0531,
        type_description_1,
        par_self,
        par_function
    );


    // Release cached frame.
    if ( frame_422d061468a4b3e59e913b207a5f0531 == cache_frame_422d061468a4b3e59e913b207a5f0531 )
    {
        Py_DECREF( frame_422d061468a4b3e59e913b207a5f0531 );
    }
    cache_frame_422d061468a4b3e59e913b207a5f0531 = NULL;

    assertFrameObject( frame_422d061468a4b3e59e913b207a5f0531 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_17_custom );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_17_custom );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_18_normalizer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2153fc9a7f1500b6cf12b107933ca8c2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2153fc9a7f1500b6cf12b107933ca8c2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2153fc9a7f1500b6cf12b107933ca8c2, codeobj_2153fc9a7f1500b6cf12b107933ca8c2, module_yargy$interpretation$attribute, sizeof(void *) );
    frame_2153fc9a7f1500b6cf12b107933ca8c2 = cache_frame_2153fc9a7f1500b6cf12b107933ca8c2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2153fc9a7f1500b6cf12b107933ca8c2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2153fc9a7f1500b6cf12b107933ca8c2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_NormalizedNormalizer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NormalizedNormalizer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NormalizedNormalizer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_2153fc9a7f1500b6cf12b107933ca8c2->m_frame.f_lineno = 115;
        tmp_return_value = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2153fc9a7f1500b6cf12b107933ca8c2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2153fc9a7f1500b6cf12b107933ca8c2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2153fc9a7f1500b6cf12b107933ca8c2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2153fc9a7f1500b6cf12b107933ca8c2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2153fc9a7f1500b6cf12b107933ca8c2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2153fc9a7f1500b6cf12b107933ca8c2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2153fc9a7f1500b6cf12b107933ca8c2,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_2153fc9a7f1500b6cf12b107933ca8c2 == cache_frame_2153fc9a7f1500b6cf12b107933ca8c2 )
    {
        Py_DECREF( frame_2153fc9a7f1500b6cf12b107933ca8c2 );
    }
    cache_frame_2153fc9a7f1500b6cf12b107933ca8c2 = NULL;

    assertFrameObject( frame_2153fc9a7f1500b6cf12b107933ca8c2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_18_normalizer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_18_normalizer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_19___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attribute = python_pars[ 1 ];
    PyObject *par_value = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_bed9472a3bd0f7eeece3562e3d0c48f8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_bed9472a3bd0f7eeece3562e3d0c48f8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bed9472a3bd0f7eeece3562e3d0c48f8, codeobj_bed9472a3bd0f7eeece3562e3d0c48f8, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bed9472a3bd0f7eeece3562e3d0c48f8 = cache_frame_bed9472a3bd0f7eeece3562e3d0c48f8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bed9472a3bd0f7eeece3562e3d0c48f8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bed9472a3bd0f7eeece3562e3d0c48f8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_attribute );
        tmp_assattr_name_1 = par_attribute;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_attribute, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_value );
        tmp_assattr_name_2 = par_value;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_value, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bed9472a3bd0f7eeece3562e3d0c48f8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bed9472a3bd0f7eeece3562e3d0c48f8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bed9472a3bd0f7eeece3562e3d0c48f8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bed9472a3bd0f7eeece3562e3d0c48f8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bed9472a3bd0f7eeece3562e3d0c48f8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bed9472a3bd0f7eeece3562e3d0c48f8,
        type_description_1,
        par_self,
        par_attribute,
        par_value
    );


    // Release cached frame.
    if ( frame_bed9472a3bd0f7eeece3562e3d0c48f8 == cache_frame_bed9472a3bd0f7eeece3562e3d0c48f8 )
    {
        Py_DECREF( frame_bed9472a3bd0f7eeece3562e3d0c48f8 );
    }
    cache_frame_bed9472a3bd0f7eeece3562e3d0c48f8 = NULL;

    assertFrameObject( frame_bed9472a3bd0f7eeece3562e3d0c48f8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_19___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_19___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_20_normalizer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5010b93be3e9347fe6780af5e3098fbb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5010b93be3e9347fe6780af5e3098fbb = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5010b93be3e9347fe6780af5e3098fbb, codeobj_5010b93be3e9347fe6780af5e3098fbb, module_yargy$interpretation$attribute, sizeof(void *) );
    frame_5010b93be3e9347fe6780af5e3098fbb = cache_frame_5010b93be3e9347fe6780af5e3098fbb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5010b93be3e9347fe6780af5e3098fbb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5010b93be3e9347fe6780af5e3098fbb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_ConstNormalizer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ConstNormalizer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ConstNormalizer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_value );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_5010b93be3e9347fe6780af5e3098fbb->m_frame.f_lineno = 131;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5010b93be3e9347fe6780af5e3098fbb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5010b93be3e9347fe6780af5e3098fbb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5010b93be3e9347fe6780af5e3098fbb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5010b93be3e9347fe6780af5e3098fbb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5010b93be3e9347fe6780af5e3098fbb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5010b93be3e9347fe6780af5e3098fbb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5010b93be3e9347fe6780af5e3098fbb,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_5010b93be3e9347fe6780af5e3098fbb == cache_frame_5010b93be3e9347fe6780af5e3098fbb )
    {
        Py_DECREF( frame_5010b93be3e9347fe6780af5e3098fbb );
    }
    cache_frame_5010b93be3e9347fe6780af5e3098fbb = NULL;

    assertFrameObject( frame_5010b93be3e9347fe6780af5e3098fbb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_20_normalizer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_20_normalizer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_21___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attribute = python_pars[ 1 ];
    PyObject *par_function = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_d096f1b27c6b6d72b1cf379514ae9f71;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d096f1b27c6b6d72b1cf379514ae9f71 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d096f1b27c6b6d72b1cf379514ae9f71, codeobj_d096f1b27c6b6d72b1cf379514ae9f71, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d096f1b27c6b6d72b1cf379514ae9f71 = cache_frame_d096f1b27c6b6d72b1cf379514ae9f71;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d096f1b27c6b6d72b1cf379514ae9f71 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d096f1b27c6b6d72b1cf379514ae9f71 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_attribute );
        tmp_assattr_name_1 = par_attribute;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_attribute, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_function );
        tmp_assattr_name_2 = par_function;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_function, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d096f1b27c6b6d72b1cf379514ae9f71 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d096f1b27c6b6d72b1cf379514ae9f71 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d096f1b27c6b6d72b1cf379514ae9f71, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d096f1b27c6b6d72b1cf379514ae9f71->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d096f1b27c6b6d72b1cf379514ae9f71, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d096f1b27c6b6d72b1cf379514ae9f71,
        type_description_1,
        par_self,
        par_attribute,
        par_function
    );


    // Release cached frame.
    if ( frame_d096f1b27c6b6d72b1cf379514ae9f71 == cache_frame_d096f1b27c6b6d72b1cf379514ae9f71 )
    {
        Py_DECREF( frame_d096f1b27c6b6d72b1cf379514ae9f71 );
    }
    cache_frame_d096f1b27c6b6d72b1cf379514ae9f71 = NULL;

    assertFrameObject( frame_d096f1b27c6b6d72b1cf379514ae9f71 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_21___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_21___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_22_custom( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_function = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_ee24f8670913e7a3237609b242682de0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ee24f8670913e7a3237609b242682de0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ee24f8670913e7a3237609b242682de0, codeobj_ee24f8670913e7a3237609b242682de0, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *) );
    frame_ee24f8670913e7a3237609b242682de0 = cache_frame_ee24f8670913e7a3237609b242682de0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ee24f8670913e7a3237609b242682de0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ee24f8670913e7a3237609b242682de0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_FunctionFunctionAttribute );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FunctionFunctionAttribute );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FunctionFunctionAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_attribute );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_function );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 144;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_function );
        tmp_args_element_name_3 = par_function;
        frame_ee24f8670913e7a3237609b242682de0->m_frame.f_lineno = 142;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ee24f8670913e7a3237609b242682de0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ee24f8670913e7a3237609b242682de0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ee24f8670913e7a3237609b242682de0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ee24f8670913e7a3237609b242682de0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ee24f8670913e7a3237609b242682de0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ee24f8670913e7a3237609b242682de0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ee24f8670913e7a3237609b242682de0,
        type_description_1,
        par_self,
        par_function
    );


    // Release cached frame.
    if ( frame_ee24f8670913e7a3237609b242682de0 == cache_frame_ee24f8670913e7a3237609b242682de0 )
    {
        Py_DECREF( frame_ee24f8670913e7a3237609b242682de0 );
    }
    cache_frame_ee24f8670913e7a3237609b242682de0 = NULL;

    assertFrameObject( frame_ee24f8670913e7a3237609b242682de0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_22_custom );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_22_custom );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_23_normalizer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3d8d077220267591c1a8e90818da009b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3d8d077220267591c1a8e90818da009b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3d8d077220267591c1a8e90818da009b, codeobj_3d8d077220267591c1a8e90818da009b, module_yargy$interpretation$attribute, sizeof(void *) );
    frame_3d8d077220267591c1a8e90818da009b = cache_frame_3d8d077220267591c1a8e90818da009b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3d8d077220267591c1a8e90818da009b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3d8d077220267591c1a8e90818da009b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_FunctionNormalizer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FunctionNormalizer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FunctionNormalizer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 150;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_function );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_3d8d077220267591c1a8e90818da009b->m_frame.f_lineno = 150;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d8d077220267591c1a8e90818da009b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d8d077220267591c1a8e90818da009b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3d8d077220267591c1a8e90818da009b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3d8d077220267591c1a8e90818da009b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3d8d077220267591c1a8e90818da009b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3d8d077220267591c1a8e90818da009b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3d8d077220267591c1a8e90818da009b,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_3d8d077220267591c1a8e90818da009b == cache_frame_3d8d077220267591c1a8e90818da009b )
    {
        Py_DECREF( frame_3d8d077220267591c1a8e90818da009b );
    }
    cache_frame_3d8d077220267591c1a8e90818da009b = NULL;

    assertFrameObject( frame_3d8d077220267591c1a8e90818da009b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_23_normalizer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_23_normalizer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_24___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attribute = python_pars[ 1 ];
    PyObject *par_first = python_pars[ 2 ];
    PyObject *par_second = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_22caa1100484c4c6c43dae68a7112bba;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_22caa1100484c4c6c43dae68a7112bba = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_22caa1100484c4c6c43dae68a7112bba, codeobj_22caa1100484c4c6c43dae68a7112bba, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_22caa1100484c4c6c43dae68a7112bba = cache_frame_22caa1100484c4c6c43dae68a7112bba;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_22caa1100484c4c6c43dae68a7112bba );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_22caa1100484c4c6c43dae68a7112bba ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_attribute );
        tmp_assattr_name_1 = par_attribute;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_attribute, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 157;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_first );
        tmp_assattr_name_2 = par_first;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_first, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_second );
        tmp_assattr_name_3 = par_second;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_second, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 159;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_22caa1100484c4c6c43dae68a7112bba );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_22caa1100484c4c6c43dae68a7112bba );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_22caa1100484c4c6c43dae68a7112bba, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_22caa1100484c4c6c43dae68a7112bba->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_22caa1100484c4c6c43dae68a7112bba, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_22caa1100484c4c6c43dae68a7112bba,
        type_description_1,
        par_self,
        par_attribute,
        par_first,
        par_second
    );


    // Release cached frame.
    if ( frame_22caa1100484c4c6c43dae68a7112bba == cache_frame_22caa1100484c4c6c43dae68a7112bba )
    {
        Py_DECREF( frame_22caa1100484c4c6c43dae68a7112bba );
    }
    cache_frame_22caa1100484c4c6c43dae68a7112bba = NULL;

    assertFrameObject( frame_22caa1100484c4c6c43dae68a7112bba );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_24___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_first );
    Py_DECREF( par_first );
    par_first = NULL;

    CHECK_OBJECT( (PyObject *)par_second );
    Py_DECREF( par_second );
    par_second = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_first );
    Py_DECREF( par_first );
    par_first = NULL;

    CHECK_OBJECT( (PyObject *)par_second );
    Py_DECREF( par_second );
    par_second = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_24___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_25_normalizer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_377b521676259273fdea78548061c369;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_377b521676259273fdea78548061c369 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_377b521676259273fdea78548061c369, codeobj_377b521676259273fdea78548061c369, module_yargy$interpretation$attribute, sizeof(void *) );
    frame_377b521676259273fdea78548061c369 = cache_frame_377b521676259273fdea78548061c369;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_377b521676259273fdea78548061c369 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_377b521676259273fdea78548061c369 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_FunctionFunctionNormalizer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FunctionFunctionNormalizer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FunctionFunctionNormalizer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 163;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_first );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_second );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 163;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_377b521676259273fdea78548061c369->m_frame.f_lineno = 163;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_377b521676259273fdea78548061c369 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_377b521676259273fdea78548061c369 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_377b521676259273fdea78548061c369 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_377b521676259273fdea78548061c369, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_377b521676259273fdea78548061c369->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_377b521676259273fdea78548061c369, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_377b521676259273fdea78548061c369,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_377b521676259273fdea78548061c369 == cache_frame_377b521676259273fdea78548061c369 )
    {
        Py_DECREF( frame_377b521676259273fdea78548061c369 );
    }
    cache_frame_377b521676259273fdea78548061c369 = NULL;

    assertFrameObject( frame_377b521676259273fdea78548061c369 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_25_normalizer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_25_normalizer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_26___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attribute = python_pars[ 1 ];
    PyObject *par_grams = python_pars[ 2 ];
    PyObject *par_function = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_29c505c15ed5f1491f38c79f0846bbd6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_29c505c15ed5f1491f38c79f0846bbd6 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_29c505c15ed5f1491f38c79f0846bbd6, codeobj_29c505c15ed5f1491f38c79f0846bbd6, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_29c505c15ed5f1491f38c79f0846bbd6 = cache_frame_29c505c15ed5f1491f38c79f0846bbd6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_29c505c15ed5f1491f38c79f0846bbd6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_29c505c15ed5f1491f38c79f0846bbd6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_attribute );
        tmp_assattr_name_1 = par_attribute;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_attribute, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_grams );
        tmp_assattr_name_2 = par_grams;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_grams, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 171;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_function );
        tmp_assattr_name_3 = par_function;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_function, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_29c505c15ed5f1491f38c79f0846bbd6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_29c505c15ed5f1491f38c79f0846bbd6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_29c505c15ed5f1491f38c79f0846bbd6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_29c505c15ed5f1491f38c79f0846bbd6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_29c505c15ed5f1491f38c79f0846bbd6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_29c505c15ed5f1491f38c79f0846bbd6,
        type_description_1,
        par_self,
        par_attribute,
        par_grams,
        par_function
    );


    // Release cached frame.
    if ( frame_29c505c15ed5f1491f38c79f0846bbd6 == cache_frame_29c505c15ed5f1491f38c79f0846bbd6 )
    {
        Py_DECREF( frame_29c505c15ed5f1491f38c79f0846bbd6 );
    }
    cache_frame_29c505c15ed5f1491f38c79f0846bbd6 = NULL;

    assertFrameObject( frame_29c505c15ed5f1491f38c79f0846bbd6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_26___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_grams );
    Py_DECREF( par_grams );
    par_grams = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_grams );
    Py_DECREF( par_grams );
    par_grams = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_26___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_27_normalizer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_15fd467c7bcc6a9ed1c988bb98444ea1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_15fd467c7bcc6a9ed1c988bb98444ea1 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_15fd467c7bcc6a9ed1c988bb98444ea1, codeobj_15fd467c7bcc6a9ed1c988bb98444ea1, module_yargy$interpretation$attribute, sizeof(void *) );
    frame_15fd467c7bcc6a9ed1c988bb98444ea1 = cache_frame_15fd467c7bcc6a9ed1c988bb98444ea1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_15fd467c7bcc6a9ed1c988bb98444ea1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_15fd467c7bcc6a9ed1c988bb98444ea1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_MorphFunctionNormalizer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MorphFunctionNormalizer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MorphFunctionNormalizer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 176;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_InflectedNormalizer );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_InflectedNormalizer );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "InflectedNormalizer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 177;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_grams );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_15fd467c7bcc6a9ed1c988bb98444ea1->m_frame.f_lineno = 177;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_function );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 178;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_15fd467c7bcc6a9ed1c988bb98444ea1->m_frame.f_lineno = 176;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_3 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_15fd467c7bcc6a9ed1c988bb98444ea1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_15fd467c7bcc6a9ed1c988bb98444ea1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_15fd467c7bcc6a9ed1c988bb98444ea1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_15fd467c7bcc6a9ed1c988bb98444ea1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_15fd467c7bcc6a9ed1c988bb98444ea1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_15fd467c7bcc6a9ed1c988bb98444ea1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_15fd467c7bcc6a9ed1c988bb98444ea1,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_15fd467c7bcc6a9ed1c988bb98444ea1 == cache_frame_15fd467c7bcc6a9ed1c988bb98444ea1 )
    {
        Py_DECREF( frame_15fd467c7bcc6a9ed1c988bb98444ea1 );
    }
    cache_frame_15fd467c7bcc6a9ed1c988bb98444ea1 = NULL;

    assertFrameObject( frame_15fd467c7bcc6a9ed1c988bb98444ea1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_27_normalizer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_27_normalizer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_28___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_attribute = python_pars[ 1 ];
    PyObject *par_function = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_f2bc2d8fd60667be51ab7f02ba475480;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_f2bc2d8fd60667be51ab7f02ba475480 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f2bc2d8fd60667be51ab7f02ba475480, codeobj_f2bc2d8fd60667be51ab7f02ba475480, module_yargy$interpretation$attribute, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f2bc2d8fd60667be51ab7f02ba475480 = cache_frame_f2bc2d8fd60667be51ab7f02ba475480;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f2bc2d8fd60667be51ab7f02ba475480 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f2bc2d8fd60667be51ab7f02ba475480 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_attribute );
        tmp_assattr_name_1 = par_attribute;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_attribute, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 186;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_function );
        tmp_assattr_name_2 = par_function;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_function, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 187;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f2bc2d8fd60667be51ab7f02ba475480 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f2bc2d8fd60667be51ab7f02ba475480 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f2bc2d8fd60667be51ab7f02ba475480, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f2bc2d8fd60667be51ab7f02ba475480->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f2bc2d8fd60667be51ab7f02ba475480, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f2bc2d8fd60667be51ab7f02ba475480,
        type_description_1,
        par_self,
        par_attribute,
        par_function
    );


    // Release cached frame.
    if ( frame_f2bc2d8fd60667be51ab7f02ba475480 == cache_frame_f2bc2d8fd60667be51ab7f02ba475480 )
    {
        Py_DECREF( frame_f2bc2d8fd60667be51ab7f02ba475480 );
    }
    cache_frame_f2bc2d8fd60667be51ab7f02ba475480 = NULL;

    assertFrameObject( frame_f2bc2d8fd60667be51ab7f02ba475480 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_28___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_attribute );
    Py_DECREF( par_attribute );
    par_attribute = NULL;

    CHECK_OBJECT( (PyObject *)par_function );
    Py_DECREF( par_function );
    par_function = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_28___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_yargy$interpretation$attribute$$$function_29_normalizer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9dbaa6a4b97bbfe63b595fd448471668;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9dbaa6a4b97bbfe63b595fd448471668 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9dbaa6a4b97bbfe63b595fd448471668, codeobj_9dbaa6a4b97bbfe63b595fd448471668, module_yargy$interpretation$attribute, sizeof(void *) );
    frame_9dbaa6a4b97bbfe63b595fd448471668 = cache_frame_9dbaa6a4b97bbfe63b595fd448471668;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9dbaa6a4b97bbfe63b595fd448471668 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9dbaa6a4b97bbfe63b595fd448471668 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_MorphFunctionNormalizer );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MorphFunctionNormalizer );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MorphFunctionNormalizer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 191;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_NormalizedNormalizer );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NormalizedNormalizer );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NormalizedNormalizer" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 192;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        frame_9dbaa6a4b97bbfe63b595fd448471668->m_frame.f_lineno = 192;
        tmp_args_element_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_function );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 193;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_9dbaa6a4b97bbfe63b595fd448471668->m_frame.f_lineno = 191;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9dbaa6a4b97bbfe63b595fd448471668 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9dbaa6a4b97bbfe63b595fd448471668 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9dbaa6a4b97bbfe63b595fd448471668 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9dbaa6a4b97bbfe63b595fd448471668, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9dbaa6a4b97bbfe63b595fd448471668->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9dbaa6a4b97bbfe63b595fd448471668, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9dbaa6a4b97bbfe63b595fd448471668,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_9dbaa6a4b97bbfe63b595fd448471668 == cache_frame_9dbaa6a4b97bbfe63b595fd448471668 )
    {
        Py_DECREF( frame_9dbaa6a4b97bbfe63b595fd448471668 );
    }
    cache_frame_9dbaa6a4b97bbfe63b595fd448471668 = NULL;

    assertFrameObject( frame_9dbaa6a4b97bbfe63b595fd448471668 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_29_normalizer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute$$$function_29_normalizer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_10_normalized(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_10_normalized,
        const_str_plain_normalized,
#if PYTHON_VERSION >= 300
        const_str_digest_c97a8355d9ca021b8804379f97daf7f2,
#endif
        codeobj_73599e6c23384c2767232198d48d6926,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_11_const(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_11_const,
        const_str_plain_const,
#if PYTHON_VERSION >= 300
        const_str_digest_cfe5bbf7f7a8ef89e6f7a49baf605bed,
#endif
        codeobj_e31d73f6aa8bf4b38e78bef6a2abb0ee,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_12_custom(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_12_custom,
        const_str_plain_custom,
#if PYTHON_VERSION >= 300
        const_str_digest_6f2779d5c1720dc47f5e747bbec4f6a1,
#endif
        codeobj_7192cc387045a2dbe9b31751e37e5bde,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_13___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_13___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_7491f710402f5d0dc9b4814c31baa037,
#endif
        codeobj_5e1a9cb2d74d6f308399178f7c805075,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_14_custom(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_14_custom,
        const_str_plain_custom,
#if PYTHON_VERSION >= 300
        const_str_digest_6163f2b16059c34f8ae8737e7bad272b,
#endif
        codeobj_42ff0cd9ce1cecbb82b9e81eebfcbd5b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_15_normalizer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_15_normalizer,
        const_str_plain_normalizer,
#if PYTHON_VERSION >= 300
        const_str_digest_a632d73533c1bd63d47bef31e64fe0ca,
#endif
        codeobj_6a044fed0f1372a9b47b0f99ad019441,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_16___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_16___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_a72b15aeef42e299f2a2b8e0aa059297,
#endif
        codeobj_972738c02d6d24bda47e53fef740b12f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_17_custom(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_17_custom,
        const_str_plain_custom,
#if PYTHON_VERSION >= 300
        const_str_digest_f2bde2926fe9591a8639769111ec375c,
#endif
        codeobj_422d061468a4b3e59e913b207a5f0531,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_18_normalizer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_18_normalizer,
        const_str_plain_normalizer,
#if PYTHON_VERSION >= 300
        const_str_digest_5ac455653ac1ebec592a152a8c3f3863,
#endif
        codeobj_2153fc9a7f1500b6cf12b107933ca8c2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_19___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_19___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_853a65832d0419b09fdc3ef6ec01028d,
#endif
        codeobj_bed9472a3bd0f7eeece3562e3d0c48f8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_1___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_7b55f2737d0b2b2aa5c84b53ea7f56d5,
#endif
        codeobj_04cda988208708971fd5ba51bbbb6f4f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_20_normalizer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_20_normalizer,
        const_str_plain_normalizer,
#if PYTHON_VERSION >= 300
        const_str_digest_c2114acdae82779ff85a8720737afc44,
#endif
        codeobj_5010b93be3e9347fe6780af5e3098fbb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_21___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_21___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_c1d4b01647b9b1a2278b66d100279aac,
#endif
        codeobj_d096f1b27c6b6d72b1cf379514ae9f71,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_22_custom(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_22_custom,
        const_str_plain_custom,
#if PYTHON_VERSION >= 300
        const_str_digest_0d63aefdd7fc17adca3283cd354fcfe1,
#endif
        codeobj_ee24f8670913e7a3237609b242682de0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_23_normalizer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_23_normalizer,
        const_str_plain_normalizer,
#if PYTHON_VERSION >= 300
        const_str_digest_100baea6fcaba7f1540b7141f0e7f43e,
#endif
        codeobj_3d8d077220267591c1a8e90818da009b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_24___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_24___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_0a5543437308f2967e0aa95dc44a1f95,
#endif
        codeobj_22caa1100484c4c6c43dae68a7112bba,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_25_normalizer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_25_normalizer,
        const_str_plain_normalizer,
#if PYTHON_VERSION >= 300
        const_str_digest_222dff31d6cd80af4ad255b128ac778d,
#endif
        codeobj_377b521676259273fdea78548061c369,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_26___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_26___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_8ba57af75f808f02e4ce84cbb2ad2951,
#endif
        codeobj_29c505c15ed5f1491f38c79f0846bbd6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_27_normalizer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_27_normalizer,
        const_str_plain_normalizer,
#if PYTHON_VERSION >= 300
        const_str_digest_ca4f688267c27f862eae033c74d642ce,
#endif
        codeobj_15fd467c7bcc6a9ed1c988bb98444ea1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_28___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_28___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_a127e924374f013ecef11ac3fcbd241d,
#endif
        codeobj_f2bc2d8fd60667be51ab7f02ba475480,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_29_normalizer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_29_normalizer,
        const_str_plain_normalizer,
#if PYTHON_VERSION >= 300
        const_str_digest_2d793d513173ba76a38f197efd2ed905,
#endif
        codeobj_9dbaa6a4b97bbfe63b595fd448471668,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_2_repeatable(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_2_repeatable,
        const_str_plain_repeatable,
#if PYTHON_VERSION >= 300
        const_str_digest_b967356cd4072140e69056d8374222b5,
#endif
        codeobj_2c6d64b151e736dc75956c2292e5bbec,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_3_construct(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_3_construct,
        const_str_plain_construct,
#if PYTHON_VERSION >= 300
        const_str_digest_4d34a4de017d7f0b6ed60cd14767060b,
#endif
        codeobj_5801d96729697de34ffa9d33b343e9d1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_4___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_4___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_e8ca02d690905faf827174dde4874079,
#endif
        codeobj_6334f04b56529bf541f0d4f7a241f913,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_5_construct(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_5_construct,
        const_str_plain_construct,
#if PYTHON_VERSION >= 300
        const_str_digest_3da057d2db6d13ae89e65a3b49a2b910,
#endif
        codeobj_da901750565b5755d2014afafe7cfd28,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_6___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_6___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_118fb053f5a49ca387482736dae25fdc,
#endif
        codeobj_0386004d739d706b8f0b0eb15ade4c4e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_7_label(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_7_label,
        const_str_plain_label,
#if PYTHON_VERSION >= 300
        const_str_digest_236bd9924ac41e9b56942586fa13a27e,
#endif
        codeobj_d5fa32dac1eb63f7ca11184a592d9b2b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_8___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_8___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_0d28743c6d4a6176d0a738c0dff250ee,
#endif
        codeobj_ec0672c8a4ede01d802e42a4799ef8e3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_yargy$interpretation$attribute$$$function_9_inflected( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_yargy$interpretation$attribute$$$function_9_inflected,
        const_str_plain_inflected,
#if PYTHON_VERSION >= 300
        const_str_digest_a34c47a9e100a181b869d7a27f3f789a,
#endif
        codeobj_e417ad6c1df5480e7701eaf0e16e0862,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_yargy$interpretation$attribute,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_yargy$interpretation$attribute =
{
    PyModuleDef_HEAD_INIT,
    "yargy.interpretation.attribute",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(yargy$interpretation$attribute)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(yargy$interpretation$attribute)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_yargy$interpretation$attribute );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("yargy.interpretation.attribute: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("yargy.interpretation.attribute: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("yargy.interpretation.attribute: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in inityargy$interpretation$attribute" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_yargy$interpretation$attribute = Py_InitModule4(
        "yargy.interpretation.attribute",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_yargy$interpretation$attribute = PyModule_Create( &mdef_yargy$interpretation$attribute );
#endif

    moduledict_yargy$interpretation$attribute = MODULE_DICT( module_yargy$interpretation$attribute );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_yargy$interpretation$attribute,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_yargy$interpretation$attribute,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_yargy$interpretation$attribute,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_yargy$interpretation$attribute,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_yargy$interpretation$attribute );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_c77a64efe299a87609aa4b56e43af14a, module_yargy$interpretation$attribute );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *outline_3_var___class__ = NULL;
    PyObject *outline_4_var___class__ = NULL;
    PyObject *outline_5_var___class__ = NULL;
    PyObject *outline_6_var___class__ = NULL;
    PyObject *outline_7_var___class__ = NULL;
    PyObject *outline_8_var___class__ = NULL;
    PyObject *outline_9_var___class__ = NULL;
    PyObject *outline_10_var___class__ = NULL;
    PyObject *outline_11_var___class__ = NULL;
    PyObject *outline_12_var___class__ = NULL;
    PyObject *outline_13_var___class__ = NULL;
    PyObject *outline_14_var___class__ = NULL;
    PyObject *tmp_class_creation_10__bases = NULL;
    PyObject *tmp_class_creation_10__bases_orig = NULL;
    PyObject *tmp_class_creation_10__class_decl_dict = NULL;
    PyObject *tmp_class_creation_10__metaclass = NULL;
    PyObject *tmp_class_creation_10__prepared = NULL;
    PyObject *tmp_class_creation_11__bases = NULL;
    PyObject *tmp_class_creation_11__bases_orig = NULL;
    PyObject *tmp_class_creation_11__class_decl_dict = NULL;
    PyObject *tmp_class_creation_11__metaclass = NULL;
    PyObject *tmp_class_creation_11__prepared = NULL;
    PyObject *tmp_class_creation_12__bases = NULL;
    PyObject *tmp_class_creation_12__bases_orig = NULL;
    PyObject *tmp_class_creation_12__class_decl_dict = NULL;
    PyObject *tmp_class_creation_12__metaclass = NULL;
    PyObject *tmp_class_creation_12__prepared = NULL;
    PyObject *tmp_class_creation_13__bases = NULL;
    PyObject *tmp_class_creation_13__bases_orig = NULL;
    PyObject *tmp_class_creation_13__class_decl_dict = NULL;
    PyObject *tmp_class_creation_13__metaclass = NULL;
    PyObject *tmp_class_creation_13__prepared = NULL;
    PyObject *tmp_class_creation_14__bases = NULL;
    PyObject *tmp_class_creation_14__bases_orig = NULL;
    PyObject *tmp_class_creation_14__class_decl_dict = NULL;
    PyObject *tmp_class_creation_14__metaclass = NULL;
    PyObject *tmp_class_creation_14__prepared = NULL;
    PyObject *tmp_class_creation_15__bases = NULL;
    PyObject *tmp_class_creation_15__bases_orig = NULL;
    PyObject *tmp_class_creation_15__class_decl_dict = NULL;
    PyObject *tmp_class_creation_15__metaclass = NULL;
    PyObject *tmp_class_creation_15__prepared = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__bases_orig = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_class_creation_4__bases = NULL;
    PyObject *tmp_class_creation_4__bases_orig = NULL;
    PyObject *tmp_class_creation_4__class_decl_dict = NULL;
    PyObject *tmp_class_creation_4__metaclass = NULL;
    PyObject *tmp_class_creation_4__prepared = NULL;
    PyObject *tmp_class_creation_5__bases = NULL;
    PyObject *tmp_class_creation_5__bases_orig = NULL;
    PyObject *tmp_class_creation_5__class_decl_dict = NULL;
    PyObject *tmp_class_creation_5__metaclass = NULL;
    PyObject *tmp_class_creation_5__prepared = NULL;
    PyObject *tmp_class_creation_6__bases = NULL;
    PyObject *tmp_class_creation_6__bases_orig = NULL;
    PyObject *tmp_class_creation_6__class_decl_dict = NULL;
    PyObject *tmp_class_creation_6__metaclass = NULL;
    PyObject *tmp_class_creation_6__prepared = NULL;
    PyObject *tmp_class_creation_7__bases = NULL;
    PyObject *tmp_class_creation_7__bases_orig = NULL;
    PyObject *tmp_class_creation_7__class_decl_dict = NULL;
    PyObject *tmp_class_creation_7__metaclass = NULL;
    PyObject *tmp_class_creation_7__prepared = NULL;
    PyObject *tmp_class_creation_8__bases = NULL;
    PyObject *tmp_class_creation_8__bases_orig = NULL;
    PyObject *tmp_class_creation_8__class_decl_dict = NULL;
    PyObject *tmp_class_creation_8__metaclass = NULL;
    PyObject *tmp_class_creation_8__prepared = NULL;
    PyObject *tmp_class_creation_9__bases = NULL;
    PyObject *tmp_class_creation_9__bases_orig = NULL;
    PyObject *tmp_class_creation_9__class_decl_dict = NULL;
    PyObject *tmp_class_creation_9__metaclass = NULL;
    PyObject *tmp_class_creation_9__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_7fd3f0c5b319b6e5d2adc1a0902611ec;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_yargy$interpretation$attribute_18 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_8ee9ad83d06e66cd88718bb360dd670e_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_8ee9ad83d06e66cd88718bb360dd670e_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *locals_yargy$interpretation$attribute_22 = NULL;
    struct Nuitka_FrameObject *frame_48e6a4d53ef38d2e801d687a6b9283f6_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_48e6a4d53ef38d2e801d687a6b9283f6_3 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *locals_yargy$interpretation$attribute_36 = NULL;
    struct Nuitka_FrameObject *frame_42d34affc81cf5f2df6085fad214394f_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_42d34affc81cf5f2df6085fad214394f_4 = NULL;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *locals_yargy$interpretation$attribute_45 = NULL;
    struct Nuitka_FrameObject *frame_3630f57b4057d3cd4ce7d87e41188fe7_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    static struct Nuitka_FrameObject *cache_frame_3630f57b4057d3cd4ce7d87e41188fe7_5 = NULL;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *locals_yargy$interpretation$attribute_60 = NULL;
    struct Nuitka_FrameObject *frame_43b522f96e9464f6cf11e230d6f1ddf8_6;
    NUITKA_MAY_BE_UNUSED char const *type_description_6 = NULL;
    static struct Nuitka_FrameObject *cache_frame_43b522f96e9464f6cf11e230d6f1ddf8_6 = NULL;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *locals_yargy$interpretation$attribute_81 = NULL;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *locals_yargy$interpretation$attribute_85 = NULL;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;
    PyObject *exception_keeper_type_22;
    PyObject *exception_keeper_value_22;
    PyTracebackObject *exception_keeper_tb_22;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_22;
    PyObject *exception_keeper_type_23;
    PyObject *exception_keeper_value_23;
    PyTracebackObject *exception_keeper_tb_23;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_23;
    PyObject *locals_yargy$interpretation$attribute_89 = NULL;
    struct Nuitka_FrameObject *frame_bc871a960d0c66a3c34ec1b1d6280e9e_7;
    NUITKA_MAY_BE_UNUSED char const *type_description_7 = NULL;
    static struct Nuitka_FrameObject *cache_frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 = NULL;
    PyObject *exception_keeper_type_24;
    PyObject *exception_keeper_value_24;
    PyTracebackObject *exception_keeper_tb_24;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_24;
    PyObject *exception_keeper_type_25;
    PyObject *exception_keeper_value_25;
    PyTracebackObject *exception_keeper_tb_25;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_25;
    PyObject *exception_keeper_type_26;
    PyObject *exception_keeper_value_26;
    PyTracebackObject *exception_keeper_tb_26;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_26;
    PyObject *locals_yargy$interpretation$attribute_104 = NULL;
    struct Nuitka_FrameObject *frame_57a7f75885b84ff2c6e77d1521f9f436_8;
    NUITKA_MAY_BE_UNUSED char const *type_description_8 = NULL;
    static struct Nuitka_FrameObject *cache_frame_57a7f75885b84ff2c6e77d1521f9f436_8 = NULL;
    PyObject *exception_keeper_type_27;
    PyObject *exception_keeper_value_27;
    PyTracebackObject *exception_keeper_tb_27;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_27;
    PyObject *exception_keeper_type_28;
    PyObject *exception_keeper_value_28;
    PyTracebackObject *exception_keeper_tb_28;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_28;
    PyObject *exception_keeper_type_29;
    PyObject *exception_keeper_value_29;
    PyTracebackObject *exception_keeper_tb_29;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_29;
    PyObject *locals_yargy$interpretation$attribute_118 = NULL;
    PyObject *exception_keeper_type_30;
    PyObject *exception_keeper_value_30;
    PyTracebackObject *exception_keeper_tb_30;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_30;
    PyObject *exception_keeper_type_31;
    PyObject *exception_keeper_value_31;
    PyTracebackObject *exception_keeper_tb_31;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_31;
    PyObject *exception_keeper_type_32;
    PyObject *exception_keeper_value_32;
    PyTracebackObject *exception_keeper_tb_32;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_32;
    PyObject *locals_yargy$interpretation$attribute_122 = NULL;
    struct Nuitka_FrameObject *frame_e3c722f334a426b726df72ea4f4a5f29_9;
    NUITKA_MAY_BE_UNUSED char const *type_description_9 = NULL;
    static struct Nuitka_FrameObject *cache_frame_e3c722f334a426b726df72ea4f4a5f29_9 = NULL;
    PyObject *exception_keeper_type_33;
    PyObject *exception_keeper_value_33;
    PyTracebackObject *exception_keeper_tb_33;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_33;
    PyObject *exception_keeper_type_34;
    PyObject *exception_keeper_value_34;
    PyTracebackObject *exception_keeper_tb_34;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_34;
    PyObject *exception_keeper_type_35;
    PyObject *exception_keeper_value_35;
    PyTracebackObject *exception_keeper_tb_35;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_35;
    PyObject *locals_yargy$interpretation$attribute_134 = NULL;
    struct Nuitka_FrameObject *frame_81b99ecbdba3360f3d8f8af995ec57e8_10;
    NUITKA_MAY_BE_UNUSED char const *type_description_10 = NULL;
    static struct Nuitka_FrameObject *cache_frame_81b99ecbdba3360f3d8f8af995ec57e8_10 = NULL;
    PyObject *exception_keeper_type_36;
    PyObject *exception_keeper_value_36;
    PyTracebackObject *exception_keeper_tb_36;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_36;
    PyObject *exception_keeper_type_37;
    PyObject *exception_keeper_value_37;
    PyTracebackObject *exception_keeper_tb_37;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_37;
    PyObject *exception_keeper_type_38;
    PyObject *exception_keeper_value_38;
    PyTracebackObject *exception_keeper_tb_38;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_38;
    PyObject *locals_yargy$interpretation$attribute_153 = NULL;
    struct Nuitka_FrameObject *frame_115e07a0ba9198c2c27c90390c1c32eb_11;
    NUITKA_MAY_BE_UNUSED char const *type_description_11 = NULL;
    static struct Nuitka_FrameObject *cache_frame_115e07a0ba9198c2c27c90390c1c32eb_11 = NULL;
    PyObject *exception_keeper_type_39;
    PyObject *exception_keeper_value_39;
    PyTracebackObject *exception_keeper_tb_39;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_39;
    PyObject *exception_keeper_type_40;
    PyObject *exception_keeper_value_40;
    PyTracebackObject *exception_keeper_tb_40;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_40;
    PyObject *exception_keeper_type_41;
    PyObject *exception_keeper_value_41;
    PyTracebackObject *exception_keeper_tb_41;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_41;
    PyObject *locals_yargy$interpretation$attribute_166 = NULL;
    struct Nuitka_FrameObject *frame_81c89dc97b0264a85ff7fd1cf1c4772f_12;
    NUITKA_MAY_BE_UNUSED char const *type_description_12 = NULL;
    static struct Nuitka_FrameObject *cache_frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 = NULL;
    PyObject *exception_keeper_type_42;
    PyObject *exception_keeper_value_42;
    PyTracebackObject *exception_keeper_tb_42;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_42;
    PyObject *exception_keeper_type_43;
    PyObject *exception_keeper_value_43;
    PyTracebackObject *exception_keeper_tb_43;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_43;
    PyObject *exception_keeper_type_44;
    PyObject *exception_keeper_value_44;
    PyTracebackObject *exception_keeper_tb_44;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_44;
    PyObject *locals_yargy$interpretation$attribute_182 = NULL;
    struct Nuitka_FrameObject *frame_5fe3f5462368f492373f952d5ca849b1_13;
    NUITKA_MAY_BE_UNUSED char const *type_description_13 = NULL;
    static struct Nuitka_FrameObject *cache_frame_5fe3f5462368f492373f952d5ca849b1_13 = NULL;
    PyObject *exception_keeper_type_45;
    PyObject *exception_keeper_value_45;
    PyTracebackObject *exception_keeper_tb_45;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_45;
    PyObject *exception_keeper_type_46;
    PyObject *exception_keeper_value_46;
    PyTracebackObject *exception_keeper_tb_46;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_46;
    PyObject *exception_keeper_type_47;
    PyObject *exception_keeper_value_47;
    PyTracebackObject *exception_keeper_tb_47;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_47;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_7fd3f0c5b319b6e5d2adc1a0902611ec = MAKE_MODULE_FRAME( codeobj_7fd3f0c5b319b6e5d2adc1a0902611ec, module_yargy$interpretation$attribute );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_7fd3f0c5b319b6e5d2adc1a0902611ec );
    assert( Py_REFCNT( frame_7fd3f0c5b319b6e5d2adc1a0902611ec ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 2;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_b0477ef6933a0579a6a546ba22c6beef;
        tmp_globals_name_1 = (PyObject *)moduledict_yargy$interpretation$attribute;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_Record_str_plain_assert_equals_tuple;
        tmp_level_name_1 = const_int_0;
        frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 4;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Record );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_Record, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_assert_equals );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_assert_equals, tmp_assign_source_7 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_normalizer;
        tmp_globals_name_2 = (PyObject *)moduledict_yargy$interpretation$attribute;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_2739264cb44534b95ff0f6466089de55_tuple;
        tmp_level_name_2 = const_int_pos_1;
        frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 8;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_8;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_4 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_4 ) )
        {
           tmp_assign_source_9 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_4,
                (PyObject *)moduledict_yargy$interpretation$attribute,
                const_str_plain_InflectedNormalizer,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_InflectedNormalizer );
        }

        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_InflectedNormalizer, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_5 ) )
        {
           tmp_assign_source_10 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_5,
                (PyObject *)moduledict_yargy$interpretation$attribute,
                const_str_plain_NormalizedNormalizer,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_NormalizedNormalizer );
        }

        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_NormalizedNormalizer, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_6 ) )
        {
           tmp_assign_source_11 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_6,
                (PyObject *)moduledict_yargy$interpretation$attribute,
                const_str_plain_ConstNormalizer,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_ConstNormalizer );
        }

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_ConstNormalizer, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_7 ) )
        {
           tmp_assign_source_12 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_7,
                (PyObject *)moduledict_yargy$interpretation$attribute,
                const_str_plain_FunctionNormalizer,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_FunctionNormalizer );
        }

        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_FunctionNormalizer, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_8 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_8 ) )
        {
           tmp_assign_source_13 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_8,
                (PyObject *)moduledict_yargy$interpretation$attribute,
                const_str_plain_FunctionFunctionNormalizer,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_FunctionFunctionNormalizer );
        }

        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_FunctionFunctionNormalizer, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_9 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_9 ) )
        {
           tmp_assign_source_14 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_9,
                (PyObject *)moduledict_yargy$interpretation$attribute,
                const_str_plain_MorphFunctionNormalizer,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_MorphFunctionNormalizer );
        }

        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_MorphFunctionNormalizer, tmp_assign_source_14 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_Record );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Record );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Record" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 18;

            goto try_except_handler_3;
        }

        tmp_tuple_element_1 = tmp_mvar_value_3;
        tmp_assign_source_15 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_15, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_16 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_16;
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_17;
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_3;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_3;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_3;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_3;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_18 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_18;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_3;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_3;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 18;

                goto try_except_handler_3;
            }
            tmp_tuple_element_2 = const_str_plain_AttributeSchemeBase;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 18;
            tmp_assign_source_19 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 18;

                goto try_except_handler_3;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_19;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 18;

                goto try_except_handler_3;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 18;

                    goto try_except_handler_3;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 18;

                    goto try_except_handler_3;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 18;

                    goto try_except_handler_3;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 18;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_3;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_20;
            tmp_assign_source_20 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_20;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_21;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_yargy$interpretation$attribute_18 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_18, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_plain_AttributeSchemeBase;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_18, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 18;

            goto try_except_handler_5;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_8ee9ad83d06e66cd88718bb360dd670e_2, codeobj_8ee9ad83d06e66cd88718bb360dd670e, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_8ee9ad83d06e66cd88718bb360dd670e_2 = cache_frame_8ee9ad83d06e66cd88718bb360dd670e_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_8ee9ad83d06e66cd88718bb360dd670e_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_8ee9ad83d06e66cd88718bb360dd670e_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_name_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_18, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8ee9ad83d06e66cd88718bb360dd670e_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8ee9ad83d06e66cd88718bb360dd670e_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_8ee9ad83d06e66cd88718bb360dd670e_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_8ee9ad83d06e66cd88718bb360dd670e_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_8ee9ad83d06e66cd88718bb360dd670e_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_8ee9ad83d06e66cd88718bb360dd670e_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_8ee9ad83d06e66cd88718bb360dd670e_2 == cache_frame_8ee9ad83d06e66cd88718bb360dd670e_2 )
        {
            Py_DECREF( frame_8ee9ad83d06e66cd88718bb360dd670e_2 );
        }
        cache_frame_8ee9ad83d06e66cd88718bb360dd670e_2 = NULL;

        assertFrameObject( frame_8ee9ad83d06e66cd88718bb360dd670e_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_5;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 18;

                goto try_except_handler_5;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_18, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 18;

                goto try_except_handler_5;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_AttributeSchemeBase;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_yargy$interpretation$attribute_18;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 18;
            tmp_assign_source_22 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 18;

                goto try_except_handler_5;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_22;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_21 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_21 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        Py_DECREF( locals_yargy$interpretation$attribute_18 );
        locals_yargy$interpretation$attribute_18 = NULL;
        goto try_return_handler_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_18 );
        locals_yargy$interpretation$attribute_18 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 18;
        goto try_except_handler_3;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_AttributeSchemeBase, tmp_assign_source_21 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_AttributeSchemeBase );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AttributeSchemeBase );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AttributeSchemeBase" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 22;

            goto try_except_handler_6;
        }

        tmp_tuple_element_5 = tmp_mvar_value_4;
        tmp_assign_source_23 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_assign_source_23, 0, tmp_tuple_element_5 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_23;
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_24 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_24;
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_25;
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_6;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_6;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_6;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_6;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_6;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_26 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_6;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_26;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_6;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_6;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_6;
            }
            tmp_tuple_element_6 = const_str_plain_AttributeScheme;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 22;
            tmp_assign_source_27 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_6;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_27;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_6;
            }
            tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_7 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 22;

                    goto try_except_handler_6;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 22;

                    goto try_except_handler_6;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_7 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 22;

                    goto try_except_handler_6;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 22;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_6;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_28;
            tmp_assign_source_28 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_28;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_29;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_yargy$interpretation$attribute_22 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_22, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_8;
        }
        tmp_dictset_value = const_str_plain_AttributeScheme;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_22, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto try_except_handler_8;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_48e6a4d53ef38d2e801d687a6b9283f6_3, codeobj_48e6a4d53ef38d2e801d687a6b9283f6, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_48e6a4d53ef38d2e801d687a6b9283f6_3 = cache_frame_48e6a4d53ef38d2e801d687a6b9283f6_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_48e6a4d53ef38d2e801d687a6b9283f6_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_48e6a4d53ef38d2e801d687a6b9283f6_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_name_str_plain_default_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_22, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_1___init__( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_22, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 25;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_2_repeatable(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_22, const_str_plain_repeatable, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_3_construct(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_22, const_str_plain_construct, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_48e6a4d53ef38d2e801d687a6b9283f6_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_48e6a4d53ef38d2e801d687a6b9283f6_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_48e6a4d53ef38d2e801d687a6b9283f6_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_48e6a4d53ef38d2e801d687a6b9283f6_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_48e6a4d53ef38d2e801d687a6b9283f6_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_48e6a4d53ef38d2e801d687a6b9283f6_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_48e6a4d53ef38d2e801d687a6b9283f6_3 == cache_frame_48e6a4d53ef38d2e801d687a6b9283f6_3 )
        {
            Py_DECREF( frame_48e6a4d53ef38d2e801d687a6b9283f6_3 );
        }
        cache_frame_48e6a4d53ef38d2e801d687a6b9283f6_3 = NULL;

        assertFrameObject( frame_48e6a4d53ef38d2e801d687a6b9283f6_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_8;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_8;
            }
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_22, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_8;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_4 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_8 = const_str_plain_AttributeScheme;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_8 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_8 );
            tmp_tuple_element_8 = locals_yargy$interpretation$attribute_22;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 22;
            tmp_assign_source_30 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 22;

                goto try_except_handler_8;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_30;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_29 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_29 );
        goto try_return_handler_8;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_8:;
        Py_DECREF( locals_yargy$interpretation$attribute_22 );
        locals_yargy$interpretation$attribute_22 = NULL;
        goto try_return_handler_7;
        // Exception handler code:
        try_except_handler_8:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_22 );
        locals_yargy$interpretation$attribute_22 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_7;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_7 = exception_type;
        exception_keeper_value_7 = exception_value;
        exception_keeper_tb_7 = exception_tb;
        exception_keeper_lineno_7 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_7;
        exception_value = exception_keeper_value_7;
        exception_tb = exception_keeper_tb_7;
        exception_lineno = exception_keeper_lineno_7;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 22;
        goto try_except_handler_6;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_AttributeScheme, tmp_assign_source_29 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_tuple_element_9;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_AttributeSchemeBase );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AttributeSchemeBase );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AttributeSchemeBase" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;

            goto try_except_handler_9;
        }

        tmp_tuple_element_9 = tmp_mvar_value_5;
        tmp_assign_source_31 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_9 );
        PyTuple_SET_ITEM( tmp_assign_source_31, 0, tmp_tuple_element_9 );
        assert( tmp_class_creation_3__bases_orig == NULL );
        tmp_class_creation_3__bases_orig = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_dircall_arg1_3;
        CHECK_OBJECT( tmp_class_creation_3__bases_orig );
        tmp_dircall_arg1_3 = tmp_class_creation_3__bases_orig;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_32 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        tmp_assign_source_33 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_33;
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_13;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_14;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_9;
        }
        tmp_condition_result_13 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_5;
        }
        else
        {
            goto condexpr_false_5;
        }
        condexpr_true_5:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_9;
        }
        goto condexpr_end_5;
        condexpr_false_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_9;
        }
        tmp_condition_result_14 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_6;
        }
        else
        {
            goto condexpr_false_6;
        }
        condexpr_true_6:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_3 = tmp_class_creation_3__bases;
        tmp_subscript_name_3 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_9;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_9;
        }
        goto condexpr_end_6;
        condexpr_false_6:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_6:;
        condexpr_end_5:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_34 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_9;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_34;
    }
    {
        nuitka_bool tmp_condition_result_15;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_9;
        }
        tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_9;
        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_16;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_9 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___prepare__ );
        tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_35;
            PyObject *tmp_called_name_5;
            PyObject *tmp_source_name_10;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_10;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_10 = tmp_class_creation_3__metaclass;
            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___prepare__ );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_9;
            }
            tmp_tuple_element_10 = const_str_plain_RepeatableAttributeScheme;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_10 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_10 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 36;
            tmp_assign_source_35 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_9;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_35;
        }
        {
            nuitka_bool tmp_condition_result_17;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_11 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_11, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_9;
            }
            tmp_condition_result_17 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_11;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_12;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_11 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 36;

                    goto try_except_handler_9;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_11 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_12 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_12 == NULL) );
                tmp_tuple_element_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_12 );
                if ( tmp_tuple_element_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 36;

                    goto try_except_handler_9;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_11 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 36;

                    goto try_except_handler_9;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 36;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_9;
            }
            branch_no_11:;
        }
        goto branch_end_10;
        branch_no_10:;
        {
            PyObject *tmp_assign_source_36;
            tmp_assign_source_36 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_36;
        }
        branch_end_10:;
    }
    {
        PyObject *tmp_assign_source_37;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_yargy$interpretation$attribute_36 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_36, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_11;
        }
        tmp_dictset_value = const_str_plain_RepeatableAttributeScheme;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_36, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;

            goto try_except_handler_11;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_42d34affc81cf5f2df6085fad214394f_4, codeobj_42d34affc81cf5f2df6085fad214394f, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_42d34affc81cf5f2df6085fad214394f_4 = cache_frame_42d34affc81cf5f2df6085fad214394f_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_42d34affc81cf5f2df6085fad214394f_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_42d34affc81cf5f2df6085fad214394f_4 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_4___init__(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_36, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 37;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_5_construct(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_36, const_str_plain_construct, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_42d34affc81cf5f2df6085fad214394f_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_42d34affc81cf5f2df6085fad214394f_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_42d34affc81cf5f2df6085fad214394f_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_42d34affc81cf5f2df6085fad214394f_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_42d34affc81cf5f2df6085fad214394f_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_42d34affc81cf5f2df6085fad214394f_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_42d34affc81cf5f2df6085fad214394f_4 == cache_frame_42d34affc81cf5f2df6085fad214394f_4 )
        {
            Py_DECREF( frame_42d34affc81cf5f2df6085fad214394f_4 );
        }
        cache_frame_42d34affc81cf5f2df6085fad214394f_4 = NULL;

        assertFrameObject( frame_42d34affc81cf5f2df6085fad214394f_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_11;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_3 = tmp_class_creation_3__bases;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_compexpr_right_3 = tmp_class_creation_3__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_11;
            }
            tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            CHECK_OBJECT( tmp_class_creation_3__bases_orig );
            tmp_dictset_value = tmp_class_creation_3__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_36, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_11;
            }
            branch_no_12:;
        }
        {
            PyObject *tmp_assign_source_38;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_12;
            PyObject *tmp_kw_name_6;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_6 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_12 = const_str_plain_RepeatableAttributeScheme;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_12 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_12 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_12 );
            tmp_tuple_element_12 = locals_yargy$interpretation$attribute_36;
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_12 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_6 = tmp_class_creation_3__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 36;
            tmp_assign_source_38 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_6, tmp_kw_name_6 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;

                goto try_except_handler_11;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_38;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_37 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_37 );
        goto try_return_handler_11;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_11:;
        Py_DECREF( locals_yargy$interpretation$attribute_36 );
        locals_yargy$interpretation$attribute_36 = NULL;
        goto try_return_handler_10;
        // Exception handler code:
        try_except_handler_11:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_36 );
        locals_yargy$interpretation$attribute_36 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto try_except_handler_10;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_10 = exception_type;
        exception_keeper_value_10 = exception_value;
        exception_keeper_tb_10 = exception_tb;
        exception_keeper_lineno_10 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_10;
        exception_value = exception_keeper_value_10;
        exception_tb = exception_keeper_tb_10;
        exception_lineno = exception_keeper_lineno_10;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 36;
        goto try_except_handler_9;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_RepeatableAttributeScheme, tmp_assign_source_37 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases_orig );
    Py_DECREF( tmp_class_creation_3__bases_orig );
    tmp_class_creation_3__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_tuple_element_13;
        PyObject *tmp_mvar_value_6;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_Record );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Record );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Record" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 45;

            goto try_except_handler_12;
        }

        tmp_tuple_element_13 = tmp_mvar_value_6;
        tmp_assign_source_39 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_13 );
        PyTuple_SET_ITEM( tmp_assign_source_39, 0, tmp_tuple_element_13 );
        assert( tmp_class_creation_4__bases_orig == NULL );
        tmp_class_creation_4__bases_orig = tmp_assign_source_39;
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_dircall_arg1_4;
        CHECK_OBJECT( tmp_class_creation_4__bases_orig );
        tmp_dircall_arg1_4 = tmp_class_creation_4__bases_orig;
        Py_INCREF( tmp_dircall_arg1_4 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_4};
            tmp_assign_source_40 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_12;
        }
        assert( tmp_class_creation_4__bases == NULL );
        tmp_class_creation_4__bases = tmp_assign_source_40;
    }
    {
        PyObject *tmp_assign_source_41;
        tmp_assign_source_41 = PyDict_New();
        assert( tmp_class_creation_4__class_decl_dict == NULL );
        tmp_class_creation_4__class_decl_dict = tmp_assign_source_41;
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_metaclass_name_4;
        nuitka_bool tmp_condition_result_19;
        PyObject *tmp_key_name_10;
        PyObject *tmp_dict_name_10;
        PyObject *tmp_dict_name_11;
        PyObject *tmp_key_name_11;
        nuitka_bool tmp_condition_result_20;
        int tmp_truth_name_4;
        PyObject *tmp_type_arg_7;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_bases_name_4;
        tmp_key_name_10 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_10 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_10, tmp_key_name_10 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_12;
        }
        tmp_condition_result_19 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_7;
        }
        else
        {
            goto condexpr_false_7;
        }
        condexpr_true_7:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_11 = tmp_class_creation_4__class_decl_dict;
        tmp_key_name_11 = const_str_plain_metaclass;
        tmp_metaclass_name_4 = DICT_GET_ITEM( tmp_dict_name_11, tmp_key_name_11 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_12;
        }
        goto condexpr_end_7;
        condexpr_false_7:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_class_creation_4__bases );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_12;
        }
        tmp_condition_result_20 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_8;
        }
        else
        {
            goto condexpr_false_8;
        }
        condexpr_true_8:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_subscribed_name_4 = tmp_class_creation_4__bases;
        tmp_subscript_name_4 = const_int_0;
        tmp_type_arg_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
        if ( tmp_type_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_12;
        }
        tmp_metaclass_name_4 = BUILTIN_TYPE1( tmp_type_arg_7 );
        Py_DECREF( tmp_type_arg_7 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_12;
        }
        goto condexpr_end_8;
        condexpr_false_8:;
        tmp_metaclass_name_4 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_4 );
        condexpr_end_8:;
        condexpr_end_7:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_bases_name_4 = tmp_class_creation_4__bases;
        tmp_assign_source_42 = SELECT_METACLASS( tmp_metaclass_name_4, tmp_bases_name_4 );
        Py_DECREF( tmp_metaclass_name_4 );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_12;
        }
        assert( tmp_class_creation_4__metaclass == NULL );
        tmp_class_creation_4__metaclass = tmp_assign_source_42;
    }
    {
        nuitka_bool tmp_condition_result_21;
        PyObject *tmp_key_name_12;
        PyObject *tmp_dict_name_12;
        tmp_key_name_12 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_12 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_12, tmp_key_name_12 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_12;
        }
        tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_13;
        }
        else
        {
            goto branch_no_13;
        }
        branch_yes_13:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_4__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_12;
        }
        branch_no_13:;
    }
    {
        nuitka_bool tmp_condition_result_22;
        PyObject *tmp_source_name_13;
        CHECK_OBJECT( tmp_class_creation_4__metaclass );
        tmp_source_name_13 = tmp_class_creation_4__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_13, const_str_plain___prepare__ );
        tmp_condition_result_22 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            PyObject *tmp_assign_source_43;
            PyObject *tmp_called_name_7;
            PyObject *tmp_source_name_14;
            PyObject *tmp_args_name_7;
            PyObject *tmp_tuple_element_14;
            PyObject *tmp_kw_name_7;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_source_name_14 = tmp_class_creation_4__metaclass;
            tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain___prepare__ );
            if ( tmp_called_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;

                goto try_except_handler_12;
            }
            tmp_tuple_element_14 = const_str_plain_AttributeBase;
            tmp_args_name_7 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_14 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_14 );
            PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_14 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_7 = tmp_class_creation_4__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 45;
            tmp_assign_source_43 = CALL_FUNCTION( tmp_called_name_7, tmp_args_name_7, tmp_kw_name_7 );
            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_name_7 );
            if ( tmp_assign_source_43 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;

                goto try_except_handler_12;
            }
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_43;
        }
        {
            nuitka_bool tmp_condition_result_23;
            PyObject *tmp_operand_name_4;
            PyObject *tmp_source_name_15;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_source_name_15 = tmp_class_creation_4__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_15, const_str_plain___getitem__ );
            tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;

                goto try_except_handler_12;
            }
            tmp_condition_result_23 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_15;
            }
            else
            {
                goto branch_no_15;
            }
            branch_yes_15:;
            {
                PyObject *tmp_raise_type_4;
                PyObject *tmp_raise_value_4;
                PyObject *tmp_left_name_4;
                PyObject *tmp_right_name_4;
                PyObject *tmp_tuple_element_15;
                PyObject *tmp_getattr_target_4;
                PyObject *tmp_getattr_attr_4;
                PyObject *tmp_getattr_default_4;
                PyObject *tmp_source_name_16;
                PyObject *tmp_type_arg_8;
                tmp_raise_type_4 = PyExc_TypeError;
                tmp_left_name_4 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_4__metaclass );
                tmp_getattr_target_4 = tmp_class_creation_4__metaclass;
                tmp_getattr_attr_4 = const_str_plain___name__;
                tmp_getattr_default_4 = const_str_angle_metaclass;
                tmp_tuple_element_15 = BUILTIN_GETATTR( tmp_getattr_target_4, tmp_getattr_attr_4, tmp_getattr_default_4 );
                if ( tmp_tuple_element_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 45;

                    goto try_except_handler_12;
                }
                tmp_right_name_4 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_15 );
                CHECK_OBJECT( tmp_class_creation_4__prepared );
                tmp_type_arg_8 = tmp_class_creation_4__prepared;
                tmp_source_name_16 = BUILTIN_TYPE1( tmp_type_arg_8 );
                assert( !(tmp_source_name_16 == NULL) );
                tmp_tuple_element_15 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_16 );
                if ( tmp_tuple_element_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_4 );

                    exception_lineno = 45;

                    goto try_except_handler_12;
                }
                PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_15 );
                tmp_raise_value_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
                Py_DECREF( tmp_right_name_4 );
                if ( tmp_raise_value_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 45;

                    goto try_except_handler_12;
                }
                exception_type = tmp_raise_type_4;
                Py_INCREF( tmp_raise_type_4 );
                exception_value = tmp_raise_value_4;
                exception_lineno = 45;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_12;
            }
            branch_no_15:;
        }
        goto branch_end_14;
        branch_no_14:;
        {
            PyObject *tmp_assign_source_44;
            tmp_assign_source_44 = PyDict_New();
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_44;
        }
        branch_end_14:;
    }
    {
        PyObject *tmp_assign_source_45;
        {
            PyObject *tmp_set_locals_4;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_set_locals_4 = tmp_class_creation_4__prepared;
            locals_yargy$interpretation$attribute_45 = tmp_set_locals_4;
            Py_INCREF( tmp_set_locals_4 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_45, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_14;
        }
        tmp_dictset_value = const_str_plain_AttributeBase;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_45, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;

            goto try_except_handler_14;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_3630f57b4057d3cd4ce7d87e41188fe7_5, codeobj_3630f57b4057d3cd4ce7d87e41188fe7, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_3630f57b4057d3cd4ce7d87e41188fe7_5 = cache_frame_3630f57b4057d3cd4ce7d87e41188fe7_5;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_3630f57b4057d3cd4ce7d87e41188fe7_5 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_3630f57b4057d3cd4ce7d87e41188fe7_5 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_fact_str_plain_name_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_45, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_6___init__(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_45, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }
        {
            nuitka_bool tmp_condition_result_24;
            PyObject *tmp_called_name_8;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_name_9;
            PyObject *tmp_args_element_name_2;
            tmp_res = MAPPING_HAS_ITEM( locals_yargy$interpretation$attribute_45, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_condition_result_24 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_9;
            }
            else
            {
                goto condexpr_false_9;
            }
            condexpr_true_9:;
            tmp_called_name_8 = PyObject_GetItem( locals_yargy$interpretation$attribute_45, const_str_plain_property );

            if ( tmp_called_name_8 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 52;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }

            if ( tmp_called_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_args_element_name_1 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_7_label(  );



            frame_3630f57b4057d3cd4ce7d87e41188fe7_5->m_frame.f_lineno = 52;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            goto condexpr_end_9;
            condexpr_false_9:;
            tmp_called_name_9 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_2 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_7_label(  );



            frame_3630f57b4057d3cd4ce7d87e41188fe7_5->m_frame.f_lineno = 52;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            condexpr_end_9:;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_45, const_str_plain_label, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3630f57b4057d3cd4ce7d87e41188fe7_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_4;

        frame_exception_exit_5:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3630f57b4057d3cd4ce7d87e41188fe7_5 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_3630f57b4057d3cd4ce7d87e41188fe7_5, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_3630f57b4057d3cd4ce7d87e41188fe7_5->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_3630f57b4057d3cd4ce7d87e41188fe7_5, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_3630f57b4057d3cd4ce7d87e41188fe7_5,
            type_description_2,
            outline_3_var___class__
        );


        // Release cached frame.
        if ( frame_3630f57b4057d3cd4ce7d87e41188fe7_5 == cache_frame_3630f57b4057d3cd4ce7d87e41188fe7_5 )
        {
            Py_DECREF( frame_3630f57b4057d3cd4ce7d87e41188fe7_5 );
        }
        cache_frame_3630f57b4057d3cd4ce7d87e41188fe7_5 = NULL;

        assertFrameObject( frame_3630f57b4057d3cd4ce7d87e41188fe7_5 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_4;

        frame_no_exception_4:;
        goto skip_nested_handling_4;
        nested_frame_exit_4:;

        goto try_except_handler_14;
        skip_nested_handling_4:;
        {
            nuitka_bool tmp_condition_result_25;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_compexpr_left_4 = tmp_class_creation_4__bases;
            CHECK_OBJECT( tmp_class_creation_4__bases_orig );
            tmp_compexpr_right_4 = tmp_class_creation_4__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;

                goto try_except_handler_14;
            }
            tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_16;
            }
            else
            {
                goto branch_no_16;
            }
            branch_yes_16:;
            CHECK_OBJECT( tmp_class_creation_4__bases_orig );
            tmp_dictset_value = tmp_class_creation_4__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_45, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;

                goto try_except_handler_14;
            }
            branch_no_16:;
        }
        {
            PyObject *tmp_assign_source_46;
            PyObject *tmp_called_name_10;
            PyObject *tmp_args_name_8;
            PyObject *tmp_tuple_element_16;
            PyObject *tmp_kw_name_8;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_called_name_10 = tmp_class_creation_4__metaclass;
            tmp_tuple_element_16 = const_str_plain_AttributeBase;
            tmp_args_name_8 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_16 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_16 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_8, 1, tmp_tuple_element_16 );
            tmp_tuple_element_16 = locals_yargy$interpretation$attribute_45;
            Py_INCREF( tmp_tuple_element_16 );
            PyTuple_SET_ITEM( tmp_args_name_8, 2, tmp_tuple_element_16 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_8 = tmp_class_creation_4__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 45;
            tmp_assign_source_46 = CALL_FUNCTION( tmp_called_name_10, tmp_args_name_8, tmp_kw_name_8 );
            Py_DECREF( tmp_args_name_8 );
            if ( tmp_assign_source_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;

                goto try_except_handler_14;
            }
            assert( outline_3_var___class__ == NULL );
            outline_3_var___class__ = tmp_assign_source_46;
        }
        CHECK_OBJECT( outline_3_var___class__ );
        tmp_assign_source_45 = outline_3_var___class__;
        Py_INCREF( tmp_assign_source_45 );
        goto try_return_handler_14;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_14:;
        Py_DECREF( locals_yargy$interpretation$attribute_45 );
        locals_yargy$interpretation$attribute_45 = NULL;
        goto try_return_handler_13;
        // Exception handler code:
        try_except_handler_14:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_45 );
        locals_yargy$interpretation$attribute_45 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto try_except_handler_13;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_13:;
        CHECK_OBJECT( (PyObject *)outline_3_var___class__ );
        Py_DECREF( outline_3_var___class__ );
        outline_3_var___class__ = NULL;

        goto outline_result_4;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_13 = exception_type;
        exception_keeper_value_13 = exception_value;
        exception_keeper_tb_13 = exception_tb;
        exception_keeper_lineno_13 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_13;
        exception_value = exception_keeper_value_13;
        exception_tb = exception_keeper_tb_13;
        exception_lineno = exception_keeper_lineno_13;

        goto outline_exception_4;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_4:;
        exception_lineno = 45;
        goto try_except_handler_12;
        outline_result_4:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_AttributeBase, tmp_assign_source_45 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_4__bases_orig );
    tmp_class_creation_4__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    Py_XDECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_14;
    exception_value = exception_keeper_value_14;
    exception_tb = exception_keeper_tb_14;
    exception_lineno = exception_keeper_lineno_14;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases_orig );
    Py_DECREF( tmp_class_creation_4__bases_orig );
    tmp_class_creation_4__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases );
    Py_DECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__class_decl_dict );
    Py_DECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__metaclass );
    Py_DECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__prepared );
    Py_DECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_tuple_element_17;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_AttributeBase );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AttributeBase );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AttributeBase" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 60;

            goto try_except_handler_15;
        }

        tmp_tuple_element_17 = tmp_mvar_value_7;
        tmp_assign_source_47 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_17 );
        PyTuple_SET_ITEM( tmp_assign_source_47, 0, tmp_tuple_element_17 );
        assert( tmp_class_creation_5__bases_orig == NULL );
        tmp_class_creation_5__bases_orig = tmp_assign_source_47;
    }
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_dircall_arg1_5;
        CHECK_OBJECT( tmp_class_creation_5__bases_orig );
        tmp_dircall_arg1_5 = tmp_class_creation_5__bases_orig;
        Py_INCREF( tmp_dircall_arg1_5 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_5};
            tmp_assign_source_48 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_15;
        }
        assert( tmp_class_creation_5__bases == NULL );
        tmp_class_creation_5__bases = tmp_assign_source_48;
    }
    {
        PyObject *tmp_assign_source_49;
        tmp_assign_source_49 = PyDict_New();
        assert( tmp_class_creation_5__class_decl_dict == NULL );
        tmp_class_creation_5__class_decl_dict = tmp_assign_source_49;
    }
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_metaclass_name_5;
        nuitka_bool tmp_condition_result_26;
        PyObject *tmp_key_name_13;
        PyObject *tmp_dict_name_13;
        PyObject *tmp_dict_name_14;
        PyObject *tmp_key_name_14;
        nuitka_bool tmp_condition_result_27;
        int tmp_truth_name_5;
        PyObject *tmp_type_arg_9;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_bases_name_5;
        tmp_key_name_13 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_13 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_13, tmp_key_name_13 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_15;
        }
        tmp_condition_result_26 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_10;
        }
        else
        {
            goto condexpr_false_10;
        }
        condexpr_true_10:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_14 = tmp_class_creation_5__class_decl_dict;
        tmp_key_name_14 = const_str_plain_metaclass;
        tmp_metaclass_name_5 = DICT_GET_ITEM( tmp_dict_name_14, tmp_key_name_14 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_15;
        }
        goto condexpr_end_10;
        condexpr_false_10:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_truth_name_5 = CHECK_IF_TRUE( tmp_class_creation_5__bases );
        if ( tmp_truth_name_5 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_15;
        }
        tmp_condition_result_27 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_27 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_11;
        }
        else
        {
            goto condexpr_false_11;
        }
        condexpr_true_11:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_subscribed_name_5 = tmp_class_creation_5__bases;
        tmp_subscript_name_5 = const_int_0;
        tmp_type_arg_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 0 );
        if ( tmp_type_arg_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_15;
        }
        tmp_metaclass_name_5 = BUILTIN_TYPE1( tmp_type_arg_9 );
        Py_DECREF( tmp_type_arg_9 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_15;
        }
        goto condexpr_end_11;
        condexpr_false_11:;
        tmp_metaclass_name_5 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_5 );
        condexpr_end_11:;
        condexpr_end_10:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_bases_name_5 = tmp_class_creation_5__bases;
        tmp_assign_source_50 = SELECT_METACLASS( tmp_metaclass_name_5, tmp_bases_name_5 );
        Py_DECREF( tmp_metaclass_name_5 );
        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_15;
        }
        assert( tmp_class_creation_5__metaclass == NULL );
        tmp_class_creation_5__metaclass = tmp_assign_source_50;
    }
    {
        nuitka_bool tmp_condition_result_28;
        PyObject *tmp_key_name_15;
        PyObject *tmp_dict_name_15;
        tmp_key_name_15 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_15 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_15, tmp_key_name_15 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_15;
        }
        tmp_condition_result_28 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_28 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_17;
        }
        else
        {
            goto branch_no_17;
        }
        branch_yes_17:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_5__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_15;
        }
        branch_no_17:;
    }
    {
        nuitka_bool tmp_condition_result_29;
        PyObject *tmp_source_name_17;
        CHECK_OBJECT( tmp_class_creation_5__metaclass );
        tmp_source_name_17 = tmp_class_creation_5__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_17, const_str_plain___prepare__ );
        tmp_condition_result_29 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_29 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_18;
        }
        else
        {
            goto branch_no_18;
        }
        branch_yes_18:;
        {
            PyObject *tmp_assign_source_51;
            PyObject *tmp_called_name_11;
            PyObject *tmp_source_name_18;
            PyObject *tmp_args_name_9;
            PyObject *tmp_tuple_element_18;
            PyObject *tmp_kw_name_9;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_source_name_18 = tmp_class_creation_5__metaclass;
            tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain___prepare__ );
            if ( tmp_called_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_15;
            }
            tmp_tuple_element_18 = const_str_plain_Attribute;
            tmp_args_name_9 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_18 );
            PyTuple_SET_ITEM( tmp_args_name_9, 0, tmp_tuple_element_18 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_18 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_18 );
            PyTuple_SET_ITEM( tmp_args_name_9, 1, tmp_tuple_element_18 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_9 = tmp_class_creation_5__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 60;
            tmp_assign_source_51 = CALL_FUNCTION( tmp_called_name_11, tmp_args_name_9, tmp_kw_name_9 );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_name_9 );
            if ( tmp_assign_source_51 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_15;
            }
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_51;
        }
        {
            nuitka_bool tmp_condition_result_30;
            PyObject *tmp_operand_name_5;
            PyObject *tmp_source_name_19;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_source_name_19 = tmp_class_creation_5__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_19, const_str_plain___getitem__ );
            tmp_operand_name_5 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_15;
            }
            tmp_condition_result_30 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_30 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_19;
            }
            else
            {
                goto branch_no_19;
            }
            branch_yes_19:;
            {
                PyObject *tmp_raise_type_5;
                PyObject *tmp_raise_value_5;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                PyObject *tmp_tuple_element_19;
                PyObject *tmp_getattr_target_5;
                PyObject *tmp_getattr_attr_5;
                PyObject *tmp_getattr_default_5;
                PyObject *tmp_source_name_20;
                PyObject *tmp_type_arg_10;
                tmp_raise_type_5 = PyExc_TypeError;
                tmp_left_name_5 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_5__metaclass );
                tmp_getattr_target_5 = tmp_class_creation_5__metaclass;
                tmp_getattr_attr_5 = const_str_plain___name__;
                tmp_getattr_default_5 = const_str_angle_metaclass;
                tmp_tuple_element_19 = BUILTIN_GETATTR( tmp_getattr_target_5, tmp_getattr_attr_5, tmp_getattr_default_5 );
                if ( tmp_tuple_element_19 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 60;

                    goto try_except_handler_15;
                }
                tmp_right_name_5 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_5, 0, tmp_tuple_element_19 );
                CHECK_OBJECT( tmp_class_creation_5__prepared );
                tmp_type_arg_10 = tmp_class_creation_5__prepared;
                tmp_source_name_20 = BUILTIN_TYPE1( tmp_type_arg_10 );
                assert( !(tmp_source_name_20 == NULL) );
                tmp_tuple_element_19 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_20 );
                if ( tmp_tuple_element_19 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_5 );

                    exception_lineno = 60;

                    goto try_except_handler_15;
                }
                PyTuple_SET_ITEM( tmp_right_name_5, 1, tmp_tuple_element_19 );
                tmp_raise_value_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                Py_DECREF( tmp_right_name_5 );
                if ( tmp_raise_value_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 60;

                    goto try_except_handler_15;
                }
                exception_type = tmp_raise_type_5;
                Py_INCREF( tmp_raise_type_5 );
                exception_value = tmp_raise_value_5;
                exception_lineno = 60;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_15;
            }
            branch_no_19:;
        }
        goto branch_end_18;
        branch_no_18:;
        {
            PyObject *tmp_assign_source_52;
            tmp_assign_source_52 = PyDict_New();
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_52;
        }
        branch_end_18:;
    }
    {
        PyObject *tmp_assign_source_53;
        {
            PyObject *tmp_set_locals_5;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_set_locals_5 = tmp_class_creation_5__prepared;
            locals_yargy$interpretation$attribute_60 = tmp_set_locals_5;
            Py_INCREF( tmp_set_locals_5 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_60, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_17;
        }
        tmp_dictset_value = const_str_plain_Attribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_60, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;

            goto try_except_handler_17;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_43b522f96e9464f6cf11e230d6f1ddf8_6, codeobj_43b522f96e9464f6cf11e230d6f1ddf8, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_43b522f96e9464f6cf11e230d6f1ddf8_6 = cache_frame_43b522f96e9464f6cf11e230d6f1ddf8_6;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_43b522f96e9464f6cf11e230d6f1ddf8_6 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_43b522f96e9464f6cf11e230d6f1ddf8_6 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_fact_str_plain_name_str_plain_default_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_60, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_8___init__(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_60, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }
        {
            PyObject *tmp_defaults_2;
            PyObject *tmp_tuple_element_20;
            tmp_tuple_element_20 = PySet_New( const_set_9bc13a39912b4878e1e8af7feb45ece3 );
            tmp_defaults_2 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_defaults_2, 0, tmp_tuple_element_20 );
            tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_9_inflected( tmp_defaults_2 );



            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_60, const_str_plain_inflected, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_2 = "o";
                goto frame_exception_exit_6;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_10_normalized(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_60, const_str_plain_normalized, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_11_const(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_60, const_str_plain_const, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_12_custom(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_60, const_str_plain_custom, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_43b522f96e9464f6cf11e230d6f1ddf8_6 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_5;

        frame_exception_exit_6:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_43b522f96e9464f6cf11e230d6f1ddf8_6 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_43b522f96e9464f6cf11e230d6f1ddf8_6, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_43b522f96e9464f6cf11e230d6f1ddf8_6->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_43b522f96e9464f6cf11e230d6f1ddf8_6, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_43b522f96e9464f6cf11e230d6f1ddf8_6,
            type_description_2,
            outline_4_var___class__
        );


        // Release cached frame.
        if ( frame_43b522f96e9464f6cf11e230d6f1ddf8_6 == cache_frame_43b522f96e9464f6cf11e230d6f1ddf8_6 )
        {
            Py_DECREF( frame_43b522f96e9464f6cf11e230d6f1ddf8_6 );
        }
        cache_frame_43b522f96e9464f6cf11e230d6f1ddf8_6 = NULL;

        assertFrameObject( frame_43b522f96e9464f6cf11e230d6f1ddf8_6 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_5;

        frame_no_exception_5:;
        goto skip_nested_handling_5;
        nested_frame_exit_5:;

        goto try_except_handler_17;
        skip_nested_handling_5:;
        {
            nuitka_bool tmp_condition_result_31;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_compexpr_left_5 = tmp_class_creation_5__bases;
            CHECK_OBJECT( tmp_class_creation_5__bases_orig );
            tmp_compexpr_right_5 = tmp_class_creation_5__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_17;
            }
            tmp_condition_result_31 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_31 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_20;
            }
            else
            {
                goto branch_no_20;
            }
            branch_yes_20:;
            CHECK_OBJECT( tmp_class_creation_5__bases_orig );
            tmp_dictset_value = tmp_class_creation_5__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_60, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_17;
            }
            branch_no_20:;
        }
        {
            PyObject *tmp_assign_source_54;
            PyObject *tmp_called_name_12;
            PyObject *tmp_args_name_10;
            PyObject *tmp_tuple_element_21;
            PyObject *tmp_kw_name_10;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_called_name_12 = tmp_class_creation_5__metaclass;
            tmp_tuple_element_21 = const_str_plain_Attribute;
            tmp_args_name_10 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_args_name_10, 0, tmp_tuple_element_21 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_21 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_args_name_10, 1, tmp_tuple_element_21 );
            tmp_tuple_element_21 = locals_yargy$interpretation$attribute_60;
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_args_name_10, 2, tmp_tuple_element_21 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_10 = tmp_class_creation_5__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 60;
            tmp_assign_source_54 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_10, tmp_kw_name_10 );
            Py_DECREF( tmp_args_name_10 );
            if ( tmp_assign_source_54 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_17;
            }
            assert( outline_4_var___class__ == NULL );
            outline_4_var___class__ = tmp_assign_source_54;
        }
        CHECK_OBJECT( outline_4_var___class__ );
        tmp_assign_source_53 = outline_4_var___class__;
        Py_INCREF( tmp_assign_source_53 );
        goto try_return_handler_17;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_17:;
        Py_DECREF( locals_yargy$interpretation$attribute_60 );
        locals_yargy$interpretation$attribute_60 = NULL;
        goto try_return_handler_16;
        // Exception handler code:
        try_except_handler_17:;
        exception_keeper_type_15 = exception_type;
        exception_keeper_value_15 = exception_value;
        exception_keeper_tb_15 = exception_tb;
        exception_keeper_lineno_15 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_60 );
        locals_yargy$interpretation$attribute_60 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_15;
        exception_value = exception_keeper_value_15;
        exception_tb = exception_keeper_tb_15;
        exception_lineno = exception_keeper_lineno_15;

        goto try_except_handler_16;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_16:;
        CHECK_OBJECT( (PyObject *)outline_4_var___class__ );
        Py_DECREF( outline_4_var___class__ );
        outline_4_var___class__ = NULL;

        goto outline_result_5;
        // Exception handler code:
        try_except_handler_16:;
        exception_keeper_type_16 = exception_type;
        exception_keeper_value_16 = exception_value;
        exception_keeper_tb_16 = exception_tb;
        exception_keeper_lineno_16 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_16;
        exception_value = exception_keeper_value_16;
        exception_tb = exception_keeper_tb_16;
        exception_lineno = exception_keeper_lineno_16;

        goto outline_exception_5;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_5:;
        exception_lineno = 60;
        goto try_except_handler_15;
        outline_result_5:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_Attribute, tmp_assign_source_53 );
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_15:;
    exception_keeper_type_17 = exception_type;
    exception_keeper_value_17 = exception_value;
    exception_keeper_tb_17 = exception_tb;
    exception_keeper_lineno_17 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_5__bases_orig );
    tmp_class_creation_5__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    Py_XDECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_17;
    exception_value = exception_keeper_value_17;
    exception_tb = exception_keeper_tb_17;
    exception_lineno = exception_keeper_lineno_17;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases_orig );
    Py_DECREF( tmp_class_creation_5__bases_orig );
    tmp_class_creation_5__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases );
    Py_DECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__class_decl_dict );
    Py_DECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__metaclass );
    Py_DECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__prepared );
    Py_DECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_55;
        PyObject *tmp_tuple_element_22;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_AttributeBase );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AttributeBase );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AttributeBase" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 81;

            goto try_except_handler_18;
        }

        tmp_tuple_element_22 = tmp_mvar_value_8;
        tmp_assign_source_55 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_22 );
        PyTuple_SET_ITEM( tmp_assign_source_55, 0, tmp_tuple_element_22 );
        assert( tmp_class_creation_6__bases_orig == NULL );
        tmp_class_creation_6__bases_orig = tmp_assign_source_55;
    }
    {
        PyObject *tmp_assign_source_56;
        PyObject *tmp_dircall_arg1_6;
        CHECK_OBJECT( tmp_class_creation_6__bases_orig );
        tmp_dircall_arg1_6 = tmp_class_creation_6__bases_orig;
        Py_INCREF( tmp_dircall_arg1_6 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_6};
            tmp_assign_source_56 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_18;
        }
        assert( tmp_class_creation_6__bases == NULL );
        tmp_class_creation_6__bases = tmp_assign_source_56;
    }
    {
        PyObject *tmp_assign_source_57;
        tmp_assign_source_57 = PyDict_New();
        assert( tmp_class_creation_6__class_decl_dict == NULL );
        tmp_class_creation_6__class_decl_dict = tmp_assign_source_57;
    }
    {
        PyObject *tmp_assign_source_58;
        PyObject *tmp_metaclass_name_6;
        nuitka_bool tmp_condition_result_32;
        PyObject *tmp_key_name_16;
        PyObject *tmp_dict_name_16;
        PyObject *tmp_dict_name_17;
        PyObject *tmp_key_name_17;
        nuitka_bool tmp_condition_result_33;
        int tmp_truth_name_6;
        PyObject *tmp_type_arg_11;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_subscript_name_6;
        PyObject *tmp_bases_name_6;
        tmp_key_name_16 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_16 = tmp_class_creation_6__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_16, tmp_key_name_16 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_18;
        }
        tmp_condition_result_32 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_32 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_12;
        }
        else
        {
            goto condexpr_false_12;
        }
        condexpr_true_12:;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_17 = tmp_class_creation_6__class_decl_dict;
        tmp_key_name_17 = const_str_plain_metaclass;
        tmp_metaclass_name_6 = DICT_GET_ITEM( tmp_dict_name_17, tmp_key_name_17 );
        if ( tmp_metaclass_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_18;
        }
        goto condexpr_end_12;
        condexpr_false_12:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_truth_name_6 = CHECK_IF_TRUE( tmp_class_creation_6__bases );
        if ( tmp_truth_name_6 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_18;
        }
        tmp_condition_result_33 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_33 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_13;
        }
        else
        {
            goto condexpr_false_13;
        }
        condexpr_true_13:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_subscribed_name_6 = tmp_class_creation_6__bases;
        tmp_subscript_name_6 = const_int_0;
        tmp_type_arg_11 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 0 );
        if ( tmp_type_arg_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_18;
        }
        tmp_metaclass_name_6 = BUILTIN_TYPE1( tmp_type_arg_11 );
        Py_DECREF( tmp_type_arg_11 );
        if ( tmp_metaclass_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_18;
        }
        goto condexpr_end_13;
        condexpr_false_13:;
        tmp_metaclass_name_6 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_6 );
        condexpr_end_13:;
        condexpr_end_12:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_bases_name_6 = tmp_class_creation_6__bases;
        tmp_assign_source_58 = SELECT_METACLASS( tmp_metaclass_name_6, tmp_bases_name_6 );
        Py_DECREF( tmp_metaclass_name_6 );
        if ( tmp_assign_source_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_18;
        }
        assert( tmp_class_creation_6__metaclass == NULL );
        tmp_class_creation_6__metaclass = tmp_assign_source_58;
    }
    {
        nuitka_bool tmp_condition_result_34;
        PyObject *tmp_key_name_18;
        PyObject *tmp_dict_name_18;
        tmp_key_name_18 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_18 = tmp_class_creation_6__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_18, tmp_key_name_18 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_18;
        }
        tmp_condition_result_34 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_34 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_21;
        }
        else
        {
            goto branch_no_21;
        }
        branch_yes_21:;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_6__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_18;
        }
        branch_no_21:;
    }
    {
        nuitka_bool tmp_condition_result_35;
        PyObject *tmp_source_name_21;
        CHECK_OBJECT( tmp_class_creation_6__metaclass );
        tmp_source_name_21 = tmp_class_creation_6__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_21, const_str_plain___prepare__ );
        tmp_condition_result_35 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_35 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_22;
        }
        else
        {
            goto branch_no_22;
        }
        branch_yes_22:;
        {
            PyObject *tmp_assign_source_59;
            PyObject *tmp_called_name_13;
            PyObject *tmp_source_name_22;
            PyObject *tmp_args_name_11;
            PyObject *tmp_tuple_element_23;
            PyObject *tmp_kw_name_11;
            CHECK_OBJECT( tmp_class_creation_6__metaclass );
            tmp_source_name_22 = tmp_class_creation_6__metaclass;
            tmp_called_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain___prepare__ );
            if ( tmp_called_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;

                goto try_except_handler_18;
            }
            tmp_tuple_element_23 = const_str_plain_RepeatableAttribute;
            tmp_args_name_11 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_args_name_11, 0, tmp_tuple_element_23 );
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_tuple_element_23 = tmp_class_creation_6__bases;
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_args_name_11, 1, tmp_tuple_element_23 );
            CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
            tmp_kw_name_11 = tmp_class_creation_6__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 81;
            tmp_assign_source_59 = CALL_FUNCTION( tmp_called_name_13, tmp_args_name_11, tmp_kw_name_11 );
            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_args_name_11 );
            if ( tmp_assign_source_59 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;

                goto try_except_handler_18;
            }
            assert( tmp_class_creation_6__prepared == NULL );
            tmp_class_creation_6__prepared = tmp_assign_source_59;
        }
        {
            nuitka_bool tmp_condition_result_36;
            PyObject *tmp_operand_name_6;
            PyObject *tmp_source_name_23;
            CHECK_OBJECT( tmp_class_creation_6__prepared );
            tmp_source_name_23 = tmp_class_creation_6__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_23, const_str_plain___getitem__ );
            tmp_operand_name_6 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;

                goto try_except_handler_18;
            }
            tmp_condition_result_36 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_36 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_23;
            }
            else
            {
                goto branch_no_23;
            }
            branch_yes_23:;
            {
                PyObject *tmp_raise_type_6;
                PyObject *tmp_raise_value_6;
                PyObject *tmp_left_name_6;
                PyObject *tmp_right_name_6;
                PyObject *tmp_tuple_element_24;
                PyObject *tmp_getattr_target_6;
                PyObject *tmp_getattr_attr_6;
                PyObject *tmp_getattr_default_6;
                PyObject *tmp_source_name_24;
                PyObject *tmp_type_arg_12;
                tmp_raise_type_6 = PyExc_TypeError;
                tmp_left_name_6 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_6__metaclass );
                tmp_getattr_target_6 = tmp_class_creation_6__metaclass;
                tmp_getattr_attr_6 = const_str_plain___name__;
                tmp_getattr_default_6 = const_str_angle_metaclass;
                tmp_tuple_element_24 = BUILTIN_GETATTR( tmp_getattr_target_6, tmp_getattr_attr_6, tmp_getattr_default_6 );
                if ( tmp_tuple_element_24 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 81;

                    goto try_except_handler_18;
                }
                tmp_right_name_6 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_6, 0, tmp_tuple_element_24 );
                CHECK_OBJECT( tmp_class_creation_6__prepared );
                tmp_type_arg_12 = tmp_class_creation_6__prepared;
                tmp_source_name_24 = BUILTIN_TYPE1( tmp_type_arg_12 );
                assert( !(tmp_source_name_24 == NULL) );
                tmp_tuple_element_24 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_24 );
                if ( tmp_tuple_element_24 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_6 );

                    exception_lineno = 81;

                    goto try_except_handler_18;
                }
                PyTuple_SET_ITEM( tmp_right_name_6, 1, tmp_tuple_element_24 );
                tmp_raise_value_6 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
                Py_DECREF( tmp_right_name_6 );
                if ( tmp_raise_value_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 81;

                    goto try_except_handler_18;
                }
                exception_type = tmp_raise_type_6;
                Py_INCREF( tmp_raise_type_6 );
                exception_value = tmp_raise_value_6;
                exception_lineno = 81;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_18;
            }
            branch_no_23:;
        }
        goto branch_end_22;
        branch_no_22:;
        {
            PyObject *tmp_assign_source_60;
            tmp_assign_source_60 = PyDict_New();
            assert( tmp_class_creation_6__prepared == NULL );
            tmp_class_creation_6__prepared = tmp_assign_source_60;
        }
        branch_end_22:;
    }
    {
        PyObject *tmp_assign_source_61;
        {
            PyObject *tmp_set_locals_6;
            CHECK_OBJECT( tmp_class_creation_6__prepared );
            tmp_set_locals_6 = tmp_class_creation_6__prepared;
            locals_yargy$interpretation$attribute_81 = tmp_set_locals_6;
            Py_INCREF( tmp_set_locals_6 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_81, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_20;
        }
        tmp_dictset_value = const_str_plain_RepeatableAttribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_81, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_20;
        }
        {
            nuitka_bool tmp_condition_result_37;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_compexpr_left_6 = tmp_class_creation_6__bases;
            CHECK_OBJECT( tmp_class_creation_6__bases_orig );
            tmp_compexpr_right_6 = tmp_class_creation_6__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;

                goto try_except_handler_20;
            }
            tmp_condition_result_37 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_37 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_24;
            }
            else
            {
                goto branch_no_24;
            }
            branch_yes_24:;
            CHECK_OBJECT( tmp_class_creation_6__bases_orig );
            tmp_dictset_value = tmp_class_creation_6__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_81, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;

                goto try_except_handler_20;
            }
            branch_no_24:;
        }
        {
            PyObject *tmp_assign_source_62;
            PyObject *tmp_called_name_14;
            PyObject *tmp_args_name_12;
            PyObject *tmp_tuple_element_25;
            PyObject *tmp_kw_name_12;
            CHECK_OBJECT( tmp_class_creation_6__metaclass );
            tmp_called_name_14 = tmp_class_creation_6__metaclass;
            tmp_tuple_element_25 = const_str_plain_RepeatableAttribute;
            tmp_args_name_12 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_25 );
            PyTuple_SET_ITEM( tmp_args_name_12, 0, tmp_tuple_element_25 );
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_tuple_element_25 = tmp_class_creation_6__bases;
            Py_INCREF( tmp_tuple_element_25 );
            PyTuple_SET_ITEM( tmp_args_name_12, 1, tmp_tuple_element_25 );
            tmp_tuple_element_25 = locals_yargy$interpretation$attribute_81;
            Py_INCREF( tmp_tuple_element_25 );
            PyTuple_SET_ITEM( tmp_args_name_12, 2, tmp_tuple_element_25 );
            CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
            tmp_kw_name_12 = tmp_class_creation_6__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 81;
            tmp_assign_source_62 = CALL_FUNCTION( tmp_called_name_14, tmp_args_name_12, tmp_kw_name_12 );
            Py_DECREF( tmp_args_name_12 );
            if ( tmp_assign_source_62 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 81;

                goto try_except_handler_20;
            }
            assert( outline_5_var___class__ == NULL );
            outline_5_var___class__ = tmp_assign_source_62;
        }
        CHECK_OBJECT( outline_5_var___class__ );
        tmp_assign_source_61 = outline_5_var___class__;
        Py_INCREF( tmp_assign_source_61 );
        goto try_return_handler_20;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_20:;
        Py_DECREF( locals_yargy$interpretation$attribute_81 );
        locals_yargy$interpretation$attribute_81 = NULL;
        goto try_return_handler_19;
        // Exception handler code:
        try_except_handler_20:;
        exception_keeper_type_18 = exception_type;
        exception_keeper_value_18 = exception_value;
        exception_keeper_tb_18 = exception_tb;
        exception_keeper_lineno_18 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_81 );
        locals_yargy$interpretation$attribute_81 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_18;
        exception_value = exception_keeper_value_18;
        exception_tb = exception_keeper_tb_18;
        exception_lineno = exception_keeper_lineno_18;

        goto try_except_handler_19;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_19:;
        CHECK_OBJECT( (PyObject *)outline_5_var___class__ );
        Py_DECREF( outline_5_var___class__ );
        outline_5_var___class__ = NULL;

        goto outline_result_6;
        // Exception handler code:
        try_except_handler_19:;
        exception_keeper_type_19 = exception_type;
        exception_keeper_value_19 = exception_value;
        exception_keeper_tb_19 = exception_tb;
        exception_keeper_lineno_19 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_19;
        exception_value = exception_keeper_value_19;
        exception_tb = exception_keeper_tb_19;
        exception_lineno = exception_keeper_lineno_19;

        goto outline_exception_6;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_6:;
        exception_lineno = 81;
        goto try_except_handler_18;
        outline_result_6:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_RepeatableAttribute, tmp_assign_source_61 );
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_18:;
    exception_keeper_type_20 = exception_type;
    exception_keeper_value_20 = exception_value;
    exception_keeper_tb_20 = exception_tb;
    exception_keeper_lineno_20 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_6__bases_orig );
    tmp_class_creation_6__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_6__bases );
    tmp_class_creation_6__bases = NULL;

    Py_XDECREF( tmp_class_creation_6__class_decl_dict );
    tmp_class_creation_6__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_6__metaclass );
    tmp_class_creation_6__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_6__prepared );
    tmp_class_creation_6__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_20;
    exception_value = exception_keeper_value_20;
    exception_tb = exception_keeper_tb_20;
    exception_lineno = exception_keeper_lineno_20;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__bases_orig );
    Py_DECREF( tmp_class_creation_6__bases_orig );
    tmp_class_creation_6__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__bases );
    Py_DECREF( tmp_class_creation_6__bases );
    tmp_class_creation_6__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__class_decl_dict );
    Py_DECREF( tmp_class_creation_6__class_decl_dict );
    tmp_class_creation_6__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__metaclass );
    Py_DECREF( tmp_class_creation_6__metaclass );
    tmp_class_creation_6__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__prepared );
    Py_DECREF( tmp_class_creation_6__prepared );
    tmp_class_creation_6__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_63;
        PyObject *tmp_tuple_element_26;
        PyObject *tmp_mvar_value_9;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_AttributeBase );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AttributeBase );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AttributeBase" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 85;

            goto try_except_handler_21;
        }

        tmp_tuple_element_26 = tmp_mvar_value_9;
        tmp_assign_source_63 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_26 );
        PyTuple_SET_ITEM( tmp_assign_source_63, 0, tmp_tuple_element_26 );
        assert( tmp_class_creation_7__bases_orig == NULL );
        tmp_class_creation_7__bases_orig = tmp_assign_source_63;
    }
    {
        PyObject *tmp_assign_source_64;
        PyObject *tmp_dircall_arg1_7;
        CHECK_OBJECT( tmp_class_creation_7__bases_orig );
        tmp_dircall_arg1_7 = tmp_class_creation_7__bases_orig;
        Py_INCREF( tmp_dircall_arg1_7 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_7};
            tmp_assign_source_64 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_21;
        }
        assert( tmp_class_creation_7__bases == NULL );
        tmp_class_creation_7__bases = tmp_assign_source_64;
    }
    {
        PyObject *tmp_assign_source_65;
        tmp_assign_source_65 = PyDict_New();
        assert( tmp_class_creation_7__class_decl_dict == NULL );
        tmp_class_creation_7__class_decl_dict = tmp_assign_source_65;
    }
    {
        PyObject *tmp_assign_source_66;
        PyObject *tmp_metaclass_name_7;
        nuitka_bool tmp_condition_result_38;
        PyObject *tmp_key_name_19;
        PyObject *tmp_dict_name_19;
        PyObject *tmp_dict_name_20;
        PyObject *tmp_key_name_20;
        nuitka_bool tmp_condition_result_39;
        int tmp_truth_name_7;
        PyObject *tmp_type_arg_13;
        PyObject *tmp_subscribed_name_7;
        PyObject *tmp_subscript_name_7;
        PyObject *tmp_bases_name_7;
        tmp_key_name_19 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
        tmp_dict_name_19 = tmp_class_creation_7__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_19, tmp_key_name_19 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_21;
        }
        tmp_condition_result_38 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_38 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_14;
        }
        else
        {
            goto condexpr_false_14;
        }
        condexpr_true_14:;
        CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
        tmp_dict_name_20 = tmp_class_creation_7__class_decl_dict;
        tmp_key_name_20 = const_str_plain_metaclass;
        tmp_metaclass_name_7 = DICT_GET_ITEM( tmp_dict_name_20, tmp_key_name_20 );
        if ( tmp_metaclass_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_21;
        }
        goto condexpr_end_14;
        condexpr_false_14:;
        CHECK_OBJECT( tmp_class_creation_7__bases );
        tmp_truth_name_7 = CHECK_IF_TRUE( tmp_class_creation_7__bases );
        if ( tmp_truth_name_7 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_21;
        }
        tmp_condition_result_39 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_39 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_15;
        }
        else
        {
            goto condexpr_false_15;
        }
        condexpr_true_15:;
        CHECK_OBJECT( tmp_class_creation_7__bases );
        tmp_subscribed_name_7 = tmp_class_creation_7__bases;
        tmp_subscript_name_7 = const_int_0;
        tmp_type_arg_13 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_7, tmp_subscript_name_7, 0 );
        if ( tmp_type_arg_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_21;
        }
        tmp_metaclass_name_7 = BUILTIN_TYPE1( tmp_type_arg_13 );
        Py_DECREF( tmp_type_arg_13 );
        if ( tmp_metaclass_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_21;
        }
        goto condexpr_end_15;
        condexpr_false_15:;
        tmp_metaclass_name_7 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_7 );
        condexpr_end_15:;
        condexpr_end_14:;
        CHECK_OBJECT( tmp_class_creation_7__bases );
        tmp_bases_name_7 = tmp_class_creation_7__bases;
        tmp_assign_source_66 = SELECT_METACLASS( tmp_metaclass_name_7, tmp_bases_name_7 );
        Py_DECREF( tmp_metaclass_name_7 );
        if ( tmp_assign_source_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_21;
        }
        assert( tmp_class_creation_7__metaclass == NULL );
        tmp_class_creation_7__metaclass = tmp_assign_source_66;
    }
    {
        nuitka_bool tmp_condition_result_40;
        PyObject *tmp_key_name_21;
        PyObject *tmp_dict_name_21;
        tmp_key_name_21 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
        tmp_dict_name_21 = tmp_class_creation_7__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_21, tmp_key_name_21 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_21;
        }
        tmp_condition_result_40 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_40 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_25;
        }
        else
        {
            goto branch_no_25;
        }
        branch_yes_25:;
        CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_7__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_21;
        }
        branch_no_25:;
    }
    {
        nuitka_bool tmp_condition_result_41;
        PyObject *tmp_source_name_25;
        CHECK_OBJECT( tmp_class_creation_7__metaclass );
        tmp_source_name_25 = tmp_class_creation_7__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_25, const_str_plain___prepare__ );
        tmp_condition_result_41 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_41 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_26;
        }
        else
        {
            goto branch_no_26;
        }
        branch_yes_26:;
        {
            PyObject *tmp_assign_source_67;
            PyObject *tmp_called_name_15;
            PyObject *tmp_source_name_26;
            PyObject *tmp_args_name_13;
            PyObject *tmp_tuple_element_27;
            PyObject *tmp_kw_name_13;
            CHECK_OBJECT( tmp_class_creation_7__metaclass );
            tmp_source_name_26 = tmp_class_creation_7__metaclass;
            tmp_called_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain___prepare__ );
            if ( tmp_called_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;

                goto try_except_handler_21;
            }
            tmp_tuple_element_27 = const_str_plain_MorphAttribute;
            tmp_args_name_13 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_27 );
            PyTuple_SET_ITEM( tmp_args_name_13, 0, tmp_tuple_element_27 );
            CHECK_OBJECT( tmp_class_creation_7__bases );
            tmp_tuple_element_27 = tmp_class_creation_7__bases;
            Py_INCREF( tmp_tuple_element_27 );
            PyTuple_SET_ITEM( tmp_args_name_13, 1, tmp_tuple_element_27 );
            CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
            tmp_kw_name_13 = tmp_class_creation_7__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 85;
            tmp_assign_source_67 = CALL_FUNCTION( tmp_called_name_15, tmp_args_name_13, tmp_kw_name_13 );
            Py_DECREF( tmp_called_name_15 );
            Py_DECREF( tmp_args_name_13 );
            if ( tmp_assign_source_67 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;

                goto try_except_handler_21;
            }
            assert( tmp_class_creation_7__prepared == NULL );
            tmp_class_creation_7__prepared = tmp_assign_source_67;
        }
        {
            nuitka_bool tmp_condition_result_42;
            PyObject *tmp_operand_name_7;
            PyObject *tmp_source_name_27;
            CHECK_OBJECT( tmp_class_creation_7__prepared );
            tmp_source_name_27 = tmp_class_creation_7__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_27, const_str_plain___getitem__ );
            tmp_operand_name_7 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;

                goto try_except_handler_21;
            }
            tmp_condition_result_42 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_42 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_27;
            }
            else
            {
                goto branch_no_27;
            }
            branch_yes_27:;
            {
                PyObject *tmp_raise_type_7;
                PyObject *tmp_raise_value_7;
                PyObject *tmp_left_name_7;
                PyObject *tmp_right_name_7;
                PyObject *tmp_tuple_element_28;
                PyObject *tmp_getattr_target_7;
                PyObject *tmp_getattr_attr_7;
                PyObject *tmp_getattr_default_7;
                PyObject *tmp_source_name_28;
                PyObject *tmp_type_arg_14;
                tmp_raise_type_7 = PyExc_TypeError;
                tmp_left_name_7 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_7__metaclass );
                tmp_getattr_target_7 = tmp_class_creation_7__metaclass;
                tmp_getattr_attr_7 = const_str_plain___name__;
                tmp_getattr_default_7 = const_str_angle_metaclass;
                tmp_tuple_element_28 = BUILTIN_GETATTR( tmp_getattr_target_7, tmp_getattr_attr_7, tmp_getattr_default_7 );
                if ( tmp_tuple_element_28 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 85;

                    goto try_except_handler_21;
                }
                tmp_right_name_7 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_7, 0, tmp_tuple_element_28 );
                CHECK_OBJECT( tmp_class_creation_7__prepared );
                tmp_type_arg_14 = tmp_class_creation_7__prepared;
                tmp_source_name_28 = BUILTIN_TYPE1( tmp_type_arg_14 );
                assert( !(tmp_source_name_28 == NULL) );
                tmp_tuple_element_28 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_28 );
                if ( tmp_tuple_element_28 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_7 );

                    exception_lineno = 85;

                    goto try_except_handler_21;
                }
                PyTuple_SET_ITEM( tmp_right_name_7, 1, tmp_tuple_element_28 );
                tmp_raise_value_7 = BINARY_OPERATION_REMAINDER( tmp_left_name_7, tmp_right_name_7 );
                Py_DECREF( tmp_right_name_7 );
                if ( tmp_raise_value_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 85;

                    goto try_except_handler_21;
                }
                exception_type = tmp_raise_type_7;
                Py_INCREF( tmp_raise_type_7 );
                exception_value = tmp_raise_value_7;
                exception_lineno = 85;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_21;
            }
            branch_no_27:;
        }
        goto branch_end_26;
        branch_no_26:;
        {
            PyObject *tmp_assign_source_68;
            tmp_assign_source_68 = PyDict_New();
            assert( tmp_class_creation_7__prepared == NULL );
            tmp_class_creation_7__prepared = tmp_assign_source_68;
        }
        branch_end_26:;
    }
    {
        PyObject *tmp_assign_source_69;
        {
            PyObject *tmp_set_locals_7;
            CHECK_OBJECT( tmp_class_creation_7__prepared );
            tmp_set_locals_7 = tmp_class_creation_7__prepared;
            locals_yargy$interpretation$attribute_85 = tmp_set_locals_7;
            Py_INCREF( tmp_set_locals_7 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_85, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_23;
        }
        tmp_dictset_value = const_str_plain_MorphAttribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_85, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 85;

            goto try_except_handler_23;
        }
        {
            nuitka_bool tmp_condition_result_43;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            CHECK_OBJECT( tmp_class_creation_7__bases );
            tmp_compexpr_left_7 = tmp_class_creation_7__bases;
            CHECK_OBJECT( tmp_class_creation_7__bases_orig );
            tmp_compexpr_right_7 = tmp_class_creation_7__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;

                goto try_except_handler_23;
            }
            tmp_condition_result_43 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_43 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_28;
            }
            else
            {
                goto branch_no_28;
            }
            branch_yes_28:;
            CHECK_OBJECT( tmp_class_creation_7__bases_orig );
            tmp_dictset_value = tmp_class_creation_7__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_85, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;

                goto try_except_handler_23;
            }
            branch_no_28:;
        }
        {
            PyObject *tmp_assign_source_70;
            PyObject *tmp_called_name_16;
            PyObject *tmp_args_name_14;
            PyObject *tmp_tuple_element_29;
            PyObject *tmp_kw_name_14;
            CHECK_OBJECT( tmp_class_creation_7__metaclass );
            tmp_called_name_16 = tmp_class_creation_7__metaclass;
            tmp_tuple_element_29 = const_str_plain_MorphAttribute;
            tmp_args_name_14 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_29 );
            PyTuple_SET_ITEM( tmp_args_name_14, 0, tmp_tuple_element_29 );
            CHECK_OBJECT( tmp_class_creation_7__bases );
            tmp_tuple_element_29 = tmp_class_creation_7__bases;
            Py_INCREF( tmp_tuple_element_29 );
            PyTuple_SET_ITEM( tmp_args_name_14, 1, tmp_tuple_element_29 );
            tmp_tuple_element_29 = locals_yargy$interpretation$attribute_85;
            Py_INCREF( tmp_tuple_element_29 );
            PyTuple_SET_ITEM( tmp_args_name_14, 2, tmp_tuple_element_29 );
            CHECK_OBJECT( tmp_class_creation_7__class_decl_dict );
            tmp_kw_name_14 = tmp_class_creation_7__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 85;
            tmp_assign_source_70 = CALL_FUNCTION( tmp_called_name_16, tmp_args_name_14, tmp_kw_name_14 );
            Py_DECREF( tmp_args_name_14 );
            if ( tmp_assign_source_70 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 85;

                goto try_except_handler_23;
            }
            assert( outline_6_var___class__ == NULL );
            outline_6_var___class__ = tmp_assign_source_70;
        }
        CHECK_OBJECT( outline_6_var___class__ );
        tmp_assign_source_69 = outline_6_var___class__;
        Py_INCREF( tmp_assign_source_69 );
        goto try_return_handler_23;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_23:;
        Py_DECREF( locals_yargy$interpretation$attribute_85 );
        locals_yargy$interpretation$attribute_85 = NULL;
        goto try_return_handler_22;
        // Exception handler code:
        try_except_handler_23:;
        exception_keeper_type_21 = exception_type;
        exception_keeper_value_21 = exception_value;
        exception_keeper_tb_21 = exception_tb;
        exception_keeper_lineno_21 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_85 );
        locals_yargy$interpretation$attribute_85 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_21;
        exception_value = exception_keeper_value_21;
        exception_tb = exception_keeper_tb_21;
        exception_lineno = exception_keeper_lineno_21;

        goto try_except_handler_22;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_22:;
        CHECK_OBJECT( (PyObject *)outline_6_var___class__ );
        Py_DECREF( outline_6_var___class__ );
        outline_6_var___class__ = NULL;

        goto outline_result_7;
        // Exception handler code:
        try_except_handler_22:;
        exception_keeper_type_22 = exception_type;
        exception_keeper_value_22 = exception_value;
        exception_keeper_tb_22 = exception_tb;
        exception_keeper_lineno_22 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_22;
        exception_value = exception_keeper_value_22;
        exception_tb = exception_keeper_tb_22;
        exception_lineno = exception_keeper_lineno_22;

        goto outline_exception_7;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_7:;
        exception_lineno = 85;
        goto try_except_handler_21;
        outline_result_7:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_MorphAttribute, tmp_assign_source_69 );
    }
    goto try_end_9;
    // Exception handler code:
    try_except_handler_21:;
    exception_keeper_type_23 = exception_type;
    exception_keeper_value_23 = exception_value;
    exception_keeper_tb_23 = exception_tb;
    exception_keeper_lineno_23 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_7__bases_orig );
    tmp_class_creation_7__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_7__bases );
    tmp_class_creation_7__bases = NULL;

    Py_XDECREF( tmp_class_creation_7__class_decl_dict );
    tmp_class_creation_7__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_7__metaclass );
    tmp_class_creation_7__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_7__prepared );
    tmp_class_creation_7__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_23;
    exception_value = exception_keeper_value_23;
    exception_tb = exception_keeper_tb_23;
    exception_lineno = exception_keeper_lineno_23;

    goto frame_exception_exit_1;
    // End of try:
    try_end_9:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_7__bases_orig );
    Py_DECREF( tmp_class_creation_7__bases_orig );
    tmp_class_creation_7__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_7__bases );
    Py_DECREF( tmp_class_creation_7__bases );
    tmp_class_creation_7__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_7__class_decl_dict );
    Py_DECREF( tmp_class_creation_7__class_decl_dict );
    tmp_class_creation_7__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_7__metaclass );
    Py_DECREF( tmp_class_creation_7__metaclass );
    tmp_class_creation_7__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_7__prepared );
    Py_DECREF( tmp_class_creation_7__prepared );
    tmp_class_creation_7__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_71;
        PyObject *tmp_tuple_element_30;
        PyObject *tmp_mvar_value_10;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_MorphAttribute );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MorphAttribute );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MorphAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 89;

            goto try_except_handler_24;
        }

        tmp_tuple_element_30 = tmp_mvar_value_10;
        tmp_assign_source_71 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_30 );
        PyTuple_SET_ITEM( tmp_assign_source_71, 0, tmp_tuple_element_30 );
        assert( tmp_class_creation_8__bases_orig == NULL );
        tmp_class_creation_8__bases_orig = tmp_assign_source_71;
    }
    {
        PyObject *tmp_assign_source_72;
        PyObject *tmp_dircall_arg1_8;
        CHECK_OBJECT( tmp_class_creation_8__bases_orig );
        tmp_dircall_arg1_8 = tmp_class_creation_8__bases_orig;
        Py_INCREF( tmp_dircall_arg1_8 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_8};
            tmp_assign_source_72 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_72 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_24;
        }
        assert( tmp_class_creation_8__bases == NULL );
        tmp_class_creation_8__bases = tmp_assign_source_72;
    }
    {
        PyObject *tmp_assign_source_73;
        tmp_assign_source_73 = PyDict_New();
        assert( tmp_class_creation_8__class_decl_dict == NULL );
        tmp_class_creation_8__class_decl_dict = tmp_assign_source_73;
    }
    {
        PyObject *tmp_assign_source_74;
        PyObject *tmp_metaclass_name_8;
        nuitka_bool tmp_condition_result_44;
        PyObject *tmp_key_name_22;
        PyObject *tmp_dict_name_22;
        PyObject *tmp_dict_name_23;
        PyObject *tmp_key_name_23;
        nuitka_bool tmp_condition_result_45;
        int tmp_truth_name_8;
        PyObject *tmp_type_arg_15;
        PyObject *tmp_subscribed_name_8;
        PyObject *tmp_subscript_name_8;
        PyObject *tmp_bases_name_8;
        tmp_key_name_22 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
        tmp_dict_name_22 = tmp_class_creation_8__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_22, tmp_key_name_22 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_24;
        }
        tmp_condition_result_44 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_44 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_16;
        }
        else
        {
            goto condexpr_false_16;
        }
        condexpr_true_16:;
        CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
        tmp_dict_name_23 = tmp_class_creation_8__class_decl_dict;
        tmp_key_name_23 = const_str_plain_metaclass;
        tmp_metaclass_name_8 = DICT_GET_ITEM( tmp_dict_name_23, tmp_key_name_23 );
        if ( tmp_metaclass_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_24;
        }
        goto condexpr_end_16;
        condexpr_false_16:;
        CHECK_OBJECT( tmp_class_creation_8__bases );
        tmp_truth_name_8 = CHECK_IF_TRUE( tmp_class_creation_8__bases );
        if ( tmp_truth_name_8 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_24;
        }
        tmp_condition_result_45 = tmp_truth_name_8 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_45 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_17;
        }
        else
        {
            goto condexpr_false_17;
        }
        condexpr_true_17:;
        CHECK_OBJECT( tmp_class_creation_8__bases );
        tmp_subscribed_name_8 = tmp_class_creation_8__bases;
        tmp_subscript_name_8 = const_int_0;
        tmp_type_arg_15 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_8, tmp_subscript_name_8, 0 );
        if ( tmp_type_arg_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_24;
        }
        tmp_metaclass_name_8 = BUILTIN_TYPE1( tmp_type_arg_15 );
        Py_DECREF( tmp_type_arg_15 );
        if ( tmp_metaclass_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_24;
        }
        goto condexpr_end_17;
        condexpr_false_17:;
        tmp_metaclass_name_8 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_8 );
        condexpr_end_17:;
        condexpr_end_16:;
        CHECK_OBJECT( tmp_class_creation_8__bases );
        tmp_bases_name_8 = tmp_class_creation_8__bases;
        tmp_assign_source_74 = SELECT_METACLASS( tmp_metaclass_name_8, tmp_bases_name_8 );
        Py_DECREF( tmp_metaclass_name_8 );
        if ( tmp_assign_source_74 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_24;
        }
        assert( tmp_class_creation_8__metaclass == NULL );
        tmp_class_creation_8__metaclass = tmp_assign_source_74;
    }
    {
        nuitka_bool tmp_condition_result_46;
        PyObject *tmp_key_name_24;
        PyObject *tmp_dict_name_24;
        tmp_key_name_24 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
        tmp_dict_name_24 = tmp_class_creation_8__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_24, tmp_key_name_24 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_24;
        }
        tmp_condition_result_46 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_46 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_29;
        }
        else
        {
            goto branch_no_29;
        }
        branch_yes_29:;
        CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_8__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_24;
        }
        branch_no_29:;
    }
    {
        nuitka_bool tmp_condition_result_47;
        PyObject *tmp_source_name_29;
        CHECK_OBJECT( tmp_class_creation_8__metaclass );
        tmp_source_name_29 = tmp_class_creation_8__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_29, const_str_plain___prepare__ );
        tmp_condition_result_47 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_47 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_30;
        }
        else
        {
            goto branch_no_30;
        }
        branch_yes_30:;
        {
            PyObject *tmp_assign_source_75;
            PyObject *tmp_called_name_17;
            PyObject *tmp_source_name_30;
            PyObject *tmp_args_name_15;
            PyObject *tmp_tuple_element_31;
            PyObject *tmp_kw_name_15;
            CHECK_OBJECT( tmp_class_creation_8__metaclass );
            tmp_source_name_30 = tmp_class_creation_8__metaclass;
            tmp_called_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain___prepare__ );
            if ( tmp_called_name_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;

                goto try_except_handler_24;
            }
            tmp_tuple_element_31 = const_str_plain_InflectedAttribute;
            tmp_args_name_15 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_31 );
            PyTuple_SET_ITEM( tmp_args_name_15, 0, tmp_tuple_element_31 );
            CHECK_OBJECT( tmp_class_creation_8__bases );
            tmp_tuple_element_31 = tmp_class_creation_8__bases;
            Py_INCREF( tmp_tuple_element_31 );
            PyTuple_SET_ITEM( tmp_args_name_15, 1, tmp_tuple_element_31 );
            CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
            tmp_kw_name_15 = tmp_class_creation_8__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 89;
            tmp_assign_source_75 = CALL_FUNCTION( tmp_called_name_17, tmp_args_name_15, tmp_kw_name_15 );
            Py_DECREF( tmp_called_name_17 );
            Py_DECREF( tmp_args_name_15 );
            if ( tmp_assign_source_75 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;

                goto try_except_handler_24;
            }
            assert( tmp_class_creation_8__prepared == NULL );
            tmp_class_creation_8__prepared = tmp_assign_source_75;
        }
        {
            nuitka_bool tmp_condition_result_48;
            PyObject *tmp_operand_name_8;
            PyObject *tmp_source_name_31;
            CHECK_OBJECT( tmp_class_creation_8__prepared );
            tmp_source_name_31 = tmp_class_creation_8__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_31, const_str_plain___getitem__ );
            tmp_operand_name_8 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_8 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;

                goto try_except_handler_24;
            }
            tmp_condition_result_48 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_48 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_31;
            }
            else
            {
                goto branch_no_31;
            }
            branch_yes_31:;
            {
                PyObject *tmp_raise_type_8;
                PyObject *tmp_raise_value_8;
                PyObject *tmp_left_name_8;
                PyObject *tmp_right_name_8;
                PyObject *tmp_tuple_element_32;
                PyObject *tmp_getattr_target_8;
                PyObject *tmp_getattr_attr_8;
                PyObject *tmp_getattr_default_8;
                PyObject *tmp_source_name_32;
                PyObject *tmp_type_arg_16;
                tmp_raise_type_8 = PyExc_TypeError;
                tmp_left_name_8 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_8__metaclass );
                tmp_getattr_target_8 = tmp_class_creation_8__metaclass;
                tmp_getattr_attr_8 = const_str_plain___name__;
                tmp_getattr_default_8 = const_str_angle_metaclass;
                tmp_tuple_element_32 = BUILTIN_GETATTR( tmp_getattr_target_8, tmp_getattr_attr_8, tmp_getattr_default_8 );
                if ( tmp_tuple_element_32 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 89;

                    goto try_except_handler_24;
                }
                tmp_right_name_8 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_8, 0, tmp_tuple_element_32 );
                CHECK_OBJECT( tmp_class_creation_8__prepared );
                tmp_type_arg_16 = tmp_class_creation_8__prepared;
                tmp_source_name_32 = BUILTIN_TYPE1( tmp_type_arg_16 );
                assert( !(tmp_source_name_32 == NULL) );
                tmp_tuple_element_32 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_32 );
                if ( tmp_tuple_element_32 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_8 );

                    exception_lineno = 89;

                    goto try_except_handler_24;
                }
                PyTuple_SET_ITEM( tmp_right_name_8, 1, tmp_tuple_element_32 );
                tmp_raise_value_8 = BINARY_OPERATION_REMAINDER( tmp_left_name_8, tmp_right_name_8 );
                Py_DECREF( tmp_right_name_8 );
                if ( tmp_raise_value_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 89;

                    goto try_except_handler_24;
                }
                exception_type = tmp_raise_type_8;
                Py_INCREF( tmp_raise_type_8 );
                exception_value = tmp_raise_value_8;
                exception_lineno = 89;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_24;
            }
            branch_no_31:;
        }
        goto branch_end_30;
        branch_no_30:;
        {
            PyObject *tmp_assign_source_76;
            tmp_assign_source_76 = PyDict_New();
            assert( tmp_class_creation_8__prepared == NULL );
            tmp_class_creation_8__prepared = tmp_assign_source_76;
        }
        branch_end_30:;
    }
    {
        PyObject *tmp_assign_source_77;
        {
            PyObject *tmp_set_locals_8;
            CHECK_OBJECT( tmp_class_creation_8__prepared );
            tmp_set_locals_8 = tmp_class_creation_8__prepared;
            locals_yargy$interpretation$attribute_89 = tmp_set_locals_8;
            Py_INCREF( tmp_set_locals_8 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_89, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_26;
        }
        tmp_dictset_value = const_str_plain_InflectedAttribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_89, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;

            goto try_except_handler_26;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_bc871a960d0c66a3c34ec1b1d6280e9e_7, codeobj_bc871a960d0c66a3c34ec1b1d6280e9e, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 = cache_frame_bc871a960d0c66a3c34ec1b1d6280e9e_7;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_attribute_str_plain_grams_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_89, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_2 = "o";
            goto frame_exception_exit_7;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_13___init__(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_89, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_2 = "o";
            goto frame_exception_exit_7;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_14_custom(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_89, const_str_plain_custom, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_2 = "o";
            goto frame_exception_exit_7;
        }
        {
            nuitka_bool tmp_condition_result_49;
            PyObject *tmp_called_name_18;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_called_name_19;
            PyObject *tmp_args_element_name_4;
            tmp_res = MAPPING_HAS_ITEM( locals_yargy$interpretation$attribute_89, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }
            tmp_condition_result_49 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_49 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_18;
            }
            else
            {
                goto condexpr_false_18;
            }
            condexpr_true_18:;
            tmp_called_name_18 = PyObject_GetItem( locals_yargy$interpretation$attribute_89, const_str_plain_property );

            if ( tmp_called_name_18 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 99;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }

            if ( tmp_called_name_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }
            tmp_args_element_name_3 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_15_normalizer(  );



            frame_bc871a960d0c66a3c34ec1b1d6280e9e_7->m_frame.f_lineno = 99;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
            }

            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }
            goto condexpr_end_18;
            condexpr_false_18:;
            tmp_called_name_19 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_4 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_15_normalizer(  );



            frame_bc871a960d0c66a3c34ec1b1d6280e9e_7->m_frame.f_lineno = 99;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
            }

            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }
            condexpr_end_18:;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_89, const_str_plain_normalizer, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_6;

        frame_exception_exit_7:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_bc871a960d0c66a3c34ec1b1d6280e9e_7, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_bc871a960d0c66a3c34ec1b1d6280e9e_7->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_bc871a960d0c66a3c34ec1b1d6280e9e_7, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_bc871a960d0c66a3c34ec1b1d6280e9e_7,
            type_description_2,
            outline_7_var___class__
        );


        // Release cached frame.
        if ( frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 == cache_frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 )
        {
            Py_DECREF( frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 );
        }
        cache_frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 = NULL;

        assertFrameObject( frame_bc871a960d0c66a3c34ec1b1d6280e9e_7 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_6;

        frame_no_exception_6:;
        goto skip_nested_handling_6;
        nested_frame_exit_6:;

        goto try_except_handler_26;
        skip_nested_handling_6:;
        {
            nuitka_bool tmp_condition_result_50;
            PyObject *tmp_compexpr_left_8;
            PyObject *tmp_compexpr_right_8;
            CHECK_OBJECT( tmp_class_creation_8__bases );
            tmp_compexpr_left_8 = tmp_class_creation_8__bases;
            CHECK_OBJECT( tmp_class_creation_8__bases_orig );
            tmp_compexpr_right_8 = tmp_class_creation_8__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;

                goto try_except_handler_26;
            }
            tmp_condition_result_50 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_50 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_32;
            }
            else
            {
                goto branch_no_32;
            }
            branch_yes_32:;
            CHECK_OBJECT( tmp_class_creation_8__bases_orig );
            tmp_dictset_value = tmp_class_creation_8__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_89, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;

                goto try_except_handler_26;
            }
            branch_no_32:;
        }
        {
            PyObject *tmp_assign_source_78;
            PyObject *tmp_called_name_20;
            PyObject *tmp_args_name_16;
            PyObject *tmp_tuple_element_33;
            PyObject *tmp_kw_name_16;
            CHECK_OBJECT( tmp_class_creation_8__metaclass );
            tmp_called_name_20 = tmp_class_creation_8__metaclass;
            tmp_tuple_element_33 = const_str_plain_InflectedAttribute;
            tmp_args_name_16 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_33 );
            PyTuple_SET_ITEM( tmp_args_name_16, 0, tmp_tuple_element_33 );
            CHECK_OBJECT( tmp_class_creation_8__bases );
            tmp_tuple_element_33 = tmp_class_creation_8__bases;
            Py_INCREF( tmp_tuple_element_33 );
            PyTuple_SET_ITEM( tmp_args_name_16, 1, tmp_tuple_element_33 );
            tmp_tuple_element_33 = locals_yargy$interpretation$attribute_89;
            Py_INCREF( tmp_tuple_element_33 );
            PyTuple_SET_ITEM( tmp_args_name_16, 2, tmp_tuple_element_33 );
            CHECK_OBJECT( tmp_class_creation_8__class_decl_dict );
            tmp_kw_name_16 = tmp_class_creation_8__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 89;
            tmp_assign_source_78 = CALL_FUNCTION( tmp_called_name_20, tmp_args_name_16, tmp_kw_name_16 );
            Py_DECREF( tmp_args_name_16 );
            if ( tmp_assign_source_78 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;

                goto try_except_handler_26;
            }
            assert( outline_7_var___class__ == NULL );
            outline_7_var___class__ = tmp_assign_source_78;
        }
        CHECK_OBJECT( outline_7_var___class__ );
        tmp_assign_source_77 = outline_7_var___class__;
        Py_INCREF( tmp_assign_source_77 );
        goto try_return_handler_26;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_26:;
        Py_DECREF( locals_yargy$interpretation$attribute_89 );
        locals_yargy$interpretation$attribute_89 = NULL;
        goto try_return_handler_25;
        // Exception handler code:
        try_except_handler_26:;
        exception_keeper_type_24 = exception_type;
        exception_keeper_value_24 = exception_value;
        exception_keeper_tb_24 = exception_tb;
        exception_keeper_lineno_24 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_89 );
        locals_yargy$interpretation$attribute_89 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_24;
        exception_value = exception_keeper_value_24;
        exception_tb = exception_keeper_tb_24;
        exception_lineno = exception_keeper_lineno_24;

        goto try_except_handler_25;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_25:;
        CHECK_OBJECT( (PyObject *)outline_7_var___class__ );
        Py_DECREF( outline_7_var___class__ );
        outline_7_var___class__ = NULL;

        goto outline_result_8;
        // Exception handler code:
        try_except_handler_25:;
        exception_keeper_type_25 = exception_type;
        exception_keeper_value_25 = exception_value;
        exception_keeper_tb_25 = exception_tb;
        exception_keeper_lineno_25 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_25;
        exception_value = exception_keeper_value_25;
        exception_tb = exception_keeper_tb_25;
        exception_lineno = exception_keeper_lineno_25;

        goto outline_exception_8;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_8:;
        exception_lineno = 89;
        goto try_except_handler_24;
        outline_result_8:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_InflectedAttribute, tmp_assign_source_77 );
    }
    goto try_end_10;
    // Exception handler code:
    try_except_handler_24:;
    exception_keeper_type_26 = exception_type;
    exception_keeper_value_26 = exception_value;
    exception_keeper_tb_26 = exception_tb;
    exception_keeper_lineno_26 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_8__bases_orig );
    tmp_class_creation_8__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_8__bases );
    tmp_class_creation_8__bases = NULL;

    Py_XDECREF( tmp_class_creation_8__class_decl_dict );
    tmp_class_creation_8__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_8__metaclass );
    tmp_class_creation_8__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_8__prepared );
    tmp_class_creation_8__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_26;
    exception_value = exception_keeper_value_26;
    exception_tb = exception_keeper_tb_26;
    exception_lineno = exception_keeper_lineno_26;

    goto frame_exception_exit_1;
    // End of try:
    try_end_10:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_8__bases_orig );
    Py_DECREF( tmp_class_creation_8__bases_orig );
    tmp_class_creation_8__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_8__bases );
    Py_DECREF( tmp_class_creation_8__bases );
    tmp_class_creation_8__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_8__class_decl_dict );
    Py_DECREF( tmp_class_creation_8__class_decl_dict );
    tmp_class_creation_8__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_8__metaclass );
    Py_DECREF( tmp_class_creation_8__metaclass );
    tmp_class_creation_8__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_8__prepared );
    Py_DECREF( tmp_class_creation_8__prepared );
    tmp_class_creation_8__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_79;
        PyObject *tmp_tuple_element_34;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_MorphAttribute );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MorphAttribute );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MorphAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 104;

            goto try_except_handler_27;
        }

        tmp_tuple_element_34 = tmp_mvar_value_11;
        tmp_assign_source_79 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_34 );
        PyTuple_SET_ITEM( tmp_assign_source_79, 0, tmp_tuple_element_34 );
        assert( tmp_class_creation_9__bases_orig == NULL );
        tmp_class_creation_9__bases_orig = tmp_assign_source_79;
    }
    {
        PyObject *tmp_assign_source_80;
        PyObject *tmp_dircall_arg1_9;
        CHECK_OBJECT( tmp_class_creation_9__bases_orig );
        tmp_dircall_arg1_9 = tmp_class_creation_9__bases_orig;
        Py_INCREF( tmp_dircall_arg1_9 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_9};
            tmp_assign_source_80 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_80 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_27;
        }
        assert( tmp_class_creation_9__bases == NULL );
        tmp_class_creation_9__bases = tmp_assign_source_80;
    }
    {
        PyObject *tmp_assign_source_81;
        tmp_assign_source_81 = PyDict_New();
        assert( tmp_class_creation_9__class_decl_dict == NULL );
        tmp_class_creation_9__class_decl_dict = tmp_assign_source_81;
    }
    {
        PyObject *tmp_assign_source_82;
        PyObject *tmp_metaclass_name_9;
        nuitka_bool tmp_condition_result_51;
        PyObject *tmp_key_name_25;
        PyObject *tmp_dict_name_25;
        PyObject *tmp_dict_name_26;
        PyObject *tmp_key_name_26;
        nuitka_bool tmp_condition_result_52;
        int tmp_truth_name_9;
        PyObject *tmp_type_arg_17;
        PyObject *tmp_subscribed_name_9;
        PyObject *tmp_subscript_name_9;
        PyObject *tmp_bases_name_9;
        tmp_key_name_25 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
        tmp_dict_name_25 = tmp_class_creation_9__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_25, tmp_key_name_25 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_27;
        }
        tmp_condition_result_51 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_51 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_19;
        }
        else
        {
            goto condexpr_false_19;
        }
        condexpr_true_19:;
        CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
        tmp_dict_name_26 = tmp_class_creation_9__class_decl_dict;
        tmp_key_name_26 = const_str_plain_metaclass;
        tmp_metaclass_name_9 = DICT_GET_ITEM( tmp_dict_name_26, tmp_key_name_26 );
        if ( tmp_metaclass_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_27;
        }
        goto condexpr_end_19;
        condexpr_false_19:;
        CHECK_OBJECT( tmp_class_creation_9__bases );
        tmp_truth_name_9 = CHECK_IF_TRUE( tmp_class_creation_9__bases );
        if ( tmp_truth_name_9 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_27;
        }
        tmp_condition_result_52 = tmp_truth_name_9 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_52 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_20;
        }
        else
        {
            goto condexpr_false_20;
        }
        condexpr_true_20:;
        CHECK_OBJECT( tmp_class_creation_9__bases );
        tmp_subscribed_name_9 = tmp_class_creation_9__bases;
        tmp_subscript_name_9 = const_int_0;
        tmp_type_arg_17 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_9, tmp_subscript_name_9, 0 );
        if ( tmp_type_arg_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_27;
        }
        tmp_metaclass_name_9 = BUILTIN_TYPE1( tmp_type_arg_17 );
        Py_DECREF( tmp_type_arg_17 );
        if ( tmp_metaclass_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_27;
        }
        goto condexpr_end_20;
        condexpr_false_20:;
        tmp_metaclass_name_9 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_9 );
        condexpr_end_20:;
        condexpr_end_19:;
        CHECK_OBJECT( tmp_class_creation_9__bases );
        tmp_bases_name_9 = tmp_class_creation_9__bases;
        tmp_assign_source_82 = SELECT_METACLASS( tmp_metaclass_name_9, tmp_bases_name_9 );
        Py_DECREF( tmp_metaclass_name_9 );
        if ( tmp_assign_source_82 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_27;
        }
        assert( tmp_class_creation_9__metaclass == NULL );
        tmp_class_creation_9__metaclass = tmp_assign_source_82;
    }
    {
        nuitka_bool tmp_condition_result_53;
        PyObject *tmp_key_name_27;
        PyObject *tmp_dict_name_27;
        tmp_key_name_27 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
        tmp_dict_name_27 = tmp_class_creation_9__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_27, tmp_key_name_27 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_27;
        }
        tmp_condition_result_53 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_53 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_33;
        }
        else
        {
            goto branch_no_33;
        }
        branch_yes_33:;
        CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_9__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_27;
        }
        branch_no_33:;
    }
    {
        nuitka_bool tmp_condition_result_54;
        PyObject *tmp_source_name_33;
        CHECK_OBJECT( tmp_class_creation_9__metaclass );
        tmp_source_name_33 = tmp_class_creation_9__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_33, const_str_plain___prepare__ );
        tmp_condition_result_54 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_54 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_34;
        }
        else
        {
            goto branch_no_34;
        }
        branch_yes_34:;
        {
            PyObject *tmp_assign_source_83;
            PyObject *tmp_called_name_21;
            PyObject *tmp_source_name_34;
            PyObject *tmp_args_name_17;
            PyObject *tmp_tuple_element_35;
            PyObject *tmp_kw_name_17;
            CHECK_OBJECT( tmp_class_creation_9__metaclass );
            tmp_source_name_34 = tmp_class_creation_9__metaclass;
            tmp_called_name_21 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain___prepare__ );
            if ( tmp_called_name_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;

                goto try_except_handler_27;
            }
            tmp_tuple_element_35 = const_str_plain_NormalizedAttribute;
            tmp_args_name_17 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_35 );
            PyTuple_SET_ITEM( tmp_args_name_17, 0, tmp_tuple_element_35 );
            CHECK_OBJECT( tmp_class_creation_9__bases );
            tmp_tuple_element_35 = tmp_class_creation_9__bases;
            Py_INCREF( tmp_tuple_element_35 );
            PyTuple_SET_ITEM( tmp_args_name_17, 1, tmp_tuple_element_35 );
            CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
            tmp_kw_name_17 = tmp_class_creation_9__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 104;
            tmp_assign_source_83 = CALL_FUNCTION( tmp_called_name_21, tmp_args_name_17, tmp_kw_name_17 );
            Py_DECREF( tmp_called_name_21 );
            Py_DECREF( tmp_args_name_17 );
            if ( tmp_assign_source_83 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;

                goto try_except_handler_27;
            }
            assert( tmp_class_creation_9__prepared == NULL );
            tmp_class_creation_9__prepared = tmp_assign_source_83;
        }
        {
            nuitka_bool tmp_condition_result_55;
            PyObject *tmp_operand_name_9;
            PyObject *tmp_source_name_35;
            CHECK_OBJECT( tmp_class_creation_9__prepared );
            tmp_source_name_35 = tmp_class_creation_9__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_35, const_str_plain___getitem__ );
            tmp_operand_name_9 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_9 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;

                goto try_except_handler_27;
            }
            tmp_condition_result_55 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_55 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_35;
            }
            else
            {
                goto branch_no_35;
            }
            branch_yes_35:;
            {
                PyObject *tmp_raise_type_9;
                PyObject *tmp_raise_value_9;
                PyObject *tmp_left_name_9;
                PyObject *tmp_right_name_9;
                PyObject *tmp_tuple_element_36;
                PyObject *tmp_getattr_target_9;
                PyObject *tmp_getattr_attr_9;
                PyObject *tmp_getattr_default_9;
                PyObject *tmp_source_name_36;
                PyObject *tmp_type_arg_18;
                tmp_raise_type_9 = PyExc_TypeError;
                tmp_left_name_9 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_9__metaclass );
                tmp_getattr_target_9 = tmp_class_creation_9__metaclass;
                tmp_getattr_attr_9 = const_str_plain___name__;
                tmp_getattr_default_9 = const_str_angle_metaclass;
                tmp_tuple_element_36 = BUILTIN_GETATTR( tmp_getattr_target_9, tmp_getattr_attr_9, tmp_getattr_default_9 );
                if ( tmp_tuple_element_36 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 104;

                    goto try_except_handler_27;
                }
                tmp_right_name_9 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_9, 0, tmp_tuple_element_36 );
                CHECK_OBJECT( tmp_class_creation_9__prepared );
                tmp_type_arg_18 = tmp_class_creation_9__prepared;
                tmp_source_name_36 = BUILTIN_TYPE1( tmp_type_arg_18 );
                assert( !(tmp_source_name_36 == NULL) );
                tmp_tuple_element_36 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_36 );
                if ( tmp_tuple_element_36 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_9 );

                    exception_lineno = 104;

                    goto try_except_handler_27;
                }
                PyTuple_SET_ITEM( tmp_right_name_9, 1, tmp_tuple_element_36 );
                tmp_raise_value_9 = BINARY_OPERATION_REMAINDER( tmp_left_name_9, tmp_right_name_9 );
                Py_DECREF( tmp_right_name_9 );
                if ( tmp_raise_value_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 104;

                    goto try_except_handler_27;
                }
                exception_type = tmp_raise_type_9;
                Py_INCREF( tmp_raise_type_9 );
                exception_value = tmp_raise_value_9;
                exception_lineno = 104;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_27;
            }
            branch_no_35:;
        }
        goto branch_end_34;
        branch_no_34:;
        {
            PyObject *tmp_assign_source_84;
            tmp_assign_source_84 = PyDict_New();
            assert( tmp_class_creation_9__prepared == NULL );
            tmp_class_creation_9__prepared = tmp_assign_source_84;
        }
        branch_end_34:;
    }
    {
        PyObject *tmp_assign_source_85;
        {
            PyObject *tmp_set_locals_9;
            CHECK_OBJECT( tmp_class_creation_9__prepared );
            tmp_set_locals_9 = tmp_class_creation_9__prepared;
            locals_yargy$interpretation$attribute_104 = tmp_set_locals_9;
            Py_INCREF( tmp_set_locals_9 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_104, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_29;
        }
        tmp_dictset_value = const_str_plain_NormalizedAttribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_104, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto try_except_handler_29;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_57a7f75885b84ff2c6e77d1521f9f436_8, codeobj_57a7f75885b84ff2c6e77d1521f9f436, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_57a7f75885b84ff2c6e77d1521f9f436_8 = cache_frame_57a7f75885b84ff2c6e77d1521f9f436_8;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_57a7f75885b84ff2c6e77d1521f9f436_8 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_57a7f75885b84ff2c6e77d1521f9f436_8 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_attribute_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_104, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_2 = "o";
            goto frame_exception_exit_8;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_16___init__(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_104, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_2 = "o";
            goto frame_exception_exit_8;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_17_custom(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_104, const_str_plain_custom, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_2 = "o";
            goto frame_exception_exit_8;
        }
        {
            nuitka_bool tmp_condition_result_56;
            PyObject *tmp_called_name_22;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_called_name_23;
            PyObject *tmp_args_element_name_6;
            tmp_res = MAPPING_HAS_ITEM( locals_yargy$interpretation$attribute_104, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_2 = "o";
                goto frame_exception_exit_8;
            }
            tmp_condition_result_56 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_56 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_21;
            }
            else
            {
                goto condexpr_false_21;
            }
            condexpr_true_21:;
            tmp_called_name_22 = PyObject_GetItem( locals_yargy$interpretation$attribute_104, const_str_plain_property );

            if ( tmp_called_name_22 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 113;
                type_description_2 = "o";
                goto frame_exception_exit_8;
            }

            if ( tmp_called_name_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_2 = "o";
                goto frame_exception_exit_8;
            }
            tmp_args_element_name_5 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_18_normalizer(  );



            frame_57a7f75885b84ff2c6e77d1521f9f436_8->m_frame.f_lineno = 113;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
            }

            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_2 = "o";
                goto frame_exception_exit_8;
            }
            goto condexpr_end_21;
            condexpr_false_21:;
            tmp_called_name_23 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_6 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_18_normalizer(  );



            frame_57a7f75885b84ff2c6e77d1521f9f436_8->m_frame.f_lineno = 113;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, call_args );
            }

            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_2 = "o";
                goto frame_exception_exit_8;
            }
            condexpr_end_21:;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_104, const_str_plain_normalizer, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_2 = "o";
                goto frame_exception_exit_8;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_57a7f75885b84ff2c6e77d1521f9f436_8 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_7;

        frame_exception_exit_8:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_57a7f75885b84ff2c6e77d1521f9f436_8 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_57a7f75885b84ff2c6e77d1521f9f436_8, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_57a7f75885b84ff2c6e77d1521f9f436_8->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_57a7f75885b84ff2c6e77d1521f9f436_8, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_57a7f75885b84ff2c6e77d1521f9f436_8,
            type_description_2,
            outline_8_var___class__
        );


        // Release cached frame.
        if ( frame_57a7f75885b84ff2c6e77d1521f9f436_8 == cache_frame_57a7f75885b84ff2c6e77d1521f9f436_8 )
        {
            Py_DECREF( frame_57a7f75885b84ff2c6e77d1521f9f436_8 );
        }
        cache_frame_57a7f75885b84ff2c6e77d1521f9f436_8 = NULL;

        assertFrameObject( frame_57a7f75885b84ff2c6e77d1521f9f436_8 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_7;

        frame_no_exception_7:;
        goto skip_nested_handling_7;
        nested_frame_exit_7:;

        goto try_except_handler_29;
        skip_nested_handling_7:;
        {
            nuitka_bool tmp_condition_result_57;
            PyObject *tmp_compexpr_left_9;
            PyObject *tmp_compexpr_right_9;
            CHECK_OBJECT( tmp_class_creation_9__bases );
            tmp_compexpr_left_9 = tmp_class_creation_9__bases;
            CHECK_OBJECT( tmp_class_creation_9__bases_orig );
            tmp_compexpr_right_9 = tmp_class_creation_9__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;

                goto try_except_handler_29;
            }
            tmp_condition_result_57 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_57 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_36;
            }
            else
            {
                goto branch_no_36;
            }
            branch_yes_36:;
            CHECK_OBJECT( tmp_class_creation_9__bases_orig );
            tmp_dictset_value = tmp_class_creation_9__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_104, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;

                goto try_except_handler_29;
            }
            branch_no_36:;
        }
        {
            PyObject *tmp_assign_source_86;
            PyObject *tmp_called_name_24;
            PyObject *tmp_args_name_18;
            PyObject *tmp_tuple_element_37;
            PyObject *tmp_kw_name_18;
            CHECK_OBJECT( tmp_class_creation_9__metaclass );
            tmp_called_name_24 = tmp_class_creation_9__metaclass;
            tmp_tuple_element_37 = const_str_plain_NormalizedAttribute;
            tmp_args_name_18 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_37 );
            PyTuple_SET_ITEM( tmp_args_name_18, 0, tmp_tuple_element_37 );
            CHECK_OBJECT( tmp_class_creation_9__bases );
            tmp_tuple_element_37 = tmp_class_creation_9__bases;
            Py_INCREF( tmp_tuple_element_37 );
            PyTuple_SET_ITEM( tmp_args_name_18, 1, tmp_tuple_element_37 );
            tmp_tuple_element_37 = locals_yargy$interpretation$attribute_104;
            Py_INCREF( tmp_tuple_element_37 );
            PyTuple_SET_ITEM( tmp_args_name_18, 2, tmp_tuple_element_37 );
            CHECK_OBJECT( tmp_class_creation_9__class_decl_dict );
            tmp_kw_name_18 = tmp_class_creation_9__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 104;
            tmp_assign_source_86 = CALL_FUNCTION( tmp_called_name_24, tmp_args_name_18, tmp_kw_name_18 );
            Py_DECREF( tmp_args_name_18 );
            if ( tmp_assign_source_86 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;

                goto try_except_handler_29;
            }
            assert( outline_8_var___class__ == NULL );
            outline_8_var___class__ = tmp_assign_source_86;
        }
        CHECK_OBJECT( outline_8_var___class__ );
        tmp_assign_source_85 = outline_8_var___class__;
        Py_INCREF( tmp_assign_source_85 );
        goto try_return_handler_29;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_29:;
        Py_DECREF( locals_yargy$interpretation$attribute_104 );
        locals_yargy$interpretation$attribute_104 = NULL;
        goto try_return_handler_28;
        // Exception handler code:
        try_except_handler_29:;
        exception_keeper_type_27 = exception_type;
        exception_keeper_value_27 = exception_value;
        exception_keeper_tb_27 = exception_tb;
        exception_keeper_lineno_27 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_104 );
        locals_yargy$interpretation$attribute_104 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_27;
        exception_value = exception_keeper_value_27;
        exception_tb = exception_keeper_tb_27;
        exception_lineno = exception_keeper_lineno_27;

        goto try_except_handler_28;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_28:;
        CHECK_OBJECT( (PyObject *)outline_8_var___class__ );
        Py_DECREF( outline_8_var___class__ );
        outline_8_var___class__ = NULL;

        goto outline_result_9;
        // Exception handler code:
        try_except_handler_28:;
        exception_keeper_type_28 = exception_type;
        exception_keeper_value_28 = exception_value;
        exception_keeper_tb_28 = exception_tb;
        exception_keeper_lineno_28 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_28;
        exception_value = exception_keeper_value_28;
        exception_tb = exception_keeper_tb_28;
        exception_lineno = exception_keeper_lineno_28;

        goto outline_exception_9;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_9:;
        exception_lineno = 104;
        goto try_except_handler_27;
        outline_result_9:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_NormalizedAttribute, tmp_assign_source_85 );
    }
    goto try_end_11;
    // Exception handler code:
    try_except_handler_27:;
    exception_keeper_type_29 = exception_type;
    exception_keeper_value_29 = exception_value;
    exception_keeper_tb_29 = exception_tb;
    exception_keeper_lineno_29 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_9__bases_orig );
    tmp_class_creation_9__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_9__bases );
    tmp_class_creation_9__bases = NULL;

    Py_XDECREF( tmp_class_creation_9__class_decl_dict );
    tmp_class_creation_9__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_9__metaclass );
    tmp_class_creation_9__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_9__prepared );
    tmp_class_creation_9__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_29;
    exception_value = exception_keeper_value_29;
    exception_tb = exception_keeper_tb_29;
    exception_lineno = exception_keeper_lineno_29;

    goto frame_exception_exit_1;
    // End of try:
    try_end_11:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_9__bases_orig );
    Py_DECREF( tmp_class_creation_9__bases_orig );
    tmp_class_creation_9__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_9__bases );
    Py_DECREF( tmp_class_creation_9__bases );
    tmp_class_creation_9__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_9__class_decl_dict );
    Py_DECREF( tmp_class_creation_9__class_decl_dict );
    tmp_class_creation_9__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_9__metaclass );
    Py_DECREF( tmp_class_creation_9__metaclass );
    tmp_class_creation_9__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_9__prepared );
    Py_DECREF( tmp_class_creation_9__prepared );
    tmp_class_creation_9__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_87;
        PyObject *tmp_tuple_element_38;
        PyObject *tmp_mvar_value_12;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_AttributeBase );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AttributeBase );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AttributeBase" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;

            goto try_except_handler_30;
        }

        tmp_tuple_element_38 = tmp_mvar_value_12;
        tmp_assign_source_87 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_38 );
        PyTuple_SET_ITEM( tmp_assign_source_87, 0, tmp_tuple_element_38 );
        assert( tmp_class_creation_10__bases_orig == NULL );
        tmp_class_creation_10__bases_orig = tmp_assign_source_87;
    }
    {
        PyObject *tmp_assign_source_88;
        PyObject *tmp_dircall_arg1_10;
        CHECK_OBJECT( tmp_class_creation_10__bases_orig );
        tmp_dircall_arg1_10 = tmp_class_creation_10__bases_orig;
        Py_INCREF( tmp_dircall_arg1_10 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_10};
            tmp_assign_source_88 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_88 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_30;
        }
        assert( tmp_class_creation_10__bases == NULL );
        tmp_class_creation_10__bases = tmp_assign_source_88;
    }
    {
        PyObject *tmp_assign_source_89;
        tmp_assign_source_89 = PyDict_New();
        assert( tmp_class_creation_10__class_decl_dict == NULL );
        tmp_class_creation_10__class_decl_dict = tmp_assign_source_89;
    }
    {
        PyObject *tmp_assign_source_90;
        PyObject *tmp_metaclass_name_10;
        nuitka_bool tmp_condition_result_58;
        PyObject *tmp_key_name_28;
        PyObject *tmp_dict_name_28;
        PyObject *tmp_dict_name_29;
        PyObject *tmp_key_name_29;
        nuitka_bool tmp_condition_result_59;
        int tmp_truth_name_10;
        PyObject *tmp_type_arg_19;
        PyObject *tmp_subscribed_name_10;
        PyObject *tmp_subscript_name_10;
        PyObject *tmp_bases_name_10;
        tmp_key_name_28 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
        tmp_dict_name_28 = tmp_class_creation_10__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_28, tmp_key_name_28 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_30;
        }
        tmp_condition_result_58 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_58 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_22;
        }
        else
        {
            goto condexpr_false_22;
        }
        condexpr_true_22:;
        CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
        tmp_dict_name_29 = tmp_class_creation_10__class_decl_dict;
        tmp_key_name_29 = const_str_plain_metaclass;
        tmp_metaclass_name_10 = DICT_GET_ITEM( tmp_dict_name_29, tmp_key_name_29 );
        if ( tmp_metaclass_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_30;
        }
        goto condexpr_end_22;
        condexpr_false_22:;
        CHECK_OBJECT( tmp_class_creation_10__bases );
        tmp_truth_name_10 = CHECK_IF_TRUE( tmp_class_creation_10__bases );
        if ( tmp_truth_name_10 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_30;
        }
        tmp_condition_result_59 = tmp_truth_name_10 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_59 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_23;
        }
        else
        {
            goto condexpr_false_23;
        }
        condexpr_true_23:;
        CHECK_OBJECT( tmp_class_creation_10__bases );
        tmp_subscribed_name_10 = tmp_class_creation_10__bases;
        tmp_subscript_name_10 = const_int_0;
        tmp_type_arg_19 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_10, tmp_subscript_name_10, 0 );
        if ( tmp_type_arg_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_30;
        }
        tmp_metaclass_name_10 = BUILTIN_TYPE1( tmp_type_arg_19 );
        Py_DECREF( tmp_type_arg_19 );
        if ( tmp_metaclass_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_30;
        }
        goto condexpr_end_23;
        condexpr_false_23:;
        tmp_metaclass_name_10 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_10 );
        condexpr_end_23:;
        condexpr_end_22:;
        CHECK_OBJECT( tmp_class_creation_10__bases );
        tmp_bases_name_10 = tmp_class_creation_10__bases;
        tmp_assign_source_90 = SELECT_METACLASS( tmp_metaclass_name_10, tmp_bases_name_10 );
        Py_DECREF( tmp_metaclass_name_10 );
        if ( tmp_assign_source_90 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_30;
        }
        assert( tmp_class_creation_10__metaclass == NULL );
        tmp_class_creation_10__metaclass = tmp_assign_source_90;
    }
    {
        nuitka_bool tmp_condition_result_60;
        PyObject *tmp_key_name_30;
        PyObject *tmp_dict_name_30;
        tmp_key_name_30 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
        tmp_dict_name_30 = tmp_class_creation_10__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_30, tmp_key_name_30 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_30;
        }
        tmp_condition_result_60 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_60 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_37;
        }
        else
        {
            goto branch_no_37;
        }
        branch_yes_37:;
        CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_10__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_30;
        }
        branch_no_37:;
    }
    {
        nuitka_bool tmp_condition_result_61;
        PyObject *tmp_source_name_37;
        CHECK_OBJECT( tmp_class_creation_10__metaclass );
        tmp_source_name_37 = tmp_class_creation_10__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_37, const_str_plain___prepare__ );
        tmp_condition_result_61 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_61 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_38;
        }
        else
        {
            goto branch_no_38;
        }
        branch_yes_38:;
        {
            PyObject *tmp_assign_source_91;
            PyObject *tmp_called_name_25;
            PyObject *tmp_source_name_38;
            PyObject *tmp_args_name_19;
            PyObject *tmp_tuple_element_39;
            PyObject *tmp_kw_name_19;
            CHECK_OBJECT( tmp_class_creation_10__metaclass );
            tmp_source_name_38 = tmp_class_creation_10__metaclass;
            tmp_called_name_25 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain___prepare__ );
            if ( tmp_called_name_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 118;

                goto try_except_handler_30;
            }
            tmp_tuple_element_39 = const_str_plain_CustomAttribute;
            tmp_args_name_19 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_39 );
            PyTuple_SET_ITEM( tmp_args_name_19, 0, tmp_tuple_element_39 );
            CHECK_OBJECT( tmp_class_creation_10__bases );
            tmp_tuple_element_39 = tmp_class_creation_10__bases;
            Py_INCREF( tmp_tuple_element_39 );
            PyTuple_SET_ITEM( tmp_args_name_19, 1, tmp_tuple_element_39 );
            CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
            tmp_kw_name_19 = tmp_class_creation_10__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 118;
            tmp_assign_source_91 = CALL_FUNCTION( tmp_called_name_25, tmp_args_name_19, tmp_kw_name_19 );
            Py_DECREF( tmp_called_name_25 );
            Py_DECREF( tmp_args_name_19 );
            if ( tmp_assign_source_91 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 118;

                goto try_except_handler_30;
            }
            assert( tmp_class_creation_10__prepared == NULL );
            tmp_class_creation_10__prepared = tmp_assign_source_91;
        }
        {
            nuitka_bool tmp_condition_result_62;
            PyObject *tmp_operand_name_10;
            PyObject *tmp_source_name_39;
            CHECK_OBJECT( tmp_class_creation_10__prepared );
            tmp_source_name_39 = tmp_class_creation_10__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_39, const_str_plain___getitem__ );
            tmp_operand_name_10 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_10 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 118;

                goto try_except_handler_30;
            }
            tmp_condition_result_62 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_62 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_39;
            }
            else
            {
                goto branch_no_39;
            }
            branch_yes_39:;
            {
                PyObject *tmp_raise_type_10;
                PyObject *tmp_raise_value_10;
                PyObject *tmp_left_name_10;
                PyObject *tmp_right_name_10;
                PyObject *tmp_tuple_element_40;
                PyObject *tmp_getattr_target_10;
                PyObject *tmp_getattr_attr_10;
                PyObject *tmp_getattr_default_10;
                PyObject *tmp_source_name_40;
                PyObject *tmp_type_arg_20;
                tmp_raise_type_10 = PyExc_TypeError;
                tmp_left_name_10 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_10__metaclass );
                tmp_getattr_target_10 = tmp_class_creation_10__metaclass;
                tmp_getattr_attr_10 = const_str_plain___name__;
                tmp_getattr_default_10 = const_str_angle_metaclass;
                tmp_tuple_element_40 = BUILTIN_GETATTR( tmp_getattr_target_10, tmp_getattr_attr_10, tmp_getattr_default_10 );
                if ( tmp_tuple_element_40 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 118;

                    goto try_except_handler_30;
                }
                tmp_right_name_10 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_10, 0, tmp_tuple_element_40 );
                CHECK_OBJECT( tmp_class_creation_10__prepared );
                tmp_type_arg_20 = tmp_class_creation_10__prepared;
                tmp_source_name_40 = BUILTIN_TYPE1( tmp_type_arg_20 );
                assert( !(tmp_source_name_40 == NULL) );
                tmp_tuple_element_40 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_40 );
                if ( tmp_tuple_element_40 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_10 );

                    exception_lineno = 118;

                    goto try_except_handler_30;
                }
                PyTuple_SET_ITEM( tmp_right_name_10, 1, tmp_tuple_element_40 );
                tmp_raise_value_10 = BINARY_OPERATION_REMAINDER( tmp_left_name_10, tmp_right_name_10 );
                Py_DECREF( tmp_right_name_10 );
                if ( tmp_raise_value_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 118;

                    goto try_except_handler_30;
                }
                exception_type = tmp_raise_type_10;
                Py_INCREF( tmp_raise_type_10 );
                exception_value = tmp_raise_value_10;
                exception_lineno = 118;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_30;
            }
            branch_no_39:;
        }
        goto branch_end_38;
        branch_no_38:;
        {
            PyObject *tmp_assign_source_92;
            tmp_assign_source_92 = PyDict_New();
            assert( tmp_class_creation_10__prepared == NULL );
            tmp_class_creation_10__prepared = tmp_assign_source_92;
        }
        branch_end_38:;
    }
    {
        PyObject *tmp_assign_source_93;
        {
            PyObject *tmp_set_locals_10;
            CHECK_OBJECT( tmp_class_creation_10__prepared );
            tmp_set_locals_10 = tmp_class_creation_10__prepared;
            locals_yargy$interpretation$attribute_118 = tmp_set_locals_10;
            Py_INCREF( tmp_set_locals_10 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_118, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_32;
        }
        tmp_dictset_value = const_str_plain_CustomAttribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_118, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto try_except_handler_32;
        }
        {
            nuitka_bool tmp_condition_result_63;
            PyObject *tmp_compexpr_left_10;
            PyObject *tmp_compexpr_right_10;
            CHECK_OBJECT( tmp_class_creation_10__bases );
            tmp_compexpr_left_10 = tmp_class_creation_10__bases;
            CHECK_OBJECT( tmp_class_creation_10__bases_orig );
            tmp_compexpr_right_10 = tmp_class_creation_10__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 118;

                goto try_except_handler_32;
            }
            tmp_condition_result_63 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_63 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_40;
            }
            else
            {
                goto branch_no_40;
            }
            branch_yes_40:;
            CHECK_OBJECT( tmp_class_creation_10__bases_orig );
            tmp_dictset_value = tmp_class_creation_10__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_118, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 118;

                goto try_except_handler_32;
            }
            branch_no_40:;
        }
        {
            PyObject *tmp_assign_source_94;
            PyObject *tmp_called_name_26;
            PyObject *tmp_args_name_20;
            PyObject *tmp_tuple_element_41;
            PyObject *tmp_kw_name_20;
            CHECK_OBJECT( tmp_class_creation_10__metaclass );
            tmp_called_name_26 = tmp_class_creation_10__metaclass;
            tmp_tuple_element_41 = const_str_plain_CustomAttribute;
            tmp_args_name_20 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_41 );
            PyTuple_SET_ITEM( tmp_args_name_20, 0, tmp_tuple_element_41 );
            CHECK_OBJECT( tmp_class_creation_10__bases );
            tmp_tuple_element_41 = tmp_class_creation_10__bases;
            Py_INCREF( tmp_tuple_element_41 );
            PyTuple_SET_ITEM( tmp_args_name_20, 1, tmp_tuple_element_41 );
            tmp_tuple_element_41 = locals_yargy$interpretation$attribute_118;
            Py_INCREF( tmp_tuple_element_41 );
            PyTuple_SET_ITEM( tmp_args_name_20, 2, tmp_tuple_element_41 );
            CHECK_OBJECT( tmp_class_creation_10__class_decl_dict );
            tmp_kw_name_20 = tmp_class_creation_10__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 118;
            tmp_assign_source_94 = CALL_FUNCTION( tmp_called_name_26, tmp_args_name_20, tmp_kw_name_20 );
            Py_DECREF( tmp_args_name_20 );
            if ( tmp_assign_source_94 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 118;

                goto try_except_handler_32;
            }
            assert( outline_9_var___class__ == NULL );
            outline_9_var___class__ = tmp_assign_source_94;
        }
        CHECK_OBJECT( outline_9_var___class__ );
        tmp_assign_source_93 = outline_9_var___class__;
        Py_INCREF( tmp_assign_source_93 );
        goto try_return_handler_32;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_32:;
        Py_DECREF( locals_yargy$interpretation$attribute_118 );
        locals_yargy$interpretation$attribute_118 = NULL;
        goto try_return_handler_31;
        // Exception handler code:
        try_except_handler_32:;
        exception_keeper_type_30 = exception_type;
        exception_keeper_value_30 = exception_value;
        exception_keeper_tb_30 = exception_tb;
        exception_keeper_lineno_30 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_118 );
        locals_yargy$interpretation$attribute_118 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_30;
        exception_value = exception_keeper_value_30;
        exception_tb = exception_keeper_tb_30;
        exception_lineno = exception_keeper_lineno_30;

        goto try_except_handler_31;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_31:;
        CHECK_OBJECT( (PyObject *)outline_9_var___class__ );
        Py_DECREF( outline_9_var___class__ );
        outline_9_var___class__ = NULL;

        goto outline_result_10;
        // Exception handler code:
        try_except_handler_31:;
        exception_keeper_type_31 = exception_type;
        exception_keeper_value_31 = exception_value;
        exception_keeper_tb_31 = exception_tb;
        exception_keeper_lineno_31 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_31;
        exception_value = exception_keeper_value_31;
        exception_tb = exception_keeper_tb_31;
        exception_lineno = exception_keeper_lineno_31;

        goto outline_exception_10;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_10:;
        exception_lineno = 118;
        goto try_except_handler_30;
        outline_result_10:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_CustomAttribute, tmp_assign_source_93 );
    }
    goto try_end_12;
    // Exception handler code:
    try_except_handler_30:;
    exception_keeper_type_32 = exception_type;
    exception_keeper_value_32 = exception_value;
    exception_keeper_tb_32 = exception_tb;
    exception_keeper_lineno_32 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_10__bases_orig );
    tmp_class_creation_10__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_10__bases );
    tmp_class_creation_10__bases = NULL;

    Py_XDECREF( tmp_class_creation_10__class_decl_dict );
    tmp_class_creation_10__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_10__metaclass );
    tmp_class_creation_10__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_10__prepared );
    tmp_class_creation_10__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_32;
    exception_value = exception_keeper_value_32;
    exception_tb = exception_keeper_tb_32;
    exception_lineno = exception_keeper_lineno_32;

    goto frame_exception_exit_1;
    // End of try:
    try_end_12:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_10__bases_orig );
    Py_DECREF( tmp_class_creation_10__bases_orig );
    tmp_class_creation_10__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_10__bases );
    Py_DECREF( tmp_class_creation_10__bases );
    tmp_class_creation_10__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_10__class_decl_dict );
    Py_DECREF( tmp_class_creation_10__class_decl_dict );
    tmp_class_creation_10__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_10__metaclass );
    Py_DECREF( tmp_class_creation_10__metaclass );
    tmp_class_creation_10__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_10__prepared );
    Py_DECREF( tmp_class_creation_10__prepared );
    tmp_class_creation_10__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_95;
        PyObject *tmp_tuple_element_42;
        PyObject *tmp_mvar_value_13;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_CustomAttribute );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CustomAttribute );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CustomAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;

            goto try_except_handler_33;
        }

        tmp_tuple_element_42 = tmp_mvar_value_13;
        tmp_assign_source_95 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_42 );
        PyTuple_SET_ITEM( tmp_assign_source_95, 0, tmp_tuple_element_42 );
        assert( tmp_class_creation_11__bases_orig == NULL );
        tmp_class_creation_11__bases_orig = tmp_assign_source_95;
    }
    {
        PyObject *tmp_assign_source_96;
        PyObject *tmp_dircall_arg1_11;
        CHECK_OBJECT( tmp_class_creation_11__bases_orig );
        tmp_dircall_arg1_11 = tmp_class_creation_11__bases_orig;
        Py_INCREF( tmp_dircall_arg1_11 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_11};
            tmp_assign_source_96 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_96 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_33;
        }
        assert( tmp_class_creation_11__bases == NULL );
        tmp_class_creation_11__bases = tmp_assign_source_96;
    }
    {
        PyObject *tmp_assign_source_97;
        tmp_assign_source_97 = PyDict_New();
        assert( tmp_class_creation_11__class_decl_dict == NULL );
        tmp_class_creation_11__class_decl_dict = tmp_assign_source_97;
    }
    {
        PyObject *tmp_assign_source_98;
        PyObject *tmp_metaclass_name_11;
        nuitka_bool tmp_condition_result_64;
        PyObject *tmp_key_name_31;
        PyObject *tmp_dict_name_31;
        PyObject *tmp_dict_name_32;
        PyObject *tmp_key_name_32;
        nuitka_bool tmp_condition_result_65;
        int tmp_truth_name_11;
        PyObject *tmp_type_arg_21;
        PyObject *tmp_subscribed_name_11;
        PyObject *tmp_subscript_name_11;
        PyObject *tmp_bases_name_11;
        tmp_key_name_31 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
        tmp_dict_name_31 = tmp_class_creation_11__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_31, tmp_key_name_31 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_33;
        }
        tmp_condition_result_64 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_64 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_24;
        }
        else
        {
            goto condexpr_false_24;
        }
        condexpr_true_24:;
        CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
        tmp_dict_name_32 = tmp_class_creation_11__class_decl_dict;
        tmp_key_name_32 = const_str_plain_metaclass;
        tmp_metaclass_name_11 = DICT_GET_ITEM( tmp_dict_name_32, tmp_key_name_32 );
        if ( tmp_metaclass_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_33;
        }
        goto condexpr_end_24;
        condexpr_false_24:;
        CHECK_OBJECT( tmp_class_creation_11__bases );
        tmp_truth_name_11 = CHECK_IF_TRUE( tmp_class_creation_11__bases );
        if ( tmp_truth_name_11 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_33;
        }
        tmp_condition_result_65 = tmp_truth_name_11 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_65 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_25;
        }
        else
        {
            goto condexpr_false_25;
        }
        condexpr_true_25:;
        CHECK_OBJECT( tmp_class_creation_11__bases );
        tmp_subscribed_name_11 = tmp_class_creation_11__bases;
        tmp_subscript_name_11 = const_int_0;
        tmp_type_arg_21 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_11, tmp_subscript_name_11, 0 );
        if ( tmp_type_arg_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_33;
        }
        tmp_metaclass_name_11 = BUILTIN_TYPE1( tmp_type_arg_21 );
        Py_DECREF( tmp_type_arg_21 );
        if ( tmp_metaclass_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_33;
        }
        goto condexpr_end_25;
        condexpr_false_25:;
        tmp_metaclass_name_11 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_11 );
        condexpr_end_25:;
        condexpr_end_24:;
        CHECK_OBJECT( tmp_class_creation_11__bases );
        tmp_bases_name_11 = tmp_class_creation_11__bases;
        tmp_assign_source_98 = SELECT_METACLASS( tmp_metaclass_name_11, tmp_bases_name_11 );
        Py_DECREF( tmp_metaclass_name_11 );
        if ( tmp_assign_source_98 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_33;
        }
        assert( tmp_class_creation_11__metaclass == NULL );
        tmp_class_creation_11__metaclass = tmp_assign_source_98;
    }
    {
        nuitka_bool tmp_condition_result_66;
        PyObject *tmp_key_name_33;
        PyObject *tmp_dict_name_33;
        tmp_key_name_33 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
        tmp_dict_name_33 = tmp_class_creation_11__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_33, tmp_key_name_33 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_33;
        }
        tmp_condition_result_66 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_66 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_41;
        }
        else
        {
            goto branch_no_41;
        }
        branch_yes_41:;
        CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_11__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_33;
        }
        branch_no_41:;
    }
    {
        nuitka_bool tmp_condition_result_67;
        PyObject *tmp_source_name_41;
        CHECK_OBJECT( tmp_class_creation_11__metaclass );
        tmp_source_name_41 = tmp_class_creation_11__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_41, const_str_plain___prepare__ );
        tmp_condition_result_67 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_67 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_42;
        }
        else
        {
            goto branch_no_42;
        }
        branch_yes_42:;
        {
            PyObject *tmp_assign_source_99;
            PyObject *tmp_called_name_27;
            PyObject *tmp_source_name_42;
            PyObject *tmp_args_name_21;
            PyObject *tmp_tuple_element_43;
            PyObject *tmp_kw_name_21;
            CHECK_OBJECT( tmp_class_creation_11__metaclass );
            tmp_source_name_42 = tmp_class_creation_11__metaclass;
            tmp_called_name_27 = LOOKUP_ATTRIBUTE( tmp_source_name_42, const_str_plain___prepare__ );
            if ( tmp_called_name_27 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 122;

                goto try_except_handler_33;
            }
            tmp_tuple_element_43 = const_str_plain_ConstAttribute;
            tmp_args_name_21 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_43 );
            PyTuple_SET_ITEM( tmp_args_name_21, 0, tmp_tuple_element_43 );
            CHECK_OBJECT( tmp_class_creation_11__bases );
            tmp_tuple_element_43 = tmp_class_creation_11__bases;
            Py_INCREF( tmp_tuple_element_43 );
            PyTuple_SET_ITEM( tmp_args_name_21, 1, tmp_tuple_element_43 );
            CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
            tmp_kw_name_21 = tmp_class_creation_11__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 122;
            tmp_assign_source_99 = CALL_FUNCTION( tmp_called_name_27, tmp_args_name_21, tmp_kw_name_21 );
            Py_DECREF( tmp_called_name_27 );
            Py_DECREF( tmp_args_name_21 );
            if ( tmp_assign_source_99 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 122;

                goto try_except_handler_33;
            }
            assert( tmp_class_creation_11__prepared == NULL );
            tmp_class_creation_11__prepared = tmp_assign_source_99;
        }
        {
            nuitka_bool tmp_condition_result_68;
            PyObject *tmp_operand_name_11;
            PyObject *tmp_source_name_43;
            CHECK_OBJECT( tmp_class_creation_11__prepared );
            tmp_source_name_43 = tmp_class_creation_11__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_43, const_str_plain___getitem__ );
            tmp_operand_name_11 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_11 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 122;

                goto try_except_handler_33;
            }
            tmp_condition_result_68 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_68 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_43;
            }
            else
            {
                goto branch_no_43;
            }
            branch_yes_43:;
            {
                PyObject *tmp_raise_type_11;
                PyObject *tmp_raise_value_11;
                PyObject *tmp_left_name_11;
                PyObject *tmp_right_name_11;
                PyObject *tmp_tuple_element_44;
                PyObject *tmp_getattr_target_11;
                PyObject *tmp_getattr_attr_11;
                PyObject *tmp_getattr_default_11;
                PyObject *tmp_source_name_44;
                PyObject *tmp_type_arg_22;
                tmp_raise_type_11 = PyExc_TypeError;
                tmp_left_name_11 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_11__metaclass );
                tmp_getattr_target_11 = tmp_class_creation_11__metaclass;
                tmp_getattr_attr_11 = const_str_plain___name__;
                tmp_getattr_default_11 = const_str_angle_metaclass;
                tmp_tuple_element_44 = BUILTIN_GETATTR( tmp_getattr_target_11, tmp_getattr_attr_11, tmp_getattr_default_11 );
                if ( tmp_tuple_element_44 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 122;

                    goto try_except_handler_33;
                }
                tmp_right_name_11 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_11, 0, tmp_tuple_element_44 );
                CHECK_OBJECT( tmp_class_creation_11__prepared );
                tmp_type_arg_22 = tmp_class_creation_11__prepared;
                tmp_source_name_44 = BUILTIN_TYPE1( tmp_type_arg_22 );
                assert( !(tmp_source_name_44 == NULL) );
                tmp_tuple_element_44 = LOOKUP_ATTRIBUTE( tmp_source_name_44, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_44 );
                if ( tmp_tuple_element_44 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_11 );

                    exception_lineno = 122;

                    goto try_except_handler_33;
                }
                PyTuple_SET_ITEM( tmp_right_name_11, 1, tmp_tuple_element_44 );
                tmp_raise_value_11 = BINARY_OPERATION_REMAINDER( tmp_left_name_11, tmp_right_name_11 );
                Py_DECREF( tmp_right_name_11 );
                if ( tmp_raise_value_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 122;

                    goto try_except_handler_33;
                }
                exception_type = tmp_raise_type_11;
                Py_INCREF( tmp_raise_type_11 );
                exception_value = tmp_raise_value_11;
                exception_lineno = 122;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_33;
            }
            branch_no_43:;
        }
        goto branch_end_42;
        branch_no_42:;
        {
            PyObject *tmp_assign_source_100;
            tmp_assign_source_100 = PyDict_New();
            assert( tmp_class_creation_11__prepared == NULL );
            tmp_class_creation_11__prepared = tmp_assign_source_100;
        }
        branch_end_42:;
    }
    {
        PyObject *tmp_assign_source_101;
        {
            PyObject *tmp_set_locals_11;
            CHECK_OBJECT( tmp_class_creation_11__prepared );
            tmp_set_locals_11 = tmp_class_creation_11__prepared;
            locals_yargy$interpretation$attribute_122 = tmp_set_locals_11;
            Py_INCREF( tmp_set_locals_11 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_122, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_35;
        }
        tmp_dictset_value = const_str_plain_ConstAttribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_122, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;

            goto try_except_handler_35;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_e3c722f334a426b726df72ea4f4a5f29_9, codeobj_e3c722f334a426b726df72ea4f4a5f29, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_e3c722f334a426b726df72ea4f4a5f29_9 = cache_frame_e3c722f334a426b726df72ea4f4a5f29_9;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_e3c722f334a426b726df72ea4f4a5f29_9 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_e3c722f334a426b726df72ea4f4a5f29_9 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_attribute_str_plain_value_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_122, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_2 = "o";
            goto frame_exception_exit_9;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_19___init__(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_122, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_2 = "o";
            goto frame_exception_exit_9;
        }
        {
            nuitka_bool tmp_condition_result_69;
            PyObject *tmp_called_name_28;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_called_name_29;
            PyObject *tmp_args_element_name_8;
            tmp_res = MAPPING_HAS_ITEM( locals_yargy$interpretation$attribute_122, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_2 = "o";
                goto frame_exception_exit_9;
            }
            tmp_condition_result_69 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_69 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_26;
            }
            else
            {
                goto condexpr_false_26;
            }
            condexpr_true_26:;
            tmp_called_name_28 = PyObject_GetItem( locals_yargy$interpretation$attribute_122, const_str_plain_property );

            if ( tmp_called_name_28 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 129;
                type_description_2 = "o";
                goto frame_exception_exit_9;
            }

            if ( tmp_called_name_28 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_2 = "o";
                goto frame_exception_exit_9;
            }
            tmp_args_element_name_7 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_20_normalizer(  );



            frame_e3c722f334a426b726df72ea4f4a5f29_9->m_frame.f_lineno = 129;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, call_args );
            }

            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_2 = "o";
                goto frame_exception_exit_9;
            }
            goto condexpr_end_26;
            condexpr_false_26:;
            tmp_called_name_29 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_8 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_20_normalizer(  );



            frame_e3c722f334a426b726df72ea4f4a5f29_9->m_frame.f_lineno = 129;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_29, call_args );
            }

            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_2 = "o";
                goto frame_exception_exit_9;
            }
            condexpr_end_26:;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_122, const_str_plain_normalizer, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_2 = "o";
                goto frame_exception_exit_9;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e3c722f334a426b726df72ea4f4a5f29_9 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_8;

        frame_exception_exit_9:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_e3c722f334a426b726df72ea4f4a5f29_9 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_e3c722f334a426b726df72ea4f4a5f29_9, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_e3c722f334a426b726df72ea4f4a5f29_9->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_e3c722f334a426b726df72ea4f4a5f29_9, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_e3c722f334a426b726df72ea4f4a5f29_9,
            type_description_2,
            outline_10_var___class__
        );


        // Release cached frame.
        if ( frame_e3c722f334a426b726df72ea4f4a5f29_9 == cache_frame_e3c722f334a426b726df72ea4f4a5f29_9 )
        {
            Py_DECREF( frame_e3c722f334a426b726df72ea4f4a5f29_9 );
        }
        cache_frame_e3c722f334a426b726df72ea4f4a5f29_9 = NULL;

        assertFrameObject( frame_e3c722f334a426b726df72ea4f4a5f29_9 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_8;

        frame_no_exception_8:;
        goto skip_nested_handling_8;
        nested_frame_exit_8:;

        goto try_except_handler_35;
        skip_nested_handling_8:;
        {
            nuitka_bool tmp_condition_result_70;
            PyObject *tmp_compexpr_left_11;
            PyObject *tmp_compexpr_right_11;
            CHECK_OBJECT( tmp_class_creation_11__bases );
            tmp_compexpr_left_11 = tmp_class_creation_11__bases;
            CHECK_OBJECT( tmp_class_creation_11__bases_orig );
            tmp_compexpr_right_11 = tmp_class_creation_11__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 122;

                goto try_except_handler_35;
            }
            tmp_condition_result_70 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_70 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_44;
            }
            else
            {
                goto branch_no_44;
            }
            branch_yes_44:;
            CHECK_OBJECT( tmp_class_creation_11__bases_orig );
            tmp_dictset_value = tmp_class_creation_11__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_122, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 122;

                goto try_except_handler_35;
            }
            branch_no_44:;
        }
        {
            PyObject *tmp_assign_source_102;
            PyObject *tmp_called_name_30;
            PyObject *tmp_args_name_22;
            PyObject *tmp_tuple_element_45;
            PyObject *tmp_kw_name_22;
            CHECK_OBJECT( tmp_class_creation_11__metaclass );
            tmp_called_name_30 = tmp_class_creation_11__metaclass;
            tmp_tuple_element_45 = const_str_plain_ConstAttribute;
            tmp_args_name_22 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_45 );
            PyTuple_SET_ITEM( tmp_args_name_22, 0, tmp_tuple_element_45 );
            CHECK_OBJECT( tmp_class_creation_11__bases );
            tmp_tuple_element_45 = tmp_class_creation_11__bases;
            Py_INCREF( tmp_tuple_element_45 );
            PyTuple_SET_ITEM( tmp_args_name_22, 1, tmp_tuple_element_45 );
            tmp_tuple_element_45 = locals_yargy$interpretation$attribute_122;
            Py_INCREF( tmp_tuple_element_45 );
            PyTuple_SET_ITEM( tmp_args_name_22, 2, tmp_tuple_element_45 );
            CHECK_OBJECT( tmp_class_creation_11__class_decl_dict );
            tmp_kw_name_22 = tmp_class_creation_11__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 122;
            tmp_assign_source_102 = CALL_FUNCTION( tmp_called_name_30, tmp_args_name_22, tmp_kw_name_22 );
            Py_DECREF( tmp_args_name_22 );
            if ( tmp_assign_source_102 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 122;

                goto try_except_handler_35;
            }
            assert( outline_10_var___class__ == NULL );
            outline_10_var___class__ = tmp_assign_source_102;
        }
        CHECK_OBJECT( outline_10_var___class__ );
        tmp_assign_source_101 = outline_10_var___class__;
        Py_INCREF( tmp_assign_source_101 );
        goto try_return_handler_35;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_35:;
        Py_DECREF( locals_yargy$interpretation$attribute_122 );
        locals_yargy$interpretation$attribute_122 = NULL;
        goto try_return_handler_34;
        // Exception handler code:
        try_except_handler_35:;
        exception_keeper_type_33 = exception_type;
        exception_keeper_value_33 = exception_value;
        exception_keeper_tb_33 = exception_tb;
        exception_keeper_lineno_33 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_122 );
        locals_yargy$interpretation$attribute_122 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_33;
        exception_value = exception_keeper_value_33;
        exception_tb = exception_keeper_tb_33;
        exception_lineno = exception_keeper_lineno_33;

        goto try_except_handler_34;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_34:;
        CHECK_OBJECT( (PyObject *)outline_10_var___class__ );
        Py_DECREF( outline_10_var___class__ );
        outline_10_var___class__ = NULL;

        goto outline_result_11;
        // Exception handler code:
        try_except_handler_34:;
        exception_keeper_type_34 = exception_type;
        exception_keeper_value_34 = exception_value;
        exception_keeper_tb_34 = exception_tb;
        exception_keeper_lineno_34 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_34;
        exception_value = exception_keeper_value_34;
        exception_tb = exception_keeper_tb_34;
        exception_lineno = exception_keeper_lineno_34;

        goto outline_exception_11;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_11:;
        exception_lineno = 122;
        goto try_except_handler_33;
        outline_result_11:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_ConstAttribute, tmp_assign_source_101 );
    }
    goto try_end_13;
    // Exception handler code:
    try_except_handler_33:;
    exception_keeper_type_35 = exception_type;
    exception_keeper_value_35 = exception_value;
    exception_keeper_tb_35 = exception_tb;
    exception_keeper_lineno_35 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_11__bases_orig );
    tmp_class_creation_11__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_11__bases );
    tmp_class_creation_11__bases = NULL;

    Py_XDECREF( tmp_class_creation_11__class_decl_dict );
    tmp_class_creation_11__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_11__metaclass );
    tmp_class_creation_11__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_11__prepared );
    tmp_class_creation_11__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_35;
    exception_value = exception_keeper_value_35;
    exception_tb = exception_keeper_tb_35;
    exception_lineno = exception_keeper_lineno_35;

    goto frame_exception_exit_1;
    // End of try:
    try_end_13:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_11__bases_orig );
    Py_DECREF( tmp_class_creation_11__bases_orig );
    tmp_class_creation_11__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_11__bases );
    Py_DECREF( tmp_class_creation_11__bases );
    tmp_class_creation_11__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_11__class_decl_dict );
    Py_DECREF( tmp_class_creation_11__class_decl_dict );
    tmp_class_creation_11__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_11__metaclass );
    Py_DECREF( tmp_class_creation_11__metaclass );
    tmp_class_creation_11__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_11__prepared );
    Py_DECREF( tmp_class_creation_11__prepared );
    tmp_class_creation_11__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_103;
        PyObject *tmp_tuple_element_46;
        PyObject *tmp_mvar_value_14;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_CustomAttribute );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CustomAttribute );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CustomAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;

            goto try_except_handler_36;
        }

        tmp_tuple_element_46 = tmp_mvar_value_14;
        tmp_assign_source_103 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_46 );
        PyTuple_SET_ITEM( tmp_assign_source_103, 0, tmp_tuple_element_46 );
        assert( tmp_class_creation_12__bases_orig == NULL );
        tmp_class_creation_12__bases_orig = tmp_assign_source_103;
    }
    {
        PyObject *tmp_assign_source_104;
        PyObject *tmp_dircall_arg1_12;
        CHECK_OBJECT( tmp_class_creation_12__bases_orig );
        tmp_dircall_arg1_12 = tmp_class_creation_12__bases_orig;
        Py_INCREF( tmp_dircall_arg1_12 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_12};
            tmp_assign_source_104 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_104 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_36;
        }
        assert( tmp_class_creation_12__bases == NULL );
        tmp_class_creation_12__bases = tmp_assign_source_104;
    }
    {
        PyObject *tmp_assign_source_105;
        tmp_assign_source_105 = PyDict_New();
        assert( tmp_class_creation_12__class_decl_dict == NULL );
        tmp_class_creation_12__class_decl_dict = tmp_assign_source_105;
    }
    {
        PyObject *tmp_assign_source_106;
        PyObject *tmp_metaclass_name_12;
        nuitka_bool tmp_condition_result_71;
        PyObject *tmp_key_name_34;
        PyObject *tmp_dict_name_34;
        PyObject *tmp_dict_name_35;
        PyObject *tmp_key_name_35;
        nuitka_bool tmp_condition_result_72;
        int tmp_truth_name_12;
        PyObject *tmp_type_arg_23;
        PyObject *tmp_subscribed_name_12;
        PyObject *tmp_subscript_name_12;
        PyObject *tmp_bases_name_12;
        tmp_key_name_34 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
        tmp_dict_name_34 = tmp_class_creation_12__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_34, tmp_key_name_34 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_36;
        }
        tmp_condition_result_71 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_71 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_27;
        }
        else
        {
            goto condexpr_false_27;
        }
        condexpr_true_27:;
        CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
        tmp_dict_name_35 = tmp_class_creation_12__class_decl_dict;
        tmp_key_name_35 = const_str_plain_metaclass;
        tmp_metaclass_name_12 = DICT_GET_ITEM( tmp_dict_name_35, tmp_key_name_35 );
        if ( tmp_metaclass_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_36;
        }
        goto condexpr_end_27;
        condexpr_false_27:;
        CHECK_OBJECT( tmp_class_creation_12__bases );
        tmp_truth_name_12 = CHECK_IF_TRUE( tmp_class_creation_12__bases );
        if ( tmp_truth_name_12 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_36;
        }
        tmp_condition_result_72 = tmp_truth_name_12 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_72 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_28;
        }
        else
        {
            goto condexpr_false_28;
        }
        condexpr_true_28:;
        CHECK_OBJECT( tmp_class_creation_12__bases );
        tmp_subscribed_name_12 = tmp_class_creation_12__bases;
        tmp_subscript_name_12 = const_int_0;
        tmp_type_arg_23 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_12, tmp_subscript_name_12, 0 );
        if ( tmp_type_arg_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_36;
        }
        tmp_metaclass_name_12 = BUILTIN_TYPE1( tmp_type_arg_23 );
        Py_DECREF( tmp_type_arg_23 );
        if ( tmp_metaclass_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_36;
        }
        goto condexpr_end_28;
        condexpr_false_28:;
        tmp_metaclass_name_12 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_12 );
        condexpr_end_28:;
        condexpr_end_27:;
        CHECK_OBJECT( tmp_class_creation_12__bases );
        tmp_bases_name_12 = tmp_class_creation_12__bases;
        tmp_assign_source_106 = SELECT_METACLASS( tmp_metaclass_name_12, tmp_bases_name_12 );
        Py_DECREF( tmp_metaclass_name_12 );
        if ( tmp_assign_source_106 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_36;
        }
        assert( tmp_class_creation_12__metaclass == NULL );
        tmp_class_creation_12__metaclass = tmp_assign_source_106;
    }
    {
        nuitka_bool tmp_condition_result_73;
        PyObject *tmp_key_name_36;
        PyObject *tmp_dict_name_36;
        tmp_key_name_36 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
        tmp_dict_name_36 = tmp_class_creation_12__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_36, tmp_key_name_36 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_36;
        }
        tmp_condition_result_73 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_73 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_45;
        }
        else
        {
            goto branch_no_45;
        }
        branch_yes_45:;
        CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_12__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_36;
        }
        branch_no_45:;
    }
    {
        nuitka_bool tmp_condition_result_74;
        PyObject *tmp_source_name_45;
        CHECK_OBJECT( tmp_class_creation_12__metaclass );
        tmp_source_name_45 = tmp_class_creation_12__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_45, const_str_plain___prepare__ );
        tmp_condition_result_74 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_74 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_46;
        }
        else
        {
            goto branch_no_46;
        }
        branch_yes_46:;
        {
            PyObject *tmp_assign_source_107;
            PyObject *tmp_called_name_31;
            PyObject *tmp_source_name_46;
            PyObject *tmp_args_name_23;
            PyObject *tmp_tuple_element_47;
            PyObject *tmp_kw_name_23;
            CHECK_OBJECT( tmp_class_creation_12__metaclass );
            tmp_source_name_46 = tmp_class_creation_12__metaclass;
            tmp_called_name_31 = LOOKUP_ATTRIBUTE( tmp_source_name_46, const_str_plain___prepare__ );
            if ( tmp_called_name_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;

                goto try_except_handler_36;
            }
            tmp_tuple_element_47 = const_str_plain_FunctionAttribute;
            tmp_args_name_23 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_47 );
            PyTuple_SET_ITEM( tmp_args_name_23, 0, tmp_tuple_element_47 );
            CHECK_OBJECT( tmp_class_creation_12__bases );
            tmp_tuple_element_47 = tmp_class_creation_12__bases;
            Py_INCREF( tmp_tuple_element_47 );
            PyTuple_SET_ITEM( tmp_args_name_23, 1, tmp_tuple_element_47 );
            CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
            tmp_kw_name_23 = tmp_class_creation_12__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 134;
            tmp_assign_source_107 = CALL_FUNCTION( tmp_called_name_31, tmp_args_name_23, tmp_kw_name_23 );
            Py_DECREF( tmp_called_name_31 );
            Py_DECREF( tmp_args_name_23 );
            if ( tmp_assign_source_107 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;

                goto try_except_handler_36;
            }
            assert( tmp_class_creation_12__prepared == NULL );
            tmp_class_creation_12__prepared = tmp_assign_source_107;
        }
        {
            nuitka_bool tmp_condition_result_75;
            PyObject *tmp_operand_name_12;
            PyObject *tmp_source_name_47;
            CHECK_OBJECT( tmp_class_creation_12__prepared );
            tmp_source_name_47 = tmp_class_creation_12__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_47, const_str_plain___getitem__ );
            tmp_operand_name_12 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_12 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;

                goto try_except_handler_36;
            }
            tmp_condition_result_75 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_75 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_47;
            }
            else
            {
                goto branch_no_47;
            }
            branch_yes_47:;
            {
                PyObject *tmp_raise_type_12;
                PyObject *tmp_raise_value_12;
                PyObject *tmp_left_name_12;
                PyObject *tmp_right_name_12;
                PyObject *tmp_tuple_element_48;
                PyObject *tmp_getattr_target_12;
                PyObject *tmp_getattr_attr_12;
                PyObject *tmp_getattr_default_12;
                PyObject *tmp_source_name_48;
                PyObject *tmp_type_arg_24;
                tmp_raise_type_12 = PyExc_TypeError;
                tmp_left_name_12 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_12__metaclass );
                tmp_getattr_target_12 = tmp_class_creation_12__metaclass;
                tmp_getattr_attr_12 = const_str_plain___name__;
                tmp_getattr_default_12 = const_str_angle_metaclass;
                tmp_tuple_element_48 = BUILTIN_GETATTR( tmp_getattr_target_12, tmp_getattr_attr_12, tmp_getattr_default_12 );
                if ( tmp_tuple_element_48 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 134;

                    goto try_except_handler_36;
                }
                tmp_right_name_12 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_12, 0, tmp_tuple_element_48 );
                CHECK_OBJECT( tmp_class_creation_12__prepared );
                tmp_type_arg_24 = tmp_class_creation_12__prepared;
                tmp_source_name_48 = BUILTIN_TYPE1( tmp_type_arg_24 );
                assert( !(tmp_source_name_48 == NULL) );
                tmp_tuple_element_48 = LOOKUP_ATTRIBUTE( tmp_source_name_48, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_48 );
                if ( tmp_tuple_element_48 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_12 );

                    exception_lineno = 134;

                    goto try_except_handler_36;
                }
                PyTuple_SET_ITEM( tmp_right_name_12, 1, tmp_tuple_element_48 );
                tmp_raise_value_12 = BINARY_OPERATION_REMAINDER( tmp_left_name_12, tmp_right_name_12 );
                Py_DECREF( tmp_right_name_12 );
                if ( tmp_raise_value_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 134;

                    goto try_except_handler_36;
                }
                exception_type = tmp_raise_type_12;
                Py_INCREF( tmp_raise_type_12 );
                exception_value = tmp_raise_value_12;
                exception_lineno = 134;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_36;
            }
            branch_no_47:;
        }
        goto branch_end_46;
        branch_no_46:;
        {
            PyObject *tmp_assign_source_108;
            tmp_assign_source_108 = PyDict_New();
            assert( tmp_class_creation_12__prepared == NULL );
            tmp_class_creation_12__prepared = tmp_assign_source_108;
        }
        branch_end_46:;
    }
    {
        PyObject *tmp_assign_source_109;
        {
            PyObject *tmp_set_locals_12;
            CHECK_OBJECT( tmp_class_creation_12__prepared );
            tmp_set_locals_12 = tmp_class_creation_12__prepared;
            locals_yargy$interpretation$attribute_134 = tmp_set_locals_12;
            Py_INCREF( tmp_set_locals_12 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_134, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_38;
        }
        tmp_dictset_value = const_str_plain_FunctionAttribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_134, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;

            goto try_except_handler_38;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_81b99ecbdba3360f3d8f8af995ec57e8_10, codeobj_81b99ecbdba3360f3d8f8af995ec57e8, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_81b99ecbdba3360f3d8f8af995ec57e8_10 = cache_frame_81b99ecbdba3360f3d8f8af995ec57e8_10;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_81b99ecbdba3360f3d8f8af995ec57e8_10 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_81b99ecbdba3360f3d8f8af995ec57e8_10 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_attribute_str_plain_function_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_134, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_2 = "o";
            goto frame_exception_exit_10;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_21___init__(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_134, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_2 = "o";
            goto frame_exception_exit_10;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_22_custom(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_134, const_str_plain_custom, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_2 = "o";
            goto frame_exception_exit_10;
        }
        {
            nuitka_bool tmp_condition_result_76;
            PyObject *tmp_called_name_32;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_called_name_33;
            PyObject *tmp_args_element_name_10;
            tmp_res = MAPPING_HAS_ITEM( locals_yargy$interpretation$attribute_134, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_2 = "o";
                goto frame_exception_exit_10;
            }
            tmp_condition_result_76 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_76 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_29;
            }
            else
            {
                goto condexpr_false_29;
            }
            condexpr_true_29:;
            tmp_called_name_32 = PyObject_GetItem( locals_yargy$interpretation$attribute_134, const_str_plain_property );

            if ( tmp_called_name_32 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 148;
                type_description_2 = "o";
                goto frame_exception_exit_10;
            }

            if ( tmp_called_name_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_2 = "o";
                goto frame_exception_exit_10;
            }
            tmp_args_element_name_9 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_23_normalizer(  );



            frame_81b99ecbdba3360f3d8f8af995ec57e8_10->m_frame.f_lineno = 148;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_32, call_args );
            }

            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_args_element_name_9 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_2 = "o";
                goto frame_exception_exit_10;
            }
            goto condexpr_end_29;
            condexpr_false_29:;
            tmp_called_name_33 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_10 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_23_normalizer(  );



            frame_81b99ecbdba3360f3d8f8af995ec57e8_10->m_frame.f_lineno = 148;
            {
                PyObject *call_args[] = { tmp_args_element_name_10 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_33, call_args );
            }

            Py_DECREF( tmp_args_element_name_10 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_2 = "o";
                goto frame_exception_exit_10;
            }
            condexpr_end_29:;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_134, const_str_plain_normalizer, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 148;
                type_description_2 = "o";
                goto frame_exception_exit_10;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_81b99ecbdba3360f3d8f8af995ec57e8_10 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_9;

        frame_exception_exit_10:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_81b99ecbdba3360f3d8f8af995ec57e8_10 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_81b99ecbdba3360f3d8f8af995ec57e8_10, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_81b99ecbdba3360f3d8f8af995ec57e8_10->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_81b99ecbdba3360f3d8f8af995ec57e8_10, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_81b99ecbdba3360f3d8f8af995ec57e8_10,
            type_description_2,
            outline_11_var___class__
        );


        // Release cached frame.
        if ( frame_81b99ecbdba3360f3d8f8af995ec57e8_10 == cache_frame_81b99ecbdba3360f3d8f8af995ec57e8_10 )
        {
            Py_DECREF( frame_81b99ecbdba3360f3d8f8af995ec57e8_10 );
        }
        cache_frame_81b99ecbdba3360f3d8f8af995ec57e8_10 = NULL;

        assertFrameObject( frame_81b99ecbdba3360f3d8f8af995ec57e8_10 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_9;

        frame_no_exception_9:;
        goto skip_nested_handling_9;
        nested_frame_exit_9:;

        goto try_except_handler_38;
        skip_nested_handling_9:;
        {
            nuitka_bool tmp_condition_result_77;
            PyObject *tmp_compexpr_left_12;
            PyObject *tmp_compexpr_right_12;
            CHECK_OBJECT( tmp_class_creation_12__bases );
            tmp_compexpr_left_12 = tmp_class_creation_12__bases;
            CHECK_OBJECT( tmp_class_creation_12__bases_orig );
            tmp_compexpr_right_12 = tmp_class_creation_12__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_12, tmp_compexpr_right_12 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;

                goto try_except_handler_38;
            }
            tmp_condition_result_77 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_77 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_48;
            }
            else
            {
                goto branch_no_48;
            }
            branch_yes_48:;
            CHECK_OBJECT( tmp_class_creation_12__bases_orig );
            tmp_dictset_value = tmp_class_creation_12__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_134, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;

                goto try_except_handler_38;
            }
            branch_no_48:;
        }
        {
            PyObject *tmp_assign_source_110;
            PyObject *tmp_called_name_34;
            PyObject *tmp_args_name_24;
            PyObject *tmp_tuple_element_49;
            PyObject *tmp_kw_name_24;
            CHECK_OBJECT( tmp_class_creation_12__metaclass );
            tmp_called_name_34 = tmp_class_creation_12__metaclass;
            tmp_tuple_element_49 = const_str_plain_FunctionAttribute;
            tmp_args_name_24 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_49 );
            PyTuple_SET_ITEM( tmp_args_name_24, 0, tmp_tuple_element_49 );
            CHECK_OBJECT( tmp_class_creation_12__bases );
            tmp_tuple_element_49 = tmp_class_creation_12__bases;
            Py_INCREF( tmp_tuple_element_49 );
            PyTuple_SET_ITEM( tmp_args_name_24, 1, tmp_tuple_element_49 );
            tmp_tuple_element_49 = locals_yargy$interpretation$attribute_134;
            Py_INCREF( tmp_tuple_element_49 );
            PyTuple_SET_ITEM( tmp_args_name_24, 2, tmp_tuple_element_49 );
            CHECK_OBJECT( tmp_class_creation_12__class_decl_dict );
            tmp_kw_name_24 = tmp_class_creation_12__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 134;
            tmp_assign_source_110 = CALL_FUNCTION( tmp_called_name_34, tmp_args_name_24, tmp_kw_name_24 );
            Py_DECREF( tmp_args_name_24 );
            if ( tmp_assign_source_110 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;

                goto try_except_handler_38;
            }
            assert( outline_11_var___class__ == NULL );
            outline_11_var___class__ = tmp_assign_source_110;
        }
        CHECK_OBJECT( outline_11_var___class__ );
        tmp_assign_source_109 = outline_11_var___class__;
        Py_INCREF( tmp_assign_source_109 );
        goto try_return_handler_38;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_38:;
        Py_DECREF( locals_yargy$interpretation$attribute_134 );
        locals_yargy$interpretation$attribute_134 = NULL;
        goto try_return_handler_37;
        // Exception handler code:
        try_except_handler_38:;
        exception_keeper_type_36 = exception_type;
        exception_keeper_value_36 = exception_value;
        exception_keeper_tb_36 = exception_tb;
        exception_keeper_lineno_36 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_134 );
        locals_yargy$interpretation$attribute_134 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_36;
        exception_value = exception_keeper_value_36;
        exception_tb = exception_keeper_tb_36;
        exception_lineno = exception_keeper_lineno_36;

        goto try_except_handler_37;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_37:;
        CHECK_OBJECT( (PyObject *)outline_11_var___class__ );
        Py_DECREF( outline_11_var___class__ );
        outline_11_var___class__ = NULL;

        goto outline_result_12;
        // Exception handler code:
        try_except_handler_37:;
        exception_keeper_type_37 = exception_type;
        exception_keeper_value_37 = exception_value;
        exception_keeper_tb_37 = exception_tb;
        exception_keeper_lineno_37 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_37;
        exception_value = exception_keeper_value_37;
        exception_tb = exception_keeper_tb_37;
        exception_lineno = exception_keeper_lineno_37;

        goto outline_exception_12;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_12:;
        exception_lineno = 134;
        goto try_except_handler_36;
        outline_result_12:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_FunctionAttribute, tmp_assign_source_109 );
    }
    goto try_end_14;
    // Exception handler code:
    try_except_handler_36:;
    exception_keeper_type_38 = exception_type;
    exception_keeper_value_38 = exception_value;
    exception_keeper_tb_38 = exception_tb;
    exception_keeper_lineno_38 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_12__bases_orig );
    tmp_class_creation_12__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_12__bases );
    tmp_class_creation_12__bases = NULL;

    Py_XDECREF( tmp_class_creation_12__class_decl_dict );
    tmp_class_creation_12__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_12__metaclass );
    tmp_class_creation_12__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_12__prepared );
    tmp_class_creation_12__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_38;
    exception_value = exception_keeper_value_38;
    exception_tb = exception_keeper_tb_38;
    exception_lineno = exception_keeper_lineno_38;

    goto frame_exception_exit_1;
    // End of try:
    try_end_14:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_12__bases_orig );
    Py_DECREF( tmp_class_creation_12__bases_orig );
    tmp_class_creation_12__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_12__bases );
    Py_DECREF( tmp_class_creation_12__bases );
    tmp_class_creation_12__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_12__class_decl_dict );
    Py_DECREF( tmp_class_creation_12__class_decl_dict );
    tmp_class_creation_12__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_12__metaclass );
    Py_DECREF( tmp_class_creation_12__metaclass );
    tmp_class_creation_12__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_12__prepared );
    Py_DECREF( tmp_class_creation_12__prepared );
    tmp_class_creation_12__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_111;
        PyObject *tmp_tuple_element_50;
        PyObject *tmp_mvar_value_15;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_CustomAttribute );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CustomAttribute );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CustomAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 153;

            goto try_except_handler_39;
        }

        tmp_tuple_element_50 = tmp_mvar_value_15;
        tmp_assign_source_111 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_50 );
        PyTuple_SET_ITEM( tmp_assign_source_111, 0, tmp_tuple_element_50 );
        assert( tmp_class_creation_13__bases_orig == NULL );
        tmp_class_creation_13__bases_orig = tmp_assign_source_111;
    }
    {
        PyObject *tmp_assign_source_112;
        PyObject *tmp_dircall_arg1_13;
        CHECK_OBJECT( tmp_class_creation_13__bases_orig );
        tmp_dircall_arg1_13 = tmp_class_creation_13__bases_orig;
        Py_INCREF( tmp_dircall_arg1_13 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_13};
            tmp_assign_source_112 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_112 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_39;
        }
        assert( tmp_class_creation_13__bases == NULL );
        tmp_class_creation_13__bases = tmp_assign_source_112;
    }
    {
        PyObject *tmp_assign_source_113;
        tmp_assign_source_113 = PyDict_New();
        assert( tmp_class_creation_13__class_decl_dict == NULL );
        tmp_class_creation_13__class_decl_dict = tmp_assign_source_113;
    }
    {
        PyObject *tmp_assign_source_114;
        PyObject *tmp_metaclass_name_13;
        nuitka_bool tmp_condition_result_78;
        PyObject *tmp_key_name_37;
        PyObject *tmp_dict_name_37;
        PyObject *tmp_dict_name_38;
        PyObject *tmp_key_name_38;
        nuitka_bool tmp_condition_result_79;
        int tmp_truth_name_13;
        PyObject *tmp_type_arg_25;
        PyObject *tmp_subscribed_name_13;
        PyObject *tmp_subscript_name_13;
        PyObject *tmp_bases_name_13;
        tmp_key_name_37 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_13__class_decl_dict );
        tmp_dict_name_37 = tmp_class_creation_13__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_37, tmp_key_name_37 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_39;
        }
        tmp_condition_result_78 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_78 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_30;
        }
        else
        {
            goto condexpr_false_30;
        }
        condexpr_true_30:;
        CHECK_OBJECT( tmp_class_creation_13__class_decl_dict );
        tmp_dict_name_38 = tmp_class_creation_13__class_decl_dict;
        tmp_key_name_38 = const_str_plain_metaclass;
        tmp_metaclass_name_13 = DICT_GET_ITEM( tmp_dict_name_38, tmp_key_name_38 );
        if ( tmp_metaclass_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_39;
        }
        goto condexpr_end_30;
        condexpr_false_30:;
        CHECK_OBJECT( tmp_class_creation_13__bases );
        tmp_truth_name_13 = CHECK_IF_TRUE( tmp_class_creation_13__bases );
        if ( tmp_truth_name_13 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_39;
        }
        tmp_condition_result_79 = tmp_truth_name_13 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_79 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_31;
        }
        else
        {
            goto condexpr_false_31;
        }
        condexpr_true_31:;
        CHECK_OBJECT( tmp_class_creation_13__bases );
        tmp_subscribed_name_13 = tmp_class_creation_13__bases;
        tmp_subscript_name_13 = const_int_0;
        tmp_type_arg_25 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_13, tmp_subscript_name_13, 0 );
        if ( tmp_type_arg_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_39;
        }
        tmp_metaclass_name_13 = BUILTIN_TYPE1( tmp_type_arg_25 );
        Py_DECREF( tmp_type_arg_25 );
        if ( tmp_metaclass_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_39;
        }
        goto condexpr_end_31;
        condexpr_false_31:;
        tmp_metaclass_name_13 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_13 );
        condexpr_end_31:;
        condexpr_end_30:;
        CHECK_OBJECT( tmp_class_creation_13__bases );
        tmp_bases_name_13 = tmp_class_creation_13__bases;
        tmp_assign_source_114 = SELECT_METACLASS( tmp_metaclass_name_13, tmp_bases_name_13 );
        Py_DECREF( tmp_metaclass_name_13 );
        if ( tmp_assign_source_114 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_39;
        }
        assert( tmp_class_creation_13__metaclass == NULL );
        tmp_class_creation_13__metaclass = tmp_assign_source_114;
    }
    {
        nuitka_bool tmp_condition_result_80;
        PyObject *tmp_key_name_39;
        PyObject *tmp_dict_name_39;
        tmp_key_name_39 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_13__class_decl_dict );
        tmp_dict_name_39 = tmp_class_creation_13__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_39, tmp_key_name_39 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_39;
        }
        tmp_condition_result_80 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_80 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_49;
        }
        else
        {
            goto branch_no_49;
        }
        branch_yes_49:;
        CHECK_OBJECT( tmp_class_creation_13__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_13__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_39;
        }
        branch_no_49:;
    }
    {
        nuitka_bool tmp_condition_result_81;
        PyObject *tmp_source_name_49;
        CHECK_OBJECT( tmp_class_creation_13__metaclass );
        tmp_source_name_49 = tmp_class_creation_13__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_49, const_str_plain___prepare__ );
        tmp_condition_result_81 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_81 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_50;
        }
        else
        {
            goto branch_no_50;
        }
        branch_yes_50:;
        {
            PyObject *tmp_assign_source_115;
            PyObject *tmp_called_name_35;
            PyObject *tmp_source_name_50;
            PyObject *tmp_args_name_25;
            PyObject *tmp_tuple_element_51;
            PyObject *tmp_kw_name_25;
            CHECK_OBJECT( tmp_class_creation_13__metaclass );
            tmp_source_name_50 = tmp_class_creation_13__metaclass;
            tmp_called_name_35 = LOOKUP_ATTRIBUTE( tmp_source_name_50, const_str_plain___prepare__ );
            if ( tmp_called_name_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 153;

                goto try_except_handler_39;
            }
            tmp_tuple_element_51 = const_str_plain_FunctionFunctionAttribute;
            tmp_args_name_25 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_51 );
            PyTuple_SET_ITEM( tmp_args_name_25, 0, tmp_tuple_element_51 );
            CHECK_OBJECT( tmp_class_creation_13__bases );
            tmp_tuple_element_51 = tmp_class_creation_13__bases;
            Py_INCREF( tmp_tuple_element_51 );
            PyTuple_SET_ITEM( tmp_args_name_25, 1, tmp_tuple_element_51 );
            CHECK_OBJECT( tmp_class_creation_13__class_decl_dict );
            tmp_kw_name_25 = tmp_class_creation_13__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 153;
            tmp_assign_source_115 = CALL_FUNCTION( tmp_called_name_35, tmp_args_name_25, tmp_kw_name_25 );
            Py_DECREF( tmp_called_name_35 );
            Py_DECREF( tmp_args_name_25 );
            if ( tmp_assign_source_115 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 153;

                goto try_except_handler_39;
            }
            assert( tmp_class_creation_13__prepared == NULL );
            tmp_class_creation_13__prepared = tmp_assign_source_115;
        }
        {
            nuitka_bool tmp_condition_result_82;
            PyObject *tmp_operand_name_13;
            PyObject *tmp_source_name_51;
            CHECK_OBJECT( tmp_class_creation_13__prepared );
            tmp_source_name_51 = tmp_class_creation_13__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_51, const_str_plain___getitem__ );
            tmp_operand_name_13 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_13 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 153;

                goto try_except_handler_39;
            }
            tmp_condition_result_82 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_82 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_51;
            }
            else
            {
                goto branch_no_51;
            }
            branch_yes_51:;
            {
                PyObject *tmp_raise_type_13;
                PyObject *tmp_raise_value_13;
                PyObject *tmp_left_name_13;
                PyObject *tmp_right_name_13;
                PyObject *tmp_tuple_element_52;
                PyObject *tmp_getattr_target_13;
                PyObject *tmp_getattr_attr_13;
                PyObject *tmp_getattr_default_13;
                PyObject *tmp_source_name_52;
                PyObject *tmp_type_arg_26;
                tmp_raise_type_13 = PyExc_TypeError;
                tmp_left_name_13 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_13__metaclass );
                tmp_getattr_target_13 = tmp_class_creation_13__metaclass;
                tmp_getattr_attr_13 = const_str_plain___name__;
                tmp_getattr_default_13 = const_str_angle_metaclass;
                tmp_tuple_element_52 = BUILTIN_GETATTR( tmp_getattr_target_13, tmp_getattr_attr_13, tmp_getattr_default_13 );
                if ( tmp_tuple_element_52 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;

                    goto try_except_handler_39;
                }
                tmp_right_name_13 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_13, 0, tmp_tuple_element_52 );
                CHECK_OBJECT( tmp_class_creation_13__prepared );
                tmp_type_arg_26 = tmp_class_creation_13__prepared;
                tmp_source_name_52 = BUILTIN_TYPE1( tmp_type_arg_26 );
                assert( !(tmp_source_name_52 == NULL) );
                tmp_tuple_element_52 = LOOKUP_ATTRIBUTE( tmp_source_name_52, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_52 );
                if ( tmp_tuple_element_52 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_13 );

                    exception_lineno = 153;

                    goto try_except_handler_39;
                }
                PyTuple_SET_ITEM( tmp_right_name_13, 1, tmp_tuple_element_52 );
                tmp_raise_value_13 = BINARY_OPERATION_REMAINDER( tmp_left_name_13, tmp_right_name_13 );
                Py_DECREF( tmp_right_name_13 );
                if ( tmp_raise_value_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;

                    goto try_except_handler_39;
                }
                exception_type = tmp_raise_type_13;
                Py_INCREF( tmp_raise_type_13 );
                exception_value = tmp_raise_value_13;
                exception_lineno = 153;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_39;
            }
            branch_no_51:;
        }
        goto branch_end_50;
        branch_no_50:;
        {
            PyObject *tmp_assign_source_116;
            tmp_assign_source_116 = PyDict_New();
            assert( tmp_class_creation_13__prepared == NULL );
            tmp_class_creation_13__prepared = tmp_assign_source_116;
        }
        branch_end_50:;
    }
    {
        PyObject *tmp_assign_source_117;
        {
            PyObject *tmp_set_locals_13;
            CHECK_OBJECT( tmp_class_creation_13__prepared );
            tmp_set_locals_13 = tmp_class_creation_13__prepared;
            locals_yargy$interpretation$attribute_153 = tmp_set_locals_13;
            Py_INCREF( tmp_set_locals_13 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_153, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_41;
        }
        tmp_dictset_value = const_str_plain_FunctionFunctionAttribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_153, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;

            goto try_except_handler_41;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_115e07a0ba9198c2c27c90390c1c32eb_11, codeobj_115e07a0ba9198c2c27c90390c1c32eb, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_115e07a0ba9198c2c27c90390c1c32eb_11 = cache_frame_115e07a0ba9198c2c27c90390c1c32eb_11;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_115e07a0ba9198c2c27c90390c1c32eb_11 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_115e07a0ba9198c2c27c90390c1c32eb_11 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_attribute_str_plain_first_str_plain_second_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_153, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_2 = "o";
            goto frame_exception_exit_11;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_24___init__(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_153, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_2 = "o";
            goto frame_exception_exit_11;
        }
        {
            nuitka_bool tmp_condition_result_83;
            PyObject *tmp_called_name_36;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_called_name_37;
            PyObject *tmp_args_element_name_12;
            tmp_res = MAPPING_HAS_ITEM( locals_yargy$interpretation$attribute_153, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            tmp_condition_result_83 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_83 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_32;
            }
            else
            {
                goto condexpr_false_32;
            }
            condexpr_true_32:;
            tmp_called_name_36 = PyObject_GetItem( locals_yargy$interpretation$attribute_153, const_str_plain_property );

            if ( tmp_called_name_36 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 161;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }

            if ( tmp_called_name_36 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            tmp_args_element_name_11 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_25_normalizer(  );



            frame_115e07a0ba9198c2c27c90390c1c32eb_11->m_frame.f_lineno = 161;
            {
                PyObject *call_args[] = { tmp_args_element_name_11 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_36, call_args );
            }

            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_args_element_name_11 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            goto condexpr_end_32;
            condexpr_false_32:;
            tmp_called_name_37 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_12 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_25_normalizer(  );



            frame_115e07a0ba9198c2c27c90390c1c32eb_11->m_frame.f_lineno = 161;
            {
                PyObject *call_args[] = { tmp_args_element_name_12 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_37, call_args );
            }

            Py_DECREF( tmp_args_element_name_12 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
            condexpr_end_32:;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_153, const_str_plain_normalizer, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 161;
                type_description_2 = "o";
                goto frame_exception_exit_11;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_115e07a0ba9198c2c27c90390c1c32eb_11 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_10;

        frame_exception_exit_11:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_115e07a0ba9198c2c27c90390c1c32eb_11 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_115e07a0ba9198c2c27c90390c1c32eb_11, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_115e07a0ba9198c2c27c90390c1c32eb_11->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_115e07a0ba9198c2c27c90390c1c32eb_11, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_115e07a0ba9198c2c27c90390c1c32eb_11,
            type_description_2,
            outline_12_var___class__
        );


        // Release cached frame.
        if ( frame_115e07a0ba9198c2c27c90390c1c32eb_11 == cache_frame_115e07a0ba9198c2c27c90390c1c32eb_11 )
        {
            Py_DECREF( frame_115e07a0ba9198c2c27c90390c1c32eb_11 );
        }
        cache_frame_115e07a0ba9198c2c27c90390c1c32eb_11 = NULL;

        assertFrameObject( frame_115e07a0ba9198c2c27c90390c1c32eb_11 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_10;

        frame_no_exception_10:;
        goto skip_nested_handling_10;
        nested_frame_exit_10:;

        goto try_except_handler_41;
        skip_nested_handling_10:;
        {
            nuitka_bool tmp_condition_result_84;
            PyObject *tmp_compexpr_left_13;
            PyObject *tmp_compexpr_right_13;
            CHECK_OBJECT( tmp_class_creation_13__bases );
            tmp_compexpr_left_13 = tmp_class_creation_13__bases;
            CHECK_OBJECT( tmp_class_creation_13__bases_orig );
            tmp_compexpr_right_13 = tmp_class_creation_13__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_13, tmp_compexpr_right_13 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 153;

                goto try_except_handler_41;
            }
            tmp_condition_result_84 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_84 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_52;
            }
            else
            {
                goto branch_no_52;
            }
            branch_yes_52:;
            CHECK_OBJECT( tmp_class_creation_13__bases_orig );
            tmp_dictset_value = tmp_class_creation_13__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_153, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 153;

                goto try_except_handler_41;
            }
            branch_no_52:;
        }
        {
            PyObject *tmp_assign_source_118;
            PyObject *tmp_called_name_38;
            PyObject *tmp_args_name_26;
            PyObject *tmp_tuple_element_53;
            PyObject *tmp_kw_name_26;
            CHECK_OBJECT( tmp_class_creation_13__metaclass );
            tmp_called_name_38 = tmp_class_creation_13__metaclass;
            tmp_tuple_element_53 = const_str_plain_FunctionFunctionAttribute;
            tmp_args_name_26 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_53 );
            PyTuple_SET_ITEM( tmp_args_name_26, 0, tmp_tuple_element_53 );
            CHECK_OBJECT( tmp_class_creation_13__bases );
            tmp_tuple_element_53 = tmp_class_creation_13__bases;
            Py_INCREF( tmp_tuple_element_53 );
            PyTuple_SET_ITEM( tmp_args_name_26, 1, tmp_tuple_element_53 );
            tmp_tuple_element_53 = locals_yargy$interpretation$attribute_153;
            Py_INCREF( tmp_tuple_element_53 );
            PyTuple_SET_ITEM( tmp_args_name_26, 2, tmp_tuple_element_53 );
            CHECK_OBJECT( tmp_class_creation_13__class_decl_dict );
            tmp_kw_name_26 = tmp_class_creation_13__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 153;
            tmp_assign_source_118 = CALL_FUNCTION( tmp_called_name_38, tmp_args_name_26, tmp_kw_name_26 );
            Py_DECREF( tmp_args_name_26 );
            if ( tmp_assign_source_118 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 153;

                goto try_except_handler_41;
            }
            assert( outline_12_var___class__ == NULL );
            outline_12_var___class__ = tmp_assign_source_118;
        }
        CHECK_OBJECT( outline_12_var___class__ );
        tmp_assign_source_117 = outline_12_var___class__;
        Py_INCREF( tmp_assign_source_117 );
        goto try_return_handler_41;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_41:;
        Py_DECREF( locals_yargy$interpretation$attribute_153 );
        locals_yargy$interpretation$attribute_153 = NULL;
        goto try_return_handler_40;
        // Exception handler code:
        try_except_handler_41:;
        exception_keeper_type_39 = exception_type;
        exception_keeper_value_39 = exception_value;
        exception_keeper_tb_39 = exception_tb;
        exception_keeper_lineno_39 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_153 );
        locals_yargy$interpretation$attribute_153 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_39;
        exception_value = exception_keeper_value_39;
        exception_tb = exception_keeper_tb_39;
        exception_lineno = exception_keeper_lineno_39;

        goto try_except_handler_40;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_40:;
        CHECK_OBJECT( (PyObject *)outline_12_var___class__ );
        Py_DECREF( outline_12_var___class__ );
        outline_12_var___class__ = NULL;

        goto outline_result_13;
        // Exception handler code:
        try_except_handler_40:;
        exception_keeper_type_40 = exception_type;
        exception_keeper_value_40 = exception_value;
        exception_keeper_tb_40 = exception_tb;
        exception_keeper_lineno_40 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_40;
        exception_value = exception_keeper_value_40;
        exception_tb = exception_keeper_tb_40;
        exception_lineno = exception_keeper_lineno_40;

        goto outline_exception_13;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_13:;
        exception_lineno = 153;
        goto try_except_handler_39;
        outline_result_13:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_FunctionFunctionAttribute, tmp_assign_source_117 );
    }
    goto try_end_15;
    // Exception handler code:
    try_except_handler_39:;
    exception_keeper_type_41 = exception_type;
    exception_keeper_value_41 = exception_value;
    exception_keeper_tb_41 = exception_tb;
    exception_keeper_lineno_41 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_13__bases_orig );
    tmp_class_creation_13__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_13__bases );
    tmp_class_creation_13__bases = NULL;

    Py_XDECREF( tmp_class_creation_13__class_decl_dict );
    tmp_class_creation_13__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_13__metaclass );
    tmp_class_creation_13__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_13__prepared );
    tmp_class_creation_13__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_41;
    exception_value = exception_keeper_value_41;
    exception_tb = exception_keeper_tb_41;
    exception_lineno = exception_keeper_lineno_41;

    goto frame_exception_exit_1;
    // End of try:
    try_end_15:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_13__bases_orig );
    Py_DECREF( tmp_class_creation_13__bases_orig );
    tmp_class_creation_13__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_13__bases );
    Py_DECREF( tmp_class_creation_13__bases );
    tmp_class_creation_13__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_13__class_decl_dict );
    Py_DECREF( tmp_class_creation_13__class_decl_dict );
    tmp_class_creation_13__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_13__metaclass );
    Py_DECREF( tmp_class_creation_13__metaclass );
    tmp_class_creation_13__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_13__prepared );
    Py_DECREF( tmp_class_creation_13__prepared );
    tmp_class_creation_13__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_119;
        PyObject *tmp_tuple_element_54;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_mvar_value_17;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_MorphAttribute );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MorphAttribute );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MorphAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto try_except_handler_42;
        }

        tmp_tuple_element_54 = tmp_mvar_value_16;
        tmp_assign_source_119 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_54 );
        PyTuple_SET_ITEM( tmp_assign_source_119, 0, tmp_tuple_element_54 );
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_CustomAttribute );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CustomAttribute );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_assign_source_119 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CustomAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto try_except_handler_42;
        }

        tmp_tuple_element_54 = tmp_mvar_value_17;
        Py_INCREF( tmp_tuple_element_54 );
        PyTuple_SET_ITEM( tmp_assign_source_119, 1, tmp_tuple_element_54 );
        assert( tmp_class_creation_14__bases_orig == NULL );
        tmp_class_creation_14__bases_orig = tmp_assign_source_119;
    }
    {
        PyObject *tmp_assign_source_120;
        PyObject *tmp_dircall_arg1_14;
        CHECK_OBJECT( tmp_class_creation_14__bases_orig );
        tmp_dircall_arg1_14 = tmp_class_creation_14__bases_orig;
        Py_INCREF( tmp_dircall_arg1_14 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_14};
            tmp_assign_source_120 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_120 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_42;
        }
        assert( tmp_class_creation_14__bases == NULL );
        tmp_class_creation_14__bases = tmp_assign_source_120;
    }
    {
        PyObject *tmp_assign_source_121;
        tmp_assign_source_121 = PyDict_New();
        assert( tmp_class_creation_14__class_decl_dict == NULL );
        tmp_class_creation_14__class_decl_dict = tmp_assign_source_121;
    }
    {
        PyObject *tmp_assign_source_122;
        PyObject *tmp_metaclass_name_14;
        nuitka_bool tmp_condition_result_85;
        PyObject *tmp_key_name_40;
        PyObject *tmp_dict_name_40;
        PyObject *tmp_dict_name_41;
        PyObject *tmp_key_name_41;
        nuitka_bool tmp_condition_result_86;
        int tmp_truth_name_14;
        PyObject *tmp_type_arg_27;
        PyObject *tmp_subscribed_name_14;
        PyObject *tmp_subscript_name_14;
        PyObject *tmp_bases_name_14;
        tmp_key_name_40 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_14__class_decl_dict );
        tmp_dict_name_40 = tmp_class_creation_14__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_40, tmp_key_name_40 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_42;
        }
        tmp_condition_result_85 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_85 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_33;
        }
        else
        {
            goto condexpr_false_33;
        }
        condexpr_true_33:;
        CHECK_OBJECT( tmp_class_creation_14__class_decl_dict );
        tmp_dict_name_41 = tmp_class_creation_14__class_decl_dict;
        tmp_key_name_41 = const_str_plain_metaclass;
        tmp_metaclass_name_14 = DICT_GET_ITEM( tmp_dict_name_41, tmp_key_name_41 );
        if ( tmp_metaclass_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_42;
        }
        goto condexpr_end_33;
        condexpr_false_33:;
        CHECK_OBJECT( tmp_class_creation_14__bases );
        tmp_truth_name_14 = CHECK_IF_TRUE( tmp_class_creation_14__bases );
        if ( tmp_truth_name_14 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_42;
        }
        tmp_condition_result_86 = tmp_truth_name_14 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_86 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_34;
        }
        else
        {
            goto condexpr_false_34;
        }
        condexpr_true_34:;
        CHECK_OBJECT( tmp_class_creation_14__bases );
        tmp_subscribed_name_14 = tmp_class_creation_14__bases;
        tmp_subscript_name_14 = const_int_0;
        tmp_type_arg_27 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_14, tmp_subscript_name_14, 0 );
        if ( tmp_type_arg_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_42;
        }
        tmp_metaclass_name_14 = BUILTIN_TYPE1( tmp_type_arg_27 );
        Py_DECREF( tmp_type_arg_27 );
        if ( tmp_metaclass_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_42;
        }
        goto condexpr_end_34;
        condexpr_false_34:;
        tmp_metaclass_name_14 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_14 );
        condexpr_end_34:;
        condexpr_end_33:;
        CHECK_OBJECT( tmp_class_creation_14__bases );
        tmp_bases_name_14 = tmp_class_creation_14__bases;
        tmp_assign_source_122 = SELECT_METACLASS( tmp_metaclass_name_14, tmp_bases_name_14 );
        Py_DECREF( tmp_metaclass_name_14 );
        if ( tmp_assign_source_122 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_42;
        }
        assert( tmp_class_creation_14__metaclass == NULL );
        tmp_class_creation_14__metaclass = tmp_assign_source_122;
    }
    {
        nuitka_bool tmp_condition_result_87;
        PyObject *tmp_key_name_42;
        PyObject *tmp_dict_name_42;
        tmp_key_name_42 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_14__class_decl_dict );
        tmp_dict_name_42 = tmp_class_creation_14__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_42, tmp_key_name_42 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_42;
        }
        tmp_condition_result_87 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_87 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_53;
        }
        else
        {
            goto branch_no_53;
        }
        branch_yes_53:;
        CHECK_OBJECT( tmp_class_creation_14__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_14__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_42;
        }
        branch_no_53:;
    }
    {
        nuitka_bool tmp_condition_result_88;
        PyObject *tmp_source_name_53;
        CHECK_OBJECT( tmp_class_creation_14__metaclass );
        tmp_source_name_53 = tmp_class_creation_14__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_53, const_str_plain___prepare__ );
        tmp_condition_result_88 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_88 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_54;
        }
        else
        {
            goto branch_no_54;
        }
        branch_yes_54:;
        {
            PyObject *tmp_assign_source_123;
            PyObject *tmp_called_name_39;
            PyObject *tmp_source_name_54;
            PyObject *tmp_args_name_27;
            PyObject *tmp_tuple_element_55;
            PyObject *tmp_kw_name_27;
            CHECK_OBJECT( tmp_class_creation_14__metaclass );
            tmp_source_name_54 = tmp_class_creation_14__metaclass;
            tmp_called_name_39 = LOOKUP_ATTRIBUTE( tmp_source_name_54, const_str_plain___prepare__ );
            if ( tmp_called_name_39 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;

                goto try_except_handler_42;
            }
            tmp_tuple_element_55 = const_str_plain_InflectedFunctionAttribute;
            tmp_args_name_27 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_55 );
            PyTuple_SET_ITEM( tmp_args_name_27, 0, tmp_tuple_element_55 );
            CHECK_OBJECT( tmp_class_creation_14__bases );
            tmp_tuple_element_55 = tmp_class_creation_14__bases;
            Py_INCREF( tmp_tuple_element_55 );
            PyTuple_SET_ITEM( tmp_args_name_27, 1, tmp_tuple_element_55 );
            CHECK_OBJECT( tmp_class_creation_14__class_decl_dict );
            tmp_kw_name_27 = tmp_class_creation_14__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 166;
            tmp_assign_source_123 = CALL_FUNCTION( tmp_called_name_39, tmp_args_name_27, tmp_kw_name_27 );
            Py_DECREF( tmp_called_name_39 );
            Py_DECREF( tmp_args_name_27 );
            if ( tmp_assign_source_123 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;

                goto try_except_handler_42;
            }
            assert( tmp_class_creation_14__prepared == NULL );
            tmp_class_creation_14__prepared = tmp_assign_source_123;
        }
        {
            nuitka_bool tmp_condition_result_89;
            PyObject *tmp_operand_name_14;
            PyObject *tmp_source_name_55;
            CHECK_OBJECT( tmp_class_creation_14__prepared );
            tmp_source_name_55 = tmp_class_creation_14__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_55, const_str_plain___getitem__ );
            tmp_operand_name_14 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_14 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;

                goto try_except_handler_42;
            }
            tmp_condition_result_89 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_89 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_55;
            }
            else
            {
                goto branch_no_55;
            }
            branch_yes_55:;
            {
                PyObject *tmp_raise_type_14;
                PyObject *tmp_raise_value_14;
                PyObject *tmp_left_name_14;
                PyObject *tmp_right_name_14;
                PyObject *tmp_tuple_element_56;
                PyObject *tmp_getattr_target_14;
                PyObject *tmp_getattr_attr_14;
                PyObject *tmp_getattr_default_14;
                PyObject *tmp_source_name_56;
                PyObject *tmp_type_arg_28;
                tmp_raise_type_14 = PyExc_TypeError;
                tmp_left_name_14 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_14__metaclass );
                tmp_getattr_target_14 = tmp_class_creation_14__metaclass;
                tmp_getattr_attr_14 = const_str_plain___name__;
                tmp_getattr_default_14 = const_str_angle_metaclass;
                tmp_tuple_element_56 = BUILTIN_GETATTR( tmp_getattr_target_14, tmp_getattr_attr_14, tmp_getattr_default_14 );
                if ( tmp_tuple_element_56 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 166;

                    goto try_except_handler_42;
                }
                tmp_right_name_14 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_14, 0, tmp_tuple_element_56 );
                CHECK_OBJECT( tmp_class_creation_14__prepared );
                tmp_type_arg_28 = tmp_class_creation_14__prepared;
                tmp_source_name_56 = BUILTIN_TYPE1( tmp_type_arg_28 );
                assert( !(tmp_source_name_56 == NULL) );
                tmp_tuple_element_56 = LOOKUP_ATTRIBUTE( tmp_source_name_56, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_56 );
                if ( tmp_tuple_element_56 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_14 );

                    exception_lineno = 166;

                    goto try_except_handler_42;
                }
                PyTuple_SET_ITEM( tmp_right_name_14, 1, tmp_tuple_element_56 );
                tmp_raise_value_14 = BINARY_OPERATION_REMAINDER( tmp_left_name_14, tmp_right_name_14 );
                Py_DECREF( tmp_right_name_14 );
                if ( tmp_raise_value_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 166;

                    goto try_except_handler_42;
                }
                exception_type = tmp_raise_type_14;
                Py_INCREF( tmp_raise_type_14 );
                exception_value = tmp_raise_value_14;
                exception_lineno = 166;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_42;
            }
            branch_no_55:;
        }
        goto branch_end_54;
        branch_no_54:;
        {
            PyObject *tmp_assign_source_124;
            tmp_assign_source_124 = PyDict_New();
            assert( tmp_class_creation_14__prepared == NULL );
            tmp_class_creation_14__prepared = tmp_assign_source_124;
        }
        branch_end_54:;
    }
    {
        PyObject *tmp_assign_source_125;
        {
            PyObject *tmp_set_locals_14;
            CHECK_OBJECT( tmp_class_creation_14__prepared );
            tmp_set_locals_14 = tmp_class_creation_14__prepared;
            locals_yargy$interpretation$attribute_166 = tmp_set_locals_14;
            Py_INCREF( tmp_set_locals_14 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_166, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_44;
        }
        tmp_dictset_value = const_str_plain_InflectedFunctionAttribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_166, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto try_except_handler_44;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_81c89dc97b0264a85ff7fd1cf1c4772f_12, codeobj_81c89dc97b0264a85ff7fd1cf1c4772f, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 = cache_frame_81c89dc97b0264a85ff7fd1cf1c4772f_12;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_attribute_str_plain_grams_str_plain_function_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_166, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 167;
            type_description_2 = "o";
            goto frame_exception_exit_12;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_26___init__(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_166, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_2 = "o";
            goto frame_exception_exit_12;
        }
        {
            nuitka_bool tmp_condition_result_90;
            PyObject *tmp_called_name_40;
            PyObject *tmp_args_element_name_13;
            PyObject *tmp_called_name_41;
            PyObject *tmp_args_element_name_14;
            tmp_res = MAPPING_HAS_ITEM( locals_yargy$interpretation$attribute_166, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_2 = "o";
                goto frame_exception_exit_12;
            }
            tmp_condition_result_90 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_90 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_35;
            }
            else
            {
                goto condexpr_false_35;
            }
            condexpr_true_35:;
            tmp_called_name_40 = PyObject_GetItem( locals_yargy$interpretation$attribute_166, const_str_plain_property );

            if ( tmp_called_name_40 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 174;
                type_description_2 = "o";
                goto frame_exception_exit_12;
            }

            if ( tmp_called_name_40 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_2 = "o";
                goto frame_exception_exit_12;
            }
            tmp_args_element_name_13 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_27_normalizer(  );



            frame_81c89dc97b0264a85ff7fd1cf1c4772f_12->m_frame.f_lineno = 174;
            {
                PyObject *call_args[] = { tmp_args_element_name_13 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_40, call_args );
            }

            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_args_element_name_13 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_2 = "o";
                goto frame_exception_exit_12;
            }
            goto condexpr_end_35;
            condexpr_false_35:;
            tmp_called_name_41 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_14 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_27_normalizer(  );



            frame_81c89dc97b0264a85ff7fd1cf1c4772f_12->m_frame.f_lineno = 174;
            {
                PyObject *call_args[] = { tmp_args_element_name_14 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_41, call_args );
            }

            Py_DECREF( tmp_args_element_name_14 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_2 = "o";
                goto frame_exception_exit_12;
            }
            condexpr_end_35:;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_166, const_str_plain_normalizer, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 174;
                type_description_2 = "o";
                goto frame_exception_exit_12;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_11;

        frame_exception_exit_12:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_81c89dc97b0264a85ff7fd1cf1c4772f_12, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_81c89dc97b0264a85ff7fd1cf1c4772f_12->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_81c89dc97b0264a85ff7fd1cf1c4772f_12, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_81c89dc97b0264a85ff7fd1cf1c4772f_12,
            type_description_2,
            outline_13_var___class__
        );


        // Release cached frame.
        if ( frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 == cache_frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 )
        {
            Py_DECREF( frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 );
        }
        cache_frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 = NULL;

        assertFrameObject( frame_81c89dc97b0264a85ff7fd1cf1c4772f_12 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_11;

        frame_no_exception_11:;
        goto skip_nested_handling_11;
        nested_frame_exit_11:;

        goto try_except_handler_44;
        skip_nested_handling_11:;
        {
            nuitka_bool tmp_condition_result_91;
            PyObject *tmp_compexpr_left_14;
            PyObject *tmp_compexpr_right_14;
            CHECK_OBJECT( tmp_class_creation_14__bases );
            tmp_compexpr_left_14 = tmp_class_creation_14__bases;
            CHECK_OBJECT( tmp_class_creation_14__bases_orig );
            tmp_compexpr_right_14 = tmp_class_creation_14__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_14, tmp_compexpr_right_14 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;

                goto try_except_handler_44;
            }
            tmp_condition_result_91 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_91 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_56;
            }
            else
            {
                goto branch_no_56;
            }
            branch_yes_56:;
            CHECK_OBJECT( tmp_class_creation_14__bases_orig );
            tmp_dictset_value = tmp_class_creation_14__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_166, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;

                goto try_except_handler_44;
            }
            branch_no_56:;
        }
        {
            PyObject *tmp_assign_source_126;
            PyObject *tmp_called_name_42;
            PyObject *tmp_args_name_28;
            PyObject *tmp_tuple_element_57;
            PyObject *tmp_kw_name_28;
            CHECK_OBJECT( tmp_class_creation_14__metaclass );
            tmp_called_name_42 = tmp_class_creation_14__metaclass;
            tmp_tuple_element_57 = const_str_plain_InflectedFunctionAttribute;
            tmp_args_name_28 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_57 );
            PyTuple_SET_ITEM( tmp_args_name_28, 0, tmp_tuple_element_57 );
            CHECK_OBJECT( tmp_class_creation_14__bases );
            tmp_tuple_element_57 = tmp_class_creation_14__bases;
            Py_INCREF( tmp_tuple_element_57 );
            PyTuple_SET_ITEM( tmp_args_name_28, 1, tmp_tuple_element_57 );
            tmp_tuple_element_57 = locals_yargy$interpretation$attribute_166;
            Py_INCREF( tmp_tuple_element_57 );
            PyTuple_SET_ITEM( tmp_args_name_28, 2, tmp_tuple_element_57 );
            CHECK_OBJECT( tmp_class_creation_14__class_decl_dict );
            tmp_kw_name_28 = tmp_class_creation_14__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 166;
            tmp_assign_source_126 = CALL_FUNCTION( tmp_called_name_42, tmp_args_name_28, tmp_kw_name_28 );
            Py_DECREF( tmp_args_name_28 );
            if ( tmp_assign_source_126 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;

                goto try_except_handler_44;
            }
            assert( outline_13_var___class__ == NULL );
            outline_13_var___class__ = tmp_assign_source_126;
        }
        CHECK_OBJECT( outline_13_var___class__ );
        tmp_assign_source_125 = outline_13_var___class__;
        Py_INCREF( tmp_assign_source_125 );
        goto try_return_handler_44;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_44:;
        Py_DECREF( locals_yargy$interpretation$attribute_166 );
        locals_yargy$interpretation$attribute_166 = NULL;
        goto try_return_handler_43;
        // Exception handler code:
        try_except_handler_44:;
        exception_keeper_type_42 = exception_type;
        exception_keeper_value_42 = exception_value;
        exception_keeper_tb_42 = exception_tb;
        exception_keeper_lineno_42 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_166 );
        locals_yargy$interpretation$attribute_166 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_42;
        exception_value = exception_keeper_value_42;
        exception_tb = exception_keeper_tb_42;
        exception_lineno = exception_keeper_lineno_42;

        goto try_except_handler_43;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_43:;
        CHECK_OBJECT( (PyObject *)outline_13_var___class__ );
        Py_DECREF( outline_13_var___class__ );
        outline_13_var___class__ = NULL;

        goto outline_result_14;
        // Exception handler code:
        try_except_handler_43:;
        exception_keeper_type_43 = exception_type;
        exception_keeper_value_43 = exception_value;
        exception_keeper_tb_43 = exception_tb;
        exception_keeper_lineno_43 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_43;
        exception_value = exception_keeper_value_43;
        exception_tb = exception_keeper_tb_43;
        exception_lineno = exception_keeper_lineno_43;

        goto outline_exception_14;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_14:;
        exception_lineno = 166;
        goto try_except_handler_42;
        outline_result_14:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_InflectedFunctionAttribute, tmp_assign_source_125 );
    }
    goto try_end_16;
    // Exception handler code:
    try_except_handler_42:;
    exception_keeper_type_44 = exception_type;
    exception_keeper_value_44 = exception_value;
    exception_keeper_tb_44 = exception_tb;
    exception_keeper_lineno_44 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_14__bases_orig );
    tmp_class_creation_14__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_14__bases );
    tmp_class_creation_14__bases = NULL;

    Py_XDECREF( tmp_class_creation_14__class_decl_dict );
    tmp_class_creation_14__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_14__metaclass );
    tmp_class_creation_14__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_14__prepared );
    tmp_class_creation_14__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_44;
    exception_value = exception_keeper_value_44;
    exception_tb = exception_keeper_tb_44;
    exception_lineno = exception_keeper_lineno_44;

    goto frame_exception_exit_1;
    // End of try:
    try_end_16:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_14__bases_orig );
    Py_DECREF( tmp_class_creation_14__bases_orig );
    tmp_class_creation_14__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_14__bases );
    Py_DECREF( tmp_class_creation_14__bases );
    tmp_class_creation_14__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_14__class_decl_dict );
    Py_DECREF( tmp_class_creation_14__class_decl_dict );
    tmp_class_creation_14__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_14__metaclass );
    Py_DECREF( tmp_class_creation_14__metaclass );
    tmp_class_creation_14__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_14__prepared );
    Py_DECREF( tmp_class_creation_14__prepared );
    tmp_class_creation_14__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_127;
        PyObject *tmp_tuple_element_58;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_mvar_value_19;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_MorphAttribute );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_MorphAttribute );
        }

        if ( tmp_mvar_value_18 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "MorphAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 182;

            goto try_except_handler_45;
        }

        tmp_tuple_element_58 = tmp_mvar_value_18;
        tmp_assign_source_127 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_58 );
        PyTuple_SET_ITEM( tmp_assign_source_127, 0, tmp_tuple_element_58 );
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_CustomAttribute );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CustomAttribute );
        }

        if ( tmp_mvar_value_19 == NULL )
        {
            Py_DECREF( tmp_assign_source_127 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CustomAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 182;

            goto try_except_handler_45;
        }

        tmp_tuple_element_58 = tmp_mvar_value_19;
        Py_INCREF( tmp_tuple_element_58 );
        PyTuple_SET_ITEM( tmp_assign_source_127, 1, tmp_tuple_element_58 );
        assert( tmp_class_creation_15__bases_orig == NULL );
        tmp_class_creation_15__bases_orig = tmp_assign_source_127;
    }
    {
        PyObject *tmp_assign_source_128;
        PyObject *tmp_dircall_arg1_15;
        CHECK_OBJECT( tmp_class_creation_15__bases_orig );
        tmp_dircall_arg1_15 = tmp_class_creation_15__bases_orig;
        Py_INCREF( tmp_dircall_arg1_15 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_15};
            tmp_assign_source_128 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_128 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_45;
        }
        assert( tmp_class_creation_15__bases == NULL );
        tmp_class_creation_15__bases = tmp_assign_source_128;
    }
    {
        PyObject *tmp_assign_source_129;
        tmp_assign_source_129 = PyDict_New();
        assert( tmp_class_creation_15__class_decl_dict == NULL );
        tmp_class_creation_15__class_decl_dict = tmp_assign_source_129;
    }
    {
        PyObject *tmp_assign_source_130;
        PyObject *tmp_metaclass_name_15;
        nuitka_bool tmp_condition_result_92;
        PyObject *tmp_key_name_43;
        PyObject *tmp_dict_name_43;
        PyObject *tmp_dict_name_44;
        PyObject *tmp_key_name_44;
        nuitka_bool tmp_condition_result_93;
        int tmp_truth_name_15;
        PyObject *tmp_type_arg_29;
        PyObject *tmp_subscribed_name_15;
        PyObject *tmp_subscript_name_15;
        PyObject *tmp_bases_name_15;
        tmp_key_name_43 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_15__class_decl_dict );
        tmp_dict_name_43 = tmp_class_creation_15__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_43, tmp_key_name_43 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_45;
        }
        tmp_condition_result_92 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_92 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_36;
        }
        else
        {
            goto condexpr_false_36;
        }
        condexpr_true_36:;
        CHECK_OBJECT( tmp_class_creation_15__class_decl_dict );
        tmp_dict_name_44 = tmp_class_creation_15__class_decl_dict;
        tmp_key_name_44 = const_str_plain_metaclass;
        tmp_metaclass_name_15 = DICT_GET_ITEM( tmp_dict_name_44, tmp_key_name_44 );
        if ( tmp_metaclass_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_45;
        }
        goto condexpr_end_36;
        condexpr_false_36:;
        CHECK_OBJECT( tmp_class_creation_15__bases );
        tmp_truth_name_15 = CHECK_IF_TRUE( tmp_class_creation_15__bases );
        if ( tmp_truth_name_15 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_45;
        }
        tmp_condition_result_93 = tmp_truth_name_15 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_93 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_37;
        }
        else
        {
            goto condexpr_false_37;
        }
        condexpr_true_37:;
        CHECK_OBJECT( tmp_class_creation_15__bases );
        tmp_subscribed_name_15 = tmp_class_creation_15__bases;
        tmp_subscript_name_15 = const_int_0;
        tmp_type_arg_29 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_15, tmp_subscript_name_15, 0 );
        if ( tmp_type_arg_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_45;
        }
        tmp_metaclass_name_15 = BUILTIN_TYPE1( tmp_type_arg_29 );
        Py_DECREF( tmp_type_arg_29 );
        if ( tmp_metaclass_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_45;
        }
        goto condexpr_end_37;
        condexpr_false_37:;
        tmp_metaclass_name_15 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_15 );
        condexpr_end_37:;
        condexpr_end_36:;
        CHECK_OBJECT( tmp_class_creation_15__bases );
        tmp_bases_name_15 = tmp_class_creation_15__bases;
        tmp_assign_source_130 = SELECT_METACLASS( tmp_metaclass_name_15, tmp_bases_name_15 );
        Py_DECREF( tmp_metaclass_name_15 );
        if ( tmp_assign_source_130 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_45;
        }
        assert( tmp_class_creation_15__metaclass == NULL );
        tmp_class_creation_15__metaclass = tmp_assign_source_130;
    }
    {
        nuitka_bool tmp_condition_result_94;
        PyObject *tmp_key_name_45;
        PyObject *tmp_dict_name_45;
        tmp_key_name_45 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_15__class_decl_dict );
        tmp_dict_name_45 = tmp_class_creation_15__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_45, tmp_key_name_45 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_45;
        }
        tmp_condition_result_94 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_94 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_57;
        }
        else
        {
            goto branch_no_57;
        }
        branch_yes_57:;
        CHECK_OBJECT( tmp_class_creation_15__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_15__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_45;
        }
        branch_no_57:;
    }
    {
        nuitka_bool tmp_condition_result_95;
        PyObject *tmp_source_name_57;
        CHECK_OBJECT( tmp_class_creation_15__metaclass );
        tmp_source_name_57 = tmp_class_creation_15__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_57, const_str_plain___prepare__ );
        tmp_condition_result_95 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_95 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_58;
        }
        else
        {
            goto branch_no_58;
        }
        branch_yes_58:;
        {
            PyObject *tmp_assign_source_131;
            PyObject *tmp_called_name_43;
            PyObject *tmp_source_name_58;
            PyObject *tmp_args_name_29;
            PyObject *tmp_tuple_element_59;
            PyObject *tmp_kw_name_29;
            CHECK_OBJECT( tmp_class_creation_15__metaclass );
            tmp_source_name_58 = tmp_class_creation_15__metaclass;
            tmp_called_name_43 = LOOKUP_ATTRIBUTE( tmp_source_name_58, const_str_plain___prepare__ );
            if ( tmp_called_name_43 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 182;

                goto try_except_handler_45;
            }
            tmp_tuple_element_59 = const_str_plain_NormalizedFunctionAttribute;
            tmp_args_name_29 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_59 );
            PyTuple_SET_ITEM( tmp_args_name_29, 0, tmp_tuple_element_59 );
            CHECK_OBJECT( tmp_class_creation_15__bases );
            tmp_tuple_element_59 = tmp_class_creation_15__bases;
            Py_INCREF( tmp_tuple_element_59 );
            PyTuple_SET_ITEM( tmp_args_name_29, 1, tmp_tuple_element_59 );
            CHECK_OBJECT( tmp_class_creation_15__class_decl_dict );
            tmp_kw_name_29 = tmp_class_creation_15__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 182;
            tmp_assign_source_131 = CALL_FUNCTION( tmp_called_name_43, tmp_args_name_29, tmp_kw_name_29 );
            Py_DECREF( tmp_called_name_43 );
            Py_DECREF( tmp_args_name_29 );
            if ( tmp_assign_source_131 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 182;

                goto try_except_handler_45;
            }
            assert( tmp_class_creation_15__prepared == NULL );
            tmp_class_creation_15__prepared = tmp_assign_source_131;
        }
        {
            nuitka_bool tmp_condition_result_96;
            PyObject *tmp_operand_name_15;
            PyObject *tmp_source_name_59;
            CHECK_OBJECT( tmp_class_creation_15__prepared );
            tmp_source_name_59 = tmp_class_creation_15__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_59, const_str_plain___getitem__ );
            tmp_operand_name_15 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_15 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 182;

                goto try_except_handler_45;
            }
            tmp_condition_result_96 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_96 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_59;
            }
            else
            {
                goto branch_no_59;
            }
            branch_yes_59:;
            {
                PyObject *tmp_raise_type_15;
                PyObject *tmp_raise_value_15;
                PyObject *tmp_left_name_15;
                PyObject *tmp_right_name_15;
                PyObject *tmp_tuple_element_60;
                PyObject *tmp_getattr_target_15;
                PyObject *tmp_getattr_attr_15;
                PyObject *tmp_getattr_default_15;
                PyObject *tmp_source_name_60;
                PyObject *tmp_type_arg_30;
                tmp_raise_type_15 = PyExc_TypeError;
                tmp_left_name_15 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_15__metaclass );
                tmp_getattr_target_15 = tmp_class_creation_15__metaclass;
                tmp_getattr_attr_15 = const_str_plain___name__;
                tmp_getattr_default_15 = const_str_angle_metaclass;
                tmp_tuple_element_60 = BUILTIN_GETATTR( tmp_getattr_target_15, tmp_getattr_attr_15, tmp_getattr_default_15 );
                if ( tmp_tuple_element_60 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 182;

                    goto try_except_handler_45;
                }
                tmp_right_name_15 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_15, 0, tmp_tuple_element_60 );
                CHECK_OBJECT( tmp_class_creation_15__prepared );
                tmp_type_arg_30 = tmp_class_creation_15__prepared;
                tmp_source_name_60 = BUILTIN_TYPE1( tmp_type_arg_30 );
                assert( !(tmp_source_name_60 == NULL) );
                tmp_tuple_element_60 = LOOKUP_ATTRIBUTE( tmp_source_name_60, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_60 );
                if ( tmp_tuple_element_60 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_15 );

                    exception_lineno = 182;

                    goto try_except_handler_45;
                }
                PyTuple_SET_ITEM( tmp_right_name_15, 1, tmp_tuple_element_60 );
                tmp_raise_value_15 = BINARY_OPERATION_REMAINDER( tmp_left_name_15, tmp_right_name_15 );
                Py_DECREF( tmp_right_name_15 );
                if ( tmp_raise_value_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 182;

                    goto try_except_handler_45;
                }
                exception_type = tmp_raise_type_15;
                Py_INCREF( tmp_raise_type_15 );
                exception_value = tmp_raise_value_15;
                exception_lineno = 182;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_45;
            }
            branch_no_59:;
        }
        goto branch_end_58;
        branch_no_58:;
        {
            PyObject *tmp_assign_source_132;
            tmp_assign_source_132 = PyDict_New();
            assert( tmp_class_creation_15__prepared == NULL );
            tmp_class_creation_15__prepared = tmp_assign_source_132;
        }
        branch_end_58:;
    }
    {
        PyObject *tmp_assign_source_133;
        {
            PyObject *tmp_set_locals_15;
            CHECK_OBJECT( tmp_class_creation_15__prepared );
            tmp_set_locals_15 = tmp_class_creation_15__prepared;
            locals_yargy$interpretation$attribute_182 = tmp_set_locals_15;
            Py_INCREF( tmp_set_locals_15 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_c77a64efe299a87609aa4b56e43af14a;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_182, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_47;
        }
        tmp_dictset_value = const_str_plain_NormalizedFunctionAttribute;
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_182, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 182;

            goto try_except_handler_47;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_5fe3f5462368f492373f952d5ca849b1_13, codeobj_5fe3f5462368f492373f952d5ca849b1, module_yargy$interpretation$attribute, sizeof(void *) );
        frame_5fe3f5462368f492373f952d5ca849b1_13 = cache_frame_5fe3f5462368f492373f952d5ca849b1_13;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_5fe3f5462368f492373f952d5ca849b1_13 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_5fe3f5462368f492373f952d5ca849b1_13 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = LIST_COPY( const_list_str_plain_attribute_str_plain_function_list );
        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_182, const_str_plain___attributes__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_2 = "o";
            goto frame_exception_exit_13;
        }
        tmp_dictset_value = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_28___init__(  );



        tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_182, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_2 = "o";
            goto frame_exception_exit_13;
        }
        {
            nuitka_bool tmp_condition_result_97;
            PyObject *tmp_called_name_44;
            PyObject *tmp_args_element_name_15;
            PyObject *tmp_called_name_45;
            PyObject *tmp_args_element_name_16;
            tmp_res = MAPPING_HAS_ITEM( locals_yargy$interpretation$attribute_182, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_13;
            }
            tmp_condition_result_97 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_97 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_38;
            }
            else
            {
                goto condexpr_false_38;
            }
            condexpr_true_38:;
            tmp_called_name_44 = PyObject_GetItem( locals_yargy$interpretation$attribute_182, const_str_plain_property );

            if ( tmp_called_name_44 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_13;
            }

            if ( tmp_called_name_44 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_13;
            }
            tmp_args_element_name_15 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_29_normalizer(  );



            frame_5fe3f5462368f492373f952d5ca849b1_13->m_frame.f_lineno = 189;
            {
                PyObject *call_args[] = { tmp_args_element_name_15 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_44, call_args );
            }

            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_args_element_name_15 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_13;
            }
            goto condexpr_end_38;
            condexpr_false_38:;
            tmp_called_name_45 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_16 = MAKE_FUNCTION_yargy$interpretation$attribute$$$function_29_normalizer(  );



            frame_5fe3f5462368f492373f952d5ca849b1_13->m_frame.f_lineno = 189;
            {
                PyObject *call_args[] = { tmp_args_element_name_16 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_45, call_args );
            }

            Py_DECREF( tmp_args_element_name_16 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_13;
            }
            condexpr_end_38:;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_182, const_str_plain_normalizer, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_13;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_5fe3f5462368f492373f952d5ca849b1_13 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_12;

        frame_exception_exit_13:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_5fe3f5462368f492373f952d5ca849b1_13 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_5fe3f5462368f492373f952d5ca849b1_13, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_5fe3f5462368f492373f952d5ca849b1_13->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_5fe3f5462368f492373f952d5ca849b1_13, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_5fe3f5462368f492373f952d5ca849b1_13,
            type_description_2,
            outline_14_var___class__
        );


        // Release cached frame.
        if ( frame_5fe3f5462368f492373f952d5ca849b1_13 == cache_frame_5fe3f5462368f492373f952d5ca849b1_13 )
        {
            Py_DECREF( frame_5fe3f5462368f492373f952d5ca849b1_13 );
        }
        cache_frame_5fe3f5462368f492373f952d5ca849b1_13 = NULL;

        assertFrameObject( frame_5fe3f5462368f492373f952d5ca849b1_13 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_12;

        frame_no_exception_12:;
        goto skip_nested_handling_12;
        nested_frame_exit_12:;

        goto try_except_handler_47;
        skip_nested_handling_12:;
        {
            nuitka_bool tmp_condition_result_98;
            PyObject *tmp_compexpr_left_15;
            PyObject *tmp_compexpr_right_15;
            CHECK_OBJECT( tmp_class_creation_15__bases );
            tmp_compexpr_left_15 = tmp_class_creation_15__bases;
            CHECK_OBJECT( tmp_class_creation_15__bases_orig );
            tmp_compexpr_right_15 = tmp_class_creation_15__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_15, tmp_compexpr_right_15 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 182;

                goto try_except_handler_47;
            }
            tmp_condition_result_98 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_98 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_60;
            }
            else
            {
                goto branch_no_60;
            }
            branch_yes_60:;
            CHECK_OBJECT( tmp_class_creation_15__bases_orig );
            tmp_dictset_value = tmp_class_creation_15__bases_orig;
            tmp_res = PyObject_SetItem( locals_yargy$interpretation$attribute_182, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 182;

                goto try_except_handler_47;
            }
            branch_no_60:;
        }
        {
            PyObject *tmp_assign_source_134;
            PyObject *tmp_called_name_46;
            PyObject *tmp_args_name_30;
            PyObject *tmp_tuple_element_61;
            PyObject *tmp_kw_name_30;
            CHECK_OBJECT( tmp_class_creation_15__metaclass );
            tmp_called_name_46 = tmp_class_creation_15__metaclass;
            tmp_tuple_element_61 = const_str_plain_NormalizedFunctionAttribute;
            tmp_args_name_30 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_61 );
            PyTuple_SET_ITEM( tmp_args_name_30, 0, tmp_tuple_element_61 );
            CHECK_OBJECT( tmp_class_creation_15__bases );
            tmp_tuple_element_61 = tmp_class_creation_15__bases;
            Py_INCREF( tmp_tuple_element_61 );
            PyTuple_SET_ITEM( tmp_args_name_30, 1, tmp_tuple_element_61 );
            tmp_tuple_element_61 = locals_yargy$interpretation$attribute_182;
            Py_INCREF( tmp_tuple_element_61 );
            PyTuple_SET_ITEM( tmp_args_name_30, 2, tmp_tuple_element_61 );
            CHECK_OBJECT( tmp_class_creation_15__class_decl_dict );
            tmp_kw_name_30 = tmp_class_creation_15__class_decl_dict;
            frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame.f_lineno = 182;
            tmp_assign_source_134 = CALL_FUNCTION( tmp_called_name_46, tmp_args_name_30, tmp_kw_name_30 );
            Py_DECREF( tmp_args_name_30 );
            if ( tmp_assign_source_134 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 182;

                goto try_except_handler_47;
            }
            assert( outline_14_var___class__ == NULL );
            outline_14_var___class__ = tmp_assign_source_134;
        }
        CHECK_OBJECT( outline_14_var___class__ );
        tmp_assign_source_133 = outline_14_var___class__;
        Py_INCREF( tmp_assign_source_133 );
        goto try_return_handler_47;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_47:;
        Py_DECREF( locals_yargy$interpretation$attribute_182 );
        locals_yargy$interpretation$attribute_182 = NULL;
        goto try_return_handler_46;
        // Exception handler code:
        try_except_handler_47:;
        exception_keeper_type_45 = exception_type;
        exception_keeper_value_45 = exception_value;
        exception_keeper_tb_45 = exception_tb;
        exception_keeper_lineno_45 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_yargy$interpretation$attribute_182 );
        locals_yargy$interpretation$attribute_182 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_45;
        exception_value = exception_keeper_value_45;
        exception_tb = exception_keeper_tb_45;
        exception_lineno = exception_keeper_lineno_45;

        goto try_except_handler_46;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_46:;
        CHECK_OBJECT( (PyObject *)outline_14_var___class__ );
        Py_DECREF( outline_14_var___class__ );
        outline_14_var___class__ = NULL;

        goto outline_result_15;
        // Exception handler code:
        try_except_handler_46:;
        exception_keeper_type_46 = exception_type;
        exception_keeper_value_46 = exception_value;
        exception_keeper_tb_46 = exception_tb;
        exception_keeper_lineno_46 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_46;
        exception_value = exception_keeper_value_46;
        exception_tb = exception_keeper_tb_46;
        exception_lineno = exception_keeper_lineno_46;

        goto outline_exception_15;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( yargy$interpretation$attribute );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_15:;
        exception_lineno = 182;
        goto try_except_handler_45;
        outline_result_15:;
        UPDATE_STRING_DICT1( moduledict_yargy$interpretation$attribute, (Nuitka_StringObject *)const_str_plain_NormalizedFunctionAttribute, tmp_assign_source_133 );
    }
    goto try_end_17;
    // Exception handler code:
    try_except_handler_45:;
    exception_keeper_type_47 = exception_type;
    exception_keeper_value_47 = exception_value;
    exception_keeper_tb_47 = exception_tb;
    exception_keeper_lineno_47 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_15__bases_orig );
    tmp_class_creation_15__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_15__bases );
    tmp_class_creation_15__bases = NULL;

    Py_XDECREF( tmp_class_creation_15__class_decl_dict );
    tmp_class_creation_15__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_15__metaclass );
    tmp_class_creation_15__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_15__prepared );
    tmp_class_creation_15__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_47;
    exception_value = exception_keeper_value_47;
    exception_tb = exception_keeper_tb_47;
    exception_lineno = exception_keeper_lineno_47;

    goto frame_exception_exit_1;
    // End of try:
    try_end_17:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7fd3f0c5b319b6e5d2adc1a0902611ec );
#endif
    popFrameStack();

    assertFrameObject( frame_7fd3f0c5b319b6e5d2adc1a0902611ec );

    goto frame_no_exception_13;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7fd3f0c5b319b6e5d2adc1a0902611ec );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7fd3f0c5b319b6e5d2adc1a0902611ec, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7fd3f0c5b319b6e5d2adc1a0902611ec->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7fd3f0c5b319b6e5d2adc1a0902611ec, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_13:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_15__bases_orig );
    Py_DECREF( tmp_class_creation_15__bases_orig );
    tmp_class_creation_15__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_15__bases );
    Py_DECREF( tmp_class_creation_15__bases );
    tmp_class_creation_15__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_15__class_decl_dict );
    Py_DECREF( tmp_class_creation_15__class_decl_dict );
    tmp_class_creation_15__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_15__metaclass );
    Py_DECREF( tmp_class_creation_15__metaclass );
    tmp_class_creation_15__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_15__prepared );
    Py_DECREF( tmp_class_creation_15__prepared );
    tmp_class_creation_15__prepared = NULL;


    return MOD_RETURN_VALUE( module_yargy$interpretation$attribute );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
