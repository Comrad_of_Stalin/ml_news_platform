/* Generated code for Python module 'prompt_toolkit.key_binding.bindings.scroll'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$key_binding$bindings$scroll" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$key_binding$bindings$scroll;
PyDictObject *moduledict_prompt_toolkit$key_binding$bindings$scroll;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_56293838bc172e7cbbdc9cc4ac6ee031;
extern PyObject *const_str_plain_scroll_page_down;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_plain_scroll_one_line_down;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_plain_event_tuple;
extern PyObject *const_str_plain_get_cursor_up_position;
static PyObject *const_str_digest_ea2b3ee05cacebccd17a172dcf1abca2;
extern PyObject *const_str_plain_max;
extern PyObject *const_str_plain_cursor_position_row;
static PyObject *const_str_digest_7b4e7e4044f3b9abebb3493073fcfb31;
extern PyObject *const_str_plain_line_index;
extern PyObject *const_str_plain_layout;
extern PyObject *const_str_plain_cursor_up;
static PyObject *const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple;
extern PyObject *const_str_plain_content_height;
extern PyObject *const_str_plain_vertical_scroll;
extern PyObject *const_str_plain___doc__;
static PyObject *const_dict_75d1a0ed0097226a6220b8f2f198e56f;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_scroll_half_page_down;
static PyObject *const_str_digest_a9e4c593e232b85d97ace15712dd3bb6;
extern PyObject *const_str_plain_scroll_half_page_up;
extern PyObject *const_str_plain_min;
extern PyObject *const_str_plain_w;
static PyObject *const_str_digest_30f03b59873a72e9a1c937a629f87f0a;
static PyObject *const_str_digest_e93e4262c3642733216f0af565178081;
extern PyObject *const_tuple_str_plain_range_tuple;
extern PyObject *const_str_plain_scroll_page_up;
extern PyObject *const_str_plain_height;
extern PyObject *const_str_plain_current_window;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_event;
extern PyObject *const_str_plain_configured_scroll_offsets;
extern PyObject *const_str_plain_top;
static PyObject *const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_info_tuple;
extern PyObject *const_str_plain_document;
static PyObject *const_str_digest_225295a71081e4a35f26630bbeddad38;
extern PyObject *const_str_plain_after_whitespace;
extern PyObject *const_str_plain_scroll_forward;
extern PyObject *const_str_plain_render_info;
extern PyObject *const_str_plain_b;
extern PyObject *const_str_plain_line_count;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_half;
extern PyObject *const_str_plain___all__;
extern PyObject *const_int_0;
static PyObject *const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_line_index_tuple;
extern PyObject *const_str_plain_cursor_position;
static PyObject *const_tuple_b5c4fae3eb02bf8402a88fedf786b15f_tuple;
static PyObject *const_str_digest_a03d439dce14a9dcc719494a0f81a490;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_scroll_one_line_up;
extern PyObject *const_str_plain_translate_row_col_to_index;
extern PyObject *const_str_plain_ui_content;
extern PyObject *const_str_plain_app;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_current_buffer;
extern PyObject *const_str_plain__;
extern PyObject *const_str_plain_unicode_literals;
static PyObject *const_str_digest_5d5e8a5ec86a06aac0e040ae6c4d5bf4;
extern PyObject *const_str_plain_range;
extern PyObject *const_str_plain_bottom;
extern PyObject *const_int_pos_1;
extern PyObject *const_dict_5b4e82b0d7f6ce57036ec2983d8d8c39;
extern PyObject *const_str_plain_y;
static PyObject *const_str_digest_d5d4264b07e30d089a394000b53ea80a;
extern PyObject *const_str_plain_scroll_backward;
extern PyObject *const_str_plain_last_visible_line;
extern PyObject *const_str_plain_line_height;
static PyObject *const_str_plain_first_line_height;
static PyObject *const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple;
extern PyObject *const_str_plain_first_visible_line;
extern PyObject *const_str_digest_d8d4f5e440c7f01129293c18f6555af5;
static PyObject *const_list_5fa4e776f6142d4bfc7525ffaa14f99b_list;
extern PyObject *const_str_plain_get_height_for_line;
extern PyObject *const_str_plain_window_height;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_edfd9fa801e0528978d8f602452058f4;
extern PyObject *const_int_pos_2;
static PyObject *const_str_digest_bf29d61e3e87f44f3bdf8fa4bbac7169;
extern PyObject *const_str_plain_get_start_of_line_position;
extern PyObject *const_str_plain_info;
extern PyObject *const_str_plain_get_cursor_down_position;
extern PyObject *const_str_plain_scroll_height;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_56293838bc172e7cbbdc9cc4ac6ee031 = UNSTREAM_STRING_ASCII( &constant_bin[ 4689577 ], 29, 0 );
    const_str_digest_ea2b3ee05cacebccd17a172dcf1abca2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4689606 ], 28, 0 );
    const_str_digest_7b4e7e4044f3b9abebb3493073fcfb31 = UNSTREAM_STRING_ASCII( &constant_bin[ 4689634 ], 27, 0 );
    const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple, 1, const_str_plain_half ); Py_INCREF( const_str_plain_half );
    PyTuple_SET_ITEM( const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple, 2, const_str_plain_w ); Py_INCREF( const_str_plain_w );
    PyTuple_SET_ITEM( const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple, 3, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple, 4, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    PyTuple_SET_ITEM( const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple, 5, const_str_plain_scroll_height ); Py_INCREF( const_str_plain_scroll_height );
    PyTuple_SET_ITEM( const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple, 6, const_str_plain_y ); Py_INCREF( const_str_plain_y );
    PyTuple_SET_ITEM( const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple, 7, const_str_plain_height ); Py_INCREF( const_str_plain_height );
    PyTuple_SET_ITEM( const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple, 8, const_str_plain_line_height ); Py_INCREF( const_str_plain_line_height );
    const_dict_75d1a0ed0097226a6220b8f2f198e56f = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_75d1a0ed0097226a6220b8f2f198e56f, const_str_plain_half, Py_True );
    assert( PyDict_Size( const_dict_75d1a0ed0097226a6220b8f2f198e56f ) == 1 );
    const_str_digest_a9e4c593e232b85d97ace15712dd3bb6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4689661 ], 28, 0 );
    const_str_digest_30f03b59873a72e9a1c937a629f87f0a = UNSTREAM_STRING_ASCII( &constant_bin[ 4689689 ], 89, 0 );
    const_str_digest_e93e4262c3642733216f0af565178081 = UNSTREAM_STRING_ASCII( &constant_bin[ 4689778 ], 45, 0 );
    const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_info_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_info_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_info_tuple, 1, const_str_plain_w ); Py_INCREF( const_str_plain_w );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_info_tuple, 2, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_info_tuple, 3, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    const_str_digest_225295a71081e4a35f26630bbeddad38 = UNSTREAM_STRING_ASCII( &constant_bin[ 4689823 ], 51, 0 );
    const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_line_index_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_line_index_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_line_index_tuple, 1, const_str_plain_w ); Py_INCREF( const_str_plain_w );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_line_index_tuple, 2, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_line_index_tuple, 3, const_str_plain_line_index ); Py_INCREF( const_str_plain_line_index );
    const_tuple_b5c4fae3eb02bf8402a88fedf786b15f_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_b5c4fae3eb02bf8402a88fedf786b15f_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_b5c4fae3eb02bf8402a88fedf786b15f_tuple, 1, const_str_plain_w ); Py_INCREF( const_str_plain_w );
    PyTuple_SET_ITEM( const_tuple_b5c4fae3eb02bf8402a88fedf786b15f_tuple, 2, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_b5c4fae3eb02bf8402a88fedf786b15f_tuple, 3, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    const_str_plain_first_line_height = UNSTREAM_STRING_ASCII( &constant_bin[ 4689874 ], 17, 1 );
    PyTuple_SET_ITEM( const_tuple_b5c4fae3eb02bf8402a88fedf786b15f_tuple, 4, const_str_plain_first_line_height ); Py_INCREF( const_str_plain_first_line_height );
    PyTuple_SET_ITEM( const_tuple_b5c4fae3eb02bf8402a88fedf786b15f_tuple, 5, const_str_plain_cursor_up ); Py_INCREF( const_str_plain_cursor_up );
    PyTuple_SET_ITEM( const_tuple_b5c4fae3eb02bf8402a88fedf786b15f_tuple, 6, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    const_str_digest_a03d439dce14a9dcc719494a0f81a490 = UNSTREAM_STRING_ASCII( &constant_bin[ 4689891 ], 56, 0 );
    const_str_digest_5d5e8a5ec86a06aac0e040ae6c4d5bf4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4689831 ], 42, 0 );
    const_str_digest_d5d4264b07e30d089a394000b53ea80a = UNSTREAM_STRING_ASCII( &constant_bin[ 4689947 ], 56, 0 );
    const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 1, const_str_plain_half ); Py_INCREF( const_str_plain_half );
    PyTuple_SET_ITEM( const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 2, const_str_plain_w ); Py_INCREF( const_str_plain_w );
    PyTuple_SET_ITEM( const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 3, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 4, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    PyTuple_SET_ITEM( const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 5, const_str_plain_ui_content ); Py_INCREF( const_str_plain_ui_content );
    PyTuple_SET_ITEM( const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 6, const_str_plain_scroll_height ); Py_INCREF( const_str_plain_scroll_height );
    PyTuple_SET_ITEM( const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 7, const_str_plain_y ); Py_INCREF( const_str_plain_y );
    PyTuple_SET_ITEM( const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 8, const_str_plain_height ); Py_INCREF( const_str_plain_height );
    PyTuple_SET_ITEM( const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 9, const_str_plain_line_height ); Py_INCREF( const_str_plain_line_height );
    const_list_5fa4e776f6142d4bfc7525ffaa14f99b_list = PyList_New( 6 );
    PyList_SET_ITEM( const_list_5fa4e776f6142d4bfc7525ffaa14f99b_list, 0, const_str_plain_scroll_forward ); Py_INCREF( const_str_plain_scroll_forward );
    PyList_SET_ITEM( const_list_5fa4e776f6142d4bfc7525ffaa14f99b_list, 1, const_str_plain_scroll_backward ); Py_INCREF( const_str_plain_scroll_backward );
    PyList_SET_ITEM( const_list_5fa4e776f6142d4bfc7525ffaa14f99b_list, 2, const_str_plain_scroll_half_page_up ); Py_INCREF( const_str_plain_scroll_half_page_up );
    PyList_SET_ITEM( const_list_5fa4e776f6142d4bfc7525ffaa14f99b_list, 3, const_str_plain_scroll_half_page_down ); Py_INCREF( const_str_plain_scroll_half_page_down );
    PyList_SET_ITEM( const_list_5fa4e776f6142d4bfc7525ffaa14f99b_list, 4, const_str_plain_scroll_one_line_up ); Py_INCREF( const_str_plain_scroll_one_line_up );
    PyList_SET_ITEM( const_list_5fa4e776f6142d4bfc7525ffaa14f99b_list, 5, const_str_plain_scroll_one_line_down ); Py_INCREF( const_str_plain_scroll_one_line_down );
    const_str_digest_edfd9fa801e0528978d8f602452058f4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4690003 ], 88, 0 );
    const_str_digest_bf29d61e3e87f44f3bdf8fa4bbac7169 = UNSTREAM_STRING_ASCII( &constant_bin[ 4690091 ], 223, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$key_binding$bindings$scroll( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_662c6cfe2740033a95b69a8f5f20216f;
static PyCodeObject *codeobj_04e64394cd2fb696f7c9c378ed63a5d8;
static PyCodeObject *codeobj_afc6a414b8ddbf6217d3963a00b6c5e5;
static PyCodeObject *codeobj_77c5ff425b49baa61aff4ce321c0dae9;
static PyCodeObject *codeobj_260479cd8ffaea2889526673d7a2bec9;
static PyCodeObject *codeobj_bd5ef991298b55f61823b6cc15ccd4bf;
static PyCodeObject *codeobj_bc1f4c408831c212686c10193896948f;
static PyCodeObject *codeobj_08ed0b52e395c497a7fc07da1be85749;
static PyCodeObject *codeobj_c138d1da45f3cedb4455a0fdfd582f2d;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_e93e4262c3642733216f0af565178081 );
    codeobj_662c6cfe2740033a95b69a8f5f20216f = MAKE_CODEOBJ( module_filename_obj, const_str_digest_225295a71081e4a35f26630bbeddad38, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_04e64394cd2fb696f7c9c378ed63a5d8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_scroll_backward, 52, const_tuple_46e9a868dfb27bf23cb4b3d86f60e5a3_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_afc6a414b8ddbf6217d3963a00b6c5e5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_scroll_forward, 21, const_tuple_6132414a7e6e8755d06f41a34f57d634_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_77c5ff425b49baa61aff4ce321c0dae9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_scroll_half_page_down, 82, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_260479cd8ffaea2889526673d7a2bec9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_scroll_half_page_up, 89, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_bd5ef991298b55f61823b6cc15ccd4bf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_scroll_one_line_down, 96, const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_info_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_bc1f4c408831c212686c10193896948f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_scroll_one_line_up, 115, const_tuple_b5c4fae3eb02bf8402a88fedf786b15f_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_08ed0b52e395c497a7fc07da1be85749 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_scroll_page_down, 142, const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_line_index_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c138d1da45f3cedb4455a0fdfd582f2d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_scroll_page_up, 158, const_tuple_str_plain_event_str_plain_w_str_plain_b_str_plain_line_index_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_1_scroll_forward( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_2_scroll_backward( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_3_scroll_half_page_down(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_4_scroll_half_page_up(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_5_scroll_one_line_down(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_6_scroll_one_line_up(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_7_scroll_page_down(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_8_scroll_page_up(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$key_binding$bindings$scroll$$$function_1_scroll_forward( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *par_half = python_pars[ 1 ];
    PyObject *var_w = NULL;
    PyObject *var_b = NULL;
    PyObject *var_info = NULL;
    PyObject *var_ui_content = NULL;
    PyObject *var_scroll_height = NULL;
    PyObject *var_y = NULL;
    PyObject *var_height = NULL;
    PyObject *var_line_height = NULL;
    struct Nuitka_FrameObject *frame_afc6a414b8ddbf6217d3963a00b6c5e5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_afc6a414b8ddbf6217d3963a00b6c5e5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_afc6a414b8ddbf6217d3963a00b6c5e5, codeobj_afc6a414b8ddbf6217d3963a00b6c5e5, module_prompt_toolkit$key_binding$bindings$scroll, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_afc6a414b8ddbf6217d3963a00b6c5e5 = cache_frame_afc6a414b8ddbf6217d3963a00b6c5e5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_afc6a414b8ddbf6217d3963a00b6c5e5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_afc6a414b8ddbf6217d3963a00b6c5e5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_layout );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_window );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_w == NULL );
        var_w = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_app );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_current_buffer );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_2;
        CHECK_OBJECT( var_w );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_w );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_w );
        tmp_source_name_6 = var_w;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_render_info );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 28;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( var_w );
            tmp_source_name_7 = var_w;
            tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_render_info );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 29;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_info == NULL );
            var_info = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( var_info );
            tmp_source_name_8 = var_info;
            tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_ui_content );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 30;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_ui_content == NULL );
            var_ui_content = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_source_name_9;
            CHECK_OBJECT( var_info );
            tmp_source_name_9 = var_info;
            tmp_assign_source_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_window_height );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 33;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_scroll_height == NULL );
            var_scroll_height = tmp_assign_source_5;
        }
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_truth_name_3;
            CHECK_OBJECT( par_half );
            tmp_truth_name_3 = CHECK_IF_TRUE( par_half );
            if ( tmp_truth_name_3 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                CHECK_OBJECT( var_scroll_height );
                tmp_left_name_1 = var_scroll_height;
                tmp_right_name_1 = const_int_pos_2;
                tmp_result = BINARY_OPERATION_INPLACE( PyNumber_InPlaceFloorDivide, &tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 35;
                    type_description_1 = "oooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_6 = tmp_left_name_1;
                var_scroll_height = tmp_assign_source_6;

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_left_name_2;
            PyObject *tmp_source_name_10;
            PyObject *tmp_source_name_11;
            PyObject *tmp_right_name_2;
            CHECK_OBJECT( var_b );
            tmp_source_name_11 = var_b;
            tmp_source_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_document );
            if ( tmp_source_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_cursor_position_row );
            Py_DECREF( tmp_source_name_10 );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_2 = const_int_pos_1;
            tmp_assign_source_7 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_2 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_y == NULL );
            var_y = tmp_assign_source_7;
        }
        {
            PyObject *tmp_assign_source_8;
            tmp_assign_source_8 = const_int_0;
            assert( var_height == NULL );
            Py_INCREF( tmp_assign_source_8 );
            var_height = tmp_assign_source_8;
        }
        loop_start_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_source_name_12;
            if ( var_y == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "y" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 40;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_left_1 = var_y;
            CHECK_OBJECT( var_ui_content );
            tmp_source_name_12 = var_ui_content;
            tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_line_count );
            if ( tmp_compexpr_right_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_operand_name_1 = RICH_COMPARE_LT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            Py_DECREF( tmp_compexpr_right_1 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            goto loop_end_1;
            branch_no_3:;
        }
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_13;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var_info );
            tmp_source_name_13 = var_info;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_get_height_for_line );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            if ( var_y == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "y" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 41;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_1 = var_y;
            frame_afc6a414b8ddbf6217d3963a00b6c5e5->m_frame.f_lineno = 41;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_line_height;
                var_line_height = tmp_assign_source_9;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            if ( var_height == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "height" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 43;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_left_name_3 = var_height;
            CHECK_OBJECT( var_line_height );
            tmp_right_name_3 = var_line_height;
            tmp_compexpr_left_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_scroll_height );
            tmp_compexpr_right_2 = var_scroll_height;
            tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_10;
                PyObject *tmp_left_name_4;
                PyObject *tmp_right_name_4;
                if ( var_height == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "height" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 44;
                    type_description_1 = "oooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_4 = var_height;
                CHECK_OBJECT( var_line_height );
                tmp_right_name_4 = var_line_height;
                tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_4, tmp_right_name_4 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 44;
                    type_description_1 = "oooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_10 = tmp_left_name_4;
                var_height = tmp_assign_source_10;

            }
            {
                PyObject *tmp_assign_source_11;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                if ( var_y == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "y" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 45;
                    type_description_1 = "oooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_5 = var_y;
                tmp_right_name_5 = const_int_pos_1;
                tmp_result = BINARY_OPERATION_ADD_OBJECT_LONG_INPLACE( &tmp_left_name_5, tmp_right_name_5 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 45;
                    type_description_1 = "oooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_11 = tmp_left_name_5;
                var_y = tmp_assign_source_11;

            }
            goto branch_end_4;
            branch_no_4:;
            goto loop_end_1;
            branch_end_4:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_1 = "oooooooooo";
            goto frame_exception_exit_1;
        }
        goto loop_start_1;
        loop_end_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_14;
            PyObject *tmp_source_name_15;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( var_b );
            tmp_source_name_15 = var_b;
            tmp_source_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_document );
            if ( tmp_source_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_translate_row_col_to_index );
            Py_DECREF( tmp_source_name_14 );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            if ( var_y == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "y" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 49;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_2 = var_y;
            tmp_args_element_name_3 = const_int_0;
            frame_afc6a414b8ddbf6217d3963a00b6c5e5->m_frame.f_lineno = 49;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_b );
            tmp_assattr_target_1 = var_b;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 49;
                type_description_1 = "oooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_afc6a414b8ddbf6217d3963a00b6c5e5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_afc6a414b8ddbf6217d3963a00b6c5e5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_afc6a414b8ddbf6217d3963a00b6c5e5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_afc6a414b8ddbf6217d3963a00b6c5e5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_afc6a414b8ddbf6217d3963a00b6c5e5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_afc6a414b8ddbf6217d3963a00b6c5e5,
        type_description_1,
        par_event,
        par_half,
        var_w,
        var_b,
        var_info,
        var_ui_content,
        var_scroll_height,
        var_y,
        var_height,
        var_line_height
    );


    // Release cached frame.
    if ( frame_afc6a414b8ddbf6217d3963a00b6c5e5 == cache_frame_afc6a414b8ddbf6217d3963a00b6c5e5 )
    {
        Py_DECREF( frame_afc6a414b8ddbf6217d3963a00b6c5e5 );
    }
    cache_frame_afc6a414b8ddbf6217d3963a00b6c5e5 = NULL;

    assertFrameObject( frame_afc6a414b8ddbf6217d3963a00b6c5e5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_1_scroll_forward );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)par_half );
    Py_DECREF( par_half );
    par_half = NULL;

    CHECK_OBJECT( (PyObject *)var_w );
    Py_DECREF( var_w );
    var_w = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    Py_XDECREF( var_ui_content );
    var_ui_content = NULL;

    Py_XDECREF( var_scroll_height );
    var_scroll_height = NULL;

    Py_XDECREF( var_y );
    var_y = NULL;

    Py_XDECREF( var_height );
    var_height = NULL;

    Py_XDECREF( var_line_height );
    var_line_height = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)par_half );
    Py_DECREF( par_half );
    par_half = NULL;

    Py_XDECREF( var_w );
    var_w = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    Py_XDECREF( var_ui_content );
    var_ui_content = NULL;

    Py_XDECREF( var_scroll_height );
    var_scroll_height = NULL;

    Py_XDECREF( var_y );
    var_y = NULL;

    Py_XDECREF( var_height );
    var_height = NULL;

    Py_XDECREF( var_line_height );
    var_line_height = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_1_scroll_forward );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$scroll$$$function_2_scroll_backward( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *par_half = python_pars[ 1 ];
    PyObject *var_w = NULL;
    PyObject *var_b = NULL;
    PyObject *var_info = NULL;
    PyObject *var_scroll_height = NULL;
    PyObject *var_y = NULL;
    PyObject *var_height = NULL;
    PyObject *var_line_height = NULL;
    struct Nuitka_FrameObject *frame_04e64394cd2fb696f7c9c378ed63a5d8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_04e64394cd2fb696f7c9c378ed63a5d8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_04e64394cd2fb696f7c9c378ed63a5d8, codeobj_04e64394cd2fb696f7c9c378ed63a5d8, module_prompt_toolkit$key_binding$bindings$scroll, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_04e64394cd2fb696f7c9c378ed63a5d8 = cache_frame_04e64394cd2fb696f7c9c378ed63a5d8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_04e64394cd2fb696f7c9c378ed63a5d8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_04e64394cd2fb696f7c9c378ed63a5d8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_layout );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_window );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_w == NULL );
        var_w = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_app );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_current_buffer );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 57;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_2;
        CHECK_OBJECT( var_w );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_w );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_w );
        tmp_source_name_6 = var_w;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_render_info );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 59;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( var_w );
            tmp_source_name_7 = var_w;
            tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_render_info );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_info == NULL );
            var_info = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( var_info );
            tmp_source_name_8 = var_info;
            tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_window_height );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_scroll_height == NULL );
            var_scroll_height = tmp_assign_source_4;
        }
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_truth_name_3;
            CHECK_OBJECT( par_half );
            tmp_truth_name_3 = CHECK_IF_TRUE( par_half );
            if ( tmp_truth_name_3 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                CHECK_OBJECT( var_scroll_height );
                tmp_left_name_1 = var_scroll_height;
                tmp_right_name_1 = const_int_pos_2;
                tmp_result = BINARY_OPERATION_INPLACE( PyNumber_InPlaceFloorDivide, &tmp_left_name_1, tmp_right_name_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 65;
                    type_description_1 = "ooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_5 = tmp_left_name_1;
                var_scroll_height = tmp_assign_source_5;

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_left_name_2;
            PyObject *tmp_source_name_9;
            PyObject *tmp_source_name_10;
            PyObject *tmp_right_name_2;
            tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_max );
            assert( tmp_called_name_1 != NULL );
            tmp_args_element_name_1 = const_int_0;
            CHECK_OBJECT( var_b );
            tmp_source_name_10 = var_b;
            tmp_source_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_document );
            if ( tmp_source_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_cursor_position_row );
            Py_DECREF( tmp_source_name_9 );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_2 = const_int_pos_1;
            tmp_args_element_name_2 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_2 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            frame_04e64394cd2fb696f7c9c378ed63a5d8->m_frame.f_lineno = 68;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_y == NULL );
            var_y = tmp_assign_source_6;
        }
        {
            PyObject *tmp_assign_source_7;
            tmp_assign_source_7 = const_int_0;
            assert( var_height == NULL );
            Py_INCREF( tmp_assign_source_7 );
            var_height = tmp_assign_source_7;
        }
        loop_start_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            if ( var_y == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "y" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 70;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_left_1 = var_y;
            tmp_compexpr_right_1 = const_int_0;
            tmp_operand_name_1 = RICH_COMPARE_GT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            goto loop_end_1;
            branch_no_3:;
        }
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_11;
            PyObject *tmp_args_element_name_3;
            CHECK_OBJECT( var_info );
            tmp_source_name_11 = var_info;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_get_height_for_line );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            if ( var_y == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "y" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 71;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_3 = var_y;
            frame_04e64394cd2fb696f7c9c378ed63a5d8->m_frame.f_lineno = 71;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_assign_source_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 71;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_line_height;
                var_line_height = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            if ( var_height == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "height" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 73;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_left_name_3 = var_height;
            CHECK_OBJECT( var_line_height );
            tmp_right_name_3 = var_line_height;
            tmp_compexpr_left_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 73;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_scroll_height );
            tmp_compexpr_right_2 = var_scroll_height;
            tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 73;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_left_name_4;
                PyObject *tmp_right_name_4;
                if ( var_height == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "height" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 74;
                    type_description_1 = "ooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_4 = var_height;
                CHECK_OBJECT( var_line_height );
                tmp_right_name_4 = var_line_height;
                tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_4, tmp_right_name_4 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 74;
                    type_description_1 = "ooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_9 = tmp_left_name_4;
                var_height = tmp_assign_source_9;

            }
            {
                PyObject *tmp_assign_source_10;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                if ( var_y == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "y" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 75;
                    type_description_1 = "ooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_left_name_5 = var_y;
                tmp_right_name_5 = const_int_pos_1;
                tmp_result = BINARY_OPERATION_INPLACE( PyNumber_InPlaceSubtract, &tmp_left_name_5, tmp_right_name_5 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 75;
                    type_description_1 = "ooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_10 = tmp_left_name_5;
                var_y = tmp_assign_source_10;

            }
            goto branch_end_4;
            branch_no_4:;
            goto loop_end_1;
            branch_end_4:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;
            type_description_1 = "ooooooooo";
            goto frame_exception_exit_1;
        }
        goto loop_start_1;
        loop_end_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_12;
            PyObject *tmp_source_name_13;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( var_b );
            tmp_source_name_13 = var_b;
            tmp_source_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_document );
            if ( tmp_source_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_translate_row_col_to_index );
            Py_DECREF( tmp_source_name_12 );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            if ( var_y == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "y" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 79;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_4 = var_y;
            tmp_args_element_name_5 = const_int_0;
            frame_04e64394cd2fb696f7c9c378ed63a5d8->m_frame.f_lineno = 79;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
                tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_b );
            tmp_assattr_target_1 = var_b;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_1 = "ooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_04e64394cd2fb696f7c9c378ed63a5d8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_04e64394cd2fb696f7c9c378ed63a5d8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_04e64394cd2fb696f7c9c378ed63a5d8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_04e64394cd2fb696f7c9c378ed63a5d8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_04e64394cd2fb696f7c9c378ed63a5d8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_04e64394cd2fb696f7c9c378ed63a5d8,
        type_description_1,
        par_event,
        par_half,
        var_w,
        var_b,
        var_info,
        var_scroll_height,
        var_y,
        var_height,
        var_line_height
    );


    // Release cached frame.
    if ( frame_04e64394cd2fb696f7c9c378ed63a5d8 == cache_frame_04e64394cd2fb696f7c9c378ed63a5d8 )
    {
        Py_DECREF( frame_04e64394cd2fb696f7c9c378ed63a5d8 );
    }
    cache_frame_04e64394cd2fb696f7c9c378ed63a5d8 = NULL;

    assertFrameObject( frame_04e64394cd2fb696f7c9c378ed63a5d8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_2_scroll_backward );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)par_half );
    Py_DECREF( par_half );
    par_half = NULL;

    CHECK_OBJECT( (PyObject *)var_w );
    Py_DECREF( var_w );
    var_w = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    Py_XDECREF( var_scroll_height );
    var_scroll_height = NULL;

    Py_XDECREF( var_y );
    var_y = NULL;

    Py_XDECREF( var_height );
    var_height = NULL;

    Py_XDECREF( var_line_height );
    var_line_height = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)par_half );
    Py_DECREF( par_half );
    par_half = NULL;

    Py_XDECREF( var_w );
    var_w = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    Py_XDECREF( var_scroll_height );
    var_scroll_height = NULL;

    Py_XDECREF( var_y );
    var_y = NULL;

    Py_XDECREF( var_height );
    var_height = NULL;

    Py_XDECREF( var_line_height );
    var_line_height = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_2_scroll_backward );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$scroll$$$function_3_scroll_half_page_down( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_77c5ff425b49baa61aff4ce321c0dae9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_77c5ff425b49baa61aff4ce321c0dae9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_77c5ff425b49baa61aff4ce321c0dae9, codeobj_77c5ff425b49baa61aff4ce321c0dae9, module_prompt_toolkit$key_binding$bindings$scroll, sizeof(void *) );
    frame_77c5ff425b49baa61aff4ce321c0dae9 = cache_frame_77c5ff425b49baa61aff4ce321c0dae9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_77c5ff425b49baa61aff4ce321c0dae9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_77c5ff425b49baa61aff4ce321c0dae9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_scroll_forward );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_scroll_forward );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "scroll_forward" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_event );
        tmp_tuple_element_1 = par_event;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_75d1a0ed0097226a6220b8f2f198e56f );
        frame_77c5ff425b49baa61aff4ce321c0dae9->m_frame.f_lineno = 86;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_77c5ff425b49baa61aff4ce321c0dae9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_77c5ff425b49baa61aff4ce321c0dae9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_77c5ff425b49baa61aff4ce321c0dae9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_77c5ff425b49baa61aff4ce321c0dae9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_77c5ff425b49baa61aff4ce321c0dae9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_77c5ff425b49baa61aff4ce321c0dae9,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_77c5ff425b49baa61aff4ce321c0dae9 == cache_frame_77c5ff425b49baa61aff4ce321c0dae9 )
    {
        Py_DECREF( frame_77c5ff425b49baa61aff4ce321c0dae9 );
    }
    cache_frame_77c5ff425b49baa61aff4ce321c0dae9 = NULL;

    assertFrameObject( frame_77c5ff425b49baa61aff4ce321c0dae9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_3_scroll_half_page_down );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_3_scroll_half_page_down );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$scroll$$$function_4_scroll_half_page_up( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_260479cd8ffaea2889526673d7a2bec9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_260479cd8ffaea2889526673d7a2bec9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_260479cd8ffaea2889526673d7a2bec9, codeobj_260479cd8ffaea2889526673d7a2bec9, module_prompt_toolkit$key_binding$bindings$scroll, sizeof(void *) );
    frame_260479cd8ffaea2889526673d7a2bec9 = cache_frame_260479cd8ffaea2889526673d7a2bec9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_260479cd8ffaea2889526673d7a2bec9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_260479cd8ffaea2889526673d7a2bec9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_scroll_backward );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_scroll_backward );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "scroll_backward" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 93;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_event );
        tmp_tuple_element_1 = par_event;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_75d1a0ed0097226a6220b8f2f198e56f );
        frame_260479cd8ffaea2889526673d7a2bec9->m_frame.f_lineno = 93;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_260479cd8ffaea2889526673d7a2bec9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_260479cd8ffaea2889526673d7a2bec9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_260479cd8ffaea2889526673d7a2bec9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_260479cd8ffaea2889526673d7a2bec9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_260479cd8ffaea2889526673d7a2bec9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_260479cd8ffaea2889526673d7a2bec9,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_260479cd8ffaea2889526673d7a2bec9 == cache_frame_260479cd8ffaea2889526673d7a2bec9 )
    {
        Py_DECREF( frame_260479cd8ffaea2889526673d7a2bec9 );
    }
    cache_frame_260479cd8ffaea2889526673d7a2bec9 = NULL;

    assertFrameObject( frame_260479cd8ffaea2889526673d7a2bec9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_4_scroll_half_page_up );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_4_scroll_half_page_up );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$scroll$$$function_5_scroll_one_line_down( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_w = NULL;
    PyObject *var_b = NULL;
    PyObject *var_info = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    PyObject *tmp_inplace_assign_attr_2__end = NULL;
    PyObject *tmp_inplace_assign_attr_2__start = NULL;
    struct Nuitka_FrameObject *frame_bd5ef991298b55f61823b6cc15ccd4bf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_bd5ef991298b55f61823b6cc15ccd4bf = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bd5ef991298b55f61823b6cc15ccd4bf, codeobj_bd5ef991298b55f61823b6cc15ccd4bf, module_prompt_toolkit$key_binding$bindings$scroll, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bd5ef991298b55f61823b6cc15ccd4bf = cache_frame_bd5ef991298b55f61823b6cc15ccd4bf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bd5ef991298b55f61823b6cc15ccd4bf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bd5ef991298b55f61823b6cc15ccd4bf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_layout );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_window );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_w == NULL );
        var_w = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_app );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_current_buffer );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_w );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_w );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_source_name_6;
            PyObject *tmp_attribute_value_1;
            int tmp_truth_name_2;
            CHECK_OBJECT( var_w );
            tmp_source_name_6 = var_w;
            tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_render_info );
            if ( tmp_attribute_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 105;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_1 );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_attribute_value_1 );

                exception_lineno = 105;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_attribute_value_1 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_source_name_7;
                CHECK_OBJECT( var_w );
                tmp_source_name_7 = var_w;
                tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_render_info );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 106;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                assert( var_info == NULL );
                var_info = tmp_assign_source_3;
            }
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_1;
                PyObject *tmp_compexpr_right_1;
                PyObject *tmp_source_name_8;
                PyObject *tmp_left_name_1;
                PyObject *tmp_source_name_9;
                PyObject *tmp_right_name_1;
                PyObject *tmp_source_name_10;
                CHECK_OBJECT( var_w );
                tmp_source_name_8 = var_w;
                tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_vertical_scroll );
                if ( tmp_compexpr_left_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 108;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_info );
                tmp_source_name_9 = var_info;
                tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_content_height );
                if ( tmp_left_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_compexpr_left_1 );

                    exception_lineno = 108;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_info );
                tmp_source_name_10 = var_info;
                tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_window_height );
                if ( tmp_right_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_compexpr_left_1 );
                    Py_DECREF( tmp_left_name_1 );

                    exception_lineno = 108;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_1 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_left_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_compexpr_right_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_compexpr_left_1 );

                    exception_lineno = 108;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
                Py_DECREF( tmp_compexpr_left_1 );
                Py_DECREF( tmp_compexpr_right_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 108;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    nuitka_bool tmp_condition_result_4;
                    PyObject *tmp_compexpr_left_2;
                    PyObject *tmp_compexpr_right_2;
                    PyObject *tmp_source_name_11;
                    PyObject *tmp_source_name_12;
                    PyObject *tmp_source_name_13;
                    PyObject *tmp_source_name_14;
                    CHECK_OBJECT( var_info );
                    tmp_source_name_12 = var_info;
                    tmp_source_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_cursor_position );
                    if ( tmp_source_name_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 109;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_y );
                    Py_DECREF( tmp_source_name_11 );
                    if ( tmp_compexpr_left_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 109;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_info );
                    tmp_source_name_14 = var_info;
                    tmp_source_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_configured_scroll_offsets );
                    if ( tmp_source_name_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_compexpr_left_2 );

                        exception_lineno = 109;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_top );
                    Py_DECREF( tmp_source_name_13 );
                    if ( tmp_compexpr_right_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_compexpr_left_2 );

                        exception_lineno = 109;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_res = RICH_COMPARE_BOOL_LTE_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                    Py_DECREF( tmp_compexpr_left_2 );
                    Py_DECREF( tmp_compexpr_right_2 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 109;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    {
                        PyObject *tmp_assign_source_4;
                        PyObject *tmp_source_name_15;
                        CHECK_OBJECT( var_b );
                        tmp_source_name_15 = var_b;
                        tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_cursor_position );
                        if ( tmp_assign_source_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 110;
                            type_description_1 = "oooo";
                            goto frame_exception_exit_1;
                        }
                        assert( tmp_inplace_assign_attr_1__start == NULL );
                        tmp_inplace_assign_attr_1__start = tmp_assign_source_4;
                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_5;
                        PyObject *tmp_left_name_2;
                        PyObject *tmp_right_name_2;
                        PyObject *tmp_called_instance_1;
                        PyObject *tmp_source_name_16;
                        CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
                        tmp_left_name_2 = tmp_inplace_assign_attr_1__start;
                        CHECK_OBJECT( var_b );
                        tmp_source_name_16 = var_b;
                        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_document );
                        if ( tmp_called_instance_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 110;
                            type_description_1 = "oooo";
                            goto try_except_handler_2;
                        }
                        frame_bd5ef991298b55f61823b6cc15ccd4bf->m_frame.f_lineno = 110;
                        tmp_right_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_cursor_down_position );
                        Py_DECREF( tmp_called_instance_1 );
                        if ( tmp_right_name_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 110;
                            type_description_1 = "oooo";
                            goto try_except_handler_2;
                        }
                        tmp_assign_source_5 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_2, tmp_right_name_2 );
                        Py_DECREF( tmp_right_name_2 );
                        if ( tmp_assign_source_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 110;
                            type_description_1 = "oooo";
                            goto try_except_handler_2;
                        }
                        assert( tmp_inplace_assign_attr_1__end == NULL );
                        tmp_inplace_assign_attr_1__end = tmp_assign_source_5;
                    }
                    // Tried code:
                    {
                        PyObject *tmp_assattr_name_1;
                        PyObject *tmp_assattr_target_1;
                        CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
                        tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
                        CHECK_OBJECT( var_b );
                        tmp_assattr_target_1 = var_b;
                        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 110;
                            type_description_1 = "oooo";
                            goto try_except_handler_3;
                        }
                    }
                    goto try_end_1;
                    // Exception handler code:
                    try_except_handler_3:;
                    exception_keeper_type_1 = exception_type;
                    exception_keeper_value_1 = exception_value;
                    exception_keeper_tb_1 = exception_tb;
                    exception_keeper_lineno_1 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
                    Py_DECREF( tmp_inplace_assign_attr_1__end );
                    tmp_inplace_assign_attr_1__end = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_1;
                    exception_value = exception_keeper_value_1;
                    exception_tb = exception_keeper_tb_1;
                    exception_lineno = exception_keeper_lineno_1;

                    goto try_except_handler_2;
                    // End of try:
                    try_end_1:;
                    goto try_end_2;
                    // Exception handler code:
                    try_except_handler_2:;
                    exception_keeper_type_2 = exception_type;
                    exception_keeper_value_2 = exception_value;
                    exception_keeper_tb_2 = exception_tb;
                    exception_keeper_lineno_2 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
                    Py_DECREF( tmp_inplace_assign_attr_1__start );
                    tmp_inplace_assign_attr_1__start = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_2;
                    exception_value = exception_keeper_value_2;
                    exception_tb = exception_keeper_tb_2;
                    exception_lineno = exception_keeper_lineno_2;

                    goto frame_exception_exit_1;
                    // End of try:
                    try_end_2:;
                    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
                    Py_DECREF( tmp_inplace_assign_attr_1__end );
                    tmp_inplace_assign_attr_1__end = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
                    Py_DECREF( tmp_inplace_assign_attr_1__start );
                    tmp_inplace_assign_attr_1__start = NULL;

                    branch_no_4:;
                }
                {
                    PyObject *tmp_assign_source_6;
                    PyObject *tmp_source_name_17;
                    CHECK_OBJECT( var_w );
                    tmp_source_name_17 = var_w;
                    tmp_assign_source_6 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_vertical_scroll );
                    if ( tmp_assign_source_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 112;
                        type_description_1 = "oooo";
                        goto frame_exception_exit_1;
                    }
                    assert( tmp_inplace_assign_attr_2__start == NULL );
                    tmp_inplace_assign_attr_2__start = tmp_assign_source_6;
                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_7;
                    PyObject *tmp_left_name_3;
                    PyObject *tmp_right_name_3;
                    CHECK_OBJECT( tmp_inplace_assign_attr_2__start );
                    tmp_left_name_3 = tmp_inplace_assign_attr_2__start;
                    tmp_right_name_3 = const_int_pos_1;
                    tmp_assign_source_7 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_3, tmp_right_name_3 );
                    if ( tmp_assign_source_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 112;
                        type_description_1 = "oooo";
                        goto try_except_handler_4;
                    }
                    assert( tmp_inplace_assign_attr_2__end == NULL );
                    tmp_inplace_assign_attr_2__end = tmp_assign_source_7;
                }
                // Tried code:
                {
                    PyObject *tmp_assattr_name_2;
                    PyObject *tmp_assattr_target_2;
                    CHECK_OBJECT( tmp_inplace_assign_attr_2__end );
                    tmp_assattr_name_2 = tmp_inplace_assign_attr_2__end;
                    CHECK_OBJECT( var_w );
                    tmp_assattr_target_2 = var_w;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_vertical_scroll, tmp_assattr_name_2 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 112;
                        type_description_1 = "oooo";
                        goto try_except_handler_5;
                    }
                }
                goto try_end_3;
                // Exception handler code:
                try_except_handler_5:;
                exception_keeper_type_3 = exception_type;
                exception_keeper_value_3 = exception_value;
                exception_keeper_tb_3 = exception_tb;
                exception_keeper_lineno_3 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__end );
                Py_DECREF( tmp_inplace_assign_attr_2__end );
                tmp_inplace_assign_attr_2__end = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_3;
                exception_value = exception_keeper_value_3;
                exception_tb = exception_keeper_tb_3;
                exception_lineno = exception_keeper_lineno_3;

                goto try_except_handler_4;
                // End of try:
                try_end_3:;
                goto try_end_4;
                // Exception handler code:
                try_except_handler_4:;
                exception_keeper_type_4 = exception_type;
                exception_keeper_value_4 = exception_value;
                exception_keeper_tb_4 = exception_tb;
                exception_keeper_lineno_4 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__start );
                Py_DECREF( tmp_inplace_assign_attr_2__start );
                tmp_inplace_assign_attr_2__start = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_4;
                exception_value = exception_keeper_value_4;
                exception_tb = exception_keeper_tb_4;
                exception_lineno = exception_keeper_lineno_4;

                goto frame_exception_exit_1;
                // End of try:
                try_end_4:;
                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__end );
                Py_DECREF( tmp_inplace_assign_attr_2__end );
                tmp_inplace_assign_attr_2__end = NULL;

                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__start );
                Py_DECREF( tmp_inplace_assign_attr_2__start );
                tmp_inplace_assign_attr_2__start = NULL;

                branch_no_3:;
            }
            branch_no_2:;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bd5ef991298b55f61823b6cc15ccd4bf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bd5ef991298b55f61823b6cc15ccd4bf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bd5ef991298b55f61823b6cc15ccd4bf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bd5ef991298b55f61823b6cc15ccd4bf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bd5ef991298b55f61823b6cc15ccd4bf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bd5ef991298b55f61823b6cc15ccd4bf,
        type_description_1,
        par_event,
        var_w,
        var_b,
        var_info
    );


    // Release cached frame.
    if ( frame_bd5ef991298b55f61823b6cc15ccd4bf == cache_frame_bd5ef991298b55f61823b6cc15ccd4bf )
    {
        Py_DECREF( frame_bd5ef991298b55f61823b6cc15ccd4bf );
    }
    cache_frame_bd5ef991298b55f61823b6cc15ccd4bf = NULL;

    assertFrameObject( frame_bd5ef991298b55f61823b6cc15ccd4bf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_5_scroll_one_line_down );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_w );
    Py_DECREF( var_w );
    var_w = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_w );
    var_w = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_5_scroll_one_line_down );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$scroll$$$function_6_scroll_one_line_up( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_w = NULL;
    PyObject *var_b = NULL;
    PyObject *var_info = NULL;
    PyObject *var_first_line_height = NULL;
    PyObject *var_cursor_up = NULL;
    PyObject *var__ = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    PyObject *tmp_inplace_assign_attr_2__end = NULL;
    PyObject *tmp_inplace_assign_attr_2__start = NULL;
    struct Nuitka_FrameObject *frame_bc1f4c408831c212686c10193896948f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_bc1f4c408831c212686c10193896948f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_bc1f4c408831c212686c10193896948f, codeobj_bc1f4c408831c212686c10193896948f, module_prompt_toolkit$key_binding$bindings$scroll, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_bc1f4c408831c212686c10193896948f = cache_frame_bc1f4c408831c212686c10193896948f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_bc1f4c408831c212686c10193896948f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_bc1f4c408831c212686c10193896948f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_layout );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_window );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_w == NULL );
        var_w = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_app );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_current_buffer );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_w );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_w );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_source_name_6;
            PyObject *tmp_attribute_value_1;
            int tmp_truth_name_2;
            CHECK_OBJECT( var_w );
            tmp_source_name_6 = var_w;
            tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_render_info );
            if ( tmp_attribute_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 124;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_1 );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_attribute_value_1 );

                exception_lineno = 124;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_attribute_value_1 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_source_name_7;
                CHECK_OBJECT( var_w );
                tmp_source_name_7 = var_w;
                tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_render_info );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 125;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_info == NULL );
                var_info = tmp_assign_source_3;
            }
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_1;
                PyObject *tmp_compexpr_right_1;
                PyObject *tmp_source_name_8;
                CHECK_OBJECT( var_w );
                tmp_source_name_8 = var_w;
                tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_vertical_scroll );
                if ( tmp_compexpr_left_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 127;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_1 = const_int_0;
                tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
                Py_DECREF( tmp_compexpr_left_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 127;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_4;
                    PyObject *tmp_called_name_1;
                    PyObject *tmp_source_name_9;
                    PyObject *tmp_args_element_name_1;
                    PyObject *tmp_called_instance_1;
                    CHECK_OBJECT( var_info );
                    tmp_source_name_9 = var_info;
                    tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_get_height_for_line );
                    if ( tmp_called_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 128;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_info );
                    tmp_called_instance_1 = var_info;
                    frame_bc1f4c408831c212686c10193896948f->m_frame.f_lineno = 128;
                    tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_first_visible_line );
                    if ( tmp_args_element_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_1 );

                        exception_lineno = 128;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_bc1f4c408831c212686c10193896948f->m_frame.f_lineno = 128;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_1 };
                        tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                    }

                    Py_DECREF( tmp_called_name_1 );
                    Py_DECREF( tmp_args_element_name_1 );
                    if ( tmp_assign_source_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 128;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_first_line_height == NULL );
                    var_first_line_height = tmp_assign_source_4;
                }
                {
                    PyObject *tmp_assign_source_5;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_source_name_10;
                    PyObject *tmp_source_name_11;
                    PyObject *tmp_right_name_1;
                    PyObject *tmp_left_name_2;
                    PyObject *tmp_left_name_3;
                    PyObject *tmp_left_name_4;
                    PyObject *tmp_source_name_12;
                    PyObject *tmp_right_name_2;
                    PyObject *tmp_right_name_3;
                    PyObject *tmp_right_name_4;
                    PyObject *tmp_source_name_13;
                    PyObject *tmp_source_name_14;
                    CHECK_OBJECT( var_info );
                    tmp_source_name_11 = var_info;
                    tmp_source_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_cursor_position );
                    if ( tmp_source_name_10 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 130;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_y );
                    Py_DECREF( tmp_source_name_10 );
                    if ( tmp_left_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 130;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_info );
                    tmp_source_name_12 = var_info;
                    tmp_left_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_window_height );
                    if ( tmp_left_name_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_left_name_1 );

                        exception_lineno = 130;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_right_name_2 = const_int_pos_1;
                    tmp_left_name_3 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_4, tmp_right_name_2 );
                    Py_DECREF( tmp_left_name_4 );
                    if ( tmp_left_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_left_name_1 );

                        exception_lineno = 130;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_first_line_height );
                    tmp_right_name_3 = var_first_line_height;
                    tmp_left_name_2 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_3 );
                    Py_DECREF( tmp_left_name_3 );
                    if ( tmp_left_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_left_name_1 );

                        exception_lineno = 130;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_info );
                    tmp_source_name_14 = var_info;
                    tmp_source_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_configured_scroll_offsets );
                    if ( tmp_source_name_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_left_name_1 );
                        Py_DECREF( tmp_left_name_2 );

                        exception_lineno = 131;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_right_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_bottom );
                    Py_DECREF( tmp_source_name_13 );
                    if ( tmp_right_name_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_left_name_1 );
                        Py_DECREF( tmp_left_name_2 );

                        exception_lineno = 131;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_right_name_1 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_4 );
                    Py_DECREF( tmp_left_name_2 );
                    Py_DECREF( tmp_right_name_4 );
                    if ( tmp_right_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_left_name_1 );

                        exception_lineno = 130;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_5 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
                    Py_DECREF( tmp_left_name_1 );
                    Py_DECREF( tmp_right_name_1 );
                    if ( tmp_assign_source_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 130;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_cursor_up == NULL );
                    var_cursor_up = tmp_assign_source_5;
                }
                {
                    PyObject *tmp_assign_source_6;
                    PyObject *tmp_iter_arg_1;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_mvar_value_1;
                    PyObject *tmp_args_element_name_2;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_args_element_name_3;
                    PyObject *tmp_args_element_name_4;
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_range );

                    if (unlikely( tmp_mvar_value_1 == NULL ))
                    {
                        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_range );
                    }

                    if ( tmp_mvar_value_1 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "range" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 135;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_2 = tmp_mvar_value_1;
                    tmp_called_name_3 = LOOKUP_BUILTIN( const_str_plain_max );
                    assert( tmp_called_name_3 != NULL );
                    tmp_args_element_name_3 = const_int_0;
                    CHECK_OBJECT( var_cursor_up );
                    tmp_args_element_name_4 = var_cursor_up;
                    frame_bc1f4c408831c212686c10193896948f->m_frame.f_lineno = 135;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                        tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
                    }

                    if ( tmp_args_element_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 135;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_bc1f4c408831c212686c10193896948f->m_frame.f_lineno = 135;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_2 };
                        tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_2 );
                    if ( tmp_iter_arg_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 135;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_6 = MAKE_ITERATOR( tmp_iter_arg_1 );
                    Py_DECREF( tmp_iter_arg_1 );
                    if ( tmp_assign_source_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 135;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( tmp_for_loop_1__for_iterator == NULL );
                    tmp_for_loop_1__for_iterator = tmp_assign_source_6;
                }
                // Tried code:
                loop_start_1:;
                {
                    PyObject *tmp_next_source_1;
                    PyObject *tmp_assign_source_7;
                    CHECK_OBJECT( tmp_for_loop_1__for_iterator );
                    tmp_next_source_1 = tmp_for_loop_1__for_iterator;
                    tmp_assign_source_7 = ITERATOR_NEXT( tmp_next_source_1 );
                    if ( tmp_assign_source_7 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_1;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "ooooooo";
                            exception_lineno = 135;
                            goto try_except_handler_2;
                        }
                    }

                    {
                        PyObject *old = tmp_for_loop_1__iter_value;
                        tmp_for_loop_1__iter_value = tmp_assign_source_7;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_8;
                    CHECK_OBJECT( tmp_for_loop_1__iter_value );
                    tmp_assign_source_8 = tmp_for_loop_1__iter_value;
                    {
                        PyObject *old = var__;
                        var__ = tmp_assign_source_8;
                        Py_INCREF( var__ );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_9;
                    PyObject *tmp_source_name_15;
                    CHECK_OBJECT( var_b );
                    tmp_source_name_15 = var_b;
                    tmp_assign_source_9 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_cursor_position );
                    if ( tmp_assign_source_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 136;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_2;
                    }
                    {
                        PyObject *old = tmp_inplace_assign_attr_1__start;
                        tmp_inplace_assign_attr_1__start = tmp_assign_source_9;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_10;
                    PyObject *tmp_left_name_5;
                    PyObject *tmp_right_name_5;
                    PyObject *tmp_called_instance_2;
                    PyObject *tmp_source_name_16;
                    CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
                    tmp_left_name_5 = tmp_inplace_assign_attr_1__start;
                    CHECK_OBJECT( var_b );
                    tmp_source_name_16 = var_b;
                    tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_document );
                    if ( tmp_called_instance_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 136;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_3;
                    }
                    frame_bc1f4c408831c212686c10193896948f->m_frame.f_lineno = 136;
                    tmp_right_name_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_cursor_up_position );
                    Py_DECREF( tmp_called_instance_2 );
                    if ( tmp_right_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 136;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_assign_source_10 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_5, tmp_right_name_5 );
                    Py_DECREF( tmp_right_name_5 );
                    if ( tmp_assign_source_10 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 136;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_3;
                    }
                    {
                        PyObject *old = tmp_inplace_assign_attr_1__end;
                        tmp_inplace_assign_attr_1__end = tmp_assign_source_10;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                {
                    PyObject *tmp_assattr_name_1;
                    PyObject *tmp_assattr_target_1;
                    CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
                    tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
                    CHECK_OBJECT( var_b );
                    tmp_assattr_target_1 = var_b;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 136;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_4;
                    }
                }
                goto try_end_1;
                // Exception handler code:
                try_except_handler_4:;
                exception_keeper_type_1 = exception_type;
                exception_keeper_value_1 = exception_value;
                exception_keeper_tb_1 = exception_tb;
                exception_keeper_lineno_1 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
                Py_DECREF( tmp_inplace_assign_attr_1__end );
                tmp_inplace_assign_attr_1__end = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_1;
                exception_value = exception_keeper_value_1;
                exception_tb = exception_keeper_tb_1;
                exception_lineno = exception_keeper_lineno_1;

                goto try_except_handler_3;
                // End of try:
                try_end_1:;
                goto try_end_2;
                // Exception handler code:
                try_except_handler_3:;
                exception_keeper_type_2 = exception_type;
                exception_keeper_value_2 = exception_value;
                exception_keeper_tb_2 = exception_tb;
                exception_keeper_lineno_2 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
                Py_DECREF( tmp_inplace_assign_attr_1__start );
                tmp_inplace_assign_attr_1__start = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_2;
                exception_value = exception_keeper_value_2;
                exception_tb = exception_keeper_tb_2;
                exception_lineno = exception_keeper_lineno_2;

                goto try_except_handler_2;
                // End of try:
                try_end_2:;
                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
                Py_DECREF( tmp_inplace_assign_attr_1__end );
                tmp_inplace_assign_attr_1__end = NULL;

                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
                Py_DECREF( tmp_inplace_assign_attr_1__start );
                tmp_inplace_assign_attr_1__start = NULL;

                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 135;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_2;
                }
                goto loop_start_1;
                loop_end_1:;
                goto try_end_3;
                // Exception handler code:
                try_except_handler_2:;
                exception_keeper_type_3 = exception_type;
                exception_keeper_value_3 = exception_value;
                exception_keeper_tb_3 = exception_tb;
                exception_keeper_lineno_3 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_for_loop_1__iter_value );
                tmp_for_loop_1__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                Py_DECREF( tmp_for_loop_1__for_iterator );
                tmp_for_loop_1__for_iterator = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_3;
                exception_value = exception_keeper_value_3;
                exception_tb = exception_keeper_tb_3;
                exception_lineno = exception_keeper_lineno_3;

                goto frame_exception_exit_1;
                // End of try:
                try_end_3:;
                Py_XDECREF( tmp_for_loop_1__iter_value );
                tmp_for_loop_1__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                Py_DECREF( tmp_for_loop_1__for_iterator );
                tmp_for_loop_1__for_iterator = NULL;

                {
                    PyObject *tmp_assign_source_11;
                    PyObject *tmp_source_name_17;
                    CHECK_OBJECT( var_w );
                    tmp_source_name_17 = var_w;
                    tmp_assign_source_11 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_vertical_scroll );
                    if ( tmp_assign_source_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 139;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( tmp_inplace_assign_attr_2__start == NULL );
                    tmp_inplace_assign_attr_2__start = tmp_assign_source_11;
                }
                // Tried code:
                {
                    PyObject *tmp_assign_source_12;
                    PyObject *tmp_left_name_6;
                    PyObject *tmp_right_name_6;
                    CHECK_OBJECT( tmp_inplace_assign_attr_2__start );
                    tmp_left_name_6 = tmp_inplace_assign_attr_2__start;
                    tmp_right_name_6 = const_int_pos_1;
                    tmp_assign_source_12 = BINARY_OPERATION( PyNumber_InPlaceSubtract, tmp_left_name_6, tmp_right_name_6 );
                    if ( tmp_assign_source_12 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 139;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_5;
                    }
                    assert( tmp_inplace_assign_attr_2__end == NULL );
                    tmp_inplace_assign_attr_2__end = tmp_assign_source_12;
                }
                // Tried code:
                {
                    PyObject *tmp_assattr_name_2;
                    PyObject *tmp_assattr_target_2;
                    CHECK_OBJECT( tmp_inplace_assign_attr_2__end );
                    tmp_assattr_name_2 = tmp_inplace_assign_attr_2__end;
                    CHECK_OBJECT( var_w );
                    tmp_assattr_target_2 = var_w;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_vertical_scroll, tmp_assattr_name_2 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 139;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_6;
                    }
                }
                goto try_end_4;
                // Exception handler code:
                try_except_handler_6:;
                exception_keeper_type_4 = exception_type;
                exception_keeper_value_4 = exception_value;
                exception_keeper_tb_4 = exception_tb;
                exception_keeper_lineno_4 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__end );
                Py_DECREF( tmp_inplace_assign_attr_2__end );
                tmp_inplace_assign_attr_2__end = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_4;
                exception_value = exception_keeper_value_4;
                exception_tb = exception_keeper_tb_4;
                exception_lineno = exception_keeper_lineno_4;

                goto try_except_handler_5;
                // End of try:
                try_end_4:;
                goto try_end_5;
                // Exception handler code:
                try_except_handler_5:;
                exception_keeper_type_5 = exception_type;
                exception_keeper_value_5 = exception_value;
                exception_keeper_tb_5 = exception_tb;
                exception_keeper_lineno_5 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__start );
                Py_DECREF( tmp_inplace_assign_attr_2__start );
                tmp_inplace_assign_attr_2__start = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_5;
                exception_value = exception_keeper_value_5;
                exception_tb = exception_keeper_tb_5;
                exception_lineno = exception_keeper_lineno_5;

                goto frame_exception_exit_1;
                // End of try:
                try_end_5:;
                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__end );
                Py_DECREF( tmp_inplace_assign_attr_2__end );
                tmp_inplace_assign_attr_2__end = NULL;

                CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__start );
                Py_DECREF( tmp_inplace_assign_attr_2__start );
                tmp_inplace_assign_attr_2__start = NULL;

                branch_no_3:;
            }
            branch_no_2:;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc1f4c408831c212686c10193896948f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_bc1f4c408831c212686c10193896948f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_bc1f4c408831c212686c10193896948f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_bc1f4c408831c212686c10193896948f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_bc1f4c408831c212686c10193896948f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_bc1f4c408831c212686c10193896948f,
        type_description_1,
        par_event,
        var_w,
        var_b,
        var_info,
        var_first_line_height,
        var_cursor_up,
        var__
    );


    // Release cached frame.
    if ( frame_bc1f4c408831c212686c10193896948f == cache_frame_bc1f4c408831c212686c10193896948f )
    {
        Py_DECREF( frame_bc1f4c408831c212686c10193896948f );
    }
    cache_frame_bc1f4c408831c212686c10193896948f = NULL;

    assertFrameObject( frame_bc1f4c408831c212686c10193896948f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_6_scroll_one_line_up );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_w );
    Py_DECREF( var_w );
    var_w = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    Py_XDECREF( var_first_line_height );
    var_first_line_height = NULL;

    Py_XDECREF( var_cursor_up );
    var_cursor_up = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_w );
    var_w = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    Py_XDECREF( var_first_line_height );
    var_first_line_height = NULL;

    Py_XDECREF( var_cursor_up );
    var_cursor_up = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_6_scroll_one_line_up );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$scroll$$$function_7_scroll_page_down( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_w = NULL;
    PyObject *var_b = NULL;
    PyObject *var_line_index = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_08ed0b52e395c497a7fc07da1be85749;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_08ed0b52e395c497a7fc07da1be85749 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_08ed0b52e395c497a7fc07da1be85749, codeobj_08ed0b52e395c497a7fc07da1be85749, module_prompt_toolkit$key_binding$bindings$scroll, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_08ed0b52e395c497a7fc07da1be85749 = cache_frame_08ed0b52e395c497a7fc07da1be85749;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_08ed0b52e395c497a7fc07da1be85749 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_08ed0b52e395c497a7fc07da1be85749 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_layout );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_window );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_w == NULL );
        var_w = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_app );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_current_buffer );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_2;
        CHECK_OBJECT( var_w );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_w );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_w );
        tmp_source_name_6 = var_w;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_render_info );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 149;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_7;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_left_name_1;
            PyObject *tmp_source_name_8;
            PyObject *tmp_right_name_1;
            tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_max );
            assert( tmp_called_name_1 != NULL );
            CHECK_OBJECT( var_w );
            tmp_source_name_7 = var_w;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_render_info );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_08ed0b52e395c497a7fc07da1be85749->m_frame.f_lineno = 151;
            tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_last_visible_line );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_w );
            tmp_source_name_8 = var_w;
            tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_vertical_scroll );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_element_name_1 );

                exception_lineno = 151;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_1 = const_int_pos_1;
            tmp_args_element_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_element_name_1 );

                exception_lineno = 151;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_08ed0b52e395c497a7fc07da1be85749->m_frame.f_lineno = 151;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 151;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_line_index == NULL );
            var_line_index = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( var_line_index );
            tmp_assattr_name_1 = var_line_index;
            CHECK_OBJECT( var_w );
            tmp_assattr_target_1 = var_w;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_vertical_scroll, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 152;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
        }
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_9;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_assattr_target_2;
            CHECK_OBJECT( var_b );
            tmp_source_name_9 = var_b;
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_document );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 154;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_line_index );
            tmp_args_element_name_3 = var_line_index;
            tmp_args_element_name_4 = const_int_0;
            frame_08ed0b52e395c497a7fc07da1be85749->m_frame.f_lineno = 154;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_assattr_name_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_translate_row_col_to_index, call_args );
            }

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_assattr_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 154;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_b );
            tmp_assattr_target_2 = var_b;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_cursor_position, tmp_assattr_name_2 );
            Py_DECREF( tmp_assattr_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 154;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
        }
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_source_name_10;
            CHECK_OBJECT( var_b );
            tmp_source_name_10 = var_b;
            tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_cursor_position );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_inplace_assign_attr_1__start == NULL );
            tmp_inplace_assign_attr_1__start = tmp_assign_source_4;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_11;
            PyObject *tmp_source_name_12;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
            tmp_left_name_2 = tmp_inplace_assign_attr_1__start;
            CHECK_OBJECT( var_b );
            tmp_source_name_12 = var_b;
            tmp_source_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_document );
            if ( tmp_source_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_get_start_of_line_position );
            Py_DECREF( tmp_source_name_11 );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_kw_name_1 = PyDict_Copy( const_dict_5b4e82b0d7f6ce57036ec2983d8d8c39 );
            frame_08ed0b52e395c497a7fc07da1be85749->m_frame.f_lineno = 155;
            tmp_right_name_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_5 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            assert( tmp_inplace_assign_attr_1__end == NULL );
            tmp_inplace_assign_attr_1__end = tmp_assign_source_5;
        }
        // Tried code:
        {
            PyObject *tmp_assattr_name_3;
            PyObject *tmp_assattr_target_3;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
            tmp_assattr_name_3 = tmp_inplace_assign_attr_1__end;
            CHECK_OBJECT( var_b );
            tmp_assattr_target_3 = var_b;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_cursor_position, tmp_assattr_name_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_08ed0b52e395c497a7fc07da1be85749 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_08ed0b52e395c497a7fc07da1be85749 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_08ed0b52e395c497a7fc07da1be85749, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_08ed0b52e395c497a7fc07da1be85749->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_08ed0b52e395c497a7fc07da1be85749, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_08ed0b52e395c497a7fc07da1be85749,
        type_description_1,
        par_event,
        var_w,
        var_b,
        var_line_index
    );


    // Release cached frame.
    if ( frame_08ed0b52e395c497a7fc07da1be85749 == cache_frame_08ed0b52e395c497a7fc07da1be85749 )
    {
        Py_DECREF( frame_08ed0b52e395c497a7fc07da1be85749 );
    }
    cache_frame_08ed0b52e395c497a7fc07da1be85749 = NULL;

    assertFrameObject( frame_08ed0b52e395c497a7fc07da1be85749 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_7_scroll_page_down );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_w );
    Py_DECREF( var_w );
    var_w = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_line_index );
    var_line_index = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_w );
    var_w = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_line_index );
    var_line_index = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_7_scroll_page_down );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$scroll$$$function_8_scroll_page_up( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_w = NULL;
    PyObject *var_b = NULL;
    PyObject *var_line_index = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_c138d1da45f3cedb4455a0fdfd582f2d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_c138d1da45f3cedb4455a0fdfd582f2d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c138d1da45f3cedb4455a0fdfd582f2d, codeobj_c138d1da45f3cedb4455a0fdfd582f2d, module_prompt_toolkit$key_binding$bindings$scroll, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c138d1da45f3cedb4455a0fdfd582f2d = cache_frame_c138d1da45f3cedb4455a0fdfd582f2d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c138d1da45f3cedb4455a0fdfd582f2d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c138d1da45f3cedb4455a0fdfd582f2d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_layout );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_window );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_w == NULL );
        var_w = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_app );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_current_buffer );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_2;
        CHECK_OBJECT( var_w );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_w );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_w );
        tmp_source_name_6 = var_w;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_render_info );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 165;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_7;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_left_name_1;
            PyObject *tmp_source_name_8;
            PyObject *tmp_source_name_9;
            PyObject *tmp_right_name_1;
            tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_max );
            assert( tmp_called_name_1 != NULL );
            tmp_args_element_name_1 = const_int_0;
            tmp_called_name_2 = LOOKUP_BUILTIN( const_str_plain_min );
            assert( tmp_called_name_2 != NULL );
            CHECK_OBJECT( var_w );
            tmp_source_name_7 = var_w;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_render_info );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_c138d1da45f3cedb4455a0fdfd582f2d->m_frame.f_lineno = 168;
            tmp_args_element_name_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_first_visible_line );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_b );
            tmp_source_name_9 = var_b;
            tmp_source_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_document );
            if ( tmp_source_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_element_name_3 );

                exception_lineno = 169;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_cursor_position_row );
            Py_DECREF( tmp_source_name_8 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_element_name_3 );

                exception_lineno = 169;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_right_name_1 = const_int_pos_1;
            tmp_args_element_name_4 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_args_element_name_3 );

                exception_lineno = 169;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_c138d1da45f3cedb4455a0fdfd582f2d->m_frame.f_lineno = 168;
            {
                PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_3 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            frame_c138d1da45f3cedb4455a0fdfd582f2d->m_frame.f_lineno = 168;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 168;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_line_index == NULL );
            var_line_index = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_10;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( var_b );
            tmp_source_name_10 = var_b;
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_document );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_line_index );
            tmp_args_element_name_5 = var_line_index;
            tmp_args_element_name_6 = const_int_0;
            frame_c138d1da45f3cedb4455a0fdfd582f2d->m_frame.f_lineno = 171;
            {
                PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
                tmp_assattr_name_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_translate_row_col_to_index, call_args );
            }

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_b );
            tmp_assattr_target_1 = var_b;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
        }
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( var_b );
            tmp_source_name_11 = var_b;
            tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_cursor_position );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_inplace_assign_attr_1__start == NULL );
            tmp_inplace_assign_attr_1__start = tmp_assign_source_4;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_12;
            PyObject *tmp_source_name_13;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
            tmp_left_name_2 = tmp_inplace_assign_attr_1__start;
            CHECK_OBJECT( var_b );
            tmp_source_name_13 = var_b;
            tmp_source_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_document );
            if ( tmp_source_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_get_start_of_line_position );
            Py_DECREF( tmp_source_name_12 );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_kw_name_1 = PyDict_Copy( const_dict_5b4e82b0d7f6ce57036ec2983d8d8c39 );
            frame_c138d1da45f3cedb4455a0fdfd582f2d->m_frame.f_lineno = 172;
            tmp_right_name_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_3, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_5 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_right_name_2 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            assert( tmp_inplace_assign_attr_1__end == NULL );
            tmp_inplace_assign_attr_1__end = tmp_assign_source_5;
        }
        // Tried code:
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_assattr_target_2;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
            tmp_assattr_name_2 = tmp_inplace_assign_attr_1__end;
            CHECK_OBJECT( var_b );
            tmp_assattr_target_2 = var_b;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_cursor_position, tmp_assattr_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        {
            PyObject *tmp_assattr_name_3;
            PyObject *tmp_assattr_target_3;
            tmp_assattr_name_3 = const_int_0;
            CHECK_OBJECT( var_w );
            tmp_assattr_target_3 = var_w;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_vertical_scroll, tmp_assattr_name_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c138d1da45f3cedb4455a0fdfd582f2d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c138d1da45f3cedb4455a0fdfd582f2d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c138d1da45f3cedb4455a0fdfd582f2d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c138d1da45f3cedb4455a0fdfd582f2d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c138d1da45f3cedb4455a0fdfd582f2d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c138d1da45f3cedb4455a0fdfd582f2d,
        type_description_1,
        par_event,
        var_w,
        var_b,
        var_line_index
    );


    // Release cached frame.
    if ( frame_c138d1da45f3cedb4455a0fdfd582f2d == cache_frame_c138d1da45f3cedb4455a0fdfd582f2d )
    {
        Py_DECREF( frame_c138d1da45f3cedb4455a0fdfd582f2d );
    }
    cache_frame_c138d1da45f3cedb4455a0fdfd582f2d = NULL;

    assertFrameObject( frame_c138d1da45f3cedb4455a0fdfd582f2d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_8_scroll_page_up );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_w );
    Py_DECREF( var_w );
    var_w = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_line_index );
    var_line_index = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_w );
    var_w = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_line_index );
    var_line_index = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$scroll$$$function_8_scroll_page_up );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_1_scroll_forward( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$scroll$$$function_1_scroll_forward,
        const_str_plain_scroll_forward,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_afc6a414b8ddbf6217d3963a00b6c5e5,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$scroll,
        const_str_digest_56293838bc172e7cbbdc9cc4ac6ee031,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_2_scroll_backward( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$scroll$$$function_2_scroll_backward,
        const_str_plain_scroll_backward,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_04e64394cd2fb696f7c9c378ed63a5d8,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$scroll,
        const_str_digest_7b4e7e4044f3b9abebb3493073fcfb31,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_3_scroll_half_page_down(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$scroll$$$function_3_scroll_half_page_down,
        const_str_plain_scroll_half_page_down,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_77c5ff425b49baa61aff4ce321c0dae9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$scroll,
        const_str_digest_a03d439dce14a9dcc719494a0f81a490,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_4_scroll_half_page_up(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$scroll$$$function_4_scroll_half_page_up,
        const_str_plain_scroll_half_page_up,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_260479cd8ffaea2889526673d7a2bec9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$scroll,
        const_str_digest_d5d4264b07e30d089a394000b53ea80a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_5_scroll_one_line_down(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$scroll$$$function_5_scroll_one_line_down,
        const_str_plain_scroll_one_line_down,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bd5ef991298b55f61823b6cc15ccd4bf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$scroll,
        const_str_digest_ea2b3ee05cacebccd17a172dcf1abca2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_6_scroll_one_line_up(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$scroll$$$function_6_scroll_one_line_up,
        const_str_plain_scroll_one_line_up,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bc1f4c408831c212686c10193896948f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$scroll,
        const_str_digest_a9e4c593e232b85d97ace15712dd3bb6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_7_scroll_page_down(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$scroll$$$function_7_scroll_page_down,
        const_str_plain_scroll_page_down,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_08ed0b52e395c497a7fc07da1be85749,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$scroll,
        const_str_digest_edfd9fa801e0528978d8f602452058f4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_8_scroll_page_up(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$scroll$$$function_8_scroll_page_up,
        const_str_plain_scroll_page_up,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c138d1da45f3cedb4455a0fdfd582f2d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$scroll,
        const_str_digest_30f03b59873a72e9a1c937a629f87f0a,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$key_binding$bindings$scroll =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.key_binding.bindings.scroll",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$scroll)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$key_binding$bindings$scroll)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$key_binding$bindings$scroll );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.scroll: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.scroll: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.scroll: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$key_binding$bindings$scroll" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$key_binding$bindings$scroll = Py_InitModule4(
        "prompt_toolkit.key_binding.bindings.scroll",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$key_binding$bindings$scroll = PyModule_Create( &mdef_prompt_toolkit$key_binding$bindings$scroll );
#endif

    moduledict_prompt_toolkit$key_binding$bindings$scroll = MODULE_DICT( module_prompt_toolkit$key_binding$bindings$scroll );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$key_binding$bindings$scroll,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$key_binding$bindings$scroll,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$key_binding$bindings$scroll,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$key_binding$bindings$scroll,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$key_binding$bindings$scroll );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_5d5e8a5ec86a06aac0e040ae6c4d5bf4, module_prompt_toolkit$key_binding$bindings$scroll );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_662c6cfe2740033a95b69a8f5f20216f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_bf29d61e3e87f44f3bdf8fa4bbac7169;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_662c6cfe2740033a95b69a8f5f20216f = MAKE_MODULE_FRAME( codeobj_662c6cfe2740033a95b69a8f5f20216f, module_prompt_toolkit$key_binding$bindings$scroll );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_662c6cfe2740033a95b69a8f5f20216f );
    assert( Py_REFCNT( frame_662c6cfe2740033a95b69a8f5f20216f ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_662c6cfe2740033a95b69a8f5f20216f->m_frame.f_lineno = 8;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_d8d4f5e440c7f01129293c18f6555af5;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$scroll;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_range_tuple;
        tmp_level_name_1 = const_int_0;
        frame_662c6cfe2740033a95b69a8f5f20216f->m_frame.f_lineno = 9;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_range );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_range, tmp_assign_source_5 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_662c6cfe2740033a95b69a8f5f20216f );
#endif
    popFrameStack();

    assertFrameObject( frame_662c6cfe2740033a95b69a8f5f20216f );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_662c6cfe2740033a95b69a8f5f20216f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_662c6cfe2740033a95b69a8f5f20216f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_662c6cfe2740033a95b69a8f5f20216f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_662c6cfe2740033a95b69a8f5f20216f, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = LIST_COPY( const_list_5fa4e776f6142d4bfc7525ffaa14f99b_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_false_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_7 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_1_scroll_forward( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_scroll_forward, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_false_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_8 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_2_scroll_backward( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_scroll_backward, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_3_scroll_half_page_down(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_scroll_half_page_down, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_4_scroll_half_page_up(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_scroll_half_page_up, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_5_scroll_one_line_down(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_scroll_one_line_down, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_6_scroll_one_line_up(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_scroll_one_line_up, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_7_scroll_page_down(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_scroll_page_down, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$scroll$$$function_8_scroll_page_up(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$scroll, (Nuitka_StringObject *)const_str_plain_scroll_page_up, tmp_assign_source_14 );
    }

    return MOD_RETURN_VALUE( module_prompt_toolkit$key_binding$bindings$scroll );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
