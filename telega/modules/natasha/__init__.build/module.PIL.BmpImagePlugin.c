/* Generated code for Python module 'PIL.BmpImagePlugin'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_PIL$BmpImagePlugin" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_PIL$BmpImagePlugin;
PyDictObject *moduledict_PIL$BmpImagePlugin;

/* The declarations of module constants used, if any. */
extern PyObject *const_int_pos_12;
extern PyObject *const_str_plain_compression;
extern PyObject *const_tuple_str_plain_Image_str_plain_ImageFile_str_plain_ImagePalette_tuple;
static PyObject *const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple;
extern PyObject *const_int_pos_20;
static PyObject *const_slice_int_pos_16_int_pos_20_none;
static PyObject *const_tuple_int_pos_14_tuple;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_str_digest_da2c0ebacadc96c5c0ecd57f371555d8;
extern PyObject *const_int_pos_24;
extern PyObject *const_str_plain_colors;
static PyObject *const_str_digest_b374c723e5277778d29b61bb9eb3a478;
extern PyObject *const_str_plain___name__;
static PyObject *const_str_plain_a_mask;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_int_pos_22;
extern PyObject *const_str_plain_palette;
extern PyObject *const_str_plain_i;
static PyObject *const_str_plain_BITFIELDS;
extern PyObject *const_str_plain___file__;
static PyObject *const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_int_pos_5_tuple;
extern PyObject *const_int_pos_4294967296;
static PyObject *const_str_plain_rgba_mask;
extern PyObject *const_str_plain_offset;
extern PyObject *const_tuple_str_plain_im_str_plain_fp_str_plain_filename_tuple;
extern PyObject *const_str_plain_header_data;
static PyObject *const_str_digest_9184770994a8dfc7f5067f9878a65025;
extern PyObject *const_str_plain_prefix;
extern PyObject *const_str_plain_rawmode;
extern PyObject *const_str_plain_bits;
extern PyObject *const_int_neg_1;
static PyObject *const_str_plain_file_info;
static PyObject *const_str_digest_ab07d643992060511107c457f3bc9b0b;
extern PyObject *const_str_plain_mode;
extern PyObject *const_str_plain_size;
extern PyObject *const_str_plain_seek;
extern PyObject *const_int_pos_31;
extern PyObject *const_str_plain_image;
extern PyObject *const_str_plain_None;
extern PyObject *const_int_pos_6;
extern PyObject *const_str_plain_JPEG;
static PyObject *const_slice_int_pos_28_int_pos_32_none;
extern PyObject *const_int_pos_52;
static PyObject *const_tuple_str_plain_dpi_tuple_int_pos_96_int_pos_96_tuple_tuple;
extern PyObject *const_str_plain_o16le;
extern PyObject *const_str_plain__size;
static PyObject *const_str_plain_RLE4;
extern PyObject *const_int_pos_5;
extern PyObject *const_float_0_5;
static PyObject *const_str_plain_RAW;
static PyObject *const_tuple_str_plain_BGRA_str_plain_RGBA_tuple;
extern PyObject *const_str_plain_ImagePalette;
static PyObject *const_str_plain_COMPRESSIONS;
extern PyObject *const_str_plain_stride;
extern PyObject *const_str_plain_RGB;
extern PyObject *const_tuple_int_0_int_0_tuple;
extern PyObject *const_str_plain_read;
extern PyObject *const_str_digest_19532ebc85912cba7ed62a9cd4e9c195;
static PyObject *const_tuple_int_pos_24_int_pos_16_tuple;
static PyObject *const_str_plain__bitmap;
static PyObject *const_tuple_str_plain_self_str_plain_head_data_str_plain_offset_tuple;
extern PyObject *const_tuple_true_tuple;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_int_pos_16;
static PyObject *const_str_plain_head_data;
extern PyObject *const_slice_none_int_pos_2_none;
extern PyObject *const_str_plain_fp;
static PyObject *const_str_digest_eabbe093ea0d4e0ea3f9a72998b0f524;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_int_pos_32;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_L;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_tuple_str_plain_prefix_tuple;
extern PyObject *const_slice_int_0_int_pos_4_none;
extern PyObject *const_str_plain_padding;
static PyObject *const_slice_int_pos_20_int_pos_24_none;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_tuple_int_pos_4_tuple;
static PyObject *const_str_plain_DIB;
extern PyObject *const_str_plain_i8;
extern PyObject *const_tuple_str_plain_P_str_digest_676e11a5ff7d2b82b18a5b0dbbad6c6d_tuple;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_direction;
static PyObject *const_str_digest_ed7db2c47dad4781835faac02fb06b46;
extern PyObject *const_int_neg_4;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_ppm;
extern PyObject *const_tuple_str_plain_P_str_plain_P_tuple;
static PyObject *const_tuple_str_plain_RGB_str_digest_919a27c187420f29c10d204749e1667d_tuple;
static PyObject *const_str_plain_palette_padding;
static PyObject *const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple;
static PyObject *const_str_digest_ad55bc4c4352c79ec48d6f5e7a30f8a2;
static PyObject *const_dict_58d7ae98441ebd965650132eb201c985;
extern PyObject *const_str_plain_height;
extern PyObject *const_str_plain_o16;
static PyObject *const_str_digest_c75b60f810566e71a401075f6a675018;
extern PyObject *const_str_plain_indices;
static PyObject *const_str_digest_d5a77806b874a047cab54ef775232bdd;
extern PyObject *const_int_pos_8;
extern PyObject *const_str_plain_mask;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_b_mask;
extern PyObject *const_str_plain_enumerate;
extern PyObject *const_str_plain_i16le;
static PyObject *const_tuple_int_pos_40_int_pos_64_int_pos_108_int_pos_124_tuple;
static PyObject *const_tuple_str_plain_colors_int_0_tuple;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_x_tuple;
static PyObject *const_str_plain_RLE8;
extern PyObject *const_str_plain__binary;
static PyObject *const_tuple_str_plain_r_mask_str_plain_g_mask_str_plain_b_mask_tuple;
static PyObject *const_dict_573115a447ef2f8dd7ef6812133c5976;
static PyObject *const_str_digest_b29042b8bf76639bd00bc6af12b19b3b;
extern PyObject *const_str_plain_rgb;
extern PyObject *const_str_plain__safe_read;
extern PyObject *const_str_plain_raw;
extern PyObject *const_str_plain_tell;
extern PyObject *const_str_plain_P;
extern PyObject *const_int_pos_10;
extern PyObject *const_str_plain_o32;
extern PyObject *const_int_pos_14;
extern PyObject *const_tuple_str_plain_x_tuple;
static PyObject *const_bytes_digest_181e1eeb195f3bcd8ad8a954f597cb5b;
static PyObject *const_tuple_str_plain_RGB_str_plain_BGR_tuple;
extern PyObject *const_slice_int_pos_6_int_pos_8_none;
extern PyObject *const_tuple_int_0_tuple;
static PyObject *const_str_digest_4ab0f9aaf08160ca1ff70c4131176341;
static PyObject *const_tuple_int_pos_40_tuple;
extern PyObject *const_str_plain__open;
extern PyObject *const_str_plain_False;
static PyObject *const_str_digest_7b3d22cf3697d3bdb60854511bfe5ef9;
extern PyObject *const_str_plain_map;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_dict_6c9a4e8e23277b1848566a99b2c6e76d;
extern PyObject *const_int_pos_96;
static PyObject *const_str_plain_MASK_MODES;
extern PyObject *const_str_plain_BGRX;
static PyObject *const_str_digest_7694c8aa5098fdbfc9d126eeda02356f;
static PyObject *const_str_plain_header_size;
static PyObject *const_str_plain_BIT2MODE;
extern PyObject *const_str_plain___version__;
extern PyObject *const_str_plain_format_description;
static PyObject *const_str_digest_2faeda2d7dc02f33bf8119f46b6cd1e8;
extern PyObject *const_str_digest_676e11a5ff7d2b82b18a5b0dbbad6c6d;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_val;
static PyObject *const_str_plain_bitmap_header;
static PyObject *const_str_digest_9c5eae82d3cef0578e37480eb32e9977;
extern PyObject *const_tuple_int_0_int_pos_255_tuple;
extern PyObject *const_int_pos_54;
static PyObject *const_tuple_str_plain_RGB_str_plain_BGRX_tuple;
extern PyObject *const_str_plain__accept;
extern PyObject *const_str_plain_tile;
extern PyObject *const_str_plain_i32le;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain_r_mask;
extern PyObject *const_slice_int_pos_10_int_pos_12_none;
static PyObject *const_str_plain_rgb_mask;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
static PyObject *const_list_int_pos_12_int_pos_40_int_pos_64_int_pos_108_int_pos_124_list;
static PyObject *const_slice_int_pos_12_int_pos_16_none;
extern PyObject *const_str_plain_x;
extern PyObject *const_str_plain_o32le;
extern PyObject *const_str_plain_ind;
extern PyObject *const_str_plain_header;
extern PyObject *const_slice_none_int_pos_4_none;
extern PyObject *const_int_pos_64;
extern PyObject *const_slice_int_pos_4_int_pos_8_none;
extern PyObject *const_str_plain_register_mime;
extern PyObject *const_int_pos_36;
extern PyObject *const_str_plain_o8;
static PyObject *const_slice_int_pos_10_int_pos_14_none;
extern PyObject *const_int_pos_4;
static PyObject *const_str_digest_3cdec28f8e9f2b1d0a7f06c815f54869;
extern PyObject *const_str_plain_SAVE;
extern PyObject *const_str_plain_getpalette;
static PyObject *const_str_digest_531b5f03cfbae3e5d98ea4f6850a6071;
extern PyObject *const_tuple_str_plain_P_str_digest_19532ebc85912cba7ed62a9cd4e9c195_tuple;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_i32;
extern PyObject *const_bytes_empty;
extern PyObject *const_xrange_0_256;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_e79ed04e2052da0c6d3f455a76033a16;
extern PyObject *const_slice_int_pos_4_int_pos_6_none;
extern PyObject *const_str_plain_encoderinfo;
static PyObject *const_str_plain_DibImageFile;
extern PyObject *const_str_plain_SUPPORTED;
extern PyObject *const_str_plain___class__;
static PyObject *const_str_digest_40e9a04769198a44edba14d650f76b42;
extern PyObject *const_str_plain_BGRA;
extern PyObject *const_str_plain_idx;
static PyObject *const_dict_0838f6241605954603b148cacbf5c6cf;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_digest_8981cffaaf03286e98d8ff2e38b0e22f;
static PyObject *const_str_plain_pixels_per_meter;
static PyObject *const_str_plain_g_mask;
extern PyObject *const_str_plain_filename;
static PyObject *const_str_plain__dib_save;
static PyObject *const_tuple_3a2700929bfdf8368dbc820243ae0d59_tuple;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_dpi;
static PyObject *const_str_plain_BmpImageFile;
extern PyObject *const_str_plain_raw_mode;
static PyObject *const_str_plain_greyscale;
extern PyObject *const_int_pos_108;
extern PyObject *const_int_pos_256;
extern PyObject *const_str_plain_1;
extern PyObject *const_str_plain_width;
extern PyObject *const_str_plain_BMP;
extern PyObject *const_str_plain_ImageFile;
extern PyObject *const_str_plain_register_save;
static PyObject *const_str_digest_919a27c187420f29c10d204749e1667d;
extern PyObject *const_str_plain__save;
static PyObject *const_str_digest_af7895b8e9a7643f2ab308f77e96c311;
extern PyObject *const_int_pos_3;
static PyObject *const_str_plain__dib_accept;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain_i16;
extern PyObject *const_int_pos_2147483648;
static PyObject *const_tuple_int_pos_96_int_pos_96_tuple;
static PyObject *const_str_plain_planes;
extern PyObject *const_int_pos_255;
extern PyObject *const_str_plain_im;
static PyObject *const_str_digest_bde65f20a0edb67aa82d817e7642f0ab;
extern PyObject *const_str_plain_self;
static PyObject *const_str_digest_4621a4026644fa18656e149a28e882cc;
static PyObject *const_slice_int_pos_24_int_pos_28_none;
extern PyObject *const_str_plain_RGBA;
extern PyObject *const_str_plain_write;
extern PyObject *const_int_pos_40;
static PyObject *const_str_plain_BGR;
extern PyObject *const_int_pos_7;
extern PyObject *const_int_pos_28;
extern PyObject *const_str_plain_PNG;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_A;
extern PyObject *const_int_pos_2;
static PyObject *const_float_39_3701;
extern PyObject *const_str_plain_Image;
extern PyObject *const_slice_int_pos_8_int_pos_10_none;
extern PyObject *const_str_plain_info;
extern PyObject *const_str_plain_format;
static PyObject *const_dict_05bbcf03645040ac7ffdd29056adaf00;
extern PyObject *const_str_empty;
extern PyObject *const_int_pos_124;
extern PyObject *const_tuple_none_none_tuple;
static PyObject *const_str_plain_data_size;
extern PyObject *const_int_pos_65536;
extern PyObject *const_str_plain_register_open;
static PyObject *const_list_fae9db5b2dff7acc072e1a63a7fc7fbf_list;
extern PyObject *const_str_plain_register_extension;
extern PyObject *const_slice_int_pos_2_int_pos_4_none;
static PyObject *const_str_digest_d95889588ee8c00ac36d1eb48e3389c2;
static PyObject *const_str_plain_y_flip;
extern PyObject *const_tuple_bytes_empty_tuple;
extern PyObject *const_slice_int_0_int_pos_2_none;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple = PyTuple_New( 19 );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 1, const_str_plain_header ); Py_INCREF( const_str_plain_header );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 2, const_str_plain_offset ); Py_INCREF( const_str_plain_offset );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 3, const_str_plain_read ); Py_INCREF( const_str_plain_read );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 4, const_str_plain_seek ); Py_INCREF( const_str_plain_seek );
    const_str_plain_file_info = UNSTREAM_STRING_ASCII( &constant_bin[ 1229 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 5, const_str_plain_file_info ); Py_INCREF( const_str_plain_file_info );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 6, const_str_plain_header_data ); Py_INCREF( const_str_plain_header_data );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 7, const_str_plain_idx ); Py_INCREF( const_str_plain_idx );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 8, const_str_plain_mask ); Py_INCREF( const_str_plain_mask );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 9, const_str_plain_raw_mode ); Py_INCREF( const_str_plain_raw_mode );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 10, const_str_plain_SUPPORTED ); Py_INCREF( const_str_plain_SUPPORTED );
    const_str_plain_MASK_MODES = UNSTREAM_STRING_ASCII( &constant_bin[ 1238 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 11, const_str_plain_MASK_MODES ); Py_INCREF( const_str_plain_MASK_MODES );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 12, const_str_plain_padding ); Py_INCREF( const_str_plain_padding );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 13, const_str_plain_palette ); Py_INCREF( const_str_plain_palette );
    const_str_plain_greyscale = UNSTREAM_STRING_ASCII( &constant_bin[ 1248 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 14, const_str_plain_greyscale ); Py_INCREF( const_str_plain_greyscale );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 15, const_str_plain_indices ); Py_INCREF( const_str_plain_indices );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 16, const_str_plain_ind ); Py_INCREF( const_str_plain_ind );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 17, const_str_plain_val ); Py_INCREF( const_str_plain_val );
    PyTuple_SET_ITEM( const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 18, const_str_plain_rgb ); Py_INCREF( const_str_plain_rgb );
    const_slice_int_pos_16_int_pos_20_none = PySlice_New( const_int_pos_16, const_int_pos_20, Py_None );
    const_tuple_int_pos_14_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_14_tuple, 0, const_int_pos_14 ); Py_INCREF( const_int_pos_14 );
    const_str_digest_da2c0ebacadc96c5c0ecd57f371555d8 = UNSTREAM_STRING_ASCII( &constant_bin[ 1257 ], 32, 0 );
    const_str_digest_b374c723e5277778d29b61bb9eb3a478 = UNSTREAM_STRING_ASCII( &constant_bin[ 1289 ], 34, 0 );
    const_str_plain_a_mask = UNSTREAM_STRING_ASCII( &constant_bin[ 1323 ], 6, 1 );
    const_str_plain_BITFIELDS = UNSTREAM_STRING_ASCII( &constant_bin[ 1329 ], 9, 1 );
    const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_int_pos_5_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_int_pos_5_tuple, 0, const_int_0 ); Py_INCREF( const_int_0 );
    PyTuple_SET_ITEM( const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_int_pos_5_tuple, 1, const_int_pos_1 ); Py_INCREF( const_int_pos_1 );
    PyTuple_SET_ITEM( const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_int_pos_5_tuple, 2, const_int_pos_2 ); Py_INCREF( const_int_pos_2 );
    PyTuple_SET_ITEM( const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_int_pos_5_tuple, 3, const_int_pos_3 ); Py_INCREF( const_int_pos_3 );
    PyTuple_SET_ITEM( const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_int_pos_5_tuple, 4, const_int_pos_4 ); Py_INCREF( const_int_pos_4 );
    PyTuple_SET_ITEM( const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_int_pos_5_tuple, 5, const_int_pos_5 ); Py_INCREF( const_int_pos_5 );
    const_str_plain_rgba_mask = UNSTREAM_STRING_ASCII( &constant_bin[ 1338 ], 9, 1 );
    const_str_digest_9184770994a8dfc7f5067f9878a65025 = UNSTREAM_STRING_ASCII( &constant_bin[ 1347 ], 18, 0 );
    const_str_digest_ab07d643992060511107c457f3bc9b0b = UNSTREAM_STRING_ASCII( &constant_bin[ 1365 ], 47, 0 );
    const_slice_int_pos_28_int_pos_32_none = PySlice_New( const_int_pos_28, const_int_pos_32, Py_None );
    const_tuple_str_plain_dpi_tuple_int_pos_96_int_pos_96_tuple_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_dpi_tuple_int_pos_96_int_pos_96_tuple_tuple, 0, const_str_plain_dpi ); Py_INCREF( const_str_plain_dpi );
    const_tuple_int_pos_96_int_pos_96_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_96_int_pos_96_tuple, 0, const_int_pos_96 ); Py_INCREF( const_int_pos_96 );
    PyTuple_SET_ITEM( const_tuple_int_pos_96_int_pos_96_tuple, 1, const_int_pos_96 ); Py_INCREF( const_int_pos_96 );
    PyTuple_SET_ITEM( const_tuple_str_plain_dpi_tuple_int_pos_96_int_pos_96_tuple_tuple, 1, const_tuple_int_pos_96_int_pos_96_tuple ); Py_INCREF( const_tuple_int_pos_96_int_pos_96_tuple );
    const_str_plain_RLE4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1412 ], 4, 1 );
    const_str_plain_RAW = UNSTREAM_STRING_ASCII( &constant_bin[ 1416 ], 3, 1 );
    const_tuple_str_plain_BGRA_str_plain_RGBA_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_BGRA_str_plain_RGBA_tuple, 0, const_str_plain_BGRA ); Py_INCREF( const_str_plain_BGRA );
    PyTuple_SET_ITEM( const_tuple_str_plain_BGRA_str_plain_RGBA_tuple, 1, const_str_plain_RGBA ); Py_INCREF( const_str_plain_RGBA );
    const_str_plain_COMPRESSIONS = UNSTREAM_STRING_ASCII( &constant_bin[ 1419 ], 12, 1 );
    const_tuple_int_pos_24_int_pos_16_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_24_int_pos_16_tuple, 0, const_int_pos_24 ); Py_INCREF( const_int_pos_24 );
    PyTuple_SET_ITEM( const_tuple_int_pos_24_int_pos_16_tuple, 1, const_int_pos_16 ); Py_INCREF( const_int_pos_16 );
    const_str_plain__bitmap = UNSTREAM_STRING_ASCII( &constant_bin[ 1431 ], 7, 1 );
    const_tuple_str_plain_self_str_plain_head_data_str_plain_offset_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_head_data_str_plain_offset_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_head_data = UNSTREAM_STRING_ASCII( &constant_bin[ 1438 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_head_data_str_plain_offset_tuple, 1, const_str_plain_head_data ); Py_INCREF( const_str_plain_head_data );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_head_data_str_plain_offset_tuple, 2, const_str_plain_offset ); Py_INCREF( const_str_plain_offset );
    const_str_digest_eabbe093ea0d4e0ea3f9a72998b0f524 = UNSTREAM_STRING_ASCII( &constant_bin[ 1447 ], 33, 0 );
    const_slice_int_pos_20_int_pos_24_none = PySlice_New( const_int_pos_20, const_int_pos_24, Py_None );
    const_str_plain_DIB = UNSTREAM_STRING_ASCII( &constant_bin[ 1480 ], 3, 1 );
    const_str_digest_ed7db2c47dad4781835faac02fb06b46 = UNSTREAM_STRING_ASCII( &constant_bin[ 1483 ], 32, 0 );
    const_tuple_str_plain_RGB_str_digest_919a27c187420f29c10d204749e1667d_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_RGB_str_digest_919a27c187420f29c10d204749e1667d_tuple, 0, const_str_plain_RGB ); Py_INCREF( const_str_plain_RGB );
    const_str_digest_919a27c187420f29c10d204749e1667d = UNSTREAM_STRING_ASCII( &constant_bin[ 1515 ], 6, 0 );
    PyTuple_SET_ITEM( const_tuple_str_plain_RGB_str_digest_919a27c187420f29c10d204749e1667d_tuple, 1, const_str_digest_919a27c187420f29c10d204749e1667d ); Py_INCREF( const_str_digest_919a27c187420f29c10d204749e1667d );
    const_str_plain_palette_padding = UNSTREAM_STRING_ASCII( &constant_bin[ 1521 ], 15, 1 );
    const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple = PyTuple_New( 15 );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 0, const_str_plain_im ); Py_INCREF( const_str_plain_im );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 1, const_str_plain_fp ); Py_INCREF( const_str_plain_fp );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 2, const_str_plain_filename ); Py_INCREF( const_str_plain_filename );
    const_str_plain_bitmap_header = UNSTREAM_STRING_ASCII( &constant_bin[ 1536 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 3, const_str_plain_bitmap_header ); Py_INCREF( const_str_plain_bitmap_header );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 4, const_str_plain_rawmode ); Py_INCREF( const_str_plain_rawmode );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 5, const_str_plain_bits ); Py_INCREF( const_str_plain_bits );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 6, const_str_plain_colors ); Py_INCREF( const_str_plain_colors );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 7, const_str_plain_info ); Py_INCREF( const_str_plain_info );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 8, const_str_plain_dpi ); Py_INCREF( const_str_plain_dpi );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 9, const_str_plain_ppm ); Py_INCREF( const_str_plain_ppm );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 10, const_str_plain_stride ); Py_INCREF( const_str_plain_stride );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 11, const_str_plain_header ); Py_INCREF( const_str_plain_header );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 12, const_str_plain_image ); Py_INCREF( const_str_plain_image );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 13, const_str_plain_offset ); Py_INCREF( const_str_plain_offset );
    PyTuple_SET_ITEM( const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 14, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    const_str_digest_ad55bc4c4352c79ec48d6f5e7a30f8a2 = UNSTREAM_STRING_ASCII( &constant_bin[ 1549 ], 4, 0 );
    const_dict_58d7ae98441ebd965650132eb201c985 = _PyDict_NewPresized( 6 );
    PyDict_SetItem( const_dict_58d7ae98441ebd965650132eb201c985, const_int_pos_1, const_tuple_str_plain_P_str_digest_676e11a5ff7d2b82b18a5b0dbbad6c6d_tuple );
    PyDict_SetItem( const_dict_58d7ae98441ebd965650132eb201c985, const_int_pos_4, const_tuple_str_plain_P_str_digest_19532ebc85912cba7ed62a9cd4e9c195_tuple );
    PyDict_SetItem( const_dict_58d7ae98441ebd965650132eb201c985, const_int_pos_8, const_tuple_str_plain_P_str_plain_P_tuple );
    PyDict_SetItem( const_dict_58d7ae98441ebd965650132eb201c985, const_int_pos_16, const_tuple_str_plain_RGB_str_digest_919a27c187420f29c10d204749e1667d_tuple );
    const_tuple_str_plain_RGB_str_plain_BGR_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_RGB_str_plain_BGR_tuple, 0, const_str_plain_RGB ); Py_INCREF( const_str_plain_RGB );
    const_str_plain_BGR = UNSTREAM_STRING_ASCII( &constant_bin[ 1515 ], 3, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_RGB_str_plain_BGR_tuple, 1, const_str_plain_BGR ); Py_INCREF( const_str_plain_BGR );
    PyDict_SetItem( const_dict_58d7ae98441ebd965650132eb201c985, const_int_pos_24, const_tuple_str_plain_RGB_str_plain_BGR_tuple );
    const_tuple_str_plain_RGB_str_plain_BGRX_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_RGB_str_plain_BGRX_tuple, 0, const_str_plain_RGB ); Py_INCREF( const_str_plain_RGB );
    PyTuple_SET_ITEM( const_tuple_str_plain_RGB_str_plain_BGRX_tuple, 1, const_str_plain_BGRX ); Py_INCREF( const_str_plain_BGRX );
    PyDict_SetItem( const_dict_58d7ae98441ebd965650132eb201c985, const_int_pos_32, const_tuple_str_plain_RGB_str_plain_BGRX_tuple );
    assert( PyDict_Size( const_dict_58d7ae98441ebd965650132eb201c985 ) == 6 );
    const_str_digest_c75b60f810566e71a401075f6a675018 = UNSTREAM_STRING_ASCII( &constant_bin[ 1553 ], 27, 0 );
    const_str_digest_d5a77806b874a047cab54ef775232bdd = UNSTREAM_STRING_ASCII( &constant_bin[ 1580 ], 29, 0 );
    const_str_plain_b_mask = UNSTREAM_STRING_ASCII( &constant_bin[ 1609 ], 6, 1 );
    const_tuple_int_pos_40_int_pos_64_int_pos_108_int_pos_124_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_int_pos_40_int_pos_64_int_pos_108_int_pos_124_tuple, 0, const_int_pos_40 ); Py_INCREF( const_int_pos_40 );
    PyTuple_SET_ITEM( const_tuple_int_pos_40_int_pos_64_int_pos_108_int_pos_124_tuple, 1, const_int_pos_64 ); Py_INCREF( const_int_pos_64 );
    PyTuple_SET_ITEM( const_tuple_int_pos_40_int_pos_64_int_pos_108_int_pos_124_tuple, 2, const_int_pos_108 ); Py_INCREF( const_int_pos_108 );
    PyTuple_SET_ITEM( const_tuple_int_pos_40_int_pos_64_int_pos_108_int_pos_124_tuple, 3, const_int_pos_124 ); Py_INCREF( const_int_pos_124 );
    const_tuple_str_plain_colors_int_0_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_colors_int_0_tuple, 0, const_str_plain_colors ); Py_INCREF( const_str_plain_colors );
    PyTuple_SET_ITEM( const_tuple_str_plain_colors_int_0_tuple, 1, const_int_0 ); Py_INCREF( const_int_0 );
    const_str_plain_RLE8 = UNSTREAM_STRING_ASCII( &constant_bin[ 1615 ], 4, 1 );
    const_tuple_str_plain_r_mask_str_plain_g_mask_str_plain_b_mask_tuple = PyTuple_New( 3 );
    const_str_plain_r_mask = UNSTREAM_STRING_ASCII( &constant_bin[ 1619 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_mask_str_plain_g_mask_str_plain_b_mask_tuple, 0, const_str_plain_r_mask ); Py_INCREF( const_str_plain_r_mask );
    const_str_plain_g_mask = UNSTREAM_STRING_ASCII( &constant_bin[ 1625 ], 6, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_mask_str_plain_g_mask_str_plain_b_mask_tuple, 1, const_str_plain_g_mask ); Py_INCREF( const_str_plain_g_mask );
    PyTuple_SET_ITEM( const_tuple_str_plain_r_mask_str_plain_g_mask_str_plain_b_mask_tuple, 2, const_str_plain_b_mask ); Py_INCREF( const_str_plain_b_mask );
    const_dict_573115a447ef2f8dd7ef6812133c5976 = _PyDict_NewPresized( 6 );
    PyDict_SetItem( const_dict_573115a447ef2f8dd7ef6812133c5976, const_str_plain_RAW, const_int_0 );
    PyDict_SetItem( const_dict_573115a447ef2f8dd7ef6812133c5976, const_str_plain_RLE8, const_int_pos_1 );
    PyDict_SetItem( const_dict_573115a447ef2f8dd7ef6812133c5976, const_str_plain_RLE4, const_int_pos_2 );
    PyDict_SetItem( const_dict_573115a447ef2f8dd7ef6812133c5976, const_str_plain_BITFIELDS, const_int_pos_3 );
    PyDict_SetItem( const_dict_573115a447ef2f8dd7ef6812133c5976, const_str_plain_JPEG, const_int_pos_4 );
    PyDict_SetItem( const_dict_573115a447ef2f8dd7ef6812133c5976, const_str_plain_PNG, const_int_pos_5 );
    assert( PyDict_Size( const_dict_573115a447ef2f8dd7ef6812133c5976 ) == 6 );
    const_str_digest_b29042b8bf76639bd00bc6af12b19b3b = UNSTREAM_STRING_ASCII( &constant_bin[ 1631 ], 9, 0 );
    const_bytes_digest_181e1eeb195f3bcd8ad8a954f597cb5b = UNSTREAM_BYTES( &constant_bin[ 1269 ], 2 );
    const_str_digest_4ab0f9aaf08160ca1ff70c4131176341 = UNSTREAM_STRING_ASCII( &constant_bin[ 1640 ], 39, 0 );
    const_tuple_int_pos_40_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_int_pos_40_tuple, 0, const_int_pos_40 ); Py_INCREF( const_int_pos_40 );
    const_str_digest_7b3d22cf3697d3bdb60854511bfe5ef9 = UNSTREAM_STRING_ASCII( &constant_bin[ 1679 ], 32, 0 );
    const_dict_6c9a4e8e23277b1848566a99b2c6e76d = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1711 ], 108 );
    const_str_digest_7694c8aa5098fdbfc9d126eeda02356f = UNSTREAM_STRING_ASCII( &constant_bin[ 1819 ], 14, 0 );
    const_str_plain_header_size = UNSTREAM_STRING_ASCII( &constant_bin[ 1833 ], 11, 1 );
    const_str_plain_BIT2MODE = UNSTREAM_STRING_ASCII( &constant_bin[ 1844 ], 8, 1 );
    const_str_digest_2faeda2d7dc02f33bf8119f46b6cd1e8 = UNSTREAM_STRING_ASCII( &constant_bin[ 1852 ], 4, 0 );
    const_str_digest_9c5eae82d3cef0578e37480eb32e9977 = UNSTREAM_STRING_ASCII( &constant_bin[ 1856 ], 50, 0 );
    const_str_plain_rgb_mask = UNSTREAM_STRING_ASCII( &constant_bin[ 1906 ], 8, 1 );
    const_list_int_pos_12_int_pos_40_int_pos_64_int_pos_108_int_pos_124_list = PyList_New( 5 );
    PyList_SET_ITEM( const_list_int_pos_12_int_pos_40_int_pos_64_int_pos_108_int_pos_124_list, 0, const_int_pos_12 ); Py_INCREF( const_int_pos_12 );
    PyList_SET_ITEM( const_list_int_pos_12_int_pos_40_int_pos_64_int_pos_108_int_pos_124_list, 1, const_int_pos_40 ); Py_INCREF( const_int_pos_40 );
    PyList_SET_ITEM( const_list_int_pos_12_int_pos_40_int_pos_64_int_pos_108_int_pos_124_list, 2, const_int_pos_64 ); Py_INCREF( const_int_pos_64 );
    PyList_SET_ITEM( const_list_int_pos_12_int_pos_40_int_pos_64_int_pos_108_int_pos_124_list, 3, const_int_pos_108 ); Py_INCREF( const_int_pos_108 );
    PyList_SET_ITEM( const_list_int_pos_12_int_pos_40_int_pos_64_int_pos_108_int_pos_124_list, 4, const_int_pos_124 ); Py_INCREF( const_int_pos_124 );
    const_slice_int_pos_12_int_pos_16_none = PySlice_New( const_int_pos_12, const_int_pos_16, Py_None );
    const_slice_int_pos_10_int_pos_14_none = PySlice_New( const_int_pos_10, const_int_pos_14, Py_None );
    const_str_digest_3cdec28f8e9f2b1d0a7f06c815f54869 = UNSTREAM_STRING_ASCII( &constant_bin[ 1914 ], 32, 0 );
    const_str_digest_531b5f03cfbae3e5d98ea4f6850a6071 = UNSTREAM_STRING_ASCII( &constant_bin[ 1946 ], 27, 0 );
    const_str_digest_e79ed04e2052da0c6d3f455a76033a16 = UNSTREAM_STRING_ASCII( &constant_bin[ 1973 ], 3, 0 );
    const_str_plain_DibImageFile = UNSTREAM_STRING_ASCII( &constant_bin[ 1347 ], 12, 1 );
    const_str_digest_40e9a04769198a44edba14d650f76b42 = UNSTREAM_STRING_ASCII( &constant_bin[ 1640 ], 20, 0 );
    const_dict_0838f6241605954603b148cacbf5c6cf = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1976 ], 211 );
    const_str_digest_8981cffaaf03286e98d8ff2e38b0e22f = UNSTREAM_STRING_ASCII( &constant_bin[ 2187 ], 14, 0 );
    const_str_plain_pixels_per_meter = UNSTREAM_STRING_ASCII( &constant_bin[ 2201 ], 16, 1 );
    const_str_plain__dib_save = UNSTREAM_STRING_ASCII( &constant_bin[ 2217 ], 9, 1 );
    const_tuple_3a2700929bfdf8368dbc820243ae0d59_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_3a2700929bfdf8368dbc820243ae0d59_tuple, 0, const_str_plain_i8 ); Py_INCREF( const_str_plain_i8 );
    PyTuple_SET_ITEM( const_tuple_3a2700929bfdf8368dbc820243ae0d59_tuple, 1, const_str_plain_i16le ); Py_INCREF( const_str_plain_i16le );
    PyTuple_SET_ITEM( const_tuple_3a2700929bfdf8368dbc820243ae0d59_tuple, 2, const_str_plain_i32le ); Py_INCREF( const_str_plain_i32le );
    PyTuple_SET_ITEM( const_tuple_3a2700929bfdf8368dbc820243ae0d59_tuple, 3, const_str_plain_o8 ); Py_INCREF( const_str_plain_o8 );
    PyTuple_SET_ITEM( const_tuple_3a2700929bfdf8368dbc820243ae0d59_tuple, 4, const_str_plain_o16le ); Py_INCREF( const_str_plain_o16le );
    PyTuple_SET_ITEM( const_tuple_3a2700929bfdf8368dbc820243ae0d59_tuple, 5, const_str_plain_o32le ); Py_INCREF( const_str_plain_o32le );
    const_str_plain_BmpImageFile = UNSTREAM_STRING_ASCII( &constant_bin[ 1640 ], 12, 1 );
    const_str_digest_af7895b8e9a7643f2ab308f77e96c311 = UNSTREAM_STRING_ASCII( &constant_bin[ 1954 ], 18, 0 );
    const_str_plain__dib_accept = UNSTREAM_STRING_ASCII( &constant_bin[ 2226 ], 11, 1 );
    const_str_plain_planes = UNSTREAM_STRING_ASCII( &constant_bin[ 2237 ], 6, 1 );
    const_str_digest_bde65f20a0edb67aa82d817e7642f0ab = UNSTREAM_STRING_ASCII( &constant_bin[ 2243 ], 18, 0 );
    const_str_digest_4621a4026644fa18656e149a28e882cc = UNSTREAM_STRING_ASCII( &constant_bin[ 2261 ], 23, 0 );
    const_slice_int_pos_24_int_pos_28_none = PySlice_New( const_int_pos_24, const_int_pos_28, Py_None );
    const_float_39_3701 = UNSTREAM_FLOAT( &constant_bin[ 2284 ] );
    const_dict_05bbcf03645040ac7ffdd29056adaf00 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 2292 ], 288 );
    const_str_plain_data_size = UNSTREAM_STRING_ASCII( &constant_bin[ 2580 ], 9, 1 );
    const_list_fae9db5b2dff7acc072e1a63a7fc7fbf_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_fae9db5b2dff7acc072e1a63a7fc7fbf_list, 0, const_str_plain_r_mask ); Py_INCREF( const_str_plain_r_mask );
    PyList_SET_ITEM( const_list_fae9db5b2dff7acc072e1a63a7fc7fbf_list, 1, const_str_plain_g_mask ); Py_INCREF( const_str_plain_g_mask );
    PyList_SET_ITEM( const_list_fae9db5b2dff7acc072e1a63a7fc7fbf_list, 2, const_str_plain_b_mask ); Py_INCREF( const_str_plain_b_mask );
    PyList_SET_ITEM( const_list_fae9db5b2dff7acc072e1a63a7fc7fbf_list, 3, const_str_plain_a_mask ); Py_INCREF( const_str_plain_a_mask );
    const_str_digest_d95889588ee8c00ac36d1eb48e3389c2 = UNSTREAM_STRING_ASCII( &constant_bin[ 2589 ], 21, 0 );
    const_str_plain_y_flip = UNSTREAM_STRING_ASCII( &constant_bin[ 2610 ], 6, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_PIL$BmpImagePlugin( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_8d1eaf5a3b5b9731739a65a4ffaee591;
static PyCodeObject *codeobj_2ef5b8b98f7babf49d79d600f651cdc2;
static PyCodeObject *codeobj_9a431d03a6469f0f6984fda36b405772;
static PyCodeObject *codeobj_ecc6fb4c94f111dc4d1d1cbf06a4a8d8;
static PyCodeObject *codeobj_c789dea7fb93995d63627c2800a6c64d;
static PyCodeObject *codeobj_1da488e34b64dfe55223e09ba30a3702;
static PyCodeObject *codeobj_ba43fad09fae1d303508d3a353e52d90;
static PyCodeObject *codeobj_c1ff15fbc2b21e907274b08755454ab2;
static PyCodeObject *codeobj_d47584d67535e2b5612fc58bb6518bd0;
static PyCodeObject *codeobj_0951ac842b8dc1d147637f06ba935aeb;
static PyCodeObject *codeobj_91ad53624b63d65e5ed6af7f162fcd41;
static PyCodeObject *codeobj_af5d674ae8579c3b7d41cf1c75d68331;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_d95889588ee8c00ac36d1eb48e3389c2 );
    codeobj_8d1eaf5a3b5b9731739a65a4ffaee591 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 123, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_x_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2ef5b8b98f7babf49d79d600f651cdc2 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 312, const_tuple_str_plain_x_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9a431d03a6469f0f6984fda36b405772 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_531b5f03cfbae3e5d98ea4f6850a6071, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_ecc6fb4c94f111dc4d1d1cbf06a4a8d8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_BmpImageFile, 61, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_c789dea7fb93995d63627c2800a6c64d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_DibImageFile, 275, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_1da488e34b64dfe55223e09ba30a3702 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__accept, 50, const_tuple_str_plain_prefix_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ba43fad09fae1d303508d3a353e52d90 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__bitmap, 79, const_tuple_a4de276af334d6f68ea10b7a0c3a112a_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c1ff15fbc2b21e907274b08755454ab2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__dib_accept, 54, const_tuple_str_plain_prefix_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d47584d67535e2b5612fc58bb6518bd0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__dib_save, 297, const_tuple_str_plain_im_str_plain_fp_str_plain_filename_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0951ac842b8dc1d147637f06ba935aeb = MAKE_CODEOBJ( module_filename_obj, const_str_plain__open, 280, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_91ad53624b63d65e5ed6af7f162fcd41 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__open, 259, const_tuple_str_plain_self_str_plain_head_data_str_plain_offset_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_af5d674ae8579c3b7d41cf1c75d68331 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__save, 301, const_tuple_d1698dc3628e43e9d0bec4dad26c9736_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *PIL$BmpImagePlugin$$$function_3__bitmap$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_1__accept(  );


static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_2__dib_accept(  );


static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_3__bitmap( PyObject *defaults );


static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_4__open(  );


static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_5__open(  );


static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_6__dib_save(  );


static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_7__save( PyObject *defaults );


static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_7__save$$$function_1_lambda(  );


// The module function definitions.
static PyObject *impl_PIL$BmpImagePlugin$$$function_1__accept( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_prefix = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_1da488e34b64dfe55223e09ba30a3702;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_1da488e34b64dfe55223e09ba30a3702 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1da488e34b64dfe55223e09ba30a3702, codeobj_1da488e34b64dfe55223e09ba30a3702, module_PIL$BmpImagePlugin, sizeof(void *) );
    frame_1da488e34b64dfe55223e09ba30a3702 = cache_frame_1da488e34b64dfe55223e09ba30a3702;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1da488e34b64dfe55223e09ba30a3702 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1da488e34b64dfe55223e09ba30a3702 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_prefix );
        tmp_subscribed_name_1 = par_prefix;
        tmp_subscript_name_1 = const_slice_none_int_pos_2_none;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_bytes_digest_181e1eeb195f3bcd8ad8a954f597cb5b;
        tmp_return_value = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1da488e34b64dfe55223e09ba30a3702 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1da488e34b64dfe55223e09ba30a3702 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1da488e34b64dfe55223e09ba30a3702 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1da488e34b64dfe55223e09ba30a3702, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1da488e34b64dfe55223e09ba30a3702->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1da488e34b64dfe55223e09ba30a3702, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1da488e34b64dfe55223e09ba30a3702,
        type_description_1,
        par_prefix
    );


    // Release cached frame.
    if ( frame_1da488e34b64dfe55223e09ba30a3702 == cache_frame_1da488e34b64dfe55223e09ba30a3702 )
    {
        Py_DECREF( frame_1da488e34b64dfe55223e09ba30a3702 );
    }
    cache_frame_1da488e34b64dfe55223e09ba30a3702 = NULL;

    assertFrameObject( frame_1da488e34b64dfe55223e09ba30a3702 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_1__accept );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_prefix );
    Py_DECREF( par_prefix );
    par_prefix = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_prefix );
    Py_DECREF( par_prefix );
    par_prefix = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_1__accept );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$BmpImagePlugin$$$function_2__dib_accept( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_prefix = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c1ff15fbc2b21e907274b08755454ab2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_c1ff15fbc2b21e907274b08755454ab2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c1ff15fbc2b21e907274b08755454ab2, codeobj_c1ff15fbc2b21e907274b08755454ab2, module_PIL$BmpImagePlugin, sizeof(void *) );
    frame_c1ff15fbc2b21e907274b08755454ab2 = cache_frame_c1ff15fbc2b21e907274b08755454ab2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c1ff15fbc2b21e907274b08755454ab2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c1ff15fbc2b21e907274b08755454ab2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_prefix );
        tmp_subscribed_name_1 = par_prefix;
        tmp_subscript_name_1 = const_slice_none_int_pos_4_none;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_c1ff15fbc2b21e907274b08755454ab2->m_frame.f_lineno = 55;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_compexpr_left_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = LIST_COPY( const_list_int_pos_12_int_pos_40_int_pos_64_int_pos_108_int_pos_124_list );
        tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res == 1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c1ff15fbc2b21e907274b08755454ab2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c1ff15fbc2b21e907274b08755454ab2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c1ff15fbc2b21e907274b08755454ab2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c1ff15fbc2b21e907274b08755454ab2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c1ff15fbc2b21e907274b08755454ab2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c1ff15fbc2b21e907274b08755454ab2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c1ff15fbc2b21e907274b08755454ab2,
        type_description_1,
        par_prefix
    );


    // Release cached frame.
    if ( frame_c1ff15fbc2b21e907274b08755454ab2 == cache_frame_c1ff15fbc2b21e907274b08755454ab2 )
    {
        Py_DECREF( frame_c1ff15fbc2b21e907274b08755454ab2 );
    }
    cache_frame_c1ff15fbc2b21e907274b08755454ab2 = NULL;

    assertFrameObject( frame_c1ff15fbc2b21e907274b08755454ab2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_2__dib_accept );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_prefix );
    Py_DECREF( par_prefix );
    par_prefix = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_prefix );
    Py_DECREF( par_prefix );
    par_prefix = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_2__dib_accept );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$BmpImagePlugin$$$function_3__bitmap( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_header = python_pars[ 1 ];
    PyObject *par_offset = python_pars[ 2 ];
    PyObject *var_read = NULL;
    PyObject *var_seek = NULL;
    PyObject *var_file_info = NULL;
    PyObject *var_header_data = NULL;
    PyObject *var_idx = NULL;
    PyObject *var_mask = NULL;
    PyObject *var_raw_mode = NULL;
    PyObject *var_SUPPORTED = NULL;
    PyObject *var_MASK_MODES = NULL;
    PyObject *var_padding = NULL;
    PyObject *var_palette = NULL;
    PyObject *var_greyscale = NULL;
    PyObject *var_indices = NULL;
    PyObject *var_ind = NULL;
    PyObject *var_val = NULL;
    PyObject *var_rgb = NULL;
    PyObject *tmp_comparison_chain_1__comparison_result = NULL;
    PyObject *tmp_comparison_chain_1__operand_2 = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    PyObject *tmp_tuple_unpack_4__element_1 = NULL;
    PyObject *tmp_tuple_unpack_4__element_2 = NULL;
    PyObject *tmp_tuple_unpack_4__source_iter = NULL;
    PyObject *tmp_tuple_unpack_5__element_1 = NULL;
    PyObject *tmp_tuple_unpack_5__element_2 = NULL;
    PyObject *tmp_tuple_unpack_5__source_iter = NULL;
    struct Nuitka_FrameObject *frame_ba43fad09fae1d303508d3a353e52d90;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    int tmp_res;
    bool tmp_result;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    static struct Nuitka_FrameObject *cache_frame_ba43fad09fae1d303508d3a353e52d90 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ba43fad09fae1d303508d3a353e52d90, codeobj_ba43fad09fae1d303508d3a353e52d90, module_PIL$BmpImagePlugin, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ba43fad09fae1d303508d3a353e52d90 = cache_frame_ba43fad09fae1d303508d3a353e52d90;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ba43fad09fae1d303508d3a353e52d90 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ba43fad09fae1d303508d3a353e52d90 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_fp );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_read );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_iter_arg_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_iter_arg_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_fp );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_iter_arg_1 );

            exception_lineno = 81;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_seek );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_iter_arg_1 );

            exception_lineno = 81;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_2;
        }
        PyTuple_SET_ITEM( tmp_iter_arg_1, 1, tmp_tuple_element_1 );
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_2;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooooooooo";
            exception_lineno = 81;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooooooooo";
            exception_lineno = 81;
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_4 = tmp_tuple_unpack_1__element_1;
        assert( var_read == NULL );
        Py_INCREF( tmp_assign_source_4 );
        var_read = tmp_assign_source_4;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_2;
        assert( var_seek == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_seek = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_header );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_header );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 82;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var_seek );
            tmp_called_name_1 = var_seek;
            CHECK_OBJECT( par_header );
            tmp_args_element_name_1 = par_header;
            frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 83;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = PyDict_New();
        assert( var_file_info == NULL );
        var_file_info = tmp_assign_source_6;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 86;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( var_read );
        tmp_called_name_3 = var_read;
        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 86;
        tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_int_pos_4_tuple, 0 ) );

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 86;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_dictset_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_file_info );
        tmp_dictset_dict = var_file_info;
        tmp_dictset_key = const_str_plain_header_size;
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        assert( !(tmp_res != 0) );
    }
    tmp_dictset_value = const_int_neg_1;
    CHECK_OBJECT( var_file_info );
    tmp_dictset_dict = var_file_info;
    tmp_dictset_key = const_str_plain_direction;
    tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
    assert( !(tmp_res != 0) );
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_6;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_left_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_right_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_ImageFile );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ImageFile );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 91;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_2;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__safe_read );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_fp );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 91;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_1 = var_file_info;
        tmp_key_name_1 = const_str_plain_header_size;
        tmp_left_name_1 = DICT_GET_ITEM( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 92;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = const_int_pos_4;
        tmp_args_element_name_4 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 92;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 91;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_header_data == NULL );
        var_header_data = tmp_assign_source_7;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_2 = var_file_info;
        tmp_key_name_2 = const_str_plain_header_size;
        tmp_compexpr_left_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_12;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i16 );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i16 );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i16" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 97;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_5 = tmp_mvar_value_3;
            CHECK_OBJECT( var_header_data );
            tmp_subscribed_name_1 = var_header_data;
            tmp_subscript_name_1 = const_slice_int_0_int_pos_2_none;
            tmp_args_element_name_5 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_args_element_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 97;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 97;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 97;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_file_info );
            tmp_dictset_dict = var_file_info;
            tmp_dictset_key = const_str_plain_width;
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            assert( !(tmp_res != 0) );
        }
        {
            PyObject *tmp_called_name_6;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i16 );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i16 );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i16" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 98;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_6 = tmp_mvar_value_4;
            CHECK_OBJECT( var_header_data );
            tmp_subscribed_name_2 = var_header_data;
            tmp_subscript_name_2 = const_slice_int_pos_2_int_pos_4_none;
            tmp_args_element_name_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 98;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 98;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 98;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_file_info );
            tmp_dictset_dict = var_file_info;
            tmp_dictset_key = const_str_plain_height;
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            assert( !(tmp_res != 0) );
        }
        {
            PyObject *tmp_called_name_7;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_subscript_name_3;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i16 );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i16 );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i16" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 99;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_7 = tmp_mvar_value_5;
            CHECK_OBJECT( var_header_data );
            tmp_subscribed_name_3 = var_header_data;
            tmp_subscript_name_3 = const_slice_int_pos_4_int_pos_6_none;
            tmp_args_element_name_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
            if ( tmp_args_element_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 99;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 99;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_file_info );
            tmp_dictset_dict = var_file_info;
            tmp_dictset_key = const_str_plain_planes;
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            assert( !(tmp_res != 0) );
        }
        {
            PyObject *tmp_called_name_8;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_subscript_name_4;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i16 );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i16 );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i16" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 100;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_8 = tmp_mvar_value_6;
            CHECK_OBJECT( var_header_data );
            tmp_subscribed_name_4 = var_header_data;
            tmp_subscript_name_4 = const_slice_int_pos_6_int_pos_8_none;
            tmp_args_element_name_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
            if ( tmp_args_element_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 100;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_file_info );
            tmp_dictset_dict = var_file_info;
            tmp_dictset_key = const_str_plain_bits;
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            assert( !(tmp_res != 0) );
        }
        {
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( par_self );
            tmp_source_name_7 = par_self;
            tmp_dictset_value = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_RAW );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 101;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_file_info );
            tmp_dictset_dict = var_file_info;
            tmp_dictset_key = const_str_plain_compression;
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            assert( !(tmp_res != 0) );
        }
        tmp_dictset_value = const_int_pos_3;
        CHECK_OBJECT( var_file_info );
        tmp_dictset_dict = var_file_info;
        tmp_dictset_key = const_str_plain_palette_padding;
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        assert( !(tmp_res != 0) );
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_dict_name_3;
            PyObject *tmp_key_name_3;
            CHECK_OBJECT( var_file_info );
            tmp_dict_name_3 = var_file_info;
            tmp_key_name_3 = const_str_plain_header_size;
            tmp_compexpr_left_2 = DICT_GET_ITEM( tmp_dict_name_3, tmp_key_name_3 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_tuple_int_pos_40_int_pos_64_int_pos_108_int_pos_124_tuple;
            tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 106;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                PyObject *tmp_called_name_9;
                PyObject *tmp_mvar_value_7;
                PyObject *tmp_args_element_name_9;
                PyObject *tmp_subscribed_name_5;
                PyObject *tmp_subscript_name_5;
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i8 );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i8 );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i8" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 107;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_9 = tmp_mvar_value_7;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_5 = var_header_data;
                tmp_subscript_name_5 = const_int_pos_7;
                tmp_args_element_name_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 7 );
                if ( tmp_args_element_name_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 107;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 107;
                {
                    PyObject *call_args[] = { tmp_args_element_name_9 };
                    tmp_compexpr_left_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
                }

                Py_DECREF( tmp_args_element_name_9 );
                if ( tmp_compexpr_left_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 107;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_3 = const_int_pos_255;
                tmp_dictset_value = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                Py_DECREF( tmp_compexpr_left_3 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 107;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_file_info );
                tmp_dictset_dict = var_file_info;
                tmp_dictset_key = const_str_plain_y_flip;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_dict_name_4;
                PyObject *tmp_key_name_4;
                PyObject *tmp_dict_value_1;
                int tmp_truth_name_2;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_4 = var_file_info;
                tmp_key_name_4 = const_str_plain_y_flip;
                tmp_dict_value_1 = DICT_GET_ITEM( tmp_dict_name_4, tmp_key_name_4 );
                if ( tmp_dict_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 108;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_truth_name_2 = CHECK_IF_TRUE( tmp_dict_value_1 );
                if ( tmp_truth_name_2 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_dict_value_1 );

                    exception_lineno = 108;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_dict_value_1 );
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_1;
                }
                else
                {
                    goto condexpr_false_1;
                }
                condexpr_true_1:;
                tmp_dictset_value = const_int_pos_1;
                goto condexpr_end_1;
                condexpr_false_1:;
                tmp_dictset_value = const_int_neg_1;
                condexpr_end_1:;
                CHECK_OBJECT( var_file_info );
                tmp_dictset_dict = var_file_info;
                tmp_dictset_key = const_str_plain_direction;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            {
                PyObject *tmp_called_name_10;
                PyObject *tmp_mvar_value_8;
                PyObject *tmp_args_element_name_10;
                PyObject *tmp_subscribed_name_6;
                PyObject *tmp_subscript_name_6;
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 109;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_10 = tmp_mvar_value_8;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_6 = var_header_data;
                tmp_subscript_name_6 = const_slice_int_0_int_pos_4_none;
                tmp_args_element_name_10 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_6 );
                if ( tmp_args_element_name_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 109;
                {
                    PyObject *call_args[] = { tmp_args_element_name_10 };
                    tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
                }

                Py_DECREF( tmp_args_element_name_10 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 109;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_file_info );
                tmp_dictset_dict = var_file_info;
                tmp_dictset_key = const_str_plain_width;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_operand_name_1;
                PyObject *tmp_dict_name_5;
                PyObject *tmp_key_name_5;
                PyObject *tmp_called_name_11;
                PyObject *tmp_mvar_value_9;
                PyObject *tmp_args_element_name_11;
                PyObject *tmp_subscribed_name_7;
                PyObject *tmp_subscript_name_7;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_called_name_12;
                PyObject *tmp_mvar_value_10;
                PyObject *tmp_args_element_name_12;
                PyObject *tmp_subscribed_name_8;
                PyObject *tmp_subscript_name_8;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_5 = var_file_info;
                tmp_key_name_5 = const_str_plain_y_flip;
                tmp_operand_name_1 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
                if ( tmp_operand_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 111;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                Py_DECREF( tmp_operand_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 111;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_2;
                }
                else
                {
                    goto condexpr_false_2;
                }
                condexpr_true_2:;
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
                }

                if ( tmp_mvar_value_9 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 110;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_11 = tmp_mvar_value_9;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_7 = var_header_data;
                tmp_subscript_name_7 = const_slice_int_pos_4_int_pos_8_none;
                tmp_args_element_name_11 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
                if ( tmp_args_element_name_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 110;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 110;
                {
                    PyObject *call_args[] = { tmp_args_element_name_11 };
                    tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
                }

                Py_DECREF( tmp_args_element_name_11 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 110;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                goto condexpr_end_2;
                condexpr_false_2:;
                tmp_left_name_2 = const_int_pos_4294967296;
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

                if (unlikely( tmp_mvar_value_10 == NULL ))
                {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
                }

                if ( tmp_mvar_value_10 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 112;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_12 = tmp_mvar_value_10;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_8 = var_header_data;
                tmp_subscript_name_8 = const_slice_int_pos_4_int_pos_8_none;
                tmp_args_element_name_12 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_8, tmp_subscript_name_8 );
                if ( tmp_args_element_name_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 112;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 112;
                {
                    PyObject *call_args[] = { tmp_args_element_name_12 };
                    tmp_right_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
                }

                Py_DECREF( tmp_args_element_name_12 );
                if ( tmp_right_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 112;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_dictset_value = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 112;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                condexpr_end_2:;
                CHECK_OBJECT( var_file_info );
                tmp_dictset_dict = var_file_info;
                tmp_dictset_key = const_str_plain_height;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            {
                PyObject *tmp_called_name_13;
                PyObject *tmp_mvar_value_11;
                PyObject *tmp_args_element_name_13;
                PyObject *tmp_subscribed_name_9;
                PyObject *tmp_subscript_name_9;
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i16 );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i16 );
                }

                if ( tmp_mvar_value_11 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i16" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 113;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_13 = tmp_mvar_value_11;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_9 = var_header_data;
                tmp_subscript_name_9 = const_slice_int_pos_8_int_pos_10_none;
                tmp_args_element_name_13 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_9, tmp_subscript_name_9 );
                if ( tmp_args_element_name_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 113;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 113;
                {
                    PyObject *call_args[] = { tmp_args_element_name_13 };
                    tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
                }

                Py_DECREF( tmp_args_element_name_13 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 113;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_file_info );
                tmp_dictset_dict = var_file_info;
                tmp_dictset_key = const_str_plain_planes;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            {
                PyObject *tmp_called_name_14;
                PyObject *tmp_mvar_value_12;
                PyObject *tmp_args_element_name_14;
                PyObject *tmp_subscribed_name_10;
                PyObject *tmp_subscript_name_10;
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i16 );

                if (unlikely( tmp_mvar_value_12 == NULL ))
                {
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i16 );
                }

                if ( tmp_mvar_value_12 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i16" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 114;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_14 = tmp_mvar_value_12;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_10 = var_header_data;
                tmp_subscript_name_10 = const_slice_int_pos_10_int_pos_12_none;
                tmp_args_element_name_14 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_10, tmp_subscript_name_10 );
                if ( tmp_args_element_name_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 114;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 114;
                {
                    PyObject *call_args[] = { tmp_args_element_name_14 };
                    tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
                }

                Py_DECREF( tmp_args_element_name_14 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 114;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_file_info );
                tmp_dictset_dict = var_file_info;
                tmp_dictset_key = const_str_plain_bits;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            {
                PyObject *tmp_called_name_15;
                PyObject *tmp_mvar_value_13;
                PyObject *tmp_args_element_name_15;
                PyObject *tmp_subscribed_name_11;
                PyObject *tmp_subscript_name_11;
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

                if (unlikely( tmp_mvar_value_13 == NULL ))
                {
                    tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
                }

                if ( tmp_mvar_value_13 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 115;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_15 = tmp_mvar_value_13;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_11 = var_header_data;
                tmp_subscript_name_11 = const_slice_int_pos_12_int_pos_16_none;
                tmp_args_element_name_15 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_11, tmp_subscript_name_11 );
                if ( tmp_args_element_name_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 115;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 115;
                {
                    PyObject *call_args[] = { tmp_args_element_name_15 };
                    tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
                }

                Py_DECREF( tmp_args_element_name_15 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 115;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_file_info );
                tmp_dictset_dict = var_file_info;
                tmp_dictset_key = const_str_plain_compression;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            {
                PyObject *tmp_called_name_16;
                PyObject *tmp_mvar_value_14;
                PyObject *tmp_args_element_name_16;
                PyObject *tmp_subscribed_name_12;
                PyObject *tmp_subscript_name_12;
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

                if (unlikely( tmp_mvar_value_14 == NULL ))
                {
                    tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
                }

                if ( tmp_mvar_value_14 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 117;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_16 = tmp_mvar_value_14;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_12 = var_header_data;
                tmp_subscript_name_12 = const_slice_int_pos_16_int_pos_20_none;
                tmp_args_element_name_16 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_12, tmp_subscript_name_12 );
                if ( tmp_args_element_name_16 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 117;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 117;
                {
                    PyObject *call_args[] = { tmp_args_element_name_16 };
                    tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
                }

                Py_DECREF( tmp_args_element_name_16 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 117;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_file_info );
                tmp_dictset_dict = var_file_info;
                tmp_dictset_key = const_str_plain_data_size;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            {
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_called_name_17;
                PyObject *tmp_mvar_value_15;
                PyObject *tmp_args_element_name_17;
                PyObject *tmp_subscribed_name_13;
                PyObject *tmp_subscript_name_13;
                PyObject *tmp_called_name_18;
                PyObject *tmp_mvar_value_16;
                PyObject *tmp_args_element_name_18;
                PyObject *tmp_subscribed_name_14;
                PyObject *tmp_subscript_name_14;
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

                if (unlikely( tmp_mvar_value_15 == NULL ))
                {
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
                }

                if ( tmp_mvar_value_15 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 118;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_17 = tmp_mvar_value_15;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_13 = var_header_data;
                tmp_subscript_name_13 = const_slice_int_pos_20_int_pos_24_none;
                tmp_args_element_name_17 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_13, tmp_subscript_name_13 );
                if ( tmp_args_element_name_17 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 118;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 118;
                {
                    PyObject *call_args[] = { tmp_args_element_name_17 };
                    tmp_tuple_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, call_args );
                }

                Py_DECREF( tmp_args_element_name_17 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 118;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_dictset_value = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_dictset_value, 0, tmp_tuple_element_2 );
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

                if (unlikely( tmp_mvar_value_16 == NULL ))
                {
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
                }

                if ( tmp_mvar_value_16 == NULL )
                {
                    Py_DECREF( tmp_dictset_value );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 119;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_18 = tmp_mvar_value_16;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_14 = var_header_data;
                tmp_subscript_name_14 = const_slice_int_pos_24_int_pos_28_none;
                tmp_args_element_name_18 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_14, tmp_subscript_name_14 );
                if ( tmp_args_element_name_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_dictset_value );

                    exception_lineno = 119;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 119;
                {
                    PyObject *call_args[] = { tmp_args_element_name_18 };
                    tmp_tuple_element_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
                }

                Py_DECREF( tmp_args_element_name_18 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_dictset_value );

                    exception_lineno = 119;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                PyTuple_SET_ITEM( tmp_dictset_value, 1, tmp_tuple_element_2 );
                CHECK_OBJECT( var_file_info );
                tmp_dictset_dict = var_file_info;
                tmp_dictset_key = const_str_plain_pixels_per_meter;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            {
                PyObject *tmp_called_name_19;
                PyObject *tmp_mvar_value_17;
                PyObject *tmp_args_element_name_19;
                PyObject *tmp_subscribed_name_15;
                PyObject *tmp_subscript_name_15;
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

                if (unlikely( tmp_mvar_value_17 == NULL ))
                {
                    tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
                }

                if ( tmp_mvar_value_17 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 120;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_19 = tmp_mvar_value_17;
                CHECK_OBJECT( var_header_data );
                tmp_subscribed_name_15 = var_header_data;
                tmp_subscript_name_15 = const_slice_int_pos_28_int_pos_32_none;
                tmp_args_element_name_19 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_15, tmp_subscript_name_15 );
                if ( tmp_args_element_name_19 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 120;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 120;
                {
                    PyObject *call_args[] = { tmp_args_element_name_19 };
                    tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
                }

                Py_DECREF( tmp_args_element_name_19 );
                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 120;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_file_info );
                tmp_dictset_dict = var_file_info;
                tmp_dictset_key = const_str_plain_colors;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                assert( !(tmp_res != 0) );
            }
            tmp_dictset_value = const_int_pos_4;
            CHECK_OBJECT( var_file_info );
            tmp_dictset_dict = var_file_info;
            tmp_dictset_key = const_str_plain_palette_padding;
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            assert( !(tmp_res != 0) );
            {
                PyObject *tmp_ass_subvalue_1;
                PyObject *tmp_tuple_arg_1;
                PyObject *tmp_ass_subscribed_1;
                PyObject *tmp_source_name_8;
                PyObject *tmp_ass_subscript_1;
                {
                    PyObject *tmp_assign_source_8;
                    PyObject *tmp_iter_arg_2;
                    PyObject *tmp_dict_name_6;
                    PyObject *tmp_key_name_6;
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_6 = var_file_info;
                    tmp_key_name_6 = const_str_plain_pixels_per_meter;
                    tmp_iter_arg_2 = DICT_GET_ITEM( tmp_dict_name_6, tmp_key_name_6 );
                    if ( tmp_iter_arg_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 123;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_8 = MAKE_ITERATOR( tmp_iter_arg_2 );
                    Py_DECREF( tmp_iter_arg_2 );
                    if ( tmp_assign_source_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 123;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( tmp_genexpr_1__$0 == NULL );
                    tmp_genexpr_1__$0 = tmp_assign_source_8;
                }
                // Tried code:
                tmp_tuple_arg_1 = PIL$BmpImagePlugin$$$function_3__bitmap$$$genexpr_1_genexpr_maker();

                ((struct Nuitka_GeneratorObject *)tmp_tuple_arg_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


                goto try_return_handler_4;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_3__bitmap );
                return NULL;
                // Return handler code:
                try_return_handler_4:;
                CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                Py_DECREF( tmp_genexpr_1__$0 );
                tmp_genexpr_1__$0 = NULL;

                goto outline_result_1;
                // End of try:
                CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                Py_DECREF( tmp_genexpr_1__$0 );
                tmp_genexpr_1__$0 = NULL;

                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_3__bitmap );
                return NULL;
                outline_result_1:;
                tmp_ass_subvalue_1 = PySequence_Tuple( tmp_tuple_arg_1 );
                Py_DECREF( tmp_tuple_arg_1 );
                if ( tmp_ass_subvalue_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 122;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_self );
                tmp_source_name_8 = par_self;
                tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_info );
                if ( tmp_ass_subscribed_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_ass_subvalue_1 );

                    exception_lineno = 122;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_ass_subscript_1 = const_str_plain_dpi;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
                Py_DECREF( tmp_ass_subscribed_1 );
                Py_DECREF( tmp_ass_subvalue_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 122;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                PyObject *tmp_dict_name_7;
                PyObject *tmp_key_name_7;
                PyObject *tmp_source_name_9;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_7 = var_file_info;
                tmp_key_name_7 = const_str_plain_compression;
                tmp_compexpr_left_4 = DICT_GET_ITEM( tmp_dict_name_7, tmp_key_name_7 );
                if ( tmp_compexpr_left_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 124;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_self );
                tmp_source_name_9 = par_self;
                tmp_compexpr_right_4 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_BITFIELDS );
                if ( tmp_compexpr_right_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_compexpr_left_4 );

                    exception_lineno = 124;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                Py_DECREF( tmp_compexpr_left_4 );
                Py_DECREF( tmp_compexpr_right_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 124;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    nuitka_bool tmp_condition_result_7;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    PyObject *tmp_len_arg_1;
                    CHECK_OBJECT( var_header_data );
                    tmp_len_arg_1 = var_header_data;
                    tmp_compexpr_left_5 = BUILTIN_LEN( tmp_len_arg_1 );
                    if ( tmp_compexpr_left_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 125;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_5 = const_int_pos_52;
                    tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                    Py_DECREF( tmp_compexpr_left_5 );
                    assert( !(tmp_res == -1) );
                    tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_assign_source_9;
                        PyObject *tmp_iter_arg_3;
                        PyObject *tmp_called_name_20;
                        PyObject *tmp_call_arg_element_1;
                        tmp_called_name_20 = (PyObject *)&PyEnum_Type;
                        tmp_call_arg_element_1 = LIST_COPY( const_list_fae9db5b2dff7acc072e1a63a7fc7fbf_list );
                        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 126;
                        {
                            PyObject *call_args[] = { tmp_call_arg_element_1 };
                            tmp_iter_arg_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
                        }

                        Py_DECREF( tmp_call_arg_element_1 );
                        if ( tmp_iter_arg_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 126;
                            type_description_1 = "ooooooooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_assign_source_9 = MAKE_ITERATOR( tmp_iter_arg_3 );
                        Py_DECREF( tmp_iter_arg_3 );
                        if ( tmp_assign_source_9 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 126;
                            type_description_1 = "ooooooooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        assert( tmp_for_loop_1__for_iterator == NULL );
                        tmp_for_loop_1__for_iterator = tmp_assign_source_9;
                    }
                    // Tried code:
                    loop_start_1:;
                    {
                        PyObject *tmp_next_source_1;
                        PyObject *tmp_assign_source_10;
                        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
                        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
                        tmp_assign_source_10 = ITERATOR_NEXT( tmp_next_source_1 );
                        if ( tmp_assign_source_10 == NULL )
                        {
                            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                            {

                                goto loop_end_1;
                            }
                            else
                            {

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                type_description_1 = "ooooooooooooooooooo";
                                exception_lineno = 126;
                                goto try_except_handler_5;
                            }
                        }

                        {
                            PyObject *old = tmp_for_loop_1__iter_value;
                            tmp_for_loop_1__iter_value = tmp_assign_source_10;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_11;
                        PyObject *tmp_iter_arg_4;
                        CHECK_OBJECT( tmp_for_loop_1__iter_value );
                        tmp_iter_arg_4 = tmp_for_loop_1__iter_value;
                        tmp_assign_source_11 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
                        if ( tmp_assign_source_11 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 126;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_6;
                        }
                        {
                            PyObject *old = tmp_tuple_unpack_2__source_iter;
                            tmp_tuple_unpack_2__source_iter = tmp_assign_source_11;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_12;
                        PyObject *tmp_unpack_3;
                        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
                        tmp_assign_source_12 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
                        if ( tmp_assign_source_12 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooooooooooooooooo";
                            exception_lineno = 126;
                            goto try_except_handler_7;
                        }
                        {
                            PyObject *old = tmp_tuple_unpack_2__element_1;
                            tmp_tuple_unpack_2__element_1 = tmp_assign_source_12;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_13;
                        PyObject *tmp_unpack_4;
                        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
                        tmp_assign_source_13 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
                        if ( tmp_assign_source_13 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooooooooooooooooo";
                            exception_lineno = 126;
                            goto try_except_handler_7;
                        }
                        {
                            PyObject *old = tmp_tuple_unpack_2__element_2;
                            tmp_tuple_unpack_2__element_2 = tmp_assign_source_13;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_iterator_name_1;
                        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                        tmp_iterator_name_1 = tmp_tuple_unpack_2__source_iter;
                        // Check if iterator has left-over elements.
                        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

                        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

                        if (likely( tmp_iterator_attempt == NULL ))
                        {
                            PyObject *error = GET_ERROR_OCCURRED();

                            if ( error != NULL )
                            {
                                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                                {
                                    CLEAR_ERROR_OCCURRED();
                                }
                                else
                                {
                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                    type_description_1 = "ooooooooooooooooooo";
                                    exception_lineno = 126;
                                    goto try_except_handler_7;
                                }
                            }
                        }
                        else
                        {
                            Py_DECREF( tmp_iterator_attempt );

                            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "ooooooooooooooooooo";
                            exception_lineno = 126;
                            goto try_except_handler_7;
                        }
                    }
                    goto try_end_3;
                    // Exception handler code:
                    try_except_handler_7:;
                    exception_keeper_type_3 = exception_type;
                    exception_keeper_value_3 = exception_value;
                    exception_keeper_tb_3 = exception_tb;
                    exception_keeper_lineno_3 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
                    Py_DECREF( tmp_tuple_unpack_2__source_iter );
                    tmp_tuple_unpack_2__source_iter = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_3;
                    exception_value = exception_keeper_value_3;
                    exception_tb = exception_keeper_tb_3;
                    exception_lineno = exception_keeper_lineno_3;

                    goto try_except_handler_6;
                    // End of try:
                    try_end_3:;
                    goto try_end_4;
                    // Exception handler code:
                    try_except_handler_6:;
                    exception_keeper_type_4 = exception_type;
                    exception_keeper_value_4 = exception_value;
                    exception_keeper_tb_4 = exception_tb;
                    exception_keeper_lineno_4 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
                    tmp_tuple_unpack_2__element_1 = NULL;

                    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
                    tmp_tuple_unpack_2__element_2 = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_4;
                    exception_value = exception_keeper_value_4;
                    exception_tb = exception_keeper_tb_4;
                    exception_lineno = exception_keeper_lineno_4;

                    goto try_except_handler_5;
                    // End of try:
                    try_end_4:;
                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
                    Py_DECREF( tmp_tuple_unpack_2__source_iter );
                    tmp_tuple_unpack_2__source_iter = NULL;

                    {
                        PyObject *tmp_assign_source_14;
                        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
                        tmp_assign_source_14 = tmp_tuple_unpack_2__element_1;
                        {
                            PyObject *old = var_idx;
                            var_idx = tmp_assign_source_14;
                            Py_INCREF( var_idx );
                            Py_XDECREF( old );
                        }

                    }
                    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
                    tmp_tuple_unpack_2__element_1 = NULL;

                    {
                        PyObject *tmp_assign_source_15;
                        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
                        tmp_assign_source_15 = tmp_tuple_unpack_2__element_2;
                        {
                            PyObject *old = var_mask;
                            var_mask = tmp_assign_source_15;
                            Py_INCREF( var_mask );
                            Py_XDECREF( old );
                        }

                    }
                    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
                    tmp_tuple_unpack_2__element_2 = NULL;

                    {
                        PyObject *tmp_called_name_21;
                        PyObject *tmp_mvar_value_18;
                        PyObject *tmp_args_element_name_20;
                        PyObject *tmp_subscribed_name_16;
                        PyObject *tmp_subscript_name_16;
                        PyObject *tmp_start_name_1;
                        PyObject *tmp_left_name_3;
                        PyObject *tmp_right_name_3;
                        PyObject *tmp_left_name_4;
                        PyObject *tmp_right_name_4;
                        PyObject *tmp_stop_name_1;
                        PyObject *tmp_left_name_5;
                        PyObject *tmp_right_name_5;
                        PyObject *tmp_left_name_6;
                        PyObject *tmp_right_name_6;
                        PyObject *tmp_step_name_1;
                        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

                        if (unlikely( tmp_mvar_value_18 == NULL ))
                        {
                            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
                        }

                        if ( tmp_mvar_value_18 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 130;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_5;
                        }

                        tmp_called_name_21 = tmp_mvar_value_18;
                        CHECK_OBJECT( var_header_data );
                        tmp_subscribed_name_16 = var_header_data;
                        tmp_left_name_3 = const_int_pos_36;
                        CHECK_OBJECT( var_idx );
                        tmp_left_name_4 = var_idx;
                        tmp_right_name_4 = const_int_pos_4;
                        tmp_right_name_3 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_4, tmp_right_name_4 );
                        if ( tmp_right_name_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 131;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_5;
                        }
                        tmp_start_name_1 = BINARY_OPERATION_ADD_LONG_OBJECT( tmp_left_name_3, tmp_right_name_3 );
                        Py_DECREF( tmp_right_name_3 );
                        if ( tmp_start_name_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 131;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_5;
                        }
                        tmp_left_name_5 = const_int_pos_40;
                        CHECK_OBJECT( var_idx );
                        tmp_left_name_6 = var_idx;
                        tmp_right_name_6 = const_int_pos_4;
                        tmp_right_name_5 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_6, tmp_right_name_6 );
                        if ( tmp_right_name_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_start_name_1 );

                            exception_lineno = 131;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_5;
                        }
                        tmp_stop_name_1 = BINARY_OPERATION_ADD_LONG_OBJECT( tmp_left_name_5, tmp_right_name_5 );
                        Py_DECREF( tmp_right_name_5 );
                        if ( tmp_stop_name_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_start_name_1 );

                            exception_lineno = 131;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_5;
                        }
                        tmp_step_name_1 = Py_None;
                        tmp_subscript_name_16 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
                        Py_DECREF( tmp_start_name_1 );
                        Py_DECREF( tmp_stop_name_1 );
                        assert( !(tmp_subscript_name_16 == NULL) );
                        tmp_args_element_name_20 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_16, tmp_subscript_name_16 );
                        Py_DECREF( tmp_subscript_name_16 );
                        if ( tmp_args_element_name_20 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 131;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_5;
                        }
                        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 130;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_20 };
                            tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, call_args );
                        }

                        Py_DECREF( tmp_args_element_name_20 );
                        if ( tmp_dictset_value == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 130;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_5;
                        }
                        CHECK_OBJECT( var_file_info );
                        tmp_dictset_dict = var_file_info;
                        CHECK_OBJECT( var_mask );
                        tmp_dictset_key = var_mask;
                        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                        Py_DECREF( tmp_dictset_value );
                        if ( tmp_res != 0 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 130;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_5;
                        }
                    }
                    if ( CONSIDER_THREADING() == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 126;
                        type_description_1 = "ooooooooooooooooooo";
                        goto try_except_handler_5;
                    }
                    goto loop_start_1;
                    loop_end_1:;
                    goto try_end_5;
                    // Exception handler code:
                    try_except_handler_5:;
                    exception_keeper_type_5 = exception_type;
                    exception_keeper_value_5 = exception_value;
                    exception_keeper_tb_5 = exception_tb;
                    exception_keeper_lineno_5 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_for_loop_1__iter_value );
                    tmp_for_loop_1__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                    Py_DECREF( tmp_for_loop_1__for_iterator );
                    tmp_for_loop_1__for_iterator = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_5;
                    exception_value = exception_keeper_value_5;
                    exception_tb = exception_keeper_tb_5;
                    exception_lineno = exception_keeper_lineno_5;

                    goto frame_exception_exit_1;
                    // End of try:
                    try_end_5:;
                    Py_XDECREF( tmp_for_loop_1__iter_value );
                    tmp_for_loop_1__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                    Py_DECREF( tmp_for_loop_1__for_iterator );
                    tmp_for_loop_1__for_iterator = NULL;

                    goto branch_end_5;
                    branch_no_5:;
                    tmp_dictset_value = const_int_0;
                    CHECK_OBJECT( var_file_info );
                    tmp_dictset_dict = var_file_info;
                    tmp_dictset_key = const_str_plain_a_mask;
                    tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                    assert( !(tmp_res != 0) );
                    {
                        PyObject *tmp_assign_source_16;
                        PyObject *tmp_iter_arg_5;
                        tmp_iter_arg_5 = const_tuple_str_plain_r_mask_str_plain_g_mask_str_plain_b_mask_tuple;
                        tmp_assign_source_16 = MAKE_ITERATOR( tmp_iter_arg_5 );
                        assert( !(tmp_assign_source_16 == NULL) );
                        assert( tmp_for_loop_2__for_iterator == NULL );
                        tmp_for_loop_2__for_iterator = tmp_assign_source_16;
                    }
                    // Tried code:
                    loop_start_2:;
                    {
                        PyObject *tmp_next_source_2;
                        PyObject *tmp_assign_source_17;
                        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
                        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
                        tmp_assign_source_17 = ITERATOR_NEXT( tmp_next_source_2 );
                        if ( tmp_assign_source_17 == NULL )
                        {
                            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                            {

                                goto loop_end_2;
                            }
                            else
                            {

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                type_description_1 = "ooooooooooooooooooo";
                                exception_lineno = 143;
                                goto try_except_handler_8;
                            }
                        }

                        {
                            PyObject *old = tmp_for_loop_2__iter_value;
                            tmp_for_loop_2__iter_value = tmp_assign_source_17;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_18;
                        CHECK_OBJECT( tmp_for_loop_2__iter_value );
                        tmp_assign_source_18 = tmp_for_loop_2__iter_value;
                        {
                            PyObject *old = var_mask;
                            var_mask = tmp_assign_source_18;
                            Py_INCREF( var_mask );
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_called_name_22;
                        PyObject *tmp_mvar_value_19;
                        PyObject *tmp_args_element_name_21;
                        PyObject *tmp_called_name_23;
                        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

                        if (unlikely( tmp_mvar_value_19 == NULL ))
                        {
                            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
                        }

                        if ( tmp_mvar_value_19 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 144;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_8;
                        }

                        tmp_called_name_22 = tmp_mvar_value_19;
                        CHECK_OBJECT( var_read );
                        tmp_called_name_23 = var_read;
                        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 144;
                        tmp_args_element_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, &PyTuple_GET_ITEM( const_tuple_int_pos_4_tuple, 0 ) );

                        if ( tmp_args_element_name_21 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 144;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_8;
                        }
                        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 144;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_21 };
                            tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
                        }

                        Py_DECREF( tmp_args_element_name_21 );
                        if ( tmp_dictset_value == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 144;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_8;
                        }
                        CHECK_OBJECT( var_file_info );
                        tmp_dictset_dict = var_file_info;
                        CHECK_OBJECT( var_mask );
                        tmp_dictset_key = var_mask;
                        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                        Py_DECREF( tmp_dictset_value );
                        if ( tmp_res != 0 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 144;
                            type_description_1 = "ooooooooooooooooooo";
                            goto try_except_handler_8;
                        }
                    }
                    if ( CONSIDER_THREADING() == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 143;
                        type_description_1 = "ooooooooooooooooooo";
                        goto try_except_handler_8;
                    }
                    goto loop_start_2;
                    loop_end_2:;
                    goto try_end_6;
                    // Exception handler code:
                    try_except_handler_8:;
                    exception_keeper_type_6 = exception_type;
                    exception_keeper_value_6 = exception_value;
                    exception_keeper_tb_6 = exception_tb;
                    exception_keeper_lineno_6 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_for_loop_2__iter_value );
                    tmp_for_loop_2__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
                    Py_DECREF( tmp_for_loop_2__for_iterator );
                    tmp_for_loop_2__for_iterator = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_6;
                    exception_value = exception_keeper_value_6;
                    exception_tb = exception_keeper_tb_6;
                    exception_lineno = exception_keeper_lineno_6;

                    goto frame_exception_exit_1;
                    // End of try:
                    try_end_6:;
                    Py_XDECREF( tmp_for_loop_2__iter_value );
                    tmp_for_loop_2__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
                    Py_DECREF( tmp_for_loop_2__for_iterator );
                    tmp_for_loop_2__for_iterator = NULL;

                    branch_end_5:;
                }
                {
                    PyObject *tmp_tuple_element_3;
                    PyObject *tmp_dict_name_8;
                    PyObject *tmp_key_name_8;
                    PyObject *tmp_dict_name_9;
                    PyObject *tmp_key_name_9;
                    PyObject *tmp_dict_name_10;
                    PyObject *tmp_key_name_10;
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_8 = var_file_info;
                    tmp_key_name_8 = const_str_plain_r_mask;
                    tmp_tuple_element_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 145;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_dictset_value = PyTuple_New( 3 );
                    PyTuple_SET_ITEM( tmp_dictset_value, 0, tmp_tuple_element_3 );
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_9 = var_file_info;
                    tmp_key_name_9 = const_str_plain_g_mask;
                    tmp_tuple_element_3 = DICT_GET_ITEM( tmp_dict_name_9, tmp_key_name_9 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_dictset_value );

                        exception_lineno = 146;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    PyTuple_SET_ITEM( tmp_dictset_value, 1, tmp_tuple_element_3 );
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_10 = var_file_info;
                    tmp_key_name_10 = const_str_plain_b_mask;
                    tmp_tuple_element_3 = DICT_GET_ITEM( tmp_dict_name_10, tmp_key_name_10 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_dictset_value );

                        exception_lineno = 147;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    PyTuple_SET_ITEM( tmp_dictset_value, 2, tmp_tuple_element_3 );
                    CHECK_OBJECT( var_file_info );
                    tmp_dictset_dict = var_file_info;
                    tmp_dictset_key = const_str_plain_rgb_mask;
                    tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                    Py_DECREF( tmp_dictset_value );
                    assert( !(tmp_res != 0) );
                }
                {
                    PyObject *tmp_tuple_element_4;
                    PyObject *tmp_dict_name_11;
                    PyObject *tmp_key_name_11;
                    PyObject *tmp_dict_name_12;
                    PyObject *tmp_key_name_12;
                    PyObject *tmp_dict_name_13;
                    PyObject *tmp_key_name_13;
                    PyObject *tmp_dict_name_14;
                    PyObject *tmp_key_name_14;
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_11 = var_file_info;
                    tmp_key_name_11 = const_str_plain_r_mask;
                    tmp_tuple_element_4 = DICT_GET_ITEM( tmp_dict_name_11, tmp_key_name_11 );
                    if ( tmp_tuple_element_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 148;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_dictset_value = PyTuple_New( 4 );
                    PyTuple_SET_ITEM( tmp_dictset_value, 0, tmp_tuple_element_4 );
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_12 = var_file_info;
                    tmp_key_name_12 = const_str_plain_g_mask;
                    tmp_tuple_element_4 = DICT_GET_ITEM( tmp_dict_name_12, tmp_key_name_12 );
                    if ( tmp_tuple_element_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_dictset_value );

                        exception_lineno = 149;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    PyTuple_SET_ITEM( tmp_dictset_value, 1, tmp_tuple_element_4 );
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_13 = var_file_info;
                    tmp_key_name_13 = const_str_plain_b_mask;
                    tmp_tuple_element_4 = DICT_GET_ITEM( tmp_dict_name_13, tmp_key_name_13 );
                    if ( tmp_tuple_element_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_dictset_value );

                        exception_lineno = 150;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    PyTuple_SET_ITEM( tmp_dictset_value, 2, tmp_tuple_element_4 );
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_14 = var_file_info;
                    tmp_key_name_14 = const_str_plain_a_mask;
                    tmp_tuple_element_4 = DICT_GET_ITEM( tmp_dict_name_14, tmp_key_name_14 );
                    if ( tmp_tuple_element_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_dictset_value );

                        exception_lineno = 151;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    PyTuple_SET_ITEM( tmp_dictset_value, 3, tmp_tuple_element_4 );
                    CHECK_OBJECT( var_file_info );
                    tmp_dictset_dict = var_file_info;
                    tmp_dictset_key = const_str_plain_rgba_mask;
                    tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                    Py_DECREF( tmp_dictset_value );
                    assert( !(tmp_res != 0) );
                }
                branch_no_4:;
            }
            goto branch_end_3;
            branch_no_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_make_exception_arg_1;
                PyObject *tmp_left_name_7;
                PyObject *tmp_right_name_7;
                PyObject *tmp_dict_name_15;
                PyObject *tmp_key_name_15;
                tmp_left_name_7 = const_str_digest_3cdec28f8e9f2b1d0a7f06c815f54869;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_15 = var_file_info;
                tmp_key_name_15 = const_str_plain_header_size;
                tmp_right_name_7 = DICT_GET_ITEM( tmp_dict_name_15, tmp_key_name_15 );
                if ( tmp_right_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 154;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_7, tmp_right_name_7 );
                Py_DECREF( tmp_right_name_7 );
                if ( tmp_make_exception_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 153;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 153;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_1 };
                    tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_IOError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_1 );
                assert( !(tmp_raise_type_1 == NULL) );
                exception_type = tmp_raise_type_1;
                exception_lineno = 153;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            branch_end_3:;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_dict_name_16;
        PyObject *tmp_key_name_16;
        PyObject *tmp_dict_name_17;
        PyObject *tmp_key_name_17;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_16 = var_file_info;
        tmp_key_name_16 = const_str_plain_width;
        tmp_tuple_element_5 = DICT_GET_ITEM( tmp_dict_name_16, tmp_key_name_16 );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assattr_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_assattr_name_1, 0, tmp_tuple_element_5 );
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_17 = var_file_info;
        tmp_key_name_17 = const_str_plain_height;
        tmp_tuple_element_5 = DICT_GET_ITEM( tmp_dict_name_17, tmp_key_name_17 );
        if ( tmp_tuple_element_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_1 );

            exception_lineno = 158;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_assattr_name_1, 1, tmp_tuple_element_5 );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__size, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 158;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_8;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_2;
        int tmp_truth_name_3;
        PyObject *tmp_dict_name_18;
        PyObject *tmp_key_name_18;
        PyObject *tmp_left_name_8;
        PyObject *tmp_right_name_8;
        PyObject *tmp_dict_name_19;
        PyObject *tmp_key_name_19;
        CHECK_OBJECT( var_file_info );
        tmp_called_instance_1 = var_file_info;
        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 162;
        tmp_call_result_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_colors_int_0_tuple, 0 ) );

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_call_result_2 );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_2 );

            exception_lineno = 162;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_2 );
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_18 = var_file_info;
        tmp_key_name_18 = const_str_plain_colors;
        tmp_dictset_value = DICT_GET_ITEM( tmp_dict_name_18, tmp_key_name_18 );
        if ( tmp_dictset_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        tmp_left_name_8 = const_int_pos_1;
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_19 = var_file_info;
        tmp_key_name_19 = const_str_plain_bits;
        tmp_right_name_8 = DICT_GET_ITEM( tmp_dict_name_19, tmp_key_name_19 );
        if ( tmp_right_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_dictset_value = BINARY_OPERATION( PyNumber_Lshift, tmp_left_name_8, tmp_right_name_8 );
        Py_DECREF( tmp_right_name_8 );
        if ( tmp_dictset_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        condexpr_end_3:;
        CHECK_OBJECT( var_file_info );
        tmp_dictset_dict = var_file_info;
        tmp_dictset_key = const_str_plain_colors;
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        assert( !(tmp_res != 0) );
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_compexpr_left_6;
        PyObject *tmp_compexpr_right_6;
        PyObject *tmp_left_name_9;
        PyObject *tmp_dict_name_20;
        PyObject *tmp_key_name_20;
        PyObject *tmp_right_name_9;
        PyObject *tmp_dict_name_21;
        PyObject *tmp_key_name_21;
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_20 = var_file_info;
        tmp_key_name_20 = const_str_plain_width;
        tmp_left_name_9 = DICT_GET_ITEM( tmp_dict_name_20, tmp_key_name_20 );
        if ( tmp_left_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_21 = var_file_info;
        tmp_key_name_21 = const_str_plain_height;
        tmp_right_name_9 = DICT_GET_ITEM( tmp_dict_name_21, tmp_key_name_21 );
        if ( tmp_right_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_9 );

            exception_lineno = 166;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_6 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_9, tmp_right_name_9 );
        Py_DECREF( tmp_left_name_9 );
        Py_DECREF( tmp_right_name_9 );
        if ( tmp_compexpr_left_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_6 = const_int_pos_2147483648;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
        Py_DECREF( tmp_compexpr_left_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_raise_type_2;
            PyObject *tmp_make_exception_arg_2;
            PyObject *tmp_left_name_10;
            PyObject *tmp_right_name_10;
            PyObject *tmp_source_name_10;
            tmp_left_name_10 = const_str_digest_d5a77806b874a047cab54ef775232bdd;
            CHECK_OBJECT( par_self );
            tmp_source_name_10 = par_self;
            tmp_right_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_size );
            if ( tmp_right_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 167;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_make_exception_arg_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_10, tmp_right_name_10 );
            Py_DECREF( tmp_right_name_10 );
            if ( tmp_make_exception_arg_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 167;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 167;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_2 };
                tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_IOError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_2 );
            assert( !(tmp_raise_type_2 == NULL) );
            exception_type = tmp_raise_type_2;
            exception_lineno = 167;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_6:;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_iter_arg_6;
        PyObject *tmp_called_name_24;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_dict_name_22;
        PyObject *tmp_key_name_22;
        PyObject *tmp_args_element_name_23;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_BIT2MODE );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BIT2MODE );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BIT2MODE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 170;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_9;
        }

        tmp_source_name_11 = tmp_mvar_value_20;
        tmp_called_name_24 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_get );
        if ( tmp_called_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_9;
        }
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_22 = var_file_info;
        tmp_key_name_22 = const_str_plain_bits;
        tmp_args_element_name_22 = DICT_GET_ITEM( tmp_dict_name_22, tmp_key_name_22 );
        if ( tmp_args_element_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_24 );

            exception_lineno = 170;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_9;
        }
        tmp_args_element_name_23 = const_tuple_none_none_tuple;
        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 170;
        {
            PyObject *call_args[] = { tmp_args_element_name_22, tmp_args_element_name_23 };
            tmp_iter_arg_6 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_24, call_args );
        }

        Py_DECREF( tmp_called_name_24 );
        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_iter_arg_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_9;
        }
        tmp_assign_source_19 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_6 );
        Py_DECREF( tmp_iter_arg_6 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_9;
        }
        assert( tmp_tuple_unpack_3__source_iter == NULL );
        tmp_tuple_unpack_3__source_iter = tmp_assign_source_19;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_unpack_5;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_20 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
        if ( tmp_assign_source_20 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooooooooo";
            exception_lineno = 170;
            goto try_except_handler_10;
        }
        assert( tmp_tuple_unpack_3__element_1 == NULL );
        tmp_tuple_unpack_3__element_1 = tmp_assign_source_20;
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_unpack_6;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;
        tmp_assign_source_21 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
        if ( tmp_assign_source_21 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "ooooooooooooooooooo";
            exception_lineno = 170;
            goto try_except_handler_10;
        }
        assert( tmp_tuple_unpack_3__element_2 == NULL );
        tmp_tuple_unpack_3__element_2 = tmp_assign_source_21;
    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
        tmp_iterator_name_2 = tmp_tuple_unpack_3__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooooooooooo";
                    exception_lineno = 170;
                    goto try_except_handler_10;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "ooooooooooooooooooo";
            exception_lineno = 170;
            goto try_except_handler_10;
        }
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto try_except_handler_9;
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
    Py_DECREF( tmp_tuple_unpack_3__source_iter );
    tmp_tuple_unpack_3__source_iter = NULL;

    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
        tmp_assattr_name_2 = tmp_tuple_unpack_3__element_1;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_mode, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooooooooooooooooooo";
            goto try_except_handler_9;
        }
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    Py_XDECREF( tmp_tuple_unpack_3__element_1 );
    tmp_tuple_unpack_3__element_1 = NULL;

    {
        PyObject *tmp_assign_source_22;
        CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
        tmp_assign_source_22 = tmp_tuple_unpack_3__element_2;
        assert( var_raw_mode == NULL );
        Py_INCREF( tmp_assign_source_22 );
        var_raw_mode = tmp_assign_source_22;
    }
    Py_XDECREF( tmp_tuple_unpack_3__element_2 );
    tmp_tuple_unpack_3__element_2 = NULL;

    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        PyObject *tmp_source_name_12;
        CHECK_OBJECT( par_self );
        tmp_source_name_12 = par_self;
        tmp_compexpr_left_7 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_mode );
        if ( tmp_compexpr_left_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 171;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_7 = Py_None;
        tmp_condition_result_10 = ( tmp_compexpr_left_7 == tmp_compexpr_right_7 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_7 );
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_raise_type_3;
            PyObject *tmp_make_exception_arg_3;
            PyObject *tmp_left_name_11;
            PyObject *tmp_right_name_11;
            PyObject *tmp_dict_name_23;
            PyObject *tmp_key_name_23;
            tmp_left_name_11 = const_str_digest_da2c0ebacadc96c5c0ecd57f371555d8;
            CHECK_OBJECT( var_file_info );
            tmp_dict_name_23 = var_file_info;
            tmp_key_name_23 = const_str_plain_bits;
            tmp_right_name_11 = DICT_GET_ITEM( tmp_dict_name_23, tmp_key_name_23 );
            if ( tmp_right_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 173;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_make_exception_arg_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_11, tmp_right_name_11 );
            Py_DECREF( tmp_right_name_11 );
            if ( tmp_make_exception_arg_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 172;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 172;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_3 };
                tmp_raise_type_3 = CALL_FUNCTION_WITH_ARGS1( PyExc_IOError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_3 );
            assert( !(tmp_raise_type_3 == NULL) );
            exception_type = tmp_raise_type_3;
            exception_lineno = 172;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        branch_no_7:;
    }
    {
        nuitka_bool tmp_condition_result_11;
        PyObject *tmp_compexpr_left_8;
        PyObject *tmp_compexpr_right_8;
        PyObject *tmp_dict_name_24;
        PyObject *tmp_key_name_24;
        PyObject *tmp_source_name_13;
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_24 = var_file_info;
        tmp_key_name_24 = const_str_plain_compression;
        tmp_compexpr_left_8 = DICT_GET_ITEM( tmp_dict_name_24, tmp_key_name_24 );
        if ( tmp_compexpr_left_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_13 = par_self;
        tmp_compexpr_right_8 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_BITFIELDS );
        if ( tmp_compexpr_right_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_8 );

            exception_lineno = 176;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
        Py_DECREF( tmp_compexpr_left_8 );
        Py_DECREF( tmp_compexpr_right_8 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            PyObject *tmp_assign_source_23;
            tmp_assign_source_23 = DEEP_COPY( const_dict_0838f6241605954603b148cacbf5c6cf );
            assert( var_SUPPORTED == NULL );
            var_SUPPORTED = tmp_assign_source_23;
        }
        {
            PyObject *tmp_assign_source_24;
            tmp_assign_source_24 = PyDict_Copy( const_dict_05bbcf03645040ac7ffdd29056adaf00 );
            assert( var_MASK_MODES == NULL );
            var_MASK_MODES = tmp_assign_source_24;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_key_name_25;
            PyObject *tmp_dict_name_25;
            PyObject *tmp_key_name_26;
            PyObject *tmp_dict_name_26;
            CHECK_OBJECT( var_file_info );
            tmp_dict_name_25 = var_file_info;
            tmp_key_name_26 = const_str_plain_bits;
            tmp_key_name_25 = DICT_GET_ITEM( tmp_dict_name_25, tmp_key_name_26 );
            if ( tmp_key_name_25 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_SUPPORTED );
            tmp_dict_name_26 = var_SUPPORTED;
            tmp_res = PyDict_Contains( tmp_dict_name_26, tmp_key_name_25 );
            Py_DECREF( tmp_key_name_25 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            {
                nuitka_bool tmp_condition_result_13;
                int tmp_and_left_truth_1;
                nuitka_bool tmp_and_left_value_1;
                nuitka_bool tmp_and_right_value_1;
                PyObject *tmp_compexpr_left_9;
                PyObject *tmp_compexpr_right_9;
                PyObject *tmp_dict_name_27;
                PyObject *tmp_key_name_27;
                PyObject *tmp_compexpr_left_10;
                PyObject *tmp_compexpr_right_10;
                PyObject *tmp_dict_name_28;
                PyObject *tmp_key_name_28;
                PyObject *tmp_dict_name_29;
                PyObject *tmp_key_name_29;
                PyObject *tmp_dict_name_30;
                PyObject *tmp_key_name_30;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_27 = var_file_info;
                tmp_key_name_27 = const_str_plain_bits;
                tmp_compexpr_left_9 = DICT_GET_ITEM( tmp_dict_name_27, tmp_key_name_27 );
                if ( tmp_compexpr_left_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_9 = const_int_pos_32;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                Py_DECREF( tmp_compexpr_left_9 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 197;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
                if ( tmp_and_left_truth_1 == 1 )
                {
                    goto and_right_1;
                }
                else
                {
                    goto and_left_1;
                }
                and_right_1:;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_28 = var_file_info;
                tmp_key_name_28 = const_str_plain_rgba_mask;
                tmp_compexpr_left_10 = DICT_GET_ITEM( tmp_dict_name_28, tmp_key_name_28 );
                if ( tmp_compexpr_left_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 198;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_SUPPORTED );
                tmp_dict_name_29 = var_SUPPORTED;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_30 = var_file_info;
                tmp_key_name_30 = const_str_plain_bits;
                tmp_key_name_29 = DICT_GET_ITEM( tmp_dict_name_30, tmp_key_name_30 );
                if ( tmp_key_name_29 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_compexpr_left_10 );

                    exception_lineno = 198;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_10 = DICT_GET_ITEM( tmp_dict_name_29, tmp_key_name_29 );
                Py_DECREF( tmp_key_name_29 );
                if ( tmp_compexpr_right_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_compexpr_left_10 );

                    exception_lineno = 198;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_res = PySequence_Contains( tmp_compexpr_right_10, tmp_compexpr_left_10 );
                Py_DECREF( tmp_compexpr_left_10 );
                Py_DECREF( tmp_compexpr_right_10 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 198;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_and_right_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_condition_result_13 = tmp_and_right_value_1;
                goto and_end_1;
                and_left_1:;
                tmp_condition_result_13 = tmp_and_left_value_1;
                and_end_1:;
                if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_10;
                }
                else
                {
                    goto branch_no_10;
                }
                branch_yes_10:;
                {
                    PyObject *tmp_assign_source_25;
                    PyObject *tmp_dict_name_31;
                    PyObject *tmp_key_name_31;
                    PyObject *tmp_tuple_element_6;
                    PyObject *tmp_dict_name_32;
                    PyObject *tmp_key_name_32;
                    PyObject *tmp_dict_name_33;
                    PyObject *tmp_key_name_33;
                    CHECK_OBJECT( var_MASK_MODES );
                    tmp_dict_name_31 = var_MASK_MODES;
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_32 = var_file_info;
                    tmp_key_name_32 = const_str_plain_bits;
                    tmp_tuple_element_6 = DICT_GET_ITEM( tmp_dict_name_32, tmp_key_name_32 );
                    if ( tmp_tuple_element_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 200;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_key_name_31 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_key_name_31, 0, tmp_tuple_element_6 );
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_33 = var_file_info;
                    tmp_key_name_33 = const_str_plain_rgba_mask;
                    tmp_tuple_element_6 = DICT_GET_ITEM( tmp_dict_name_33, tmp_key_name_33 );
                    if ( tmp_tuple_element_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_key_name_31 );

                        exception_lineno = 200;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    PyTuple_SET_ITEM( tmp_key_name_31, 1, tmp_tuple_element_6 );
                    tmp_assign_source_25 = DICT_GET_ITEM( tmp_dict_name_31, tmp_key_name_31 );
                    Py_DECREF( tmp_key_name_31 );
                    if ( tmp_assign_source_25 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 199;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_raw_mode;
                        assert( old != NULL );
                        var_raw_mode = tmp_assign_source_25;
                        Py_DECREF( old );
                    }

                }
                {
                    PyObject *tmp_assattr_name_3;
                    nuitka_bool tmp_condition_result_14;
                    PyObject *tmp_compexpr_left_11;
                    PyObject *tmp_compexpr_right_11;
                    PyObject *tmp_source_name_14;
                    PyObject *tmp_assattr_target_3;
                    tmp_compexpr_left_11 = const_str_plain_A;
                    CHECK_OBJECT( var_raw_mode );
                    tmp_compexpr_right_11 = var_raw_mode;
                    tmp_res = PySequence_Contains( tmp_compexpr_right_11, tmp_compexpr_left_11 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 202;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_14 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                    {
                        goto condexpr_true_4;
                    }
                    else
                    {
                        goto condexpr_false_4;
                    }
                    condexpr_true_4:;
                    tmp_assattr_name_3 = const_str_plain_RGBA;
                    Py_INCREF( tmp_assattr_name_3 );
                    goto condexpr_end_4;
                    condexpr_false_4:;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_14 = par_self;
                    tmp_assattr_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_mode );
                    if ( tmp_assattr_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 202;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    condexpr_end_4:;
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_3 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_mode, tmp_assattr_name_3 );
                    Py_DECREF( tmp_assattr_name_3 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 202;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                goto branch_end_10;
                branch_no_10:;
                {
                    nuitka_bool tmp_condition_result_15;
                    int tmp_and_left_truth_2;
                    nuitka_bool tmp_and_left_value_2;
                    nuitka_bool tmp_and_right_value_2;
                    PyObject *tmp_compexpr_left_12;
                    PyObject *tmp_compexpr_right_12;
                    PyObject *tmp_dict_name_34;
                    PyObject *tmp_key_name_34;
                    PyObject *tmp_compexpr_left_13;
                    PyObject *tmp_compexpr_right_13;
                    PyObject *tmp_dict_name_35;
                    PyObject *tmp_key_name_35;
                    PyObject *tmp_dict_name_36;
                    PyObject *tmp_key_name_36;
                    PyObject *tmp_dict_name_37;
                    PyObject *tmp_key_name_37;
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_34 = var_file_info;
                    tmp_key_name_34 = const_str_plain_bits;
                    tmp_compexpr_left_12 = DICT_GET_ITEM( tmp_dict_name_34, tmp_key_name_34 );
                    if ( tmp_compexpr_left_12 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 203;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_12 = const_tuple_int_pos_24_int_pos_16_tuple;
                    tmp_res = PySequence_Contains( tmp_compexpr_right_12, tmp_compexpr_left_12 );
                    Py_DECREF( tmp_compexpr_left_12 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 203;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_and_left_value_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
                    if ( tmp_and_left_truth_2 == 1 )
                    {
                        goto and_right_2;
                    }
                    else
                    {
                        goto and_left_2;
                    }
                    and_right_2:;
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_35 = var_file_info;
                    tmp_key_name_35 = const_str_plain_rgb_mask;
                    tmp_compexpr_left_13 = DICT_GET_ITEM( tmp_dict_name_35, tmp_key_name_35 );
                    if ( tmp_compexpr_left_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 204;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_SUPPORTED );
                    tmp_dict_name_36 = var_SUPPORTED;
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_37 = var_file_info;
                    tmp_key_name_37 = const_str_plain_bits;
                    tmp_key_name_36 = DICT_GET_ITEM( tmp_dict_name_37, tmp_key_name_37 );
                    if ( tmp_key_name_36 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_compexpr_left_13 );

                        exception_lineno = 204;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_13 = DICT_GET_ITEM( tmp_dict_name_36, tmp_key_name_36 );
                    Py_DECREF( tmp_key_name_36 );
                    if ( tmp_compexpr_right_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_compexpr_left_13 );

                        exception_lineno = 204;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_res = PySequence_Contains( tmp_compexpr_right_13, tmp_compexpr_left_13 );
                    Py_DECREF( tmp_compexpr_left_13 );
                    Py_DECREF( tmp_compexpr_right_13 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 204;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_and_right_value_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    tmp_condition_result_15 = tmp_and_right_value_2;
                    goto and_end_2;
                    and_left_2:;
                    tmp_condition_result_15 = tmp_and_left_value_2;
                    and_end_2:;
                    if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_11;
                    }
                    else
                    {
                        goto branch_no_11;
                    }
                    branch_yes_11:;
                    {
                        PyObject *tmp_assign_source_26;
                        PyObject *tmp_dict_name_38;
                        PyObject *tmp_key_name_38;
                        PyObject *tmp_tuple_element_7;
                        PyObject *tmp_dict_name_39;
                        PyObject *tmp_key_name_39;
                        PyObject *tmp_dict_name_40;
                        PyObject *tmp_key_name_40;
                        CHECK_OBJECT( var_MASK_MODES );
                        tmp_dict_name_38 = var_MASK_MODES;
                        CHECK_OBJECT( var_file_info );
                        tmp_dict_name_39 = var_file_info;
                        tmp_key_name_39 = const_str_plain_bits;
                        tmp_tuple_element_7 = DICT_GET_ITEM( tmp_dict_name_39, tmp_key_name_39 );
                        if ( tmp_tuple_element_7 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 206;
                            type_description_1 = "ooooooooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_key_name_38 = PyTuple_New( 2 );
                        PyTuple_SET_ITEM( tmp_key_name_38, 0, tmp_tuple_element_7 );
                        CHECK_OBJECT( var_file_info );
                        tmp_dict_name_40 = var_file_info;
                        tmp_key_name_40 = const_str_plain_rgb_mask;
                        tmp_tuple_element_7 = DICT_GET_ITEM( tmp_dict_name_40, tmp_key_name_40 );
                        if ( tmp_tuple_element_7 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_key_name_38 );

                            exception_lineno = 206;
                            type_description_1 = "ooooooooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        PyTuple_SET_ITEM( tmp_key_name_38, 1, tmp_tuple_element_7 );
                        tmp_assign_source_26 = DICT_GET_ITEM( tmp_dict_name_38, tmp_key_name_38 );
                        Py_DECREF( tmp_key_name_38 );
                        if ( tmp_assign_source_26 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 205;
                            type_description_1 = "ooooooooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        {
                            PyObject *old = var_raw_mode;
                            assert( old != NULL );
                            var_raw_mode = tmp_assign_source_26;
                            Py_DECREF( old );
                        }

                    }
                    goto branch_end_11;
                    branch_no_11:;
                    {
                        PyObject *tmp_raise_type_4;
                        PyObject *tmp_make_exception_arg_4;
                        tmp_make_exception_arg_4 = const_str_digest_7b3d22cf3697d3bdb60854511bfe5ef9;
                        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 209;
                        {
                            PyObject *call_args[] = { tmp_make_exception_arg_4 };
                            tmp_raise_type_4 = CALL_FUNCTION_WITH_ARGS1( PyExc_IOError, call_args );
                        }

                        assert( !(tmp_raise_type_4 == NULL) );
                        exception_type = tmp_raise_type_4;
                        exception_lineno = 209;
                        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    branch_end_11:;
                }
                branch_end_10:;
            }
            goto branch_end_9;
            branch_no_9:;
            {
                PyObject *tmp_raise_type_5;
                PyObject *tmp_make_exception_arg_5;
                tmp_make_exception_arg_5 = const_str_digest_7b3d22cf3697d3bdb60854511bfe5ef9;
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 211;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_5 };
                    tmp_raise_type_5 = CALL_FUNCTION_WITH_ARGS1( PyExc_IOError, call_args );
                }

                assert( !(tmp_raise_type_5 == NULL) );
                exception_type = tmp_raise_type_5;
                exception_lineno = 211;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            branch_end_9:;
        }
        goto branch_end_8;
        branch_no_8:;
        {
            nuitka_bool tmp_condition_result_16;
            PyObject *tmp_compexpr_left_14;
            PyObject *tmp_compexpr_right_14;
            PyObject *tmp_dict_name_41;
            PyObject *tmp_key_name_41;
            PyObject *tmp_source_name_15;
            CHECK_OBJECT( var_file_info );
            tmp_dict_name_41 = var_file_info;
            tmp_key_name_41 = const_str_plain_compression;
            tmp_compexpr_left_14 = DICT_GET_ITEM( tmp_dict_name_41, tmp_key_name_41 );
            if ( tmp_compexpr_left_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_15 = par_self;
            tmp_compexpr_right_14 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_RAW );
            if ( tmp_compexpr_right_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_14 );

                exception_lineno = 212;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_14, tmp_compexpr_right_14 );
            Py_DECREF( tmp_compexpr_left_14 );
            Py_DECREF( tmp_compexpr_right_14 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 212;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            {
                nuitka_bool tmp_condition_result_17;
                int tmp_and_left_truth_3;
                nuitka_bool tmp_and_left_value_3;
                nuitka_bool tmp_and_right_value_3;
                PyObject *tmp_compexpr_left_15;
                PyObject *tmp_compexpr_right_15;
                PyObject *tmp_dict_name_42;
                PyObject *tmp_key_name_42;
                PyObject *tmp_compexpr_left_16;
                PyObject *tmp_compexpr_right_16;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_42 = var_file_info;
                tmp_key_name_42 = const_str_plain_bits;
                tmp_compexpr_left_15 = DICT_GET_ITEM( tmp_dict_name_42, tmp_key_name_42 );
                if ( tmp_compexpr_left_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 213;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_15 = const_int_pos_32;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_15, tmp_compexpr_right_15 );
                Py_DECREF( tmp_compexpr_left_15 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 213;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_and_left_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_and_left_truth_3 = tmp_and_left_value_3 == NUITKA_BOOL_TRUE ? 1 : 0;
                if ( tmp_and_left_truth_3 == 1 )
                {
                    goto and_right_3;
                }
                else
                {
                    goto and_left_3;
                }
                and_right_3:;
                CHECK_OBJECT( par_header );
                tmp_compexpr_left_16 = par_header;
                tmp_compexpr_right_16 = const_int_pos_22;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_16, tmp_compexpr_right_16 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 213;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_and_right_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_condition_result_17 = tmp_and_right_value_3;
                goto and_end_3;
                and_left_3:;
                tmp_condition_result_17 = tmp_and_left_value_3;
                and_end_3:;
                if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_13;
                }
                else
                {
                    goto branch_no_13;
                }
                branch_yes_13:;
                {
                    PyObject *tmp_assign_source_27;
                    PyObject *tmp_iter_arg_7;
                    tmp_iter_arg_7 = const_tuple_str_plain_BGRA_str_plain_RGBA_tuple;
                    tmp_assign_source_27 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_7 );
                    assert( !(tmp_assign_source_27 == NULL) );
                    assert( tmp_tuple_unpack_4__source_iter == NULL );
                    tmp_tuple_unpack_4__source_iter = tmp_assign_source_27;
                }
                // Tried code:
                // Tried code:
                {
                    PyObject *tmp_assign_source_28;
                    PyObject *tmp_unpack_7;
                    CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
                    tmp_unpack_7 = tmp_tuple_unpack_4__source_iter;
                    tmp_assign_source_28 = UNPACK_NEXT( tmp_unpack_7, 0, 2 );
                    if ( tmp_assign_source_28 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooooooooooooooooo";
                        exception_lineno = 214;
                        goto try_except_handler_12;
                    }
                    assert( tmp_tuple_unpack_4__element_1 == NULL );
                    tmp_tuple_unpack_4__element_1 = tmp_assign_source_28;
                }
                {
                    PyObject *tmp_assign_source_29;
                    PyObject *tmp_unpack_8;
                    CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
                    tmp_unpack_8 = tmp_tuple_unpack_4__source_iter;
                    tmp_assign_source_29 = UNPACK_NEXT( tmp_unpack_8, 1, 2 );
                    if ( tmp_assign_source_29 == NULL )
                    {
                        if ( !ERROR_OCCURRED() )
                        {
                            exception_type = PyExc_StopIteration;
                            Py_INCREF( exception_type );
                            exception_value = NULL;
                            exception_tb = NULL;
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        }


                        type_description_1 = "ooooooooooooooooooo";
                        exception_lineno = 214;
                        goto try_except_handler_12;
                    }
                    assert( tmp_tuple_unpack_4__element_2 == NULL );
                    tmp_tuple_unpack_4__element_2 = tmp_assign_source_29;
                }
                goto try_end_9;
                // Exception handler code:
                try_except_handler_12:;
                exception_keeper_type_9 = exception_type;
                exception_keeper_value_9 = exception_value;
                exception_keeper_tb_9 = exception_tb;
                exception_keeper_lineno_9 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
                Py_DECREF( tmp_tuple_unpack_4__source_iter );
                tmp_tuple_unpack_4__source_iter = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_9;
                exception_value = exception_keeper_value_9;
                exception_tb = exception_keeper_tb_9;
                exception_lineno = exception_keeper_lineno_9;

                goto try_except_handler_11;
                // End of try:
                try_end_9:;
                CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
                Py_DECREF( tmp_tuple_unpack_4__source_iter );
                tmp_tuple_unpack_4__source_iter = NULL;

                {
                    PyObject *tmp_assign_source_30;
                    CHECK_OBJECT( tmp_tuple_unpack_4__element_1 );
                    tmp_assign_source_30 = tmp_tuple_unpack_4__element_1;
                    {
                        PyObject *old = var_raw_mode;
                        assert( old != NULL );
                        var_raw_mode = tmp_assign_source_30;
                        Py_INCREF( var_raw_mode );
                        Py_DECREF( old );
                    }

                }
                Py_XDECREF( tmp_tuple_unpack_4__element_1 );
                tmp_tuple_unpack_4__element_1 = NULL;

                {
                    PyObject *tmp_assattr_name_4;
                    PyObject *tmp_assattr_target_4;
                    CHECK_OBJECT( tmp_tuple_unpack_4__element_2 );
                    tmp_assattr_name_4 = tmp_tuple_unpack_4__element_2;
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_4 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_mode, tmp_assattr_name_4 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 214;
                        type_description_1 = "ooooooooooooooooooo";
                        goto try_except_handler_11;
                    }
                }
                goto try_end_10;
                // Exception handler code:
                try_except_handler_11:;
                exception_keeper_type_10 = exception_type;
                exception_keeper_value_10 = exception_value;
                exception_keeper_tb_10 = exception_tb;
                exception_keeper_lineno_10 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_tuple_unpack_4__element_1 );
                tmp_tuple_unpack_4__element_1 = NULL;

                Py_XDECREF( tmp_tuple_unpack_4__element_2 );
                tmp_tuple_unpack_4__element_2 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_10;
                exception_value = exception_keeper_value_10;
                exception_tb = exception_keeper_tb_10;
                exception_lineno = exception_keeper_lineno_10;

                goto frame_exception_exit_1;
                // End of try:
                try_end_10:;
                Py_XDECREF( tmp_tuple_unpack_4__element_2 );
                tmp_tuple_unpack_4__element_2 = NULL;

                branch_no_13:;
            }
            goto branch_end_12;
            branch_no_12:;
            {
                PyObject *tmp_raise_type_6;
                PyObject *tmp_make_exception_arg_6;
                PyObject *tmp_left_name_12;
                PyObject *tmp_right_name_12;
                PyObject *tmp_dict_name_43;
                PyObject *tmp_key_name_43;
                tmp_left_name_12 = const_str_digest_ed7db2c47dad4781835faac02fb06b46;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_43 = var_file_info;
                tmp_key_name_43 = const_str_plain_compression;
                tmp_right_name_12 = DICT_GET_ITEM( tmp_dict_name_43, tmp_key_name_43 );
                if ( tmp_right_name_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 217;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_make_exception_arg_6 = BINARY_OPERATION_REMAINDER( tmp_left_name_12, tmp_right_name_12 );
                Py_DECREF( tmp_right_name_12 );
                if ( tmp_make_exception_arg_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 216;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 216;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_6 };
                    tmp_raise_type_6 = CALL_FUNCTION_WITH_ARGS1( PyExc_IOError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_6 );
                assert( !(tmp_raise_type_6 == NULL) );
                exception_type = tmp_raise_type_6;
                exception_lineno = 216;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            branch_end_12:;
        }
        branch_end_8:;
    }
    {
        nuitka_bool tmp_condition_result_18;
        PyObject *tmp_compexpr_left_17;
        PyObject *tmp_compexpr_right_17;
        PyObject *tmp_source_name_16;
        CHECK_OBJECT( par_self );
        tmp_source_name_16 = par_self;
        tmp_compexpr_left_17 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_mode );
        if ( tmp_compexpr_left_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 220;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_17 = const_str_plain_P;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_17, tmp_compexpr_right_17 );
        Py_DECREF( tmp_compexpr_left_17 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 220;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            nuitka_bool tmp_condition_result_19;
            PyObject *tmp_operand_name_2;
            // Tried code:
            {
                PyObject *tmp_assign_source_31;
                PyObject *tmp_dict_name_44;
                PyObject *tmp_key_name_44;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_44 = var_file_info;
                tmp_key_name_44 = const_str_plain_colors;
                tmp_assign_source_31 = DICT_GET_ITEM( tmp_dict_name_44, tmp_key_name_44 );
                if ( tmp_assign_source_31 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 223;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_13;
                }
                assert( tmp_comparison_chain_1__operand_2 == NULL );
                tmp_comparison_chain_1__operand_2 = tmp_assign_source_31;
            }
            {
                PyObject *tmp_assign_source_32;
                PyObject *tmp_compexpr_left_18;
                PyObject *tmp_compexpr_right_18;
                tmp_compexpr_left_18 = const_int_0;
                CHECK_OBJECT( tmp_comparison_chain_1__operand_2 );
                tmp_compexpr_right_18 = tmp_comparison_chain_1__operand_2;
                tmp_assign_source_32 = RICH_COMPARE_LT_OBJECT_OBJECT( tmp_compexpr_left_18, tmp_compexpr_right_18 );
                if ( tmp_assign_source_32 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 223;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_13;
                }
                assert( tmp_comparison_chain_1__comparison_result == NULL );
                tmp_comparison_chain_1__comparison_result = tmp_assign_source_32;
            }
            {
                nuitka_bool tmp_condition_result_20;
                PyObject *tmp_operand_name_3;
                CHECK_OBJECT( tmp_comparison_chain_1__comparison_result );
                tmp_operand_name_3 = tmp_comparison_chain_1__comparison_result;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 223;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_13;
                }
                tmp_condition_result_20 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_16;
                }
                else
                {
                    goto branch_no_16;
                }
                branch_yes_16:;
                CHECK_OBJECT( tmp_comparison_chain_1__comparison_result );
                tmp_operand_name_2 = tmp_comparison_chain_1__comparison_result;
                Py_INCREF( tmp_operand_name_2 );
                goto try_return_handler_13;
                branch_no_16:;
            }
            {
                PyObject *tmp_compexpr_left_19;
                PyObject *tmp_compexpr_right_19;
                CHECK_OBJECT( tmp_comparison_chain_1__operand_2 );
                tmp_compexpr_left_19 = tmp_comparison_chain_1__operand_2;
                tmp_compexpr_right_19 = const_int_pos_65536;
                tmp_operand_name_2 = RICH_COMPARE_LTE_OBJECT_OBJECT( tmp_compexpr_left_19, tmp_compexpr_right_19 );
                if ( tmp_operand_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 223;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_13;
                }
                goto try_return_handler_13;
            }
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_3__bitmap );
            return NULL;
            // Return handler code:
            try_return_handler_13:;
            CHECK_OBJECT( (PyObject *)tmp_comparison_chain_1__operand_2 );
            Py_DECREF( tmp_comparison_chain_1__operand_2 );
            tmp_comparison_chain_1__operand_2 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_comparison_chain_1__comparison_result );
            Py_DECREF( tmp_comparison_chain_1__comparison_result );
            tmp_comparison_chain_1__comparison_result = NULL;

            goto outline_result_2;
            // Exception handler code:
            try_except_handler_13:;
            exception_keeper_type_11 = exception_type;
            exception_keeper_value_11 = exception_value;
            exception_keeper_tb_11 = exception_tb;
            exception_keeper_lineno_11 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_comparison_chain_1__operand_2 );
            tmp_comparison_chain_1__operand_2 = NULL;

            Py_XDECREF( tmp_comparison_chain_1__comparison_result );
            tmp_comparison_chain_1__comparison_result = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_11;
            exception_value = exception_keeper_value_11;
            exception_tb = exception_keeper_tb_11;
            exception_lineno = exception_keeper_lineno_11;

            goto frame_exception_exit_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_3__bitmap );
            return NULL;
            outline_result_2:;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            Py_DECREF( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 223;
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_19 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_15;
            }
            else
            {
                goto branch_no_15;
            }
            branch_yes_15:;
            {
                PyObject *tmp_raise_type_7;
                PyObject *tmp_make_exception_arg_7;
                PyObject *tmp_left_name_13;
                PyObject *tmp_right_name_13;
                PyObject *tmp_dict_name_45;
                PyObject *tmp_key_name_45;
                tmp_left_name_13 = const_str_digest_eabbe093ea0d4e0ea3f9a72998b0f524;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_45 = var_file_info;
                tmp_key_name_45 = const_str_plain_colors;
                tmp_right_name_13 = DICT_GET_ITEM( tmp_dict_name_45, tmp_key_name_45 );
                if ( tmp_right_name_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 225;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_make_exception_arg_7 = BINARY_OPERATION_REMAINDER( tmp_left_name_13, tmp_right_name_13 );
                Py_DECREF( tmp_right_name_13 );
                if ( tmp_make_exception_arg_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 224;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 224;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_7 };
                    tmp_raise_type_7 = CALL_FUNCTION_WITH_ARGS1( PyExc_IOError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_7 );
                assert( !(tmp_raise_type_7 == NULL) );
                exception_type = tmp_raise_type_7;
                exception_lineno = 224;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            goto branch_end_15;
            branch_no_15:;
            {
                PyObject *tmp_assign_source_33;
                PyObject *tmp_dict_name_46;
                PyObject *tmp_key_name_46;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_46 = var_file_info;
                tmp_key_name_46 = const_str_plain_palette_padding;
                tmp_assign_source_33 = DICT_GET_ITEM( tmp_dict_name_46, tmp_key_name_46 );
                if ( tmp_assign_source_33 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 227;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_padding == NULL );
                var_padding = tmp_assign_source_33;
            }
            {
                PyObject *tmp_assign_source_34;
                PyObject *tmp_called_name_25;
                PyObject *tmp_args_element_name_24;
                PyObject *tmp_left_name_14;
                PyObject *tmp_right_name_14;
                PyObject *tmp_dict_name_47;
                PyObject *tmp_key_name_47;
                CHECK_OBJECT( var_read );
                tmp_called_name_25 = var_read;
                CHECK_OBJECT( var_padding );
                tmp_left_name_14 = var_padding;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_47 = var_file_info;
                tmp_key_name_47 = const_str_plain_colors;
                tmp_right_name_14 = DICT_GET_ITEM( tmp_dict_name_47, tmp_key_name_47 );
                if ( tmp_right_name_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 228;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_args_element_name_24 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_14, tmp_right_name_14 );
                Py_DECREF( tmp_right_name_14 );
                if ( tmp_args_element_name_24 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 228;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 228;
                {
                    PyObject *call_args[] = { tmp_args_element_name_24 };
                    tmp_assign_source_34 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, call_args );
                }

                Py_DECREF( tmp_args_element_name_24 );
                if ( tmp_assign_source_34 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 228;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_palette == NULL );
                var_palette = tmp_assign_source_34;
            }
            {
                PyObject *tmp_assign_source_35;
                tmp_assign_source_35 = Py_True;
                assert( var_greyscale == NULL );
                Py_INCREF( tmp_assign_source_35 );
                var_greyscale = tmp_assign_source_35;
            }
            {
                PyObject *tmp_assign_source_36;
                nuitka_bool tmp_condition_result_21;
                PyObject *tmp_compexpr_left_20;
                PyObject *tmp_compexpr_right_20;
                PyObject *tmp_dict_name_48;
                PyObject *tmp_key_name_48;
                PyObject *tmp_list_arg_1;
                PyObject *tmp_xrange_low_1;
                PyObject *tmp_dict_name_49;
                PyObject *tmp_key_name_49;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_48 = var_file_info;
                tmp_key_name_48 = const_str_plain_colors;
                tmp_compexpr_left_20 = DICT_GET_ITEM( tmp_dict_name_48, tmp_key_name_48 );
                if ( tmp_compexpr_left_20 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 230;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_20 = const_int_pos_2;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_20, tmp_compexpr_right_20 );
                Py_DECREF( tmp_compexpr_left_20 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 230;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_21 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
                {
                    goto condexpr_true_5;
                }
                else
                {
                    goto condexpr_false_5;
                }
                condexpr_true_5:;
                tmp_assign_source_36 = const_tuple_int_0_int_pos_255_tuple;
                Py_INCREF( tmp_assign_source_36 );
                goto condexpr_end_5;
                condexpr_false_5:;
                CHECK_OBJECT( var_file_info );
                tmp_dict_name_49 = var_file_info;
                tmp_key_name_49 = const_str_plain_colors;
                tmp_xrange_low_1 = DICT_GET_ITEM( tmp_dict_name_49, tmp_key_name_49 );
                if ( tmp_xrange_low_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 231;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_list_arg_1 = BUILTIN_XRANGE1( tmp_xrange_low_1 );
                Py_DECREF( tmp_xrange_low_1 );
                if ( tmp_list_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 231;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_36 = PySequence_List( tmp_list_arg_1 );
                Py_DECREF( tmp_list_arg_1 );
                if ( tmp_assign_source_36 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 231;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                condexpr_end_5:;
                assert( var_indices == NULL );
                var_indices = tmp_assign_source_36;
            }
            {
                PyObject *tmp_assign_source_37;
                PyObject *tmp_iter_arg_8;
                PyObject *tmp_called_name_26;
                PyObject *tmp_args_element_name_25;
                tmp_called_name_26 = (PyObject *)&PyEnum_Type;
                CHECK_OBJECT( var_indices );
                tmp_args_element_name_25 = var_indices;
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 234;
                {
                    PyObject *call_args[] = { tmp_args_element_name_25 };
                    tmp_iter_arg_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, call_args );
                }

                if ( tmp_iter_arg_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_37 = MAKE_ITERATOR( tmp_iter_arg_8 );
                Py_DECREF( tmp_iter_arg_8 );
                if ( tmp_assign_source_37 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_for_loop_3__for_iterator == NULL );
                tmp_for_loop_3__for_iterator = tmp_assign_source_37;
            }
            // Tried code:
            loop_start_3:;
            {
                PyObject *tmp_next_source_3;
                PyObject *tmp_assign_source_38;
                CHECK_OBJECT( tmp_for_loop_3__for_iterator );
                tmp_next_source_3 = tmp_for_loop_3__for_iterator;
                tmp_assign_source_38 = ITERATOR_NEXT( tmp_next_source_3 );
                if ( tmp_assign_source_38 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_3;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "ooooooooooooooooooo";
                        exception_lineno = 234;
                        goto try_except_handler_14;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_3__iter_value;
                    tmp_for_loop_3__iter_value = tmp_assign_source_38;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_39;
                PyObject *tmp_iter_arg_9;
                CHECK_OBJECT( tmp_for_loop_3__iter_value );
                tmp_iter_arg_9 = tmp_for_loop_3__iter_value;
                tmp_assign_source_39 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_9 );
                if ( tmp_assign_source_39 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_15;
                }
                {
                    PyObject *old = tmp_tuple_unpack_5__source_iter;
                    tmp_tuple_unpack_5__source_iter = tmp_assign_source_39;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_40;
                PyObject *tmp_unpack_9;
                CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
                tmp_unpack_9 = tmp_tuple_unpack_5__source_iter;
                tmp_assign_source_40 = UNPACK_NEXT( tmp_unpack_9, 0, 2 );
                if ( tmp_assign_source_40 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooooooooooooooooo";
                    exception_lineno = 234;
                    goto try_except_handler_16;
                }
                {
                    PyObject *old = tmp_tuple_unpack_5__element_1;
                    tmp_tuple_unpack_5__element_1 = tmp_assign_source_40;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_41;
                PyObject *tmp_unpack_10;
                CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
                tmp_unpack_10 = tmp_tuple_unpack_5__source_iter;
                tmp_assign_source_41 = UNPACK_NEXT( tmp_unpack_10, 1, 2 );
                if ( tmp_assign_source_41 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "ooooooooooooooooooo";
                    exception_lineno = 234;
                    goto try_except_handler_16;
                }
                {
                    PyObject *old = tmp_tuple_unpack_5__element_2;
                    tmp_tuple_unpack_5__element_2 = tmp_assign_source_41;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_iterator_name_3;
                CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
                tmp_iterator_name_3 = tmp_tuple_unpack_5__source_iter;
                // Check if iterator has left-over elements.
                CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

                tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

                if (likely( tmp_iterator_attempt == NULL ))
                {
                    PyObject *error = GET_ERROR_OCCURRED();

                    if ( error != NULL )
                    {
                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                        {
                            CLEAR_ERROR_OCCURRED();
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "ooooooooooooooooooo";
                            exception_lineno = 234;
                            goto try_except_handler_16;
                        }
                    }
                }
                else
                {
                    Py_DECREF( tmp_iterator_attempt );

                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "ooooooooooooooooooo";
                    exception_lineno = 234;
                    goto try_except_handler_16;
                }
            }
            goto try_end_11;
            // Exception handler code:
            try_except_handler_16:;
            exception_keeper_type_12 = exception_type;
            exception_keeper_value_12 = exception_value;
            exception_keeper_tb_12 = exception_tb;
            exception_keeper_lineno_12 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
            Py_DECREF( tmp_tuple_unpack_5__source_iter );
            tmp_tuple_unpack_5__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_12;
            exception_value = exception_keeper_value_12;
            exception_tb = exception_keeper_tb_12;
            exception_lineno = exception_keeper_lineno_12;

            goto try_except_handler_15;
            // End of try:
            try_end_11:;
            goto try_end_12;
            // Exception handler code:
            try_except_handler_15:;
            exception_keeper_type_13 = exception_type;
            exception_keeper_value_13 = exception_value;
            exception_keeper_tb_13 = exception_tb;
            exception_keeper_lineno_13 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_tuple_unpack_5__element_1 );
            tmp_tuple_unpack_5__element_1 = NULL;

            Py_XDECREF( tmp_tuple_unpack_5__element_2 );
            tmp_tuple_unpack_5__element_2 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_13;
            exception_value = exception_keeper_value_13;
            exception_tb = exception_keeper_tb_13;
            exception_lineno = exception_keeper_lineno_13;

            goto try_except_handler_14;
            // End of try:
            try_end_12:;
            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
            Py_DECREF( tmp_tuple_unpack_5__source_iter );
            tmp_tuple_unpack_5__source_iter = NULL;

            {
                PyObject *tmp_assign_source_42;
                CHECK_OBJECT( tmp_tuple_unpack_5__element_1 );
                tmp_assign_source_42 = tmp_tuple_unpack_5__element_1;
                {
                    PyObject *old = var_ind;
                    var_ind = tmp_assign_source_42;
                    Py_INCREF( var_ind );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_tuple_unpack_5__element_1 );
            tmp_tuple_unpack_5__element_1 = NULL;

            {
                PyObject *tmp_assign_source_43;
                CHECK_OBJECT( tmp_tuple_unpack_5__element_2 );
                tmp_assign_source_43 = tmp_tuple_unpack_5__element_2;
                {
                    PyObject *old = var_val;
                    var_val = tmp_assign_source_43;
                    Py_INCREF( var_val );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_tuple_unpack_5__element_2 );
            tmp_tuple_unpack_5__element_2 = NULL;

            {
                PyObject *tmp_assign_source_44;
                PyObject *tmp_subscribed_name_17;
                PyObject *tmp_subscript_name_17;
                PyObject *tmp_start_name_2;
                PyObject *tmp_left_name_15;
                PyObject *tmp_right_name_15;
                PyObject *tmp_stop_name_2;
                PyObject *tmp_left_name_16;
                PyObject *tmp_left_name_17;
                PyObject *tmp_right_name_16;
                PyObject *tmp_right_name_17;
                PyObject *tmp_step_name_2;
                CHECK_OBJECT( var_palette );
                tmp_subscribed_name_17 = var_palette;
                CHECK_OBJECT( var_ind );
                tmp_left_name_15 = var_ind;
                CHECK_OBJECT( var_padding );
                tmp_right_name_15 = var_padding;
                tmp_start_name_2 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_15, tmp_right_name_15 );
                if ( tmp_start_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 235;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_14;
                }
                CHECK_OBJECT( var_ind );
                tmp_left_name_17 = var_ind;
                CHECK_OBJECT( var_padding );
                tmp_right_name_16 = var_padding;
                tmp_left_name_16 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_17, tmp_right_name_16 );
                if ( tmp_left_name_16 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_start_name_2 );

                    exception_lineno = 235;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_14;
                }
                tmp_right_name_17 = const_int_pos_3;
                tmp_stop_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_16, tmp_right_name_17 );
                Py_DECREF( tmp_left_name_16 );
                if ( tmp_stop_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_start_name_2 );

                    exception_lineno = 235;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_14;
                }
                tmp_step_name_2 = Py_None;
                tmp_subscript_name_17 = MAKE_SLICEOBJ3( tmp_start_name_2, tmp_stop_name_2, tmp_step_name_2 );
                Py_DECREF( tmp_start_name_2 );
                Py_DECREF( tmp_stop_name_2 );
                assert( !(tmp_subscript_name_17 == NULL) );
                tmp_assign_source_44 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_17, tmp_subscript_name_17 );
                Py_DECREF( tmp_subscript_name_17 );
                if ( tmp_assign_source_44 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 235;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_14;
                }
                {
                    PyObject *old = var_rgb;
                    var_rgb = tmp_assign_source_44;
                    Py_XDECREF( old );
                }

            }
            {
                nuitka_bool tmp_condition_result_22;
                PyObject *tmp_compexpr_left_21;
                PyObject *tmp_compexpr_right_21;
                PyObject *tmp_left_name_18;
                PyObject *tmp_called_name_27;
                PyObject *tmp_mvar_value_21;
                PyObject *tmp_args_element_name_26;
                PyObject *tmp_right_name_18;
                CHECK_OBJECT( var_rgb );
                tmp_compexpr_left_21 = var_rgb;
                tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o8 );

                if (unlikely( tmp_mvar_value_21 == NULL ))
                {
                    tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o8 );
                }

                if ( tmp_mvar_value_21 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o8" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 236;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_14;
                }

                tmp_called_name_27 = tmp_mvar_value_21;
                CHECK_OBJECT( var_val );
                tmp_args_element_name_26 = var_val;
                frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 236;
                {
                    PyObject *call_args[] = { tmp_args_element_name_26 };
                    tmp_left_name_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, call_args );
                }

                if ( tmp_left_name_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 236;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_14;
                }
                tmp_right_name_18 = const_int_pos_3;
                tmp_compexpr_right_21 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_18, tmp_right_name_18 );
                Py_DECREF( tmp_left_name_18 );
                if ( tmp_compexpr_right_21 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 236;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_14;
                }
                tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_21, tmp_compexpr_right_21 );
                Py_DECREF( tmp_compexpr_right_21 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 236;
                    type_description_1 = "ooooooooooooooooooo";
                    goto try_except_handler_14;
                }
                tmp_condition_result_22 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_17;
                }
                else
                {
                    goto branch_no_17;
                }
                branch_yes_17:;
                {
                    PyObject *tmp_assign_source_45;
                    tmp_assign_source_45 = Py_False;
                    {
                        PyObject *old = var_greyscale;
                        var_greyscale = tmp_assign_source_45;
                        Py_INCREF( var_greyscale );
                        Py_XDECREF( old );
                    }

                }
                branch_no_17:;
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 234;
                type_description_1 = "ooooooooooooooooooo";
                goto try_except_handler_14;
            }
            goto loop_start_3;
            loop_end_3:;
            goto try_end_13;
            // Exception handler code:
            try_except_handler_14:;
            exception_keeper_type_14 = exception_type;
            exception_keeper_value_14 = exception_value;
            exception_keeper_tb_14 = exception_tb;
            exception_keeper_lineno_14 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_3__iter_value );
            tmp_for_loop_3__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
            Py_DECREF( tmp_for_loop_3__for_iterator );
            tmp_for_loop_3__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_14;
            exception_value = exception_keeper_value_14;
            exception_tb = exception_keeper_tb_14;
            exception_lineno = exception_keeper_lineno_14;

            goto frame_exception_exit_1;
            // End of try:
            try_end_13:;
            Py_XDECREF( tmp_for_loop_3__iter_value );
            tmp_for_loop_3__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
            Py_DECREF( tmp_for_loop_3__for_iterator );
            tmp_for_loop_3__for_iterator = NULL;

            {
                nuitka_bool tmp_condition_result_23;
                int tmp_truth_name_4;
                if ( var_greyscale == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "greyscale" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 240;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_truth_name_4 = CHECK_IF_TRUE( var_greyscale );
                if ( tmp_truth_name_4 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 240;
                    type_description_1 = "ooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_23 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_18;
                }
                else
                {
                    goto branch_no_18;
                }
                branch_yes_18:;
                {
                    PyObject *tmp_assattr_name_5;
                    nuitka_bool tmp_condition_result_24;
                    PyObject *tmp_compexpr_left_22;
                    PyObject *tmp_compexpr_right_22;
                    PyObject *tmp_dict_name_50;
                    PyObject *tmp_key_name_50;
                    PyObject *tmp_assattr_target_5;
                    CHECK_OBJECT( var_file_info );
                    tmp_dict_name_50 = var_file_info;
                    tmp_key_name_50 = const_str_plain_colors;
                    tmp_compexpr_left_22 = DICT_GET_ITEM( tmp_dict_name_50, tmp_key_name_50 );
                    if ( tmp_compexpr_left_22 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 241;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_22 = const_int_pos_2;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_22, tmp_compexpr_right_22 );
                    Py_DECREF( tmp_compexpr_left_22 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 241;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_24 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
                    {
                        goto condexpr_true_6;
                    }
                    else
                    {
                        goto condexpr_false_6;
                    }
                    condexpr_true_6:;
                    tmp_assattr_name_5 = const_str_plain_1;
                    goto condexpr_end_6;
                    condexpr_false_6:;
                    tmp_assattr_name_5 = const_str_plain_L;
                    condexpr_end_6:;
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_5 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_mode, tmp_assattr_name_5 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 241;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                {
                    PyObject *tmp_assign_source_46;
                    PyObject *tmp_source_name_17;
                    CHECK_OBJECT( par_self );
                    tmp_source_name_17 = par_self;
                    tmp_assign_source_46 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_mode );
                    if ( tmp_assign_source_46 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 242;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_raw_mode;
                        var_raw_mode = tmp_assign_source_46;
                        Py_XDECREF( old );
                    }

                }
                goto branch_end_18;
                branch_no_18:;
                {
                    PyObject *tmp_assattr_name_6;
                    PyObject *tmp_assattr_target_6;
                    tmp_assattr_name_6 = const_str_plain_P;
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_6 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain_mode, tmp_assattr_name_6 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 244;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                {
                    PyObject *tmp_assattr_name_7;
                    PyObject *tmp_called_name_28;
                    PyObject *tmp_source_name_18;
                    PyObject *tmp_mvar_value_22;
                    PyObject *tmp_args_element_name_27;
                    nuitka_bool tmp_condition_result_25;
                    PyObject *tmp_compexpr_left_23;
                    PyObject *tmp_compexpr_right_23;
                    PyObject *tmp_args_element_name_28;
                    PyObject *tmp_assattr_target_7;
                    tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_ImagePalette );

                    if (unlikely( tmp_mvar_value_22 == NULL ))
                    {
                        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ImagePalette );
                    }

                    if ( tmp_mvar_value_22 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ImagePalette" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 245;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_18 = tmp_mvar_value_22;
                    tmp_called_name_28 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_raw );
                    if ( tmp_called_name_28 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 245;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_padding );
                    tmp_compexpr_left_23 = var_padding;
                    tmp_compexpr_right_23 = const_int_pos_4;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_23, tmp_compexpr_right_23 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_28 );

                        exception_lineno = 246;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
                    {
                        goto condexpr_true_7;
                    }
                    else
                    {
                        goto condexpr_false_7;
                    }
                    condexpr_true_7:;
                    tmp_args_element_name_27 = const_str_plain_BGRX;
                    goto condexpr_end_7;
                    condexpr_false_7:;
                    tmp_args_element_name_27 = const_str_plain_BGR;
                    condexpr_end_7:;
                    CHECK_OBJECT( var_palette );
                    tmp_args_element_name_28 = var_palette;
                    frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 245;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_27, tmp_args_element_name_28 };
                        tmp_assattr_name_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_28, call_args );
                    }

                    Py_DECREF( tmp_called_name_28 );
                    if ( tmp_assattr_name_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 245;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( par_self );
                    tmp_assattr_target_7 = par_self;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain_palette, tmp_assattr_name_7 );
                    Py_DECREF( tmp_assattr_name_7 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 245;
                        type_description_1 = "ooooooooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                branch_end_18:;
            }
            branch_end_15:;
        }
        branch_no_14:;
    }
    {
        PyObject *tmp_ass_subvalue_2;
        PyObject *tmp_dict_name_51;
        PyObject *tmp_key_name_51;
        PyObject *tmp_ass_subscribed_2;
        PyObject *tmp_source_name_19;
        PyObject *tmp_ass_subscript_2;
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_51 = var_file_info;
        tmp_key_name_51 = const_str_plain_compression;
        tmp_ass_subvalue_2 = DICT_GET_ITEM( tmp_dict_name_51, tmp_key_name_51 );
        if ( tmp_ass_subvalue_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 249;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_19 = par_self;
        tmp_ass_subscribed_2 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_info );
        if ( tmp_ass_subscribed_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_ass_subvalue_2 );

            exception_lineno = 249;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_ass_subscript_2 = const_str_plain_compression;
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
        Py_DECREF( tmp_ass_subscribed_2 );
        Py_DECREF( tmp_ass_subvalue_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 249;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_8;
        PyObject *tmp_list_element_1;
        PyObject *tmp_tuple_element_8;
        PyObject *tmp_tuple_element_9;
        PyObject *tmp_dict_name_52;
        PyObject *tmp_key_name_52;
        PyObject *tmp_dict_name_53;
        PyObject *tmp_key_name_53;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_20;
        PyObject *tmp_tuple_element_10;
        PyObject *tmp_left_name_19;
        PyObject *tmp_left_name_20;
        PyObject *tmp_left_name_21;
        PyObject *tmp_left_name_22;
        PyObject *tmp_dict_name_54;
        PyObject *tmp_key_name_54;
        PyObject *tmp_right_name_19;
        PyObject *tmp_dict_name_55;
        PyObject *tmp_key_name_55;
        PyObject *tmp_right_name_20;
        PyObject *tmp_right_name_21;
        PyObject *tmp_right_name_22;
        PyObject *tmp_dict_name_56;
        PyObject *tmp_key_name_56;
        PyObject *tmp_assattr_target_8;
        tmp_tuple_element_8 = const_str_plain_raw;
        tmp_list_element_1 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_8 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_8 );
        tmp_tuple_element_9 = const_int_0;
        tmp_tuple_element_8 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_9 );
        PyTuple_SET_ITEM( tmp_tuple_element_8, 0, tmp_tuple_element_9 );
        tmp_tuple_element_9 = const_int_0;
        Py_INCREF( tmp_tuple_element_9 );
        PyTuple_SET_ITEM( tmp_tuple_element_8, 1, tmp_tuple_element_9 );
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_52 = var_file_info;
        tmp_key_name_52 = const_str_plain_width;
        tmp_tuple_element_9 = DICT_GET_ITEM( tmp_dict_name_52, tmp_key_name_52 );
        if ( tmp_tuple_element_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_8 );

            exception_lineno = 252;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_tuple_element_8, 2, tmp_tuple_element_9 );
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_53 = var_file_info;
        tmp_key_name_53 = const_str_plain_height;
        tmp_tuple_element_9 = DICT_GET_ITEM( tmp_dict_name_53, tmp_key_name_53 );
        if ( tmp_tuple_element_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_8 );

            exception_lineno = 252;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_tuple_element_8, 3, tmp_tuple_element_9 );
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_8 );
        CHECK_OBJECT( par_offset );
        tmp_or_left_value_1 = par_offset;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 253;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( par_self );
        tmp_source_name_20 = par_self;
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_fp );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 253;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_ba43fad09fae1d303508d3a353e52d90->m_frame.f_lineno = 253;
        tmp_or_right_value_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_tell );
        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_or_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 253;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_8 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_tuple_element_8 = tmp_or_left_value_1;
        or_end_1:;
        PyTuple_SET_ITEM( tmp_list_element_1, 2, tmp_tuple_element_8 );
        if ( var_raw_mode == NULL )
        {
            Py_DECREF( tmp_list_element_1 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "raw_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 254;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_tuple_element_10 = var_raw_mode;
        tmp_tuple_element_8 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_10 );
        PyTuple_SET_ITEM( tmp_tuple_element_8, 0, tmp_tuple_element_10 );
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_54 = var_file_info;
        tmp_key_name_54 = const_str_plain_width;
        tmp_left_name_22 = DICT_GET_ITEM( tmp_dict_name_54, tmp_key_name_54 );
        if ( tmp_left_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_8 );

            exception_lineno = 255;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_55 = var_file_info;
        tmp_key_name_55 = const_str_plain_bits;
        tmp_right_name_19 = DICT_GET_ITEM( tmp_dict_name_55, tmp_key_name_55 );
        if ( tmp_right_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_8 );
            Py_DECREF( tmp_left_name_22 );

            exception_lineno = 255;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_21 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_22, tmp_right_name_19 );
        Py_DECREF( tmp_left_name_22 );
        Py_DECREF( tmp_right_name_19 );
        if ( tmp_left_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_8 );

            exception_lineno = 255;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_20 = const_int_pos_31;
        tmp_left_name_20 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_21, tmp_right_name_20 );
        Py_DECREF( tmp_left_name_21 );
        if ( tmp_left_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_8 );

            exception_lineno = 255;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_21 = const_int_pos_3;
        tmp_left_name_19 = BINARY_OPERATION( PyNumber_Rshift, tmp_left_name_20, tmp_right_name_21 );
        Py_DECREF( tmp_left_name_20 );
        if ( tmp_left_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_8 );

            exception_lineno = 255;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_22 = const_int_neg_4;
        tmp_tuple_element_10 = BINARY_OPERATION( PyNumber_And, tmp_left_name_19, tmp_right_name_22 );
        Py_DECREF( tmp_left_name_19 );
        if ( tmp_tuple_element_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_8 );

            exception_lineno = 255;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_tuple_element_8, 1, tmp_tuple_element_10 );
        CHECK_OBJECT( var_file_info );
        tmp_dict_name_56 = var_file_info;
        tmp_key_name_56 = const_str_plain_direction;
        tmp_tuple_element_10 = DICT_GET_ITEM( tmp_dict_name_56, tmp_key_name_56 );
        if ( tmp_tuple_element_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_list_element_1 );
            Py_DECREF( tmp_tuple_element_8 );

            exception_lineno = 256;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_tuple_element_8, 2, tmp_tuple_element_10 );
        PyTuple_SET_ITEM( tmp_list_element_1, 3, tmp_tuple_element_8 );
        tmp_assattr_name_8 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_assattr_name_8, 0, tmp_list_element_1 );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_8 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain_tile, tmp_assattr_name_8 );
        Py_DECREF( tmp_assattr_name_8 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 250;
            type_description_1 = "ooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ba43fad09fae1d303508d3a353e52d90 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ba43fad09fae1d303508d3a353e52d90 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ba43fad09fae1d303508d3a353e52d90, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ba43fad09fae1d303508d3a353e52d90->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ba43fad09fae1d303508d3a353e52d90, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ba43fad09fae1d303508d3a353e52d90,
        type_description_1,
        par_self,
        par_header,
        par_offset,
        var_read,
        var_seek,
        var_file_info,
        var_header_data,
        var_idx,
        var_mask,
        var_raw_mode,
        var_SUPPORTED,
        var_MASK_MODES,
        var_padding,
        var_palette,
        var_greyscale,
        var_indices,
        var_ind,
        var_val,
        var_rgb
    );


    // Release cached frame.
    if ( frame_ba43fad09fae1d303508d3a353e52d90 == cache_frame_ba43fad09fae1d303508d3a353e52d90 )
    {
        Py_DECREF( frame_ba43fad09fae1d303508d3a353e52d90 );
    }
    cache_frame_ba43fad09fae1d303508d3a353e52d90 = NULL;

    assertFrameObject( frame_ba43fad09fae1d303508d3a353e52d90 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_3__bitmap );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_header );
    Py_DECREF( par_header );
    par_header = NULL;

    CHECK_OBJECT( (PyObject *)par_offset );
    Py_DECREF( par_offset );
    par_offset = NULL;

    CHECK_OBJECT( (PyObject *)var_read );
    Py_DECREF( var_read );
    var_read = NULL;

    CHECK_OBJECT( (PyObject *)var_seek );
    Py_DECREF( var_seek );
    var_seek = NULL;

    CHECK_OBJECT( (PyObject *)var_file_info );
    Py_DECREF( var_file_info );
    var_file_info = NULL;

    CHECK_OBJECT( (PyObject *)var_header_data );
    Py_DECREF( var_header_data );
    var_header_data = NULL;

    Py_XDECREF( var_idx );
    var_idx = NULL;

    Py_XDECREF( var_mask );
    var_mask = NULL;

    Py_XDECREF( var_raw_mode );
    var_raw_mode = NULL;

    Py_XDECREF( var_SUPPORTED );
    var_SUPPORTED = NULL;

    Py_XDECREF( var_MASK_MODES );
    var_MASK_MODES = NULL;

    Py_XDECREF( var_padding );
    var_padding = NULL;

    Py_XDECREF( var_palette );
    var_palette = NULL;

    Py_XDECREF( var_greyscale );
    var_greyscale = NULL;

    Py_XDECREF( var_indices );
    var_indices = NULL;

    Py_XDECREF( var_ind );
    var_ind = NULL;

    Py_XDECREF( var_val );
    var_val = NULL;

    Py_XDECREF( var_rgb );
    var_rgb = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_15 = exception_type;
    exception_keeper_value_15 = exception_value;
    exception_keeper_tb_15 = exception_tb;
    exception_keeper_lineno_15 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_header );
    Py_DECREF( par_header );
    par_header = NULL;

    CHECK_OBJECT( (PyObject *)par_offset );
    Py_DECREF( par_offset );
    par_offset = NULL;

    Py_XDECREF( var_read );
    var_read = NULL;

    Py_XDECREF( var_seek );
    var_seek = NULL;

    Py_XDECREF( var_file_info );
    var_file_info = NULL;

    Py_XDECREF( var_header_data );
    var_header_data = NULL;

    Py_XDECREF( var_idx );
    var_idx = NULL;

    Py_XDECREF( var_mask );
    var_mask = NULL;

    Py_XDECREF( var_raw_mode );
    var_raw_mode = NULL;

    Py_XDECREF( var_SUPPORTED );
    var_SUPPORTED = NULL;

    Py_XDECREF( var_MASK_MODES );
    var_MASK_MODES = NULL;

    Py_XDECREF( var_padding );
    var_padding = NULL;

    Py_XDECREF( var_palette );
    var_palette = NULL;

    Py_XDECREF( var_greyscale );
    var_greyscale = NULL;

    Py_XDECREF( var_indices );
    var_indices = NULL;

    Py_XDECREF( var_ind );
    var_ind = NULL;

    Py_XDECREF( var_val );
    var_val = NULL;

    Py_XDECREF( var_rgb );
    var_rgb = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_15;
    exception_value = exception_keeper_value_15;
    exception_tb = exception_keeper_tb_15;
    exception_lineno = exception_keeper_lineno_15;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_3__bitmap );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct PIL$BmpImagePlugin$$$function_3__bitmap$$$genexpr_1_genexpr_locals {
    PyObject *var_x;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *PIL$BmpImagePlugin$$$function_3__bitmap$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct PIL$BmpImagePlugin$$$function_3__bitmap$$$genexpr_1_genexpr_locals *generator_heap = (struct PIL$BmpImagePlugin$$$function_3__bitmap$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_x = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_8d1eaf5a3b5b9731739a65a4ffaee591, module_PIL$BmpImagePlugin, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 123;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_x;
            generator_heap->var_x = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_x );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_int_arg_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_x );
        tmp_left_name_2 = generator_heap->var_x;
        tmp_right_name_1 = const_float_39_3701;
        tmp_left_name_1 = BINARY_OPERATION_TRUEDIV_OBJECT_FLOAT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 123;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_right_name_2 = const_float_0_5;
        tmp_int_arg_1 = BINARY_OPERATION_ADD_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_int_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 123;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = PyNumber_Int( tmp_int_arg_1 );
        Py_DECREF( tmp_int_arg_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 123;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_int_arg_1, sizeof(PyObject *), &tmp_left_name_1, sizeof(PyObject *), &tmp_left_name_2, sizeof(PyObject *), &tmp_right_name_1, sizeof(PyObject *), &tmp_right_name_2, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_int_arg_1, sizeof(PyObject *), &tmp_left_name_1, sizeof(PyObject *), &tmp_left_name_2, sizeof(PyObject *), &tmp_right_name_1, sizeof(PyObject *), &tmp_right_name_2, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 123;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 123;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_x
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_x );
    generator_heap->var_x = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_x );
    generator_heap->var_x = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *PIL$BmpImagePlugin$$$function_3__bitmap$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        PIL$BmpImagePlugin$$$function_3__bitmap$$$genexpr_1_genexpr_context,
        module_PIL$BmpImagePlugin,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_4ab0f9aaf08160ca1ff70c4131176341,
#endif
        codeobj_8d1eaf5a3b5b9731739a65a4ffaee591,
        1,
        sizeof(struct PIL$BmpImagePlugin$$$function_3__bitmap$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_PIL$BmpImagePlugin$$$function_4__open( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_head_data = NULL;
    PyObject *var_offset = NULL;
    struct Nuitka_FrameObject *frame_91ad53624b63d65e5ed6af7f162fcd41;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_91ad53624b63d65e5ed6af7f162fcd41 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_91ad53624b63d65e5ed6af7f162fcd41, codeobj_91ad53624b63d65e5ed6af7f162fcd41, module_PIL$BmpImagePlugin, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_91ad53624b63d65e5ed6af7f162fcd41 = cache_frame_91ad53624b63d65e5ed6af7f162fcd41;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_91ad53624b63d65e5ed6af7f162fcd41 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_91ad53624b63d65e5ed6af7f162fcd41 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_fp );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_91ad53624b63d65e5ed6af7f162fcd41->m_frame.f_lineno = 262;
        tmp_assign_source_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_read, &PyTuple_GET_ITEM( const_tuple_int_pos_14_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_head_data == NULL );
        var_head_data = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( var_head_data );
        tmp_subscribed_name_1 = var_head_data;
        tmp_subscript_name_1 = const_slice_int_0_int_pos_2_none;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 264;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_bytes_digest_181e1eeb195f3bcd8ad8a954f597cb5b;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 264;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_8981cffaaf03286e98d8ff2e38b0e22f;
            frame_91ad53624b63d65e5ed6af7f162fcd41->m_frame.f_lineno = 265;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_SyntaxError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 265;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32 );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_i32 );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "i32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 267;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_head_data );
        tmp_subscribed_name_2 = var_head_data;
        tmp_subscript_name_2 = const_slice_int_pos_10_int_pos_14_none;
        tmp_args_element_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 267;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_91ad53624b63d65e5ed6af7f162fcd41->m_frame.f_lineno = 267;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 267;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_offset == NULL );
        var_offset = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__bitmap );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 269;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_offset;
        CHECK_OBJECT( var_offset );
        tmp_dict_value_1 = var_offset;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_91ad53624b63d65e5ed6af7f162fcd41->m_frame.f_lineno = 269;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 269;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_91ad53624b63d65e5ed6af7f162fcd41 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_91ad53624b63d65e5ed6af7f162fcd41 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_91ad53624b63d65e5ed6af7f162fcd41, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_91ad53624b63d65e5ed6af7f162fcd41->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_91ad53624b63d65e5ed6af7f162fcd41, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_91ad53624b63d65e5ed6af7f162fcd41,
        type_description_1,
        par_self,
        var_head_data,
        var_offset
    );


    // Release cached frame.
    if ( frame_91ad53624b63d65e5ed6af7f162fcd41 == cache_frame_91ad53624b63d65e5ed6af7f162fcd41 )
    {
        Py_DECREF( frame_91ad53624b63d65e5ed6af7f162fcd41 );
    }
    cache_frame_91ad53624b63d65e5ed6af7f162fcd41 = NULL;

    assertFrameObject( frame_91ad53624b63d65e5ed6af7f162fcd41 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_4__open );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_head_data );
    Py_DECREF( var_head_data );
    var_head_data = NULL;

    CHECK_OBJECT( (PyObject *)var_offset );
    Py_DECREF( var_offset );
    var_offset = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_head_data );
    var_head_data = NULL;

    Py_XDECREF( var_offset );
    var_offset = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_4__open );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$BmpImagePlugin$$$function_5__open( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0951ac842b8dc1d147637f06ba935aeb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0951ac842b8dc1d147637f06ba935aeb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0951ac842b8dc1d147637f06ba935aeb, codeobj_0951ac842b8dc1d147637f06ba935aeb, module_PIL$BmpImagePlugin, sizeof(void *) );
    frame_0951ac842b8dc1d147637f06ba935aeb = cache_frame_0951ac842b8dc1d147637f06ba935aeb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0951ac842b8dc1d147637f06ba935aeb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0951ac842b8dc1d147637f06ba935aeb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_0951ac842b8dc1d147637f06ba935aeb->m_frame.f_lineno = 281;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain__bitmap );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 281;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0951ac842b8dc1d147637f06ba935aeb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0951ac842b8dc1d147637f06ba935aeb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0951ac842b8dc1d147637f06ba935aeb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0951ac842b8dc1d147637f06ba935aeb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0951ac842b8dc1d147637f06ba935aeb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0951ac842b8dc1d147637f06ba935aeb,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_0951ac842b8dc1d147637f06ba935aeb == cache_frame_0951ac842b8dc1d147637f06ba935aeb )
    {
        Py_DECREF( frame_0951ac842b8dc1d147637f06ba935aeb );
    }
    cache_frame_0951ac842b8dc1d147637f06ba935aeb = NULL;

    assertFrameObject( frame_0951ac842b8dc1d147637f06ba935aeb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_5__open );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_5__open );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$BmpImagePlugin$$$function_6__dib_save( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_im = python_pars[ 0 ];
    PyObject *par_fp = python_pars[ 1 ];
    PyObject *par_filename = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_d47584d67535e2b5612fc58bb6518bd0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d47584d67535e2b5612fc58bb6518bd0 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d47584d67535e2b5612fc58bb6518bd0, codeobj_d47584d67535e2b5612fc58bb6518bd0, module_PIL$BmpImagePlugin, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d47584d67535e2b5612fc58bb6518bd0 = cache_frame_d47584d67535e2b5612fc58bb6518bd0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d47584d67535e2b5612fc58bb6518bd0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d47584d67535e2b5612fc58bb6518bd0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain__save );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__save );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_save" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 298;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_im );
        tmp_args_element_name_1 = par_im;
        CHECK_OBJECT( par_fp );
        tmp_args_element_name_2 = par_fp;
        CHECK_OBJECT( par_filename );
        tmp_args_element_name_3 = par_filename;
        tmp_args_element_name_4 = Py_False;
        frame_d47584d67535e2b5612fc58bb6518bd0->m_frame.f_lineno = 298;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 298;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d47584d67535e2b5612fc58bb6518bd0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d47584d67535e2b5612fc58bb6518bd0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d47584d67535e2b5612fc58bb6518bd0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d47584d67535e2b5612fc58bb6518bd0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d47584d67535e2b5612fc58bb6518bd0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d47584d67535e2b5612fc58bb6518bd0,
        type_description_1,
        par_im,
        par_fp,
        par_filename
    );


    // Release cached frame.
    if ( frame_d47584d67535e2b5612fc58bb6518bd0 == cache_frame_d47584d67535e2b5612fc58bb6518bd0 )
    {
        Py_DECREF( frame_d47584d67535e2b5612fc58bb6518bd0 );
    }
    cache_frame_d47584d67535e2b5612fc58bb6518bd0 = NULL;

    assertFrameObject( frame_d47584d67535e2b5612fc58bb6518bd0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_6__dib_save );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    CHECK_OBJECT( (PyObject *)par_fp );
    Py_DECREF( par_fp );
    par_fp = NULL;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    CHECK_OBJECT( (PyObject *)par_fp );
    Py_DECREF( par_fp );
    par_fp = NULL;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_6__dib_save );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$BmpImagePlugin$$$function_7__save( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_im = python_pars[ 0 ];
    PyObject *par_fp = python_pars[ 1 ];
    PyObject *par_filename = python_pars[ 2 ];
    PyObject *par_bitmap_header = python_pars[ 3 ];
    PyObject *var_rawmode = NULL;
    PyObject *var_bits = NULL;
    PyObject *var_colors = NULL;
    PyObject *var_info = NULL;
    PyObject *var_dpi = NULL;
    PyObject *var_ppm = NULL;
    PyObject *var_stride = NULL;
    PyObject *var_image = NULL;
    PyObject *var_offset = NULL;
    PyObject *var_i = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_af5d674ae8579c3b7d41cf1c75d68331;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_af5d674ae8579c3b7d41cf1c75d68331 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_af5d674ae8579c3b7d41cf1c75d68331, codeobj_af5d674ae8579c3b7d41cf1c75d68331, module_PIL$BmpImagePlugin, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_af5d674ae8579c3b7d41cf1c75d68331 = cache_frame_af5d674ae8579c3b7d41cf1c75d68331;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_af5d674ae8579c3b7d41cf1c75d68331 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_af5d674ae8579c3b7d41cf1c75d68331 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_source_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_SAVE );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SAVE );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SAVE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 303;
            type_description_1 = "oooooooooooNooo";
            goto try_except_handler_3;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_im );
        tmp_source_name_1 = par_im;
        tmp_subscript_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_mode );
        if ( tmp_subscript_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 303;
            type_description_1 = "oooooooooooNooo";
            goto try_except_handler_3;
        }
        tmp_iter_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 303;
            type_description_1 = "oooooooooooNooo";
            goto try_except_handler_3;
        }
        tmp_assign_source_1 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 303;
            type_description_1 = "oooooooooooNooo";
            goto try_except_handler_3;
        }
        assert( tmp_tuple_unpack_1__source_iter == NULL );
        tmp_tuple_unpack_1__source_iter = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_2 = UNPACK_NEXT( tmp_unpack_1, 0, 3 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooNooo";
            exception_lineno = 303;
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_1__element_1 == NULL );
        tmp_tuple_unpack_1__element_1 = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_2, 1, 3 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooNooo";
            exception_lineno = 303;
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_1__element_2 == NULL );
        tmp_tuple_unpack_1__element_2 = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_3, 2, 3 );
        if ( tmp_assign_source_4 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooNooo";
            exception_lineno = 303;
            goto try_except_handler_4;
        }
        assert( tmp_tuple_unpack_1__element_3 == NULL );
        tmp_tuple_unpack_1__element_3 = tmp_assign_source_4;
    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooNooo";
                    exception_lineno = 303;
                    goto try_except_handler_4;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 3)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooNooo";
            exception_lineno = 303;
            goto try_except_handler_4;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_3;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_2;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_af5d674ae8579c3b7d41cf1c75d68331, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_af5d674ae8579c3b7d41cf1c75d68331, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "oooooooooooNooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_source_name_2;
            tmp_left_name_1 = const_str_digest_c75b60f810566e71a401075f6a675018;
            CHECK_OBJECT( par_im );
            tmp_source_name_2 = par_im;
            tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_mode );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 305;
                type_description_1 = "oooooooooooNooo";
                goto try_except_handler_5;
            }
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 305;
                type_description_1 = "oooooooooooNooo";
                goto try_except_handler_5;
            }
            frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 305;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_IOError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 305;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooooooNooo";
            goto try_except_handler_5;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 302;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame) frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooooooooNooo";
        goto try_except_handler_5;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_7__save );
    return NULL;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_5;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;
        assert( var_rawmode == NULL );
        Py_INCREF( tmp_assign_source_5 );
        var_rawmode = tmp_assign_source_5;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;
        assert( var_bits == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_bits = tmp_assign_source_6;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_3 );
        tmp_assign_source_7 = tmp_tuple_unpack_1__element_3;
        assert( var_colors == NULL );
        Py_INCREF( tmp_assign_source_7 );
        var_colors = tmp_assign_source_7;
    }
    Py_XDECREF( tmp_tuple_unpack_1__element_3 );
    tmp_tuple_unpack_1__element_3 = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_im );
        tmp_source_name_3 = par_im;
        tmp_assign_source_8 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_encoderinfo );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 307;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        assert( var_info == NULL );
        var_info = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_info );
        tmp_called_instance_1 = var_info;
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 309;
        tmp_assign_source_9 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_dpi_tuple_int_pos_96_int_pos_96_tuple_tuple, 0 ) );

        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 309;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        assert( var_dpi == NULL );
        var_dpi = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_tuple_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_called_name_1 = (PyObject *)&PyMap_Type;
        tmp_args_element_name_1 = MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_7__save$$$function_1_lambda(  );



        CHECK_OBJECT( var_dpi );
        tmp_args_element_name_2 = var_dpi;
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 312;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_tuple_arg_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_tuple_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = PySequence_Tuple( tmp_tuple_arg_1 );
        Py_DECREF( tmp_tuple_arg_1 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        assert( var_ppm == NULL );
        var_ppm = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_left_name_6;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_right_name_3;
        PyObject *tmp_right_name_4;
        PyObject *tmp_right_name_5;
        PyObject *tmp_right_name_6;
        CHECK_OBJECT( par_im );
        tmp_source_name_4 = par_im;
        tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_size );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_2 = const_int_0;
        tmp_left_name_6 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_left_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_bits );
        tmp_right_name_2 = var_bits;
        tmp_left_name_5 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_6 );
        if ( tmp_left_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = const_int_pos_7;
        tmp_left_name_4 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_5, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_5 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_4 = const_int_pos_8;
        tmp_left_name_3 = BINARY_OPERATION_FLOORDIV_OBJECT_LONG( tmp_left_name_4, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_4 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_5 = const_int_pos_3;
        tmp_left_name_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_3, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_3 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_6 = const_int_neg_4;
        tmp_assign_source_11 = BINARY_OPERATION( PyNumber_And, tmp_left_name_2, tmp_right_name_6 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 314;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        assert( var_stride == NULL );
        var_stride = tmp_assign_source_11;
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_left_name_7;
        PyObject *tmp_right_name_7;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_subscript_name_3;
        CHECK_OBJECT( var_stride );
        tmp_left_name_7 = var_stride;
        CHECK_OBJECT( par_im );
        tmp_source_name_5 = par_im;
        tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_size );
        if ( tmp_subscribed_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 316;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_3 = const_int_pos_1;
        tmp_right_name_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 1 );
        Py_DECREF( tmp_subscribed_name_3 );
        if ( tmp_right_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 316;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_7 );
        Py_DECREF( tmp_right_name_7 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 316;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        assert( var_image == NULL );
        var_image = tmp_assign_source_12;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_bitmap_header );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_bitmap_header );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_13;
            PyObject *tmp_left_name_8;
            PyObject *tmp_right_name_8;
            PyObject *tmp_left_name_9;
            PyObject *tmp_right_name_9;
            tmp_left_name_8 = const_int_pos_54;
            CHECK_OBJECT( var_colors );
            tmp_left_name_9 = var_colors;
            tmp_right_name_9 = const_int_pos_4;
            tmp_right_name_8 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_9, tmp_right_name_9 );
            if ( tmp_right_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_13 = BINARY_OPERATION_ADD_LONG_OBJECT( tmp_left_name_8, tmp_right_name_8 );
            Py_DECREF( tmp_right_name_8 );
            if ( tmp_assign_source_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            assert( var_offset == NULL );
            var_offset = tmp_assign_source_13;
        }
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_6;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_left_name_10;
            PyObject *tmp_left_name_11;
            PyObject *tmp_left_name_12;
            PyObject *tmp_right_name_10;
            PyObject *tmp_called_name_3;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_left_name_13;
            PyObject *tmp_right_name_11;
            PyObject *tmp_right_name_12;
            PyObject *tmp_called_name_4;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_right_name_13;
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_5;
            CHECK_OBJECT( par_fp );
            tmp_source_name_6 = par_fp;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_write );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 321;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_12 = const_bytes_digest_181e1eeb195f3bcd8ad8a954f597cb5b;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
            }

            if ( tmp_mvar_value_2 == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 322;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_3 = tmp_mvar_value_2;
            CHECK_OBJECT( var_offset );
            tmp_left_name_13 = var_offset;
            CHECK_OBJECT( var_image );
            tmp_right_name_11 = var_image;
            tmp_args_element_name_4 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_13, tmp_right_name_11 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 322;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 322;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_right_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_right_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 322;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_11 = BINARY_OPERATION_ADD_BYTES_OBJECT( tmp_left_name_12, tmp_right_name_10 );
            Py_DECREF( tmp_right_name_10 );
            if ( tmp_left_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 321;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_left_name_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 323;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_4 = tmp_mvar_value_3;
            frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 323;
            tmp_right_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

            if ( tmp_right_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_left_name_11 );

                exception_lineno = 323;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_10 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_11, tmp_right_name_12 );
            Py_DECREF( tmp_left_name_11 );
            Py_DECREF( tmp_right_name_12 );
            if ( tmp_left_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 322;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
            }

            if ( tmp_mvar_value_4 == NULL )
            {
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_left_name_10 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 324;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_5 = tmp_mvar_value_4;
            CHECK_OBJECT( var_offset );
            tmp_args_element_name_5 = var_offset;
            frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 324;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_right_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            if ( tmp_right_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_left_name_10 );

                exception_lineno = 324;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_3 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_10, tmp_right_name_13 );
            Py_DECREF( tmp_left_name_10 );
            Py_DECREF( tmp_right_name_13 );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 323;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 321;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 321;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_7;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_left_name_14;
        PyObject *tmp_left_name_15;
        PyObject *tmp_left_name_16;
        PyObject *tmp_left_name_17;
        PyObject *tmp_left_name_18;
        PyObject *tmp_left_name_19;
        PyObject *tmp_left_name_20;
        PyObject *tmp_left_name_21;
        PyObject *tmp_left_name_22;
        PyObject *tmp_left_name_23;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_right_name_14;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_source_name_8;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_right_name_15;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_source_name_9;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_right_name_16;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_right_name_17;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_right_name_18;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_right_name_19;
        PyObject *tmp_called_name_13;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_right_name_20;
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_subscript_name_6;
        PyObject *tmp_right_name_21;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_subscribed_name_7;
        PyObject *tmp_subscript_name_7;
        PyObject *tmp_right_name_22;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_right_name_23;
        PyObject *tmp_called_name_17;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_args_element_name_14;
        CHECK_OBJECT( par_fp );
        tmp_source_name_7 = par_fp;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_write );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 327;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 327;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_5;
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 327;
        tmp_left_name_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_int_pos_40_tuple, 0 ) );

        if ( tmp_left_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 327;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_23 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 328;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_6;
        CHECK_OBJECT( par_im );
        tmp_source_name_8 = par_im;
        tmp_subscribed_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_size );
        if ( tmp_subscribed_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_23 );

            exception_lineno = 328;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_4 = const_int_0;
        tmp_args_element_name_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
        Py_DECREF( tmp_subscribed_name_4 );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_23 );

            exception_lineno = 328;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 328;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_right_name_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_right_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_23 );

            exception_lineno = 328;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_22 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_23, tmp_right_name_14 );
        Py_DECREF( tmp_left_name_23 );
        Py_DECREF( tmp_right_name_14 );
        if ( tmp_left_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 327;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 329;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_7;
        CHECK_OBJECT( par_im );
        tmp_source_name_9 = par_im;
        tmp_subscribed_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_size );
        if ( tmp_subscribed_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_22 );

            exception_lineno = 329;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_5 = const_int_pos_1;
        tmp_args_element_name_8 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 1 );
        Py_DECREF( tmp_subscribed_name_5 );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_22 );

            exception_lineno = 329;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 329;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_right_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_right_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_22 );

            exception_lineno = 329;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_21 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_22, tmp_right_name_15 );
        Py_DECREF( tmp_left_name_22 );
        Py_DECREF( tmp_right_name_15 );
        if ( tmp_left_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 328;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o16 );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o16 );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_21 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o16" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 330;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_8;
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 330;
        tmp_right_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

        if ( tmp_right_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_21 );

            exception_lineno = 330;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_20 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_21, tmp_right_name_16 );
        Py_DECREF( tmp_left_name_21 );
        Py_DECREF( tmp_right_name_16 );
        if ( tmp_left_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 329;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o16 );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o16 );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_20 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o16" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 331;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_9;
        CHECK_OBJECT( var_bits );
        tmp_args_element_name_9 = var_bits;
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 331;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_right_name_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
        }

        if ( tmp_right_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_20 );

            exception_lineno = 331;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_19 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_20, tmp_right_name_17 );
        Py_DECREF( tmp_left_name_20 );
        Py_DECREF( tmp_right_name_17 );
        if ( tmp_left_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 330;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_19 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 332;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_10;
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 332;
        tmp_right_name_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_right_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_19 );

            exception_lineno = 332;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_18 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_19, tmp_right_name_18 );
        Py_DECREF( tmp_left_name_19 );
        Py_DECREF( tmp_right_name_18 );
        if ( tmp_left_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 331;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 333;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_13 = tmp_mvar_value_11;
        CHECK_OBJECT( var_image );
        tmp_args_element_name_10 = var_image;
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 333;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_right_name_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
        }

        if ( tmp_right_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_18 );

            exception_lineno = 333;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_17 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_18, tmp_right_name_19 );
        Py_DECREF( tmp_left_name_18 );
        Py_DECREF( tmp_right_name_19 );
        if ( tmp_left_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 332;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_17 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 334;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_12;
        CHECK_OBJECT( var_ppm );
        tmp_subscribed_name_6 = var_ppm;
        tmp_subscript_name_6 = const_int_0;
        tmp_args_element_name_11 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 0 );
        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_17 );

            exception_lineno = 334;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 334;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_right_name_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_right_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_17 );

            exception_lineno = 334;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_16 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_17, tmp_right_name_20 );
        Py_DECREF( tmp_left_name_17 );
        Py_DECREF( tmp_right_name_20 );
        if ( tmp_left_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 333;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
        }

        if ( tmp_mvar_value_13 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_16 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 334;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_13;
        CHECK_OBJECT( var_ppm );
        tmp_subscribed_name_7 = var_ppm;
        tmp_subscript_name_7 = const_int_pos_1;
        tmp_args_element_name_12 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_7, tmp_subscript_name_7, 1 );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_16 );

            exception_lineno = 334;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 334;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_right_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
        }

        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_right_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_16 );

            exception_lineno = 334;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_15 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_16, tmp_right_name_21 );
        Py_DECREF( tmp_left_name_16 );
        Py_DECREF( tmp_right_name_21 );
        if ( tmp_left_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 334;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_15 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 335;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_14;
        CHECK_OBJECT( var_colors );
        tmp_args_element_name_13 = var_colors;
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 335;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_right_name_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
        }

        if ( tmp_right_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_15 );

            exception_lineno = 335;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_14 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_15, tmp_right_name_22 );
        Py_DECREF( tmp_left_name_15 );
        Py_DECREF( tmp_right_name_22 );
        if ( tmp_left_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 334;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32 );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o32 );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_14 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 336;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_17 = tmp_mvar_value_15;
        CHECK_OBJECT( var_colors );
        tmp_args_element_name_14 = var_colors;
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 336;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_right_name_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, call_args );
        }

        if ( tmp_right_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_left_name_14 );

            exception_lineno = 336;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_6 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_14, tmp_right_name_23 );
        Py_DECREF( tmp_left_name_14 );
        Py_DECREF( tmp_right_name_23 );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 335;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 327;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 327;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_3;
        CHECK_OBJECT( par_fp );
        tmp_called_instance_2 = par_fp;
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 338;
        tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_write, &PyTuple_GET_ITEM( const_tuple_bytes_empty_tuple, 0 ) );

        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 338;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_10;
        CHECK_OBJECT( par_im );
        tmp_source_name_10 = par_im;
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_mode );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 340;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_str_plain_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 340;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_14;
            PyObject *tmp_iter_arg_2;
            tmp_iter_arg_2 = const_tuple_int_0_int_pos_255_tuple;
            tmp_assign_source_14 = MAKE_ITERATOR( tmp_iter_arg_2 );
            assert( !(tmp_assign_source_14 == NULL) );
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_14;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_15;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_15 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oooooooooooNooo";
                    exception_lineno = 341;
                    goto try_except_handler_6;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_15;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_16;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_16 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_i;
                var_i = tmp_assign_source_16;
                Py_INCREF( var_i );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_called_name_18;
            PyObject *tmp_source_name_11;
            PyObject *tmp_call_result_4;
            PyObject *tmp_args_element_name_15;
            PyObject *tmp_left_name_24;
            PyObject *tmp_called_name_19;
            PyObject *tmp_mvar_value_16;
            PyObject *tmp_args_element_name_16;
            PyObject *tmp_right_name_24;
            CHECK_OBJECT( par_fp );
            tmp_source_name_11 = par_fp;
            tmp_called_name_18 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_write );
            if ( tmp_called_name_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 342;
                type_description_1 = "oooooooooooNooo";
                goto try_except_handler_6;
            }
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o8 );

            if (unlikely( tmp_mvar_value_16 == NULL ))
            {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o8 );
            }

            if ( tmp_mvar_value_16 == NULL )
            {
                Py_DECREF( tmp_called_name_18 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o8" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 342;
                type_description_1 = "oooooooooooNooo";
                goto try_except_handler_6;
            }

            tmp_called_name_19 = tmp_mvar_value_16;
            CHECK_OBJECT( var_i );
            tmp_args_element_name_16 = var_i;
            frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 342;
            {
                PyObject *call_args[] = { tmp_args_element_name_16 };
                tmp_left_name_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
            }

            if ( tmp_left_name_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_18 );

                exception_lineno = 342;
                type_description_1 = "oooooooooooNooo";
                goto try_except_handler_6;
            }
            tmp_right_name_24 = const_int_pos_4;
            tmp_args_element_name_15 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_24, tmp_right_name_24 );
            Py_DECREF( tmp_left_name_24 );
            if ( tmp_args_element_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_18 );

                exception_lineno = 342;
                type_description_1 = "oooooooooooNooo";
                goto try_except_handler_6;
            }
            frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 342;
            {
                PyObject *call_args[] = { tmp_args_element_name_15 };
                tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
            }

            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_args_element_name_15 );
            if ( tmp_call_result_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 342;
                type_description_1 = "oooooooooooNooo";
                goto try_except_handler_6;
            }
            Py_DECREF( tmp_call_result_4 );
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 341;
            type_description_1 = "oooooooooooNooo";
            goto try_except_handler_6;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        goto branch_end_3;
        branch_no_3:;
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_source_name_12;
            CHECK_OBJECT( par_im );
            tmp_source_name_12 = par_im;
            tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_mode );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 343;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_3 = const_str_plain_L;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 343;
                type_description_1 = "oooooooooooNooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_17;
                PyObject *tmp_iter_arg_3;
                tmp_iter_arg_3 = const_xrange_0_256;
                tmp_assign_source_17 = MAKE_ITERATOR( tmp_iter_arg_3 );
                assert( !(tmp_assign_source_17 == NULL) );
                assert( tmp_for_loop_2__for_iterator == NULL );
                tmp_for_loop_2__for_iterator = tmp_assign_source_17;
            }
            // Tried code:
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_18;
                CHECK_OBJECT( tmp_for_loop_2__for_iterator );
                tmp_next_source_2 = tmp_for_loop_2__for_iterator;
                tmp_assign_source_18 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_18 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "oooooooooooNooo";
                        exception_lineno = 344;
                        goto try_except_handler_7;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_2__iter_value;
                    tmp_for_loop_2__iter_value = tmp_assign_source_18;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_19;
                CHECK_OBJECT( tmp_for_loop_2__iter_value );
                tmp_assign_source_19 = tmp_for_loop_2__iter_value;
                {
                    PyObject *old = var_i;
                    var_i = tmp_assign_source_19;
                    Py_INCREF( var_i );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_name_20;
                PyObject *tmp_source_name_13;
                PyObject *tmp_call_result_5;
                PyObject *tmp_args_element_name_17;
                PyObject *tmp_left_name_25;
                PyObject *tmp_called_name_21;
                PyObject *tmp_mvar_value_17;
                PyObject *tmp_args_element_name_18;
                PyObject *tmp_right_name_25;
                CHECK_OBJECT( par_fp );
                tmp_source_name_13 = par_fp;
                tmp_called_name_20 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_write );
                if ( tmp_called_name_20 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 345;
                    type_description_1 = "oooooooooooNooo";
                    goto try_except_handler_7;
                }
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o8 );

                if (unlikely( tmp_mvar_value_17 == NULL ))
                {
                    tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_o8 );
                }

                if ( tmp_mvar_value_17 == NULL )
                {
                    Py_DECREF( tmp_called_name_20 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "o8" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 345;
                    type_description_1 = "oooooooooooNooo";
                    goto try_except_handler_7;
                }

                tmp_called_name_21 = tmp_mvar_value_17;
                CHECK_OBJECT( var_i );
                tmp_args_element_name_18 = var_i;
                frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 345;
                {
                    PyObject *call_args[] = { tmp_args_element_name_18 };
                    tmp_left_name_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, call_args );
                }

                if ( tmp_left_name_25 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_20 );

                    exception_lineno = 345;
                    type_description_1 = "oooooooooooNooo";
                    goto try_except_handler_7;
                }
                tmp_right_name_25 = const_int_pos_4;
                tmp_args_element_name_17 = BINARY_OPERATION_MUL_OBJECT_LONG( tmp_left_name_25, tmp_right_name_25 );
                Py_DECREF( tmp_left_name_25 );
                if ( tmp_args_element_name_17 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_20 );

                    exception_lineno = 345;
                    type_description_1 = "oooooooooooNooo";
                    goto try_except_handler_7;
                }
                frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 345;
                {
                    PyObject *call_args[] = { tmp_args_element_name_17 };
                    tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
                }

                Py_DECREF( tmp_called_name_20 );
                Py_DECREF( tmp_args_element_name_17 );
                if ( tmp_call_result_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 345;
                    type_description_1 = "oooooooooooNooo";
                    goto try_except_handler_7;
                }
                Py_DECREF( tmp_call_result_5 );
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 344;
                type_description_1 = "oooooooooooNooo";
                goto try_except_handler_7;
            }
            goto loop_start_2;
            loop_end_2:;
            goto try_end_5;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_2__iter_value );
            tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
            Py_DECREF( tmp_for_loop_2__for_iterator );
            tmp_for_loop_2__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto frame_exception_exit_1;
            // End of try:
            try_end_5:;
            Py_XDECREF( tmp_for_loop_2__iter_value );
            tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
            Py_DECREF( tmp_for_loop_2__for_iterator );
            tmp_for_loop_2__for_iterator = NULL;

            goto branch_end_4;
            branch_no_4:;
            {
                nuitka_bool tmp_condition_result_5;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                PyObject *tmp_source_name_14;
                CHECK_OBJECT( par_im );
                tmp_source_name_14 = par_im;
                tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_mode );
                if ( tmp_compexpr_left_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 346;
                    type_description_1 = "oooooooooooNooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_4 = const_str_plain_P;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                Py_DECREF( tmp_compexpr_left_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 346;
                    type_description_1 = "oooooooooooNooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_called_name_22;
                    PyObject *tmp_source_name_15;
                    PyObject *tmp_call_result_6;
                    PyObject *tmp_args_element_name_19;
                    PyObject *tmp_called_instance_3;
                    PyObject *tmp_source_name_16;
                    CHECK_OBJECT( par_fp );
                    tmp_source_name_15 = par_fp;
                    tmp_called_name_22 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_write );
                    if ( tmp_called_name_22 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 347;
                        type_description_1 = "oooooooooooNooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( par_im );
                    tmp_source_name_16 = par_im;
                    tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_im );
                    if ( tmp_called_instance_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_22 );

                        exception_lineno = 347;
                        type_description_1 = "oooooooooooNooo";
                        goto frame_exception_exit_1;
                    }
                    frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 347;
                    tmp_args_element_name_19 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_3, const_str_plain_getpalette, &PyTuple_GET_ITEM( const_tuple_str_plain_RGB_str_plain_BGRX_tuple, 0 ) );

                    Py_DECREF( tmp_called_instance_3 );
                    if ( tmp_args_element_name_19 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_22 );

                        exception_lineno = 347;
                        type_description_1 = "oooooooooooNooo";
                        goto frame_exception_exit_1;
                    }
                    frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 347;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_19 };
                        tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
                    }

                    Py_DECREF( tmp_called_name_22 );
                    Py_DECREF( tmp_args_element_name_19 );
                    if ( tmp_call_result_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 347;
                        type_description_1 = "oooooooooooNooo";
                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_6 );
                }
                branch_no_5:;
            }
            branch_end_4:;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_called_name_23;
        PyObject *tmp_source_name_17;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_list_element_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_left_name_26;
        PyObject *tmp_right_name_26;
        PyObject *tmp_source_name_18;
        PyObject *tmp_tuple_element_2;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_ImageFile );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ImageFile );
        }

        if ( tmp_mvar_value_18 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 349;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_17 = tmp_mvar_value_18;
        tmp_called_name_23 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain__save );
        if ( tmp_called_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 349;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_im );
        tmp_args_element_name_20 = par_im;
        CHECK_OBJECT( par_fp );
        tmp_args_element_name_21 = par_fp;
        tmp_tuple_element_1 = const_str_plain_raw;
        tmp_list_element_1 = PyTuple_New( 4 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_1 );
        tmp_left_name_26 = const_tuple_int_0_int_0_tuple;
        CHECK_OBJECT( par_im );
        tmp_source_name_18 = par_im;
        tmp_right_name_26 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_size );
        if ( tmp_right_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_23 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 349;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = BINARY_OPERATION_ADD_TUPLE_OBJECT( tmp_left_name_26, tmp_right_name_26 );
        Py_DECREF( tmp_right_name_26 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_23 );
            Py_DECREF( tmp_list_element_1 );

            exception_lineno = 349;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_1 );
        tmp_tuple_element_1 = const_int_0;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_list_element_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( var_rawmode );
        tmp_tuple_element_2 = var_rawmode;
        tmp_tuple_element_1 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_tuple_element_1, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( var_stride );
        tmp_tuple_element_2 = var_stride;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_tuple_element_1, 1, tmp_tuple_element_2 );
        tmp_tuple_element_2 = const_int_neg_1;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_tuple_element_1, 2, tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_list_element_1, 3, tmp_tuple_element_1 );
        tmp_args_element_name_22 = PyList_New( 1 );
        PyList_SET_ITEM( tmp_args_element_name_22, 0, tmp_list_element_1 );
        frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame.f_lineno = 349;
        {
            PyObject *call_args[] = { tmp_args_element_name_20, tmp_args_element_name_21, tmp_args_element_name_22 };
            tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_23, call_args );
        }

        Py_DECREF( tmp_called_name_23 );
        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 349;
            type_description_1 = "oooooooooooNooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_af5d674ae8579c3b7d41cf1c75d68331 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_af5d674ae8579c3b7d41cf1c75d68331 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_af5d674ae8579c3b7d41cf1c75d68331, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_af5d674ae8579c3b7d41cf1c75d68331->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_af5d674ae8579c3b7d41cf1c75d68331, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_af5d674ae8579c3b7d41cf1c75d68331,
        type_description_1,
        par_im,
        par_fp,
        par_filename,
        par_bitmap_header,
        var_rawmode,
        var_bits,
        var_colors,
        var_info,
        var_dpi,
        var_ppm,
        var_stride,
        NULL,
        var_image,
        var_offset,
        var_i
    );


    // Release cached frame.
    if ( frame_af5d674ae8579c3b7d41cf1c75d68331 == cache_frame_af5d674ae8579c3b7d41cf1c75d68331 )
    {
        Py_DECREF( frame_af5d674ae8579c3b7d41cf1c75d68331 );
    }
    cache_frame_af5d674ae8579c3b7d41cf1c75d68331 = NULL;

    assertFrameObject( frame_af5d674ae8579c3b7d41cf1c75d68331 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_7__save );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    CHECK_OBJECT( (PyObject *)par_fp );
    Py_DECREF( par_fp );
    par_fp = NULL;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    CHECK_OBJECT( (PyObject *)par_bitmap_header );
    Py_DECREF( par_bitmap_header );
    par_bitmap_header = NULL;

    CHECK_OBJECT( (PyObject *)var_rawmode );
    Py_DECREF( var_rawmode );
    var_rawmode = NULL;

    CHECK_OBJECT( (PyObject *)var_bits );
    Py_DECREF( var_bits );
    var_bits = NULL;

    CHECK_OBJECT( (PyObject *)var_colors );
    Py_DECREF( var_colors );
    var_colors = NULL;

    CHECK_OBJECT( (PyObject *)var_info );
    Py_DECREF( var_info );
    var_info = NULL;

    CHECK_OBJECT( (PyObject *)var_dpi );
    Py_DECREF( var_dpi );
    var_dpi = NULL;

    CHECK_OBJECT( (PyObject *)var_ppm );
    Py_DECREF( var_ppm );
    var_ppm = NULL;

    CHECK_OBJECT( (PyObject *)var_stride );
    Py_DECREF( var_stride );
    var_stride = NULL;

    CHECK_OBJECT( (PyObject *)var_image );
    Py_DECREF( var_image );
    var_image = NULL;

    Py_XDECREF( var_offset );
    var_offset = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_im );
    Py_DECREF( par_im );
    par_im = NULL;

    CHECK_OBJECT( (PyObject *)par_fp );
    Py_DECREF( par_fp );
    par_fp = NULL;

    CHECK_OBJECT( (PyObject *)par_filename );
    Py_DECREF( par_filename );
    par_filename = NULL;

    CHECK_OBJECT( (PyObject *)par_bitmap_header );
    Py_DECREF( par_bitmap_header );
    par_bitmap_header = NULL;

    Py_XDECREF( var_rawmode );
    var_rawmode = NULL;

    Py_XDECREF( var_bits );
    var_bits = NULL;

    Py_XDECREF( var_colors );
    var_colors = NULL;

    Py_XDECREF( var_info );
    var_info = NULL;

    Py_XDECREF( var_dpi );
    var_dpi = NULL;

    Py_XDECREF( var_ppm );
    var_ppm = NULL;

    Py_XDECREF( var_stride );
    var_stride = NULL;

    Py_XDECREF( var_image );
    var_image = NULL;

    Py_XDECREF( var_offset );
    var_offset = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_7__save );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_PIL$BmpImagePlugin$$$function_7__save$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_x = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2ef5b8b98f7babf49d79d600f651cdc2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2ef5b8b98f7babf49d79d600f651cdc2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2ef5b8b98f7babf49d79d600f651cdc2, codeobj_2ef5b8b98f7babf49d79d600f651cdc2, module_PIL$BmpImagePlugin, sizeof(void *) );
    frame_2ef5b8b98f7babf49d79d600f651cdc2 = cache_frame_2ef5b8b98f7babf49d79d600f651cdc2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2ef5b8b98f7babf49d79d600f651cdc2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2ef5b8b98f7babf49d79d600f651cdc2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_int_arg_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( par_x );
        tmp_left_name_2 = par_x;
        tmp_right_name_1 = const_float_39_3701;
        tmp_left_name_1 = BINARY_OPERATION_MUL_OBJECT_FLOAT( tmp_left_name_2, tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_float_0_5;
        tmp_int_arg_1 = BINARY_OPERATION_ADD_OBJECT_FLOAT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_int_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = PyNumber_Int( tmp_int_arg_1 );
        Py_DECREF( tmp_int_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 312;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ef5b8b98f7babf49d79d600f651cdc2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ef5b8b98f7babf49d79d600f651cdc2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ef5b8b98f7babf49d79d600f651cdc2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2ef5b8b98f7babf49d79d600f651cdc2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2ef5b8b98f7babf49d79d600f651cdc2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2ef5b8b98f7babf49d79d600f651cdc2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2ef5b8b98f7babf49d79d600f651cdc2,
        type_description_1,
        par_x
    );


    // Release cached frame.
    if ( frame_2ef5b8b98f7babf49d79d600f651cdc2 == cache_frame_2ef5b8b98f7babf49d79d600f651cdc2 )
    {
        Py_DECREF( frame_2ef5b8b98f7babf49d79d600f651cdc2 );
    }
    cache_frame_2ef5b8b98f7babf49d79d600f651cdc2 = NULL;

    assertFrameObject( frame_2ef5b8b98f7babf49d79d600f651cdc2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_7__save$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_x );
    Py_DECREF( par_x );
    par_x = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin$$$function_7__save$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_1__accept(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$BmpImagePlugin$$$function_1__accept,
        const_str_plain__accept,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1da488e34b64dfe55223e09ba30a3702,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$BmpImagePlugin,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_2__dib_accept(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$BmpImagePlugin$$$function_2__dib_accept,
        const_str_plain__dib_accept,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c1ff15fbc2b21e907274b08755454ab2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$BmpImagePlugin,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_3__bitmap( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$BmpImagePlugin$$$function_3__bitmap,
        const_str_plain__bitmap,
#if PYTHON_VERSION >= 300
        const_str_digest_40e9a04769198a44edba14d650f76b42,
#endif
        codeobj_ba43fad09fae1d303508d3a353e52d90,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$BmpImagePlugin,
        const_str_digest_b374c723e5277778d29b61bb9eb3a478,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_4__open(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$BmpImagePlugin$$$function_4__open,
        const_str_plain__open,
#if PYTHON_VERSION >= 300
        const_str_digest_bde65f20a0edb67aa82d817e7642f0ab,
#endif
        codeobj_91ad53624b63d65e5ed6af7f162fcd41,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$BmpImagePlugin,
        const_str_digest_ab07d643992060511107c457f3bc9b0b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_5__open(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$BmpImagePlugin$$$function_5__open,
        const_str_plain__open,
#if PYTHON_VERSION >= 300
        const_str_digest_9184770994a8dfc7f5067f9878a65025,
#endif
        codeobj_0951ac842b8dc1d147637f06ba935aeb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$BmpImagePlugin,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_6__dib_save(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$BmpImagePlugin$$$function_6__dib_save,
        const_str_plain__dib_save,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d47584d67535e2b5612fc58bb6518bd0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$BmpImagePlugin,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_7__save( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$BmpImagePlugin$$$function_7__save,
        const_str_plain__save,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_af5d674ae8579c3b7d41cf1c75d68331,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$BmpImagePlugin,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_7__save$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_PIL$BmpImagePlugin$$$function_7__save$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_4621a4026644fa18656e149a28e882cc,
#endif
        codeobj_2ef5b8b98f7babf49d79d600f651cdc2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_PIL$BmpImagePlugin,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_PIL$BmpImagePlugin =
{
    PyModuleDef_HEAD_INIT,
    "PIL.BmpImagePlugin",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(PIL$BmpImagePlugin)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(PIL$BmpImagePlugin)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_PIL$BmpImagePlugin );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("PIL.BmpImagePlugin: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.BmpImagePlugin: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("PIL.BmpImagePlugin: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initPIL$BmpImagePlugin" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_PIL$BmpImagePlugin = Py_InitModule4(
        "PIL.BmpImagePlugin",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_PIL$BmpImagePlugin = PyModule_Create( &mdef_PIL$BmpImagePlugin );
#endif

    moduledict_PIL$BmpImagePlugin = MODULE_DICT( module_PIL$BmpImagePlugin );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_PIL$BmpImagePlugin,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_PIL$BmpImagePlugin,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$BmpImagePlugin,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_PIL$BmpImagePlugin,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_PIL$BmpImagePlugin );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_af7895b8e9a7643f2ab308f77e96c311, module_PIL$BmpImagePlugin );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *tmp_BmpImageFile$tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_BmpImageFile$tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_BmpImageFile$tuple_unpack_1__element_3 = NULL;
    PyObject *tmp_BmpImageFile$tuple_unpack_1__element_4 = NULL;
    PyObject *tmp_BmpImageFile$tuple_unpack_1__element_5 = NULL;
    PyObject *tmp_BmpImageFile$tuple_unpack_1__element_6 = NULL;
    PyObject *tmp_BmpImageFile$tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    struct Nuitka_FrameObject *frame_9a431d03a6469f0f6984fda36b405772;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_PIL$BmpImagePlugin_61 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *locals_PIL$BmpImagePlugin_275 = NULL;
    struct Nuitka_FrameObject *frame_c789dea7fb93995d63627c2800a6c64d_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_c789dea7fb93995d63627c2800a6c64d_3 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_9a431d03a6469f0f6984fda36b405772 = MAKE_MODULE_FRAME( codeobj_9a431d03a6469f0f6984fda36b405772, module_PIL$BmpImagePlugin );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_9a431d03a6469f0f6984fda36b405772 );
    assert( Py_REFCNT( frame_9a431d03a6469f0f6984fda36b405772 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_empty;
        tmp_globals_name_1 = (PyObject *)moduledict_PIL$BmpImagePlugin;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_Image_str_plain_ImageFile_str_plain_ImagePalette_tuple;
        tmp_level_name_1 = const_int_pos_1;
        frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 27;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_4;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_1 ) )
        {
           tmp_assign_source_5 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_1,
                (PyObject *)moduledict_PIL$BmpImagePlugin,
                const_str_plain_Image,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Image );
        }

        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_Image, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_2 ) )
        {
           tmp_assign_source_6 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_2,
                (PyObject *)moduledict_PIL$BmpImagePlugin,
                const_str_plain_ImageFile,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_ImageFile );
        }

        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_ImageFile, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_3 ) )
        {
           tmp_assign_source_7 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_3,
                (PyObject *)moduledict_PIL$BmpImagePlugin,
                const_str_plain_ImagePalette,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_ImagePalette );
        }

        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 27;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_ImagePalette, tmp_assign_source_7 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain__binary;
        tmp_globals_name_2 = (PyObject *)moduledict_PIL$BmpImagePlugin;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_3a2700929bfdf8368dbc820243ae0d59_tuple;
        tmp_level_name_2 = const_int_pos_1;
        frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 28;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_8;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_4 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_4 ) )
        {
           tmp_assign_source_9 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_4,
                (PyObject *)moduledict_PIL$BmpImagePlugin,
                const_str_plain_i8,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_i8 );
        }

        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i8, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_5 ) )
        {
           tmp_assign_source_10 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_5,
                (PyObject *)moduledict_PIL$BmpImagePlugin,
                const_str_plain_i16le,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_i16le );
        }

        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i16, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_6 ) )
        {
           tmp_assign_source_11 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_6,
                (PyObject *)moduledict_PIL$BmpImagePlugin,
                const_str_plain_i32le,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_i32le );
        }

        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_i32, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_7 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_7 ) )
        {
           tmp_assign_source_12 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_7,
                (PyObject *)moduledict_PIL$BmpImagePlugin,
                const_str_plain_o8,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_o8 );
        }

        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o8, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_8 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_8 ) )
        {
           tmp_assign_source_13 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_8,
                (PyObject *)moduledict_PIL$BmpImagePlugin,
                const_str_plain_o16le,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_o16le );
        }

        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o16, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_9 = tmp_import_from_2__module;
        if ( PyModule_Check( tmp_import_name_from_9 ) )
        {
           tmp_assign_source_14 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_9,
                (PyObject *)moduledict_PIL$BmpImagePlugin,
                const_str_plain_o32le,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_o32le );
        }

        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_o32, tmp_assign_source_14 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = const_str_digest_e79ed04e2052da0c6d3f455a76033a16;
        UPDATE_STRING_DICT0( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain___version__, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = PyDict_Copy( const_dict_58d7ae98441ebd965650132eb201c985 );
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_BIT2MODE, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_1__accept(  );



        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain__accept, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_2__dib_accept(  );



        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain__dib_accept, tmp_assign_source_18 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_ImageFile );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ImageFile );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 61;

            goto try_except_handler_3;
        }

        tmp_source_name_1 = tmp_mvar_value_3;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_ImageFile );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_3;
        }
        tmp_assign_source_19 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_assign_source_19, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_19;
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_20 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_20;
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_21;
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_3;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_3;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_3;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_3;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_22 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_3;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_22;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_3;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_3;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_2 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_2, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_23;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_3 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;

                goto try_except_handler_3;
            }
            tmp_tuple_element_2 = const_str_plain_BmpImageFile;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 61;
            tmp_assign_source_23 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;

                goto try_except_handler_3;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_23;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_4 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_4, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;

                goto try_except_handler_3;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_5;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 61;

                    goto try_except_handler_3;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_5 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_5 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_5 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 61;

                    goto try_except_handler_3;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 61;

                    goto try_except_handler_3;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 61;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_3;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_24;
            tmp_assign_source_24 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_24;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_25;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_PIL$BmpImagePlugin_61 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_af7895b8e9a7643f2ab308f77e96c311;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_digest_9c5eae82d3cef0578e37480eb32e9977;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_5;
        }
        tmp_dictset_value = const_str_plain_BmpImageFile;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 61;

            goto try_except_handler_5;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2, codeobj_ecc6fb4c94f111dc4d1d1cbf06a4a8d8, module_PIL$BmpImagePlugin, sizeof(void *) );
        frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 = cache_frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = const_str_digest_7694c8aa5098fdbfc9d126eeda02356f;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain_format_description, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 65;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = const_str_plain_BMP;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain_format, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = PyDict_Copy( const_dict_573115a447ef2f8dd7ef6812133c5976 );
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain_COMPRESSIONS, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_assign_source_26;
            PyObject *tmp_iter_arg_1;
            tmp_iter_arg_1 = const_tuple_int_0_int_pos_1_int_pos_2_int_pos_3_int_pos_4_int_pos_5_tuple;
            tmp_assign_source_26 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
            assert( !(tmp_assign_source_26 == NULL) );
            assert( tmp_BmpImageFile$tuple_unpack_1__source_iter == NULL );
            tmp_BmpImageFile$tuple_unpack_1__source_iter = tmp_assign_source_26;
        }
        // Tried code:
        // Tried code:
        {
            PyObject *tmp_assign_source_27;
            PyObject *tmp_unpack_1;
            CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__source_iter );
            tmp_unpack_1 = tmp_BmpImageFile$tuple_unpack_1__source_iter;
            tmp_assign_source_27 = UNPACK_NEXT( tmp_unpack_1, 0, 6 );
            if ( tmp_assign_source_27 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_2 = "o";
                exception_lineno = 77;
                goto try_except_handler_7;
            }
            assert( tmp_BmpImageFile$tuple_unpack_1__element_1 == NULL );
            tmp_BmpImageFile$tuple_unpack_1__element_1 = tmp_assign_source_27;
        }
        {
            PyObject *tmp_assign_source_28;
            PyObject *tmp_unpack_2;
            CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__source_iter );
            tmp_unpack_2 = tmp_BmpImageFile$tuple_unpack_1__source_iter;
            tmp_assign_source_28 = UNPACK_NEXT( tmp_unpack_2, 1, 6 );
            if ( tmp_assign_source_28 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_2 = "o";
                exception_lineno = 77;
                goto try_except_handler_7;
            }
            assert( tmp_BmpImageFile$tuple_unpack_1__element_2 == NULL );
            tmp_BmpImageFile$tuple_unpack_1__element_2 = tmp_assign_source_28;
        }
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_unpack_3;
            CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__source_iter );
            tmp_unpack_3 = tmp_BmpImageFile$tuple_unpack_1__source_iter;
            tmp_assign_source_29 = UNPACK_NEXT( tmp_unpack_3, 2, 6 );
            if ( tmp_assign_source_29 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_2 = "o";
                exception_lineno = 77;
                goto try_except_handler_7;
            }
            assert( tmp_BmpImageFile$tuple_unpack_1__element_3 == NULL );
            tmp_BmpImageFile$tuple_unpack_1__element_3 = tmp_assign_source_29;
        }
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_unpack_4;
            CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__source_iter );
            tmp_unpack_4 = tmp_BmpImageFile$tuple_unpack_1__source_iter;
            tmp_assign_source_30 = UNPACK_NEXT( tmp_unpack_4, 3, 6 );
            if ( tmp_assign_source_30 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_2 = "o";
                exception_lineno = 77;
                goto try_except_handler_7;
            }
            assert( tmp_BmpImageFile$tuple_unpack_1__element_4 == NULL );
            tmp_BmpImageFile$tuple_unpack_1__element_4 = tmp_assign_source_30;
        }
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_unpack_5;
            CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__source_iter );
            tmp_unpack_5 = tmp_BmpImageFile$tuple_unpack_1__source_iter;
            tmp_assign_source_31 = UNPACK_NEXT( tmp_unpack_5, 4, 6 );
            if ( tmp_assign_source_31 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_2 = "o";
                exception_lineno = 77;
                goto try_except_handler_7;
            }
            assert( tmp_BmpImageFile$tuple_unpack_1__element_5 == NULL );
            tmp_BmpImageFile$tuple_unpack_1__element_5 = tmp_assign_source_31;
        }
        {
            PyObject *tmp_assign_source_32;
            PyObject *tmp_unpack_6;
            CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__source_iter );
            tmp_unpack_6 = tmp_BmpImageFile$tuple_unpack_1__source_iter;
            tmp_assign_source_32 = UNPACK_NEXT( tmp_unpack_6, 5, 6 );
            if ( tmp_assign_source_32 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_2 = "o";
                exception_lineno = 77;
                goto try_except_handler_7;
            }
            assert( tmp_BmpImageFile$tuple_unpack_1__element_6 == NULL );
            tmp_BmpImageFile$tuple_unpack_1__element_6 = tmp_assign_source_32;
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_BmpImageFile$tuple_unpack_1__source_iter );
        Py_DECREF( tmp_BmpImageFile$tuple_unpack_1__source_iter );
        tmp_BmpImageFile$tuple_unpack_1__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_6;
        // End of try:
        try_end_3:;
        CHECK_OBJECT( (PyObject *)tmp_BmpImageFile$tuple_unpack_1__source_iter );
        Py_DECREF( tmp_BmpImageFile$tuple_unpack_1__source_iter );
        tmp_BmpImageFile$tuple_unpack_1__source_iter = NULL;

        CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__element_1 );
        tmp_dictset_value = tmp_BmpImageFile$tuple_unpack_1__element_1;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain_RAW, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_2 = "o";
            goto try_except_handler_6;
        }
        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_1 );
        tmp_BmpImageFile$tuple_unpack_1__element_1 = NULL;

        CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__element_2 );
        tmp_dictset_value = tmp_BmpImageFile$tuple_unpack_1__element_2;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain_RLE8, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_2 = "o";
            goto try_except_handler_6;
        }
        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_2 );
        tmp_BmpImageFile$tuple_unpack_1__element_2 = NULL;

        CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__element_3 );
        tmp_dictset_value = tmp_BmpImageFile$tuple_unpack_1__element_3;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain_RLE4, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_2 = "o";
            goto try_except_handler_6;
        }
        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_3 );
        tmp_BmpImageFile$tuple_unpack_1__element_3 = NULL;

        CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__element_4 );
        tmp_dictset_value = tmp_BmpImageFile$tuple_unpack_1__element_4;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain_BITFIELDS, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_2 = "o";
            goto try_except_handler_6;
        }
        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_4 );
        tmp_BmpImageFile$tuple_unpack_1__element_4 = NULL;

        CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__element_5 );
        tmp_dictset_value = tmp_BmpImageFile$tuple_unpack_1__element_5;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain_JPEG, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_2 = "o";
            goto try_except_handler_6;
        }
        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_5 );
        tmp_BmpImageFile$tuple_unpack_1__element_5 = NULL;

        CHECK_OBJECT( tmp_BmpImageFile$tuple_unpack_1__element_6 );
        tmp_dictset_value = tmp_BmpImageFile$tuple_unpack_1__element_6;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain_PNG, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_2 = "o";
            goto try_except_handler_6;
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_1 );
        tmp_BmpImageFile$tuple_unpack_1__element_1 = NULL;

        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_2 );
        tmp_BmpImageFile$tuple_unpack_1__element_2 = NULL;

        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_3 );
        tmp_BmpImageFile$tuple_unpack_1__element_3 = NULL;

        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_4 );
        tmp_BmpImageFile$tuple_unpack_1__element_4 = NULL;

        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_5 );
        tmp_BmpImageFile$tuple_unpack_1__element_5 = NULL;

        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_6 );
        tmp_BmpImageFile$tuple_unpack_1__element_6 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_2;
        // End of try:
        try_end_4:;
        Py_XDECREF( tmp_BmpImageFile$tuple_unpack_1__element_6 );
        tmp_BmpImageFile$tuple_unpack_1__element_6 = NULL;

        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_int_0_int_0_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_3__bitmap( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain__bitmap, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_4__open(  );



        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain__open, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 == cache_frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 )
        {
            Py_DECREF( frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 );
        }
        cache_frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 = NULL;

        assertFrameObject( frame_ecc6fb4c94f111dc4d1d1cbf06a4a8d8_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_5;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_1 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;

                goto try_except_handler_5;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_61, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;

                goto try_except_handler_5;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_BmpImageFile;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_PIL$BmpImagePlugin_61;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 61;
            tmp_assign_source_33 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;

                goto try_except_handler_5;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_33;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_25 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_25 );
        goto try_return_handler_5;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        Py_DECREF( locals_PIL$BmpImagePlugin_61 );
        locals_PIL$BmpImagePlugin_61 = NULL;
        goto try_return_handler_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_PIL$BmpImagePlugin_61 );
        locals_PIL$BmpImagePlugin_61 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_4;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 61;
        goto try_except_handler_3;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_BmpImageFile, tmp_assign_source_25 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BmpImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 275;

            goto try_except_handler_8;
        }

        tmp_tuple_element_5 = tmp_mvar_value_4;
        tmp_assign_source_34 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_assign_source_34, 0, tmp_tuple_element_5 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_34;
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_35 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_8;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_35;
    }
    {
        PyObject *tmp_assign_source_36;
        tmp_assign_source_36 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_36;
    }
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_8;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_8;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_8;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_2 = tmp_class_creation_2__bases;
        tmp_subscript_name_2 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_8;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_8;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_37 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_8;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_37;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_8;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_8;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_6 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_6, const_str_plain___prepare__ );
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_38;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_7;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_7 = tmp_class_creation_2__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 275;

                goto try_except_handler_8;
            }
            tmp_tuple_element_6 = const_str_plain_DibImageFile;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 275;
            tmp_assign_source_38 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_38 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 275;

                goto try_except_handler_8;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_38;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_8 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_8, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 275;

                goto try_except_handler_8;
            }
            tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_9;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_7 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 275;

                    goto try_except_handler_8;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_9 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_9 == NULL) );
                tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_9 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 275;

                    goto try_except_handler_8;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_7 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 275;

                    goto try_except_handler_8;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 275;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_8;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_39;
            tmp_assign_source_39 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_39;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_40;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_PIL$BmpImagePlugin_275 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_af7895b8e9a7643f2ab308f77e96c311;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_275, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_10;
        }
        tmp_dictset_value = const_str_plain_DibImageFile;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_275, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 275;

            goto try_except_handler_10;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_c789dea7fb93995d63627c2800a6c64d_3, codeobj_c789dea7fb93995d63627c2800a6c64d, module_PIL$BmpImagePlugin, sizeof(void *) );
        frame_c789dea7fb93995d63627c2800a6c64d_3 = cache_frame_c789dea7fb93995d63627c2800a6c64d_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_c789dea7fb93995d63627c2800a6c64d_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_c789dea7fb93995d63627c2800a6c64d_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = const_str_plain_DIB;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_275, const_str_plain_format, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = const_str_digest_7694c8aa5098fdbfc9d126eeda02356f;
        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_275, const_str_plain_format_description, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 278;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        tmp_dictset_value = MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_5__open(  );



        tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_275, const_str_plain__open, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 280;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_c789dea7fb93995d63627c2800a6c64d_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_c789dea7fb93995d63627c2800a6c64d_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_c789dea7fb93995d63627c2800a6c64d_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_c789dea7fb93995d63627c2800a6c64d_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_c789dea7fb93995d63627c2800a6c64d_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_c789dea7fb93995d63627c2800a6c64d_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_c789dea7fb93995d63627c2800a6c64d_3 == cache_frame_c789dea7fb93995d63627c2800a6c64d_3 )
        {
            Py_DECREF( frame_c789dea7fb93995d63627c2800a6c64d_3 );
        }
        cache_frame_c789dea7fb93995d63627c2800a6c64d_3 = NULL;

        assertFrameObject( frame_c789dea7fb93995d63627c2800a6c64d_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_10;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 275;

                goto try_except_handler_10;
            }
            tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_PIL$BmpImagePlugin_275, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 275;

                goto try_except_handler_10;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_41;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_4 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_8 = const_str_plain_DibImageFile;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_8 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_8 );
            tmp_tuple_element_8 = locals_PIL$BmpImagePlugin_275;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_8 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 275;
            tmp_assign_source_41 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_41 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 275;

                goto try_except_handler_10;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_41;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_40 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_40 );
        goto try_return_handler_10;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        Py_DECREF( locals_PIL$BmpImagePlugin_275 );
        locals_PIL$BmpImagePlugin_275 = NULL;
        goto try_return_handler_9;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_PIL$BmpImagePlugin_275 );
        locals_PIL$BmpImagePlugin_275 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto try_except_handler_9;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( PIL$BmpImagePlugin );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 275;
        goto try_except_handler_8;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_DibImageFile, tmp_assign_source_40 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    {
        PyObject *tmp_assign_source_42;
        tmp_assign_source_42 = PyDict_Copy( const_dict_6c9a4e8e23277b1848566a99b2c6e76d );
        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_SAVE, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        tmp_assign_source_43 = MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_6__dib_save(  );



        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain__dib_save, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_true_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_44 = MAKE_FUNCTION_PIL$BmpImagePlugin$$$function_7__save( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain__save, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_10;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_11;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_mvar_value_8;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 357;

            goto frame_exception_exit_1;
        }

        tmp_source_name_10 = tmp_mvar_value_5;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_register_open );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BmpImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 357;

            goto frame_exception_exit_1;
        }

        tmp_source_name_11 = tmp_mvar_value_6;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_format );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 357;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BmpImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 357;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_2 = tmp_mvar_value_7;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain__accept );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__accept );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_accept" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 357;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = tmp_mvar_value_8;
        frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 357;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_source_name_12;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_13;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_mvar_value_11;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 358;

            goto frame_exception_exit_1;
        }

        tmp_source_name_12 = tmp_mvar_value_9;
        tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_register_save );
        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BmpImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 358;

            goto frame_exception_exit_1;
        }

        tmp_source_name_13 = tmp_mvar_value_10;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_format );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 358;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain__save );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__save );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_element_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_save" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 358;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_5 = tmp_mvar_value_11;
        frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 358;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 358;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_7;
        PyObject *tmp_source_name_14;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_source_name_15;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 360;

            goto frame_exception_exit_1;
        }

        tmp_source_name_14 = tmp_mvar_value_12;
        tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_register_extension );
        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 360;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );
        }

        if ( tmp_mvar_value_13 == NULL )
        {
            Py_DECREF( tmp_called_name_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BmpImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 360;

            goto frame_exception_exit_1;
        }

        tmp_source_name_15 = tmp_mvar_value_13;
        tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_format );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_7 );

            exception_lineno = 360;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_7 = const_str_digest_ad55bc4c4352c79ec48d6f5e7a30f8a2;
        frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 360;
        {
            PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_called_name_7 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 360;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_name_8;
        PyObject *tmp_source_name_16;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_source_name_17;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_args_element_name_9;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 362;

            goto frame_exception_exit_1;
        }

        tmp_source_name_16 = tmp_mvar_value_14;
        tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_register_mime );
        if ( tmp_called_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 362;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BmpImageFile );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_called_name_8 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BmpImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 362;

            goto frame_exception_exit_1;
        }

        tmp_source_name_17 = tmp_mvar_value_15;
        tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_format );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_8 );

            exception_lineno = 362;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_9 = const_str_digest_b29042b8bf76639bd00bc6af12b19b3b;
        frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 362;
        {
            PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_called_name_8 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 362;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_called_name_9;
        PyObject *tmp_source_name_18;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_source_name_19;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_mvar_value_19;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 364;

            goto frame_exception_exit_1;
        }

        tmp_source_name_18 = tmp_mvar_value_16;
        tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_register_open );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 364;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_DibImageFile );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DibImageFile );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_called_name_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DibImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 364;

            goto frame_exception_exit_1;
        }

        tmp_source_name_19 = tmp_mvar_value_17;
        tmp_args_element_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_format );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );

            exception_lineno = 364;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_DibImageFile );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DibImageFile );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_element_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DibImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 364;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_11 = tmp_mvar_value_18;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain__dib_accept );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__dib_accept );
        }

        if ( tmp_mvar_value_19 == NULL )
        {
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_element_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_dib_accept" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 364;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_12 = tmp_mvar_value_19;
        frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 364;
        {
            PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11, tmp_args_element_name_12 };
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 364;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        PyObject *tmp_called_name_10;
        PyObject *tmp_source_name_20;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_source_name_21;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_mvar_value_22;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 365;

            goto frame_exception_exit_1;
        }

        tmp_source_name_20 = tmp_mvar_value_20;
        tmp_called_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_register_save );
        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 365;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_DibImageFile );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DibImageFile );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_called_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DibImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 365;

            goto frame_exception_exit_1;
        }

        tmp_source_name_21 = tmp_mvar_value_21;
        tmp_args_element_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_format );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 365;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain__dib_save );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__dib_save );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_args_element_name_13 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_dib_save" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 365;

            goto frame_exception_exit_1;
        }

        tmp_args_element_name_14 = tmp_mvar_value_22;
        frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 365;
        {
            PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_14 };
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_called_name_10 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 365;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    {
        PyObject *tmp_called_name_11;
        PyObject *tmp_source_name_22;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_source_name_23;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_args_element_name_16;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_23 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 367;

            goto frame_exception_exit_1;
        }

        tmp_source_name_22 = tmp_mvar_value_23;
        tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_register_extension );
        if ( tmp_called_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 367;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_DibImageFile );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DibImageFile );
        }

        if ( tmp_mvar_value_24 == NULL )
        {
            Py_DECREF( tmp_called_name_11 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DibImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 367;

            goto frame_exception_exit_1;
        }

        tmp_source_name_23 = tmp_mvar_value_24;
        tmp_args_element_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_format );
        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_11 );

            exception_lineno = 367;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_16 = const_str_digest_2faeda2d7dc02f33bf8119f46b6cd1e8;
        frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 367;
        {
            PyObject *call_args[] = { tmp_args_element_name_15, tmp_args_element_name_16 };
            tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_called_name_11 );
        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 367;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    {
        PyObject *tmp_called_name_12;
        PyObject *tmp_source_name_24;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_call_result_8;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_source_name_25;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_args_element_name_18;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_Image );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Image );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Image" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 369;

            goto frame_exception_exit_1;
        }

        tmp_source_name_24 = tmp_mvar_value_25;
        tmp_called_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_register_mime );
        if ( tmp_called_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_PIL$BmpImagePlugin, (Nuitka_StringObject *)const_str_plain_DibImageFile );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_DibImageFile );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_called_name_12 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "DibImageFile" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 369;

            goto frame_exception_exit_1;
        }

        tmp_source_name_25 = tmp_mvar_value_26;
        tmp_args_element_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_format );
        if ( tmp_args_element_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_12 );

            exception_lineno = 369;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_18 = const_str_digest_b29042b8bf76639bd00bc6af12b19b3b;
        frame_9a431d03a6469f0f6984fda36b405772->m_frame.f_lineno = 369;
        {
            PyObject *call_args[] = { tmp_args_element_name_17, tmp_args_element_name_18 };
            tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_12, call_args );
        }

        Py_DECREF( tmp_called_name_12 );
        Py_DECREF( tmp_args_element_name_17 );
        if ( tmp_call_result_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;

            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_8 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a431d03a6469f0f6984fda36b405772 );
#endif
    popFrameStack();

    assertFrameObject( frame_9a431d03a6469f0f6984fda36b405772 );

    goto frame_no_exception_3;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a431d03a6469f0f6984fda36b405772 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9a431d03a6469f0f6984fda36b405772, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9a431d03a6469f0f6984fda36b405772->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9a431d03a6469f0f6984fda36b405772, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_3:;

    return MOD_RETURN_VALUE( module_PIL$BmpImagePlugin );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
