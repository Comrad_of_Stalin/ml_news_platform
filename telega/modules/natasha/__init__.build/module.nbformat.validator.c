/* Generated code for Python module 'nbformat.validator'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_nbformat$validator" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_nbformat$validator;
PyDictObject *moduledict_nbformat$validator;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_ab4d7d63411881c04058efe70cf6bfd1;
extern PyObject *const_str_angle_dictcontraction;
extern PyObject *const_str_plain_warn;
static PyObject *const_str_digest_84a0fe848149a514cbfb0fdd8d1fa2ed;
extern PyObject *const_str_plain_validators;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_index;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_dict;
static PyObject *const_str_digest_8cdf6e7379d2e8580522d2a3982783b7;
extern PyObject *const_str_plain_Draft4Validator;
extern PyObject *const_str_plain___name__;
extern PyObject *const_tuple_str_plain_self_str_plain_key_tuple;
extern PyObject *const_str_plain_UserWarning;
static PyObject *const_tuple_str_plain_self_str_plain_error_str_plain_instance_tuple;
extern PyObject *const_tuple_str_plain___class___tuple;
static PyObject *const_str_plain__STR_LIMIT;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_i;
extern PyObject *const_tuple_str_plain_ValidationError_tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_get_version;
extern PyObject *const_str_plain_oneOf;
static PyObject *const_str_digest_3db58559bb6e158db1119a1cd969836d;
static PyObject *const_str_plain__cell;
static PyObject *const_str_digest_e2fe51ca39323cfa3982965f7518090b;
extern PyObject *const_str_plain___exit__;
extern PyObject *const_str_plain_notebook;
extern PyObject *const_str_plain_jsonschema;
extern PyObject *const_tuple_str_plain_schema_tuple;
extern PyObject *const_str_plain_json;
static PyObject *const_str_digest_7c8b18874751383d58d8e1d740744a9d;
static PyObject *const_str_digest_f5960f053c799842b8e0ed714ea133f9;
extern PyObject *const_str_plain_items;
extern PyObject *const_int_neg_1;
static PyObject *const_str_digest_4480aceac7f9a1ea050b506f25fc3746;
static PyObject *const_dict_51f76991ef7771af136a0c949efbabd5;
extern PyObject *const_str_plain_reader;
extern PyObject *const_str_plain_instance;
static PyObject *const_str_plain_verbose_msg;
extern PyObject *const_str_plain_os;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_schema;
extern PyObject *const_str_plain_nbjson;
static PyObject *const_str_plain_better_validation_error;
static PyObject *const_str_digest_26e92d503609cc5c01254f14e323f90f;
static PyObject *const_str_digest_05e6cbb831921041a48eb20404075cc5;
extern PyObject *const_str_plain___enter__;
extern PyObject *const_str_plain_validate;
extern PyObject *const_str_digest_0674107b2d3e86aecfffb00b3395ae4a;
extern PyObject *const_str_plain_validator;
static PyObject *const_str_digest_b92312044731484797159e6bd08ff1bf;
extern PyObject *const_str_digest_99b2b9518bc27b61b80729fcb0aefd4f;
static PyObject *const_str_plain_truncated;
static PyObject *const_str_digest_e5a1c615b7fb0920230aea115440c8ab;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_digest_b1de2f4e7d17f11eaeb3062e52aa9890;
static PyObject *const_str_digest_632f3d17d2a1ae4c930aa73e179bc065;
static PyObject *const_tuple_str_plain_current_nbformat_tuple;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_int_pos_16;
extern PyObject *const_str_plain___doc__;
static PyObject *const_tuple_09b27332d76da71db55f73794866148e_tuple;
static PyObject *const_str_plain__truncate_obj;
extern PyObject *const_str_plain_nbformat_schema;
extern PyObject *const_str_plain_extend;
static PyObject *const_tuple_str_plain_obj_str_plain_key_str_plain_value_str_plain_i_tuple;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_relative_schema_path;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_relative_path;
extern PyObject *const_str_plain_warnings;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_plain___qualname__;
static PyObject *const_str_digest_3ba7abdd6423f8828eb25e55f9612f02;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_path;
static PyObject *const_tuple_str_plain_Draft4Validator_tuple;
static PyObject *const_str_digest_b8d5ba12f59ff311c96a191521fc9587;
extern PyObject *const_str_plain_item;
extern PyObject *const_str_plain_get_validator;
extern PyObject *const_str_plain_value;
static PyObject *const_str_plain_current_minor;
static PyObject *const_dict_4327091cb4fc3295df2b2776d735ca47;
extern PyObject *const_str_digest_c184ec84b5d2106ce1dac869eaf47241;
static PyObject *const_str_digest_bf957fafa2570246718e247af8a204fc;
static PyObject *const_str_digest_b815ad5d8695ec6d8f3e05a850258a3f;
static PyObject *const_str_plain_schema_json;
extern PyObject *const_str_plain_outputs;
static PyObject *const_str_plain__allow_undefined;
extern PyObject *const_str_plain_pprint;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_plain_message;
static PyObject *const_tuple_590c194f53cb95dc33490133d37eb38e_tuple;
extern PyObject *const_str_plain_load;
static PyObject *const_str_digest_46cae9a15e0fb3f2d59b6d514eaa1119;
static PyObject *const_str_digest_313dd3cbc4af57bc5f16ba1a71ffd70b;
extern PyObject *const_str_plain_indices;
extern PyObject *const_str_digest_3501979af1b70861f5e9d6a0f04129bf;
static PyObject *const_str_digest_6add32c35304927ffbff5a4594fe611a;
extern PyObject *const_tuple_empty;
static PyObject *const_str_plain_isvalid;
extern PyObject *const_tuple_str_plain_item_tuple;
extern PyObject *const_str_plain___getattr__;
extern PyObject *const_tuple_str_plain_get_version_tuple;
static PyObject *const_tuple_6988cdc77fca81f6e92f8d67fa7493bd_tuple;
extern PyObject *const_str_plain_enumerate;
extern PyObject *const_tuple_str_plain_cells_tuple;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_plain_endswith;
extern PyObject *const_str_plain_nbformat_minor;
extern PyObject *const_tuple_str_plain_outputs_tuple;
extern PyObject *const_tuple_str_plain_indices_tuple;
extern PyObject *const_tuple_str_plain_k_str_plain_v_tuple;
extern PyObject *const_str_plain_error;
static PyObject *const_str_plain_better;
extern PyObject *const_str_plain_schema_path;
extern PyObject *const_str_plain_False;
static PyObject *const_str_plain_Of;
extern PyObject *const_str_plain_output;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_k;
extern PyObject *const_tuple_int_pos_3_tuple;
extern PyObject *const_str_plain_list;
extern PyObject *const_str_plain_f;
extern PyObject *const_str_plain_version_minor;
static PyObject *const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple;
static PyObject *const_str_plain__relax_additional_properties;
static PyObject *const_str_digest_acb2a3ac556b7ea554f9b7aef19c0e4d;
extern PyObject *const_int_0;
extern PyObject *const_tuple_str_plain_import_item_tuple;
extern PyObject *const_str_plain_additionalProperties;
extern PyObject *const_int_pos_78;
static PyObject *const_str_digest_1ce647b6b40d3803cab42709ed4c2933;
extern PyObject *const_tuple_none_none_none_false_tuple;
extern PyObject *const_str_plain_cell_type;
extern PyObject *const_str_plain_ref;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_output_type;
static PyObject *const_str_digest_d58d968678b6839a6825319f681d2032;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_cell;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_int_pos_64;
extern PyObject *const_str_plain_import_item;
extern PyObject *const_str_plain_pformat;
static PyObject *const_str_digest_ee4fb7f34081bbea5fd2f72e8362a336;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_index_tuple;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_dirname;
static PyObject *const_dict_892d7172c91c0d0d5faa082d734d9593;
extern PyObject *const_str_plain___unicode__;
extern PyObject *const_str_plain_cells;
extern PyObject *const_str_plain_ValidationError;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_v;
extern PyObject *const_str_plain_print_function;
extern PyObject *const_str_plain___class__;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_current_nbformat;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain___str__;
extern PyObject *const_str_plain_sys;
static PyObject *const_tuple_str_plain_obj_str_plain_truncated_tuple;
extern PyObject *const_str_plain_definitions;
extern PyObject *const_slice_none_int_neg_1_none;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_64b1f97bf44235012b982cf7b16efef1;
static PyObject *const_tuple_str_plain_Of_tuple;
extern PyObject *const_str_plain_width;
extern PyObject *const_str_plain_key;
extern PyObject *const_str_newline;
static PyObject *const_str_plain_version_tuple;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_str_plain__get_schema_json;
static PyObject *const_str_digest_4f8b32c6b411603474e36b16db170008;
extern PyObject *const_str_plain___init__;
static PyObject *const_str_plain__format_as_index;
static PyObject *const_str_digest_e5801a88f04942ec8c4ba6c020e52806;
extern PyObject *const_int_pos_3;
static PyObject *const_str_plain_sub_error;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_NotebookValidationError;
extern PyObject *const_str_plain_version_info;
extern PyObject *const_str_plain_Validator;
extern PyObject *const_str_plain_original;
extern PyObject *const_tuple_none_none_false_tuple;
extern PyObject *const_str_plain_get;
static PyObject *const_str_digest_735a36f536d01aaba1eff83af28ab513;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_version;
extern PyObject *const_int_pos_2;
static PyObject *const_tuple_str_plain_self_str_plain_original_str_plain_ref_tuple;
static PyObject *const_tuple_a6c385678d3829d70aefece1d065d334_tuple;
extern PyObject *const_str_plain_obj;
extern PyObject *const_str_empty;
static PyObject *const_str_digest_4231fff64c06f120b877fa923b99296c;
extern PyObject *const_str_plain_relax_add_props;
static PyObject *const_str_plain__ITEM_LIMIT;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_ab4d7d63411881c04058efe70cf6bfd1 = UNSTREAM_STRING_ASCII( &constant_bin[ 2807670 ], 14, 0 );
    const_str_digest_84a0fe848149a514cbfb0fdd8d1fa2ed = UNSTREAM_STRING_ASCII( &constant_bin[ 2807684 ], 148, 0 );
    const_str_digest_8cdf6e7379d2e8580522d2a3982783b7 = UNSTREAM_STRING_ASCII( &constant_bin[ 2807832 ], 17, 0 );
    const_tuple_str_plain_self_str_plain_error_str_plain_instance_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_error_str_plain_instance_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_error_str_plain_instance_tuple, 1, const_str_plain_error ); Py_INCREF( const_str_plain_error );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_error_str_plain_instance_tuple, 2, const_str_plain_instance ); Py_INCREF( const_str_plain_instance );
    const_str_plain__STR_LIMIT = UNSTREAM_STRING_ASCII( &constant_bin[ 2807849 ], 10, 1 );
    const_str_digest_3db58559bb6e158db1119a1cd969836d = UNSTREAM_STRING_ASCII( &constant_bin[ 2807859 ], 99, 0 );
    const_str_plain__cell = UNSTREAM_STRING_ASCII( &constant_bin[ 113977 ], 5, 1 );
    const_str_digest_e2fe51ca39323cfa3982965f7518090b = UNSTREAM_STRING_ASCII( &constant_bin[ 2807958 ], 35, 0 );
    const_str_digest_7c8b18874751383d58d8e1d740744a9d = UNSTREAM_STRING_ASCII( &constant_bin[ 1159925 ], 29, 0 );
    const_str_digest_f5960f053c799842b8e0ed714ea133f9 = UNSTREAM_STRING_ASCII( &constant_bin[ 2807993 ], 37, 0 );
    const_str_digest_4480aceac7f9a1ea050b506f25fc3746 = UNSTREAM_STRING_ASCII( &constant_bin[ 2808030 ], 32, 0 );
    const_dict_51f76991ef7771af136a0c949efbabd5 = _PyDict_NewPresized( 1 );
    const_str_digest_632f3d17d2a1ae4c930aa73e179bc065 = UNSTREAM_STRING_ASCII( &constant_bin[ 2808062 ], 31, 0 );
    PyDict_SetItem( const_dict_51f76991ef7771af136a0c949efbabd5, const_str_digest_99b2b9518bc27b61b80729fcb0aefd4f, const_str_digest_632f3d17d2a1ae4c930aa73e179bc065 );
    assert( PyDict_Size( const_dict_51f76991ef7771af136a0c949efbabd5 ) == 1 );
    const_str_plain_verbose_msg = UNSTREAM_STRING_ASCII( &constant_bin[ 2808093 ], 11, 1 );
    const_str_plain_better_validation_error = UNSTREAM_STRING_ASCII( &constant_bin[ 2808104 ], 23, 1 );
    const_str_digest_26e92d503609cc5c01254f14e323f90f = UNSTREAM_STRING_ASCII( &constant_bin[ 2808127 ], 218, 0 );
    const_str_digest_05e6cbb831921041a48eb20404075cc5 = UNSTREAM_STRING_ASCII( &constant_bin[ 2808345 ], 35, 0 );
    const_str_digest_b92312044731484797159e6bd08ff1bf = UNSTREAM_STRING_ASCII( &constant_bin[ 2808380 ], 98, 0 );
    const_str_plain_truncated = UNSTREAM_STRING_ASCII( &constant_bin[ 62811 ], 9, 1 );
    const_str_digest_e5a1c615b7fb0920230aea115440c8ab = UNSTREAM_STRING_ASCII( &constant_bin[ 2808478 ], 12, 0 );
    const_tuple_str_plain_current_nbformat_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_current_nbformat_tuple, 0, const_str_plain_current_nbformat ); Py_INCREF( const_str_plain_current_nbformat );
    const_tuple_09b27332d76da71db55f73794866148e_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_09b27332d76da71db55f73794866148e_tuple, 0, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    PyTuple_SET_ITEM( const_tuple_09b27332d76da71db55f73794866148e_tuple, 1, const_str_plain_schema_path ); Py_INCREF( const_str_plain_schema_path );
    PyTuple_SET_ITEM( const_tuple_09b27332d76da71db55f73794866148e_tuple, 2, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    const_str_plain_schema_json = UNSTREAM_STRING_ASCII( &constant_bin[ 2808490 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_09b27332d76da71db55f73794866148e_tuple, 3, const_str_plain_schema_json ); Py_INCREF( const_str_plain_schema_json );
    const_str_plain__truncate_obj = UNSTREAM_STRING_ASCII( &constant_bin[ 2808501 ], 13, 1 );
    const_tuple_str_plain_obj_str_plain_key_str_plain_value_str_plain_i_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_obj_str_plain_key_str_plain_value_str_plain_i_tuple, 0, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_str_plain_obj_str_plain_key_str_plain_value_str_plain_i_tuple, 1, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_str_plain_obj_str_plain_key_str_plain_value_str_plain_i_tuple, 2, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_str_plain_obj_str_plain_key_str_plain_value_str_plain_i_tuple, 3, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    const_str_digest_3ba7abdd6423f8828eb25e55f9612f02 = UNSTREAM_STRING_ASCII( &constant_bin[ 2808514 ], 16, 0 );
    const_tuple_str_plain_Draft4Validator_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Draft4Validator_tuple, 0, const_str_plain_Draft4Validator ); Py_INCREF( const_str_plain_Draft4Validator );
    const_str_digest_b8d5ba12f59ff311c96a191521fc9587 = UNSTREAM_STRING_ASCII( &constant_bin[ 2808530 ], 24, 0 );
    const_str_plain_current_minor = UNSTREAM_STRING_ASCII( &constant_bin[ 2808554 ], 13, 1 );
    const_dict_4327091cb4fc3295df2b2776d735ca47 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_4327091cb4fc3295df2b2776d735ca47, const_str_plain_width, const_int_pos_78 );
    assert( PyDict_Size( const_dict_4327091cb4fc3295df2b2776d735ca47 ) == 1 );
    const_str_digest_bf957fafa2570246718e247af8a204fc = UNSTREAM_STRING_ASCII( &constant_bin[ 2808567 ], 14, 0 );
    const_str_digest_b815ad5d8695ec6d8f3e05a850258a3f = UNSTREAM_STRING_ASCII( &constant_bin[ 2808581 ], 27, 0 );
    const_str_plain__allow_undefined = UNSTREAM_STRING_ASCII( &constant_bin[ 2808608 ], 16, 1 );
    const_tuple_590c194f53cb95dc33490133d37eb38e_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_590c194f53cb95dc33490133d37eb38e_tuple, 0, const_str_plain_nbjson ); Py_INCREF( const_str_plain_nbjson );
    PyTuple_SET_ITEM( const_tuple_590c194f53cb95dc33490133d37eb38e_tuple, 1, const_str_plain_ref ); Py_INCREF( const_str_plain_ref );
    PyTuple_SET_ITEM( const_tuple_590c194f53cb95dc33490133d37eb38e_tuple, 2, const_str_plain_version ); Py_INCREF( const_str_plain_version );
    PyTuple_SET_ITEM( const_tuple_590c194f53cb95dc33490133d37eb38e_tuple, 3, const_str_plain_version_minor ); Py_INCREF( const_str_plain_version_minor );
    PyTuple_SET_ITEM( const_tuple_590c194f53cb95dc33490133d37eb38e_tuple, 4, const_str_plain_relax_add_props ); Py_INCREF( const_str_plain_relax_add_props );
    PyTuple_SET_ITEM( const_tuple_590c194f53cb95dc33490133d37eb38e_tuple, 5, const_str_plain_get_version ); Py_INCREF( const_str_plain_get_version );
    PyTuple_SET_ITEM( const_tuple_590c194f53cb95dc33490133d37eb38e_tuple, 6, const_str_plain_validator ); Py_INCREF( const_str_plain_validator );
    PyTuple_SET_ITEM( const_tuple_590c194f53cb95dc33490133d37eb38e_tuple, 7, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    const_str_digest_46cae9a15e0fb3f2d59b6d514eaa1119 = UNSTREAM_STRING_ASCII( &constant_bin[ 2808624 ], 38, 0 );
    const_str_digest_313dd3cbc4af57bc5f16ba1a71ffd70b = UNSTREAM_STRING_ASCII( &constant_bin[ 2808662 ], 16, 0 );
    const_str_digest_6add32c35304927ffbff5a4594fe611a = UNSTREAM_STRING_ASCII( &constant_bin[ 2808678 ], 206, 0 );
    const_str_plain_isvalid = UNSTREAM_STRING_ASCII( &constant_bin[ 2808884 ], 7, 1 );
    const_tuple_6988cdc77fca81f6e92f8d67fa7493bd_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_6988cdc77fca81f6e92f8d67fa7493bd_tuple, 0, const_str_plain_nbjson ); Py_INCREF( const_str_plain_nbjson );
    PyTuple_SET_ITEM( const_tuple_6988cdc77fca81f6e92f8d67fa7493bd_tuple, 1, const_str_plain_ref ); Py_INCREF( const_str_plain_ref );
    PyTuple_SET_ITEM( const_tuple_6988cdc77fca81f6e92f8d67fa7493bd_tuple, 2, const_str_plain_version ); Py_INCREF( const_str_plain_version );
    PyTuple_SET_ITEM( const_tuple_6988cdc77fca81f6e92f8d67fa7493bd_tuple, 3, const_str_plain_version_minor ); Py_INCREF( const_str_plain_version_minor );
    const_str_plain_better = UNSTREAM_STRING_ASCII( &constant_bin[ 904685 ], 6, 1 );
    const_str_plain_Of = UNSTREAM_STRING_ASCII( &constant_bin[ 68828 ], 2, 1 );
    const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple, 0, const_str_plain_version ); Py_INCREF( const_str_plain_version );
    PyTuple_SET_ITEM( const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple, 1, const_str_plain_version_minor ); Py_INCREF( const_str_plain_version_minor );
    PyTuple_SET_ITEM( const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple, 2, const_str_plain_relax_add_props ); Py_INCREF( const_str_plain_relax_add_props );
    PyTuple_SET_ITEM( const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple, 3, const_str_plain_current_nbformat ); Py_INCREF( const_str_plain_current_nbformat );
    PyTuple_SET_ITEM( const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple, 4, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    PyTuple_SET_ITEM( const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple, 5, const_str_plain_current_minor ); Py_INCREF( const_str_plain_current_minor );
    const_str_plain_version_tuple = UNSTREAM_STRING_ASCII( &constant_bin[ 2808891 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple, 6, const_str_plain_version_tuple ); Py_INCREF( const_str_plain_version_tuple );
    PyTuple_SET_ITEM( const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple, 7, const_str_plain_schema_json ); Py_INCREF( const_str_plain_schema_json );
    const_str_plain__relax_additional_properties = UNSTREAM_STRING_ASCII( &constant_bin[ 2808904 ], 28, 1 );
    const_str_digest_acb2a3ac556b7ea554f9b7aef19c0e4d = UNSTREAM_STRING_ASCII( &constant_bin[ 2808589 ], 18, 0 );
    const_str_digest_1ce647b6b40d3803cab42709ed4c2933 = UNSTREAM_STRING_ASCII( &constant_bin[ 2808932 ], 32, 0 );
    const_str_digest_d58d968678b6839a6825319f681d2032 = UNSTREAM_STRING_ASCII( &constant_bin[ 2808964 ], 137, 0 );
    const_str_digest_ee4fb7f34081bbea5fd2f72e8362a336 = UNSTREAM_STRING_ASCII( &constant_bin[ 2809101 ], 260, 0 );
    const_dict_892d7172c91c0d0d5faa082d734d9593 = _PyDict_NewPresized( 1 );
    const_str_digest_735a36f536d01aaba1eff83af28ab513 = UNSTREAM_STRING_ASCII( &constant_bin[ 2809361 ], 33, 0 );
    PyDict_SetItem( const_dict_892d7172c91c0d0d5faa082d734d9593, const_str_digest_99b2b9518bc27b61b80729fcb0aefd4f, const_str_digest_735a36f536d01aaba1eff83af28ab513 );
    assert( PyDict_Size( const_dict_892d7172c91c0d0d5faa082d734d9593 ) == 1 );
    const_tuple_str_plain_obj_str_plain_truncated_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_obj_str_plain_truncated_tuple, 0, const_str_plain_obj ); Py_INCREF( const_str_plain_obj );
    PyTuple_SET_ITEM( const_tuple_str_plain_obj_str_plain_truncated_tuple, 1, const_str_plain_truncated ); Py_INCREF( const_str_plain_truncated );
    const_str_digest_64b1f97bf44235012b982cf7b16efef1 = UNSTREAM_STRING_ASCII( &constant_bin[ 2809394 ], 80, 0 );
    const_tuple_str_plain_Of_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Of_tuple, 0, const_str_plain_Of ); Py_INCREF( const_str_plain_Of );
    const_str_plain__get_schema_json = UNSTREAM_STRING_ASCII( &constant_bin[ 2809474 ], 16, 1 );
    const_str_digest_4f8b32c6b411603474e36b16db170008 = UNSTREAM_STRING_ASCII( &constant_bin[ 2809490 ], 134, 0 );
    const_str_plain__format_as_index = UNSTREAM_STRING_ASCII( &constant_bin[ 2809624 ], 16, 1 );
    const_str_digest_e5801a88f04942ec8c4ba6c020e52806 = UNSTREAM_STRING_ASCII( &constant_bin[ 2809640 ], 21, 0 );
    const_str_plain_sub_error = UNSTREAM_STRING_ASCII( &constant_bin[ 2809661 ], 9, 1 );
    const_str_plain_NotebookValidationError = UNSTREAM_STRING_ASCII( &constant_bin[ 2807958 ], 23, 1 );
    const_tuple_str_plain_self_str_plain_original_str_plain_ref_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_original_str_plain_ref_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_original_str_plain_ref_tuple, 1, const_str_plain_original ); Py_INCREF( const_str_plain_original );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_original_str_plain_ref_tuple, 2, const_str_plain_ref ); Py_INCREF( const_str_plain_ref );
    const_tuple_a6c385678d3829d70aefece1d065d334_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_a6c385678d3829d70aefece1d065d334_tuple, 0, const_str_plain_error ); Py_INCREF( const_str_plain_error );
    PyTuple_SET_ITEM( const_tuple_a6c385678d3829d70aefece1d065d334_tuple, 1, const_str_plain_version ); Py_INCREF( const_str_plain_version );
    PyTuple_SET_ITEM( const_tuple_a6c385678d3829d70aefece1d065d334_tuple, 2, const_str_plain_version_minor ); Py_INCREF( const_str_plain_version_minor );
    PyTuple_SET_ITEM( const_tuple_a6c385678d3829d70aefece1d065d334_tuple, 3, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    PyTuple_SET_ITEM( const_tuple_a6c385678d3829d70aefece1d065d334_tuple, 4, const_str_plain_ref ); Py_INCREF( const_str_plain_ref );
    PyTuple_SET_ITEM( const_tuple_a6c385678d3829d70aefece1d065d334_tuple, 5, const_str_plain_sub_error ); Py_INCREF( const_str_plain_sub_error );
    PyTuple_SET_ITEM( const_tuple_a6c385678d3829d70aefece1d065d334_tuple, 6, const_str_plain_better ); Py_INCREF( const_str_plain_better );
    const_str_digest_4231fff64c06f120b877fa923b99296c = UNSTREAM_STRING_ASCII( &constant_bin[ 2809670 ], 35, 0 );
    const_str_plain__ITEM_LIMIT = UNSTREAM_STRING_ASCII( &constant_bin[ 2809705 ], 11, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_nbformat$validator( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_0f4073f2f3ee8f4812497a8bc5624d58;
static PyCodeObject *codeobj_5de0013cd5729bd6a157c35ec96909a3;
static PyCodeObject *codeobj_07d770b8e9333a18782916368eaaa45b;
static PyCodeObject *codeobj_6a814c4d623439bf68cd6e1ac172ae65;
static PyCodeObject *codeobj_f9b61aaf579eeba5ba64e674c2300a71;
static PyCodeObject *codeobj_97501fbef0568d5fd31c3093b607e4be;
static PyCodeObject *codeobj_86ac625832a0fbc6a73ec6019fb2cd70;
static PyCodeObject *codeobj_7cd870927385df22f14f39c4776aa107;
static PyCodeObject *codeobj_9938aee43aa0f3502e63636a194a4acb;
static PyCodeObject *codeobj_5f3d662285e0b4833cd1e6ebec7cd9ce;
static PyCodeObject *codeobj_575fedaf3fe165e75d8b92029ab5ae29;
static PyCodeObject *codeobj_d4a68d445da39bc494ea5548053ffb3c;
static PyCodeObject *codeobj_50599b5737e0048448bdec7289af66ba;
static PyCodeObject *codeobj_2ffdb114606679c9c2c41f9996831a71;
static PyCodeObject *codeobj_ae0e53b0d21be0bf143aa79b66326c88;
static PyCodeObject *codeobj_a4ace2a051dff874cfe8b840f1be8cab;
static PyCodeObject *codeobj_80acff7fb0ecbb9c3057078e8ba1256b;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_e5801a88f04942ec8c4ba6c020e52806 );
    codeobj_0f4073f2f3ee8f4812497a8bc5624d58 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_dictcontraction, 140, const_tuple_str_plain_k_str_plain_v_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5de0013cd5729bd6a157c35ec96909a3 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 129, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_index_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_07d770b8e9333a18782916368eaaa45b = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 150, const_tuple_str_plain_item_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6a814c4d623439bf68cd6e1ac172ae65 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_b815ad5d8695ec6d8f3e05a850258a3f, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_f9b61aaf579eeba5ba64e674c2300a71 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_NotebookValidationError, 162, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_97501fbef0568d5fd31c3093b607e4be = MAKE_CODEOBJ( module_filename_obj, const_str_plain___getattr__, 172, const_tuple_str_plain_self_str_plain_key_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_86ac625832a0fbc6a73ec6019fb2cd70 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 167, const_tuple_str_plain_self_str_plain_original_str_plain_ref_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7cd870927385df22f14f39c4776aa107 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___unicode__, 175, const_tuple_str_plain_self_str_plain_error_str_plain_instance_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9938aee43aa0f3502e63636a194a4acb = MAKE_CODEOBJ( module_filename_obj, const_str_plain__allow_undefined, 44, const_tuple_str_plain_schema_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5f3d662285e0b4833cd1e6ebec7cd9ce = MAKE_CODEOBJ( module_filename_obj, const_str_plain__format_as_index, 118, const_tuple_str_plain_indices_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_575fedaf3fe165e75d8b92029ab5ae29 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__get_schema_json, 94, const_tuple_09b27332d76da71db55f73794866148e_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d4a68d445da39bc494ea5548053ffb3c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__relax_additional_properties, 30, const_tuple_str_plain_obj_str_plain_key_str_plain_value_str_plain_i_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_50599b5737e0048448bdec7289af66ba = MAKE_CODEOBJ( module_filename_obj, const_str_plain__truncate_obj, 134, const_tuple_str_plain_obj_str_plain_truncated_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2ffdb114606679c9c2c41f9996831a71 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_better_validation_error, 198, const_tuple_a6c385678d3829d70aefece1d065d334_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ae0e53b0d21be0bf143aa79b66326c88 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_validator, 53, const_tuple_1dca41feedf22f9adccfb62455db7e9c_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a4ace2a051dff874cfe8b840f1be8cab = MAKE_CODEOBJ( module_filename_obj, const_str_plain_isvalid, 103, const_tuple_6988cdc77fca81f6e92f8d67fa7493bd_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_80acff7fb0ecbb9c3057078e8ba1256b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_validate, 237, const_tuple_590c194f53cb95dc33490133d37eb38e_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *nbformat$validator$$$function_6__format_as_index$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_10___unicode__(  );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_11_better_validation_error(  );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_12_validate( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_1__relax_additional_properties(  );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_2__allow_undefined(  );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_3_get_validator( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_4__get_schema_json(  );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_5_isvalid( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_6__format_as_index(  );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_7__truncate_obj(  );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_8___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_9___getattr__(  );


// The module function definitions.
static PyObject *impl_nbformat$validator$$$function_1__relax_additional_properties( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_obj = python_pars[ 0 ];
    PyObject *var_key = NULL;
    PyObject *var_value = NULL;
    PyObject *var_i = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    struct Nuitka_FrameObject *frame_d4a68d445da39bc494ea5548053ffb3c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_d4a68d445da39bc494ea5548053ffb3c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d4a68d445da39bc494ea5548053ffb3c, codeobj_d4a68d445da39bc494ea5548053ffb3c, module_nbformat$validator, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d4a68d445da39bc494ea5548053ffb3c = cache_frame_d4a68d445da39bc494ea5548053ffb3c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d4a68d445da39bc494ea5548053ffb3c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d4a68d445da39bc494ea5548053ffb3c ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_obj );
        tmp_isinstance_inst_1 = par_obj;
        tmp_isinstance_cls_1 = (PyObject *)&PyDict_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_obj );
            tmp_called_instance_1 = par_obj;
            frame_d4a68d445da39bc494ea5548053ffb3c->m_frame.f_lineno = 33;
            tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 33;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 33;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_1;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_2;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oooo";
                    exception_lineno = 33;
                    goto try_except_handler_2;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_2;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_iter_arg_2;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_iter_arg_2 = tmp_for_loop_1__iter_value;
            tmp_assign_source_3 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 33;
                type_description_1 = "oooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = tmp_tuple_unpack_1__source_iter;
                tmp_tuple_unpack_1__source_iter = tmp_assign_source_3;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_unpack_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooo";
                exception_lineno = 33;
                goto try_except_handler_4;
            }
            {
                PyObject *old = tmp_tuple_unpack_1__element_1;
                tmp_tuple_unpack_1__element_1 = tmp_assign_source_4;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_unpack_2;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_5 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
            if ( tmp_assign_source_5 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooo";
                exception_lineno = 33;
                goto try_except_handler_4;
            }
            {
                PyObject *old = tmp_tuple_unpack_1__element_2;
                tmp_tuple_unpack_1__element_2 = tmp_assign_source_5;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_iterator_name_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "oooo";
                        exception_lineno = 33;
                        goto try_except_handler_4;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooo";
                exception_lineno = 33;
                goto try_except_handler_4;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_3;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_2;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        {
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
            tmp_assign_source_6 = tmp_tuple_unpack_1__element_1;
            {
                PyObject *old = var_key;
                var_key = tmp_assign_source_6;
                Py_INCREF( var_key );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
            tmp_assign_source_7 = tmp_tuple_unpack_1__element_2;
            {
                PyObject *old = var_value;
                var_value = tmp_assign_source_7;
                Py_INCREF( var_value );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( var_key );
            tmp_compexpr_left_1 = var_key;
            tmp_compexpr_right_1 = const_str_plain_additionalProperties;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_8;
                tmp_assign_source_8 = Py_True;
                {
                    PyObject *old = var_value;
                    assert( old != NULL );
                    var_value = tmp_assign_source_8;
                    Py_INCREF( var_value );
                    Py_DECREF( old );
                }

            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_args_element_name_1;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__relax_additional_properties );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__relax_additional_properties );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_relax_additional_properties" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 37;
                    type_description_1 = "oooo";
                    goto try_except_handler_2;
                }

                tmp_called_name_1 = tmp_mvar_value_1;
                CHECK_OBJECT( var_value );
                tmp_args_element_name_1 = var_value;
                frame_d4a68d445da39bc494ea5548053ffb3c->m_frame.f_lineno = 37;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1 };
                    tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 37;
                    type_description_1 = "oooo";
                    goto try_except_handler_2;
                }
                {
                    PyObject *old = var_value;
                    assert( old != NULL );
                    var_value = tmp_assign_source_9;
                    Py_DECREF( old );
                }

            }
            branch_end_2:;
        }
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( var_value );
            tmp_ass_subvalue_1 = var_value;
            CHECK_OBJECT( par_obj );
            tmp_ass_subscribed_1 = par_obj;
            CHECK_OBJECT( var_key );
            tmp_ass_subscript_1 = var_key;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 38;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 33;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_3;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto frame_exception_exit_1;
        // End of try:
        try_end_3:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            CHECK_OBJECT( par_obj );
            tmp_isinstance_inst_2 = par_obj;
            tmp_isinstance_cls_2 = (PyObject *)&PyList_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_10;
                PyObject *tmp_iter_arg_3;
                PyObject *tmp_called_name_2;
                PyObject *tmp_args_element_name_2;
                tmp_called_name_2 = (PyObject *)&PyEnum_Type;
                CHECK_OBJECT( par_obj );
                tmp_args_element_name_2 = par_obj;
                frame_d4a68d445da39bc494ea5548053ffb3c->m_frame.f_lineno = 40;
                {
                    PyObject *call_args[] = { tmp_args_element_name_2 };
                    tmp_iter_arg_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                }

                if ( tmp_iter_arg_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 40;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_3 );
                Py_DECREF( tmp_iter_arg_3 );
                if ( tmp_assign_source_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 40;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_for_loop_2__for_iterator == NULL );
                tmp_for_loop_2__for_iterator = tmp_assign_source_10;
            }
            // Tried code:
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_11;
                CHECK_OBJECT( tmp_for_loop_2__for_iterator );
                tmp_next_source_2 = tmp_for_loop_2__for_iterator;
                tmp_assign_source_11 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_11 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "oooo";
                        exception_lineno = 40;
                        goto try_except_handler_5;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_2__iter_value;
                    tmp_for_loop_2__iter_value = tmp_assign_source_11;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_12;
                PyObject *tmp_iter_arg_4;
                CHECK_OBJECT( tmp_for_loop_2__iter_value );
                tmp_iter_arg_4 = tmp_for_loop_2__iter_value;
                tmp_assign_source_12 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
                if ( tmp_assign_source_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 40;
                    type_description_1 = "oooo";
                    goto try_except_handler_6;
                }
                {
                    PyObject *old = tmp_tuple_unpack_2__source_iter;
                    tmp_tuple_unpack_2__source_iter = tmp_assign_source_12;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_13;
                PyObject *tmp_unpack_3;
                CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
                tmp_assign_source_13 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
                if ( tmp_assign_source_13 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "oooo";
                    exception_lineno = 40;
                    goto try_except_handler_7;
                }
                {
                    PyObject *old = tmp_tuple_unpack_2__element_1;
                    tmp_tuple_unpack_2__element_1 = tmp_assign_source_13;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_14;
                PyObject *tmp_unpack_4;
                CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
                tmp_assign_source_14 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
                if ( tmp_assign_source_14 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "oooo";
                    exception_lineno = 40;
                    goto try_except_handler_7;
                }
                {
                    PyObject *old = tmp_tuple_unpack_2__element_2;
                    tmp_tuple_unpack_2__element_2 = tmp_assign_source_14;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_iterator_name_2;
                CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
                tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
                // Check if iterator has left-over elements.
                CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

                tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

                if (likely( tmp_iterator_attempt == NULL ))
                {
                    PyObject *error = GET_ERROR_OCCURRED();

                    if ( error != NULL )
                    {
                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                        {
                            CLEAR_ERROR_OCCURRED();
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "oooo";
                            exception_lineno = 40;
                            goto try_except_handler_7;
                        }
                    }
                }
                else
                {
                    Py_DECREF( tmp_iterator_attempt );

                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooo";
                    exception_lineno = 40;
                    goto try_except_handler_7;
                }
            }
            goto try_end_4;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_4 = exception_type;
            exception_keeper_value_4 = exception_value;
            exception_keeper_tb_4 = exception_tb;
            exception_keeper_lineno_4 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
            Py_DECREF( tmp_tuple_unpack_2__source_iter );
            tmp_tuple_unpack_2__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_4;
            exception_value = exception_keeper_value_4;
            exception_tb = exception_keeper_tb_4;
            exception_lineno = exception_keeper_lineno_4;

            goto try_except_handler_6;
            // End of try:
            try_end_4:;
            goto try_end_5;
            // Exception handler code:
            try_except_handler_6:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_tuple_unpack_2__element_1 );
            tmp_tuple_unpack_2__element_1 = NULL;

            Py_XDECREF( tmp_tuple_unpack_2__element_2 );
            tmp_tuple_unpack_2__element_2 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_5;
            exception_value = exception_keeper_value_5;
            exception_tb = exception_keeper_tb_5;
            exception_lineno = exception_keeper_lineno_5;

            goto try_except_handler_5;
            // End of try:
            try_end_5:;
            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
            Py_DECREF( tmp_tuple_unpack_2__source_iter );
            tmp_tuple_unpack_2__source_iter = NULL;

            {
                PyObject *tmp_assign_source_15;
                CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
                tmp_assign_source_15 = tmp_tuple_unpack_2__element_1;
                {
                    PyObject *old = var_i;
                    var_i = tmp_assign_source_15;
                    Py_INCREF( var_i );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_tuple_unpack_2__element_1 );
            tmp_tuple_unpack_2__element_1 = NULL;

            {
                PyObject *tmp_assign_source_16;
                CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
                tmp_assign_source_16 = tmp_tuple_unpack_2__element_2;
                {
                    PyObject *old = var_value;
                    var_value = tmp_assign_source_16;
                    Py_INCREF( var_value );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_tuple_unpack_2__element_2 );
            tmp_tuple_unpack_2__element_2 = NULL;

            {
                PyObject *tmp_ass_subvalue_2;
                PyObject *tmp_called_name_3;
                PyObject *tmp_mvar_value_2;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_ass_subscribed_2;
                PyObject *tmp_ass_subscript_2;
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__relax_additional_properties );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__relax_additional_properties );
                }

                if ( tmp_mvar_value_2 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_relax_additional_properties" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 41;
                    type_description_1 = "oooo";
                    goto try_except_handler_5;
                }

                tmp_called_name_3 = tmp_mvar_value_2;
                CHECK_OBJECT( var_value );
                tmp_args_element_name_3 = var_value;
                frame_d4a68d445da39bc494ea5548053ffb3c->m_frame.f_lineno = 41;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3 };
                    tmp_ass_subvalue_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                }

                if ( tmp_ass_subvalue_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 41;
                    type_description_1 = "oooo";
                    goto try_except_handler_5;
                }
                CHECK_OBJECT( par_obj );
                tmp_ass_subscribed_2 = par_obj;
                CHECK_OBJECT( var_i );
                tmp_ass_subscript_2 = var_i;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
                Py_DECREF( tmp_ass_subvalue_2 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 41;
                    type_description_1 = "oooo";
                    goto try_except_handler_5;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "oooo";
                goto try_except_handler_5;
            }
            goto loop_start_2;
            loop_end_2:;
            goto try_end_6;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_2__iter_value );
            tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
            Py_DECREF( tmp_for_loop_2__for_iterator );
            tmp_for_loop_2__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto frame_exception_exit_1;
            // End of try:
            try_end_6:;
            Py_XDECREF( tmp_for_loop_2__iter_value );
            tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
            Py_DECREF( tmp_for_loop_2__for_iterator );
            tmp_for_loop_2__for_iterator = NULL;

            branch_no_3:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d4a68d445da39bc494ea5548053ffb3c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d4a68d445da39bc494ea5548053ffb3c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d4a68d445da39bc494ea5548053ffb3c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d4a68d445da39bc494ea5548053ffb3c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d4a68d445da39bc494ea5548053ffb3c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d4a68d445da39bc494ea5548053ffb3c,
        type_description_1,
        par_obj,
        var_key,
        var_value,
        var_i
    );


    // Release cached frame.
    if ( frame_d4a68d445da39bc494ea5548053ffb3c == cache_frame_d4a68d445da39bc494ea5548053ffb3c )
    {
        Py_DECREF( frame_d4a68d445da39bc494ea5548053ffb3c );
    }
    cache_frame_d4a68d445da39bc494ea5548053ffb3c = NULL;

    assertFrameObject( frame_d4a68d445da39bc494ea5548053ffb3c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_obj );
    tmp_return_value = par_obj;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_1__relax_additional_properties );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_value );
    var_value = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_1__relax_additional_properties );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$validator$$$function_2__allow_undefined( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_schema = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9938aee43aa0f3502e63636a194a4acb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9938aee43aa0f3502e63636a194a4acb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9938aee43aa0f3502e63636a194a4acb, codeobj_9938aee43aa0f3502e63636a194a4acb, module_nbformat$validator, sizeof(void *) );
    frame_9938aee43aa0f3502e63636a194a4acb = cache_frame_9938aee43aa0f3502e63636a194a4acb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9938aee43aa0f3502e63636a194a4acb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9938aee43aa0f3502e63636a194a4acb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscribed_name_3;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_subscript_name_2;
        PyObject *tmp_subscript_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_call_arg_element_1;
        CHECK_OBJECT( par_schema );
        tmp_subscribed_name_3 = par_schema;
        tmp_subscript_name_1 = const_str_plain_definitions;
        tmp_subscribed_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_1 );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_2 = const_str_plain_cell;
        tmp_subscribed_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_3 = const_str_plain_oneOf;
        tmp_called_instance_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_3 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_call_arg_element_1 = PyDict_Copy( const_dict_51f76991ef7771af136a0c949efbabd5 );
        frame_9938aee43aa0f3502e63636a194a4acb->m_frame.f_lineno = 45;
        {
            PyObject *call_args[] = { tmp_call_arg_element_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        Py_DECREF( tmp_call_arg_element_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 45;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscribed_name_5;
        PyObject *tmp_subscribed_name_6;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_subscript_name_5;
        PyObject *tmp_subscript_name_6;
        PyObject *tmp_call_result_2;
        PyObject *tmp_call_arg_element_2;
        CHECK_OBJECT( par_schema );
        tmp_subscribed_name_6 = par_schema;
        tmp_subscript_name_4 = const_str_plain_definitions;
        tmp_subscribed_name_5 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_4 );
        if ( tmp_subscribed_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_5 = const_str_plain_output;
        tmp_subscribed_name_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
        Py_DECREF( tmp_subscribed_name_5 );
        if ( tmp_subscribed_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_6 = const_str_plain_oneOf;
        tmp_called_instance_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_6 );
        Py_DECREF( tmp_subscribed_name_4 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_call_arg_element_2 = PyDict_Copy( const_dict_892d7172c91c0d0d5faa082d734d9593 );
        frame_9938aee43aa0f3502e63636a194a4acb->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_call_arg_element_2 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        Py_DECREF( tmp_call_arg_element_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9938aee43aa0f3502e63636a194a4acb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9938aee43aa0f3502e63636a194a4acb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9938aee43aa0f3502e63636a194a4acb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9938aee43aa0f3502e63636a194a4acb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9938aee43aa0f3502e63636a194a4acb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9938aee43aa0f3502e63636a194a4acb,
        type_description_1,
        par_schema
    );


    // Release cached frame.
    if ( frame_9938aee43aa0f3502e63636a194a4acb == cache_frame_9938aee43aa0f3502e63636a194a4acb )
    {
        Py_DECREF( frame_9938aee43aa0f3502e63636a194a4acb );
    }
    cache_frame_9938aee43aa0f3502e63636a194a4acb = NULL;

    assertFrameObject( frame_9938aee43aa0f3502e63636a194a4acb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_schema );
    tmp_return_value = par_schema;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_2__allow_undefined );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_schema );
    Py_DECREF( par_schema );
    par_schema = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_schema );
    Py_DECREF( par_schema );
    par_schema = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_2__allow_undefined );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$validator$$$function_3_get_validator( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_version = python_pars[ 0 ];
    PyObject *par_version_minor = python_pars[ 1 ];
    PyObject *par_relax_add_props = python_pars[ 2 ];
    PyObject *var_current_nbformat = NULL;
    PyObject *var_v = NULL;
    PyObject *var_current_minor = NULL;
    PyObject *var_version_tuple = NULL;
    PyObject *var_schema_json = NULL;
    struct Nuitka_FrameObject *frame_ae0e53b0d21be0bf143aa79b66326c88;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_ae0e53b0d21be0bf143aa79b66326c88 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ae0e53b0d21be0bf143aa79b66326c88, codeobj_ae0e53b0d21be0bf143aa79b66326c88, module_nbformat$validator, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ae0e53b0d21be0bf143aa79b66326c88 = cache_frame_ae0e53b0d21be0bf143aa79b66326c88;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ae0e53b0d21be0bf143aa79b66326c88 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ae0e53b0d21be0bf143aa79b66326c88 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_version );
        tmp_compexpr_left_1 = par_version;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_import_name_from_1;
            PyObject *tmp_name_name_1;
            PyObject *tmp_globals_name_1;
            PyObject *tmp_locals_name_1;
            PyObject *tmp_fromlist_name_1;
            PyObject *tmp_level_name_1;
            tmp_name_name_1 = const_str_empty;
            tmp_globals_name_1 = (PyObject *)moduledict_nbformat$validator;
            tmp_locals_name_1 = Py_None;
            tmp_fromlist_name_1 = const_tuple_str_plain_current_nbformat_tuple;
            tmp_level_name_1 = const_int_pos_2;
            frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = 56;
            tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
            if ( tmp_import_name_from_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 56;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            if ( PyModule_Check( tmp_import_name_from_1 ) )
            {
               tmp_assign_source_1 = IMPORT_NAME_OR_MODULE(
                    tmp_import_name_from_1,
                    (PyObject *)moduledict_nbformat$validator,
                    const_str_plain_current_nbformat,
                    const_int_pos_2
                );
            }
            else
            {
               tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_current_nbformat );
            }

            Py_DECREF( tmp_import_name_from_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 56;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_current_nbformat == NULL );
            var_current_nbformat = tmp_assign_source_1;
        }
        {
            PyObject *tmp_assign_source_2;
            CHECK_OBJECT( var_current_nbformat );
            tmp_assign_source_2 = var_current_nbformat;
            {
                PyObject *old = par_version;
                assert( old != NULL );
                par_version = tmp_assign_source_2;
                Py_INCREF( par_version );
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_import_item );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_import_item );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "import_item" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 59;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_left_name_1 = const_str_digest_e5a1c615b7fb0920230aea115440c8ab;
        CHECK_OBJECT( par_version );
        tmp_right_name_1 = par_version;
        tmp_args_element_name_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = 59;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_v == NULL );
        var_v = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_getattr_default_1;
        CHECK_OBJECT( var_v );
        tmp_getattr_target_1 = var_v;
        tmp_getattr_attr_1 = const_str_plain_nbformat_minor;
        tmp_getattr_default_1 = const_int_0;
        tmp_assign_source_4 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_current_minor == NULL );
        var_current_minor = tmp_assign_source_4;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_version_minor );
        tmp_compexpr_left_2 = par_version_minor;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( var_current_minor );
            tmp_assign_source_5 = var_current_minor;
            {
                PyObject *old = par_version_minor;
                assert( old != NULL );
                par_version_minor = tmp_assign_source_5;
                Py_INCREF( par_version_minor );
                Py_DECREF( old );
            }

        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_tuple_element_1;
        CHECK_OBJECT( par_version );
        tmp_tuple_element_1 = par_version;
        tmp_assign_source_6 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_6, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_version_minor );
        tmp_tuple_element_1 = par_version_minor;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_6, 1, tmp_tuple_element_1 );
        assert( var_version_tuple == NULL );
        var_version_tuple = tmp_assign_source_6;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( var_version_tuple );
        tmp_compexpr_left_3 = var_version_tuple;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_validators );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validators );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validators" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 66;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_3 = tmp_mvar_value_2;
        tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__get_schema_json );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_schema_json );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_schema_json" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 68;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }

            tmp_called_name_2 = tmp_mvar_value_3;
            CHECK_OBJECT( var_v );
            tmp_args_element_name_2 = var_v;
            frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = 68;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            assert( var_schema_json == NULL );
            var_schema_json = tmp_assign_source_7;
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_1 );
        exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_1 );
        exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_1 );

        if ( exception_keeper_tb_1 == NULL )
        {
            exception_keeper_tb_1 = MAKE_TRACEBACK( frame_ae0e53b0d21be0bf143aa79b66326c88, exception_keeper_lineno_1 );
        }
        else if ( exception_keeper_lineno_1 != 0 )
        {
            exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_ae0e53b0d21be0bf143aa79b66326c88, exception_keeper_lineno_1 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
        PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            tmp_compexpr_left_4 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_4 = PyExc_AttributeError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 69;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_return_value = Py_None;
            Py_INCREF( tmp_return_value );
            goto try_return_handler_3;
            goto branch_end_4;
            branch_no_4:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 67;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame) frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
            branch_end_4:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_3_get_validator );
        return NULL;
        // Return handler code:
        try_return_handler_3:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        // End of try:
        try_end_1:;
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( var_current_minor );
            tmp_compexpr_left_5 = var_current_minor;
            CHECK_OBJECT( par_version_minor );
            tmp_compexpr_right_5 = par_version_minor;
            tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 72;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assign_source_8;
                PyObject *tmp_called_name_3;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_args_element_name_3;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__relax_additional_properties );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__relax_additional_properties );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_relax_additional_properties" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 74;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_3 = tmp_mvar_value_4;
                CHECK_OBJECT( var_schema_json );
                tmp_args_element_name_3 = var_schema_json;
                frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = 74;
                {
                    PyObject *call_args[] = { tmp_args_element_name_3 };
                    tmp_assign_source_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                }

                if ( tmp_assign_source_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 74;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_schema_json;
                    assert( old != NULL );
                    var_schema_json = tmp_assign_source_8;
                    Py_DECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_9;
                PyObject *tmp_called_name_4;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_args_element_name_4;
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__allow_undefined );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__allow_undefined );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_allow_undefined" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 76;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_4 = tmp_mvar_value_5;
                CHECK_OBJECT( var_schema_json );
                tmp_args_element_name_4 = var_schema_json;
                frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = 76;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4 };
                    tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                }

                if ( tmp_assign_source_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 76;
                    type_description_1 = "oooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_schema_json;
                    assert( old != NULL );
                    var_schema_json = tmp_assign_source_9;
                    Py_DECREF( old );
                }

            }
            branch_no_5:;
        }
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_ass_subscript_1;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_Validator );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Validator );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Validator" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 78;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_5 = tmp_mvar_value_6;
            CHECK_OBJECT( var_schema_json );
            tmp_args_element_name_5 = var_schema_json;
            frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = 78;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_ass_subvalue_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            if ( tmp_ass_subvalue_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 78;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_validators );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validators );
            }

            if ( tmp_mvar_value_7 == NULL )
            {
                Py_DECREF( tmp_ass_subvalue_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validators" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 78;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_ass_subscribed_1 = tmp_mvar_value_7;
            CHECK_OBJECT( var_version_tuple );
            tmp_ass_subscript_1 = var_version_tuple;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 78;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_6;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_relax_add_props );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_relax_add_props );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_6 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        // Tried code:
        {
            PyObject *tmp_assign_source_10;
            PyObject *tmp_called_name_6;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_args_element_name_6;
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__get_schema_json );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__get_schema_json );
            }

            if ( tmp_mvar_value_8 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_get_schema_json" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 82;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }

            tmp_called_name_6 = tmp_mvar_value_8;
            CHECK_OBJECT( var_v );
            tmp_args_element_name_6 = var_v;
            frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = 82;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_assign_source_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            if ( tmp_assign_source_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 82;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            {
                PyObject *old = var_schema_json;
                var_schema_json = tmp_assign_source_10;
                Py_XDECREF( old );
            }

        }
        goto try_end_2;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Preserve existing published exception.
        exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_type_2 );
        exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
        Py_XINCREF( exception_preserved_value_2 );
        exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
        Py_XINCREF( exception_preserved_tb_2 );

        if ( exception_keeper_tb_3 == NULL )
        {
            exception_keeper_tb_3 = MAKE_TRACEBACK( frame_ae0e53b0d21be0bf143aa79b66326c88, exception_keeper_lineno_3 );
        }
        else if ( exception_keeper_lineno_3 != 0 )
        {
            exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_ae0e53b0d21be0bf143aa79b66326c88, exception_keeper_lineno_3 );
        }

        NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
        PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
        PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
        // Tried code:
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            tmp_compexpr_left_6 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_6 = PyExc_AttributeError;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 83;
                type_description_1 = "oooooooo";
                goto try_except_handler_5;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            tmp_return_value = Py_None;
            Py_INCREF( tmp_return_value );
            goto try_return_handler_5;
            goto branch_end_7;
            branch_no_7:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 81;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame) frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
            branch_end_7:;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_3_get_validator );
        return NULL;
        // Return handler code:
        try_return_handler_5:;
        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
        goto frame_return_exit_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Restore previous exception.
        SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        // End of try:
        try_end_2:;
        {
            PyObject *tmp_assign_source_11;
            PyObject *tmp_called_name_7;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_args_element_name_7;
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__relax_additional_properties );

            if (unlikely( tmp_mvar_value_9 == NULL ))
            {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__relax_additional_properties );
            }

            if ( tmp_mvar_value_9 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_relax_additional_properties" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 88;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_7 = tmp_mvar_value_9;
            CHECK_OBJECT( var_schema_json );
            tmp_args_element_name_7 = var_schema_json;
            frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = 88;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_assign_source_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            if ( tmp_assign_source_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_schema_json;
                assert( old != NULL );
                var_schema_json = tmp_assign_source_11;
                Py_DECREF( old );
            }

        }
        {
            PyObject *tmp_ass_subvalue_2;
            PyObject *tmp_called_name_8;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_ass_subscribed_2;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_ass_subscript_2;
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_Validator );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Validator );
            }

            if ( tmp_mvar_value_10 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Validator" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 90;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_8 = tmp_mvar_value_10;
            CHECK_OBJECT( var_schema_json );
            tmp_args_element_name_8 = var_schema_json;
            frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame.f_lineno = 90;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_ass_subvalue_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
            }

            if ( tmp_ass_subvalue_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_validators );

            if (unlikely( tmp_mvar_value_11 == NULL ))
            {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validators );
            }

            if ( tmp_mvar_value_11 == NULL )
            {
                Py_DECREF( tmp_ass_subvalue_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validators" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 90;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_ass_subscribed_2 = tmp_mvar_value_11;
            CHECK_OBJECT( var_version_tuple );
            tmp_ass_subscript_2 = var_version_tuple;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
            Py_DECREF( tmp_ass_subvalue_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_6:;
    }
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_validators );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validators );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validators" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 91;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_12;
        CHECK_OBJECT( var_version_tuple );
        tmp_subscript_name_1 = var_version_tuple;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ae0e53b0d21be0bf143aa79b66326c88 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ae0e53b0d21be0bf143aa79b66326c88 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ae0e53b0d21be0bf143aa79b66326c88 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ae0e53b0d21be0bf143aa79b66326c88, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ae0e53b0d21be0bf143aa79b66326c88->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ae0e53b0d21be0bf143aa79b66326c88, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ae0e53b0d21be0bf143aa79b66326c88,
        type_description_1,
        par_version,
        par_version_minor,
        par_relax_add_props,
        var_current_nbformat,
        var_v,
        var_current_minor,
        var_version_tuple,
        var_schema_json
    );


    // Release cached frame.
    if ( frame_ae0e53b0d21be0bf143aa79b66326c88 == cache_frame_ae0e53b0d21be0bf143aa79b66326c88 )
    {
        Py_DECREF( frame_ae0e53b0d21be0bf143aa79b66326c88 );
    }
    cache_frame_ae0e53b0d21be0bf143aa79b66326c88 = NULL;

    assertFrameObject( frame_ae0e53b0d21be0bf143aa79b66326c88 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_3_get_validator );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_version );
    Py_DECREF( par_version );
    par_version = NULL;

    CHECK_OBJECT( (PyObject *)par_version_minor );
    Py_DECREF( par_version_minor );
    par_version_minor = NULL;

    CHECK_OBJECT( (PyObject *)par_relax_add_props );
    Py_DECREF( par_relax_add_props );
    par_relax_add_props = NULL;

    Py_XDECREF( var_current_nbformat );
    var_current_nbformat = NULL;

    CHECK_OBJECT( (PyObject *)var_v );
    Py_DECREF( var_v );
    var_v = NULL;

    CHECK_OBJECT( (PyObject *)var_current_minor );
    Py_DECREF( var_current_minor );
    var_current_minor = NULL;

    CHECK_OBJECT( (PyObject *)var_version_tuple );
    Py_DECREF( var_version_tuple );
    var_version_tuple = NULL;

    Py_XDECREF( var_schema_json );
    var_schema_json = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_version );
    par_version = NULL;

    Py_XDECREF( par_version_minor );
    par_version_minor = NULL;

    CHECK_OBJECT( (PyObject *)par_relax_add_props );
    Py_DECREF( par_relax_add_props );
    par_relax_add_props = NULL;

    Py_XDECREF( var_current_nbformat );
    var_current_nbformat = NULL;

    Py_XDECREF( var_v );
    var_v = NULL;

    Py_XDECREF( var_current_minor );
    var_current_minor = NULL;

    Py_XDECREF( var_version_tuple );
    var_version_tuple = NULL;

    Py_XDECREF( var_schema_json );
    var_schema_json = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_3_get_validator );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$validator$$$function_4__get_schema_json( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_v = python_pars[ 0 ];
    PyObject *var_schema_path = NULL;
    PyObject *var_f = NULL;
    PyObject *var_schema_json = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_575fedaf3fe165e75d8b92029ab5ae29;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_575fedaf3fe165e75d8b92029ab5ae29 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_575fedaf3fe165e75d8b92029ab5ae29, codeobj_575fedaf3fe165e75d8b92029ab5ae29, module_nbformat$validator, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_575fedaf3fe165e75d8b92029ab5ae29 = cache_frame_575fedaf3fe165e75d8b92029ab5ae29;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_575fedaf3fe165e75d8b92029ab5ae29 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_575fedaf3fe165e75d8b92029ab5ae29 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_6;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_1;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_path );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_os );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_os );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "os" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_2;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_path );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_dirname );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_v );
        tmp_source_name_5 = par_v;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain___file__ );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame.f_lineno = 98;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_v );
        tmp_source_name_6 = par_v;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_nbformat_schema );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame.f_lineno = 98;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_3 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_schema_path == NULL );
        var_schema_path = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_open_filename_1;
        CHECK_OBJECT( var_schema_path );
        tmp_open_filename_1 = var_schema_path;
        tmp_assign_source_2 = BUILTIN_OPEN( tmp_open_filename_1, NULL, NULL, NULL, NULL, NULL, NULL, NULL );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__source == NULL );
        tmp_with_1__source = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_7 = tmp_with_1__source;
        tmp_called_name_3 = LOOKUP_SPECIAL( tmp_source_name_7, const_str_plain___enter__ );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame.f_lineno = 99;
        tmp_assign_source_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        Py_DECREF( tmp_called_name_3 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__enter == NULL );
        tmp_with_1__enter = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_source_name_8;
        CHECK_OBJECT( tmp_with_1__source );
        tmp_source_name_8 = tmp_with_1__source;
        tmp_assign_source_4 = LOOKUP_SPECIAL( tmp_source_name_8, const_str_plain___exit__ );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        assert( tmp_with_1__exit == NULL );
        tmp_with_1__exit = tmp_assign_source_4;
    }
    {
        nuitka_bool tmp_assign_source_5;
        tmp_assign_source_5 = NUITKA_BOOL_TRUE;
        tmp_with_1__indicator = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_with_1__enter );
        tmp_assign_source_6 = tmp_with_1__enter;
        assert( var_f == NULL );
        Py_INCREF( tmp_assign_source_6 );
        var_f = tmp_assign_source_6;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_json );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_json );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "json" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 100;
            type_description_1 = "oooo";
            goto try_except_handler_4;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        CHECK_OBJECT( var_f );
        tmp_args_element_name_4 = var_f;
        frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame.f_lineno = 100;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_load, call_args );
        }

        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "oooo";
            goto try_except_handler_4;
        }
        assert( var_schema_json == NULL );
        var_schema_json = tmp_assign_source_7;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_575fedaf3fe165e75d8b92029ab5ae29, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_575fedaf3fe165e75d8b92029ab5ae29, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_BaseException;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_assign_source_8;
            tmp_assign_source_8 = NUITKA_BOOL_FALSE;
            tmp_with_1__indicator = tmp_assign_source_8;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_args_element_name_7;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_4 = tmp_with_1__exit;
            tmp_args_element_name_5 = EXC_TYPE(PyThreadState_GET());
            tmp_args_element_name_6 = EXC_VALUE(PyThreadState_GET());
            tmp_args_element_name_7 = EXC_TRACEBACK(PyThreadState_GET());
            frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame.f_lineno = 100;
            {
                PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
                tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
            }

            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "oooo";
                goto try_except_handler_5;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "oooo";
                goto try_except_handler_5;
            }
            tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 100;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame) frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooo";
            goto try_except_handler_5;
            branch_no_2:;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 99;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame) frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooo";
        goto try_except_handler_5;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_3;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_4__get_schema_json );
    return NULL;
    // End of try:
    try_end_1:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = tmp_with_1__indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_5;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_5 = tmp_with_1__exit;
            frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame.f_lineno = 100;
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                Py_DECREF( exception_keeper_type_3 );
                Py_XDECREF( exception_keeper_value_3 );
                Py_XDECREF( exception_keeper_tb_3 );

                exception_lineno = 100;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_3:;
    }
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_2;
    // End of try:
    try_end_3:;
    {
        nuitka_bool tmp_condition_result_4;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_3 = tmp_with_1__indicator;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_6;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( tmp_with_1__exit );
            tmp_called_name_6 = tmp_with_1__exit;
            frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame.f_lineno = 100;
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "oooo";
                goto try_except_handler_2;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_4:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    Py_XDECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    Py_XDECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_with_1__source );
    Py_DECREF( tmp_with_1__source );
    tmp_with_1__source = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
    Py_DECREF( tmp_with_1__enter );
    tmp_with_1__enter = NULL;

    CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
    Py_DECREF( tmp_with_1__exit );
    tmp_with_1__exit = NULL;

    if ( var_schema_json == NULL )
    {

        exception_type = PyExc_UnboundLocalError;
        Py_INCREF( exception_type );
        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "schema_json" );
        exception_tb = NULL;
        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        CHAIN_EXCEPTION( exception_value );

        exception_lineno = 101;
        type_description_1 = "oooo";
        goto frame_exception_exit_1;
    }

    tmp_return_value = var_schema_json;
    Py_INCREF( tmp_return_value );
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_575fedaf3fe165e75d8b92029ab5ae29 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_575fedaf3fe165e75d8b92029ab5ae29 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_575fedaf3fe165e75d8b92029ab5ae29 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_575fedaf3fe165e75d8b92029ab5ae29, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_575fedaf3fe165e75d8b92029ab5ae29->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_575fedaf3fe165e75d8b92029ab5ae29, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_575fedaf3fe165e75d8b92029ab5ae29,
        type_description_1,
        par_v,
        var_schema_path,
        var_f,
        var_schema_json
    );


    // Release cached frame.
    if ( frame_575fedaf3fe165e75d8b92029ab5ae29 == cache_frame_575fedaf3fe165e75d8b92029ab5ae29 )
    {
        Py_DECREF( frame_575fedaf3fe165e75d8b92029ab5ae29 );
    }
    cache_frame_575fedaf3fe165e75d8b92029ab5ae29 = NULL;

    assertFrameObject( frame_575fedaf3fe165e75d8b92029ab5ae29 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_4__get_schema_json );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_v );
    Py_DECREF( par_v );
    par_v = NULL;

    CHECK_OBJECT( (PyObject *)var_schema_path );
    Py_DECREF( var_schema_path );
    var_schema_path = NULL;

    CHECK_OBJECT( (PyObject *)var_f );
    Py_DECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_schema_json );
    var_schema_json = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_v );
    Py_DECREF( par_v );
    par_v = NULL;

    Py_XDECREF( var_schema_path );
    var_schema_path = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    Py_XDECREF( var_schema_json );
    var_schema_json = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_4__get_schema_json );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$validator$$$function_5_isvalid( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_nbjson = python_pars[ 0 ];
    PyObject *par_ref = python_pars[ 1 ];
    PyObject *par_version = python_pars[ 2 ];
    PyObject *par_version_minor = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_a4ace2a051dff874cfe8b840f1be8cab;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_a4ace2a051dff874cfe8b840f1be8cab = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a4ace2a051dff874cfe8b840f1be8cab, codeobj_a4ace2a051dff874cfe8b840f1be8cab, module_nbformat$validator, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a4ace2a051dff874cfe8b840f1be8cab = cache_frame_a4ace2a051dff874cfe8b840f1be8cab;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a4ace2a051dff874cfe8b840f1be8cab );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a4ace2a051dff874cfe8b840f1be8cab ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_validate );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 112;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_nbjson );
        tmp_args_element_name_1 = par_nbjson;
        CHECK_OBJECT( par_ref );
        tmp_args_element_name_2 = par_ref;
        CHECK_OBJECT( par_version );
        tmp_args_element_name_3 = par_version;
        CHECK_OBJECT( par_version_minor );
        tmp_args_element_name_4 = par_version_minor;
        frame_a4ace2a051dff874cfe8b840f1be8cab->m_frame.f_lineno = 112;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS4( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_a4ace2a051dff874cfe8b840f1be8cab, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_a4ace2a051dff874cfe8b840f1be8cab, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_2;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_ValidationError );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ValidationError );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ValidationError" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 113;
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_2;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "oooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_False;
        Py_INCREF( tmp_return_value );
        goto try_return_handler_3;
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 111;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_a4ace2a051dff874cfe8b840f1be8cab->m_frame) frame_a4ace2a051dff874cfe8b840f1be8cab->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_5_isvalid );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a4ace2a051dff874cfe8b840f1be8cab );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a4ace2a051dff874cfe8b840f1be8cab );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a4ace2a051dff874cfe8b840f1be8cab );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a4ace2a051dff874cfe8b840f1be8cab, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a4ace2a051dff874cfe8b840f1be8cab->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a4ace2a051dff874cfe8b840f1be8cab, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a4ace2a051dff874cfe8b840f1be8cab,
        type_description_1,
        par_nbjson,
        par_ref,
        par_version,
        par_version_minor
    );


    // Release cached frame.
    if ( frame_a4ace2a051dff874cfe8b840f1be8cab == cache_frame_a4ace2a051dff874cfe8b840f1be8cab )
    {
        Py_DECREF( frame_a4ace2a051dff874cfe8b840f1be8cab );
    }
    cache_frame_a4ace2a051dff874cfe8b840f1be8cab = NULL;

    assertFrameObject( frame_a4ace2a051dff874cfe8b840f1be8cab );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_True;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_5_isvalid );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_nbjson );
    Py_DECREF( par_nbjson );
    par_nbjson = NULL;

    CHECK_OBJECT( (PyObject *)par_ref );
    Py_DECREF( par_ref );
    par_ref = NULL;

    CHECK_OBJECT( (PyObject *)par_version );
    Py_DECREF( par_version );
    par_version = NULL;

    CHECK_OBJECT( (PyObject *)par_version_minor );
    Py_DECREF( par_version_minor );
    par_version_minor = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_nbjson );
    Py_DECREF( par_nbjson );
    par_nbjson = NULL;

    CHECK_OBJECT( (PyObject *)par_ref );
    Py_DECREF( par_ref );
    par_ref = NULL;

    CHECK_OBJECT( (PyObject *)par_version );
    Py_DECREF( par_version );
    par_version = NULL;

    CHECK_OBJECT( (PyObject *)par_version_minor );
    Py_DECREF( par_version_minor );
    par_version_minor = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_5_isvalid );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$validator$$$function_6__format_as_index( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_indices = python_pars[ 0 ];
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_5f3d662285e0b4833cd1e6ebec7cd9ce;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_5f3d662285e0b4833cd1e6ebec7cd9ce = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5f3d662285e0b4833cd1e6ebec7cd9ce, codeobj_5f3d662285e0b4833cd1e6ebec7cd9ce, module_nbformat$validator, sizeof(void *) );
    frame_5f3d662285e0b4833cd1e6ebec7cd9ce = cache_frame_5f3d662285e0b4833cd1e6ebec7cd9ce;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5f3d662285e0b4833cd1e6ebec7cd9ce );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5f3d662285e0b4833cd1e6ebec7cd9ce ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( par_indices );
        tmp_operand_name_1 = par_indices;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = const_str_empty;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        tmp_left_name_1 = const_str_digest_0674107b2d3e86aecfffb00b3395ae4a;
        tmp_source_name_1 = const_str_digest_c184ec84b5d2106ce1dac869eaf47241;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        assert( !(tmp_called_name_1 == NULL) );
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_indices );
            tmp_iter_arg_1 = par_indices;
            tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_1;
        }
        // Tried code:
        tmp_args_element_name_1 = nbformat$validator$$$function_6__format_as_index$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_6__format_as_index );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_6__format_as_index );
        return NULL;
        outline_result_1:;
        frame_5f3d662285e0b4833cd1e6ebec7cd9ce->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f3d662285e0b4833cd1e6ebec7cd9ce );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f3d662285e0b4833cd1e6ebec7cd9ce );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5f3d662285e0b4833cd1e6ebec7cd9ce );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5f3d662285e0b4833cd1e6ebec7cd9ce, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5f3d662285e0b4833cd1e6ebec7cd9ce->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5f3d662285e0b4833cd1e6ebec7cd9ce, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5f3d662285e0b4833cd1e6ebec7cd9ce,
        type_description_1,
        par_indices
    );


    // Release cached frame.
    if ( frame_5f3d662285e0b4833cd1e6ebec7cd9ce == cache_frame_5f3d662285e0b4833cd1e6ebec7cd9ce )
    {
        Py_DECREF( frame_5f3d662285e0b4833cd1e6ebec7cd9ce );
    }
    cache_frame_5f3d662285e0b4833cd1e6ebec7cd9ce = NULL;

    assertFrameObject( frame_5f3d662285e0b4833cd1e6ebec7cd9ce );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_6__format_as_index );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_indices );
    Py_DECREF( par_indices );
    par_indices = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_indices );
    Py_DECREF( par_indices );
    par_indices = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_6__format_as_index );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct nbformat$validator$$$function_6__format_as_index$$$genexpr_1_genexpr_locals {
    PyObject *var_index;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *nbformat$validator$$$function_6__format_as_index$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct nbformat$validator$$$function_6__format_as_index$$$genexpr_1_genexpr_locals *generator_heap = (struct nbformat$validator$$$function_6__format_as_index$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_index = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_5de0013cd5729bd6a157c35ec96909a3, module_nbformat$validator, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 129;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_index;
            generator_heap->var_index = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_index );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_operand_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_index );
        tmp_operand_name_1 = generator_heap->var_index;
        tmp_expression_name_1 = UNARY_OPERATION( PyObject_Repr, tmp_operand_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 129;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_operand_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_operand_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 129;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 129;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_index
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_index );
    generator_heap->var_index = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_index );
    generator_heap->var_index = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *nbformat$validator$$$function_6__format_as_index$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        nbformat$validator$$$function_6__format_as_index$$$genexpr_1_genexpr_context,
        module_nbformat$validator,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_4231fff64c06f120b877fa923b99296c,
#endif
        codeobj_5de0013cd5729bd6a157c35ec96909a3,
        1,
        sizeof(struct nbformat$validator$$$function_6__format_as_index$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_nbformat$validator$$$function_7__truncate_obj( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_obj = python_pars[ 0 ];
    PyObject *var_truncated = NULL;
    PyObject *outline_0_var_k = NULL;
    PyObject *outline_0_var_v = NULL;
    PyObject *outline_1_var_item = NULL;
    PyObject *tmp_dictcontraction$tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_dictcontraction$tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_dictcontraction$tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_dictcontraction_1__$0 = NULL;
    PyObject *tmp_dictcontraction_1__contraction = NULL;
    PyObject *tmp_dictcontraction_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_50599b5737e0048448bdec7289af66ba;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    int tmp_res;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_0f4073f2f3ee8f4812497a8bc5624d58_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_0f4073f2f3ee8f4812497a8bc5624d58_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    struct Nuitka_FrameObject *frame_07d770b8e9333a18782916368eaaa45b_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_07d770b8e9333a18782916368eaaa45b_3 = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_50599b5737e0048448bdec7289af66ba = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_50599b5737e0048448bdec7289af66ba, codeobj_50599b5737e0048448bdec7289af66ba, module_nbformat$validator, sizeof(void *)+sizeof(void *) );
    frame_50599b5737e0048448bdec7289af66ba = cache_frame_50599b5737e0048448bdec7289af66ba;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_50599b5737e0048448bdec7289af66ba );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_50599b5737e0048448bdec7289af66ba ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        CHECK_OBJECT( par_obj );
        tmp_isinstance_inst_1 = par_obj;
        tmp_isinstance_cls_1 = (PyObject *)&PyDict_Type;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            // Tried code:
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_iter_arg_1;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_list_arg_1;
                PyObject *tmp_called_instance_1;
                PyObject *tmp_subscript_name_1;
                PyObject *tmp_start_name_1;
                PyObject *tmp_stop_name_1;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_step_name_1;
                CHECK_OBJECT( par_obj );
                tmp_called_instance_1 = par_obj;
                frame_50599b5737e0048448bdec7289af66ba->m_frame.f_lineno = 140;
                tmp_list_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
                if ( tmp_list_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_1 = "oo";
                    goto try_except_handler_2;
                }
                tmp_subscribed_name_1 = PySequence_List( tmp_list_arg_1 );
                Py_DECREF( tmp_list_arg_1 );
                if ( tmp_subscribed_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_1 = "oo";
                    goto try_except_handler_2;
                }
                tmp_start_name_1 = Py_None;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );
                }

                if ( tmp_mvar_value_1 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_1 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ITEM_LIMIT" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 140;
                    type_description_1 = "oo";
                    goto try_except_handler_2;
                }

                tmp_stop_name_1 = tmp_mvar_value_1;
                tmp_step_name_1 = Py_None;
                tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
                assert( !(tmp_subscript_name_1 == NULL) );
                tmp_iter_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
                Py_DECREF( tmp_subscribed_name_1 );
                Py_DECREF( tmp_subscript_name_1 );
                if ( tmp_iter_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_1 = "oo";
                    goto try_except_handler_2;
                }
                tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
                Py_DECREF( tmp_iter_arg_1 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_1 = "oo";
                    goto try_except_handler_2;
                }
                assert( tmp_dictcontraction_1__$0 == NULL );
                tmp_dictcontraction_1__$0 = tmp_assign_source_2;
            }
            {
                PyObject *tmp_assign_source_3;
                tmp_assign_source_3 = PyDict_New();
                assert( tmp_dictcontraction_1__contraction == NULL );
                tmp_dictcontraction_1__contraction = tmp_assign_source_3;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_0f4073f2f3ee8f4812497a8bc5624d58_2, codeobj_0f4073f2f3ee8f4812497a8bc5624d58, module_nbformat$validator, sizeof(void *)+sizeof(void *) );
            frame_0f4073f2f3ee8f4812497a8bc5624d58_2 = cache_frame_0f4073f2f3ee8f4812497a8bc5624d58_2;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_0f4073f2f3ee8f4812497a8bc5624d58_2 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_0f4073f2f3ee8f4812497a8bc5624d58_2 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_1:;
            {
                PyObject *tmp_next_source_1;
                PyObject *tmp_assign_source_4;
                CHECK_OBJECT( tmp_dictcontraction_1__$0 );
                tmp_next_source_1 = tmp_dictcontraction_1__$0;
                tmp_assign_source_4 = ITERATOR_NEXT( tmp_next_source_1 );
                if ( tmp_assign_source_4 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_1;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "oo";
                        exception_lineno = 140;
                        goto try_except_handler_3;
                    }
                }

                {
                    PyObject *old = tmp_dictcontraction_1__iter_value_0;
                    tmp_dictcontraction_1__iter_value_0 = tmp_assign_source_4;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_iter_arg_2;
                CHECK_OBJECT( tmp_dictcontraction_1__iter_value_0 );
                tmp_iter_arg_2 = tmp_dictcontraction_1__iter_value_0;
                tmp_assign_source_5 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_2 );
                if ( tmp_assign_source_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_2 = "oo";
                    goto try_except_handler_4;
                }
                {
                    PyObject *old = tmp_dictcontraction$tuple_unpack_1__source_iter;
                    tmp_dictcontraction$tuple_unpack_1__source_iter = tmp_assign_source_5;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_unpack_1;
                CHECK_OBJECT( tmp_dictcontraction$tuple_unpack_1__source_iter );
                tmp_unpack_1 = tmp_dictcontraction$tuple_unpack_1__source_iter;
                tmp_assign_source_6 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
                if ( tmp_assign_source_6 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_2 = "oo";
                    exception_lineno = 140;
                    goto try_except_handler_5;
                }
                {
                    PyObject *old = tmp_dictcontraction$tuple_unpack_1__element_1;
                    tmp_dictcontraction$tuple_unpack_1__element_1 = tmp_assign_source_6;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_7;
                PyObject *tmp_unpack_2;
                CHECK_OBJECT( tmp_dictcontraction$tuple_unpack_1__source_iter );
                tmp_unpack_2 = tmp_dictcontraction$tuple_unpack_1__source_iter;
                tmp_assign_source_7 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
                if ( tmp_assign_source_7 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_2 = "oo";
                    exception_lineno = 140;
                    goto try_except_handler_5;
                }
                {
                    PyObject *old = tmp_dictcontraction$tuple_unpack_1__element_2;
                    tmp_dictcontraction$tuple_unpack_1__element_2 = tmp_assign_source_7;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_iterator_name_1;
                CHECK_OBJECT( tmp_dictcontraction$tuple_unpack_1__source_iter );
                tmp_iterator_name_1 = tmp_dictcontraction$tuple_unpack_1__source_iter;
                // Check if iterator has left-over elements.
                CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

                tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

                if (likely( tmp_iterator_attempt == NULL ))
                {
                    PyObject *error = GET_ERROR_OCCURRED();

                    if ( error != NULL )
                    {
                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                        {
                            CLEAR_ERROR_OCCURRED();
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_2 = "oo";
                            exception_lineno = 140;
                            goto try_except_handler_5;
                        }
                    }
                }
                else
                {
                    Py_DECREF( tmp_iterator_attempt );

                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_2 = "oo";
                    exception_lineno = 140;
                    goto try_except_handler_5;
                }
            }
            goto try_end_1;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_1 = exception_type;
            exception_keeper_value_1 = exception_value;
            exception_keeper_tb_1 = exception_tb;
            exception_keeper_lineno_1 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_dictcontraction$tuple_unpack_1__source_iter );
            Py_DECREF( tmp_dictcontraction$tuple_unpack_1__source_iter );
            tmp_dictcontraction$tuple_unpack_1__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_1;
            exception_value = exception_keeper_value_1;
            exception_tb = exception_keeper_tb_1;
            exception_lineno = exception_keeper_lineno_1;

            goto try_except_handler_4;
            // End of try:
            try_end_1:;
            goto try_end_2;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_dictcontraction$tuple_unpack_1__element_1 );
            tmp_dictcontraction$tuple_unpack_1__element_1 = NULL;

            Py_XDECREF( tmp_dictcontraction$tuple_unpack_1__element_2 );
            tmp_dictcontraction$tuple_unpack_1__element_2 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto try_except_handler_3;
            // End of try:
            try_end_2:;
            CHECK_OBJECT( (PyObject *)tmp_dictcontraction$tuple_unpack_1__source_iter );
            Py_DECREF( tmp_dictcontraction$tuple_unpack_1__source_iter );
            tmp_dictcontraction$tuple_unpack_1__source_iter = NULL;

            {
                PyObject *tmp_assign_source_8;
                CHECK_OBJECT( tmp_dictcontraction$tuple_unpack_1__element_1 );
                tmp_assign_source_8 = tmp_dictcontraction$tuple_unpack_1__element_1;
                {
                    PyObject *old = outline_0_var_k;
                    outline_0_var_k = tmp_assign_source_8;
                    Py_INCREF( outline_0_var_k );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_dictcontraction$tuple_unpack_1__element_1 );
            tmp_dictcontraction$tuple_unpack_1__element_1 = NULL;

            {
                PyObject *tmp_assign_source_9;
                CHECK_OBJECT( tmp_dictcontraction$tuple_unpack_1__element_2 );
                tmp_assign_source_9 = tmp_dictcontraction$tuple_unpack_1__element_2;
                {
                    PyObject *old = outline_0_var_v;
                    outline_0_var_v = tmp_assign_source_9;
                    Py_INCREF( outline_0_var_v );
                    Py_XDECREF( old );
                }

            }
            Py_XDECREF( tmp_dictcontraction$tuple_unpack_1__element_2 );
            tmp_dictcontraction$tuple_unpack_1__element_2 = NULL;

            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_2;
                PyObject *tmp_args_element_name_1;
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__truncate_obj );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__truncate_obj );
                }

                if ( tmp_mvar_value_2 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_truncate_obj" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 140;
                    type_description_2 = "oo";
                    goto try_except_handler_3;
                }

                tmp_called_name_1 = tmp_mvar_value_2;
                CHECK_OBJECT( outline_0_var_v );
                tmp_args_element_name_1 = outline_0_var_v;
                frame_0f4073f2f3ee8f4812497a8bc5624d58_2->m_frame.f_lineno = 140;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1 };
                    tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
                }

                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_2 = "oo";
                    goto try_except_handler_3;
                }
                CHECK_OBJECT( tmp_dictcontraction_1__contraction );
                tmp_dictset_dict = tmp_dictcontraction_1__contraction;
                CHECK_OBJECT( outline_0_var_k );
                tmp_dictset_key = outline_0_var_k;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 140;
                    type_description_2 = "oo";
                    goto try_except_handler_3;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 140;
                type_description_2 = "oo";
                goto try_except_handler_3;
            }
            goto loop_start_1;
            loop_end_1:;
            CHECK_OBJECT( tmp_dictcontraction_1__contraction );
            tmp_assign_source_1 = tmp_dictcontraction_1__contraction;
            Py_INCREF( tmp_assign_source_1 );
            goto try_return_handler_3;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_7__truncate_obj );
            return NULL;
            // Return handler code:
            try_return_handler_3:;
            CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__$0 );
            Py_DECREF( tmp_dictcontraction_1__$0 );
            tmp_dictcontraction_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__contraction );
            Py_DECREF( tmp_dictcontraction_1__contraction );
            tmp_dictcontraction_1__contraction = NULL;

            Py_XDECREF( tmp_dictcontraction_1__iter_value_0 );
            tmp_dictcontraction_1__iter_value_0 = NULL;

            goto frame_return_exit_2;
            // Exception handler code:
            try_except_handler_3:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__$0 );
            Py_DECREF( tmp_dictcontraction_1__$0 );
            tmp_dictcontraction_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_dictcontraction_1__contraction );
            Py_DECREF( tmp_dictcontraction_1__contraction );
            tmp_dictcontraction_1__contraction = NULL;

            Py_XDECREF( tmp_dictcontraction_1__iter_value_0 );
            tmp_dictcontraction_1__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto frame_exception_exit_2;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_0f4073f2f3ee8f4812497a8bc5624d58_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_1;

            frame_return_exit_2:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_0f4073f2f3ee8f4812497a8bc5624d58_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_2;

            frame_exception_exit_2:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_0f4073f2f3ee8f4812497a8bc5624d58_2 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_0f4073f2f3ee8f4812497a8bc5624d58_2, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_0f4073f2f3ee8f4812497a8bc5624d58_2->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_0f4073f2f3ee8f4812497a8bc5624d58_2, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_0f4073f2f3ee8f4812497a8bc5624d58_2,
                type_description_2,
                outline_0_var_k,
                outline_0_var_v
            );


            // Release cached frame.
            if ( frame_0f4073f2f3ee8f4812497a8bc5624d58_2 == cache_frame_0f4073f2f3ee8f4812497a8bc5624d58_2 )
            {
                Py_DECREF( frame_0f4073f2f3ee8f4812497a8bc5624d58_2 );
            }
            cache_frame_0f4073f2f3ee8f4812497a8bc5624d58_2 = NULL;

            assertFrameObject( frame_0f4073f2f3ee8f4812497a8bc5624d58_2 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_1;

            frame_no_exception_1:;
            goto skip_nested_handling_1;
            nested_frame_exit_1:;
            type_description_1 = "oo";
            goto try_except_handler_2;
            skip_nested_handling_1:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_7__truncate_obj );
            return NULL;
            // Return handler code:
            try_return_handler_2:;
            Py_XDECREF( outline_0_var_k );
            outline_0_var_k = NULL;

            Py_XDECREF( outline_0_var_v );
            outline_0_var_v = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_2:;
            exception_keeper_type_4 = exception_type;
            exception_keeper_value_4 = exception_value;
            exception_keeper_tb_4 = exception_tb;
            exception_keeper_lineno_4 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_0_var_k );
            outline_0_var_k = NULL;

            Py_XDECREF( outline_0_var_v );
            outline_0_var_v = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_4;
            exception_value = exception_keeper_value_4;
            exception_tb = exception_keeper_tb_4;
            exception_lineno = exception_keeper_lineno_4;

            goto outline_exception_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_7__truncate_obj );
            return NULL;
            outline_exception_1:;
            exception_lineno = 140;
            goto frame_exception_exit_1;
            outline_result_1:;
            assert( var_truncated == NULL );
            var_truncated = tmp_assign_source_1;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_isinstance_inst_2;
            PyObject *tmp_isinstance_cls_2;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( var_truncated );
            tmp_called_instance_2 = var_truncated;
            frame_50599b5737e0048448bdec7289af66ba->m_frame.f_lineno = 141;
            tmp_isinstance_inst_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_cells_tuple, 0 ) );

            if ( tmp_isinstance_inst_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_isinstance_cls_2 = (PyObject *)&PyList_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_2, tmp_isinstance_cls_2 );
            Py_DECREF( tmp_isinstance_inst_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 141;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_ass_subvalue_1;
                PyObject *tmp_list_element_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_len_arg_1;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_subscript_name_2;
                PyObject *tmp_ass_subscribed_1;
                PyObject *tmp_ass_subscript_1;
                tmp_left_name_1 = const_str_digest_ab4d7d63411881c04058efe70cf6bfd1;
                CHECK_OBJECT( par_obj );
                tmp_subscribed_name_2 = par_obj;
                tmp_subscript_name_2 = const_str_plain_cells;
                tmp_len_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
                if ( tmp_len_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
                Py_DECREF( tmp_len_arg_1 );
                if ( tmp_right_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_list_element_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_list_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_ass_subvalue_1 = PyList_New( 1 );
                PyList_SET_ITEM( tmp_ass_subvalue_1, 0, tmp_list_element_1 );
                CHECK_OBJECT( var_truncated );
                tmp_ass_subscribed_1 = var_truncated;
                tmp_ass_subscript_1 = const_str_plain_cells;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
                Py_DECREF( tmp_ass_subvalue_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 142;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_2:;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_isinstance_inst_3;
            PyObject *tmp_isinstance_cls_3;
            PyObject *tmp_called_instance_3;
            CHECK_OBJECT( var_truncated );
            tmp_called_instance_3 = var_truncated;
            frame_50599b5737e0048448bdec7289af66ba->m_frame.f_lineno = 143;
            tmp_isinstance_inst_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_str_plain_outputs_tuple, 0 ) );

            if ( tmp_isinstance_inst_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_isinstance_cls_3 = (PyObject *)&PyList_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_3, tmp_isinstance_cls_3 );
            Py_DECREF( tmp_isinstance_inst_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 143;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_ass_subvalue_2;
                PyObject *tmp_list_element_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_len_arg_2;
                PyObject *tmp_subscribed_name_3;
                PyObject *tmp_subscript_name_3;
                PyObject *tmp_ass_subscribed_2;
                PyObject *tmp_ass_subscript_2;
                tmp_left_name_2 = const_str_digest_313dd3cbc4af57bc5f16ba1a71ffd70b;
                CHECK_OBJECT( par_obj );
                tmp_subscribed_name_3 = par_obj;
                tmp_subscript_name_3 = const_str_plain_outputs;
                tmp_len_arg_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
                if ( tmp_len_arg_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 144;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_2 = BUILTIN_LEN( tmp_len_arg_2 );
                Py_DECREF( tmp_len_arg_2 );
                if ( tmp_right_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 144;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_list_element_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_list_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 144;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_ass_subvalue_2 = PyList_New( 1 );
                PyList_SET_ITEM( tmp_ass_subvalue_2, 0, tmp_list_element_2 );
                CHECK_OBJECT( var_truncated );
                tmp_ass_subscribed_2 = var_truncated;
                tmp_ass_subscript_2 = const_str_plain_outputs;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
                Py_DECREF( tmp_ass_subvalue_2 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 144;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_3:;
        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_len_arg_3;
            PyObject *tmp_mvar_value_3;
            CHECK_OBJECT( par_obj );
            tmp_len_arg_3 = par_obj;
            tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_3 );
            if ( tmp_compexpr_left_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 146;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_compexpr_left_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ITEM_LIMIT" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 146;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_compexpr_right_1 = tmp_mvar_value_3;
            tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            Py_DECREF( tmp_compexpr_left_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 146;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_ass_subvalue_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_left_name_4;
                PyObject *tmp_len_arg_4;
                PyObject *tmp_right_name_4;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_ass_subscribed_3;
                PyObject *tmp_ass_subscript_3;
                tmp_left_name_3 = const_str_digest_8cdf6e7379d2e8580522d2a3982783b7;
                CHECK_OBJECT( par_obj );
                tmp_len_arg_4 = par_obj;
                tmp_left_name_4 = BUILTIN_LEN( tmp_len_arg_4 );
                if ( tmp_left_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 147;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );
                }

                if ( tmp_mvar_value_4 == NULL )
                {
                    Py_DECREF( tmp_left_name_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ITEM_LIMIT" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 147;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }

                tmp_right_name_4 = tmp_mvar_value_4;
                tmp_right_name_3 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_4, tmp_right_name_4 );
                Py_DECREF( tmp_left_name_4 );
                if ( tmp_right_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 147;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_ass_subvalue_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_ass_subvalue_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 147;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_truncated );
                tmp_ass_subscribed_3 = var_truncated;
                tmp_ass_subscript_3 = const_str_digest_3501979af1b70861f5e9d6a0f04129bf;
                tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
                Py_DECREF( tmp_ass_subvalue_3 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 147;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_4:;
        }
        CHECK_OBJECT( var_truncated );
        tmp_return_value = var_truncated;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_isinstance_inst_4;
            PyObject *tmp_isinstance_cls_4;
            CHECK_OBJECT( par_obj );
            tmp_isinstance_inst_4 = par_obj;
            tmp_isinstance_cls_4 = (PyObject *)&PyList_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_4, tmp_isinstance_cls_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 149;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assign_source_10;
                // Tried code:
                {
                    PyObject *tmp_assign_source_11;
                    PyObject *tmp_iter_arg_3;
                    PyObject *tmp_subscribed_name_4;
                    PyObject *tmp_subscript_name_4;
                    PyObject *tmp_start_name_2;
                    PyObject *tmp_stop_name_2;
                    PyObject *tmp_mvar_value_5;
                    PyObject *tmp_step_name_2;
                    CHECK_OBJECT( par_obj );
                    tmp_subscribed_name_4 = par_obj;
                    tmp_start_name_2 = Py_None;
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );

                    if (unlikely( tmp_mvar_value_5 == NULL ))
                    {
                        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );
                    }

                    if ( tmp_mvar_value_5 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ITEM_LIMIT" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 150;
                        type_description_1 = "oo";
                        goto try_except_handler_6;
                    }

                    tmp_stop_name_2 = tmp_mvar_value_5;
                    tmp_step_name_2 = Py_None;
                    tmp_subscript_name_4 = MAKE_SLICEOBJ3( tmp_start_name_2, tmp_stop_name_2, tmp_step_name_2 );
                    assert( !(tmp_subscript_name_4 == NULL) );
                    tmp_iter_arg_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
                    Py_DECREF( tmp_subscript_name_4 );
                    if ( tmp_iter_arg_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 150;
                        type_description_1 = "oo";
                        goto try_except_handler_6;
                    }
                    tmp_assign_source_11 = MAKE_ITERATOR( tmp_iter_arg_3 );
                    Py_DECREF( tmp_iter_arg_3 );
                    if ( tmp_assign_source_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 150;
                        type_description_1 = "oo";
                        goto try_except_handler_6;
                    }
                    assert( tmp_listcomp_1__$0 == NULL );
                    tmp_listcomp_1__$0 = tmp_assign_source_11;
                }
                {
                    PyObject *tmp_assign_source_12;
                    tmp_assign_source_12 = PyList_New( 0 );
                    assert( tmp_listcomp_1__contraction == NULL );
                    tmp_listcomp_1__contraction = tmp_assign_source_12;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_07d770b8e9333a18782916368eaaa45b_3, codeobj_07d770b8e9333a18782916368eaaa45b, module_nbformat$validator, sizeof(void *) );
                frame_07d770b8e9333a18782916368eaaa45b_3 = cache_frame_07d770b8e9333a18782916368eaaa45b_3;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_07d770b8e9333a18782916368eaaa45b_3 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_07d770b8e9333a18782916368eaaa45b_3 ) == 2 ); // Frame stack

                // Framed code:
                // Tried code:
                loop_start_2:;
                {
                    PyObject *tmp_next_source_2;
                    PyObject *tmp_assign_source_13;
                    CHECK_OBJECT( tmp_listcomp_1__$0 );
                    tmp_next_source_2 = tmp_listcomp_1__$0;
                    tmp_assign_source_13 = ITERATOR_NEXT( tmp_next_source_2 );
                    if ( tmp_assign_source_13 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_2;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_2 = "o";
                            exception_lineno = 150;
                            goto try_except_handler_7;
                        }
                    }

                    {
                        PyObject *old = tmp_listcomp_1__iter_value_0;
                        tmp_listcomp_1__iter_value_0 = tmp_assign_source_13;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_14;
                    CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                    tmp_assign_source_14 = tmp_listcomp_1__iter_value_0;
                    {
                        PyObject *old = outline_1_var_item;
                        outline_1_var_item = tmp_assign_source_14;
                        Py_INCREF( outline_1_var_item );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_append_list_1;
                    PyObject *tmp_append_value_1;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_mvar_value_6;
                    PyObject *tmp_args_element_name_2;
                    CHECK_OBJECT( tmp_listcomp_1__contraction );
                    tmp_append_list_1 = tmp_listcomp_1__contraction;
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__truncate_obj );

                    if (unlikely( tmp_mvar_value_6 == NULL ))
                    {
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__truncate_obj );
                    }

                    if ( tmp_mvar_value_6 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_truncate_obj" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 150;
                        type_description_2 = "o";
                        goto try_except_handler_7;
                    }

                    tmp_called_name_2 = tmp_mvar_value_6;
                    CHECK_OBJECT( outline_1_var_item );
                    tmp_args_element_name_2 = outline_1_var_item;
                    frame_07d770b8e9333a18782916368eaaa45b_3->m_frame.f_lineno = 150;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_2 };
                        tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                    }

                    if ( tmp_append_value_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 150;
                        type_description_2 = "o";
                        goto try_except_handler_7;
                    }
                    assert( PyList_Check( tmp_append_list_1 ) );
                    tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                    Py_DECREF( tmp_append_value_1 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 150;
                        type_description_2 = "o";
                        goto try_except_handler_7;
                    }
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 150;
                    type_description_2 = "o";
                    goto try_except_handler_7;
                }
                goto loop_start_2;
                loop_end_2:;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_assign_source_10 = tmp_listcomp_1__contraction;
                Py_INCREF( tmp_assign_source_10 );
                goto try_return_handler_7;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_7__truncate_obj );
                return NULL;
                // Return handler code:
                try_return_handler_7:;
                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                goto frame_return_exit_3;
                // Exception handler code:
                try_except_handler_7:;
                exception_keeper_type_5 = exception_type;
                exception_keeper_value_5 = exception_value;
                exception_keeper_tb_5 = exception_tb;
                exception_keeper_lineno_5 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_5;
                exception_value = exception_keeper_value_5;
                exception_tb = exception_keeper_tb_5;
                exception_lineno = exception_keeper_lineno_5;

                goto frame_exception_exit_3;
                // End of try:

#if 0
                RESTORE_FRAME_EXCEPTION( frame_07d770b8e9333a18782916368eaaa45b_3 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_2;

                frame_return_exit_3:;
#if 0
                RESTORE_FRAME_EXCEPTION( frame_07d770b8e9333a18782916368eaaa45b_3 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto try_return_handler_6;

                frame_exception_exit_3:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_07d770b8e9333a18782916368eaaa45b_3 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_07d770b8e9333a18782916368eaaa45b_3, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_07d770b8e9333a18782916368eaaa45b_3->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_07d770b8e9333a18782916368eaaa45b_3, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_07d770b8e9333a18782916368eaaa45b_3,
                    type_description_2,
                    outline_1_var_item
                );


                // Release cached frame.
                if ( frame_07d770b8e9333a18782916368eaaa45b_3 == cache_frame_07d770b8e9333a18782916368eaaa45b_3 )
                {
                    Py_DECREF( frame_07d770b8e9333a18782916368eaaa45b_3 );
                }
                cache_frame_07d770b8e9333a18782916368eaaa45b_3 = NULL;

                assertFrameObject( frame_07d770b8e9333a18782916368eaaa45b_3 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_2;

                frame_no_exception_2:;
                goto skip_nested_handling_2;
                nested_frame_exit_2:;
                type_description_1 = "oo";
                goto try_except_handler_6;
                skip_nested_handling_2:;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_7__truncate_obj );
                return NULL;
                // Return handler code:
                try_return_handler_6:;
                Py_XDECREF( outline_1_var_item );
                outline_1_var_item = NULL;

                goto outline_result_2;
                // Exception handler code:
                try_except_handler_6:;
                exception_keeper_type_6 = exception_type;
                exception_keeper_value_6 = exception_value;
                exception_keeper_tb_6 = exception_tb;
                exception_keeper_lineno_6 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( outline_1_var_item );
                outline_1_var_item = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_6;
                exception_value = exception_keeper_value_6;
                exception_tb = exception_keeper_tb_6;
                exception_lineno = exception_keeper_lineno_6;

                goto outline_exception_2;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_7__truncate_obj );
                return NULL;
                outline_exception_2:;
                exception_lineno = 150;
                goto frame_exception_exit_1;
                outline_result_2:;
                assert( var_truncated == NULL );
                var_truncated = tmp_assign_source_10;
            }
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                PyObject *tmp_len_arg_5;
                PyObject *tmp_mvar_value_7;
                CHECK_OBJECT( par_obj );
                tmp_len_arg_5 = par_obj;
                tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_5 );
                if ( tmp_compexpr_left_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 151;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );
                }

                if ( tmp_mvar_value_7 == NULL )
                {
                    Py_DECREF( tmp_compexpr_left_2 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ITEM_LIMIT" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 151;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }

                tmp_compexpr_right_2 = tmp_mvar_value_7;
                tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                Py_DECREF( tmp_compexpr_left_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 151;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_6;
                }
                else
                {
                    goto branch_no_6;
                }
                branch_yes_6:;
                {
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_source_name_1;
                    PyObject *tmp_call_result_1;
                    PyObject *tmp_args_element_name_3;
                    PyObject *tmp_left_name_5;
                    PyObject *tmp_right_name_5;
                    PyObject *tmp_left_name_6;
                    PyObject *tmp_len_arg_6;
                    PyObject *tmp_right_name_6;
                    PyObject *tmp_mvar_value_8;
                    CHECK_OBJECT( var_truncated );
                    tmp_source_name_1 = var_truncated;
                    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_append );
                    if ( tmp_called_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 152;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    tmp_left_name_5 = const_str_digest_b8d5ba12f59ff311c96a191521fc9587;
                    CHECK_OBJECT( par_obj );
                    tmp_len_arg_6 = par_obj;
                    tmp_left_name_6 = BUILTIN_LEN( tmp_len_arg_6 );
                    if ( tmp_left_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_3 );

                        exception_lineno = 152;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );

                    if (unlikely( tmp_mvar_value_8 == NULL ))
                    {
                        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT );
                    }

                    if ( tmp_mvar_value_8 == NULL )
                    {
                        Py_DECREF( tmp_called_name_3 );
                        Py_DECREF( tmp_left_name_6 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_ITEM_LIMIT" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 152;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }

                    tmp_right_name_6 = tmp_mvar_value_8;
                    tmp_right_name_5 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_6, tmp_right_name_6 );
                    Py_DECREF( tmp_left_name_6 );
                    if ( tmp_right_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_3 );

                        exception_lineno = 152;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    tmp_args_element_name_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                    Py_DECREF( tmp_right_name_5 );
                    if ( tmp_args_element_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_3 );

                        exception_lineno = 152;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    frame_50599b5737e0048448bdec7289af66ba->m_frame.f_lineno = 152;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_3 };
                        tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                    }

                    Py_DECREF( tmp_called_name_3 );
                    Py_DECREF( tmp_args_element_name_3 );
                    if ( tmp_call_result_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 152;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    Py_DECREF( tmp_call_result_1 );
                }
                branch_no_6:;
            }
            CHECK_OBJECT( var_truncated );
            tmp_return_value = var_truncated;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            goto branch_end_5;
            branch_no_5:;
            {
                nuitka_bool tmp_condition_result_7;
                PyObject *tmp_isinstance_inst_5;
                PyObject *tmp_isinstance_cls_5;
                CHECK_OBJECT( par_obj );
                tmp_isinstance_inst_5 = par_obj;
                tmp_isinstance_cls_5 = (PyObject *)&PyUnicode_Type;
                tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_5, tmp_isinstance_cls_5 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 154;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                {
                    PyObject *tmp_assign_source_15;
                    PyObject *tmp_subscribed_name_5;
                    PyObject *tmp_subscript_name_5;
                    PyObject *tmp_start_name_3;
                    PyObject *tmp_stop_name_3;
                    PyObject *tmp_mvar_value_9;
                    PyObject *tmp_step_name_3;
                    CHECK_OBJECT( par_obj );
                    tmp_subscribed_name_5 = par_obj;
                    tmp_start_name_3 = Py_None;
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__STR_LIMIT );

                    if (unlikely( tmp_mvar_value_9 == NULL ))
                    {
                        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__STR_LIMIT );
                    }

                    if ( tmp_mvar_value_9 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_STR_LIMIT" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 155;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }

                    tmp_stop_name_3 = tmp_mvar_value_9;
                    tmp_step_name_3 = Py_None;
                    tmp_subscript_name_5 = MAKE_SLICEOBJ3( tmp_start_name_3, tmp_stop_name_3, tmp_step_name_3 );
                    assert( !(tmp_subscript_name_5 == NULL) );
                    tmp_assign_source_15 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
                    Py_DECREF( tmp_subscript_name_5 );
                    if ( tmp_assign_source_15 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 155;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_truncated == NULL );
                    var_truncated = tmp_assign_source_15;
                }
                {
                    nuitka_bool tmp_condition_result_8;
                    PyObject *tmp_compexpr_left_3;
                    PyObject *tmp_compexpr_right_3;
                    PyObject *tmp_len_arg_7;
                    PyObject *tmp_mvar_value_10;
                    CHECK_OBJECT( par_obj );
                    tmp_len_arg_7 = par_obj;
                    tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_7 );
                    if ( tmp_compexpr_left_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 156;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__STR_LIMIT );

                    if (unlikely( tmp_mvar_value_10 == NULL ))
                    {
                        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__STR_LIMIT );
                    }

                    if ( tmp_mvar_value_10 == NULL )
                    {
                        Py_DECREF( tmp_compexpr_left_3 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_STR_LIMIT" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 156;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }

                    tmp_compexpr_right_3 = tmp_mvar_value_10;
                    tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                    Py_DECREF( tmp_compexpr_left_3 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 156;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_8;
                    }
                    else
                    {
                        goto branch_no_8;
                    }
                    branch_yes_8:;
                    {
                        PyObject *tmp_assign_source_16;
                        PyObject *tmp_left_name_7;
                        PyObject *tmp_right_name_7;
                        CHECK_OBJECT( var_truncated );
                        tmp_left_name_7 = var_truncated;
                        tmp_right_name_7 = const_str_digest_3501979af1b70861f5e9d6a0f04129bf;
                        tmp_result = BINARY_OPERATION_ADD_OBJECT_UNICODE_INPLACE( &tmp_left_name_7, tmp_right_name_7 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 157;
                            type_description_1 = "oo";
                            goto frame_exception_exit_1;
                        }
                        tmp_assign_source_16 = tmp_left_name_7;
                        var_truncated = tmp_assign_source_16;

                    }
                    branch_no_8:;
                }
                CHECK_OBJECT( var_truncated );
                tmp_return_value = var_truncated;
                Py_INCREF( tmp_return_value );
                goto frame_return_exit_1;
                goto branch_end_7;
                branch_no_7:;
                CHECK_OBJECT( par_obj );
                tmp_return_value = par_obj;
                Py_INCREF( tmp_return_value );
                goto frame_return_exit_1;
                branch_end_7:;
            }
            branch_end_5:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_50599b5737e0048448bdec7289af66ba );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_3;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_50599b5737e0048448bdec7289af66ba );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_50599b5737e0048448bdec7289af66ba );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_50599b5737e0048448bdec7289af66ba, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_50599b5737e0048448bdec7289af66ba->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_50599b5737e0048448bdec7289af66ba, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_50599b5737e0048448bdec7289af66ba,
        type_description_1,
        par_obj,
        var_truncated
    );


    // Release cached frame.
    if ( frame_50599b5737e0048448bdec7289af66ba == cache_frame_50599b5737e0048448bdec7289af66ba )
    {
        Py_DECREF( frame_50599b5737e0048448bdec7289af66ba );
    }
    cache_frame_50599b5737e0048448bdec7289af66ba = NULL;

    assertFrameObject( frame_50599b5737e0048448bdec7289af66ba );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_3:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_7__truncate_obj );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    Py_XDECREF( var_truncated );
    var_truncated = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_obj );
    Py_DECREF( par_obj );
    par_obj = NULL;

    Py_XDECREF( var_truncated );
    var_truncated = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_7__truncate_obj );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$validator$$$function_8___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_original = python_pars[ 1 ];
    PyObject *par_ref = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_86ac625832a0fbc6a73ec6019fb2cd70;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_86ac625832a0fbc6a73ec6019fb2cd70 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_86ac625832a0fbc6a73ec6019fb2cd70, codeobj_86ac625832a0fbc6a73ec6019fb2cd70, module_nbformat$validator, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_86ac625832a0fbc6a73ec6019fb2cd70 = cache_frame_86ac625832a0fbc6a73ec6019fb2cd70;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_86ac625832a0fbc6a73ec6019fb2cd70 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_86ac625832a0fbc6a73ec6019fb2cd70 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_original );
        tmp_assattr_name_1 = par_original;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_original, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_getattr_attr_1;
        PyObject *tmp_getattr_default_1;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_getattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_original );
        if ( tmp_getattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_getattr_attr_1 = const_str_plain_ref;
        CHECK_OBJECT( par_ref );
        tmp_getattr_default_1 = par_ref;
        tmp_assattr_name_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
        Py_DECREF( tmp_getattr_target_1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_ref, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 169;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_original );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assattr_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_message );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_message, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_86ac625832a0fbc6a73ec6019fb2cd70 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_86ac625832a0fbc6a73ec6019fb2cd70 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_86ac625832a0fbc6a73ec6019fb2cd70, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_86ac625832a0fbc6a73ec6019fb2cd70->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_86ac625832a0fbc6a73ec6019fb2cd70, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_86ac625832a0fbc6a73ec6019fb2cd70,
        type_description_1,
        par_self,
        par_original,
        par_ref
    );


    // Release cached frame.
    if ( frame_86ac625832a0fbc6a73ec6019fb2cd70 == cache_frame_86ac625832a0fbc6a73ec6019fb2cd70 )
    {
        Py_DECREF( frame_86ac625832a0fbc6a73ec6019fb2cd70 );
    }
    cache_frame_86ac625832a0fbc6a73ec6019fb2cd70 = NULL;

    assertFrameObject( frame_86ac625832a0fbc6a73ec6019fb2cd70 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_8___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_original );
    Py_DECREF( par_original );
    par_original = NULL;

    CHECK_OBJECT( (PyObject *)par_ref );
    Py_DECREF( par_ref );
    par_ref = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_original );
    Py_DECREF( par_original );
    par_original = NULL;

    CHECK_OBJECT( (PyObject *)par_ref );
    Py_DECREF( par_ref );
    par_ref = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_8___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$validator$$$function_9___getattr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_key = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_97501fbef0568d5fd31c3093b607e4be;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_97501fbef0568d5fd31c3093b607e4be = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_97501fbef0568d5fd31c3093b607e4be, codeobj_97501fbef0568d5fd31c3093b607e4be, module_nbformat$validator, sizeof(void *)+sizeof(void *) );
    frame_97501fbef0568d5fd31c3093b607e4be = cache_frame_97501fbef0568d5fd31c3093b607e4be;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_97501fbef0568d5fd31c3093b607e4be );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_97501fbef0568d5fd31c3093b607e4be ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_getattr_attr_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_getattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_original );
        if ( tmp_getattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_key );
        tmp_getattr_attr_1 = par_key;
        tmp_return_value = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        Py_DECREF( tmp_getattr_target_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_97501fbef0568d5fd31c3093b607e4be );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_97501fbef0568d5fd31c3093b607e4be );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_97501fbef0568d5fd31c3093b607e4be );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_97501fbef0568d5fd31c3093b607e4be, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_97501fbef0568d5fd31c3093b607e4be->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_97501fbef0568d5fd31c3093b607e4be, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_97501fbef0568d5fd31c3093b607e4be,
        type_description_1,
        par_self,
        par_key
    );


    // Release cached frame.
    if ( frame_97501fbef0568d5fd31c3093b607e4be == cache_frame_97501fbef0568d5fd31c3093b607e4be )
    {
        Py_DECREF( frame_97501fbef0568d5fd31c3093b607e4be );
    }
    cache_frame_97501fbef0568d5fd31c3093b607e4be = NULL;

    assertFrameObject( frame_97501fbef0568d5fd31c3093b607e4be );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_9___getattr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_key );
    Py_DECREF( par_key );
    par_key = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_key );
    Py_DECREF( par_key );
    par_key = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_9___getattr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$validator$$$function_10___unicode__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_error = NULL;
    PyObject *var_instance = NULL;
    struct Nuitka_FrameObject *frame_7cd870927385df22f14f39c4776aa107;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_7cd870927385df22f14f39c4776aa107 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7cd870927385df22f14f39c4776aa107, codeobj_7cd870927385df22f14f39c4776aa107, module_nbformat$validator, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_7cd870927385df22f14f39c4776aa107 = cache_frame_7cd870927385df22f14f39c4776aa107;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7cd870927385df22f14f39c4776aa107 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7cd870927385df22f14f39c4776aa107 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_original );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 180;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_error == NULL );
        var_error = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__truncate_obj );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__truncate_obj );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_truncate_obj" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_error );
        tmp_source_name_2 = var_error;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_instance );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_7cd870927385df22f14f39c4776aa107->m_frame.f_lineno = 181;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_instance == NULL );
        var_instance = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_list_element_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_5;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_source_name_7;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_8;
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_9;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_kw_name_1;
        tmp_source_name_3 = const_str_newline;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_join );
        assert( !(tmp_called_name_2 == NULL) );
        CHECK_OBJECT( var_error );
        tmp_source_name_4 = var_error;
        tmp_list_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_message );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 184;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = PyList_New( 6 );
        PyList_SET_ITEM( tmp_args_element_name_2, 0, tmp_list_element_1 );
        tmp_list_element_1 = const_str_empty;
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_2, 1, tmp_list_element_1 );
        tmp_left_name_1 = const_str_digest_7c8b18874751383d58d8e1d740744a9d;
        CHECK_OBJECT( var_error );
        tmp_source_name_5 = var_error;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_validator );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 187;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 3 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_or_left_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_ref );
        if ( tmp_or_left_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 188;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_right_name_1 );
            Py_DECREF( tmp_or_left_value_1 );

            exception_lineno = 188;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        Py_DECREF( tmp_or_left_value_1 );
        tmp_or_right_value_1 = const_str_plain_notebook;
        Py_INCREF( tmp_or_right_value_1 );
        tmp_tuple_element_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_tuple_element_1 = tmp_or_left_value_1;
        or_end_1:;
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__format_as_index );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__format_as_index );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_right_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_format_as_index" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 189;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_2;
        CHECK_OBJECT( var_error );
        tmp_source_name_7 = var_error;
        tmp_list_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_relative_schema_path );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 189;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_subscribed_name_1 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 189;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_slice_none_int_neg_1_none;
        tmp_args_element_name_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 189;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_7cd870927385df22f14f39c4776aa107->m_frame.f_lineno = 189;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 189;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
        tmp_list_element_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 186;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_2, 2, tmp_list_element_1 );
        tmp_list_element_1 = const_str_empty;
        Py_INCREF( tmp_list_element_1 );
        PyList_SET_ITEM( tmp_args_element_name_2, 3, tmp_list_element_1 );
        tmp_left_name_2 = const_str_digest_bf957fafa2570246718e247af8a204fc;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__format_as_index );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__format_as_index );
        }

        if ( tmp_mvar_value_3 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_format_as_index" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 191;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_3;
        CHECK_OBJECT( var_error );
        tmp_source_name_8 = var_error;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_relative_path );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 191;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_7cd870927385df22f14f39c4776aa107->m_frame.f_lineno = 191;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_right_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 191;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_list_element_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 191;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_2, 4, tmp_list_element_1 );
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_pprint );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pprint );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pprint" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 192;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_9 = tmp_mvar_value_4;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_pformat );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 192;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_instance );
        tmp_tuple_element_2 = var_instance;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_4327091cb4fc3295df2b2776d735ca47 );
        frame_7cd870927385df22f14f39c4776aa107->m_frame.f_lineno = 192;
        tmp_list_element_1 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );

            exception_lineno = 192;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_args_element_name_2, 5, tmp_list_element_1 );
        frame_7cd870927385df22f14f39c4776aa107->m_frame.f_lineno = 183;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7cd870927385df22f14f39c4776aa107 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7cd870927385df22f14f39c4776aa107 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7cd870927385df22f14f39c4776aa107 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7cd870927385df22f14f39c4776aa107, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7cd870927385df22f14f39c4776aa107->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7cd870927385df22f14f39c4776aa107, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7cd870927385df22f14f39c4776aa107,
        type_description_1,
        par_self,
        var_error,
        var_instance
    );


    // Release cached frame.
    if ( frame_7cd870927385df22f14f39c4776aa107 == cache_frame_7cd870927385df22f14f39c4776aa107 )
    {
        Py_DECREF( frame_7cd870927385df22f14f39c4776aa107 );
    }
    cache_frame_7cd870927385df22f14f39c4776aa107 = NULL;

    assertFrameObject( frame_7cd870927385df22f14f39c4776aa107 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_10___unicode__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_error );
    Py_DECREF( var_error );
    var_error = NULL;

    CHECK_OBJECT( (PyObject *)var_instance );
    Py_DECREF( var_instance );
    var_instance = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_error );
    var_error = NULL;

    Py_XDECREF( var_instance );
    var_instance = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_10___unicode__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$validator$$$function_11_better_validation_error( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_error = python_pars[ 0 ];
    PyObject *par_version = python_pars[ 1 ];
    PyObject *par_version_minor = python_pars[ 2 ];
    PyObject *var_key = NULL;
    PyObject *var_ref = NULL;
    PyObject *var_sub_error = NULL;
    PyObject *var_better = NULL;
    struct Nuitka_FrameObject *frame_2ffdb114606679c9c2c41f9996831a71;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    static struct Nuitka_FrameObject *cache_frame_2ffdb114606679c9c2c41f9996831a71 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2ffdb114606679c9c2c41f9996831a71, codeobj_2ffdb114606679c9c2c41f9996831a71, module_nbformat$validator, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2ffdb114606679c9c2c41f9996831a71 = cache_frame_2ffdb114606679c9c2c41f9996831a71;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2ffdb114606679c9c2c41f9996831a71 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2ffdb114606679c9c2c41f9996831a71 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_error );
        tmp_source_name_1 = par_error;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_schema_path );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 205;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_neg_1;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, -1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 205;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_key == NULL );
        var_key = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = Py_None;
        assert( var_ref == NULL );
        Py_INCREF( tmp_assign_source_2 );
        var_ref = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_key );
        tmp_called_instance_1 = var_key;
        frame_2ffdb114606679c9c2c41f9996831a71->m_frame.f_lineno = 207;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_endswith, &PyTuple_GET_ITEM( const_tuple_str_plain_Of_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 207;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_error );
            tmp_source_name_2 = par_error;
            tmp_isinstance_inst_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_instance );
            if ( tmp_isinstance_inst_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 209;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_isinstance_cls_1 = (PyObject *)&PyDict_Type;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            Py_DECREF( tmp_isinstance_inst_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 209;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_1;
                PyObject *tmp_compexpr_right_1;
                PyObject *tmp_source_name_3;
                tmp_compexpr_left_1 = const_str_plain_cell_type;
                CHECK_OBJECT( par_error );
                tmp_source_name_3 = par_error;
                tmp_compexpr_right_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_instance );
                if ( tmp_compexpr_right_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 210;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
                Py_DECREF( tmp_compexpr_right_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 210;
                    type_description_1 = "ooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_3;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_subscribed_name_2;
                    PyObject *tmp_source_name_4;
                    PyObject *tmp_subscript_name_2;
                    PyObject *tmp_right_name_1;
                    CHECK_OBJECT( par_error );
                    tmp_source_name_4 = par_error;
                    tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_instance );
                    if ( tmp_subscribed_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 211;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_subscript_name_2 = const_str_plain_cell_type;
                    tmp_left_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
                    Py_DECREF( tmp_subscribed_name_2 );
                    if ( tmp_left_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 211;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_right_name_1 = const_str_plain__cell;
                    tmp_assign_source_3 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_1, tmp_right_name_1 );
                    Py_DECREF( tmp_left_name_1 );
                    if ( tmp_assign_source_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 211;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = var_ref;
                        assert( old != NULL );
                        var_ref = tmp_assign_source_3;
                        Py_DECREF( old );
                    }

                }
                goto branch_end_3;
                branch_no_3:;
                {
                    nuitka_bool tmp_condition_result_4;
                    PyObject *tmp_compexpr_left_2;
                    PyObject *tmp_compexpr_right_2;
                    PyObject *tmp_source_name_5;
                    tmp_compexpr_left_2 = const_str_plain_output_type;
                    CHECK_OBJECT( par_error );
                    tmp_source_name_5 = par_error;
                    tmp_compexpr_right_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_instance );
                    if ( tmp_compexpr_right_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 212;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
                    Py_DECREF( tmp_compexpr_right_2 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 212;
                        type_description_1 = "ooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    {
                        PyObject *tmp_assign_source_4;
                        PyObject *tmp_subscribed_name_3;
                        PyObject *tmp_source_name_6;
                        PyObject *tmp_subscript_name_3;
                        CHECK_OBJECT( par_error );
                        tmp_source_name_6 = par_error;
                        tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_instance );
                        if ( tmp_subscribed_name_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 213;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_subscript_name_3 = const_str_plain_output_type;
                        tmp_assign_source_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
                        Py_DECREF( tmp_subscribed_name_3 );
                        if ( tmp_assign_source_4 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 213;
                            type_description_1 = "ooooooo";
                            goto frame_exception_exit_1;
                        }
                        {
                            PyObject *old = var_ref;
                            assert( old != NULL );
                            var_ref = tmp_assign_source_4;
                            Py_DECREF( old );
                        }

                    }
                    branch_no_4:;
                }
                branch_end_3:;
            }
            branch_no_2:;
        }
        {
            nuitka_bool tmp_condition_result_5;
            int tmp_truth_name_2;
            if ( var_ref == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ref" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 215;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }

            tmp_truth_name_2 = CHECK_IF_TRUE( var_ref );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 215;
                type_description_1 = "ooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            // Tried code:
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_source_name_7;
                PyObject *tmp_kw_name_1;
                PyObject *tmp_dict_key_1;
                PyObject *tmp_dict_value_1;
                PyObject *tmp_dict_key_2;
                PyObject *tmp_dict_value_2;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_validate );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_validate );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "validate" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 217;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_2;
                }

                tmp_called_name_1 = tmp_mvar_value_1;
                CHECK_OBJECT( par_error );
                tmp_source_name_7 = par_error;
                tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_instance );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 217;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_2;
                }
                tmp_args_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
                if ( var_ref == NULL )
                {
                    Py_DECREF( tmp_args_name_1 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ref" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 218;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_2;
                }

                tmp_tuple_element_1 = var_ref;
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
                tmp_dict_key_1 = const_str_plain_version;
                CHECK_OBJECT( par_version );
                tmp_dict_value_1 = par_version;
                tmp_kw_name_1 = _PyDict_NewPresized( 2 );
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
                assert( !(tmp_res != 0) );
                tmp_dict_key_2 = const_str_plain_version_minor;
                CHECK_OBJECT( par_version_minor );
                tmp_dict_value_2 = par_version_minor;
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
                assert( !(tmp_res != 0) );
                frame_2ffdb114606679c9c2c41f9996831a71->m_frame.f_lineno = 217;
                tmp_call_result_2 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 217;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            goto try_end_1;
            // Exception handler code:
            try_except_handler_2:;
            exception_keeper_type_1 = exception_type;
            exception_keeper_value_1 = exception_value;
            exception_keeper_tb_1 = exception_tb;
            exception_keeper_lineno_1 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Preserve existing published exception.
            exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_type_1 );
            exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_value_1 );
            exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( exception_preserved_tb_1 );

            if ( exception_keeper_tb_1 == NULL )
            {
                exception_keeper_tb_1 = MAKE_TRACEBACK( frame_2ffdb114606679c9c2c41f9996831a71, exception_keeper_lineno_1 );
            }
            else if ( exception_keeper_lineno_1 != 0 )
            {
                exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_2ffdb114606679c9c2c41f9996831a71, exception_keeper_lineno_1 );
            }

            NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
            PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
            PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_6;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                PyObject *tmp_mvar_value_2;
                tmp_compexpr_left_3 = EXC_TYPE(PyThreadState_GET());
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_ValidationError );

                if (unlikely( tmp_mvar_value_2 == NULL ))
                {
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ValidationError );
                }

                if ( tmp_mvar_value_2 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ValidationError" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 222;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_3;
                }

                tmp_compexpr_right_3 = tmp_mvar_value_2;
                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 222;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_3;
                }
                tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_6;
                }
                else
                {
                    goto branch_no_6;
                }
                branch_yes_6:;
                {
                    PyObject *tmp_assign_source_5;
                    tmp_assign_source_5 = EXC_VALUE(PyThreadState_GET());
                    assert( var_sub_error == NULL );
                    Py_INCREF( tmp_assign_source_5 );
                    var_sub_error = tmp_assign_source_5;
                }
                // Tried code:
                {
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_source_name_8;
                    PyObject *tmp_source_name_9;
                    PyObject *tmp_call_result_3;
                    PyObject *tmp_args_element_name_1;
                    PyObject *tmp_source_name_10;
                    CHECK_OBJECT( par_error );
                    tmp_source_name_9 = par_error;
                    tmp_source_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_relative_path );
                    if ( tmp_source_name_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 224;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_4;
                    }
                    tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_extend );
                    Py_DECREF( tmp_source_name_8 );
                    if ( tmp_called_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 224;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_4;
                    }
                    CHECK_OBJECT( var_sub_error );
                    tmp_source_name_10 = var_sub_error;
                    tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_relative_path );
                    if ( tmp_args_element_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_2 );

                        exception_lineno = 224;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_4;
                    }
                    frame_2ffdb114606679c9c2c41f9996831a71->m_frame.f_lineno = 224;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_1 };
                        tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
                    }

                    Py_DECREF( tmp_called_name_2 );
                    Py_DECREF( tmp_args_element_name_1 );
                    if ( tmp_call_result_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 224;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_4;
                    }
                    Py_DECREF( tmp_call_result_3 );
                }
                {
                    PyObject *tmp_assattr_name_1;
                    PyObject *tmp_source_name_11;
                    PyObject *tmp_assattr_target_1;
                    CHECK_OBJECT( par_error );
                    tmp_source_name_11 = par_error;
                    tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_relative_path );
                    if ( tmp_assattr_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 225;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_4;
                    }
                    CHECK_OBJECT( var_sub_error );
                    tmp_assattr_target_1 = var_sub_error;
                    tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_relative_path, tmp_assattr_name_1 );
                    Py_DECREF( tmp_assattr_name_1 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 225;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_4;
                    }
                }
                {
                    PyObject *tmp_assign_source_6;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_mvar_value_3;
                    PyObject *tmp_args_element_name_2;
                    PyObject *tmp_args_element_name_3;
                    PyObject *tmp_args_element_name_4;
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_better_validation_error );

                    if (unlikely( tmp_mvar_value_3 == NULL ))
                    {
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_better_validation_error );
                    }

                    if ( tmp_mvar_value_3 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "better_validation_error" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 226;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_4;
                    }

                    tmp_called_name_3 = tmp_mvar_value_3;
                    CHECK_OBJECT( var_sub_error );
                    tmp_args_element_name_2 = var_sub_error;
                    CHECK_OBJECT( par_version );
                    tmp_args_element_name_3 = par_version;
                    CHECK_OBJECT( par_version_minor );
                    tmp_args_element_name_4 = par_version_minor;
                    frame_2ffdb114606679c9c2c41f9996831a71->m_frame.f_lineno = 226;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                        tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
                    }

                    if ( tmp_assign_source_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 226;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_4;
                    }
                    assert( var_better == NULL );
                    var_better = tmp_assign_source_6;
                }
                {
                    nuitka_bool tmp_condition_result_7;
                    PyObject *tmp_compexpr_left_4;
                    PyObject *tmp_compexpr_right_4;
                    PyObject *tmp_source_name_12;
                    CHECK_OBJECT( var_better );
                    tmp_source_name_12 = var_better;
                    tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_ref );
                    if ( tmp_compexpr_left_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 227;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_4;
                    }
                    tmp_compexpr_right_4 = Py_None;
                    tmp_condition_result_7 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    Py_DECREF( tmp_compexpr_left_4 );
                    if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_7;
                    }
                    else
                    {
                        goto branch_no_7;
                    }
                    branch_yes_7:;
                    {
                        PyObject *tmp_assattr_name_2;
                        PyObject *tmp_assattr_target_2;
                        if ( var_ref == NULL )
                        {

                            exception_type = PyExc_UnboundLocalError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ref" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 228;
                            type_description_1 = "ooooooo";
                            goto try_except_handler_4;
                        }

                        tmp_assattr_name_2 = var_ref;
                        CHECK_OBJECT( var_better );
                        tmp_assattr_target_2 = var_better;
                        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_ref, tmp_assattr_name_2 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 228;
                            type_description_1 = "ooooooo";
                            goto try_except_handler_4;
                        }
                    }
                    branch_no_7:;
                }
                CHECK_OBJECT( var_better );
                tmp_return_value = var_better;
                Py_INCREF( tmp_return_value );
                goto try_return_handler_4;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_11_better_validation_error );
                return NULL;
                // Return handler code:
                try_return_handler_4:;
                Py_XDECREF( var_sub_error );
                var_sub_error = NULL;

                goto try_return_handler_3;
                // Exception handler code:
                try_except_handler_4:;
                exception_keeper_type_2 = exception_type;
                exception_keeper_value_2 = exception_value;
                exception_keeper_tb_2 = exception_tb;
                exception_keeper_lineno_2 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( var_sub_error );
                var_sub_error = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_2;
                exception_value = exception_keeper_value_2;
                exception_tb = exception_keeper_tb_2;
                exception_lineno = exception_keeper_lineno_2;

                goto try_except_handler_3;
                // End of try:
                goto branch_end_6;
                branch_no_6:;
                {
                    nuitka_bool tmp_condition_result_8;
                    PyObject *tmp_operand_name_1;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    tmp_compexpr_left_5 = EXC_TYPE(PyThreadState_GET());
                    tmp_compexpr_right_5 = PyExc_Exception;
                    tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 230;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 230;
                        type_description_1 = "ooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_condition_result_8 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_8;
                    }
                    else
                    {
                        goto branch_no_8;
                    }
                    branch_yes_8:;
                    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    if (unlikely( tmp_result == false ))
                    {
                        exception_lineno = 216;
                    }

                    if (exception_tb && exception_tb->tb_frame == &frame_2ffdb114606679c9c2c41f9996831a71->m_frame) frame_2ffdb114606679c9c2c41f9996831a71->m_frame.f_lineno = exception_tb->tb_lineno;
                    type_description_1 = "ooooooo";
                    goto try_except_handler_3;
                    branch_no_8:;
                }
                branch_end_6:;
            }
            goto try_end_2;
            // Return handler code:
            try_return_handler_3:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            goto frame_return_exit_1;
            // Exception handler code:
            try_except_handler_3:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto frame_exception_exit_1;
            // End of try:
            try_end_2:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            goto try_end_1;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_11_better_validation_error );
            return NULL;
            // End of try:
            try_end_1:;
            branch_no_5:;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_NotebookValidationError );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NotebookValidationError );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NotebookValidationError" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 234;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_4;
        CHECK_OBJECT( par_error );
        tmp_args_element_name_5 = par_error;
        if ( var_ref == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "ref" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 234;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_6 = var_ref;
        frame_2ffdb114606679c9c2c41f9996831a71->m_frame.f_lineno = 234;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 234;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ffdb114606679c9c2c41f9996831a71 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ffdb114606679c9c2c41f9996831a71 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2ffdb114606679c9c2c41f9996831a71 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2ffdb114606679c9c2c41f9996831a71, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2ffdb114606679c9c2c41f9996831a71->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2ffdb114606679c9c2c41f9996831a71, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2ffdb114606679c9c2c41f9996831a71,
        type_description_1,
        par_error,
        par_version,
        par_version_minor,
        var_key,
        var_ref,
        var_sub_error,
        var_better
    );


    // Release cached frame.
    if ( frame_2ffdb114606679c9c2c41f9996831a71 == cache_frame_2ffdb114606679c9c2c41f9996831a71 )
    {
        Py_DECREF( frame_2ffdb114606679c9c2c41f9996831a71 );
    }
    cache_frame_2ffdb114606679c9c2c41f9996831a71 = NULL;

    assertFrameObject( frame_2ffdb114606679c9c2c41f9996831a71 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_11_better_validation_error );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_error );
    Py_DECREF( par_error );
    par_error = NULL;

    CHECK_OBJECT( (PyObject *)par_version );
    Py_DECREF( par_version );
    par_version = NULL;

    CHECK_OBJECT( (PyObject *)par_version_minor );
    Py_DECREF( par_version_minor );
    par_version_minor = NULL;

    CHECK_OBJECT( (PyObject *)var_key );
    Py_DECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_ref );
    var_ref = NULL;

    Py_XDECREF( var_sub_error );
    var_sub_error = NULL;

    Py_XDECREF( var_better );
    var_better = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_error );
    Py_DECREF( par_error );
    par_error = NULL;

    CHECK_OBJECT( (PyObject *)par_version );
    Py_DECREF( par_version );
    par_version = NULL;

    CHECK_OBJECT( (PyObject *)par_version_minor );
    Py_DECREF( par_version_minor );
    par_version_minor = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_ref );
    var_ref = NULL;

    Py_XDECREF( var_sub_error );
    var_sub_error = NULL;

    Py_XDECREF( var_better );
    var_better = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_11_better_validation_error );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_nbformat$validator$$$function_12_validate( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_nbjson = python_pars[ 0 ];
    PyObject *par_ref = python_pars[ 1 ];
    PyObject *par_version = python_pars[ 2 ];
    PyObject *par_version_minor = python_pars[ 3 ];
    PyObject *par_relax_add_props = python_pars[ 4 ];
    PyObject *var_get_version = NULL;
    PyObject *var_validator = NULL;
    PyObject *var_e = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    struct Nuitka_FrameObject *frame_80acff7fb0ecbb9c3057078e8ba1256b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    bool tmp_result;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_80acff7fb0ecbb9c3057078e8ba1256b = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_80acff7fb0ecbb9c3057078e8ba1256b, codeobj_80acff7fb0ecbb9c3057078e8ba1256b, module_nbformat$validator, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_80acff7fb0ecbb9c3057078e8ba1256b = cache_frame_80acff7fb0ecbb9c3057078e8ba1256b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_80acff7fb0ecbb9c3057078e8ba1256b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_80acff7fb0ecbb9c3057078e8ba1256b ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_version );
        tmp_compexpr_left_1 = par_version;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_import_name_from_1;
            PyObject *tmp_name_name_1;
            PyObject *tmp_globals_name_1;
            PyObject *tmp_locals_name_1;
            PyObject *tmp_fromlist_name_1;
            PyObject *tmp_level_name_1;
            tmp_name_name_1 = const_str_plain_reader;
            tmp_globals_name_1 = (PyObject *)moduledict_nbformat$validator;
            tmp_locals_name_1 = Py_None;
            tmp_fromlist_name_1 = const_tuple_str_plain_get_version_tuple;
            tmp_level_name_1 = const_int_pos_1;
            frame_80acff7fb0ecbb9c3057078e8ba1256b->m_frame.f_lineno = 244;
            tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
            if ( tmp_import_name_from_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 244;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            if ( PyModule_Check( tmp_import_name_from_1 ) )
            {
               tmp_assign_source_1 = IMPORT_NAME_OR_MODULE(
                    tmp_import_name_from_1,
                    (PyObject *)moduledict_nbformat$validator,
                    const_str_plain_get_version,
                    const_int_pos_1
                );
            }
            else
            {
               tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_get_version );
            }

            Py_DECREF( tmp_import_name_from_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 244;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_get_version == NULL );
            var_get_version = tmp_assign_source_1;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var_get_version );
            tmp_called_name_1 = var_get_version;
            CHECK_OBJECT( par_nbjson );
            tmp_args_element_name_1 = par_nbjson;
            frame_80acff7fb0ecbb9c3057078e8ba1256b->m_frame.f_lineno = 245;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 245;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_2 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 245;
                type_description_1 = "oooooooo";
                goto try_except_handler_2;
            }
            assert( tmp_tuple_unpack_1__source_iter == NULL );
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_2;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_unpack_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_3 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
            if ( tmp_assign_source_3 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooo";
                exception_lineno = 245;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_1 == NULL );
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_unpack_2;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
            tmp_assign_source_4 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
            if ( tmp_assign_source_4 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooo";
                exception_lineno = 245;
                goto try_except_handler_3;
            }
            assert( tmp_tuple_unpack_1__element_2 == NULL );
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_4;
        }
        {
            PyObject *tmp_iterator_name_1;
            CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
            tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "oooooooo";
                        exception_lineno = 245;
                        goto try_except_handler_3;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooooooo";
                exception_lineno = 245;
                goto try_except_handler_3;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
        Py_DECREF( tmp_tuple_unpack_1__source_iter );
        tmp_tuple_unpack_1__source_iter = NULL;

        {
            PyObject *tmp_assign_source_5;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
            tmp_assign_source_5 = tmp_tuple_unpack_1__element_1;
            {
                PyObject *old = par_version;
                assert( old != NULL );
                par_version = tmp_assign_source_5;
                Py_INCREF( par_version );
                Py_DECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_1__element_1 );
        tmp_tuple_unpack_1__element_1 = NULL;

        {
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
            tmp_assign_source_6 = tmp_tuple_unpack_1__element_2;
            {
                PyObject *old = par_version_minor;
                assert( old != NULL );
                par_version_minor = tmp_assign_source_6;
                Py_INCREF( par_version_minor );
                Py_DECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_1__element_2 );
        tmp_tuple_unpack_1__element_2 = NULL;

        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_get_validator );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_validator );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_validator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 247;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( par_version );
        tmp_tuple_element_1 = par_version;
        tmp_args_name_1 = PyTuple_New( 2 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_version_minor );
        tmp_tuple_element_1 = par_version_minor;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_relax_add_props;
        CHECK_OBJECT( par_relax_add_props );
        tmp_dict_value_1 = par_relax_add_props;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_80acff7fb0ecbb9c3057078e8ba1256b->m_frame.f_lineno = 247;
        tmp_assign_source_7 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_validator == NULL );
        var_validator = tmp_assign_source_7;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_validator );
        tmp_compexpr_left_2 = var_validator;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_mvar_value_3;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_warnings );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_warnings );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "warnings" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 251;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_1 = tmp_mvar_value_2;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_warn );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 251;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_left_name_1 = const_str_digest_46cae9a15e0fb3f2d59b6d514eaa1119;
            CHECK_OBJECT( par_version );
            tmp_right_name_1 = par_version;
            tmp_args_element_name_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_3 );

                exception_lineno = 251;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_UserWarning );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_UserWarning );
            }

            if ( tmp_mvar_value_3 == NULL )
            {
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_args_element_name_2 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "UserWarning" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 251;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_3 = tmp_mvar_value_3;
            frame_80acff7fb0ecbb9c3057078e8ba1256b->m_frame.f_lineno = 251;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 251;
                type_description_1 = "oooooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_2:;
    }
    // Tried code:
    {
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_ref );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_ref );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 255;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_4;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_2;
            CHECK_OBJECT( var_validator );
            tmp_source_name_2 = var_validator;
            tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_validate );
            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 256;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            CHECK_OBJECT( par_nbjson );
            tmp_args_element_name_4 = par_nbjson;
            tmp_dict_key_2 = const_str_digest_99b2b9518bc27b61b80729fcb0aefd4f;
            tmp_left_name_2 = const_str_digest_3ba7abdd6423f8828eb25e55f9612f02;
            CHECK_OBJECT( par_ref );
            tmp_right_name_2 = par_ref;
            tmp_dict_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
            if ( tmp_dict_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 256;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            tmp_args_element_name_5 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_args_element_name_5, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            frame_80acff7fb0ecbb9c3057078e8ba1256b->m_frame.f_lineno = 256;
            {
                PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 256;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_args_element_name_6;
            CHECK_OBJECT( var_validator );
            tmp_called_instance_1 = var_validator;
            CHECK_OBJECT( par_nbjson );
            tmp_args_element_name_6 = par_nbjson;
            frame_80acff7fb0ecbb9c3057078e8ba1256b->m_frame.f_lineno = 258;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_return_value = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_validate, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 258;
                type_description_1 = "oooooooo";
                goto try_except_handler_4;
            }
            goto frame_return_exit_1;
        }
        branch_end_3:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_12_validate );
    return NULL;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_3 == NULL )
    {
        exception_keeper_tb_3 = MAKE_TRACEBACK( frame_80acff7fb0ecbb9c3057078e8ba1256b, exception_keeper_lineno_3 );
    }
    else if ( exception_keeper_lineno_3 != 0 )
    {
        exception_keeper_tb_3 = ADD_TRACEBACK( exception_keeper_tb_3, frame_80acff7fb0ecbb9c3057078e8ba1256b, exception_keeper_lineno_3 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    PyException_SetTraceback( exception_keeper_value_3, (PyObject *)exception_keeper_tb_3 );
    PUBLISH_EXCEPTION( &exception_keeper_type_3, &exception_keeper_value_3, &exception_keeper_tb_3 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        PyObject *tmp_mvar_value_4;
        tmp_compexpr_left_3 = EXC_TYPE(PyThreadState_GET());
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_ValidationError );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ValidationError );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ValidationError" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 259;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }

        tmp_compexpr_right_3 = tmp_mvar_value_4;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_3, tmp_compexpr_right_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_8;
            tmp_assign_source_8 = EXC_VALUE(PyThreadState_GET());
            assert( var_e == NULL );
            Py_INCREF( tmp_assign_source_8 );
            var_e = tmp_assign_source_8;
        }
        // Tried code:
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_args_element_name_9;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_better_validation_error );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_better_validation_error );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "better_validation_error" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 260;
                type_description_1 = "oooooooo";
                goto try_except_handler_6;
            }

            tmp_called_name_5 = tmp_mvar_value_5;
            CHECK_OBJECT( var_e );
            tmp_args_element_name_7 = var_e;
            CHECK_OBJECT( par_version );
            tmp_args_element_name_8 = par_version;
            CHECK_OBJECT( par_version_minor );
            tmp_args_element_name_9 = par_version_minor;
            frame_80acff7fb0ecbb9c3057078e8ba1256b->m_frame.f_lineno = 260;
            {
                PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_5, call_args );
            }

            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 260;
                type_description_1 = "oooooooo";
                goto try_except_handler_6;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 260;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oooooooo";
            goto try_except_handler_6;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_12_validate );
        return NULL;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_e );
        var_e = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        goto branch_end_4;
        branch_no_4:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 254;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_80acff7fb0ecbb9c3057078e8ba1256b->m_frame) frame_80acff7fb0ecbb9c3057078e8ba1256b->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooooo";
        goto try_except_handler_5;
        branch_end_4:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_12_validate );
    return NULL;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_80acff7fb0ecbb9c3057078e8ba1256b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_80acff7fb0ecbb9c3057078e8ba1256b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_80acff7fb0ecbb9c3057078e8ba1256b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_80acff7fb0ecbb9c3057078e8ba1256b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_80acff7fb0ecbb9c3057078e8ba1256b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_80acff7fb0ecbb9c3057078e8ba1256b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_80acff7fb0ecbb9c3057078e8ba1256b,
        type_description_1,
        par_nbjson,
        par_ref,
        par_version,
        par_version_minor,
        par_relax_add_props,
        var_get_version,
        var_validator,
        var_e
    );


    // Release cached frame.
    if ( frame_80acff7fb0ecbb9c3057078e8ba1256b == cache_frame_80acff7fb0ecbb9c3057078e8ba1256b )
    {
        Py_DECREF( frame_80acff7fb0ecbb9c3057078e8ba1256b );
    }
    cache_frame_80acff7fb0ecbb9c3057078e8ba1256b = NULL;

    assertFrameObject( frame_80acff7fb0ecbb9c3057078e8ba1256b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_12_validate );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_nbjson );
    Py_DECREF( par_nbjson );
    par_nbjson = NULL;

    CHECK_OBJECT( (PyObject *)par_ref );
    Py_DECREF( par_ref );
    par_ref = NULL;

    CHECK_OBJECT( (PyObject *)par_version );
    Py_DECREF( par_version );
    par_version = NULL;

    CHECK_OBJECT( (PyObject *)par_version_minor );
    Py_DECREF( par_version_minor );
    par_version_minor = NULL;

    CHECK_OBJECT( (PyObject *)par_relax_add_props );
    Py_DECREF( par_relax_add_props );
    par_relax_add_props = NULL;

    Py_XDECREF( var_get_version );
    var_get_version = NULL;

    CHECK_OBJECT( (PyObject *)var_validator );
    Py_DECREF( var_validator );
    var_validator = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_nbjson );
    Py_DECREF( par_nbjson );
    par_nbjson = NULL;

    CHECK_OBJECT( (PyObject *)par_ref );
    Py_DECREF( par_ref );
    par_ref = NULL;

    Py_XDECREF( par_version );
    par_version = NULL;

    Py_XDECREF( par_version_minor );
    par_version_minor = NULL;

    CHECK_OBJECT( (PyObject *)par_relax_add_props );
    Py_DECREF( par_relax_add_props );
    par_relax_add_props = NULL;

    Py_XDECREF( var_get_version );
    var_get_version = NULL;

    Py_XDECREF( var_validator );
    var_validator = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( nbformat$validator$$$function_12_validate );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_10___unicode__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_10___unicode__,
        const_str_plain___unicode__,
#if PYTHON_VERSION >= 300
        const_str_digest_e2fe51ca39323cfa3982965f7518090b,
#endif
        codeobj_7cd870927385df22f14f39c4776aa107,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        const_str_digest_b92312044731484797159e6bd08ff1bf,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_11_better_validation_error(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_11_better_validation_error,
        const_str_plain_better_validation_error,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2ffdb114606679c9c2c41f9996831a71,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        const_str_digest_6add32c35304927ffbff5a4594fe611a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_12_validate( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_12_validate,
        const_str_plain_validate,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_80acff7fb0ecbb9c3057078e8ba1256b,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        const_str_digest_d58d968678b6839a6825319f681d2032,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_1__relax_additional_properties(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_1__relax_additional_properties,
        const_str_plain__relax_additional_properties,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d4a68d445da39bc494ea5548053ffb3c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        const_str_digest_4480aceac7f9a1ea050b506f25fc3746,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_2__allow_undefined(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_2__allow_undefined,
        const_str_plain__allow_undefined,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9938aee43aa0f3502e63636a194a4acb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_3_get_validator( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_3_get_validator,
        const_str_plain_get_validator,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ae0e53b0d21be0bf143aa79b66326c88,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        const_str_digest_f5960f053c799842b8e0ed714ea133f9,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_4__get_schema_json(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_4__get_schema_json,
        const_str_plain__get_schema_json,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_575fedaf3fe165e75d8b92029ab5ae29,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        const_str_digest_64b1f97bf44235012b982cf7b16efef1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_5_isvalid( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_5_isvalid,
        const_str_plain_isvalid,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a4ace2a051dff874cfe8b840f1be8cab,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        const_str_digest_ee4fb7f34081bbea5fd2f72e8362a336,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_6__format_as_index(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_6__format_as_index,
        const_str_plain__format_as_index,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5f3d662285e0b4833cd1e6ebec7cd9ce,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        const_str_digest_26e92d503609cc5c01254f14e323f90f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_7__truncate_obj(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_7__truncate_obj,
        const_str_plain__truncate_obj,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_50599b5737e0048448bdec7289af66ba,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        const_str_digest_4f8b32c6b411603474e36b16db170008,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_8___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_8___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_1ce647b6b40d3803cab42709ed4c2933,
#endif
        codeobj_86ac625832a0fbc6a73ec6019fb2cd70,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_nbformat$validator$$$function_9___getattr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_nbformat$validator$$$function_9___getattr__,
        const_str_plain___getattr__,
#if PYTHON_VERSION >= 300
        const_str_digest_05e6cbb831921041a48eb20404075cc5,
#endif
        codeobj_97501fbef0568d5fd31c3093b607e4be,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_nbformat$validator,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_nbformat$validator =
{
    PyModuleDef_HEAD_INIT,
    "nbformat.validator",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(nbformat$validator)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(nbformat$validator)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_nbformat$validator );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("nbformat.validator: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbformat.validator: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("nbformat.validator: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initnbformat$validator" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_nbformat$validator = Py_InitModule4(
        "nbformat.validator",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_nbformat$validator = PyModule_Create( &mdef_nbformat$validator );
#endif

    moduledict_nbformat$validator = MODULE_DICT( module_nbformat$validator );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_nbformat$validator,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_nbformat$validator,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbformat$validator,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_nbformat$validator,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_nbformat$validator );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_acb2a3ac556b7ea554f9b7aef19c0e4d, module_nbformat$validator );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    struct Nuitka_FrameObject *frame_6a814c4d623439bf68cd6e1ac172ae65;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_nbformat$validator_162 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_f9b61aaf579eeba5ba64e674c2300a71_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_f9b61aaf579eeba5ba64e674c2300a71_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_6a814c4d623439bf68cd6e1ac172ae65 = MAKE_MODULE_FRAME( codeobj_6a814c4d623439bf68cd6e1ac172ae65, module_nbformat$validator );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_6a814c4d623439bf68cd6e1ac172ae65 );
    assert( Py_REFCNT( frame_6a814c4d623439bf68cd6e1ac172ae65 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 4;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_print_function );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_print_function, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_json;
        tmp_globals_name_1 = (PyObject *)moduledict_nbformat$validator;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 5;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_json, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_os;
        tmp_globals_name_2 = (PyObject *)moduledict_nbformat$validator;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 6;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_os, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_pprint;
        tmp_globals_name_3 = (PyObject *)moduledict_nbformat$validator;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = Py_None;
        tmp_level_name_3 = const_int_0;
        frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 7;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_pprint, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_sys;
        tmp_globals_name_4 = (PyObject *)moduledict_nbformat$validator;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 8;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        assert( !(tmp_assign_source_8 == NULL) );
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_warnings;
        tmp_globals_name_5 = (PyObject *)moduledict_nbformat$validator;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 9;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_warnings, tmp_assign_source_9 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_jsonschema;
        tmp_globals_name_6 = (PyObject *)moduledict_nbformat$validator;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_ValidationError_tuple;
        tmp_level_name_6 = const_int_0;
        frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 12;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_ValidationError );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_ValidationError, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_plain_jsonschema;
        tmp_globals_name_7 = (PyObject *)moduledict_nbformat$validator;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_Draft4Validator_tuple;
        tmp_level_name_7 = const_int_0;
        frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 13;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_Draft4Validator );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_Validator, tmp_assign_source_11 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_6a814c4d623439bf68cd6e1ac172ae65, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_6a814c4d623439bf68cd6e1ac172ae65, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_12;
            tmp_assign_source_12 = EXC_VALUE(PyThreadState_GET());
            UPDATE_STRING_DICT0( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_e, tmp_assign_source_12 );
        }
        {
            PyObject *tmp_assign_source_13;
            tmp_assign_source_13 = const_str_digest_84a0fe848149a514cbfb0fdd8d1fa2ed;
            UPDATE_STRING_DICT0( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_verbose_msg, tmp_assign_source_13 );
        }
        // Tried code:
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_unicode_arg_1;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_right_name_1;
            PyObject *tmp_mvar_value_4;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_e );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_e );
            }

            CHECK_OBJECT( tmp_mvar_value_3 );
            tmp_unicode_arg_1 = tmp_mvar_value_3;
            tmp_left_name_1 = PyObject_Unicode( tmp_unicode_arg_1 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;

                goto try_except_handler_3;
            }
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_verbose_msg );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_verbose_msg );
            }

            if ( tmp_mvar_value_4 == NULL )
            {
                Py_DECREF( tmp_left_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "verbose_msg" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 23;

                goto try_except_handler_3;
            }

            tmp_right_name_1 = tmp_mvar_value_4;
            tmp_make_exception_arg_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;

                goto try_except_handler_3;
            }
            frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 23;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_ImportError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 23;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );

            goto try_except_handler_3;
        }
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$validator );
        return MOD_RETURN_VALUE( NULL );
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        tmp_res = PyDict_DelItem( (PyObject *)moduledict_nbformat$validator, const_str_plain_e );
        if ( tmp_res == -1 ) CLEAR_ERROR_OCCURRED();

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_2;
        // End of try:
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 11;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame) frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_2;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( nbformat$validator );
    return MOD_RETURN_VALUE( NULL );
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_b1de2f4e7d17f11eaeb3062e52aa9890;
        tmp_globals_name_8 = (PyObject *)moduledict_nbformat$validator;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_import_item_tuple;
        tmp_level_name_8 = const_int_0;
        frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 25;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_import_item );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_import_item, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = PyDict_New();
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_validators, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_nbformat$validator$$$function_1__relax_additional_properties(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__relax_additional_properties, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_nbformat$validator$$$function_2__allow_undefined(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__allow_undefined, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_none_false_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_18 = MAKE_FUNCTION_nbformat$validator$$$function_3_get_validator( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_get_validator, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_nbformat$validator$$$function_4__get_schema_json(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__get_schema_json, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_defaults_2;
        tmp_defaults_2 = const_tuple_none_none_none_tuple;
        Py_INCREF( tmp_defaults_2 );
        tmp_assign_source_20 = MAKE_FUNCTION_nbformat$validator$$$function_5_isvalid( tmp_defaults_2 );



        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_isvalid, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = MAKE_FUNCTION_nbformat$validator$$$function_6__format_as_index(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__format_as_index, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = const_int_pos_16;
        UPDATE_STRING_DICT0( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__ITEM_LIMIT, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = const_int_pos_64;
        UPDATE_STRING_DICT0( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__STR_LIMIT, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_nbformat$validator$$$function_7__truncate_obj(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain__truncate_obj, tmp_assign_source_24 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_ValidationError );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ValidationError );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ValidationError" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;

            goto try_except_handler_4;
        }

        tmp_tuple_element_1 = tmp_mvar_value_5;
        tmp_assign_source_25 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_assign_source_25, 0, tmp_tuple_element_1 );
        assert( tmp_class_creation_1__bases_orig == NULL );
        tmp_class_creation_1__bases_orig = tmp_assign_source_25;
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_dircall_arg1_1;
        CHECK_OBJECT( tmp_class_creation_1__bases_orig );
        tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_26 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_26;
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_27;
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_3;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_4;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_4;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_4;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_4;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_4;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_28 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_4;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_28;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_4;
        }
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_4;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;

                goto try_except_handler_4;
            }
            tmp_tuple_element_2 = const_str_plain_NotebookValidationError;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_2 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 162;
            tmp_assign_source_29 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;

                goto try_except_handler_4;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_29;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;

                goto try_except_handler_4;
            }
            tmp_condition_result_6 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_3;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 162;

                    goto try_except_handler_4;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_3 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 162;

                    goto try_except_handler_4;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_3 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 162;

                    goto try_except_handler_4;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 162;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_4;
            }
            branch_no_4:;
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assign_source_30;
            tmp_assign_source_30 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_30;
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assign_source_31;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_nbformat$validator_162 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_acb2a3ac556b7ea554f9b7aef19c0e4d;
        tmp_res = PyObject_SetItem( locals_nbformat$validator_162, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_digest_3db58559bb6e158db1119a1cd969836d;
        tmp_res = PyObject_SetItem( locals_nbformat$validator_162, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_6;
        }
        tmp_dictset_value = const_str_plain_NotebookValidationError;
        tmp_res = PyObject_SetItem( locals_nbformat$validator_162, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;

            goto try_except_handler_6;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_f9b61aaf579eeba5ba64e674c2300a71_2, codeobj_f9b61aaf579eeba5ba64e674c2300a71, module_nbformat$validator, sizeof(void *) );
        frame_f9b61aaf579eeba5ba64e674c2300a71_2 = cache_frame_f9b61aaf579eeba5ba64e674c2300a71_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_f9b61aaf579eeba5ba64e674c2300a71_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_f9b61aaf579eeba5ba64e674c2300a71_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_defaults_3;
            tmp_defaults_3 = const_tuple_none_tuple;
            Py_INCREF( tmp_defaults_3 );
            tmp_dictset_value = MAKE_FUNCTION_nbformat$validator$$$function_8___init__( tmp_defaults_3 );



            tmp_res = PyObject_SetItem( locals_nbformat$validator_162, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 167;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_nbformat$validator$$$function_9___getattr__(  );



        tmp_res = PyObject_SetItem( locals_nbformat$validator_162, const_str_plain___getattr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_nbformat$validator$$$function_10___unicode__(  );



        tmp_res = PyObject_SetItem( locals_nbformat$validator_162, const_str_plain___unicode__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 175;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_mvar_value_6;
            tmp_source_name_5 = PyObject_GetItem( locals_nbformat$validator_162, const_str_plain_sys );

            if ( tmp_source_name_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_sys );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 195;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_source_name_5 = tmp_mvar_value_6;
                Py_INCREF( tmp_source_name_5 );
                }
            }

            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_version_info );
            Py_DECREF( tmp_source_name_5 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 195;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_compexpr_right_2 = const_tuple_int_pos_3_tuple;
            tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 195;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            tmp_dictset_value = PyObject_GetItem( locals_nbformat$validator_162, const_str_plain___unicode__ );

            if ( tmp_dictset_value == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "__unicode__" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 196;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_nbformat$validator_162, const_str_plain___str__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            branch_no_5:;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_f9b61aaf579eeba5ba64e674c2300a71_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_f9b61aaf579eeba5ba64e674c2300a71_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_f9b61aaf579eeba5ba64e674c2300a71_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_f9b61aaf579eeba5ba64e674c2300a71_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_f9b61aaf579eeba5ba64e674c2300a71_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_f9b61aaf579eeba5ba64e674c2300a71_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_f9b61aaf579eeba5ba64e674c2300a71_2 == cache_frame_f9b61aaf579eeba5ba64e674c2300a71_2 )
        {
            Py_DECREF( frame_f9b61aaf579eeba5ba64e674c2300a71_2 );
        }
        cache_frame_f9b61aaf579eeba5ba64e674c2300a71_2 = NULL;

        assertFrameObject( frame_f9b61aaf579eeba5ba64e674c2300a71_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_6;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_3 = tmp_class_creation_1__bases;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_compexpr_right_3 = tmp_class_creation_1__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;

                goto try_except_handler_6;
            }
            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dictset_value = tmp_class_creation_1__bases_orig;
            tmp_res = PyObject_SetItem( locals_nbformat$validator_162, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;

                goto try_except_handler_6;
            }
            branch_no_6:;
        }
        {
            PyObject *tmp_assign_source_32;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_NotebookValidationError;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_nbformat$validator_162;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame.f_lineno = 162;
            tmp_assign_source_32 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 162;

                goto try_except_handler_6;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_32;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_31 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_31 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$validator );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        Py_DECREF( locals_nbformat$validator_162 );
        locals_nbformat$validator_162 = NULL;
        goto try_return_handler_5;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_nbformat$validator_162 );
        locals_nbformat$validator_162 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto try_except_handler_5;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( nbformat$validator );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_5:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( nbformat$validator );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 162;
        goto try_except_handler_4;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_NotebookValidationError, tmp_assign_source_31 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a814c4d623439bf68cd6e1ac172ae65 );
#endif
    popFrameStack();

    assertFrameObject( frame_6a814c4d623439bf68cd6e1ac172ae65 );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6a814c4d623439bf68cd6e1ac172ae65 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6a814c4d623439bf68cd6e1ac172ae65, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6a814c4d623439bf68cd6e1ac172ae65->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6a814c4d623439bf68cd6e1ac172ae65, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
    Py_DECREF( tmp_class_creation_1__bases_orig );
    tmp_class_creation_1__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_33;
        tmp_assign_source_33 = MAKE_FUNCTION_nbformat$validator$$$function_11_better_validation_error(  );



        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_better_validation_error, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_defaults_4;
        tmp_defaults_4 = const_tuple_none_none_none_false_tuple;
        Py_INCREF( tmp_defaults_4 );
        tmp_assign_source_34 = MAKE_FUNCTION_nbformat$validator$$$function_12_validate( tmp_defaults_4 );



        UPDATE_STRING_DICT1( moduledict_nbformat$validator, (Nuitka_StringObject *)const_str_plain_validate, tmp_assign_source_34 );
    }

    return MOD_RETURN_VALUE( module_nbformat$validator );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
