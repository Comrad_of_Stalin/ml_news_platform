/* Generated code for Python module 'prompt_toolkit.input.defaults'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$input$defaults" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$input$defaults;
PyDictObject *moduledict_prompt_toolkit$input$defaults;

/* The declarations of module constants used, if any. */
extern PyObject *const_tuple_str_plain_value_str_plain_app_tuple;
static PyObject *const_str_digest_2bb71600ed34042e348761c337418f71;
extern PyObject *const_tuple_none_tuple;
static PyObject *const_str_plain_win32_pipe;
extern PyObject *const_str_plain_set;
extern PyObject *const_str_digest_722e5e8c7f53b34663d9669ca7ddc056;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_digest_8cff9047d0926d732c79d282e1f19e8e;
extern PyObject *const_str_plain_sys;
extern PyObject *const_tuple_str_plain_input_tuple;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain___all__;
static PyObject *const_tuple_str_plain_Win32PipeInput_tuple;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_3e3208174f7d10b86ea9779a4c67f7e4;
extern PyObject *const_tuple_str_plain_TaskLocal_str_plain_TaskLocalNotSetError_tuple;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_Win32PipeInput;
static PyObject *const_str_plain_create_pipe_input;
static PyObject *const_list_72311564eb8ddb5bedb2d42b9a1124a8_list;
static PyObject *const_str_digest_8175dc6570324e08d2818e1021dc0e76;
static PyObject *const_tuple_str_plain_PosixPipeInput_tuple;
extern PyObject *const_int_0;
extern PyObject *const_dict_2f0bd481556f74b4a5355abeb2791423;
extern PyObject *const_str_plain_get_default_input;
extern PyObject *const_str_plain_Input;
extern PyObject *const_str_plain_value;
extern PyObject *const_tuple_str_plain_Vt100Input_tuple;
extern PyObject *const_str_plain_get_app;
extern PyObject *const_str_plain_TaskLocal;
extern PyObject *const_str_plain_is_windows;
static PyObject *const_tuple_str_plain_stdin_str_plain_Win32Input_str_plain_Vt100Input_tuple;
static PyObject *const_str_plain_posix_pipe;
extern PyObject *const_str_plain_stdin;
extern PyObject *const_str_plain_PosixPipeInput;
extern PyObject *const_str_plain_origin;
extern PyObject *const_tuple_str_plain_get_app_tuple;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_ad8facaa99e9308761cd3fc35f13c443;
static PyObject *const_str_plain__default_input;
extern PyObject *const_str_plain_input;
extern PyObject *const_str_plain_Win32Input;
extern PyObject *const_str_plain_create_input;
extern PyObject *const_tuple_empty;
static PyObject *const_tuple_str_plain_Win32Input_tuple;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_TaskLocalNotSetError;
extern PyObject *const_str_digest_15810eaf43342c5627475c0bf384673e;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_tuple_str_plain_is_windows_tuple;
extern PyObject *const_str_plain_base;
extern PyObject *const_str_digest_cf6a2fbed33a79df3055b39a946503a7;
extern PyObject *const_str_plain_vt100;
static PyObject *const_tuple_str_plain_Win32PipeInput_str_plain_PosixPipeInput_tuple;
extern PyObject *const_str_plain_return_none;
extern PyObject *const_str_plain_Vt100Input;
static PyObject *const_str_digest_981fa8417d2e7a53279e174b202ed0e2;
extern PyObject *const_str_plain_set_default_input;
extern PyObject *const_tuple_str_plain_Input_tuple;
extern PyObject *const_str_plain_app;
extern PyObject *const_str_plain_win32;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___debug__;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_2bb71600ed34042e348761c337418f71 = UNSTREAM_STRING_ASCII( &constant_bin[ 4665292 ], 92, 0 );
    const_str_plain_win32_pipe = UNSTREAM_STRING_ASCII( &constant_bin[ 4665384 ], 10, 1 );
    const_tuple_str_plain_Win32PipeInput_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Win32PipeInput_tuple, 0, const_str_plain_Win32PipeInput ); Py_INCREF( const_str_plain_Win32PipeInput );
    const_str_digest_3e3208174f7d10b86ea9779a4c67f7e4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4665394 ], 32, 0 );
    const_str_plain_create_pipe_input = UNSTREAM_STRING_ASCII( &constant_bin[ 4665426 ], 17, 1 );
    const_list_72311564eb8ddb5bedb2d42b9a1124a8_list = PyList_New( 4 );
    PyList_SET_ITEM( const_list_72311564eb8ddb5bedb2d42b9a1124a8_list, 0, const_str_plain_create_input ); Py_INCREF( const_str_plain_create_input );
    PyList_SET_ITEM( const_list_72311564eb8ddb5bedb2d42b9a1124a8_list, 1, const_str_plain_create_pipe_input ); Py_INCREF( const_str_plain_create_pipe_input );
    PyList_SET_ITEM( const_list_72311564eb8ddb5bedb2d42b9a1124a8_list, 2, const_str_plain_get_default_input ); Py_INCREF( const_str_plain_get_default_input );
    PyList_SET_ITEM( const_list_72311564eb8ddb5bedb2d42b9a1124a8_list, 3, const_str_plain_set_default_input ); Py_INCREF( const_str_plain_set_default_input );
    const_str_digest_8175dc6570324e08d2818e1021dc0e76 = UNSTREAM_STRING_ASCII( &constant_bin[ 4665443 ], 75, 0 );
    const_tuple_str_plain_PosixPipeInput_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PosixPipeInput_tuple, 0, const_str_plain_PosixPipeInput ); Py_INCREF( const_str_plain_PosixPipeInput );
    const_tuple_str_plain_stdin_str_plain_Win32Input_str_plain_Vt100Input_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_stdin_str_plain_Win32Input_str_plain_Vt100Input_tuple, 0, const_str_plain_stdin ); Py_INCREF( const_str_plain_stdin );
    PyTuple_SET_ITEM( const_tuple_str_plain_stdin_str_plain_Win32Input_str_plain_Vt100Input_tuple, 1, const_str_plain_Win32Input ); Py_INCREF( const_str_plain_Win32Input );
    PyTuple_SET_ITEM( const_tuple_str_plain_stdin_str_plain_Win32Input_str_plain_Vt100Input_tuple, 2, const_str_plain_Vt100Input ); Py_INCREF( const_str_plain_Vt100Input );
    const_str_plain_posix_pipe = UNSTREAM_STRING_ASCII( &constant_bin[ 4665518 ], 10, 1 );
    const_str_digest_ad8facaa99e9308761cd3fc35f13c443 = UNSTREAM_STRING_ASCII( &constant_bin[ 4665528 ], 38, 0 );
    const_str_plain__default_input = UNSTREAM_STRING_ASCII( &constant_bin[ 4665566 ], 14, 1 );
    const_tuple_str_plain_Win32Input_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Win32Input_tuple, 0, const_str_plain_Win32Input ); Py_INCREF( const_str_plain_Win32Input );
    const_tuple_str_plain_Win32PipeInput_str_plain_PosixPipeInput_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_Win32PipeInput_str_plain_PosixPipeInput_tuple, 0, const_str_plain_Win32PipeInput ); Py_INCREF( const_str_plain_Win32PipeInput );
    PyTuple_SET_ITEM( const_tuple_str_plain_Win32PipeInput_str_plain_PosixPipeInput_tuple, 1, const_str_plain_PosixPipeInput ); Py_INCREF( const_str_plain_PosixPipeInput );
    const_str_digest_981fa8417d2e7a53279e174b202ed0e2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4665580 ], 132, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$input$defaults( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_05c1ca9cd5e0fa451d5da663a55a8d32;
static PyCodeObject *codeobj_4c36dd999259b5151ff6d64ceaf57900;
static PyCodeObject *codeobj_ec71f767328cf5d84ec689e7a8fcd95d;
static PyCodeObject *codeobj_0b78a049b4e8e806f417067cc34e5b58;
static PyCodeObject *codeobj_1e0d7ae2a062d0d270d1614a18223df7;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_3e3208174f7d10b86ea9779a4c67f7e4 );
    codeobj_05c1ca9cd5e0fa451d5da663a55a8d32 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_ad8facaa99e9308761cd3fc35f13c443, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_4c36dd999259b5151ff6d64ceaf57900 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_create_input, 16, const_tuple_str_plain_stdin_str_plain_Win32Input_str_plain_Vt100Input_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_ec71f767328cf5d84ec689e7a8fcd95d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_create_pipe_input, 27, const_tuple_str_plain_Win32PipeInput_str_plain_PosixPipeInput_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0b78a049b4e8e806f417067cc34e5b58 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_default_input, 43, const_tuple_str_plain_value_str_plain_app_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1e0d7ae2a062d0d270d1614a18223df7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_default_input, 65, const_tuple_str_plain_input_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_1_create_input( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_2_create_pipe_input(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_3_get_default_input(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_4_set_default_input(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$input$defaults$$$function_1_create_input( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_stdin = python_pars[ 0 ];
    PyObject *var_Win32Input = NULL;
    PyObject *var_Vt100Input = NULL;
    struct Nuitka_FrameObject *frame_4c36dd999259b5151ff6d64ceaf57900;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_4c36dd999259b5151ff6d64ceaf57900 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4c36dd999259b5151ff6d64ceaf57900, codeobj_4c36dd999259b5151ff6d64ceaf57900, module_prompt_toolkit$input$defaults, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_4c36dd999259b5151ff6d64ceaf57900 = cache_frame_4c36dd999259b5151ff6d64ceaf57900;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4c36dd999259b5151ff6d64ceaf57900 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4c36dd999259b5151ff6d64ceaf57900 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_stdin );
        tmp_or_left_value_1 = par_stdin;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_sys );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_sys );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "sys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 17;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_or_right_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_stdin );
        if ( tmp_or_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_assign_source_1 = tmp_or_left_value_1;
        or_end_1:;
        {
            PyObject *old = par_stdin;
            assert( old != NULL );
            par_stdin = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_is_windows );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_windows );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_windows" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 19;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        frame_4c36dd999259b5151ff6d64ceaf57900->m_frame.f_lineno = 19;
        tmp_call_result_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 19;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_import_name_from_1;
            PyObject *tmp_name_name_1;
            PyObject *tmp_globals_name_1;
            PyObject *tmp_locals_name_1;
            PyObject *tmp_fromlist_name_1;
            PyObject *tmp_level_name_1;
            tmp_name_name_1 = const_str_plain_win32;
            tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$input$defaults;
            tmp_locals_name_1 = Py_None;
            tmp_fromlist_name_1 = const_tuple_str_plain_Win32Input_tuple;
            tmp_level_name_1 = const_int_pos_1;
            frame_4c36dd999259b5151ff6d64ceaf57900->m_frame.f_lineno = 20;
            tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
            if ( tmp_import_name_from_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            if ( PyModule_Check( tmp_import_name_from_1 ) )
            {
               tmp_assign_source_2 = IMPORT_NAME_OR_MODULE(
                    tmp_import_name_from_1,
                    (PyObject *)moduledict_prompt_toolkit$input$defaults,
                    const_str_plain_Win32Input,
                    const_int_pos_1
                );
            }
            else
            {
               tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Win32Input );
            }

            Py_DECREF( tmp_import_name_from_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( var_Win32Input == NULL );
            var_Win32Input = tmp_assign_source_2;
        }
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( var_Win32Input );
            tmp_called_name_2 = var_Win32Input;
            CHECK_OBJECT( par_stdin );
            tmp_args_element_name_1 = par_stdin;
            frame_4c36dd999259b5151ff6d64ceaf57900->m_frame.f_lineno = 21;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_import_name_from_2;
            PyObject *tmp_name_name_2;
            PyObject *tmp_globals_name_2;
            PyObject *tmp_locals_name_2;
            PyObject *tmp_fromlist_name_2;
            PyObject *tmp_level_name_2;
            tmp_name_name_2 = const_str_plain_vt100;
            tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$input$defaults;
            tmp_locals_name_2 = Py_None;
            tmp_fromlist_name_2 = const_tuple_str_plain_Vt100Input_tuple;
            tmp_level_name_2 = const_int_pos_1;
            frame_4c36dd999259b5151ff6d64ceaf57900->m_frame.f_lineno = 23;
            tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
            if ( tmp_import_name_from_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            if ( PyModule_Check( tmp_import_name_from_2 ) )
            {
               tmp_assign_source_3 = IMPORT_NAME_OR_MODULE(
                    tmp_import_name_from_2,
                    (PyObject *)moduledict_prompt_toolkit$input$defaults,
                    const_str_plain_Vt100Input,
                    const_int_pos_1
                );
            }
            else
            {
               tmp_assign_source_3 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Vt100Input );
            }

            Py_DECREF( tmp_import_name_from_2 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 23;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( var_Vt100Input == NULL );
            var_Vt100Input = tmp_assign_source_3;
        }
        {
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( var_Vt100Input );
            tmp_called_name_3 = var_Vt100Input;
            CHECK_OBJECT( par_stdin );
            tmp_args_element_name_2 = par_stdin;
            frame_4c36dd999259b5151ff6d64ceaf57900->m_frame.f_lineno = 24;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 24;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4c36dd999259b5151ff6d64ceaf57900 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4c36dd999259b5151ff6d64ceaf57900 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4c36dd999259b5151ff6d64ceaf57900 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4c36dd999259b5151ff6d64ceaf57900, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4c36dd999259b5151ff6d64ceaf57900->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4c36dd999259b5151ff6d64ceaf57900, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4c36dd999259b5151ff6d64ceaf57900,
        type_description_1,
        par_stdin,
        var_Win32Input,
        var_Vt100Input
    );


    // Release cached frame.
    if ( frame_4c36dd999259b5151ff6d64ceaf57900 == cache_frame_4c36dd999259b5151ff6d64ceaf57900 )
    {
        Py_DECREF( frame_4c36dd999259b5151ff6d64ceaf57900 );
    }
    cache_frame_4c36dd999259b5151ff6d64ceaf57900 = NULL;

    assertFrameObject( frame_4c36dd999259b5151ff6d64ceaf57900 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$defaults$$$function_1_create_input );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_stdin );
    Py_DECREF( par_stdin );
    par_stdin = NULL;

    Py_XDECREF( var_Win32Input );
    var_Win32Input = NULL;

    Py_XDECREF( var_Vt100Input );
    var_Vt100Input = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_stdin );
    Py_DECREF( par_stdin );
    par_stdin = NULL;

    Py_XDECREF( var_Win32Input );
    var_Win32Input = NULL;

    Py_XDECREF( var_Vt100Input );
    var_Vt100Input = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$defaults$$$function_1_create_input );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$input$defaults$$$function_2_create_pipe_input( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_Win32PipeInput = NULL;
    PyObject *var_PosixPipeInput = NULL;
    struct Nuitka_FrameObject *frame_ec71f767328cf5d84ec689e7a8fcd95d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_ec71f767328cf5d84ec689e7a8fcd95d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ec71f767328cf5d84ec689e7a8fcd95d, codeobj_ec71f767328cf5d84ec689e7a8fcd95d, module_prompt_toolkit$input$defaults, sizeof(void *)+sizeof(void *) );
    frame_ec71f767328cf5d84ec689e7a8fcd95d = cache_frame_ec71f767328cf5d84ec689e7a8fcd95d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ec71f767328cf5d84ec689e7a8fcd95d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ec71f767328cf5d84ec689e7a8fcd95d ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_is_windows );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_windows );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_windows" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 32;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_ec71f767328cf5d84ec689e7a8fcd95d->m_frame.f_lineno = 32;
        tmp_call_result_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 32;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_import_name_from_1;
            PyObject *tmp_name_name_1;
            PyObject *tmp_globals_name_1;
            PyObject *tmp_locals_name_1;
            PyObject *tmp_fromlist_name_1;
            PyObject *tmp_level_name_1;
            tmp_name_name_1 = const_str_plain_win32_pipe;
            tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$input$defaults;
            tmp_locals_name_1 = Py_None;
            tmp_fromlist_name_1 = const_tuple_str_plain_Win32PipeInput_tuple;
            tmp_level_name_1 = const_int_pos_1;
            frame_ec71f767328cf5d84ec689e7a8fcd95d->m_frame.f_lineno = 33;
            tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
            if ( tmp_import_name_from_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 33;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            if ( PyModule_Check( tmp_import_name_from_1 ) )
            {
               tmp_assign_source_1 = IMPORT_NAME_OR_MODULE(
                    tmp_import_name_from_1,
                    (PyObject *)moduledict_prompt_toolkit$input$defaults,
                    const_str_plain_Win32PipeInput,
                    const_int_pos_1
                );
            }
            else
            {
               tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_Win32PipeInput );
            }

            Py_DECREF( tmp_import_name_from_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 33;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( var_Win32PipeInput == NULL );
            var_Win32PipeInput = tmp_assign_source_1;
        }
        {
            PyObject *tmp_called_name_2;
            CHECK_OBJECT( var_Win32PipeInput );
            tmp_called_name_2 = var_Win32PipeInput;
            frame_ec71f767328cf5d84ec689e7a8fcd95d->m_frame.f_lineno = 34;
            tmp_return_value = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 34;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_import_name_from_2;
            PyObject *tmp_name_name_2;
            PyObject *tmp_globals_name_2;
            PyObject *tmp_locals_name_2;
            PyObject *tmp_fromlist_name_2;
            PyObject *tmp_level_name_2;
            tmp_name_name_2 = const_str_plain_posix_pipe;
            tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$input$defaults;
            tmp_locals_name_2 = Py_None;
            tmp_fromlist_name_2 = const_tuple_str_plain_PosixPipeInput_tuple;
            tmp_level_name_2 = const_int_pos_1;
            frame_ec71f767328cf5d84ec689e7a8fcd95d->m_frame.f_lineno = 36;
            tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
            if ( tmp_import_name_from_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            if ( PyModule_Check( tmp_import_name_from_2 ) )
            {
               tmp_assign_source_2 = IMPORT_NAME_OR_MODULE(
                    tmp_import_name_from_2,
                    (PyObject *)moduledict_prompt_toolkit$input$defaults,
                    const_str_plain_PosixPipeInput,
                    const_int_pos_1
                );
            }
            else
            {
               tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_PosixPipeInput );
            }

            Py_DECREF( tmp_import_name_from_2 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( var_PosixPipeInput == NULL );
            var_PosixPipeInput = tmp_assign_source_2;
        }
        {
            PyObject *tmp_called_name_3;
            CHECK_OBJECT( var_PosixPipeInput );
            tmp_called_name_3 = var_PosixPipeInput;
            frame_ec71f767328cf5d84ec689e7a8fcd95d->m_frame.f_lineno = 37;
            tmp_return_value = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec71f767328cf5d84ec689e7a8fcd95d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec71f767328cf5d84ec689e7a8fcd95d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec71f767328cf5d84ec689e7a8fcd95d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ec71f767328cf5d84ec689e7a8fcd95d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ec71f767328cf5d84ec689e7a8fcd95d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ec71f767328cf5d84ec689e7a8fcd95d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ec71f767328cf5d84ec689e7a8fcd95d,
        type_description_1,
        var_Win32PipeInput,
        var_PosixPipeInput
    );


    // Release cached frame.
    if ( frame_ec71f767328cf5d84ec689e7a8fcd95d == cache_frame_ec71f767328cf5d84ec689e7a8fcd95d )
    {
        Py_DECREF( frame_ec71f767328cf5d84ec689e7a8fcd95d );
    }
    cache_frame_ec71f767328cf5d84ec689e7a8fcd95d = NULL;

    assertFrameObject( frame_ec71f767328cf5d84ec689e7a8fcd95d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$defaults$$$function_2_create_pipe_input );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_Win32PipeInput );
    var_Win32PipeInput = NULL;

    Py_XDECREF( var_PosixPipeInput );
    var_PosixPipeInput = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_Win32PipeInput );
    var_Win32PipeInput = NULL;

    Py_XDECREF( var_PosixPipeInput );
    var_PosixPipeInput = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$defaults$$$function_2_create_pipe_input );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$input$defaults$$$function_3_get_default_input( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_value = NULL;
    PyObject *var_app = NULL;
    struct Nuitka_FrameObject *frame_0b78a049b4e8e806f417067cc34e5b58;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_0b78a049b4e8e806f417067cc34e5b58 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0b78a049b4e8e806f417067cc34e5b58, codeobj_0b78a049b4e8e806f417067cc34e5b58, module_prompt_toolkit$input$defaults, sizeof(void *)+sizeof(void *) );
    frame_0b78a049b4e8e806f417067cc34e5b58 = cache_frame_0b78a049b4e8e806f417067cc34e5b58;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0b78a049b4e8e806f417067cc34e5b58 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0b78a049b4e8e806f417067cc34e5b58 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain__default_input );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__default_input );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_default_input" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 51;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_0b78a049b4e8e806f417067cc34e5b58->m_frame.f_lineno = 51;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( var_value == NULL );
        var_value = tmp_assign_source_1;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_0b78a049b4e8e806f417067cc34e5b58, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_0b78a049b4e8e806f417067cc34e5b58, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_2;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_TaskLocalNotSetError );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TaskLocalNotSetError );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TaskLocalNotSetError" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 52;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_2;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_kw_name_1;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_get_app );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 56;
                type_description_1 = "oo";
                goto try_except_handler_3;
            }

            tmp_called_name_1 = tmp_mvar_value_3;
            tmp_kw_name_1 = PyDict_Copy( const_dict_2f0bd481556f74b4a5355abeb2791423 );
            frame_0b78a049b4e8e806f417067cc34e5b58->m_frame.f_lineno = 56;
            tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 56;
                type_description_1 = "oo";
                goto try_except_handler_3;
            }
            assert( var_app == NULL );
            var_app = tmp_assign_source_2;
        }
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_truth_name_1;
            CHECK_OBJECT( var_app );
            tmp_truth_name_1 = CHECK_IF_TRUE( var_app );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 57;
                type_description_1 = "oo";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_source_name_1;
                CHECK_OBJECT( var_app );
                tmp_source_name_1 = var_app;
                tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_input );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 58;
                    type_description_1 = "oo";
                    goto try_except_handler_3;
                }
                goto try_return_handler_3;
            }
            branch_no_2:;
        }
        {
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_4;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_create_input );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_create_input );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "create_input" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 60;
                type_description_1 = "oo";
                goto try_except_handler_3;
            }

            tmp_called_name_2 = tmp_mvar_value_4;
            frame_0b78a049b4e8e806f417067cc34e5b58->m_frame.f_lineno = 60;
            tmp_return_value = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_1 = "oo";
                goto try_except_handler_3;
            }
            goto try_return_handler_3;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 50;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_0b78a049b4e8e806f417067cc34e5b58->m_frame) frame_0b78a049b4e8e806f417067cc34e5b58->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oo";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$defaults$$$function_3_get_default_input );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto frame_return_exit_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0b78a049b4e8e806f417067cc34e5b58 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0b78a049b4e8e806f417067cc34e5b58 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0b78a049b4e8e806f417067cc34e5b58 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0b78a049b4e8e806f417067cc34e5b58, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0b78a049b4e8e806f417067cc34e5b58->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0b78a049b4e8e806f417067cc34e5b58, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0b78a049b4e8e806f417067cc34e5b58,
        type_description_1,
        var_value,
        var_app
    );


    // Release cached frame.
    if ( frame_0b78a049b4e8e806f417067cc34e5b58 == cache_frame_0b78a049b4e8e806f417067cc34e5b58 )
    {
        Py_DECREF( frame_0b78a049b4e8e806f417067cc34e5b58 );
    }
    cache_frame_0b78a049b4e8e806f417067cc34e5b58 = NULL;

    assertFrameObject( frame_0b78a049b4e8e806f417067cc34e5b58 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_value );
    tmp_return_value = var_value;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$defaults$$$function_3_get_default_input );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_value );
    var_value = NULL;

    Py_XDECREF( var_app );
    var_app = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_app );
    var_app = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$defaults$$$function_3_get_default_input );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$input$defaults$$$function_4_set_default_input( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_input = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_1e0d7ae2a062d0d270d1614a18223df7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_1e0d7ae2a062d0d270d1614a18223df7 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1e0d7ae2a062d0d270d1614a18223df7, codeobj_1e0d7ae2a062d0d270d1614a18223df7, module_prompt_toolkit$input$defaults, sizeof(void *) );
    frame_1e0d7ae2a062d0d270d1614a18223df7 = cache_frame_1e0d7ae2a062d0d270d1614a18223df7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1e0d7ae2a062d0d270d1614a18223df7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1e0d7ae2a062d0d270d1614a18223df7 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_input );
        tmp_isinstance_inst_1 = par_input;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_Input );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Input );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Input" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 71;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 71;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain__default_input );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__default_input );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_default_input" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 72;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_input );
        tmp_args_element_name_1 = par_input;
        frame_1e0d7ae2a062d0d270d1614a18223df7->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_set, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1e0d7ae2a062d0d270d1614a18223df7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1e0d7ae2a062d0d270d1614a18223df7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1e0d7ae2a062d0d270d1614a18223df7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1e0d7ae2a062d0d270d1614a18223df7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1e0d7ae2a062d0d270d1614a18223df7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1e0d7ae2a062d0d270d1614a18223df7,
        type_description_1,
        par_input
    );


    // Release cached frame.
    if ( frame_1e0d7ae2a062d0d270d1614a18223df7 == cache_frame_1e0d7ae2a062d0d270d1614a18223df7 )
    {
        Py_DECREF( frame_1e0d7ae2a062d0d270d1614a18223df7 );
    }
    cache_frame_1e0d7ae2a062d0d270d1614a18223df7 = NULL;

    assertFrameObject( frame_1e0d7ae2a062d0d270d1614a18223df7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$defaults$$$function_4_set_default_input );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_input );
    Py_DECREF( par_input );
    par_input = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_input );
    Py_DECREF( par_input );
    par_input = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$input$defaults$$$function_4_set_default_input );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_1_create_input( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$input$defaults$$$function_1_create_input,
        const_str_plain_create_input,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4c36dd999259b5151ff6d64ceaf57900,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$input$defaults,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_2_create_pipe_input(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$input$defaults$$$function_2_create_pipe_input,
        const_str_plain_create_pipe_input,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ec71f767328cf5d84ec689e7a8fcd95d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$input$defaults,
        const_str_digest_8175dc6570324e08d2818e1021dc0e76,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_3_get_default_input(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$input$defaults$$$function_3_get_default_input,
        const_str_plain_get_default_input,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0b78a049b4e8e806f417067cc34e5b58,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$input$defaults,
        const_str_digest_981fa8417d2e7a53279e174b202ed0e2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_4_set_default_input(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$input$defaults$$$function_4_set_default_input,
        const_str_plain_set_default_input,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1e0d7ae2a062d0d270d1614a18223df7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$input$defaults,
        const_str_digest_2bb71600ed34042e348761c337418f71,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$input$defaults =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.input.defaults",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$input$defaults)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$input$defaults)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$input$defaults );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.input.defaults: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.input.defaults: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.input.defaults: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$input$defaults" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$input$defaults = Py_InitModule4(
        "prompt_toolkit.input.defaults",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$input$defaults = PyModule_Create( &mdef_prompt_toolkit$input$defaults );
#endif

    moduledict_prompt_toolkit$input$defaults = MODULE_DICT( module_prompt_toolkit$input$defaults );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$input$defaults,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$input$defaults,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$input$defaults,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$input$defaults,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$input$defaults );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_cf6a2fbed33a79df3055b39a946503a7, module_prompt_toolkit$input$defaults );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_05c1ca9cd5e0fa451d5da663a55a8d32;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_05c1ca9cd5e0fa451d5da663a55a8d32 = MAKE_MODULE_FRAME( codeobj_05c1ca9cd5e0fa451d5da663a55a8d32, module_prompt_toolkit$input$defaults );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_05c1ca9cd5e0fa451d5da663a55a8d32 );
    assert( Py_REFCNT( frame_05c1ca9cd5e0fa451d5da663a55a8d32 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_05c1ca9cd5e0fa451d5da663a55a8d32->m_frame.f_lineno = 1;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_722e5e8c7f53b34663d9669ca7ddc056;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$input$defaults;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_get_app_tuple;
        tmp_level_name_1 = const_int_0;
        frame_05c1ca9cd5e0fa451d5da663a55a8d32->m_frame.f_lineno = 2;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_get_app );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_get_app, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_15810eaf43342c5627475c0bf384673e;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$input$defaults;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_TaskLocal_str_plain_TaskLocalNotSetError_tuple;
        tmp_level_name_2 = const_int_0;
        frame_05c1ca9cd5e0fa451d5da663a55a8d32->m_frame.f_lineno = 3;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_TaskLocal );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_TaskLocal, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_TaskLocalNotSetError );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_TaskLocalNotSetError, tmp_assign_source_8 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_8cff9047d0926d732c79d282e1f19e8e;
        tmp_globals_name_3 = (PyObject *)moduledict_prompt_toolkit$input$defaults;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_is_windows_tuple;
        tmp_level_name_3 = const_int_0;
        frame_05c1ca9cd5e0fa451d5da663a55a8d32->m_frame.f_lineno = 4;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_is_windows );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_is_windows, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_base;
        tmp_globals_name_4 = (PyObject *)moduledict_prompt_toolkit$input$defaults;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_Input_tuple;
        tmp_level_name_4 = const_int_pos_1;
        frame_05c1ca9cd5e0fa451d5da663a55a8d32->m_frame.f_lineno = 5;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_6 ) )
        {
           tmp_assign_source_10 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_6,
                (PyObject *)moduledict_prompt_toolkit$input$defaults,
                const_str_plain_Input,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_Input );
        }

        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_Input, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_sys;
        tmp_globals_name_5 = (PyObject *)moduledict_prompt_toolkit$input$defaults;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_05c1ca9cd5e0fa451d5da663a55a8d32->m_frame.f_lineno = 6;
        tmp_assign_source_11 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        assert( !(tmp_assign_source_11 == NULL) );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_sys, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = LIST_COPY( const_list_72311564eb8ddb5bedb2d42b9a1124a8_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_13 = MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_1_create_input( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_create_input, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_2_create_pipe_input(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_create_pipe_input, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_TaskLocal );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TaskLocal );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TaskLocal" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 40;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_3;
        frame_05c1ca9cd5e0fa451d5da663a55a8d32->m_frame.f_lineno = 40;
        tmp_assign_source_15 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain__default_input, tmp_assign_source_15 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_05c1ca9cd5e0fa451d5da663a55a8d32 );
#endif
    popFrameStack();

    assertFrameObject( frame_05c1ca9cd5e0fa451d5da663a55a8d32 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_05c1ca9cd5e0fa451d5da663a55a8d32 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_05c1ca9cd5e0fa451d5da663a55a8d32, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_05c1ca9cd5e0fa451d5da663a55a8d32->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_05c1ca9cd5e0fa451d5da663a55a8d32, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_3_get_default_input(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_get_default_input, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_prompt_toolkit$input$defaults$$$function_4_set_default_input(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$input$defaults, (Nuitka_StringObject *)const_str_plain_set_default_input, tmp_assign_source_17 );
    }

    return MOD_RETURN_VALUE( module_prompt_toolkit$input$defaults );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
