/* Generated code for Python module 'colorama.win32'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_colorama$win32" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_colorama$win32;
PyDictObject *moduledict_colorama$win32;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_wAttributes;
extern PyObject *const_int_neg_12;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain_LibraryLoader;
extern PyObject *const_str_plain_position;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_FillConsoleOutputAttribute;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_h;
static PyObject *const_str_digest_ab18682ed06d04e3e4f6a6cc2afa5e6c;
extern PyObject *const_str_plain_BOOL;
static PyObject *const_str_digest_616214d16603016e183078b6e7118582;
extern PyObject *const_str_plain_encode;
extern PyObject *const_tuple_str_plain___tuple;
extern PyObject *const_str_plain_STDOUT;
extern PyObject *const_tuple_str_plain_wintypes_tuple;
extern PyObject *const_str_plain__fields_;
extern PyObject *const_str_plain_dwMaximumWindowSize;
extern PyObject *const_str_plain_windll;
extern PyObject *const_str_plain_SetConsoleTitleW;
extern PyObject *const_tuple_str_plain_title_tuple;
extern PyObject *const_str_plain_Left;
extern PyObject *const_str_plain_WinDLL;
static PyObject *const_str_plain__SetConsoleTitleW;
static PyObject *const_str_digest_ec9d76a7d573d96eeeb376de53296761;
extern PyObject *const_str_plain_Top;
extern PyObject *const_str_plain_Y;
extern PyObject *const_str_plain_winapi_test;
extern PyObject *const_str_plain_start;
extern PyObject *const_tuple_true_tuple;
extern PyObject *const_str_plain___doc__;
static PyObject *const_tuple_5e9eaf5414a044008caa70e29ac90259_tuple;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_plain_LPCWSTR;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_Bottom;
extern PyObject *const_str_plain_Structure;
extern PyObject *const_str_plain_attrs;
extern PyObject *const_str_plain_handles;
static PyObject *const_tuple_4c68d872265d8301fa31c41fb95c26f5_tuple;
extern PyObject *const_str_plain_POINTER;
extern PyObject *const_str_plain_DWORD;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain_GetConsoleScreenBufferInfo;
extern PyObject *const_str_plain_WORD;
static PyObject *const_tuple_type_AttributeError_type_ImportError_tuple;
static PyObject *const_str_plain__SetConsoleTextAttribute;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_sr;
extern PyObject *const_str_plain_SetConsoleTitle;
static PyObject *const_tuple_str_plain_stream_id_str_plain_attrs_str_plain_handle_tuple;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_HANDLE;
extern PyObject *const_str_plain_value;
extern PyObject *const_tuple_str_plain_self_tuple;
static PyObject *const_str_digest_92a38182783a859e30a7b8ed5c303db9;
static PyObject *const_tuple_3426746585531e8b909566d9c2999226_tuple;
extern PyObject *const_str_plain_CONSOLE_SCREEN_BUFFER_INFO;
extern PyObject *const_str_plain_byref;
extern PyObject *const_str_plain_argtypes;
static PyObject *const_str_plain__GetStdHandle;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_attribute;
extern PyObject *const_str_plain_title;
extern PyObject *const_str_plain_STDERR;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_h_tuple;
extern PyObject *const_str_plain_handle;
extern PyObject *const_str_plain_restype;
extern PyObject *const_str_plain_attr;
extern PyObject *const_int_neg_11;
static PyObject *const_tuple_41e1b644985ee52c928e274c6b8e80a7_tuple;
static PyObject *const_str_plain__winapi_test;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_kernel32;
extern PyObject *const_str_plain_Right;
extern PyObject *const_str_plain_char;
static PyObject *const_str_plain__FillConsoleOutputCharacterA;
extern PyObject *const_str_plain_ctypes;
extern PyObject *const_int_0;
static PyObject *const_str_plain__SetConsoleCursorPosition;
static PyObject *const_tuple_str_plain_LibraryLoader_tuple;
extern PyObject *const_str_plain_dwSize;
static PyObject *const_str_digest_fb38e65c823e8a65cba40d555fc4596a;
static PyObject *const_tuple_c4f893c84bf14b64cda50aee0d7f18ba_tuple;
extern PyObject *const_str_plain_adjusted_position;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_length;
extern PyObject *const_str_plain_c_char;
extern PyObject *const_str_plain_GetStdHandle;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_SMALL_RECT;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_str_plain_srWindow;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_wintypes;
static PyObject *const_str_plain_num_written;
static PyObject *const_str_plain__GetConsoleScreenBufferInfo;
extern PyObject *const_str_plain_SetConsoleTextAttribute;
extern PyObject *const_str_plain_FillConsoleOutputCharacter;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain__;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_plain__FillConsoleOutputAttribute;
extern PyObject *const_str_plain___str__;
extern PyObject *const_str_plain_csbi;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_values;
static PyObject *const_tuple_str_plain_handle_str_plain_csbi_str_plain_success_tuple;
extern PyObject *const_str_plain_X;
extern PyObject *const_str_plain_COORD;
extern PyObject *const_str_digest_fb656301b107321fc396a230d8297473;
extern PyObject *const_str_plain_adjust;
static PyObject *const_str_plain__COORD;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_digest_a806b1bd6283c21c21e03a6c34c70977;
extern PyObject *const_str_plain_dwCursorPosition;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_stream_id;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_SetConsoleCursorPosition;
extern PyObject *const_str_digest_b535a8e4efee6c807b96bf3113d01108;
extern PyObject *const_str_plain_success;
extern PyObject *const_str_plain_FillConsoleOutputCharacterA;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_LibraryLoader = UNSTREAM_STRING_ASCII( &constant_bin[ 564789 ], 13, 1 );
    const_str_digest_ab18682ed06d04e3e4f6a6cc2afa5e6c = UNSTREAM_STRING_ASCII( &constant_bin[ 564802 ], 17, 0 );
    const_str_digest_616214d16603016e183078b6e7118582 = UNSTREAM_STRING_ASCII( &constant_bin[ 564819 ], 97, 0 );
    const_str_plain__SetConsoleTitleW = UNSTREAM_STRING_ASCII( &constant_bin[ 564916 ], 17, 1 );
    const_str_digest_ec9d76a7d573d96eeeb376de53296761 = UNSTREAM_STRING_ASCII( &constant_bin[ 564933 ], 23, 0 );
    const_tuple_5e9eaf5414a044008caa70e29ac90259_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_5e9eaf5414a044008caa70e29ac90259_tuple, 0, const_str_plain_byref ); Py_INCREF( const_str_plain_byref );
    PyTuple_SET_ITEM( const_tuple_5e9eaf5414a044008caa70e29ac90259_tuple, 1, const_str_plain_Structure ); Py_INCREF( const_str_plain_Structure );
    PyTuple_SET_ITEM( const_tuple_5e9eaf5414a044008caa70e29ac90259_tuple, 2, const_str_plain_c_char ); Py_INCREF( const_str_plain_c_char );
    PyTuple_SET_ITEM( const_tuple_5e9eaf5414a044008caa70e29ac90259_tuple, 3, const_str_plain_POINTER ); Py_INCREF( const_str_plain_POINTER );
    const_str_plain_LPCWSTR = UNSTREAM_STRING_ASCII( &constant_bin[ 564956 ], 7, 1 );
    const_tuple_4c68d872265d8301fa31c41fb95c26f5_tuple = PyTuple_New( 7 );
    const_str_plain_stream_id = UNSTREAM_STRING_ASCII( &constant_bin[ 564963 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_4c68d872265d8301fa31c41fb95c26f5_tuple, 0, const_str_plain_stream_id ); Py_INCREF( const_str_plain_stream_id );
    PyTuple_SET_ITEM( const_tuple_4c68d872265d8301fa31c41fb95c26f5_tuple, 1, const_str_plain_char ); Py_INCREF( const_str_plain_char );
    PyTuple_SET_ITEM( const_tuple_4c68d872265d8301fa31c41fb95c26f5_tuple, 2, const_str_plain_length ); Py_INCREF( const_str_plain_length );
    PyTuple_SET_ITEM( const_tuple_4c68d872265d8301fa31c41fb95c26f5_tuple, 3, const_str_plain_start ); Py_INCREF( const_str_plain_start );
    PyTuple_SET_ITEM( const_tuple_4c68d872265d8301fa31c41fb95c26f5_tuple, 4, const_str_plain_handle ); Py_INCREF( const_str_plain_handle );
    const_str_plain_num_written = UNSTREAM_STRING_ASCII( &constant_bin[ 564972 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_4c68d872265d8301fa31c41fb95c26f5_tuple, 5, const_str_plain_num_written ); Py_INCREF( const_str_plain_num_written );
    PyTuple_SET_ITEM( const_tuple_4c68d872265d8301fa31c41fb95c26f5_tuple, 6, const_str_plain_success ); Py_INCREF( const_str_plain_success );
    const_tuple_type_AttributeError_type_ImportError_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_type_AttributeError_type_ImportError_tuple, 0, (PyObject *)PyExc_AttributeError ); Py_INCREF( (PyObject *)PyExc_AttributeError );
    PyTuple_SET_ITEM( const_tuple_type_AttributeError_type_ImportError_tuple, 1, (PyObject *)PyExc_ImportError ); Py_INCREF( (PyObject *)PyExc_ImportError );
    const_str_plain__SetConsoleTextAttribute = UNSTREAM_STRING_ASCII( &constant_bin[ 564983 ], 24, 1 );
    const_tuple_str_plain_stream_id_str_plain_attrs_str_plain_handle_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_id_str_plain_attrs_str_plain_handle_tuple, 0, const_str_plain_stream_id ); Py_INCREF( const_str_plain_stream_id );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_id_str_plain_attrs_str_plain_handle_tuple, 1, const_str_plain_attrs ); Py_INCREF( const_str_plain_attrs );
    PyTuple_SET_ITEM( const_tuple_str_plain_stream_id_str_plain_attrs_str_plain_handle_tuple, 2, const_str_plain_handle ); Py_INCREF( const_str_plain_handle );
    const_str_digest_92a38182783a859e30a7b8ed5c303db9 = UNSTREAM_STRING_ASCII( &constant_bin[ 564941 ], 14, 0 );
    const_tuple_3426746585531e8b909566d9c2999226_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_3426746585531e8b909566d9c2999226_tuple, 0, const_str_plain_stream_id ); Py_INCREF( const_str_plain_stream_id );
    PyTuple_SET_ITEM( const_tuple_3426746585531e8b909566d9c2999226_tuple, 1, const_str_plain_position ); Py_INCREF( const_str_plain_position );
    PyTuple_SET_ITEM( const_tuple_3426746585531e8b909566d9c2999226_tuple, 2, const_str_plain_adjust ); Py_INCREF( const_str_plain_adjust );
    PyTuple_SET_ITEM( const_tuple_3426746585531e8b909566d9c2999226_tuple, 3, const_str_plain_adjusted_position ); Py_INCREF( const_str_plain_adjusted_position );
    PyTuple_SET_ITEM( const_tuple_3426746585531e8b909566d9c2999226_tuple, 4, const_str_plain_sr ); Py_INCREF( const_str_plain_sr );
    PyTuple_SET_ITEM( const_tuple_3426746585531e8b909566d9c2999226_tuple, 5, const_str_plain_handle ); Py_INCREF( const_str_plain_handle );
    const_str_plain__GetStdHandle = UNSTREAM_STRING_ASCII( &constant_bin[ 565007 ], 13, 1 );
    const_tuple_41e1b644985ee52c928e274c6b8e80a7_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_41e1b644985ee52c928e274c6b8e80a7_tuple, 0, const_str_plain_stream_id ); Py_INCREF( const_str_plain_stream_id );
    PyTuple_SET_ITEM( const_tuple_41e1b644985ee52c928e274c6b8e80a7_tuple, 1, const_str_plain_attr ); Py_INCREF( const_str_plain_attr );
    PyTuple_SET_ITEM( const_tuple_41e1b644985ee52c928e274c6b8e80a7_tuple, 2, const_str_plain_length ); Py_INCREF( const_str_plain_length );
    PyTuple_SET_ITEM( const_tuple_41e1b644985ee52c928e274c6b8e80a7_tuple, 3, const_str_plain_start ); Py_INCREF( const_str_plain_start );
    PyTuple_SET_ITEM( const_tuple_41e1b644985ee52c928e274c6b8e80a7_tuple, 4, const_str_plain_handle ); Py_INCREF( const_str_plain_handle );
    PyTuple_SET_ITEM( const_tuple_41e1b644985ee52c928e274c6b8e80a7_tuple, 5, const_str_plain_attribute ); Py_INCREF( const_str_plain_attribute );
    PyTuple_SET_ITEM( const_tuple_41e1b644985ee52c928e274c6b8e80a7_tuple, 6, const_str_plain_num_written ); Py_INCREF( const_str_plain_num_written );
    const_str_plain__winapi_test = UNSTREAM_STRING_ASCII( &constant_bin[ 565020 ], 12, 1 );
    const_str_plain__FillConsoleOutputCharacterA = UNSTREAM_STRING_ASCII( &constant_bin[ 565032 ], 28, 1 );
    const_str_plain__SetConsoleCursorPosition = UNSTREAM_STRING_ASCII( &constant_bin[ 565060 ], 25, 1 );
    const_tuple_str_plain_LibraryLoader_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_LibraryLoader_tuple, 0, const_str_plain_LibraryLoader ); Py_INCREF( const_str_plain_LibraryLoader );
    const_str_digest_fb38e65c823e8a65cba40d555fc4596a = UNSTREAM_STRING_ASCII( &constant_bin[ 565085 ], 30, 0 );
    const_tuple_c4f893c84bf14b64cda50aee0d7f18ba_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_c4f893c84bf14b64cda50aee0d7f18ba_tuple, 0, const_str_plain_stream_id ); Py_INCREF( const_str_plain_stream_id );
    PyTuple_SET_ITEM( const_tuple_c4f893c84bf14b64cda50aee0d7f18ba_tuple, 1, const_str_plain_handle ); Py_INCREF( const_str_plain_handle );
    PyTuple_SET_ITEM( const_tuple_c4f893c84bf14b64cda50aee0d7f18ba_tuple, 2, const_str_plain_csbi ); Py_INCREF( const_str_plain_csbi );
    PyTuple_SET_ITEM( const_tuple_c4f893c84bf14b64cda50aee0d7f18ba_tuple, 3, const_str_plain_success ); Py_INCREF( const_str_plain_success );
    const_str_plain__GetConsoleScreenBufferInfo = UNSTREAM_STRING_ASCII( &constant_bin[ 565115 ], 27, 1 );
    const_str_plain__FillConsoleOutputAttribute = UNSTREAM_STRING_ASCII( &constant_bin[ 565142 ], 27, 1 );
    const_tuple_str_plain_handle_str_plain_csbi_str_plain_success_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_handle_str_plain_csbi_str_plain_success_tuple, 0, const_str_plain_handle ); Py_INCREF( const_str_plain_handle );
    PyTuple_SET_ITEM( const_tuple_str_plain_handle_str_plain_csbi_str_plain_success_tuple, 1, const_str_plain_csbi ); Py_INCREF( const_str_plain_csbi );
    PyTuple_SET_ITEM( const_tuple_str_plain_handle_str_plain_csbi_str_plain_success_tuple, 2, const_str_plain_success ); Py_INCREF( const_str_plain_success );
    const_str_plain__COORD = UNSTREAM_STRING_ASCII( &constant_bin[ 565169 ], 6, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_colorama$win32( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_641f2d2efe404bc81e256fc64855fa98;
static PyCodeObject *codeobj_b0feb02e53efe22d71f7f111c8b1fc44;
static PyCodeObject *codeobj_75e539cf10fc860fd0562685b8fbe477;
static PyCodeObject *codeobj_b5760f74927d4aa5719917afd9996a6c;
static PyCodeObject *codeobj_81d580a13321964c190a12ce2fb85ace;
static PyCodeObject *codeobj_e1a17dffa51cb08fb058692565398aa8;
static PyCodeObject *codeobj_8cfeaf87838ae8425c65328a2287c79e;
static PyCodeObject *codeobj_992441511efea1c3967de22270b0b7df;
static PyCodeObject *codeobj_9d02cf7cad68cfe80e23c160019b4538;
static PyCodeObject *codeobj_05fac8f5117cda7d1a214697ed3ad0c2;
static PyCodeObject *codeobj_c382b059dba5261a78f491a5da27e699;
static PyCodeObject *codeobj_582661f927acb87c292b1a2433e90f49;
static PyCodeObject *codeobj_ec18ca0940cf0b9d64c97ee048884a9d;
static PyCodeObject *codeobj_d1f32d56d8ddee9756871a93dde60af6;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_ab18682ed06d04e3e4f6a6cc2afa5e6c );
    codeobj_641f2d2efe404bc81e256fc64855fa98 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 104, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_h_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b0feb02e53efe22d71f7f111c8b1fc44 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 14, const_tuple_str_plain___tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_NOFREE );
    codeobj_75e539cf10fc860fd0562685b8fbe477 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 15, const_tuple_str_plain___tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_NOFREE );
    codeobj_b5760f74927d4aa5719917afd9996a6c = MAKE_CODEOBJ( module_filename_obj, const_str_digest_ec9d76a7d573d96eeeb376de53296761, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_81d580a13321964c190a12ce2fb85ace = MAKE_CODEOBJ( module_filename_obj, const_str_plain_CONSOLE_SCREEN_BUFFER_INFO, 21, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_e1a17dffa51cb08fb058692565398aa8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_FillConsoleOutputAttribute, 145, const_tuple_41e1b644985ee52c928e274c6b8e80a7_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_8cfeaf87838ae8425c65328a2287c79e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_FillConsoleOutputCharacter, 135, const_tuple_4c68d872265d8301fa31c41fb95c26f5_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_992441511efea1c3967de22270b0b7df = MAKE_CODEOBJ( module_filename_obj, const_str_plain_GetConsoleScreenBufferInfo, 106, const_tuple_c4f893c84bf14b64cda50aee0d7f18ba_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9d02cf7cad68cfe80e23c160019b4538 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_SetConsoleCursorPosition, 117, const_tuple_3426746585531e8b909566d9c2999226_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_05fac8f5117cda7d1a214697ed3ad0c2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_SetConsoleTextAttribute, 113, const_tuple_str_plain_stream_id_str_plain_attrs_str_plain_handle_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c382b059dba5261a78f491a5da27e699 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_SetConsoleTitle, 155, const_tuple_str_plain_title_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_582661f927acb87c292b1a2433e90f49 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___str__, 30, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ec18ca0940cf0b9d64c97ee048884a9d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__winapi_test, 97, const_tuple_str_plain_handle_str_plain_csbi_str_plain_success_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d1f32d56d8ddee9756871a93dde60af6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_winapi_test, 103, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *colorama$win32$$$function_5_winapi_test$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_4_complex_call_helper_star_list( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_10_FillConsoleOutputAttribute(  );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_11_SetConsoleTitle(  );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_2_lambda(  );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_3___str__(  );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_4__winapi_test(  );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_5_winapi_test(  );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_6_GetConsoleScreenBufferInfo( PyObject *defaults );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_7_SetConsoleTextAttribute(  );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_8_SetConsoleCursorPosition( PyObject *defaults );


static PyObject *MAKE_FUNCTION_colorama$win32$$$function_9_FillConsoleOutputCharacter(  );


// The module function definitions.
static PyObject *impl_colorama$win32$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par__ = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par__ );
    Py_DECREF( par__ );
    par__ = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par__ );
    Py_DECREF( par__ );
    par__ = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_1_lambda );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$win32$$$function_2_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par__ = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_2_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par__ );
    Py_DECREF( par__ );
    par__ = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par__ );
    Py_DECREF( par__ );
    par__ = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_2_lambda );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$win32$$$function_3___str__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_582661f927acb87c292b1a2433e90f49;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_582661f927acb87c292b1a2433e90f49 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_582661f927acb87c292b1a2433e90f49, codeobj_582661f927acb87c292b1a2433e90f49, module_colorama$win32, sizeof(void *) );
    frame_582661f927acb87c292b1a2433e90f49 = cache_frame_582661f927acb87c292b1a2433e90f49;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_582661f927acb87c292b1a2433e90f49 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_582661f927acb87c292b1a2433e90f49 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_source_name_6;
        PyObject *tmp_source_name_7;
        PyObject *tmp_source_name_8;
        PyObject *tmp_source_name_9;
        PyObject *tmp_source_name_10;
        PyObject *tmp_source_name_11;
        PyObject *tmp_source_name_12;
        PyObject *tmp_source_name_13;
        PyObject *tmp_source_name_14;
        PyObject *tmp_source_name_15;
        PyObject *tmp_source_name_16;
        PyObject *tmp_source_name_17;
        PyObject *tmp_source_name_18;
        PyObject *tmp_source_name_19;
        PyObject *tmp_source_name_20;
        PyObject *tmp_source_name_21;
        tmp_left_name_1 = const_str_digest_fb656301b107321fc396a230d8297473;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_dwSize );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Y );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 11 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_dwSize );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_X );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 32;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_dwCursorPosition );
        if ( tmp_source_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 33;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_Y );
        Py_DECREF( tmp_source_name_5 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 33;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_8 = par_self;
        tmp_source_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_dwCursorPosition );
        if ( tmp_source_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 33;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_X );
        Py_DECREF( tmp_source_name_7 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 33;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 3, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_9 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_wAttributes );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 34;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 4, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_11 = par_self;
        tmp_source_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_srWindow );
        if ( tmp_source_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 35;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_Top );
        Py_DECREF( tmp_source_name_10 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 35;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 5, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_13 = par_self;
        tmp_source_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_srWindow );
        if ( tmp_source_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 35;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_Left );
        Py_DECREF( tmp_source_name_12 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 35;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 6, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_15 = par_self;
        tmp_source_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_srWindow );
        if ( tmp_source_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 35;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_Bottom );
        Py_DECREF( tmp_source_name_14 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 35;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 7, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_17 = par_self;
        tmp_source_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_srWindow );
        if ( tmp_source_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 35;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_Right );
        Py_DECREF( tmp_source_name_16 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 35;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 8, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_19 = par_self;
        tmp_source_name_18 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_dwMaximumWindowSize );
        if ( tmp_source_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_Y );
        Py_DECREF( tmp_source_name_18 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 9, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_21 = par_self;
        tmp_source_name_20 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_dwMaximumWindowSize );
        if ( tmp_source_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_X );
        Py_DECREF( tmp_source_name_20 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 36;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 10, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_582661f927acb87c292b1a2433e90f49 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_582661f927acb87c292b1a2433e90f49 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_582661f927acb87c292b1a2433e90f49 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_582661f927acb87c292b1a2433e90f49, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_582661f927acb87c292b1a2433e90f49->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_582661f927acb87c292b1a2433e90f49, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_582661f927acb87c292b1a2433e90f49,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_582661f927acb87c292b1a2433e90f49 == cache_frame_582661f927acb87c292b1a2433e90f49 )
    {
        Py_DECREF( frame_582661f927acb87c292b1a2433e90f49 );
    }
    cache_frame_582661f927acb87c292b1a2433e90f49 = NULL;

    assertFrameObject( frame_582661f927acb87c292b1a2433e90f49 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_3___str__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_3___str__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$win32$$$function_4__winapi_test( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_handle = python_pars[ 0 ];
    PyObject *var_csbi = NULL;
    PyObject *var_success = NULL;
    struct Nuitka_FrameObject *frame_ec18ca0940cf0b9d64c97ee048884a9d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_ec18ca0940cf0b9d64c97ee048884a9d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ec18ca0940cf0b9d64c97ee048884a9d, codeobj_ec18ca0940cf0b9d64c97ee048884a9d, module_colorama$win32, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_ec18ca0940cf0b9d64c97ee048884a9d = cache_frame_ec18ca0940cf0b9d64c97ee048884a9d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ec18ca0940cf0b9d64c97ee048884a9d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ec18ca0940cf0b9d64c97ee048884a9d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_CONSOLE_SCREEN_BUFFER_INFO );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CONSOLE_SCREEN_BUFFER_INFO );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CONSOLE_SCREEN_BUFFER_INFO" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 98;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_ec18ca0940cf0b9d64c97ee048884a9d->m_frame.f_lineno = 98;
        tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_csbi == NULL );
        var_csbi = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__GetConsoleScreenBufferInfo );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__GetConsoleScreenBufferInfo );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_GetConsoleScreenBufferInfo" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_handle );
        tmp_args_element_name_1 = par_handle;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_byref );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_byref );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "byref" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 100;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_3;
        CHECK_OBJECT( var_csbi );
        tmp_args_element_name_3 = var_csbi;
        frame_ec18ca0940cf0b9d64c97ee048884a9d->m_frame.f_lineno = 100;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_ec18ca0940cf0b9d64c97ee048884a9d->m_frame.f_lineno = 99;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_success == NULL );
        var_success = tmp_assign_source_2;
    }
    {
        PyObject *tmp_value_name_1;
        CHECK_OBJECT( var_success );
        tmp_value_name_1 = var_success;
        tmp_res = CHECK_IF_TRUE( tmp_value_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res != 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec18ca0940cf0b9d64c97ee048884a9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec18ca0940cf0b9d64c97ee048884a9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ec18ca0940cf0b9d64c97ee048884a9d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ec18ca0940cf0b9d64c97ee048884a9d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ec18ca0940cf0b9d64c97ee048884a9d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ec18ca0940cf0b9d64c97ee048884a9d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ec18ca0940cf0b9d64c97ee048884a9d,
        type_description_1,
        par_handle,
        var_csbi,
        var_success
    );


    // Release cached frame.
    if ( frame_ec18ca0940cf0b9d64c97ee048884a9d == cache_frame_ec18ca0940cf0b9d64c97ee048884a9d )
    {
        Py_DECREF( frame_ec18ca0940cf0b9d64c97ee048884a9d );
    }
    cache_frame_ec18ca0940cf0b9d64c97ee048884a9d = NULL;

    assertFrameObject( frame_ec18ca0940cf0b9d64c97ee048884a9d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_4__winapi_test );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_handle );
    Py_DECREF( par_handle );
    par_handle = NULL;

    CHECK_OBJECT( (PyObject *)var_csbi );
    Py_DECREF( var_csbi );
    var_csbi = NULL;

    CHECK_OBJECT( (PyObject *)var_success );
    Py_DECREF( var_success );
    var_success = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_handle );
    Py_DECREF( par_handle );
    par_handle = NULL;

    Py_XDECREF( var_csbi );
    var_csbi = NULL;

    Py_XDECREF( var_success );
    var_success = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_4__winapi_test );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$win32$$$function_5_winapi_test( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_d1f32d56d8ddee9756871a93dde60af6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d1f32d56d8ddee9756871a93dde60af6 = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_d1f32d56d8ddee9756871a93dde60af6, codeobj_d1f32d56d8ddee9756871a93dde60af6, module_colorama$win32, 0 );
    frame_d1f32d56d8ddee9756871a93dde60af6 = cache_frame_d1f32d56d8ddee9756871a93dde60af6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d1f32d56d8ddee9756871a93dde60af6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d1f32d56d8ddee9756871a93dde60af6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_any_arg_1;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_handles );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_handles );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "handles" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 104;

                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            frame_d1f32d56d8ddee9756871a93dde60af6->m_frame.f_lineno = 104;
            tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_values );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;

                goto frame_exception_exit_1;
            }
            tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 104;

                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_1;
        }
        // Tried code:
        tmp_any_arg_1 = colorama$win32$$$function_5_winapi_test$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_any_arg_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_1;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_5_winapi_test );
        return NULL;
        // Return handler code:
        try_return_handler_1:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_5_winapi_test );
        return NULL;
        outline_result_1:;
        tmp_return_value = BUILTIN_ANY( tmp_any_arg_1 );
        Py_DECREF( tmp_any_arg_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;

            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d1f32d56d8ddee9756871a93dde60af6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d1f32d56d8ddee9756871a93dde60af6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d1f32d56d8ddee9756871a93dde60af6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d1f32d56d8ddee9756871a93dde60af6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d1f32d56d8ddee9756871a93dde60af6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d1f32d56d8ddee9756871a93dde60af6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d1f32d56d8ddee9756871a93dde60af6,
        type_description_1
    );


    // Release cached frame.
    if ( frame_d1f32d56d8ddee9756871a93dde60af6 == cache_frame_d1f32d56d8ddee9756871a93dde60af6 )
    {
        Py_DECREF( frame_d1f32d56d8ddee9756871a93dde60af6 );
    }
    cache_frame_d1f32d56d8ddee9756871a93dde60af6 = NULL;

    assertFrameObject( frame_d1f32d56d8ddee9756871a93dde60af6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_5_winapi_test );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct colorama$win32$$$function_5_winapi_test$$$genexpr_1_genexpr_locals {
    PyObject *var_h;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *colorama$win32$$$function_5_winapi_test$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct colorama$win32$$$function_5_winapi_test$$$genexpr_1_genexpr_locals *generator_heap = (struct colorama$win32$$$function_5_winapi_test$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_h = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_641f2d2efe404bc81e256fc64855fa98, module_colorama$win32, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 104;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_h;
            generator_heap->var_h = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_h );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__winapi_test );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__winapi_test );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_winapi_test" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 104;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_h );
        tmp_args_element_name_1 = generator_heap->var_h;
        generator->m_frame->m_frame.f_lineno = 104;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_expression_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 104;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 104;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 104;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_h
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_h );
    generator_heap->var_h = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_h );
    generator_heap->var_h = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *colorama$win32$$$function_5_winapi_test$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        colorama$win32$$$function_5_winapi_test$$$genexpr_1_genexpr_context,
        module_colorama$win32,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_fb38e65c823e8a65cba40d555fc4596a,
#endif
        codeobj_641f2d2efe404bc81e256fc64855fa98,
        1,
        sizeof(struct colorama$win32$$$function_5_winapi_test$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_colorama$win32$$$function_6_GetConsoleScreenBufferInfo( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_stream_id = python_pars[ 0 ];
    PyObject *var_handle = NULL;
    PyObject *var_csbi = NULL;
    PyObject *var_success = NULL;
    struct Nuitka_FrameObject *frame_992441511efea1c3967de22270b0b7df;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_992441511efea1c3967de22270b0b7df = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_992441511efea1c3967de22270b0b7df, codeobj_992441511efea1c3967de22270b0b7df, module_colorama$win32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_992441511efea1c3967de22270b0b7df = cache_frame_992441511efea1c3967de22270b0b7df;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_992441511efea1c3967de22270b0b7df );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_992441511efea1c3967de22270b0b7df ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_handles );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_handles );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "handles" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 107;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_stream_id );
        tmp_subscript_name_1 = par_stream_id;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_handle == NULL );
        var_handle = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_CONSOLE_SCREEN_BUFFER_INFO );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CONSOLE_SCREEN_BUFFER_INFO );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CONSOLE_SCREEN_BUFFER_INFO" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 108;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        frame_992441511efea1c3967de22270b0b7df->m_frame.f_lineno = 108;
        tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_csbi == NULL );
        var_csbi = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__GetConsoleScreenBufferInfo );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__GetConsoleScreenBufferInfo );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_GetConsoleScreenBufferInfo" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 109;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_3;
        CHECK_OBJECT( var_handle );
        tmp_args_element_name_1 = var_handle;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_byref );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_byref );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "byref" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 110;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_4;
        CHECK_OBJECT( var_csbi );
        tmp_args_element_name_3 = var_csbi;
        frame_992441511efea1c3967de22270b0b7df->m_frame.f_lineno = 110;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_992441511efea1c3967de22270b0b7df->m_frame.f_lineno = 109;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 109;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_success == NULL );
        var_success = tmp_assign_source_3;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_992441511efea1c3967de22270b0b7df );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_992441511efea1c3967de22270b0b7df );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_992441511efea1c3967de22270b0b7df, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_992441511efea1c3967de22270b0b7df->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_992441511efea1c3967de22270b0b7df, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_992441511efea1c3967de22270b0b7df,
        type_description_1,
        par_stream_id,
        var_handle,
        var_csbi,
        var_success
    );


    // Release cached frame.
    if ( frame_992441511efea1c3967de22270b0b7df == cache_frame_992441511efea1c3967de22270b0b7df )
    {
        Py_DECREF( frame_992441511efea1c3967de22270b0b7df );
    }
    cache_frame_992441511efea1c3967de22270b0b7df = NULL;

    assertFrameObject( frame_992441511efea1c3967de22270b0b7df );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_csbi );
    tmp_return_value = var_csbi;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_6_GetConsoleScreenBufferInfo );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_stream_id );
    Py_DECREF( par_stream_id );
    par_stream_id = NULL;

    CHECK_OBJECT( (PyObject *)var_handle );
    Py_DECREF( var_handle );
    var_handle = NULL;

    CHECK_OBJECT( (PyObject *)var_csbi );
    Py_DECREF( var_csbi );
    var_csbi = NULL;

    CHECK_OBJECT( (PyObject *)var_success );
    Py_DECREF( var_success );
    var_success = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_stream_id );
    Py_DECREF( par_stream_id );
    par_stream_id = NULL;

    Py_XDECREF( var_handle );
    var_handle = NULL;

    Py_XDECREF( var_csbi );
    var_csbi = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_6_GetConsoleScreenBufferInfo );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$win32$$$function_7_SetConsoleTextAttribute( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_stream_id = python_pars[ 0 ];
    PyObject *par_attrs = python_pars[ 1 ];
    PyObject *var_handle = NULL;
    struct Nuitka_FrameObject *frame_05fac8f5117cda7d1a214697ed3ad0c2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_05fac8f5117cda7d1a214697ed3ad0c2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_05fac8f5117cda7d1a214697ed3ad0c2, codeobj_05fac8f5117cda7d1a214697ed3ad0c2, module_colorama$win32, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_05fac8f5117cda7d1a214697ed3ad0c2 = cache_frame_05fac8f5117cda7d1a214697ed3ad0c2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_05fac8f5117cda7d1a214697ed3ad0c2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_05fac8f5117cda7d1a214697ed3ad0c2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_handles );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_handles );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "handles" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 114;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_stream_id );
        tmp_subscript_name_1 = par_stream_id;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 114;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_handle == NULL );
        var_handle = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleTextAttribute );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__SetConsoleTextAttribute );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_SetConsoleTextAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( var_handle );
        tmp_args_element_name_1 = var_handle;
        CHECK_OBJECT( par_attrs );
        tmp_args_element_name_2 = par_attrs;
        frame_05fac8f5117cda7d1a214697ed3ad0c2->m_frame.f_lineno = 115;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_05fac8f5117cda7d1a214697ed3ad0c2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_05fac8f5117cda7d1a214697ed3ad0c2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_05fac8f5117cda7d1a214697ed3ad0c2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_05fac8f5117cda7d1a214697ed3ad0c2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_05fac8f5117cda7d1a214697ed3ad0c2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_05fac8f5117cda7d1a214697ed3ad0c2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_05fac8f5117cda7d1a214697ed3ad0c2,
        type_description_1,
        par_stream_id,
        par_attrs,
        var_handle
    );


    // Release cached frame.
    if ( frame_05fac8f5117cda7d1a214697ed3ad0c2 == cache_frame_05fac8f5117cda7d1a214697ed3ad0c2 )
    {
        Py_DECREF( frame_05fac8f5117cda7d1a214697ed3ad0c2 );
    }
    cache_frame_05fac8f5117cda7d1a214697ed3ad0c2 = NULL;

    assertFrameObject( frame_05fac8f5117cda7d1a214697ed3ad0c2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_7_SetConsoleTextAttribute );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_stream_id );
    Py_DECREF( par_stream_id );
    par_stream_id = NULL;

    CHECK_OBJECT( (PyObject *)par_attrs );
    Py_DECREF( par_attrs );
    par_attrs = NULL;

    CHECK_OBJECT( (PyObject *)var_handle );
    Py_DECREF( var_handle );
    var_handle = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_stream_id );
    Py_DECREF( par_stream_id );
    par_stream_id = NULL;

    CHECK_OBJECT( (PyObject *)par_attrs );
    Py_DECREF( par_attrs );
    par_attrs = NULL;

    Py_XDECREF( var_handle );
    var_handle = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_7_SetConsoleTextAttribute );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$win32$$$function_8_SetConsoleCursorPosition( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_stream_id = python_pars[ 0 ];
    PyObject *par_position = python_pars[ 1 ];
    PyObject *par_adjust = python_pars[ 2 ];
    PyObject *var_adjusted_position = NULL;
    PyObject *var_sr = NULL;
    PyObject *var_handle = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    PyObject *tmp_inplace_assign_attr_2__end = NULL;
    PyObject *tmp_inplace_assign_attr_2__start = NULL;
    struct Nuitka_FrameObject *frame_9d02cf7cad68cfe80e23c160019b4538;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_9d02cf7cad68cfe80e23c160019b4538 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9d02cf7cad68cfe80e23c160019b4538, codeobj_9d02cf7cad68cfe80e23c160019b4538, module_colorama$win32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9d02cf7cad68cfe80e23c160019b4538 = cache_frame_9d02cf7cad68cfe80e23c160019b4538;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9d02cf7cad68cfe80e23c160019b4538 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9d02cf7cad68cfe80e23c160019b4538 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_dircall_arg2_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_COORD );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COORD );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COORD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_position );
        tmp_dircall_arg2_1 = par_position;
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
            tmp_assign_source_1 = impl___internal__$$$function_4_complex_call_helper_star_list( dir_call_args );
        }
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_position;
            assert( old != NULL );
            par_position = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_position );
        tmp_source_name_1 = par_position;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_Y );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_LTE_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( par_position );
        tmp_source_name_2 = par_position;
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_X );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_LTE_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_1 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_left_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_right_name_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_COORD );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COORD );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COORD" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_position );
        tmp_source_name_3 = par_position;
        tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_Y );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = const_int_pos_1;
        tmp_args_element_name_1 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_position );
        tmp_source_name_4 = par_position;
        tmp_left_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_X );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 125;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_int_pos_1;
        tmp_args_element_name_2 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 125;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        frame_9d02cf7cad68cfe80e23c160019b4538->m_frame.f_lineno = 125;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_adjusted_position == NULL );
        var_adjusted_position = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_adjust );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_adjust );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_source_name_5;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_mvar_value_4;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_GetConsoleScreenBufferInfo );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_GetConsoleScreenBufferInfo );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "GetConsoleScreenBufferInfo" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 128;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_3;
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_STDOUT );

            if (unlikely( tmp_mvar_value_4 == NULL ))
            {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STDOUT );
            }

            if ( tmp_mvar_value_4 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STDOUT" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 128;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }

            tmp_args_element_name_3 = tmp_mvar_value_4;
            frame_9d02cf7cad68cfe80e23c160019b4538->m_frame.f_lineno = 128;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_source_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            if ( tmp_source_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_srWindow );
            Py_DECREF( tmp_source_name_5 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 128;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            assert( var_sr == NULL );
            var_sr = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( var_adjusted_position );
            tmp_source_name_6 = var_adjusted_position;
            tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_Y );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_inplace_assign_attr_1__start == NULL );
            tmp_inplace_assign_attr_1__start = tmp_assign_source_4;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_left_name_3;
            PyObject *tmp_right_name_3;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
            tmp_left_name_3 = tmp_inplace_assign_attr_1__start;
            CHECK_OBJECT( var_sr );
            tmp_source_name_7 = var_sr;
            tmp_right_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_Top );
            if ( tmp_right_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            tmp_assign_source_5 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_3, tmp_right_name_3 );
            Py_DECREF( tmp_right_name_3 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "oooooo";
                goto try_except_handler_2;
            }
            assert( tmp_inplace_assign_attr_1__end == NULL );
            tmp_inplace_assign_attr_1__end = tmp_assign_source_5;
        }
        // Tried code:
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
            tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
            CHECK_OBJECT( var_adjusted_position );
            tmp_assattr_target_1 = var_adjusted_position;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_Y, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 129;
                type_description_1 = "oooooo";
                goto try_except_handler_3;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( var_adjusted_position );
            tmp_source_name_8 = var_adjusted_position;
            tmp_assign_source_6 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_X );
            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_inplace_assign_attr_2__start == NULL );
            tmp_inplace_assign_attr_2__start = tmp_assign_source_6;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_left_name_4;
            PyObject *tmp_right_name_4;
            PyObject *tmp_source_name_9;
            CHECK_OBJECT( tmp_inplace_assign_attr_2__start );
            tmp_left_name_4 = tmp_inplace_assign_attr_2__start;
            CHECK_OBJECT( var_sr );
            tmp_source_name_9 = var_sr;
            tmp_right_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_Left );
            if ( tmp_right_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;
                type_description_1 = "oooooo";
                goto try_except_handler_4;
            }
            tmp_assign_source_7 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_4, tmp_right_name_4 );
            Py_DECREF( tmp_right_name_4 );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;
                type_description_1 = "oooooo";
                goto try_except_handler_4;
            }
            assert( tmp_inplace_assign_attr_2__end == NULL );
            tmp_inplace_assign_attr_2__end = tmp_assign_source_7;
        }
        // Tried code:
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_assattr_target_2;
            CHECK_OBJECT( tmp_inplace_assign_attr_2__end );
            tmp_assattr_name_2 = tmp_inplace_assign_attr_2__end;
            CHECK_OBJECT( var_adjusted_position );
            tmp_assattr_target_2 = var_adjusted_position;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_X, tmp_assattr_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 130;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
            }
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__end );
        Py_DECREF( tmp_inplace_assign_attr_2__end );
        tmp_inplace_assign_attr_2__end = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_4;
        // End of try:
        try_end_3:;
        goto try_end_4;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__start );
        Py_DECREF( tmp_inplace_assign_attr_2__start );
        tmp_inplace_assign_attr_2__start = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_1;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__end );
        Py_DECREF( tmp_inplace_assign_attr_2__end );
        tmp_inplace_assign_attr_2__end = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_2__start );
        Py_DECREF( tmp_inplace_assign_attr_2__start );
        tmp_inplace_assign_attr_2__start = NULL;

        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_handles );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_handles );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "handles" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_5;
        CHECK_OBJECT( par_stream_id );
        tmp_subscript_name_1 = par_stream_id;
        tmp_assign_source_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_handle == NULL );
        var_handle = tmp_assign_source_8;
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleCursorPosition );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__SetConsoleCursorPosition );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_SetConsoleCursorPosition" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_6;
        CHECK_OBJECT( var_handle );
        tmp_args_element_name_4 = var_handle;
        CHECK_OBJECT( var_adjusted_position );
        tmp_args_element_name_5 = var_adjusted_position;
        frame_9d02cf7cad68cfe80e23c160019b4538->m_frame.f_lineno = 133;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9d02cf7cad68cfe80e23c160019b4538 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9d02cf7cad68cfe80e23c160019b4538 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9d02cf7cad68cfe80e23c160019b4538 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9d02cf7cad68cfe80e23c160019b4538, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9d02cf7cad68cfe80e23c160019b4538->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9d02cf7cad68cfe80e23c160019b4538, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9d02cf7cad68cfe80e23c160019b4538,
        type_description_1,
        par_stream_id,
        par_position,
        par_adjust,
        var_adjusted_position,
        var_sr,
        var_handle
    );


    // Release cached frame.
    if ( frame_9d02cf7cad68cfe80e23c160019b4538 == cache_frame_9d02cf7cad68cfe80e23c160019b4538 )
    {
        Py_DECREF( frame_9d02cf7cad68cfe80e23c160019b4538 );
    }
    cache_frame_9d02cf7cad68cfe80e23c160019b4538 = NULL;

    assertFrameObject( frame_9d02cf7cad68cfe80e23c160019b4538 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_8_SetConsoleCursorPosition );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_stream_id );
    Py_DECREF( par_stream_id );
    par_stream_id = NULL;

    CHECK_OBJECT( (PyObject *)par_position );
    Py_DECREF( par_position );
    par_position = NULL;

    CHECK_OBJECT( (PyObject *)par_adjust );
    Py_DECREF( par_adjust );
    par_adjust = NULL;

    Py_XDECREF( var_adjusted_position );
    var_adjusted_position = NULL;

    Py_XDECREF( var_sr );
    var_sr = NULL;

    Py_XDECREF( var_handle );
    var_handle = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_stream_id );
    Py_DECREF( par_stream_id );
    par_stream_id = NULL;

    CHECK_OBJECT( (PyObject *)par_position );
    Py_DECREF( par_position );
    par_position = NULL;

    CHECK_OBJECT( (PyObject *)par_adjust );
    Py_DECREF( par_adjust );
    par_adjust = NULL;

    Py_XDECREF( var_adjusted_position );
    var_adjusted_position = NULL;

    Py_XDECREF( var_sr );
    var_sr = NULL;

    Py_XDECREF( var_handle );
    var_handle = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_8_SetConsoleCursorPosition );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$win32$$$function_9_FillConsoleOutputCharacter( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_stream_id = python_pars[ 0 ];
    PyObject *par_char = python_pars[ 1 ];
    PyObject *par_length = python_pars[ 2 ];
    PyObject *par_start = python_pars[ 3 ];
    PyObject *var_handle = NULL;
    PyObject *var_num_written = NULL;
    PyObject *var_success = NULL;
    struct Nuitka_FrameObject *frame_8cfeaf87838ae8425c65328a2287c79e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_8cfeaf87838ae8425c65328a2287c79e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8cfeaf87838ae8425c65328a2287c79e, codeobj_8cfeaf87838ae8425c65328a2287c79e, module_colorama$win32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8cfeaf87838ae8425c65328a2287c79e = cache_frame_8cfeaf87838ae8425c65328a2287c79e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8cfeaf87838ae8425c65328a2287c79e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8cfeaf87838ae8425c65328a2287c79e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_handles );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_handles );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "handles" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 136;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_stream_id );
        tmp_subscript_name_1 = par_stream_id;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 136;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_handle == NULL );
        var_handle = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_c_char );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_c_char );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "c_char" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 137;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_char );
        tmp_called_instance_1 = par_char;
        frame_8cfeaf87838ae8425c65328a2287c79e->m_frame.f_lineno = 137;
        tmp_args_element_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_encode );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        frame_8cfeaf87838ae8425c65328a2287c79e->m_frame.f_lineno = 137;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_char;
            assert( old != NULL );
            par_char = tmp_assign_source_2;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_3;
        CHECK_OBJECT( par_length );
        tmp_args_element_name_2 = par_length;
        frame_8cfeaf87838ae8425c65328a2287c79e->m_frame.f_lineno = 138;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_DWORD, call_args );
        }

        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_length;
            assert( old != NULL );
            par_length = tmp_assign_source_3;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_4;
        frame_8cfeaf87838ae8425c65328a2287c79e->m_frame.f_lineno = 139;
        tmp_assign_source_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_DWORD, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_num_written == NULL );
        var_num_written = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputCharacterA );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputCharacterA );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_FillConsoleOutputCharacterA" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 141;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_5;
        CHECK_OBJECT( var_handle );
        tmp_args_element_name_3 = var_handle;
        CHECK_OBJECT( par_char );
        tmp_args_element_name_4 = par_char;
        CHECK_OBJECT( par_length );
        tmp_args_element_name_5 = par_length;
        CHECK_OBJECT( par_start );
        tmp_args_element_name_6 = par_start;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_byref );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_byref );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "byref" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_6;
        CHECK_OBJECT( var_num_written );
        tmp_args_element_name_8 = var_num_written;
        frame_8cfeaf87838ae8425c65328a2287c79e->m_frame.f_lineno = 142;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        frame_8cfeaf87838ae8425c65328a2287c79e->m_frame.f_lineno = 141;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_success == NULL );
        var_success = tmp_assign_source_5;
    }
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( var_num_written );
        tmp_source_name_1 = var_num_written;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_value );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8cfeaf87838ae8425c65328a2287c79e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8cfeaf87838ae8425c65328a2287c79e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8cfeaf87838ae8425c65328a2287c79e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8cfeaf87838ae8425c65328a2287c79e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8cfeaf87838ae8425c65328a2287c79e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8cfeaf87838ae8425c65328a2287c79e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8cfeaf87838ae8425c65328a2287c79e,
        type_description_1,
        par_stream_id,
        par_char,
        par_length,
        par_start,
        var_handle,
        var_num_written,
        var_success
    );


    // Release cached frame.
    if ( frame_8cfeaf87838ae8425c65328a2287c79e == cache_frame_8cfeaf87838ae8425c65328a2287c79e )
    {
        Py_DECREF( frame_8cfeaf87838ae8425c65328a2287c79e );
    }
    cache_frame_8cfeaf87838ae8425c65328a2287c79e = NULL;

    assertFrameObject( frame_8cfeaf87838ae8425c65328a2287c79e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_9_FillConsoleOutputCharacter );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_stream_id );
    Py_DECREF( par_stream_id );
    par_stream_id = NULL;

    CHECK_OBJECT( (PyObject *)par_char );
    Py_DECREF( par_char );
    par_char = NULL;

    CHECK_OBJECT( (PyObject *)par_length );
    Py_DECREF( par_length );
    par_length = NULL;

    CHECK_OBJECT( (PyObject *)par_start );
    Py_DECREF( par_start );
    par_start = NULL;

    CHECK_OBJECT( (PyObject *)var_handle );
    Py_DECREF( var_handle );
    var_handle = NULL;

    CHECK_OBJECT( (PyObject *)var_num_written );
    Py_DECREF( var_num_written );
    var_num_written = NULL;

    CHECK_OBJECT( (PyObject *)var_success );
    Py_DECREF( var_success );
    var_success = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_stream_id );
    Py_DECREF( par_stream_id );
    par_stream_id = NULL;

    CHECK_OBJECT( (PyObject *)par_char );
    Py_DECREF( par_char );
    par_char = NULL;

    CHECK_OBJECT( (PyObject *)par_length );
    Py_DECREF( par_length );
    par_length = NULL;

    CHECK_OBJECT( (PyObject *)par_start );
    Py_DECREF( par_start );
    par_start = NULL;

    Py_XDECREF( var_handle );
    var_handle = NULL;

    Py_XDECREF( var_num_written );
    var_num_written = NULL;

    Py_XDECREF( var_success );
    var_success = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_9_FillConsoleOutputCharacter );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$win32$$$function_10_FillConsoleOutputAttribute( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_stream_id = python_pars[ 0 ];
    PyObject *par_attr = python_pars[ 1 ];
    PyObject *par_length = python_pars[ 2 ];
    PyObject *par_start = python_pars[ 3 ];
    PyObject *var_handle = NULL;
    PyObject *var_attribute = NULL;
    PyObject *var_num_written = NULL;
    struct Nuitka_FrameObject *frame_e1a17dffa51cb08fb058692565398aa8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_e1a17dffa51cb08fb058692565398aa8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e1a17dffa51cb08fb058692565398aa8, codeobj_e1a17dffa51cb08fb058692565398aa8, module_colorama$win32, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e1a17dffa51cb08fb058692565398aa8 = cache_frame_e1a17dffa51cb08fb058692565398aa8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e1a17dffa51cb08fb058692565398aa8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e1a17dffa51cb08fb058692565398aa8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_handles );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_handles );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "handles" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_stream_id );
        tmp_subscript_name_1 = par_stream_id;
        tmp_assign_source_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_handle == NULL );
        var_handle = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_attr );
        tmp_args_element_name_1 = par_attr;
        frame_e1a17dffa51cb08fb058692565398aa8->m_frame.f_lineno = 148;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_WORD, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_attribute == NULL );
        var_attribute = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_3;
        CHECK_OBJECT( par_length );
        tmp_args_element_name_2 = par_length;
        frame_e1a17dffa51cb08fb058692565398aa8->m_frame.f_lineno = 149;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_DWORD, call_args );
        }

        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_length;
            assert( old != NULL );
            par_length = tmp_assign_source_3;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 150;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_3 = tmp_mvar_value_4;
        frame_e1a17dffa51cb08fb058692565398aa8->m_frame.f_lineno = 150;
        tmp_assign_source_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_DWORD, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_num_written == NULL );
        var_num_written = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputAttribute );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputAttribute );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_FillConsoleOutputAttribute" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 152;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_5;
        CHECK_OBJECT( var_handle );
        tmp_args_element_name_3 = var_handle;
        CHECK_OBJECT( var_attribute );
        tmp_args_element_name_4 = var_attribute;
        CHECK_OBJECT( par_length );
        tmp_args_element_name_5 = par_length;
        CHECK_OBJECT( par_start );
        tmp_args_element_name_6 = par_start;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_byref );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_byref );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "byref" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 153;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_6;
        CHECK_OBJECT( var_num_written );
        tmp_args_element_name_8 = var_num_written;
        frame_e1a17dffa51cb08fb058692565398aa8->m_frame.f_lineno = 153;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        frame_e1a17dffa51cb08fb058692565398aa8->m_frame.f_lineno = 152;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6, tmp_args_element_name_7 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS5( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "ooooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e1a17dffa51cb08fb058692565398aa8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e1a17dffa51cb08fb058692565398aa8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e1a17dffa51cb08fb058692565398aa8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e1a17dffa51cb08fb058692565398aa8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e1a17dffa51cb08fb058692565398aa8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e1a17dffa51cb08fb058692565398aa8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e1a17dffa51cb08fb058692565398aa8,
        type_description_1,
        par_stream_id,
        par_attr,
        par_length,
        par_start,
        var_handle,
        var_attribute,
        var_num_written
    );


    // Release cached frame.
    if ( frame_e1a17dffa51cb08fb058692565398aa8 == cache_frame_e1a17dffa51cb08fb058692565398aa8 )
    {
        Py_DECREF( frame_e1a17dffa51cb08fb058692565398aa8 );
    }
    cache_frame_e1a17dffa51cb08fb058692565398aa8 = NULL;

    assertFrameObject( frame_e1a17dffa51cb08fb058692565398aa8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_10_FillConsoleOutputAttribute );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_stream_id );
    Py_DECREF( par_stream_id );
    par_stream_id = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_length );
    Py_DECREF( par_length );
    par_length = NULL;

    CHECK_OBJECT( (PyObject *)par_start );
    Py_DECREF( par_start );
    par_start = NULL;

    CHECK_OBJECT( (PyObject *)var_handle );
    Py_DECREF( var_handle );
    var_handle = NULL;

    CHECK_OBJECT( (PyObject *)var_attribute );
    Py_DECREF( var_attribute );
    var_attribute = NULL;

    CHECK_OBJECT( (PyObject *)var_num_written );
    Py_DECREF( var_num_written );
    var_num_written = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_stream_id );
    Py_DECREF( par_stream_id );
    par_stream_id = NULL;

    CHECK_OBJECT( (PyObject *)par_attr );
    Py_DECREF( par_attr );
    par_attr = NULL;

    CHECK_OBJECT( (PyObject *)par_length );
    Py_DECREF( par_length );
    par_length = NULL;

    CHECK_OBJECT( (PyObject *)par_start );
    Py_DECREF( par_start );
    par_start = NULL;

    Py_XDECREF( var_handle );
    var_handle = NULL;

    Py_XDECREF( var_attribute );
    var_attribute = NULL;

    Py_XDECREF( var_num_written );
    var_num_written = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_10_FillConsoleOutputAttribute );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_colorama$win32$$$function_11_SetConsoleTitle( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_title = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c382b059dba5261a78f491a5da27e699;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c382b059dba5261a78f491a5da27e699 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c382b059dba5261a78f491a5da27e699, codeobj_c382b059dba5261a78f491a5da27e699, module_colorama$win32, sizeof(void *) );
    frame_c382b059dba5261a78f491a5da27e699 = cache_frame_c382b059dba5261a78f491a5da27e699;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c382b059dba5261a78f491a5da27e699 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c382b059dba5261a78f491a5da27e699 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleTitleW );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__SetConsoleTitleW );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_SetConsoleTitleW" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_title );
        tmp_args_element_name_1 = par_title;
        frame_c382b059dba5261a78f491a5da27e699->m_frame.f_lineno = 156;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c382b059dba5261a78f491a5da27e699 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c382b059dba5261a78f491a5da27e699 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c382b059dba5261a78f491a5da27e699 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c382b059dba5261a78f491a5da27e699, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c382b059dba5261a78f491a5da27e699->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c382b059dba5261a78f491a5da27e699, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c382b059dba5261a78f491a5da27e699,
        type_description_1,
        par_title
    );


    // Release cached frame.
    if ( frame_c382b059dba5261a78f491a5da27e699 == cache_frame_c382b059dba5261a78f491a5da27e699 )
    {
        Py_DECREF( frame_c382b059dba5261a78f491a5da27e699 );
    }
    cache_frame_c382b059dba5261a78f491a5da27e699 = NULL;

    assertFrameObject( frame_c382b059dba5261a78f491a5da27e699 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_11_SetConsoleTitle );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_title );
    Py_DECREF( par_title );
    par_title = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( colorama$win32$$$function_11_SetConsoleTitle );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_10_FillConsoleOutputAttribute(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_10_FillConsoleOutputAttribute,
        const_str_plain_FillConsoleOutputAttribute,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e1a17dffa51cb08fb058692565398aa8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        const_str_digest_616214d16603016e183078b6e7118582,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_11_SetConsoleTitle(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_11_SetConsoleTitle,
        const_str_plain_SetConsoleTitle,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c382b059dba5261a78f491a5da27e699,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b0feb02e53efe22d71f7f111c8b1fc44,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_2_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_2_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_75e539cf10fc860fd0562685b8fbe477,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_3___str__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_3___str__,
        const_str_plain___str__,
#if PYTHON_VERSION >= 300
        const_str_digest_b535a8e4efee6c807b96bf3113d01108,
#endif
        codeobj_582661f927acb87c292b1a2433e90f49,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_4__winapi_test(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_4__winapi_test,
        const_str_plain__winapi_test,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ec18ca0940cf0b9d64c97ee048884a9d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_5_winapi_test(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_5_winapi_test,
        const_str_plain_winapi_test,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d1f32d56d8ddee9756871a93dde60af6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_6_GetConsoleScreenBufferInfo( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_6_GetConsoleScreenBufferInfo,
        const_str_plain_GetConsoleScreenBufferInfo,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_992441511efea1c3967de22270b0b7df,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_7_SetConsoleTextAttribute(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_7_SetConsoleTextAttribute,
        const_str_plain_SetConsoleTextAttribute,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_05fac8f5117cda7d1a214697ed3ad0c2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_8_SetConsoleCursorPosition( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_8_SetConsoleCursorPosition,
        const_str_plain_SetConsoleCursorPosition,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9d02cf7cad68cfe80e23c160019b4538,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_colorama$win32$$$function_9_FillConsoleOutputCharacter(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_colorama$win32$$$function_9_FillConsoleOutputCharacter,
        const_str_plain_FillConsoleOutputCharacter,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8cfeaf87838ae8425c65328a2287c79e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_colorama$win32,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_colorama$win32 =
{
    PyModuleDef_HEAD_INIT,
    "colorama.win32",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(colorama$win32)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(colorama$win32)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_colorama$win32 );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("colorama.win32: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("colorama.win32: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("colorama.win32: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initcolorama$win32" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_colorama$win32 = Py_InitModule4(
        "colorama.win32",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_colorama$win32 = PyModule_Create( &mdef_colorama$win32 );
#endif

    moduledict_colorama$win32 = MODULE_DICT( module_colorama$win32 );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_colorama$win32,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_colorama$win32,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_colorama$win32,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_colorama$win32,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_colorama$win32 );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_92a38182783a859e30a7b8ed5c303db9, module_colorama$win32 );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__bases_orig = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    nuitka_bool tmp_try_except_1__unhandled_indicator = NUITKA_BOOL_UNASSIGNED;
    struct Nuitka_FrameObject *frame_b5760f74927d4aa5719917afd9996a6c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_colorama$win32_21 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_81d580a13321964c190a12ce2fb85ace_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_81d580a13321964c190a12ce2fb85ace_2 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_b5760f74927d4aa5719917afd9996a6c = MAKE_MODULE_FRAME( codeobj_b5760f74927d4aa5719917afd9996a6c, module_colorama$win32 );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_b5760f74927d4aa5719917afd9996a6c );
    assert( Py_REFCNT( frame_b5760f74927d4aa5719917afd9996a6c ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = const_int_neg_11;
        UPDATE_STRING_DICT0( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_STDOUT, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = const_int_neg_12;
        UPDATE_STRING_DICT0( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_STDERR, tmp_assign_source_5 );
    }
    {
        nuitka_bool tmp_assign_source_6;
        tmp_assign_source_6 = NUITKA_BOOL_TRUE;
        tmp_try_except_1__unhandled_indicator = tmp_assign_source_6;
    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_ctypes;
        tmp_globals_name_1 = (PyObject *)moduledict_colorama$win32;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 8;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_ctypes, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_ctypes;
        tmp_globals_name_2 = (PyObject *)moduledict_colorama$win32;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_LibraryLoader_tuple;
        tmp_level_name_2 = const_int_0;
        frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 9;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_LibraryLoader );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_LibraryLoader, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_LibraryLoader );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LibraryLoader );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_1 = tmp_mvar_value_3;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_ctypes );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ctypes );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ctypes" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 10;

            goto try_except_handler_2;
        }

        tmp_source_name_1 = tmp_mvar_value_4;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_WinDLL );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_2;
        }
        frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 10;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_windll, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_ctypes;
        tmp_globals_name_3 = (PyObject *)moduledict_colorama$win32;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_wintypes_tuple;
        tmp_level_name_3 = const_int_0;
        frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 11;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_wintypes );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes, tmp_assign_source_10 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        nuitka_bool tmp_assign_source_11;
        tmp_assign_source_11 = NUITKA_BOOL_FALSE;
        tmp_try_except_1__unhandled_indicator = tmp_assign_source_11;
    }
    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_b5760f74927d4aa5719917afd9996a6c, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_b5760f74927d4aa5719917afd9996a6c, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = const_tuple_type_AttributeError_type_ImportError_tuple;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_12;
            tmp_assign_source_12 = Py_None;
            UPDATE_STRING_DICT0( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_windll, tmp_assign_source_12 );
        }
        {
            PyObject *tmp_assign_source_13;
            tmp_assign_source_13 = MAKE_FUNCTION_colorama$win32$$$function_1_lambda(  );



            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_SetConsoleTextAttribute, tmp_assign_source_13 );
        }
        {
            PyObject *tmp_assign_source_14;
            tmp_assign_source_14 = MAKE_FUNCTION_colorama$win32$$$function_2_lambda(  );



            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_winapi_test, tmp_assign_source_14 );
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 7;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_b5760f74927d4aa5719917afd9996a6c->m_frame) frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = exception_tb->tb_lineno;

        goto try_except_handler_3;
        branch_end_1:;
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_1;
    // End of try:
    try_end_2:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( colorama$win32 );
    return MOD_RETURN_VALUE( NULL );
    // End of try:
    try_end_1:;
    {
        nuitka_bool tmp_condition_result_2;
        nuitka_bool tmp_compexpr_left_2;
        nuitka_bool tmp_compexpr_right_2;
        assert( tmp_try_except_1__unhandled_indicator != NUITKA_BOOL_UNASSIGNED);
        tmp_compexpr_left_2 = tmp_try_except_1__unhandled_indicator;
        tmp_compexpr_right_2 = NUITKA_BOOL_TRUE;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_name_name_4;
            PyObject *tmp_globals_name_4;
            PyObject *tmp_locals_name_4;
            PyObject *tmp_fromlist_name_4;
            PyObject *tmp_level_name_4;
            tmp_name_name_4 = const_str_plain_ctypes;
            tmp_globals_name_4 = (PyObject *)moduledict_colorama$win32;
            tmp_locals_name_4 = Py_None;
            tmp_fromlist_name_4 = const_tuple_5e9eaf5414a044008caa70e29ac90259_tuple;
            tmp_level_name_4 = const_int_0;
            frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 17;
            tmp_assign_source_15 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 17;

                goto try_except_handler_1;
            }
            assert( tmp_import_from_1__module == NULL );
            tmp_import_from_1__module = tmp_assign_source_15;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_16;
            PyObject *tmp_import_name_from_3;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_3 = tmp_import_from_1__module;
            tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_byref );
            if ( tmp_assign_source_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 17;

                goto try_except_handler_4;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_byref, tmp_assign_source_16 );
        }
        {
            PyObject *tmp_assign_source_17;
            PyObject *tmp_import_name_from_4;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_4 = tmp_import_from_1__module;
            tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_Structure );
            if ( tmp_assign_source_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 17;

                goto try_except_handler_4;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_Structure, tmp_assign_source_17 );
        }
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_import_name_from_5;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_5 = tmp_import_from_1__module;
            tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_c_char );
            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 17;

                goto try_except_handler_4;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_c_char, tmp_assign_source_18 );
        }
        {
            PyObject *tmp_assign_source_19;
            PyObject *tmp_import_name_from_6;
            CHECK_OBJECT( tmp_import_from_1__module );
            tmp_import_name_from_6 = tmp_import_from_1__module;
            tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_POINTER );
            if ( tmp_assign_source_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 17;

                goto try_except_handler_4;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_POINTER, tmp_assign_source_19 );
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
        Py_DECREF( tmp_import_from_1__module );
        tmp_import_from_1__module = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_1;
        // End of try:
        try_end_3:;
        CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
        Py_DECREF( tmp_import_from_1__module );
        tmp_import_from_1__module = NULL;

        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_5;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 19;

                goto try_except_handler_1;
            }

            tmp_source_name_2 = tmp_mvar_value_5;
            tmp_assign_source_20 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__COORD );
            if ( tmp_assign_source_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 19;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_COORD, tmp_assign_source_20 );
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_mvar_value_6;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_Structure );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Structure );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Structure" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 21;

                goto try_except_handler_5;
            }

            tmp_tuple_element_1 = tmp_mvar_value_6;
            tmp_assign_source_21 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_assign_source_21, 0, tmp_tuple_element_1 );
            assert( tmp_class_creation_1__bases_orig == NULL );
            tmp_class_creation_1__bases_orig = tmp_assign_source_21;
        }
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_dircall_arg1_1;
            CHECK_OBJECT( tmp_class_creation_1__bases_orig );
            tmp_dircall_arg1_1 = tmp_class_creation_1__bases_orig;
            Py_INCREF( tmp_dircall_arg1_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
                tmp_assign_source_22 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
            }
            if ( tmp_assign_source_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_1__bases == NULL );
            tmp_class_creation_1__bases = tmp_assign_source_22;
        }
        {
            PyObject *tmp_assign_source_23;
            tmp_assign_source_23 = PyDict_New();
            assert( tmp_class_creation_1__class_decl_dict == NULL );
            tmp_class_creation_1__class_decl_dict = tmp_assign_source_23;
        }
        {
            PyObject *tmp_assign_source_24;
            PyObject *tmp_metaclass_name_1;
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_key_name_1;
            PyObject *tmp_dict_name_1;
            PyObject *tmp_dict_name_2;
            PyObject *tmp_key_name_2;
            nuitka_bool tmp_condition_result_4;
            int tmp_truth_name_1;
            PyObject *tmp_type_arg_1;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_bases_name_1;
            tmp_key_name_1 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_5;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_1;
            }
            else
            {
                goto condexpr_false_1;
            }
            condexpr_true_1:;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
            tmp_key_name_2 = const_str_plain_metaclass;
            tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
            if ( tmp_metaclass_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_5;
            }
            goto condexpr_end_1;
            condexpr_false_1:;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_5;
            }
            tmp_condition_result_4 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_2;
            }
            else
            {
                goto condexpr_false_2;
            }
            condexpr_true_2:;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_subscribed_name_1 = tmp_class_creation_1__bases;
            tmp_subscript_name_1 = const_int_0;
            tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
            if ( tmp_type_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_5;
            }
            tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
            Py_DECREF( tmp_type_arg_1 );
            if ( tmp_metaclass_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_5;
            }
            goto condexpr_end_2;
            condexpr_false_2:;
            tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
            Py_INCREF( tmp_metaclass_name_1 );
            condexpr_end_2:;
            condexpr_end_1:;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_bases_name_1 = tmp_class_creation_1__bases;
            tmp_assign_source_24 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
            Py_DECREF( tmp_metaclass_name_1 );
            if ( tmp_assign_source_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_1__metaclass == NULL );
            tmp_class_creation_1__metaclass = tmp_assign_source_24;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_key_name_3;
            PyObject *tmp_dict_name_3;
            tmp_key_name_3 = const_str_plain_metaclass;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
            tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_5;
            }
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
            tmp_dictdel_key = const_str_plain_metaclass;
            tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_5;
            }
            branch_no_3:;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_3 = tmp_class_creation_1__metaclass;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___prepare__ );
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_25;
                PyObject *tmp_called_name_2;
                PyObject *tmp_source_name_4;
                PyObject *tmp_args_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_kw_name_1;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_source_name_4 = tmp_class_creation_1__metaclass;
                tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___prepare__ );
                if ( tmp_called_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 21;

                    goto try_except_handler_5;
                }
                tmp_tuple_element_2 = const_str_plain_CONSOLE_SCREEN_BUFFER_INFO;
                tmp_args_name_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_tuple_element_2 = tmp_class_creation_1__bases;
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
                frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 21;
                tmp_assign_source_25 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_args_name_1 );
                if ( tmp_assign_source_25 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 21;

                    goto try_except_handler_5;
                }
                assert( tmp_class_creation_1__prepared == NULL );
                tmp_class_creation_1__prepared = tmp_assign_source_25;
            }
            {
                nuitka_bool tmp_condition_result_7;
                PyObject *tmp_operand_name_1;
                PyObject *tmp_source_name_5;
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_source_name_5 = tmp_class_creation_1__prepared;
                tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___getitem__ );
                tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 21;

                    goto try_except_handler_5;
                }
                tmp_condition_result_7 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_raise_type_1;
                    PyObject *tmp_raise_value_1;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_right_name_1;
                    PyObject *tmp_tuple_element_3;
                    PyObject *tmp_getattr_target_1;
                    PyObject *tmp_getattr_attr_1;
                    PyObject *tmp_getattr_default_1;
                    PyObject *tmp_source_name_6;
                    PyObject *tmp_type_arg_2;
                    tmp_raise_type_1 = PyExc_TypeError;
                    tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                    CHECK_OBJECT( tmp_class_creation_1__metaclass );
                    tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                    tmp_getattr_attr_1 = const_str_plain___name__;
                    tmp_getattr_default_1 = const_str_angle_metaclass;
                    tmp_tuple_element_3 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 21;

                        goto try_except_handler_5;
                    }
                    tmp_right_name_1 = PyTuple_New( 2 );
                    PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_3 );
                    CHECK_OBJECT( tmp_class_creation_1__prepared );
                    tmp_type_arg_2 = tmp_class_creation_1__prepared;
                    tmp_source_name_6 = BUILTIN_TYPE1( tmp_type_arg_2 );
                    assert( !(tmp_source_name_6 == NULL) );
                    tmp_tuple_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___name__ );
                    Py_DECREF( tmp_source_name_6 );
                    if ( tmp_tuple_element_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_right_name_1 );

                        exception_lineno = 21;

                        goto try_except_handler_5;
                    }
                    PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_3 );
                    tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                    Py_DECREF( tmp_right_name_1 );
                    if ( tmp_raise_value_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 21;

                        goto try_except_handler_5;
                    }
                    exception_type = tmp_raise_type_1;
                    Py_INCREF( tmp_raise_type_1 );
                    exception_value = tmp_raise_value_1;
                    exception_lineno = 21;
                    RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                    goto try_except_handler_5;
                }
                branch_no_5:;
            }
            goto branch_end_4;
            branch_no_4:;
            {
                PyObject *tmp_assign_source_26;
                tmp_assign_source_26 = PyDict_New();
                assert( tmp_class_creation_1__prepared == NULL );
                tmp_class_creation_1__prepared = tmp_assign_source_26;
            }
            branch_end_4:;
        }
        {
            PyObject *tmp_assign_source_27;
            {
                PyObject *tmp_set_locals_1;
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_set_locals_1 = tmp_class_creation_1__prepared;
                locals_colorama$win32_21 = tmp_set_locals_1;
                Py_INCREF( tmp_set_locals_1 );
            }
            // Tried code:
            // Tried code:
            tmp_dictset_value = const_str_digest_92a38182783a859e30a7b8ed5c303db9;
            tmp_res = PyObject_SetItem( locals_colorama$win32_21, const_str_plain___module__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_7;
            }
            tmp_dictset_value = const_str_digest_a806b1bd6283c21c21e03a6c34c70977;
            tmp_res = PyObject_SetItem( locals_colorama$win32_21, const_str_plain___doc__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_7;
            }
            tmp_dictset_value = const_str_plain_CONSOLE_SCREEN_BUFFER_INFO;
            tmp_res = PyObject_SetItem( locals_colorama$win32_21, const_str_plain___qualname__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 21;

                goto try_except_handler_7;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_81d580a13321964c190a12ce2fb85ace_2, codeobj_81d580a13321964c190a12ce2fb85ace, module_colorama$win32, sizeof(void *) );
            frame_81d580a13321964c190a12ce2fb85ace_2 = cache_frame_81d580a13321964c190a12ce2fb85ace_2;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_81d580a13321964c190a12ce2fb85ace_2 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_81d580a13321964c190a12ce2fb85ace_2 ) == 2 ); // Frame stack

            // Framed code:
            {
                PyObject *tmp_list_element_1;
                PyObject *tmp_tuple_element_4;
                PyObject *tmp_mvar_value_7;
                PyObject *tmp_tuple_element_5;
                PyObject *tmp_mvar_value_8;
                PyObject *tmp_tuple_element_6;
                PyObject *tmp_source_name_7;
                PyObject *tmp_mvar_value_9;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_source_name_8;
                PyObject *tmp_mvar_value_10;
                PyObject *tmp_tuple_element_8;
                PyObject *tmp_mvar_value_11;
                tmp_tuple_element_4 = const_str_plain_dwSize;
                tmp_list_element_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_4 );
                PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_4 );
                tmp_tuple_element_4 = PyObject_GetItem( locals_colorama$win32_21, const_str_plain_COORD );

                if ( tmp_tuple_element_4 == NULL )
                {
                    if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                    {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_COORD );

                    if (unlikely( tmp_mvar_value_7 == NULL ))
                    {
                        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COORD );
                    }

                    if ( tmp_mvar_value_7 == NULL )
                    {
                        Py_DECREF( tmp_list_element_1 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COORD" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 24;
                        type_description_2 = "o";
                        goto frame_exception_exit_2;
                    }

                    tmp_tuple_element_4 = tmp_mvar_value_7;
                    Py_INCREF( tmp_tuple_element_4 );
                    }
                }

                PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_4 );
                tmp_dictset_value = PyList_New( 5 );
                PyList_SET_ITEM( tmp_dictset_value, 0, tmp_list_element_1 );
                tmp_tuple_element_5 = const_str_plain_dwCursorPosition;
                tmp_list_element_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_5 );
                PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_5 );
                tmp_tuple_element_5 = PyObject_GetItem( locals_colorama$win32_21, const_str_plain_COORD );

                if ( tmp_tuple_element_5 == NULL )
                {
                    if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                    {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_COORD );

                    if (unlikely( tmp_mvar_value_8 == NULL ))
                    {
                        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COORD );
                    }

                    if ( tmp_mvar_value_8 == NULL )
                    {
                        Py_DECREF( tmp_dictset_value );
                        Py_DECREF( tmp_list_element_1 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COORD" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 25;
                        type_description_2 = "o";
                        goto frame_exception_exit_2;
                    }

                    tmp_tuple_element_5 = tmp_mvar_value_8;
                    Py_INCREF( tmp_tuple_element_5 );
                    }
                }

                PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_5 );
                PyList_SET_ITEM( tmp_dictset_value, 1, tmp_list_element_1 );
                tmp_tuple_element_6 = const_str_plain_wAttributes;
                tmp_list_element_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_6 );
                PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_6 );
                tmp_source_name_7 = PyObject_GetItem( locals_colorama$win32_21, const_str_plain_wintypes );

                if ( tmp_source_name_7 == NULL )
                {
                    if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                    {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

                    if (unlikely( tmp_mvar_value_9 == NULL ))
                    {
                        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
                    }

                    if ( tmp_mvar_value_9 == NULL )
                    {
                        Py_DECREF( tmp_dictset_value );
                        Py_DECREF( tmp_list_element_1 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 26;
                        type_description_2 = "o";
                        goto frame_exception_exit_2;
                    }

                    tmp_source_name_7 = tmp_mvar_value_9;
                    Py_INCREF( tmp_source_name_7 );
                    }
                }

                tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_WORD );
                Py_DECREF( tmp_source_name_7 );
                if ( tmp_tuple_element_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_dictset_value );
                    Py_DECREF( tmp_list_element_1 );

                    exception_lineno = 26;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_6 );
                PyList_SET_ITEM( tmp_dictset_value, 2, tmp_list_element_1 );
                tmp_tuple_element_7 = const_str_plain_srWindow;
                tmp_list_element_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_7 );
                PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_7 );
                tmp_source_name_8 = PyObject_GetItem( locals_colorama$win32_21, const_str_plain_wintypes );

                if ( tmp_source_name_8 == NULL )
                {
                    if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                    {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

                    if (unlikely( tmp_mvar_value_10 == NULL ))
                    {
                        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
                    }

                    if ( tmp_mvar_value_10 == NULL )
                    {
                        Py_DECREF( tmp_dictset_value );
                        Py_DECREF( tmp_list_element_1 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 27;
                        type_description_2 = "o";
                        goto frame_exception_exit_2;
                    }

                    tmp_source_name_8 = tmp_mvar_value_10;
                    Py_INCREF( tmp_source_name_8 );
                    }
                }

                tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_SMALL_RECT );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_dictset_value );
                    Py_DECREF( tmp_list_element_1 );

                    exception_lineno = 27;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
                PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_7 );
                PyList_SET_ITEM( tmp_dictset_value, 3, tmp_list_element_1 );
                tmp_tuple_element_8 = const_str_plain_dwMaximumWindowSize;
                tmp_list_element_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_8 );
                PyTuple_SET_ITEM( tmp_list_element_1, 0, tmp_tuple_element_8 );
                tmp_tuple_element_8 = PyObject_GetItem( locals_colorama$win32_21, const_str_plain_COORD );

                if ( tmp_tuple_element_8 == NULL )
                {
                    if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                    {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_COORD );

                    if (unlikely( tmp_mvar_value_11 == NULL ))
                    {
                        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COORD );
                    }

                    if ( tmp_mvar_value_11 == NULL )
                    {
                        Py_DECREF( tmp_dictset_value );
                        Py_DECREF( tmp_list_element_1 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COORD" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 28;
                        type_description_2 = "o";
                        goto frame_exception_exit_2;
                    }

                    tmp_tuple_element_8 = tmp_mvar_value_11;
                    Py_INCREF( tmp_tuple_element_8 );
                    }
                }

                PyTuple_SET_ITEM( tmp_list_element_1, 1, tmp_tuple_element_8 );
                PyList_SET_ITEM( tmp_dictset_value, 4, tmp_list_element_1 );
                tmp_res = PyObject_SetItem( locals_colorama$win32_21, const_str_plain__fields_, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 23;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }
            }
            tmp_dictset_value = MAKE_FUNCTION_colorama$win32$$$function_3___str__(  );



            tmp_res = PyObject_SetItem( locals_colorama$win32_21, const_str_plain___str__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 30;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

#if 0
            RESTORE_FRAME_EXCEPTION( frame_81d580a13321964c190a12ce2fb85ace_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_1;

            frame_exception_exit_2:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_81d580a13321964c190a12ce2fb85ace_2 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_81d580a13321964c190a12ce2fb85ace_2, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_81d580a13321964c190a12ce2fb85ace_2->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_81d580a13321964c190a12ce2fb85ace_2, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_81d580a13321964c190a12ce2fb85ace_2,
                type_description_2,
                outline_0_var___class__
            );


            // Release cached frame.
            if ( frame_81d580a13321964c190a12ce2fb85ace_2 == cache_frame_81d580a13321964c190a12ce2fb85ace_2 )
            {
                Py_DECREF( frame_81d580a13321964c190a12ce2fb85ace_2 );
            }
            cache_frame_81d580a13321964c190a12ce2fb85ace_2 = NULL;

            assertFrameObject( frame_81d580a13321964c190a12ce2fb85ace_2 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_1;

            frame_no_exception_1:;
            goto skip_nested_handling_1;
            nested_frame_exit_1:;

            goto try_except_handler_7;
            skip_nested_handling_1:;
            {
                nuitka_bool tmp_condition_result_8;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_compexpr_left_3 = tmp_class_creation_1__bases;
                CHECK_OBJECT( tmp_class_creation_1__bases_orig );
                tmp_compexpr_right_3 = tmp_class_creation_1__bases_orig;
                tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 21;

                    goto try_except_handler_7;
                }
                tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_6;
                }
                else
                {
                    goto branch_no_6;
                }
                branch_yes_6:;
                CHECK_OBJECT( tmp_class_creation_1__bases_orig );
                tmp_dictset_value = tmp_class_creation_1__bases_orig;
                tmp_res = PyObject_SetItem( locals_colorama$win32_21, const_str_plain___orig_bases__, tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 21;

                    goto try_except_handler_7;
                }
                branch_no_6:;
            }
            {
                PyObject *tmp_assign_source_28;
                PyObject *tmp_called_name_3;
                PyObject *tmp_args_name_2;
                PyObject *tmp_tuple_element_9;
                PyObject *tmp_kw_name_2;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_called_name_3 = tmp_class_creation_1__metaclass;
                tmp_tuple_element_9 = const_str_plain_CONSOLE_SCREEN_BUFFER_INFO;
                tmp_args_name_2 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_9 );
                PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_9 );
                CHECK_OBJECT( tmp_class_creation_1__bases );
                tmp_tuple_element_9 = tmp_class_creation_1__bases;
                Py_INCREF( tmp_tuple_element_9 );
                PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_9 );
                tmp_tuple_element_9 = locals_colorama$win32_21;
                Py_INCREF( tmp_tuple_element_9 );
                PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_9 );
                CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
                tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
                frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 21;
                tmp_assign_source_28 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_2, tmp_kw_name_2 );
                Py_DECREF( tmp_args_name_2 );
                if ( tmp_assign_source_28 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 21;

                    goto try_except_handler_7;
                }
                assert( outline_0_var___class__ == NULL );
                outline_0_var___class__ = tmp_assign_source_28;
            }
            CHECK_OBJECT( outline_0_var___class__ );
            tmp_assign_source_27 = outline_0_var___class__;
            Py_INCREF( tmp_assign_source_27 );
            goto try_return_handler_7;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( colorama$win32 );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_7:;
            Py_DECREF( locals_colorama$win32_21 );
            locals_colorama$win32_21 = NULL;
            goto try_return_handler_6;
            // Exception handler code:
            try_except_handler_7:;
            exception_keeper_type_4 = exception_type;
            exception_keeper_value_4 = exception_value;
            exception_keeper_tb_4 = exception_tb;
            exception_keeper_lineno_4 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_DECREF( locals_colorama$win32_21 );
            locals_colorama$win32_21 = NULL;
            // Re-raise.
            exception_type = exception_keeper_type_4;
            exception_value = exception_keeper_value_4;
            exception_tb = exception_keeper_tb_4;
            exception_lineno = exception_keeper_lineno_4;

            goto try_except_handler_6;
            // End of try:
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( colorama$win32 );
            return MOD_RETURN_VALUE( NULL );
            // Return handler code:
            try_return_handler_6:;
            CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
            Py_DECREF( outline_0_var___class__ );
            outline_0_var___class__ = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_6:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Re-raise.
            exception_type = exception_keeper_type_5;
            exception_value = exception_keeper_value_5;
            exception_tb = exception_keeper_tb_5;
            exception_lineno = exception_keeper_lineno_5;

            goto outline_exception_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( colorama$win32 );
            return MOD_RETURN_VALUE( NULL );
            outline_exception_1:;
            exception_lineno = 21;
            goto try_except_handler_5;
            outline_result_1:;
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_CONSOLE_SCREEN_BUFFER_INFO, tmp_assign_source_27 );
        }
        goto try_end_4;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_class_creation_1__bases_orig );
        tmp_class_creation_1__bases_orig = NULL;

        Py_XDECREF( tmp_class_creation_1__bases );
        tmp_class_creation_1__bases = NULL;

        Py_XDECREF( tmp_class_creation_1__class_decl_dict );
        tmp_class_creation_1__class_decl_dict = NULL;

        Py_XDECREF( tmp_class_creation_1__metaclass );
        tmp_class_creation_1__metaclass = NULL;

        Py_XDECREF( tmp_class_creation_1__prepared );
        tmp_class_creation_1__prepared = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto try_except_handler_1;
        // End of try:
        try_end_4:;
        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases_orig );
        Py_DECREF( tmp_class_creation_1__bases_orig );
        tmp_class_creation_1__bases_orig = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
        Py_DECREF( tmp_class_creation_1__bases );
        tmp_class_creation_1__bases = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
        Py_DECREF( tmp_class_creation_1__class_decl_dict );
        tmp_class_creation_1__class_decl_dict = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
        Py_DECREF( tmp_class_creation_1__metaclass );
        tmp_class_creation_1__metaclass = NULL;

        CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
        Py_DECREF( tmp_class_creation_1__prepared );
        tmp_class_creation_1__prepared = NULL;

        {
            PyObject *tmp_assign_source_29;
            PyObject *tmp_source_name_9;
            PyObject *tmp_source_name_10;
            PyObject *tmp_mvar_value_12;
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_windll );

            if (unlikely( tmp_mvar_value_12 == NULL ))
            {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_windll );
            }

            if ( tmp_mvar_value_12 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "windll" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 39;

                goto try_except_handler_1;
            }

            tmp_source_name_10 = tmp_mvar_value_12;
            tmp_source_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_kernel32 );
            if ( tmp_source_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;

                goto try_except_handler_1;
            }
            tmp_assign_source_29 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_GetStdHandle );
            Py_DECREF( tmp_source_name_9 );
            if ( tmp_assign_source_29 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 39;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__GetStdHandle, tmp_assign_source_29 );
        }
        {
            PyObject *tmp_assattr_name_3;
            PyObject *tmp_list_element_2;
            PyObject *tmp_source_name_11;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_assattr_target_3;
            PyObject *tmp_mvar_value_14;
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_13 == NULL ))
            {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_13 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 41;

                goto try_except_handler_1;
            }

            tmp_source_name_11 = tmp_mvar_value_13;
            tmp_list_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_DWORD );
            if ( tmp_list_element_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;

                goto try_except_handler_1;
            }
            tmp_assattr_name_3 = PyList_New( 1 );
            PyList_SET_ITEM( tmp_assattr_name_3, 0, tmp_list_element_2 );
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__GetStdHandle );

            if (unlikely( tmp_mvar_value_14 == NULL ))
            {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__GetStdHandle );
            }

            if ( tmp_mvar_value_14 == NULL )
            {
                Py_DECREF( tmp_assattr_name_3 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_GetStdHandle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 40;

                goto try_except_handler_1;
            }

            tmp_assattr_target_3 = tmp_mvar_value_14;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_argtypes, tmp_assattr_name_3 );
            Py_DECREF( tmp_assattr_name_3 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assattr_name_4;
            PyObject *tmp_source_name_12;
            PyObject *tmp_mvar_value_15;
            PyObject *tmp_assattr_target_4;
            PyObject *tmp_mvar_value_16;
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_15 == NULL ))
            {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_15 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 43;

                goto try_except_handler_1;
            }

            tmp_source_name_12 = tmp_mvar_value_15;
            tmp_assattr_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_HANDLE );
            if ( tmp_assattr_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_1;
            }
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__GetStdHandle );

            if (unlikely( tmp_mvar_value_16 == NULL ))
            {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__GetStdHandle );
            }

            if ( tmp_mvar_value_16 == NULL )
            {
                Py_DECREF( tmp_assattr_name_4 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_GetStdHandle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 43;

                goto try_except_handler_1;
            }

            tmp_assattr_target_4 = tmp_mvar_value_16;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_restype, tmp_assattr_name_4 );
            Py_DECREF( tmp_assattr_name_4 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 43;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assign_source_30;
            PyObject *tmp_source_name_13;
            PyObject *tmp_source_name_14;
            PyObject *tmp_mvar_value_17;
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_windll );

            if (unlikely( tmp_mvar_value_17 == NULL ))
            {
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_windll );
            }

            if ( tmp_mvar_value_17 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "windll" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 45;

                goto try_except_handler_1;
            }

            tmp_source_name_14 = tmp_mvar_value_17;
            tmp_source_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_kernel32 );
            if ( tmp_source_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;

                goto try_except_handler_1;
            }
            tmp_assign_source_30 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_GetConsoleScreenBufferInfo );
            Py_DECREF( tmp_source_name_13 );
            if ( tmp_assign_source_30 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 45;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__GetConsoleScreenBufferInfo, tmp_assign_source_30 );
        }
        {
            PyObject *tmp_assattr_name_5;
            PyObject *tmp_list_element_3;
            PyObject *tmp_source_name_15;
            PyObject *tmp_mvar_value_18;
            PyObject *tmp_called_name_4;
            PyObject *tmp_mvar_value_19;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_mvar_value_20;
            PyObject *tmp_assattr_target_5;
            PyObject *tmp_mvar_value_21;
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_18 == NULL ))
            {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_18 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 47;

                goto try_except_handler_1;
            }

            tmp_source_name_15 = tmp_mvar_value_18;
            tmp_list_element_3 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_HANDLE );
            if ( tmp_list_element_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 47;

                goto try_except_handler_1;
            }
            tmp_assattr_name_5 = PyList_New( 2 );
            PyList_SET_ITEM( tmp_assattr_name_5, 0, tmp_list_element_3 );
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_POINTER );

            if (unlikely( tmp_mvar_value_19 == NULL ))
            {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_POINTER );
            }

            if ( tmp_mvar_value_19 == NULL )
            {
                Py_DECREF( tmp_assattr_name_5 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "POINTER" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 48;

                goto try_except_handler_1;
            }

            tmp_called_name_4 = tmp_mvar_value_19;
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_CONSOLE_SCREEN_BUFFER_INFO );

            if (unlikely( tmp_mvar_value_20 == NULL ))
            {
                tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CONSOLE_SCREEN_BUFFER_INFO );
            }

            if ( tmp_mvar_value_20 == NULL )
            {
                Py_DECREF( tmp_assattr_name_5 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CONSOLE_SCREEN_BUFFER_INFO" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 48;

                goto try_except_handler_1;
            }

            tmp_args_element_name_2 = tmp_mvar_value_20;
            frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 48;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_list_element_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            if ( tmp_list_element_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_5 );

                exception_lineno = 48;

                goto try_except_handler_1;
            }
            PyList_SET_ITEM( tmp_assattr_name_5, 1, tmp_list_element_3 );
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__GetConsoleScreenBufferInfo );

            if (unlikely( tmp_mvar_value_21 == NULL ))
            {
                tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__GetConsoleScreenBufferInfo );
            }

            if ( tmp_mvar_value_21 == NULL )
            {
                Py_DECREF( tmp_assattr_name_5 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_GetConsoleScreenBufferInfo" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 46;

                goto try_except_handler_1;
            }

            tmp_assattr_target_5 = tmp_mvar_value_21;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_argtypes, tmp_assattr_name_5 );
            Py_DECREF( tmp_assattr_name_5 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assattr_name_6;
            PyObject *tmp_source_name_16;
            PyObject *tmp_mvar_value_22;
            PyObject *tmp_assattr_target_6;
            PyObject *tmp_mvar_value_23;
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_22 == NULL ))
            {
                tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_22 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 50;

                goto try_except_handler_1;
            }

            tmp_source_name_16 = tmp_mvar_value_22;
            tmp_assattr_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_BOOL );
            if ( tmp_assattr_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;

                goto try_except_handler_1;
            }
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__GetConsoleScreenBufferInfo );

            if (unlikely( tmp_mvar_value_23 == NULL ))
            {
                tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__GetConsoleScreenBufferInfo );
            }

            if ( tmp_mvar_value_23 == NULL )
            {
                Py_DECREF( tmp_assattr_name_6 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_GetConsoleScreenBufferInfo" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 50;

                goto try_except_handler_1;
            }

            tmp_assattr_target_6 = tmp_mvar_value_23;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain_restype, tmp_assattr_name_6 );
            Py_DECREF( tmp_assattr_name_6 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 50;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_source_name_17;
            PyObject *tmp_source_name_18;
            PyObject *tmp_mvar_value_24;
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_windll );

            if (unlikely( tmp_mvar_value_24 == NULL ))
            {
                tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_windll );
            }

            if ( tmp_mvar_value_24 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "windll" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 52;

                goto try_except_handler_1;
            }

            tmp_source_name_18 = tmp_mvar_value_24;
            tmp_source_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_kernel32 );
            if ( tmp_source_name_17 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;

                goto try_except_handler_1;
            }
            tmp_assign_source_31 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_SetConsoleTextAttribute );
            Py_DECREF( tmp_source_name_17 );
            if ( tmp_assign_source_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 52;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleTextAttribute, tmp_assign_source_31 );
        }
        {
            PyObject *tmp_assattr_name_7;
            PyObject *tmp_list_element_4;
            PyObject *tmp_source_name_19;
            PyObject *tmp_mvar_value_25;
            PyObject *tmp_source_name_20;
            PyObject *tmp_mvar_value_26;
            PyObject *tmp_assattr_target_7;
            PyObject *tmp_mvar_value_27;
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_25 == NULL ))
            {
                tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_25 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 54;

                goto try_except_handler_1;
            }

            tmp_source_name_19 = tmp_mvar_value_25;
            tmp_list_element_4 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_HANDLE );
            if ( tmp_list_element_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 54;

                goto try_except_handler_1;
            }
            tmp_assattr_name_7 = PyList_New( 2 );
            PyList_SET_ITEM( tmp_assattr_name_7, 0, tmp_list_element_4 );
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_26 == NULL ))
            {
                tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_26 == NULL )
            {
                Py_DECREF( tmp_assattr_name_7 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 55;

                goto try_except_handler_1;
            }

            tmp_source_name_20 = tmp_mvar_value_26;
            tmp_list_element_4 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_WORD );
            if ( tmp_list_element_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_7 );

                exception_lineno = 55;

                goto try_except_handler_1;
            }
            PyList_SET_ITEM( tmp_assattr_name_7, 1, tmp_list_element_4 );
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleTextAttribute );

            if (unlikely( tmp_mvar_value_27 == NULL ))
            {
                tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__SetConsoleTextAttribute );
            }

            if ( tmp_mvar_value_27 == NULL )
            {
                Py_DECREF( tmp_assattr_name_7 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_SetConsoleTextAttribute" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 53;

                goto try_except_handler_1;
            }

            tmp_assattr_target_7 = tmp_mvar_value_27;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain_argtypes, tmp_assattr_name_7 );
            Py_DECREF( tmp_assattr_name_7 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 53;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assattr_name_8;
            PyObject *tmp_source_name_21;
            PyObject *tmp_mvar_value_28;
            PyObject *tmp_assattr_target_8;
            PyObject *tmp_mvar_value_29;
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_28 == NULL ))
            {
                tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_28 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 57;

                goto try_except_handler_1;
            }

            tmp_source_name_21 = tmp_mvar_value_28;
            tmp_assattr_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_BOOL );
            if ( tmp_assattr_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 57;

                goto try_except_handler_1;
            }
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleTextAttribute );

            if (unlikely( tmp_mvar_value_29 == NULL ))
            {
                tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__SetConsoleTextAttribute );
            }

            if ( tmp_mvar_value_29 == NULL )
            {
                Py_DECREF( tmp_assattr_name_8 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_SetConsoleTextAttribute" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 57;

                goto try_except_handler_1;
            }

            tmp_assattr_target_8 = tmp_mvar_value_29;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain_restype, tmp_assattr_name_8 );
            Py_DECREF( tmp_assattr_name_8 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 57;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assign_source_32;
            PyObject *tmp_source_name_22;
            PyObject *tmp_source_name_23;
            PyObject *tmp_mvar_value_30;
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_windll );

            if (unlikely( tmp_mvar_value_30 == NULL ))
            {
                tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_windll );
            }

            if ( tmp_mvar_value_30 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "windll" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 59;

                goto try_except_handler_1;
            }

            tmp_source_name_23 = tmp_mvar_value_30;
            tmp_source_name_22 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_kernel32 );
            if ( tmp_source_name_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;

                goto try_except_handler_1;
            }
            tmp_assign_source_32 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_SetConsoleCursorPosition );
            Py_DECREF( tmp_source_name_22 );
            if ( tmp_assign_source_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleCursorPosition, tmp_assign_source_32 );
        }
        {
            PyObject *tmp_assattr_name_9;
            PyObject *tmp_list_element_5;
            PyObject *tmp_source_name_24;
            PyObject *tmp_mvar_value_31;
            PyObject *tmp_mvar_value_32;
            PyObject *tmp_assattr_target_9;
            PyObject *tmp_mvar_value_33;
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_31 == NULL ))
            {
                tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_31 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 61;

                goto try_except_handler_1;
            }

            tmp_source_name_24 = tmp_mvar_value_31;
            tmp_list_element_5 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_HANDLE );
            if ( tmp_list_element_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 61;

                goto try_except_handler_1;
            }
            tmp_assattr_name_9 = PyList_New( 2 );
            PyList_SET_ITEM( tmp_assattr_name_9, 0, tmp_list_element_5 );
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_COORD );

            if (unlikely( tmp_mvar_value_32 == NULL ))
            {
                tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COORD );
            }

            if ( tmp_mvar_value_32 == NULL )
            {
                Py_DECREF( tmp_assattr_name_9 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COORD" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 62;

                goto try_except_handler_1;
            }

            tmp_list_element_5 = tmp_mvar_value_32;
            Py_INCREF( tmp_list_element_5 );
            PyList_SET_ITEM( tmp_assattr_name_9, 1, tmp_list_element_5 );
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleCursorPosition );

            if (unlikely( tmp_mvar_value_33 == NULL ))
            {
                tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__SetConsoleCursorPosition );
            }

            if ( tmp_mvar_value_33 == NULL )
            {
                Py_DECREF( tmp_assattr_name_9 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_SetConsoleCursorPosition" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 60;

                goto try_except_handler_1;
            }

            tmp_assattr_target_9 = tmp_mvar_value_33;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_9, const_str_plain_argtypes, tmp_assattr_name_9 );
            Py_DECREF( tmp_assattr_name_9 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assattr_name_10;
            PyObject *tmp_source_name_25;
            PyObject *tmp_mvar_value_34;
            PyObject *tmp_assattr_target_10;
            PyObject *tmp_mvar_value_35;
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_34 == NULL ))
            {
                tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_34 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 64;

                goto try_except_handler_1;
            }

            tmp_source_name_25 = tmp_mvar_value_34;
            tmp_assattr_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_BOOL );
            if ( tmp_assattr_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;

                goto try_except_handler_1;
            }
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleCursorPosition );

            if (unlikely( tmp_mvar_value_35 == NULL ))
            {
                tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__SetConsoleCursorPosition );
            }

            if ( tmp_mvar_value_35 == NULL )
            {
                Py_DECREF( tmp_assattr_name_10 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_SetConsoleCursorPosition" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 64;

                goto try_except_handler_1;
            }

            tmp_assattr_target_10 = tmp_mvar_value_35;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_10, const_str_plain_restype, tmp_assattr_name_10 );
            Py_DECREF( tmp_assattr_name_10 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assign_source_33;
            PyObject *tmp_source_name_26;
            PyObject *tmp_source_name_27;
            PyObject *tmp_mvar_value_36;
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_windll );

            if (unlikely( tmp_mvar_value_36 == NULL ))
            {
                tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_windll );
            }

            if ( tmp_mvar_value_36 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "windll" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 66;

                goto try_except_handler_1;
            }

            tmp_source_name_27 = tmp_mvar_value_36;
            tmp_source_name_26 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_kernel32 );
            if ( tmp_source_name_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_1;
            }
            tmp_assign_source_33 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_FillConsoleOutputCharacterA );
            Py_DECREF( tmp_source_name_26 );
            if ( tmp_assign_source_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 66;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputCharacterA, tmp_assign_source_33 );
        }
        {
            PyObject *tmp_assattr_name_11;
            PyObject *tmp_list_element_6;
            PyObject *tmp_source_name_28;
            PyObject *tmp_mvar_value_37;
            PyObject *tmp_mvar_value_38;
            PyObject *tmp_source_name_29;
            PyObject *tmp_mvar_value_39;
            PyObject *tmp_mvar_value_40;
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_41;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_source_name_30;
            PyObject *tmp_mvar_value_42;
            PyObject *tmp_assattr_target_11;
            PyObject *tmp_mvar_value_43;
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_37 == NULL ))
            {
                tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_37 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 68;

                goto try_except_handler_1;
            }

            tmp_source_name_28 = tmp_mvar_value_37;
            tmp_list_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_HANDLE );
            if ( tmp_list_element_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 68;

                goto try_except_handler_1;
            }
            tmp_assattr_name_11 = PyList_New( 5 );
            PyList_SET_ITEM( tmp_assattr_name_11, 0, tmp_list_element_6 );
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_c_char );

            if (unlikely( tmp_mvar_value_38 == NULL ))
            {
                tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_c_char );
            }

            if ( tmp_mvar_value_38 == NULL )
            {
                Py_DECREF( tmp_assattr_name_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "c_char" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 69;

                goto try_except_handler_1;
            }

            tmp_list_element_6 = tmp_mvar_value_38;
            Py_INCREF( tmp_list_element_6 );
            PyList_SET_ITEM( tmp_assattr_name_11, 1, tmp_list_element_6 );
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_39 == NULL ))
            {
                tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_39 == NULL )
            {
                Py_DECREF( tmp_assattr_name_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 70;

                goto try_except_handler_1;
            }

            tmp_source_name_29 = tmp_mvar_value_39;
            tmp_list_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_DWORD );
            if ( tmp_list_element_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_11 );

                exception_lineno = 70;

                goto try_except_handler_1;
            }
            PyList_SET_ITEM( tmp_assattr_name_11, 2, tmp_list_element_6 );
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_COORD );

            if (unlikely( tmp_mvar_value_40 == NULL ))
            {
                tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COORD );
            }

            if ( tmp_mvar_value_40 == NULL )
            {
                Py_DECREF( tmp_assattr_name_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COORD" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 71;

                goto try_except_handler_1;
            }

            tmp_list_element_6 = tmp_mvar_value_40;
            Py_INCREF( tmp_list_element_6 );
            PyList_SET_ITEM( tmp_assattr_name_11, 3, tmp_list_element_6 );
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_POINTER );

            if (unlikely( tmp_mvar_value_41 == NULL ))
            {
                tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_POINTER );
            }

            if ( tmp_mvar_value_41 == NULL )
            {
                Py_DECREF( tmp_assattr_name_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "POINTER" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 72;

                goto try_except_handler_1;
            }

            tmp_called_name_5 = tmp_mvar_value_41;
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_42 == NULL ))
            {
                tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_42 == NULL )
            {
                Py_DECREF( tmp_assattr_name_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 72;

                goto try_except_handler_1;
            }

            tmp_source_name_30 = tmp_mvar_value_42;
            tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_DWORD );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_11 );

                exception_lineno = 72;

                goto try_except_handler_1;
            }
            frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 72;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_list_element_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_list_element_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_11 );

                exception_lineno = 72;

                goto try_except_handler_1;
            }
            PyList_SET_ITEM( tmp_assattr_name_11, 4, tmp_list_element_6 );
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputCharacterA );

            if (unlikely( tmp_mvar_value_43 == NULL ))
            {
                tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputCharacterA );
            }

            if ( tmp_mvar_value_43 == NULL )
            {
                Py_DECREF( tmp_assattr_name_11 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_FillConsoleOutputCharacterA" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 67;

                goto try_except_handler_1;
            }

            tmp_assattr_target_11 = tmp_mvar_value_43;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_11, const_str_plain_argtypes, tmp_assattr_name_11 );
            Py_DECREF( tmp_assattr_name_11 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assattr_name_12;
            PyObject *tmp_source_name_31;
            PyObject *tmp_mvar_value_44;
            PyObject *tmp_assattr_target_12;
            PyObject *tmp_mvar_value_45;
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_44 == NULL ))
            {
                tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_44 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 74;

                goto try_except_handler_1;
            }

            tmp_source_name_31 = tmp_mvar_value_44;
            tmp_assattr_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_BOOL );
            if ( tmp_assattr_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;

                goto try_except_handler_1;
            }
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputCharacterA );

            if (unlikely( tmp_mvar_value_45 == NULL ))
            {
                tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputCharacterA );
            }

            if ( tmp_mvar_value_45 == NULL )
            {
                Py_DECREF( tmp_assattr_name_12 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_FillConsoleOutputCharacterA" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 74;

                goto try_except_handler_1;
            }

            tmp_assattr_target_12 = tmp_mvar_value_45;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_12, const_str_plain_restype, tmp_assattr_name_12 );
            Py_DECREF( tmp_assattr_name_12 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assign_source_34;
            PyObject *tmp_source_name_32;
            PyObject *tmp_source_name_33;
            PyObject *tmp_mvar_value_46;
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_windll );

            if (unlikely( tmp_mvar_value_46 == NULL ))
            {
                tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_windll );
            }

            if ( tmp_mvar_value_46 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "windll" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 76;

                goto try_except_handler_1;
            }

            tmp_source_name_33 = tmp_mvar_value_46;
            tmp_source_name_32 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain_kernel32 );
            if ( tmp_source_name_32 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;

                goto try_except_handler_1;
            }
            tmp_assign_source_34 = LOOKUP_ATTRIBUTE( tmp_source_name_32, const_str_plain_FillConsoleOutputAttribute );
            Py_DECREF( tmp_source_name_32 );
            if ( tmp_assign_source_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputAttribute, tmp_assign_source_34 );
        }
        {
            PyObject *tmp_assattr_name_13;
            PyObject *tmp_list_element_7;
            PyObject *tmp_source_name_34;
            PyObject *tmp_mvar_value_47;
            PyObject *tmp_source_name_35;
            PyObject *tmp_mvar_value_48;
            PyObject *tmp_source_name_36;
            PyObject *tmp_mvar_value_49;
            PyObject *tmp_mvar_value_50;
            PyObject *tmp_called_name_6;
            PyObject *tmp_mvar_value_51;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_source_name_37;
            PyObject *tmp_mvar_value_52;
            PyObject *tmp_assattr_target_13;
            PyObject *tmp_mvar_value_53;
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_47 == NULL ))
            {
                tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_47 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 78;

                goto try_except_handler_1;
            }

            tmp_source_name_34 = tmp_mvar_value_47;
            tmp_list_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_34, const_str_plain_HANDLE );
            if ( tmp_list_element_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 78;

                goto try_except_handler_1;
            }
            tmp_assattr_name_13 = PyList_New( 5 );
            PyList_SET_ITEM( tmp_assattr_name_13, 0, tmp_list_element_7 );
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_48 == NULL ))
            {
                tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_48 == NULL )
            {
                Py_DECREF( tmp_assattr_name_13 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 79;

                goto try_except_handler_1;
            }

            tmp_source_name_35 = tmp_mvar_value_48;
            tmp_list_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_35, const_str_plain_WORD );
            if ( tmp_list_element_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_13 );

                exception_lineno = 79;

                goto try_except_handler_1;
            }
            PyList_SET_ITEM( tmp_assattr_name_13, 1, tmp_list_element_7 );
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_49 == NULL ))
            {
                tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_49 == NULL )
            {
                Py_DECREF( tmp_assattr_name_13 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 80;

                goto try_except_handler_1;
            }

            tmp_source_name_36 = tmp_mvar_value_49;
            tmp_list_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_36, const_str_plain_DWORD );
            if ( tmp_list_element_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_13 );

                exception_lineno = 80;

                goto try_except_handler_1;
            }
            PyList_SET_ITEM( tmp_assattr_name_13, 2, tmp_list_element_7 );
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_COORD );

            if (unlikely( tmp_mvar_value_50 == NULL ))
            {
                tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_COORD );
            }

            if ( tmp_mvar_value_50 == NULL )
            {
                Py_DECREF( tmp_assattr_name_13 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "COORD" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 81;

                goto try_except_handler_1;
            }

            tmp_list_element_7 = tmp_mvar_value_50;
            Py_INCREF( tmp_list_element_7 );
            PyList_SET_ITEM( tmp_assattr_name_13, 3, tmp_list_element_7 );
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_POINTER );

            if (unlikely( tmp_mvar_value_51 == NULL ))
            {
                tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_POINTER );
            }

            if ( tmp_mvar_value_51 == NULL )
            {
                Py_DECREF( tmp_assattr_name_13 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "POINTER" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 82;

                goto try_except_handler_1;
            }

            tmp_called_name_6 = tmp_mvar_value_51;
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_52 == NULL ))
            {
                tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_52 == NULL )
            {
                Py_DECREF( tmp_assattr_name_13 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 82;

                goto try_except_handler_1;
            }

            tmp_source_name_37 = tmp_mvar_value_52;
            tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_37, const_str_plain_DWORD );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_13 );

                exception_lineno = 82;

                goto try_except_handler_1;
            }
            frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 82;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_list_element_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_list_element_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_13 );

                exception_lineno = 82;

                goto try_except_handler_1;
            }
            PyList_SET_ITEM( tmp_assattr_name_13, 4, tmp_list_element_7 );
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputAttribute );

            if (unlikely( tmp_mvar_value_53 == NULL ))
            {
                tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputAttribute );
            }

            if ( tmp_mvar_value_53 == NULL )
            {
                Py_DECREF( tmp_assattr_name_13 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_FillConsoleOutputAttribute" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 77;

                goto try_except_handler_1;
            }

            tmp_assattr_target_13 = tmp_mvar_value_53;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_13, const_str_plain_argtypes, tmp_assattr_name_13 );
            Py_DECREF( tmp_assattr_name_13 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 77;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assattr_name_14;
            PyObject *tmp_source_name_38;
            PyObject *tmp_mvar_value_54;
            PyObject *tmp_assattr_target_14;
            PyObject *tmp_mvar_value_55;
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_54 == NULL ))
            {
                tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_54 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 84;

                goto try_except_handler_1;
            }

            tmp_source_name_38 = tmp_mvar_value_54;
            tmp_assattr_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_38, const_str_plain_BOOL );
            if ( tmp_assattr_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;

                goto try_except_handler_1;
            }
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputAttribute );

            if (unlikely( tmp_mvar_value_55 == NULL ))
            {
                tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__FillConsoleOutputAttribute );
            }

            if ( tmp_mvar_value_55 == NULL )
            {
                Py_DECREF( tmp_assattr_name_14 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_FillConsoleOutputAttribute" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 84;

                goto try_except_handler_1;
            }

            tmp_assattr_target_14 = tmp_mvar_value_55;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_14, const_str_plain_restype, tmp_assattr_name_14 );
            Py_DECREF( tmp_assattr_name_14 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 84;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assign_source_35;
            PyObject *tmp_source_name_39;
            PyObject *tmp_source_name_40;
            PyObject *tmp_mvar_value_56;
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_windll );

            if (unlikely( tmp_mvar_value_56 == NULL ))
            {
                tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_windll );
            }

            if ( tmp_mvar_value_56 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "windll" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 86;

                goto try_except_handler_1;
            }

            tmp_source_name_40 = tmp_mvar_value_56;
            tmp_source_name_39 = LOOKUP_ATTRIBUTE( tmp_source_name_40, const_str_plain_kernel32 );
            if ( tmp_source_name_39 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;

                goto try_except_handler_1;
            }
            tmp_assign_source_35 = LOOKUP_ATTRIBUTE( tmp_source_name_39, const_str_plain_SetConsoleTitleW );
            Py_DECREF( tmp_source_name_39 );
            if ( tmp_assign_source_35 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 86;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleTitleW, tmp_assign_source_35 );
        }
        {
            PyObject *tmp_assattr_name_15;
            PyObject *tmp_list_element_8;
            PyObject *tmp_source_name_41;
            PyObject *tmp_mvar_value_57;
            PyObject *tmp_assattr_target_15;
            PyObject *tmp_mvar_value_58;
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_57 == NULL ))
            {
                tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_57 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 88;

                goto try_except_handler_1;
            }

            tmp_source_name_41 = tmp_mvar_value_57;
            tmp_list_element_8 = LOOKUP_ATTRIBUTE( tmp_source_name_41, const_str_plain_LPCWSTR );
            if ( tmp_list_element_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;

                goto try_except_handler_1;
            }
            tmp_assattr_name_15 = PyList_New( 1 );
            PyList_SET_ITEM( tmp_assattr_name_15, 0, tmp_list_element_8 );
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleTitleW );

            if (unlikely( tmp_mvar_value_58 == NULL ))
            {
                tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__SetConsoleTitleW );
            }

            if ( tmp_mvar_value_58 == NULL )
            {
                Py_DECREF( tmp_assattr_name_15 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_SetConsoleTitleW" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 87;

                goto try_except_handler_1;
            }

            tmp_assattr_target_15 = tmp_mvar_value_58;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_15, const_str_plain_argtypes, tmp_assattr_name_15 );
            Py_DECREF( tmp_assattr_name_15 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 87;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assattr_name_16;
            PyObject *tmp_source_name_42;
            PyObject *tmp_mvar_value_59;
            PyObject *tmp_assattr_target_16;
            PyObject *tmp_mvar_value_60;
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_wintypes );

            if (unlikely( tmp_mvar_value_59 == NULL ))
            {
                tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_wintypes );
            }

            if ( tmp_mvar_value_59 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "wintypes" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 90;

                goto try_except_handler_1;
            }

            tmp_source_name_42 = tmp_mvar_value_59;
            tmp_assattr_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_42, const_str_plain_BOOL );
            if ( tmp_assattr_name_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;

                goto try_except_handler_1;
            }
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__SetConsoleTitleW );

            if (unlikely( tmp_mvar_value_60 == NULL ))
            {
                tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__SetConsoleTitleW );
            }

            if ( tmp_mvar_value_60 == NULL )
            {
                Py_DECREF( tmp_assattr_name_16 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_SetConsoleTitleW" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 90;

                goto try_except_handler_1;
            }

            tmp_assattr_target_16 = tmp_mvar_value_60;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_16, const_str_plain_restype, tmp_assattr_name_16 );
            Py_DECREF( tmp_assattr_name_16 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;

                goto try_except_handler_1;
            }
        }
        {
            PyObject *tmp_assign_source_36;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_mvar_value_61;
            PyObject *tmp_called_name_7;
            PyObject *tmp_mvar_value_62;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_mvar_value_63;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_mvar_value_64;
            PyObject *tmp_called_name_8;
            PyObject *tmp_mvar_value_65;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_mvar_value_66;
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_STDOUT );

            if (unlikely( tmp_mvar_value_61 == NULL ))
            {
                tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STDOUT );
            }

            if ( tmp_mvar_value_61 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STDOUT" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 93;

                goto try_except_handler_1;
            }

            tmp_dict_key_1 = tmp_mvar_value_61;
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__GetStdHandle );

            if (unlikely( tmp_mvar_value_62 == NULL ))
            {
                tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__GetStdHandle );
            }

            if ( tmp_mvar_value_62 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_GetStdHandle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 93;

                goto try_except_handler_1;
            }

            tmp_called_name_7 = tmp_mvar_value_62;
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_STDOUT );

            if (unlikely( tmp_mvar_value_63 == NULL ))
            {
                tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STDOUT );
            }

            if ( tmp_mvar_value_63 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STDOUT" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 93;

                goto try_except_handler_1;
            }

            tmp_args_element_name_5 = tmp_mvar_value_63;
            frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 93;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 93;

                goto try_except_handler_1;
            }
            tmp_assign_source_36 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_assign_source_36, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assign_source_36 );

                exception_lineno = 92;

                goto try_except_handler_1;
            }
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_STDERR );

            if (unlikely( tmp_mvar_value_64 == NULL ))
            {
                tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STDERR );
            }

            if ( tmp_mvar_value_64 == NULL )
            {
                Py_DECREF( tmp_assign_source_36 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STDERR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 94;

                goto try_except_handler_1;
            }

            tmp_dict_key_2 = tmp_mvar_value_64;
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__GetStdHandle );

            if (unlikely( tmp_mvar_value_65 == NULL ))
            {
                tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__GetStdHandle );
            }

            if ( tmp_mvar_value_65 == NULL )
            {
                Py_DECREF( tmp_assign_source_36 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_GetStdHandle" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 94;

                goto try_except_handler_1;
            }

            tmp_called_name_8 = tmp_mvar_value_65;
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_STDERR );

            if (unlikely( tmp_mvar_value_66 == NULL ))
            {
                tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STDERR );
            }

            if ( tmp_mvar_value_66 == NULL )
            {
                Py_DECREF( tmp_assign_source_36 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STDERR" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 94;

                goto try_except_handler_1;
            }

            tmp_args_element_name_6 = tmp_mvar_value_66;
            frame_b5760f74927d4aa5719917afd9996a6c->m_frame.f_lineno = 94;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dict_value_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
            }

            if ( tmp_dict_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assign_source_36 );

                exception_lineno = 94;

                goto try_except_handler_1;
            }
            tmp_res = PyDict_SetItem( tmp_assign_source_36, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assign_source_36 );

                exception_lineno = 92;

                goto try_except_handler_1;
            }
            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_handles, tmp_assign_source_36 );
        }
        {
            PyObject *tmp_assign_source_37;
            tmp_assign_source_37 = MAKE_FUNCTION_colorama$win32$$$function_4__winapi_test(  );



            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain__winapi_test, tmp_assign_source_37 );
        }
        {
            PyObject *tmp_assign_source_38;
            tmp_assign_source_38 = MAKE_FUNCTION_colorama$win32$$$function_5_winapi_test(  );



            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_winapi_test, tmp_assign_source_38 );
        }
        {
            PyObject *tmp_assign_source_39;
            PyObject *tmp_defaults_1;
            PyObject *tmp_tuple_element_10;
            PyObject *tmp_mvar_value_67;
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_STDOUT );

            if (unlikely( tmp_mvar_value_67 == NULL ))
            {
                tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_STDOUT );
            }

            if ( tmp_mvar_value_67 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "STDOUT" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 106;

                goto try_except_handler_1;
            }

            tmp_tuple_element_10 = tmp_mvar_value_67;
            tmp_defaults_1 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_10 );
            PyTuple_SET_ITEM( tmp_defaults_1, 0, tmp_tuple_element_10 );
            tmp_assign_source_39 = MAKE_FUNCTION_colorama$win32$$$function_6_GetConsoleScreenBufferInfo( tmp_defaults_1 );



            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_GetConsoleScreenBufferInfo, tmp_assign_source_39 );
        }
        {
            PyObject *tmp_assign_source_40;
            tmp_assign_source_40 = MAKE_FUNCTION_colorama$win32$$$function_7_SetConsoleTextAttribute(  );



            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_SetConsoleTextAttribute, tmp_assign_source_40 );
        }
        {
            PyObject *tmp_assign_source_41;
            PyObject *tmp_defaults_2;
            tmp_defaults_2 = const_tuple_true_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_assign_source_41 = MAKE_FUNCTION_colorama$win32$$$function_8_SetConsoleCursorPosition( tmp_defaults_2 );



            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_SetConsoleCursorPosition, tmp_assign_source_41 );
        }
        {
            PyObject *tmp_assign_source_42;
            tmp_assign_source_42 = MAKE_FUNCTION_colorama$win32$$$function_9_FillConsoleOutputCharacter(  );



            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_FillConsoleOutputCharacter, tmp_assign_source_42 );
        }
        {
            PyObject *tmp_assign_source_43;
            tmp_assign_source_43 = MAKE_FUNCTION_colorama$win32$$$function_10_FillConsoleOutputAttribute(  );



            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_FillConsoleOutputAttribute, tmp_assign_source_43 );
        }
        {
            PyObject *tmp_assign_source_44;
            tmp_assign_source_44 = MAKE_FUNCTION_colorama$win32$$$function_11_SetConsoleTitle(  );



            UPDATE_STRING_DICT1( moduledict_colorama$win32, (Nuitka_StringObject *)const_str_plain_SetConsoleTitle, tmp_assign_source_44 );
        }
        branch_no_2:;
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b5760f74927d4aa5719917afd9996a6c );
#endif
    popFrameStack();

    assertFrameObject( frame_b5760f74927d4aa5719917afd9996a6c );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b5760f74927d4aa5719917afd9996a6c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b5760f74927d4aa5719917afd9996a6c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b5760f74927d4aa5719917afd9996a6c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b5760f74927d4aa5719917afd9996a6c, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;

    return MOD_RETURN_VALUE( module_colorama$win32 );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
