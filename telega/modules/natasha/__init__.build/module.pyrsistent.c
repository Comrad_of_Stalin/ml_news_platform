/* Generated code for Python module 'pyrsistent'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_pyrsistent" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pyrsistent;
PyDictObject *moduledict_pyrsistent;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_PList;
static PyObject *const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple;
extern PyObject *const_str_plain_rex;
extern PyObject *const_str_plain_pmap;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_pvector;
extern PyObject *const_str_digest_56627b427bf195e543503a696c72fe95;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_immutable;
extern PyObject *const_str_digest_4e9ceaddc207dd32785ccdb8162f67c8;
extern PyObject *const_str_plain_CheckedPMap;
extern PyObject *const_str_plain_PSet;
extern PyObject *const_str_plain_pset_field;
extern PyObject *const_str_plain_m;
extern PyObject *const_str_digest_f15741d45e8011305189f1568b9f9f78;
static PyObject *const_str_plain_NUITKA_PACKAGE_pyrsistent;
extern PyObject *const_str_digest_558b02d50f93abc24b9cc91580d4be6e;
extern PyObject *const_str_plain_PVector;
static PyObject *const_tuple_str_plain_immutable_tuple;
extern PyObject *const_str_plain_freeze;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_digest_bbcd3eedb95ca64812664a529506b818;
extern PyObject *const_str_plain_environ;
extern PyObject *const_str_digest_2ef06c78dedff1f54ddea6126ab11898;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_digest_1acfec9db8a06e568fb00194707d7493;
extern PyObject *const_str_plain_PClass;
static PyObject *const_tuple_str_plain_freeze_str_plain_thaw_str_plain_mutant_tuple;
extern PyObject *const_str_plain_CheckedPSet;
extern PyObject *const_str_plain_mutant;
extern PyObject *const_str_plain_path;
extern PyObject *const_str_plain_PTypeError;
extern PyObject *const_str_digest_ff0a2eed9efce2dbbaa53ac5b4a3cb56;
extern PyObject *const_str_plain_dq;
static PyObject *const_tuple_d12bf79c1b789da3cabcb8e253b992d9_tuple;
static PyObject *const_tuple_str_plain_pvector_str_plain_v_str_plain_PVector_tuple;
extern PyObject *const_str_plain_PBag;
static PyObject *const_tuple_str_plain_plist_str_plain_l_str_plain_PList_tuple;
extern PyObject *const_str_plain_PMap;
static PyObject *const_tuple_str_plain_PRecord_tuple;
static PyObject *const_tuple_str_plain_get_in_tuple;
extern PyObject *const_str_plain_l;
extern PyObject *const_str_plain___path__;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_thaw;
extern PyObject *const_str_digest_811ebf3d380eed202488e73f18944a13;
extern PyObject *const_str_plain_pset;
extern PyObject *const_str_plain_b;
extern PyObject *const_str_plain_get_in;
extern PyObject *const_str_plain_CheckedValueTypeError;
static PyObject *const_tuple_518a93834beb9d231038ff60ca3253d4_tuple;
extern PyObject *const_str_plain___all__;
extern PyObject *const_str_plain_pyrsistent;
extern PyObject *const_str_plain_CheckedPVector;
extern PyObject *const_str_digest_da2247a8fd410289ecfcc9f4e6c3a584;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_InvariantException;
extern PyObject *const_str_plain_ny;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_a8050813fbf1722354a903bae411c27a;
extern PyObject *const_str_plain_dirname;
extern PyObject *const_str_plain_PClassMeta;
static PyObject *const_tuple_str_plain_pset_str_plain_s_str_plain_PSet_tuple;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_v;
static PyObject *const_tuple_str_plain_pmap_str_plain_m_str_plain_PMap_tuple;
extern PyObject *const_str_plain_pvector_field;
extern PyObject *const_str_plain_inc;
extern PyObject *const_str_plain_optional;
extern PyObject *const_str_plain_plist;
extern PyObject *const_str_plain_pdeque;
extern PyObject *const_str_plain_CheckedKeyTypeError;
extern PyObject *const_str_digest_d0293e53a57025d5190dd31692d88ab3;
extern PyObject *const_str_digest_e844d5dee179ba0f412361a73fa49293;
extern PyObject *const_str_plain_field;
static PyObject *const_tuple_str_plain_PClass_str_plain_PClassMeta_tuple;
static PyObject *const_tuple_str_plain_pbag_str_plain_b_str_plain_PBag_tuple;
extern PyObject *const_str_plain_CheckedType;
static PyObject *const_tuple_str_plain_pdeque_str_plain_dq_str_plain_PDeque_tuple;
extern PyObject *const_str_digest_5bfaf90dbd407b4fc29090c8f6415242;
extern PyObject *const_str_plain_pbag;
static PyObject *const_tuple_str_plain_inc_str_plain_discard_str_plain_rex_str_plain_ny_tuple;
static PyObject *const_tuple_d50d7f8498d7d71b754176a25f6364d1_tuple;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_PDeque;
extern PyObject *const_str_plain_discard;
extern PyObject *const_str_plain_pmap_field;
extern PyObject *const_str_plain_PRecord;
extern PyObject *const_str_digest_1e3d53cc9681554a4d7ed16674ec1ac9;
extern PyObject *const_str_digest_fe02a1170f09a52095603d103fefd814;
static PyObject *const_str_digest_0067e2ce01e22b757be66360e5e0b81e;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple, 0, const_str_plain_CheckedPMap ); Py_INCREF( const_str_plain_CheckedPMap );
    PyTuple_SET_ITEM( const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple, 1, const_str_plain_CheckedPVector ); Py_INCREF( const_str_plain_CheckedPVector );
    PyTuple_SET_ITEM( const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple, 2, const_str_plain_CheckedPSet ); Py_INCREF( const_str_plain_CheckedPSet );
    PyTuple_SET_ITEM( const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple, 3, const_str_plain_InvariantException ); Py_INCREF( const_str_plain_InvariantException );
    PyTuple_SET_ITEM( const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple, 4, const_str_plain_CheckedKeyTypeError ); Py_INCREF( const_str_plain_CheckedKeyTypeError );
    PyTuple_SET_ITEM( const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple, 5, const_str_plain_CheckedValueTypeError ); Py_INCREF( const_str_plain_CheckedValueTypeError );
    PyTuple_SET_ITEM( const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple, 6, const_str_plain_CheckedType ); Py_INCREF( const_str_plain_CheckedType );
    PyTuple_SET_ITEM( const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple, 7, const_str_plain_optional ); Py_INCREF( const_str_plain_optional );
    const_str_plain_NUITKA_PACKAGE_pyrsistent = UNSTREAM_STRING_ASCII( &constant_bin[ 5189885 ], 25, 1 );
    const_tuple_str_plain_immutable_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_immutable_tuple, 0, const_str_plain_immutable ); Py_INCREF( const_str_plain_immutable );
    const_tuple_str_plain_freeze_str_plain_thaw_str_plain_mutant_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_freeze_str_plain_thaw_str_plain_mutant_tuple, 0, const_str_plain_freeze ); Py_INCREF( const_str_plain_freeze );
    PyTuple_SET_ITEM( const_tuple_str_plain_freeze_str_plain_thaw_str_plain_mutant_tuple, 1, const_str_plain_thaw ); Py_INCREF( const_str_plain_thaw );
    PyTuple_SET_ITEM( const_tuple_str_plain_freeze_str_plain_thaw_str_plain_mutant_tuple, 2, const_str_plain_mutant ); Py_INCREF( const_str_plain_mutant );
    const_tuple_d12bf79c1b789da3cabcb8e253b992d9_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_d12bf79c1b789da3cabcb8e253b992d9_tuple, 0, const_str_plain_NUITKA_PACKAGE_pyrsistent ); Py_INCREF( const_str_plain_NUITKA_PACKAGE_pyrsistent );
    PyTuple_SET_ITEM( const_tuple_d12bf79c1b789da3cabcb8e253b992d9_tuple, 1, const_str_digest_5bfaf90dbd407b4fc29090c8f6415242 ); Py_INCREF( const_str_digest_5bfaf90dbd407b4fc29090c8f6415242 );
    const_tuple_str_plain_pvector_str_plain_v_str_plain_PVector_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_pvector_str_plain_v_str_plain_PVector_tuple, 0, const_str_plain_pvector ); Py_INCREF( const_str_plain_pvector );
    PyTuple_SET_ITEM( const_tuple_str_plain_pvector_str_plain_v_str_plain_PVector_tuple, 1, const_str_plain_v ); Py_INCREF( const_str_plain_v );
    PyTuple_SET_ITEM( const_tuple_str_plain_pvector_str_plain_v_str_plain_PVector_tuple, 2, const_str_plain_PVector ); Py_INCREF( const_str_plain_PVector );
    const_tuple_str_plain_plist_str_plain_l_str_plain_PList_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_plist_str_plain_l_str_plain_PList_tuple, 0, const_str_plain_plist ); Py_INCREF( const_str_plain_plist );
    PyTuple_SET_ITEM( const_tuple_str_plain_plist_str_plain_l_str_plain_PList_tuple, 1, const_str_plain_l ); Py_INCREF( const_str_plain_l );
    PyTuple_SET_ITEM( const_tuple_str_plain_plist_str_plain_l_str_plain_PList_tuple, 2, const_str_plain_PList ); Py_INCREF( const_str_plain_PList );
    const_tuple_str_plain_PRecord_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PRecord_tuple, 0, const_str_plain_PRecord ); Py_INCREF( const_str_plain_PRecord );
    const_tuple_str_plain_get_in_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_get_in_tuple, 0, const_str_plain_get_in ); Py_INCREF( const_str_plain_get_in );
    const_tuple_518a93834beb9d231038ff60ca3253d4_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 5189910 ], 373 );
    const_str_digest_a8050813fbf1722354a903bae411c27a = UNSTREAM_STRING_ASCII( &constant_bin[ 5190283 ], 19, 0 );
    const_tuple_str_plain_pset_str_plain_s_str_plain_PSet_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_pset_str_plain_s_str_plain_PSet_tuple, 0, const_str_plain_pset ); Py_INCREF( const_str_plain_pset );
    PyTuple_SET_ITEM( const_tuple_str_plain_pset_str_plain_s_str_plain_PSet_tuple, 1, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_str_plain_pset_str_plain_s_str_plain_PSet_tuple, 2, const_str_plain_PSet ); Py_INCREF( const_str_plain_PSet );
    const_tuple_str_plain_pmap_str_plain_m_str_plain_PMap_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_pmap_str_plain_m_str_plain_PMap_tuple, 0, const_str_plain_pmap ); Py_INCREF( const_str_plain_pmap );
    PyTuple_SET_ITEM( const_tuple_str_plain_pmap_str_plain_m_str_plain_PMap_tuple, 1, const_str_plain_m ); Py_INCREF( const_str_plain_m );
    PyTuple_SET_ITEM( const_tuple_str_plain_pmap_str_plain_m_str_plain_PMap_tuple, 2, const_str_plain_PMap ); Py_INCREF( const_str_plain_PMap );
    const_tuple_str_plain_PClass_str_plain_PClassMeta_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PClass_str_plain_PClassMeta_tuple, 0, const_str_plain_PClass ); Py_INCREF( const_str_plain_PClass );
    PyTuple_SET_ITEM( const_tuple_str_plain_PClass_str_plain_PClassMeta_tuple, 1, const_str_plain_PClassMeta ); Py_INCREF( const_str_plain_PClassMeta );
    const_tuple_str_plain_pbag_str_plain_b_str_plain_PBag_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_pbag_str_plain_b_str_plain_PBag_tuple, 0, const_str_plain_pbag ); Py_INCREF( const_str_plain_pbag );
    PyTuple_SET_ITEM( const_tuple_str_plain_pbag_str_plain_b_str_plain_PBag_tuple, 1, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_str_plain_pbag_str_plain_b_str_plain_PBag_tuple, 2, const_str_plain_PBag ); Py_INCREF( const_str_plain_PBag );
    const_tuple_str_plain_pdeque_str_plain_dq_str_plain_PDeque_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_pdeque_str_plain_dq_str_plain_PDeque_tuple, 0, const_str_plain_pdeque ); Py_INCREF( const_str_plain_pdeque );
    PyTuple_SET_ITEM( const_tuple_str_plain_pdeque_str_plain_dq_str_plain_PDeque_tuple, 1, const_str_plain_dq ); Py_INCREF( const_str_plain_dq );
    PyTuple_SET_ITEM( const_tuple_str_plain_pdeque_str_plain_dq_str_plain_PDeque_tuple, 2, const_str_plain_PDeque ); Py_INCREF( const_str_plain_PDeque );
    const_tuple_str_plain_inc_str_plain_discard_str_plain_rex_str_plain_ny_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_str_plain_inc_str_plain_discard_str_plain_rex_str_plain_ny_tuple, 0, const_str_plain_inc ); Py_INCREF( const_str_plain_inc );
    PyTuple_SET_ITEM( const_tuple_str_plain_inc_str_plain_discard_str_plain_rex_str_plain_ny_tuple, 1, const_str_plain_discard ); Py_INCREF( const_str_plain_discard );
    PyTuple_SET_ITEM( const_tuple_str_plain_inc_str_plain_discard_str_plain_rex_str_plain_ny_tuple, 2, const_str_plain_rex ); Py_INCREF( const_str_plain_rex );
    PyTuple_SET_ITEM( const_tuple_str_plain_inc_str_plain_discard_str_plain_rex_str_plain_ny_tuple, 3, const_str_plain_ny ); Py_INCREF( const_str_plain_ny );
    const_tuple_d50d7f8498d7d71b754176a25f6364d1_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_d50d7f8498d7d71b754176a25f6364d1_tuple, 0, const_str_plain_field ); Py_INCREF( const_str_plain_field );
    PyTuple_SET_ITEM( const_tuple_d50d7f8498d7d71b754176a25f6364d1_tuple, 1, const_str_plain_PTypeError ); Py_INCREF( const_str_plain_PTypeError );
    PyTuple_SET_ITEM( const_tuple_d50d7f8498d7d71b754176a25f6364d1_tuple, 2, const_str_plain_pset_field ); Py_INCREF( const_str_plain_pset_field );
    PyTuple_SET_ITEM( const_tuple_d50d7f8498d7d71b754176a25f6364d1_tuple, 3, const_str_plain_pmap_field ); Py_INCREF( const_str_plain_pmap_field );
    PyTuple_SET_ITEM( const_tuple_d50d7f8498d7d71b754176a25f6364d1_tuple, 4, const_str_plain_pvector_field ); Py_INCREF( const_str_plain_pvector_field );
    const_str_digest_0067e2ce01e22b757be66360e5e0b81e = UNSTREAM_STRING_ASCII( &constant_bin[ 5190302 ], 22, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pyrsistent( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_df2ac0f55d5992077290958d04166ea9;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_0067e2ce01e22b757be66360e5e0b81e );
    codeobj_df2ac0f55d5992077290958d04166ea9 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_a8050813fbf1722354a903bae411c27a, 1, const_tuple_empty, 0, 0, CO_NOFREE );
}

// The module function declarations.


// The module function definitions.



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pyrsistent =
{
    PyModuleDef_HEAD_INIT,
    "pyrsistent",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(pyrsistent)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(pyrsistent)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pyrsistent );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pyrsistent: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pyrsistent: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pyrsistent: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpyrsistent" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pyrsistent = Py_InitModule4(
        "pyrsistent",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pyrsistent = PyModule_Create( &mdef_pyrsistent );
#endif

    moduledict_pyrsistent = MODULE_DICT( module_pyrsistent );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_pyrsistent,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 1
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pyrsistent,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pyrsistent,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pyrsistent,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pyrsistent );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_plain_pyrsistent, module_pyrsistent );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 1
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_10__module = NULL;
    PyObject *tmp_import_from_11__module = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    PyObject *tmp_import_from_4__module = NULL;
    PyObject *tmp_import_from_5__module = NULL;
    PyObject *tmp_import_from_6__module = NULL;
    PyObject *tmp_import_from_7__module = NULL;
    PyObject *tmp_import_from_8__module = NULL;
    PyObject *tmp_import_from_9__module = NULL;
    struct Nuitka_FrameObject *frame_df2ac0f55d5992077290958d04166ea9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_df2ac0f55d5992077290958d04166ea9 = MAKE_MODULE_FRAME( codeobj_df2ac0f55d5992077290958d04166ea9, module_pyrsistent );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_df2ac0f55d5992077290958d04166ea9 );
    assert( Py_REFCNT( frame_df2ac0f55d5992077290958d04166ea9 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_instance_1;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_source_name_1 = PyObject_GetAttr( module, const_str_plain_path );
            }
            else
            {
                tmp_source_name_1 = NULL;
            }
        }

        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_dirname );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = module_filename_obj;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 1;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = PyList_New( 2 );
        PyList_SET_ITEM( tmp_assign_source_3, 0, tmp_list_element_1 );
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_called_instance_1 = PyObject_GetAttr( module, const_str_plain_environ );
            }
            else
            {
                tmp_called_instance_1 = NULL;
            }
        }

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 1;
        tmp_list_element_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_d12bf79c1b789da3cabcb8e253b992d9_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assign_source_3, 1, tmp_list_element_1 );
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___path__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = Py_None;
        UPDATE_STRING_DICT0( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_fe02a1170f09a52095603d103fefd814;
        tmp_globals_name_1 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_pmap_str_plain_m_str_plain_PMap_tuple;
        tmp_level_name_1 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 3;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_5;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_pmap );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_pmap, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_m );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_m, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_PMap );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_PMap, tmp_assign_source_8 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_1acfec9db8a06e568fb00194707d7493;
        tmp_globals_name_2 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_pvector_str_plain_v_str_plain_PVector_tuple;
        tmp_level_name_2 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 5;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_9;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_4 = tmp_import_from_2__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_pvector );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_pvector, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_v );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_v, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_PVector );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_PVector, tmp_assign_source_12 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_2ef06c78dedff1f54ddea6126ab11898;
        tmp_globals_name_3 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_pset_str_plain_s_str_plain_PSet_tuple;
        tmp_level_name_3 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 7;
        tmp_assign_source_13 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_13;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_7 = tmp_import_from_3__module;
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_pset );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_pset, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_8 = tmp_import_from_3__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_s );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_s, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_9;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_9 = tmp_import_from_3__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_PSet );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_PSet, tmp_assign_source_16 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_da2247a8fd410289ecfcc9f4e6c3a584;
        tmp_globals_name_4 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_pbag_str_plain_b_str_plain_PBag_tuple;
        tmp_level_name_4 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 9;
        tmp_assign_source_17 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_4__module == NULL );
        tmp_import_from_4__module = tmp_assign_source_17;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_10;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_10 = tmp_import_from_4__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_pbag );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_pbag, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_11 = tmp_import_from_4__module;
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_b );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_b, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_12 = tmp_import_from_4__module;
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_PBag );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_PBag, tmp_assign_source_20 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_558b02d50f93abc24b9cc91580d4be6e;
        tmp_globals_name_5 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_plist_str_plain_l_str_plain_PList_tuple;
        tmp_level_name_5 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 11;
        tmp_assign_source_21 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_5__module == NULL );
        tmp_import_from_5__module = tmp_assign_source_21;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_5__module );
        tmp_import_name_from_13 = tmp_import_from_5__module;
        tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_plist );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_plist, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_5__module );
        tmp_import_name_from_14 = tmp_import_from_5__module;
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_l );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_l, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_15;
        CHECK_OBJECT( tmp_import_from_5__module );
        tmp_import_name_from_15 = tmp_import_from_5__module;
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_PList );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto try_except_handler_5;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_PList, tmp_assign_source_24 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
    Py_DECREF( tmp_import_from_5__module );
    tmp_import_from_5__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_5__module );
    Py_DECREF( tmp_import_from_5__module );
    tmp_import_from_5__module = NULL;

    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_f15741d45e8011305189f1568b9f9f78;
        tmp_globals_name_6 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_pdeque_str_plain_dq_str_plain_PDeque_tuple;
        tmp_level_name_6 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 13;
        tmp_assign_source_25 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_6__module == NULL );
        tmp_import_from_6__module = tmp_assign_source_25;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_import_name_from_16;
        CHECK_OBJECT( tmp_import_from_6__module );
        tmp_import_name_from_16 = tmp_import_from_6__module;
        tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_pdeque );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_6;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_pdeque, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_import_name_from_17;
        CHECK_OBJECT( tmp_import_from_6__module );
        tmp_import_name_from_17 = tmp_import_from_6__module;
        tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_dq );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_6;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_dq, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_import_name_from_18;
        CHECK_OBJECT( tmp_import_from_6__module );
        tmp_import_name_from_18 = tmp_import_from_6__module;
        tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_PDeque );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto try_except_handler_6;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_PDeque, tmp_assign_source_28 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_6__module );
    Py_DECREF( tmp_import_from_6__module );
    tmp_import_from_6__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_6__module );
    Py_DECREF( tmp_import_from_6__module );
    tmp_import_from_6__module = NULL;

    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_1e3d53cc9681554a4d7ed16674ec1ac9;
        tmp_globals_name_7 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_3ae6c313eb2ef94ed58b36cc72853001_tuple;
        tmp_level_name_7 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 15;
        tmp_assign_source_29 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_7__module == NULL );
        tmp_import_from_7__module = tmp_assign_source_29;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_import_name_from_19;
        CHECK_OBJECT( tmp_import_from_7__module );
        tmp_import_name_from_19 = tmp_import_from_7__module;
        tmp_assign_source_30 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_CheckedPMap );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_7;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_CheckedPMap, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_import_name_from_20;
        CHECK_OBJECT( tmp_import_from_7__module );
        tmp_import_name_from_20 = tmp_import_from_7__module;
        tmp_assign_source_31 = IMPORT_NAME( tmp_import_name_from_20, const_str_plain_CheckedPVector );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_7;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_CheckedPVector, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_import_name_from_21;
        CHECK_OBJECT( tmp_import_from_7__module );
        tmp_import_name_from_21 = tmp_import_from_7__module;
        tmp_assign_source_32 = IMPORT_NAME( tmp_import_name_from_21, const_str_plain_CheckedPSet );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_7;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_CheckedPSet, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_import_name_from_22;
        CHECK_OBJECT( tmp_import_from_7__module );
        tmp_import_name_from_22 = tmp_import_from_7__module;
        tmp_assign_source_33 = IMPORT_NAME( tmp_import_name_from_22, const_str_plain_InvariantException );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_7;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_InvariantException, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_import_name_from_23;
        CHECK_OBJECT( tmp_import_from_7__module );
        tmp_import_name_from_23 = tmp_import_from_7__module;
        tmp_assign_source_34 = IMPORT_NAME( tmp_import_name_from_23, const_str_plain_CheckedKeyTypeError );
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_7;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_CheckedKeyTypeError, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_import_name_from_24;
        CHECK_OBJECT( tmp_import_from_7__module );
        tmp_import_name_from_24 = tmp_import_from_7__module;
        tmp_assign_source_35 = IMPORT_NAME( tmp_import_name_from_24, const_str_plain_CheckedValueTypeError );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_7;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_CheckedValueTypeError, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_import_name_from_25;
        CHECK_OBJECT( tmp_import_from_7__module );
        tmp_import_name_from_25 = tmp_import_from_7__module;
        tmp_assign_source_36 = IMPORT_NAME( tmp_import_name_from_25, const_str_plain_CheckedType );
        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_7;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_CheckedType, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_import_name_from_26;
        CHECK_OBJECT( tmp_import_from_7__module );
        tmp_import_name_from_26 = tmp_import_from_7__module;
        tmp_assign_source_37 = IMPORT_NAME( tmp_import_name_from_26, const_str_plain_optional );
        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto try_except_handler_7;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_optional, tmp_assign_source_37 );
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_7__module );
    Py_DECREF( tmp_import_from_7__module );
    tmp_import_from_7__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_7__module );
    Py_DECREF( tmp_import_from_7__module );
    tmp_import_from_7__module = NULL;

    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_ff0a2eed9efce2dbbaa53ac5b4a3cb56;
        tmp_globals_name_8 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_d50d7f8498d7d71b754176a25f6364d1_tuple;
        tmp_level_name_8 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 19;
        tmp_assign_source_38 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_8__module == NULL );
        tmp_import_from_8__module = tmp_assign_source_38;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_import_name_from_27;
        CHECK_OBJECT( tmp_import_from_8__module );
        tmp_import_name_from_27 = tmp_import_from_8__module;
        tmp_assign_source_39 = IMPORT_NAME( tmp_import_name_from_27, const_str_plain_field );
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_8;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_field, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_import_name_from_28;
        CHECK_OBJECT( tmp_import_from_8__module );
        tmp_import_name_from_28 = tmp_import_from_8__module;
        tmp_assign_source_40 = IMPORT_NAME( tmp_import_name_from_28, const_str_plain_PTypeError );
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_8;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_PTypeError, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_import_name_from_29;
        CHECK_OBJECT( tmp_import_from_8__module );
        tmp_import_name_from_29 = tmp_import_from_8__module;
        tmp_assign_source_41 = IMPORT_NAME( tmp_import_name_from_29, const_str_plain_pset_field );
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_8;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_pset_field, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_import_name_from_30;
        CHECK_OBJECT( tmp_import_from_8__module );
        tmp_import_name_from_30 = tmp_import_from_8__module;
        tmp_assign_source_42 = IMPORT_NAME( tmp_import_name_from_30, const_str_plain_pmap_field );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_8;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_pmap_field, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_import_name_from_31;
        CHECK_OBJECT( tmp_import_from_8__module );
        tmp_import_name_from_31 = tmp_import_from_8__module;
        tmp_assign_source_43 = IMPORT_NAME( tmp_import_name_from_31, const_str_plain_pvector_field );
        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto try_except_handler_8;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_pvector_field, tmp_assign_source_43 );
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_8__module );
    Py_DECREF( tmp_import_from_8__module );
    tmp_import_from_8__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_8__module );
    Py_DECREF( tmp_import_from_8__module );
    tmp_import_from_8__module = NULL;

    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_import_name_from_32;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_4e9ceaddc207dd32785ccdb8162f67c8;
        tmp_globals_name_9 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_PRecord_tuple;
        tmp_level_name_9 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 22;
        tmp_import_name_from_32 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_44 = IMPORT_NAME( tmp_import_name_from_32, const_str_plain_PRecord );
        Py_DECREF( tmp_import_name_from_32 );
        if ( tmp_assign_source_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_PRecord, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_digest_e844d5dee179ba0f412361a73fa49293;
        tmp_globals_name_10 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_PClass_str_plain_PClassMeta_tuple;
        tmp_level_name_10 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 24;
        tmp_assign_source_45 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_9__module == NULL );
        tmp_import_from_9__module = tmp_assign_source_45;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_import_name_from_33;
        CHECK_OBJECT( tmp_import_from_9__module );
        tmp_import_name_from_33 = tmp_import_from_9__module;
        tmp_assign_source_46 = IMPORT_NAME( tmp_import_name_from_33, const_str_plain_PClass );
        if ( tmp_assign_source_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_9;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_PClass, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_import_name_from_34;
        CHECK_OBJECT( tmp_import_from_9__module );
        tmp_import_name_from_34 = tmp_import_from_9__module;
        tmp_assign_source_47 = IMPORT_NAME( tmp_import_name_from_34, const_str_plain_PClassMeta );
        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;

            goto try_except_handler_9;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_PClassMeta, tmp_assign_source_47 );
    }
    goto try_end_9;
    // Exception handler code:
    try_except_handler_9:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_9__module );
    Py_DECREF( tmp_import_from_9__module );
    tmp_import_from_9__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto frame_exception_exit_1;
    // End of try:
    try_end_9:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_9__module );
    Py_DECREF( tmp_import_from_9__module );
    tmp_import_from_9__module = NULL;

    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_import_name_from_35;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_digest_d0293e53a57025d5190dd31692d88ab3;
        tmp_globals_name_11 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = const_tuple_str_plain_immutable_tuple;
        tmp_level_name_11 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 26;
        tmp_import_name_from_35 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_import_name_from_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_48 = IMPORT_NAME( tmp_import_name_from_35, const_str_plain_immutable );
        Py_DECREF( tmp_import_name_from_35 );
        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_immutable, tmp_assign_source_48 );
    }
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_digest_56627b427bf195e543503a696c72fe95;
        tmp_globals_name_12 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = const_tuple_str_plain_freeze_str_plain_thaw_str_plain_mutant_tuple;
        tmp_level_name_12 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 28;
        tmp_assign_source_49 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_10__module == NULL );
        tmp_import_from_10__module = tmp_assign_source_49;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_import_name_from_36;
        CHECK_OBJECT( tmp_import_from_10__module );
        tmp_import_name_from_36 = tmp_import_from_10__module;
        tmp_assign_source_50 = IMPORT_NAME( tmp_import_name_from_36, const_str_plain_freeze );
        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto try_except_handler_10;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_freeze, tmp_assign_source_50 );
    }
    {
        PyObject *tmp_assign_source_51;
        PyObject *tmp_import_name_from_37;
        CHECK_OBJECT( tmp_import_from_10__module );
        tmp_import_name_from_37 = tmp_import_from_10__module;
        tmp_assign_source_51 = IMPORT_NAME( tmp_import_name_from_37, const_str_plain_thaw );
        if ( tmp_assign_source_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto try_except_handler_10;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_thaw, tmp_assign_source_51 );
    }
    {
        PyObject *tmp_assign_source_52;
        PyObject *tmp_import_name_from_38;
        CHECK_OBJECT( tmp_import_from_10__module );
        tmp_import_name_from_38 = tmp_import_from_10__module;
        tmp_assign_source_52 = IMPORT_NAME( tmp_import_name_from_38, const_str_plain_mutant );
        if ( tmp_assign_source_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;

            goto try_except_handler_10;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_mutant, tmp_assign_source_52 );
    }
    goto try_end_10;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_10__module );
    Py_DECREF( tmp_import_from_10__module );
    tmp_import_from_10__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_10:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_10__module );
    Py_DECREF( tmp_import_from_10__module );
    tmp_import_from_10__module = NULL;

    {
        PyObject *tmp_assign_source_53;
        PyObject *tmp_name_name_13;
        PyObject *tmp_globals_name_13;
        PyObject *tmp_locals_name_13;
        PyObject *tmp_fromlist_name_13;
        PyObject *tmp_level_name_13;
        tmp_name_name_13 = const_str_digest_811ebf3d380eed202488e73f18944a13;
        tmp_globals_name_13 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_13 = Py_None;
        tmp_fromlist_name_13 = const_tuple_str_plain_inc_str_plain_discard_str_plain_rex_str_plain_ny_tuple;
        tmp_level_name_13 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 30;
        tmp_assign_source_53 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
        if ( tmp_assign_source_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_11__module == NULL );
        tmp_import_from_11__module = tmp_assign_source_53;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_54;
        PyObject *tmp_import_name_from_39;
        CHECK_OBJECT( tmp_import_from_11__module );
        tmp_import_name_from_39 = tmp_import_from_11__module;
        tmp_assign_source_54 = IMPORT_NAME( tmp_import_name_from_39, const_str_plain_inc );
        if ( tmp_assign_source_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto try_except_handler_11;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_inc, tmp_assign_source_54 );
    }
    {
        PyObject *tmp_assign_source_55;
        PyObject *tmp_import_name_from_40;
        CHECK_OBJECT( tmp_import_from_11__module );
        tmp_import_name_from_40 = tmp_import_from_11__module;
        tmp_assign_source_55 = IMPORT_NAME( tmp_import_name_from_40, const_str_plain_discard );
        if ( tmp_assign_source_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto try_except_handler_11;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_discard, tmp_assign_source_55 );
    }
    {
        PyObject *tmp_assign_source_56;
        PyObject *tmp_import_name_from_41;
        CHECK_OBJECT( tmp_import_from_11__module );
        tmp_import_name_from_41 = tmp_import_from_11__module;
        tmp_assign_source_56 = IMPORT_NAME( tmp_import_name_from_41, const_str_plain_rex );
        if ( tmp_assign_source_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto try_except_handler_11;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_rex, tmp_assign_source_56 );
    }
    {
        PyObject *tmp_assign_source_57;
        PyObject *tmp_import_name_from_42;
        CHECK_OBJECT( tmp_import_from_11__module );
        tmp_import_name_from_42 = tmp_import_from_11__module;
        tmp_assign_source_57 = IMPORT_NAME( tmp_import_name_from_42, const_str_plain_ny );
        if ( tmp_assign_source_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;

            goto try_except_handler_11;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_ny, tmp_assign_source_57 );
    }
    goto try_end_11;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_11__module );
    Py_DECREF( tmp_import_from_11__module );
    tmp_import_from_11__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto frame_exception_exit_1;
    // End of try:
    try_end_11:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_11__module );
    Py_DECREF( tmp_import_from_11__module );
    tmp_import_from_11__module = NULL;

    {
        PyObject *tmp_assign_source_58;
        PyObject *tmp_import_name_from_43;
        PyObject *tmp_name_name_14;
        PyObject *tmp_globals_name_14;
        PyObject *tmp_locals_name_14;
        PyObject *tmp_fromlist_name_14;
        PyObject *tmp_level_name_14;
        tmp_name_name_14 = const_str_digest_bbcd3eedb95ca64812664a529506b818;
        tmp_globals_name_14 = (PyObject *)moduledict_pyrsistent;
        tmp_locals_name_14 = Py_None;
        tmp_fromlist_name_14 = const_tuple_str_plain_get_in_tuple;
        tmp_level_name_14 = const_int_0;
        frame_df2ac0f55d5992077290958d04166ea9->m_frame.f_lineno = 32;
        tmp_import_name_from_43 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
        if ( tmp_import_name_from_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_58 = IMPORT_NAME( tmp_import_name_from_43, const_str_plain_get_in );
        Py_DECREF( tmp_import_name_from_43 );
        if ( tmp_assign_source_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain_get_in, tmp_assign_source_58 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_df2ac0f55d5992077290958d04166ea9 );
#endif
    popFrameStack();

    assertFrameObject( frame_df2ac0f55d5992077290958d04166ea9 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_df2ac0f55d5992077290958d04166ea9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_df2ac0f55d5992077290958d04166ea9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_df2ac0f55d5992077290958d04166ea9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_df2ac0f55d5992077290958d04166ea9, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_59;
        tmp_assign_source_59 = const_tuple_518a93834beb9d231038ff60ca3253d4_tuple;
        UPDATE_STRING_DICT0( moduledict_pyrsistent, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_59 );
    }

    return MOD_RETURN_VALUE( module_pyrsistent );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
