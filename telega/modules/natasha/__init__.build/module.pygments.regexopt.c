/* Generated code for Python module 'pygments.regexopt'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_pygments$regexopt" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_pygments$regexopt;
PyDictObject *moduledict_pygments$regexopt;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_itertools;
static PyObject *const_str_digest_7ddfb0de4e167dc289be4b4442e18e68;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_groupby;
extern PyObject *const_str_plain_sorted;
static PyObject *const_str_plain_CS_ESCAPE;
extern PyObject *const_slice_int_pos_1_none_none;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_letters;
extern PyObject *const_str_plain_group;
extern PyObject *const_str_digest_e399ba4554180f37de594a6743234f17;
extern PyObject *const_str_plain_prefix;
static PyObject *const_str_plain_make_charset;
static PyObject *const_str_digest_61b3fcba8d7123eba0f194d03f1d0f75;
static PyObject *const_str_digest_14311f23e7ae45de94df92b3fd1ae031;
static PyObject *const_str_plain_close_paren;
extern PyObject *const_int_neg_1;
extern PyObject *const_tuple_str_plain_escape_tuple;
extern PyObject *const_tuple_str_plain_s_tuple;
static PyObject *const_tuple_str_plain_letters_tuple;
extern PyObject *const_str_plain_m;
extern PyObject *const_str_plain_sub;
extern PyObject *const_str_plain_None;
static PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_group_tuple;
extern PyObject *const_str_plain_join;
static PyObject *const_tuple_str_plain_commonprefix_tuple;
extern PyObject *const_str_chr_63;
static PyObject *const_str_plain_open_paren;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_re;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_s;
extern PyObject *const_str_chr_41;
extern PyObject *const_str_digest_c64595dbb448219932b659de56da4f30;
extern PyObject *const_str_angle_genexpr;
static PyObject *const_str_digest_8642ce1dc5a05e0c723e523581f32262;
extern PyObject *const_str_plain_suffix;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
static PyObject *const_str_plain_commonprefix;
static PyObject *const_str_plain_strings_rev;
extern PyObject *const_str_chr_91;
static PyObject *const_tuple_str_plain_s_str_plain_plen_tuple;
extern PyObject *const_tuple_empty;
static PyObject *const_tuple_str_digest_c2bbb78996da99693dc4e3fc4fcbf0dd_tuple;
extern PyObject *const_str_plain_append;
static PyObject *const_str_digest_0a1d3ec37c4216862507e32dcc18487f;
extern PyObject *const_str_chr_92;
extern PyObject *const_str_plain_compile;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_rest;
static PyObject *const_str_plain_plen;
extern PyObject *const_str_plain_first;
extern PyObject *const_int_0;
static PyObject *const_str_digest_f0c026cc7b1fabcfd74f86b03f743c77;
static PyObject *const_str_plain_regex_opt_inner;
extern PyObject *const_tuple_str_plain_m_tuple;
extern PyObject *const_str_plain_origin;
static PyObject *const_tuple_str_plain_strings_str_plain_prefix_str_plain_suffix_tuple;
static PyObject *const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_chr_40;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_tuple_str_empty_str_empty_tuple;
static PyObject *const_tuple_b1a66f6ce773e546361918097a3d93ac_tuple;
extern PyObject *const_str_chr_93;
extern PyObject *const_slice_none_none_int_neg_1;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_chr_124;
static PyObject *const_str_plain_FIRST_ELEMENT;
extern PyObject *const_str_plain_strings;
static PyObject *const_str_digest_7a6bdb3f76a16962004591e54418e1a2;
extern PyObject *const_int_pos_1;
extern PyObject *const_tuple_str_plain_itemgetter_tuple;
static PyObject *const_tuple_str_plain_groupby_tuple;
static PyObject *const_str_digest_a591309809460e293fa6e61fa316544f;
static PyObject *const_str_plain_oneletter;
extern PyObject *const_str_plain_itemgetter;
extern PyObject *const_str_digest_ba02bab26133df200d8839dde6036acb;
extern PyObject *const_str_plain_slen;
extern PyObject *const_str_plain_operator;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_digest_c2bbb78996da99693dc4e3fc4fcbf0dd;
extern PyObject *const_str_empty;
extern PyObject *const_str_plain_escape;
extern PyObject *const_str_plain_regex_opt;
static PyObject *const_tuple_str_plain_s_str_plain_first_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_7ddfb0de4e167dc289be4b4442e18e68 = UNSTREAM_STRING_ASCII( &constant_bin[ 5036069 ], 30, 0 );
    const_str_plain_CS_ESCAPE = UNSTREAM_STRING_ASCII( &constant_bin[ 5036099 ], 9, 1 );
    const_str_plain_letters = UNSTREAM_STRING_ASCII( &constant_bin[ 263879 ], 7, 1 );
    const_str_plain_make_charset = UNSTREAM_STRING_ASCII( &constant_bin[ 5036069 ], 12, 1 );
    const_str_digest_61b3fcba8d7123eba0f194d03f1d0f75 = UNSTREAM_STRING_ASCII( &constant_bin[ 5036108 ], 260, 0 );
    const_str_digest_14311f23e7ae45de94df92b3fd1ae031 = UNSTREAM_STRING_ASCII( &constant_bin[ 5036368 ], 26, 0 );
    const_str_plain_close_paren = UNSTREAM_STRING_ASCII( &constant_bin[ 4549309 ], 11, 1 );
    const_tuple_str_plain_letters_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_letters_tuple, 0, const_str_plain_letters ); Py_INCREF( const_str_plain_letters );
    const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_group_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_group_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_group_tuple, 1, const_str_plain_group ); Py_INCREF( const_str_plain_group );
    const_tuple_str_plain_commonprefix_tuple = PyTuple_New( 1 );
    const_str_plain_commonprefix = UNSTREAM_STRING_ASCII( &constant_bin[ 4611110 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_commonprefix_tuple, 0, const_str_plain_commonprefix ); Py_INCREF( const_str_plain_commonprefix );
    const_str_plain_open_paren = UNSTREAM_STRING_ASCII( &constant_bin[ 4549416 ], 10, 1 );
    const_str_digest_8642ce1dc5a05e0c723e523581f32262 = UNSTREAM_STRING_ASCII( &constant_bin[ 5036394 ], 237, 0 );
    const_str_plain_strings_rev = UNSTREAM_STRING_ASCII( &constant_bin[ 5036631 ], 11, 1 );
    const_tuple_str_plain_s_str_plain_plen_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_s_str_plain_plen_tuple, 0, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    const_str_plain_plen = UNSTREAM_STRING_ASCII( &constant_bin[ 882649 ], 4, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_s_str_plain_plen_tuple, 1, const_str_plain_plen ); Py_INCREF( const_str_plain_plen );
    const_tuple_str_digest_c2bbb78996da99693dc4e3fc4fcbf0dd_tuple = PyTuple_New( 1 );
    const_str_digest_c2bbb78996da99693dc4e3fc4fcbf0dd = UNSTREAM_STRING_ASCII( &constant_bin[ 5036642 ], 10, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_c2bbb78996da99693dc4e3fc4fcbf0dd_tuple, 0, const_str_digest_c2bbb78996da99693dc4e3fc4fcbf0dd ); Py_INCREF( const_str_digest_c2bbb78996da99693dc4e3fc4fcbf0dd );
    const_str_digest_0a1d3ec37c4216862507e32dcc18487f = UNSTREAM_STRING_ASCII( &constant_bin[ 5036652 ], 34, 0 );
    const_str_digest_f0c026cc7b1fabcfd74f86b03f743c77 = UNSTREAM_STRING_ASCII( &constant_bin[ 5036686 ], 20, 0 );
    const_str_plain_regex_opt_inner = UNSTREAM_STRING_ASCII( &constant_bin[ 5036652 ], 15, 1 );
    const_tuple_str_plain_strings_str_plain_prefix_str_plain_suffix_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_strings_str_plain_prefix_str_plain_suffix_tuple, 0, const_str_plain_strings ); Py_INCREF( const_str_plain_strings );
    PyTuple_SET_ITEM( const_tuple_str_plain_strings_str_plain_prefix_str_plain_suffix_tuple, 1, const_str_plain_prefix ); Py_INCREF( const_str_plain_prefix );
    PyTuple_SET_ITEM( const_tuple_str_plain_strings_str_plain_prefix_str_plain_suffix_tuple, 2, const_str_plain_suffix ); Py_INCREF( const_str_plain_suffix );
    const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple = PyTuple_New( 12 );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 0, const_str_plain_strings ); Py_INCREF( const_str_plain_strings );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 1, const_str_plain_open_paren ); Py_INCREF( const_str_plain_open_paren );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 2, const_str_plain_close_paren ); Py_INCREF( const_str_plain_close_paren );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 3, const_str_plain_first ); Py_INCREF( const_str_plain_first );
    const_str_plain_oneletter = UNSTREAM_STRING_ASCII( &constant_bin[ 5036706 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 4, const_str_plain_oneletter ); Py_INCREF( const_str_plain_oneletter );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 5, const_str_plain_rest ); Py_INCREF( const_str_plain_rest );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 6, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 7, const_str_plain_prefix ); Py_INCREF( const_str_plain_prefix );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 8, const_str_plain_plen ); Py_INCREF( const_str_plain_plen );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 9, const_str_plain_strings_rev ); Py_INCREF( const_str_plain_strings_rev );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 10, const_str_plain_suffix ); Py_INCREF( const_str_plain_suffix );
    PyTuple_SET_ITEM( const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 11, const_str_plain_slen ); Py_INCREF( const_str_plain_slen );
    const_tuple_b1a66f6ce773e546361918097a3d93ac_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_b1a66f6ce773e546361918097a3d93ac_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_b1a66f6ce773e546361918097a3d93ac_tuple, 1, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_b1a66f6ce773e546361918097a3d93ac_tuple, 2, const_str_plain_slen ); Py_INCREF( const_str_plain_slen );
    const_str_plain_FIRST_ELEMENT = UNSTREAM_STRING_ASCII( &constant_bin[ 5036715 ], 13, 1 );
    const_str_digest_7a6bdb3f76a16962004591e54418e1a2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5036728 ], 33, 0 );
    const_tuple_str_plain_groupby_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_groupby_tuple, 0, const_str_plain_groupby ); Py_INCREF( const_str_plain_groupby );
    const_str_digest_a591309809460e293fa6e61fa316544f = UNSTREAM_STRING_ASCII( &constant_bin[ 5036761 ], 69, 0 );
    const_tuple_str_plain_s_str_plain_first_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_s_str_plain_first_tuple, 0, const_str_plain_s ); Py_INCREF( const_str_plain_s );
    PyTuple_SET_ITEM( const_tuple_str_plain_s_str_plain_first_tuple, 1, const_str_plain_first ); Py_INCREF( const_str_plain_first );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_pygments$regexopt( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_2612fdefdb533b33ab494dd312882cd1;
static PyCodeObject *codeobj_a5c233acb2de4313fccd2cffd6ace119;
static PyCodeObject *codeobj_4ae53562437f62ba2121875b06f4be02;
static PyCodeObject *codeobj_aa56ee63a94d87385eded3153043b17e;
static PyCodeObject *codeobj_68a96a32eae4e3b939f6b91c88796c45;
static PyCodeObject *codeobj_838a6180808ceb82d18170a638a9f095;
static PyCodeObject *codeobj_7b214c6996713caf50702de8719b6701;
static PyCodeObject *codeobj_458a46c6911aeeda0098d40d0444831d;
static PyCodeObject *codeobj_6ccda6d91de4f7cceac77b4e07a3d00b;
static PyCodeObject *codeobj_9b3f37882c907843562bd5760b0b39ed;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_f0c026cc7b1fabcfd74f86b03f743c77 );
    codeobj_2612fdefdb533b33ab494dd312882cd1 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 78, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_group_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a5c233acb2de4313fccd2cffd6ace119 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 73, const_tuple_b1a66f6ce773e546361918097a3d93ac_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4ae53562437f62ba2121875b06f4be02 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 24, const_tuple_str_plain_m_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_aa56ee63a94d87385eded3153043b17e = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 79, const_tuple_str_plain_s_str_plain_first_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_68a96a32eae4e3b939f6b91c88796c45 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 67, const_tuple_str_plain_s_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_838a6180808ceb82d18170a638a9f095 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 64, const_tuple_str_plain_s_str_plain_plen_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7b214c6996713caf50702de8719b6701 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_14311f23e7ae45de94df92b3fd1ae031, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_458a46c6911aeeda0098d40d0444831d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_make_charset, 23, const_tuple_str_plain_letters_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6ccda6d91de4f7cceac77b4e07a3d00b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_regex_opt, 83, const_tuple_str_plain_strings_str_plain_prefix_str_plain_suffix_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9b3f37882c907843562bd5760b0b39ed = MAKE_CODEOBJ( module_filename_obj, const_str_plain_regex_opt_inner, 27, const_tuple_4ad628efddf97583839b5d6d81b50a17_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_1_genexpr_maker( void );


static PyObject *pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_2_genexpr_maker( void );


static PyObject *MAKE_FUNCTION_pygments$regexopt$$$function_1_make_charset(  );


static PyObject *MAKE_FUNCTION_pygments$regexopt$$$function_1_make_charset$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_pygments$regexopt$$$function_2_regex_opt_inner(  );


static PyObject *MAKE_FUNCTION_pygments$regexopt$$$function_2_regex_opt_inner$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_pygments$regexopt$$$function_3_regex_opt( PyObject *defaults );


// The module function definitions.
static PyObject *impl_pygments$regexopt$$$function_1_make_charset( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_letters = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_458a46c6911aeeda0098d40d0444831d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_458a46c6911aeeda0098d40d0444831d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_458a46c6911aeeda0098d40d0444831d, codeobj_458a46c6911aeeda0098d40d0444831d, module_pygments$regexopt, sizeof(void *) );
    frame_458a46c6911aeeda0098d40d0444831d = cache_frame_458a46c6911aeeda0098d40d0444831d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_458a46c6911aeeda0098d40d0444831d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_458a46c6911aeeda0098d40d0444831d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_right_name_2;
        tmp_left_name_2 = const_str_chr_91;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_CS_ESCAPE );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CS_ESCAPE );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CS_ESCAPE" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 24;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_sub );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = MAKE_FUNCTION_pygments$regexopt$$$function_1_make_charset$$$function_1_lambda(  );



        tmp_called_instance_1 = const_str_empty;
        CHECK_OBJECT( par_letters );
        tmp_args_element_name_3 = par_letters;
        frame_458a46c6911aeeda0098d40d0444831d->m_frame.f_lineno = 24;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_join, call_args );
        }

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 24;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_458a46c6911aeeda0098d40d0444831d->m_frame.f_lineno = 24;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = const_str_chr_93;
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_458a46c6911aeeda0098d40d0444831d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_458a46c6911aeeda0098d40d0444831d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_458a46c6911aeeda0098d40d0444831d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_458a46c6911aeeda0098d40d0444831d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_458a46c6911aeeda0098d40d0444831d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_458a46c6911aeeda0098d40d0444831d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_458a46c6911aeeda0098d40d0444831d,
        type_description_1,
        par_letters
    );


    // Release cached frame.
    if ( frame_458a46c6911aeeda0098d40d0444831d == cache_frame_458a46c6911aeeda0098d40d0444831d )
    {
        Py_DECREF( frame_458a46c6911aeeda0098d40d0444831d );
    }
    cache_frame_458a46c6911aeeda0098d40d0444831d = NULL;

    assertFrameObject( frame_458a46c6911aeeda0098d40d0444831d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_1_make_charset );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_letters );
    Py_DECREF( par_letters );
    par_letters = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_letters );
    Py_DECREF( par_letters );
    par_letters = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_1_make_charset );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pygments$regexopt$$$function_1_make_charset$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_m = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_4ae53562437f62ba2121875b06f4be02;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_4ae53562437f62ba2121875b06f4be02 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4ae53562437f62ba2121875b06f4be02, codeobj_4ae53562437f62ba2121875b06f4be02, module_pygments$regexopt, sizeof(void *) );
    frame_4ae53562437f62ba2121875b06f4be02 = cache_frame_4ae53562437f62ba2121875b06f4be02;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4ae53562437f62ba2121875b06f4be02 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4ae53562437f62ba2121875b06f4be02 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_instance_1;
        tmp_left_name_1 = const_str_chr_92;
        CHECK_OBJECT( par_m );
        tmp_called_instance_1 = par_m;
        frame_4ae53562437f62ba2121875b06f4be02->m_frame.f_lineno = 24;
        tmp_right_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_group );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4ae53562437f62ba2121875b06f4be02 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4ae53562437f62ba2121875b06f4be02 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4ae53562437f62ba2121875b06f4be02 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4ae53562437f62ba2121875b06f4be02, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4ae53562437f62ba2121875b06f4be02->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4ae53562437f62ba2121875b06f4be02, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4ae53562437f62ba2121875b06f4be02,
        type_description_1,
        par_m
    );


    // Release cached frame.
    if ( frame_4ae53562437f62ba2121875b06f4be02 == cache_frame_4ae53562437f62ba2121875b06f4be02 )
    {
        Py_DECREF( frame_4ae53562437f62ba2121875b06f4be02 );
    }
    cache_frame_4ae53562437f62ba2121875b06f4be02 = NULL;

    assertFrameObject( frame_4ae53562437f62ba2121875b06f4be02 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_1_make_charset$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_m );
    Py_DECREF( par_m );
    par_m = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_1_make_charset$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_pygments$regexopt$$$function_2_regex_opt_inner( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_strings = python_pars[ 0 ];
    PyObject *par_open_paren = python_pars[ 1 ];
    PyObject *var_close_paren = NULL;
    struct Nuitka_CellObject *var_first = PyCell_EMPTY();
    PyObject *var_oneletter = NULL;
    PyObject *var_rest = NULL;
    PyObject *var_s = NULL;
    PyObject *var_prefix = NULL;
    PyObject *var_plen = NULL;
    PyObject *var_strings_rev = NULL;
    PyObject *var_suffix = NULL;
    struct Nuitka_CellObject *var_slen = PyCell_EMPTY();
    PyObject *outline_0_var_s = NULL;
    PyObject *outline_1_var_s = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_genexpr_2__$0 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_listcomp_2__$0 = NULL;
    PyObject *tmp_listcomp_2__contraction = NULL;
    PyObject *tmp_listcomp_2__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_9b3f37882c907843562bd5760b0b39ed;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    struct Nuitka_FrameObject *frame_838a6180808ceb82d18170a638a9f095_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_838a6180808ceb82d18170a638a9f095_2 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    struct Nuitka_FrameObject *frame_68a96a32eae4e3b939f6b91c88796c45_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_68a96a32eae4e3b939f6b91c88796c45_3 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_9b3f37882c907843562bd5760b0b39ed = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9b3f37882c907843562bd5760b0b39ed, codeobj_9b3f37882c907843562bd5760b0b39ed, module_pygments$regexopt, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9b3f37882c907843562bd5760b0b39ed = cache_frame_9b3f37882c907843562bd5760b0b39ed;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9b3f37882c907843562bd5760b0b39ed );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9b3f37882c907843562bd5760b0b39ed ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        int tmp_and_left_truth_1;
        PyObject *tmp_and_left_value_1;
        PyObject *tmp_and_right_value_1;
        CHECK_OBJECT( par_open_paren );
        tmp_and_left_value_1 = par_open_paren;
        tmp_and_left_truth_1 = CHECK_IF_TRUE( tmp_and_left_value_1 );
        if ( tmp_and_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_and_right_value_1 = const_str_chr_41;
        tmp_or_left_value_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_or_left_value_1 = tmp_and_left_value_1;
        and_end_1:;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_or_right_value_1 = const_str_empty;
        tmp_assign_source_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_assign_source_1 = tmp_or_left_value_1;
        or_end_1:;
        assert( var_close_paren == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var_close_paren = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( par_strings );
        tmp_operand_name_1 = par_strings;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = const_str_empty;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_strings );
        tmp_subscribed_name_1 = par_strings;
        tmp_subscript_name_1 = const_int_0;
        tmp_assign_source_2 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_first ) == NULL );
        PyCell_SET( var_first, tmp_assign_source_2 );

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        CHECK_OBJECT( par_strings );
        tmp_len_arg_1 = par_strings;
        tmp_compexpr_left_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 35;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_left_name_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_right_name_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_right_name_2;
            CHECK_OBJECT( par_open_paren );
            tmp_left_name_2 = par_open_paren;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_escape );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_escape );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "escape" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 37;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( PyCell_GET( var_first ) );
            tmp_args_element_name_1 = PyCell_GET( var_first );
            frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 37;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_left_name_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_left_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_close_paren );
            tmp_right_name_2 = var_close_paren;
            tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 37;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_2:;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_operand_name_2;
        CHECK_OBJECT( PyCell_GET( var_first ) );
        tmp_operand_name_2 = PyCell_GET( var_first );
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_left_name_3;
            PyObject *tmp_left_name_4;
            PyObject *tmp_left_name_5;
            PyObject *tmp_right_name_3;
            PyObject *tmp_called_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_right_name_4;
            PyObject *tmp_right_name_5;
            CHECK_OBJECT( par_open_paren );
            tmp_left_name_5 = par_open_paren;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "regex_opt_inner" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 40;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_2 = tmp_mvar_value_2;
            CHECK_OBJECT( par_strings );
            tmp_subscribed_name_2 = par_strings;
            tmp_subscript_name_2 = const_slice_int_pos_1_none_none;
            tmp_args_element_name_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_3 = const_str_digest_c64595dbb448219932b659de56da4f30;
            frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 40;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_right_name_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_right_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_left_name_4 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_5, tmp_right_name_3 );
            Py_DECREF( tmp_right_name_3 );
            if ( tmp_left_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 40;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_right_name_4 = const_str_chr_63;
            tmp_left_name_3 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_4, tmp_right_name_4 );
            Py_DECREF( tmp_left_name_4 );
            if ( tmp_left_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_close_paren );
            tmp_right_name_5 = var_close_paren;
            tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_5 );
            Py_DECREF( tmp_left_name_3 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 41;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_len_arg_2;
        CHECK_OBJECT( PyCell_GET( var_first ) );
        tmp_len_arg_2 = PyCell_GET( var_first );
        tmp_compexpr_left_2 = BUILTIN_LEN( tmp_len_arg_2 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        assert( !(tmp_res == -1) );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = PyList_New( 0 );
            assert( var_oneletter == NULL );
            var_oneletter = tmp_assign_source_3;
        }
        {
            PyObject *tmp_assign_source_4;
            tmp_assign_source_4 = PyList_New( 0 );
            assert( var_rest == NULL );
            var_rest = tmp_assign_source_4;
        }
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( par_strings );
            tmp_iter_arg_1 = par_strings;
            tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 46;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_1__for_iterator == NULL );
            tmp_for_loop_1__for_iterator = tmp_assign_source_5;
        }
        // Tried code:
        loop_start_1:;
        {
            PyObject *tmp_next_source_1;
            PyObject *tmp_assign_source_6;
            CHECK_OBJECT( tmp_for_loop_1__for_iterator );
            tmp_next_source_1 = tmp_for_loop_1__for_iterator;
            tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
            if ( tmp_assign_source_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_1;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "ooocoooooooc";
                    exception_lineno = 46;
                    goto try_except_handler_2;
                }
            }

            {
                PyObject *old = tmp_for_loop_1__iter_value;
                tmp_for_loop_1__iter_value = tmp_assign_source_6;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_7;
            CHECK_OBJECT( tmp_for_loop_1__iter_value );
            tmp_assign_source_7 = tmp_for_loop_1__iter_value;
            {
                PyObject *old = var_s;
                var_s = tmp_assign_source_7;
                Py_INCREF( var_s );
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_len_arg_3;
            CHECK_OBJECT( var_s );
            tmp_len_arg_3 = var_s;
            tmp_compexpr_left_3 = BUILTIN_LEN( tmp_len_arg_3 );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 47;
                type_description_1 = "ooocoooooooc";
                goto try_except_handler_2;
            }
            tmp_compexpr_right_3 = const_int_pos_1;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_called_instance_1;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_element_name_4;
                CHECK_OBJECT( var_oneletter );
                tmp_called_instance_1 = var_oneletter;
                CHECK_OBJECT( var_s );
                tmp_args_element_name_4 = var_s;
                frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 48;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4 };
                    tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_append, call_args );
                }

                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 48;
                    type_description_1 = "ooocoooooooc";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            goto branch_end_5;
            branch_no_5:;
            {
                PyObject *tmp_called_instance_2;
                PyObject *tmp_call_result_2;
                PyObject *tmp_args_element_name_5;
                CHECK_OBJECT( var_rest );
                tmp_called_instance_2 = var_rest;
                CHECK_OBJECT( var_s );
                tmp_args_element_name_5 = var_s;
                frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 50;
                {
                    PyObject *call_args[] = { tmp_args_element_name_5 };
                    tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, call_args );
                }

                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 50;
                    type_description_1 = "ooocoooooooc";
                    goto try_except_handler_2;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_end_5:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "ooocoooooooc";
            goto try_except_handler_2;
        }
        goto loop_start_1;
        loop_end_1:;
        goto try_end_1;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto frame_exception_exit_1;
        // End of try:
        try_end_1:;
        Py_XDECREF( tmp_for_loop_1__iter_value );
        tmp_for_loop_1__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
        Py_DECREF( tmp_for_loop_1__for_iterator );
        tmp_for_loop_1__for_iterator = NULL;

        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            PyObject *tmp_len_arg_4;
            CHECK_OBJECT( var_oneletter );
            tmp_len_arg_4 = var_oneletter;
            tmp_compexpr_left_4 = BUILTIN_LEN( tmp_len_arg_4 );
            assert( !(tmp_compexpr_left_4 == NULL) );
            tmp_compexpr_right_4 = const_int_pos_1;
            tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            Py_DECREF( tmp_compexpr_left_4 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                nuitka_bool tmp_condition_result_7;
                int tmp_truth_name_1;
                CHECK_OBJECT( var_rest );
                tmp_truth_name_1 = CHECK_IF_TRUE( var_rest );
                assert( !(tmp_truth_name_1 == -1) );
                tmp_condition_result_7 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                {
                    PyObject *tmp_left_name_6;
                    PyObject *tmp_left_name_7;
                    PyObject *tmp_left_name_8;
                    PyObject *tmp_left_name_9;
                    PyObject *tmp_right_name_6;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_mvar_value_3;
                    PyObject *tmp_args_element_name_6;
                    PyObject *tmp_args_element_name_7;
                    PyObject *tmp_right_name_7;
                    PyObject *tmp_right_name_8;
                    PyObject *tmp_called_name_4;
                    PyObject *tmp_mvar_value_4;
                    PyObject *tmp_args_element_name_8;
                    PyObject *tmp_right_name_9;
                    CHECK_OBJECT( par_open_paren );
                    tmp_left_name_9 = par_open_paren;
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );

                    if (unlikely( tmp_mvar_value_3 == NULL ))
                    {
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );
                    }

                    if ( tmp_mvar_value_3 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "regex_opt_inner" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 54;
                        type_description_1 = "ooocoooooooc";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_3 = tmp_mvar_value_3;
                    CHECK_OBJECT( var_rest );
                    tmp_args_element_name_6 = var_rest;
                    tmp_args_element_name_7 = const_str_empty;
                    frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 54;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
                        tmp_right_name_6 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
                    }

                    if ( tmp_right_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 54;
                        type_description_1 = "ooocoooooooc";
                        goto frame_exception_exit_1;
                    }
                    tmp_left_name_8 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_9, tmp_right_name_6 );
                    Py_DECREF( tmp_right_name_6 );
                    if ( tmp_left_name_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 54;
                        type_description_1 = "ooocoooooooc";
                        goto frame_exception_exit_1;
                    }
                    tmp_right_name_7 = const_str_chr_124;
                    tmp_left_name_7 = BINARY_OPERATION_ADD_OBJECT_UNICODE( tmp_left_name_8, tmp_right_name_7 );
                    Py_DECREF( tmp_left_name_8 );
                    if ( tmp_left_name_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 54;
                        type_description_1 = "ooocoooooooc";
                        goto frame_exception_exit_1;
                    }
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_make_charset );

                    if (unlikely( tmp_mvar_value_4 == NULL ))
                    {
                        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_make_charset );
                    }

                    if ( tmp_mvar_value_4 == NULL )
                    {
                        Py_DECREF( tmp_left_name_7 );
                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "make_charset" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 55;
                        type_description_1 = "ooocoooooooc";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_4 = tmp_mvar_value_4;
                    CHECK_OBJECT( var_oneletter );
                    tmp_args_element_name_8 = var_oneletter;
                    frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 55;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_8 };
                        tmp_right_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                    }

                    if ( tmp_right_name_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_left_name_7 );

                        exception_lineno = 55;
                        type_description_1 = "ooocoooooooc";
                        goto frame_exception_exit_1;
                    }
                    tmp_left_name_6 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_8 );
                    Py_DECREF( tmp_left_name_7 );
                    Py_DECREF( tmp_right_name_8 );
                    if ( tmp_left_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 55;
                        type_description_1 = "ooocoooooooc";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_close_paren );
                    tmp_right_name_9 = var_close_paren;
                    tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_9 );
                    Py_DECREF( tmp_left_name_6 );
                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 55;
                        type_description_1 = "ooocoooooooc";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                branch_no_7:;
            }
            {
                PyObject *tmp_left_name_10;
                PyObject *tmp_left_name_11;
                PyObject *tmp_right_name_10;
                PyObject *tmp_called_name_5;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_args_element_name_9;
                PyObject *tmp_right_name_11;
                CHECK_OBJECT( par_open_paren );
                tmp_left_name_11 = par_open_paren;
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_make_charset );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_make_charset );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "make_charset" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 57;
                    type_description_1 = "ooocoooooooc";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_5 = tmp_mvar_value_5;
                CHECK_OBJECT( var_oneletter );
                tmp_args_element_name_9 = var_oneletter;
                frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 57;
                {
                    PyObject *call_args[] = { tmp_args_element_name_9 };
                    tmp_right_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
                }

                if ( tmp_right_name_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 57;
                    type_description_1 = "ooocoooooooc";
                    goto frame_exception_exit_1;
                }
                tmp_left_name_10 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_11, tmp_right_name_10 );
                Py_DECREF( tmp_right_name_10 );
                if ( tmp_left_name_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 57;
                    type_description_1 = "ooocoooooooc";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_close_paren );
                tmp_right_name_11 = var_close_paren;
                tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_10, tmp_right_name_11 );
                Py_DECREF( tmp_left_name_10 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 57;
                    type_description_1 = "ooocoooooooc";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            branch_no_6:;
        }
        branch_no_4:;
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_commonprefix );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_commonprefix );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "commonprefix" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 58;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_6;
        CHECK_OBJECT( par_strings );
        tmp_args_element_name_10 = par_strings;
        frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 58;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_assign_source_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 58;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        assert( var_prefix == NULL );
        var_prefix = tmp_assign_source_8;
    }
    {
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        CHECK_OBJECT( var_prefix );
        tmp_truth_name_2 = CHECK_IF_TRUE( var_prefix );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_8;
        }
        else
        {
            goto branch_no_8;
        }
        branch_yes_8:;
        {
            PyObject *tmp_assign_source_9;
            PyObject *tmp_len_arg_5;
            CHECK_OBJECT( var_prefix );
            tmp_len_arg_5 = var_prefix;
            tmp_assign_source_9 = BUILTIN_LEN( tmp_len_arg_5 );
            if ( tmp_assign_source_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 60;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            assert( var_plen == NULL );
            var_plen = tmp_assign_source_9;
        }
        {
            PyObject *tmp_left_name_12;
            PyObject *tmp_left_name_13;
            PyObject *tmp_left_name_14;
            PyObject *tmp_right_name_12;
            PyObject *tmp_called_name_7;
            PyObject *tmp_mvar_value_7;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_right_name_13;
            PyObject *tmp_called_name_8;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_args_element_name_12;
            PyObject *tmp_args_element_name_13;
            PyObject *tmp_right_name_14;
            CHECK_OBJECT( par_open_paren );
            tmp_left_name_14 = par_open_paren;
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_escape );

            if (unlikely( tmp_mvar_value_7 == NULL ))
            {
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_escape );
            }

            if ( tmp_mvar_value_7 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "escape" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 63;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_7 = tmp_mvar_value_7;
            CHECK_OBJECT( var_prefix );
            tmp_args_element_name_11 = var_prefix;
            frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 63;
            {
                PyObject *call_args[] = { tmp_args_element_name_11 };
                tmp_right_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            if ( tmp_right_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_left_name_13 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_14, tmp_right_name_12 );
            Py_DECREF( tmp_right_name_12 );
            if ( tmp_left_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 63;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );
            }

            if ( tmp_mvar_value_8 == NULL )
            {
                Py_DECREF( tmp_left_name_13 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "regex_opt_inner" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 64;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_8 = tmp_mvar_value_8;
            // Tried code:
            {
                PyObject *tmp_assign_source_10;
                PyObject *tmp_iter_arg_2;
                CHECK_OBJECT( par_strings );
                tmp_iter_arg_2 = par_strings;
                tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_2 );
                if ( tmp_assign_source_10 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 64;
                    type_description_1 = "ooocoooooooc";
                    goto try_except_handler_3;
                }
                assert( tmp_listcomp_1__$0 == NULL );
                tmp_listcomp_1__$0 = tmp_assign_source_10;
            }
            {
                PyObject *tmp_assign_source_11;
                tmp_assign_source_11 = PyList_New( 0 );
                assert( tmp_listcomp_1__contraction == NULL );
                tmp_listcomp_1__contraction = tmp_assign_source_11;
            }
            MAKE_OR_REUSE_FRAME( cache_frame_838a6180808ceb82d18170a638a9f095_2, codeobj_838a6180808ceb82d18170a638a9f095, module_pygments$regexopt, sizeof(void *)+sizeof(void *) );
            frame_838a6180808ceb82d18170a638a9f095_2 = cache_frame_838a6180808ceb82d18170a638a9f095_2;

            // Push the new frame as the currently active one.
            pushFrameStack( frame_838a6180808ceb82d18170a638a9f095_2 );

            // Mark the frame object as in use, ref count 1 will be up for reuse.
            assert( Py_REFCNT( frame_838a6180808ceb82d18170a638a9f095_2 ) == 2 ); // Frame stack

            // Framed code:
            // Tried code:
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_12;
                CHECK_OBJECT( tmp_listcomp_1__$0 );
                tmp_next_source_2 = tmp_listcomp_1__$0;
                tmp_assign_source_12 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_12 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_2 = "oo";
                        exception_lineno = 64;
                        goto try_except_handler_4;
                    }
                }

                {
                    PyObject *old = tmp_listcomp_1__iter_value_0;
                    tmp_listcomp_1__iter_value_0 = tmp_assign_source_12;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_13;
                CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                tmp_assign_source_13 = tmp_listcomp_1__iter_value_0;
                {
                    PyObject *old = outline_0_var_s;
                    outline_0_var_s = tmp_assign_source_13;
                    Py_INCREF( outline_0_var_s );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_append_list_1;
                PyObject *tmp_append_value_1;
                PyObject *tmp_subscribed_name_3;
                PyObject *tmp_subscript_name_3;
                PyObject *tmp_start_name_1;
                PyObject *tmp_stop_name_1;
                PyObject *tmp_step_name_1;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_append_list_1 = tmp_listcomp_1__contraction;
                CHECK_OBJECT( outline_0_var_s );
                tmp_subscribed_name_3 = outline_0_var_s;
                CHECK_OBJECT( var_plen );
                tmp_start_name_1 = var_plen;
                tmp_stop_name_1 = Py_None;
                tmp_step_name_1 = Py_None;
                tmp_subscript_name_3 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
                assert( !(tmp_subscript_name_3 == NULL) );
                tmp_append_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
                Py_DECREF( tmp_subscript_name_3 );
                if ( tmp_append_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 64;
                    type_description_2 = "oo";
                    goto try_except_handler_4;
                }
                assert( PyList_Check( tmp_append_list_1 ) );
                tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                Py_DECREF( tmp_append_value_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 64;
                    type_description_2 = "oo";
                    goto try_except_handler_4;
                }
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_2 = "oo";
                goto try_except_handler_4;
            }
            goto loop_start_2;
            loop_end_2:;
            CHECK_OBJECT( tmp_listcomp_1__contraction );
            tmp_args_element_name_12 = tmp_listcomp_1__contraction;
            Py_INCREF( tmp_args_element_name_12 );
            goto try_return_handler_4;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
            return NULL;
            // Return handler code:
            try_return_handler_4:;
            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
            Py_DECREF( tmp_listcomp_1__$0 );
            tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
            Py_DECREF( tmp_listcomp_1__contraction );
            tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_0 );
            tmp_listcomp_1__iter_value_0 = NULL;

            goto frame_return_exit_2;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
            Py_DECREF( tmp_listcomp_1__$0 );
            tmp_listcomp_1__$0 = NULL;

            CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
            Py_DECREF( tmp_listcomp_1__contraction );
            tmp_listcomp_1__contraction = NULL;

            Py_XDECREF( tmp_listcomp_1__iter_value_0 );
            tmp_listcomp_1__iter_value_0 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto frame_exception_exit_2;
            // End of try:

#if 0
            RESTORE_FRAME_EXCEPTION( frame_838a6180808ceb82d18170a638a9f095_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto frame_no_exception_1;

            frame_return_exit_2:;
#if 0
            RESTORE_FRAME_EXCEPTION( frame_838a6180808ceb82d18170a638a9f095_2 );
#endif

            // Put the previous frame back on top.
            popFrameStack();

            goto try_return_handler_3;

            frame_exception_exit_2:;

#if 0
            RESTORE_FRAME_EXCEPTION( frame_838a6180808ceb82d18170a638a9f095_2 );
#endif

            if ( exception_tb == NULL )
            {
                exception_tb = MAKE_TRACEBACK( frame_838a6180808ceb82d18170a638a9f095_2, exception_lineno );
            }
            else if ( exception_tb->tb_frame != &frame_838a6180808ceb82d18170a638a9f095_2->m_frame )
            {
                exception_tb = ADD_TRACEBACK( exception_tb, frame_838a6180808ceb82d18170a638a9f095_2, exception_lineno );
            }

            // Attachs locals to frame if any.
            Nuitka_Frame_AttachLocals(
                (struct Nuitka_FrameObject *)frame_838a6180808ceb82d18170a638a9f095_2,
                type_description_2,
                outline_0_var_s,
                var_plen
            );


            // Release cached frame.
            if ( frame_838a6180808ceb82d18170a638a9f095_2 == cache_frame_838a6180808ceb82d18170a638a9f095_2 )
            {
                Py_DECREF( frame_838a6180808ceb82d18170a638a9f095_2 );
            }
            cache_frame_838a6180808ceb82d18170a638a9f095_2 = NULL;

            assertFrameObject( frame_838a6180808ceb82d18170a638a9f095_2 );

            // Put the previous frame back on top.
            popFrameStack();

            // Return the error.
            goto nested_frame_exit_1;

            frame_no_exception_1:;
            goto skip_nested_handling_1;
            nested_frame_exit_1:;
            type_description_1 = "ooocoooooooc";
            goto try_except_handler_3;
            skip_nested_handling_1:;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
            return NULL;
            // Return handler code:
            try_return_handler_3:;
            Py_XDECREF( outline_0_var_s );
            outline_0_var_s = NULL;

            goto outline_result_1;
            // Exception handler code:
            try_except_handler_3:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( outline_0_var_s );
            outline_0_var_s = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto outline_exception_1;
            // End of try:
            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
            return NULL;
            outline_exception_1:;
            exception_lineno = 64;
            goto frame_exception_exit_1;
            outline_result_1:;
            tmp_args_element_name_13 = const_str_digest_c64595dbb448219932b659de56da4f30;
            frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 64;
            {
                PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13 };
                tmp_right_name_13 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_args_element_name_12 );
            if ( tmp_right_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_13 );

                exception_lineno = 64;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_left_name_12 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_13, tmp_right_name_13 );
            Py_DECREF( tmp_left_name_13 );
            Py_DECREF( tmp_right_name_13 );
            if ( tmp_left_name_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 64;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_close_paren );
            tmp_right_name_14 = var_close_paren;
            tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_12, tmp_right_name_14 );
            Py_DECREF( tmp_left_name_12 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 65;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_8:;
    }
    {
        PyObject *tmp_assign_source_14;
        // Tried code:
        {
            PyObject *tmp_assign_source_15;
            PyObject *tmp_iter_arg_3;
            CHECK_OBJECT( par_strings );
            tmp_iter_arg_3 = par_strings;
            tmp_assign_source_15 = MAKE_ITERATOR( tmp_iter_arg_3 );
            if ( tmp_assign_source_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;
                type_description_1 = "ooocoooooooc";
                goto try_except_handler_5;
            }
            assert( tmp_listcomp_2__$0 == NULL );
            tmp_listcomp_2__$0 = tmp_assign_source_15;
        }
        {
            PyObject *tmp_assign_source_16;
            tmp_assign_source_16 = PyList_New( 0 );
            assert( tmp_listcomp_2__contraction == NULL );
            tmp_listcomp_2__contraction = tmp_assign_source_16;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_68a96a32eae4e3b939f6b91c88796c45_3, codeobj_68a96a32eae4e3b939f6b91c88796c45, module_pygments$regexopt, sizeof(void *) );
        frame_68a96a32eae4e3b939f6b91c88796c45_3 = cache_frame_68a96a32eae4e3b939f6b91c88796c45_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_68a96a32eae4e3b939f6b91c88796c45_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_68a96a32eae4e3b939f6b91c88796c45_3 ) == 2 ); // Frame stack

        // Framed code:
        // Tried code:
        loop_start_3:;
        {
            PyObject *tmp_next_source_3;
            PyObject *tmp_assign_source_17;
            CHECK_OBJECT( tmp_listcomp_2__$0 );
            tmp_next_source_3 = tmp_listcomp_2__$0;
            tmp_assign_source_17 = ITERATOR_NEXT( tmp_next_source_3 );
            if ( tmp_assign_source_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_3;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_2 = "o";
                    exception_lineno = 67;
                    goto try_except_handler_6;
                }
            }

            {
                PyObject *old = tmp_listcomp_2__iter_value_0;
                tmp_listcomp_2__iter_value_0 = tmp_assign_source_17;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_18;
            CHECK_OBJECT( tmp_listcomp_2__iter_value_0 );
            tmp_assign_source_18 = tmp_listcomp_2__iter_value_0;
            {
                PyObject *old = outline_1_var_s;
                outline_1_var_s = tmp_assign_source_18;
                Py_INCREF( outline_1_var_s );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_append_list_2;
            PyObject *tmp_append_value_2;
            PyObject *tmp_subscribed_name_4;
            PyObject *tmp_subscript_name_4;
            CHECK_OBJECT( tmp_listcomp_2__contraction );
            tmp_append_list_2 = tmp_listcomp_2__contraction;
            CHECK_OBJECT( outline_1_var_s );
            tmp_subscribed_name_4 = outline_1_var_s;
            tmp_subscript_name_4 = const_slice_none_none_int_neg_1;
            tmp_append_value_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
            if ( tmp_append_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;
                type_description_2 = "o";
                goto try_except_handler_6;
            }
            assert( PyList_Check( tmp_append_list_2 ) );
            tmp_res = PyList_Append( tmp_append_list_2, tmp_append_value_2 );
            Py_DECREF( tmp_append_value_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 67;
                type_description_2 = "o";
                goto try_except_handler_6;
            }
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_2 = "o";
            goto try_except_handler_6;
        }
        goto loop_start_3;
        loop_end_3:;
        CHECK_OBJECT( tmp_listcomp_2__contraction );
        tmp_assign_source_14 = tmp_listcomp_2__contraction;
        Py_INCREF( tmp_assign_source_14 );
        goto try_return_handler_6;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
        return NULL;
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        goto frame_return_exit_3;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_4 = exception_type;
        exception_keeper_value_4 = exception_value;
        exception_keeper_tb_4 = exception_tb;
        exception_keeper_lineno_4 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__$0 );
        Py_DECREF( tmp_listcomp_2__$0 );
        tmp_listcomp_2__$0 = NULL;

        CHECK_OBJECT( (PyObject *)tmp_listcomp_2__contraction );
        Py_DECREF( tmp_listcomp_2__contraction );
        tmp_listcomp_2__contraction = NULL;

        Py_XDECREF( tmp_listcomp_2__iter_value_0 );
        tmp_listcomp_2__iter_value_0 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_4;
        exception_value = exception_keeper_value_4;
        exception_tb = exception_keeper_tb_4;
        exception_lineno = exception_keeper_lineno_4;

        goto frame_exception_exit_3;
        // End of try:

#if 0
        RESTORE_FRAME_EXCEPTION( frame_68a96a32eae4e3b939f6b91c88796c45_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_return_exit_3:;
#if 0
        RESTORE_FRAME_EXCEPTION( frame_68a96a32eae4e3b939f6b91c88796c45_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto try_return_handler_5;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_68a96a32eae4e3b939f6b91c88796c45_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_68a96a32eae4e3b939f6b91c88796c45_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_68a96a32eae4e3b939f6b91c88796c45_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_68a96a32eae4e3b939f6b91c88796c45_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_68a96a32eae4e3b939f6b91c88796c45_3,
            type_description_2,
            outline_1_var_s
        );


        // Release cached frame.
        if ( frame_68a96a32eae4e3b939f6b91c88796c45_3 == cache_frame_68a96a32eae4e3b939f6b91c88796c45_3 )
        {
            Py_DECREF( frame_68a96a32eae4e3b939f6b91c88796c45_3 );
        }
        cache_frame_68a96a32eae4e3b939f6b91c88796c45_3 = NULL;

        assertFrameObject( frame_68a96a32eae4e3b939f6b91c88796c45_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;
        type_description_1 = "ooocoooooooc";
        goto try_except_handler_5;
        skip_nested_handling_2:;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
        return NULL;
        // Return handler code:
        try_return_handler_5:;
        Py_XDECREF( outline_1_var_s );
        outline_1_var_s = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_5:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( outline_1_var_s );
        outline_1_var_s = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
        return NULL;
        outline_exception_2:;
        exception_lineno = 67;
        goto frame_exception_exit_1;
        outline_result_2:;
        assert( var_strings_rev == NULL );
        var_strings_rev = tmp_assign_source_14;
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_14;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_commonprefix );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_commonprefix );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "commonprefix" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 68;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_9;
        CHECK_OBJECT( var_strings_rev );
        tmp_args_element_name_14 = var_strings_rev;
        frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 68;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_assign_source_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
        }

        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        assert( var_suffix == NULL );
        var_suffix = tmp_assign_source_19;
    }
    {
        nuitka_bool tmp_condition_result_9;
        int tmp_truth_name_3;
        CHECK_OBJECT( var_suffix );
        tmp_truth_name_3 = CHECK_IF_TRUE( var_suffix );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_9 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        {
            PyObject *tmp_assign_source_20;
            PyObject *tmp_len_arg_6;
            CHECK_OBJECT( var_suffix );
            tmp_len_arg_6 = var_suffix;
            tmp_assign_source_20 = BUILTIN_LEN( tmp_len_arg_6 );
            if ( tmp_assign_source_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            assert( PyCell_GET( var_slen ) == NULL );
            PyCell_SET( var_slen, tmp_assign_source_20 );

        }
        {
            PyObject *tmp_left_name_15;
            PyObject *tmp_left_name_16;
            PyObject *tmp_left_name_17;
            PyObject *tmp_right_name_15;
            PyObject *tmp_called_name_10;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_args_element_name_15;
            PyObject *tmp_called_name_11;
            PyObject *tmp_args_element_name_16;
            PyObject *tmp_args_element_name_17;
            PyObject *tmp_right_name_16;
            PyObject *tmp_called_name_12;
            PyObject *tmp_mvar_value_11;
            PyObject *tmp_args_element_name_18;
            PyObject *tmp_subscribed_name_5;
            PyObject *tmp_subscript_name_5;
            PyObject *tmp_right_name_17;
            CHECK_OBJECT( par_open_paren );
            tmp_left_name_17 = par_open_paren;
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );
            }

            if ( tmp_mvar_value_10 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "regex_opt_inner" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 73;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_10 = tmp_mvar_value_10;
            tmp_called_name_11 = LOOKUP_BUILTIN( const_str_plain_sorted );
            assert( tmp_called_name_11 != NULL );
            {
                PyObject *tmp_assign_source_21;
                PyObject *tmp_iter_arg_4;
                CHECK_OBJECT( par_strings );
                tmp_iter_arg_4 = par_strings;
                tmp_assign_source_21 = MAKE_ITERATOR( tmp_iter_arg_4 );
                if ( tmp_assign_source_21 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 73;
                    type_description_1 = "ooocoooooooc";
                    goto frame_exception_exit_1;
                }
                assert( tmp_genexpr_1__$0 == NULL );
                tmp_genexpr_1__$0 = tmp_assign_source_21;
            }
            // Tried code:
            tmp_args_element_name_16 = pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_1_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_16)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );
            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_16)->m_closure[1] = var_slen;
            Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_16)->m_closure[1] );


            goto try_return_handler_7;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
            return NULL;
            // Return handler code:
            try_return_handler_7:;
            CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
            Py_DECREF( tmp_genexpr_1__$0 );
            tmp_genexpr_1__$0 = NULL;

            goto outline_result_3;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
            Py_DECREF( tmp_genexpr_1__$0 );
            tmp_genexpr_1__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
            return NULL;
            outline_result_3:;
            frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 73;
            {
                PyObject *call_args[] = { tmp_args_element_name_16 };
                tmp_args_element_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
            }

            Py_DECREF( tmp_args_element_name_16 );
            if ( tmp_args_element_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 73;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_17 = const_str_digest_c64595dbb448219932b659de56da4f30;
            frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 73;
            {
                PyObject *call_args[] = { tmp_args_element_name_15, tmp_args_element_name_17 };
                tmp_right_name_15 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_10, call_args );
            }

            Py_DECREF( tmp_args_element_name_15 );
            if ( tmp_right_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 73;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_left_name_16 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_17, tmp_right_name_15 );
            Py_DECREF( tmp_right_name_15 );
            if ( tmp_left_name_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 72;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_escape );

            if (unlikely( tmp_mvar_value_11 == NULL ))
            {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_escape );
            }

            if ( tmp_mvar_value_11 == NULL )
            {
                Py_DECREF( tmp_left_name_16 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "escape" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 74;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_12 = tmp_mvar_value_11;
            CHECK_OBJECT( var_suffix );
            tmp_subscribed_name_5 = var_suffix;
            tmp_subscript_name_5 = const_slice_none_none_int_neg_1;
            tmp_args_element_name_18 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
            if ( tmp_args_element_name_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_16 );

                exception_lineno = 74;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 74;
            {
                PyObject *call_args[] = { tmp_args_element_name_18 };
                tmp_right_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
            }

            Py_DECREF( tmp_args_element_name_18 );
            if ( tmp_right_name_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_16 );

                exception_lineno = 74;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_left_name_15 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_16, tmp_right_name_16 );
            Py_DECREF( tmp_left_name_16 );
            Py_DECREF( tmp_right_name_16 );
            if ( tmp_left_name_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_close_paren );
            tmp_right_name_17 = var_close_paren;
            tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_15, tmp_right_name_17 );
            Py_DECREF( tmp_left_name_15 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 74;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_9:;
    }
    {
        PyObject *tmp_left_name_18;
        PyObject *tmp_left_name_19;
        PyObject *tmp_right_name_18;
        PyObject *tmp_called_name_13;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_right_name_19;
        CHECK_OBJECT( par_open_paren );
        tmp_left_name_19 = par_open_paren;
        tmp_source_name_1 = const_str_chr_124;
        tmp_called_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        assert( !(tmp_called_name_13 == NULL) );
        {
            PyObject *tmp_assign_source_22;
            PyObject *tmp_iter_arg_5;
            PyObject *tmp_called_name_14;
            PyObject *tmp_mvar_value_12;
            PyObject *tmp_args_element_name_20;
            PyObject *tmp_args_element_name_21;
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_groupby );

            if (unlikely( tmp_mvar_value_12 == NULL ))
            {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_groupby );
            }

            if ( tmp_mvar_value_12 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "groupby" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 79;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }

            tmp_called_name_14 = tmp_mvar_value_12;
            CHECK_OBJECT( par_strings );
            tmp_args_element_name_20 = par_strings;
            tmp_args_element_name_21 = MAKE_FUNCTION_pygments$regexopt$$$function_2_regex_opt_inner$$$function_1_lambda(  );

            ((struct Nuitka_FunctionObject *)tmp_args_element_name_21)->m_closure[0] = var_first;
            Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_args_element_name_21)->m_closure[0] );


            frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 79;
            {
                PyObject *call_args[] = { tmp_args_element_name_20, tmp_args_element_name_21 };
                tmp_iter_arg_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_14, call_args );
            }

            Py_DECREF( tmp_args_element_name_21 );
            if ( tmp_iter_arg_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 79;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_22 = MAKE_ITERATOR( tmp_iter_arg_5 );
            Py_DECREF( tmp_iter_arg_5 );
            if ( tmp_assign_source_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 78;
                type_description_1 = "ooocoooooooc";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_2__$0 == NULL );
            tmp_genexpr_2__$0 = tmp_assign_source_22;
        }
        // Tried code:
        tmp_args_element_name_19 = pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_2_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_19)->m_closure[0] = PyCell_NEW0( tmp_genexpr_2__$0 );


        goto try_return_handler_8;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
        return NULL;
        // Return handler code:
        try_return_handler_8:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
        Py_DECREF( tmp_genexpr_2__$0 );
        tmp_genexpr_2__$0 = NULL;

        goto outline_result_4;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
        Py_DECREF( tmp_genexpr_2__$0 );
        tmp_genexpr_2__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
        return NULL;
        outline_result_4:;
        frame_9b3f37882c907843562bd5760b0b39ed->m_frame.f_lineno = 78;
        {
            PyObject *call_args[] = { tmp_args_element_name_19 };
            tmp_right_name_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
        }

        Py_DECREF( tmp_called_name_13 );
        Py_DECREF( tmp_args_element_name_19 );
        if ( tmp_right_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        tmp_left_name_18 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_19, tmp_right_name_18 );
        Py_DECREF( tmp_right_name_18 );
        if ( tmp_left_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_close_paren );
        tmp_right_name_19 = var_close_paren;
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_18, tmp_right_name_19 );
        Py_DECREF( tmp_left_name_18 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "ooocoooooooc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b3f37882c907843562bd5760b0b39ed );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_3;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b3f37882c907843562bd5760b0b39ed );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b3f37882c907843562bd5760b0b39ed );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9b3f37882c907843562bd5760b0b39ed, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9b3f37882c907843562bd5760b0b39ed->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9b3f37882c907843562bd5760b0b39ed, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9b3f37882c907843562bd5760b0b39ed,
        type_description_1,
        par_strings,
        par_open_paren,
        var_close_paren,
        var_first,
        var_oneletter,
        var_rest,
        var_s,
        var_prefix,
        var_plen,
        var_strings_rev,
        var_suffix,
        var_slen
    );


    // Release cached frame.
    if ( frame_9b3f37882c907843562bd5760b0b39ed == cache_frame_9b3f37882c907843562bd5760b0b39ed )
    {
        Py_DECREF( frame_9b3f37882c907843562bd5760b0b39ed );
    }
    cache_frame_9b3f37882c907843562bd5760b0b39ed = NULL;

    assertFrameObject( frame_9b3f37882c907843562bd5760b0b39ed );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_3:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_strings );
    Py_DECREF( par_strings );
    par_strings = NULL;

    CHECK_OBJECT( (PyObject *)par_open_paren );
    Py_DECREF( par_open_paren );
    par_open_paren = NULL;

    CHECK_OBJECT( (PyObject *)var_close_paren );
    Py_DECREF( var_close_paren );
    var_close_paren = NULL;

    CHECK_OBJECT( (PyObject *)var_first );
    Py_DECREF( var_first );
    var_first = NULL;

    Py_XDECREF( var_oneletter );
    var_oneletter = NULL;

    Py_XDECREF( var_rest );
    var_rest = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    Py_XDECREF( var_prefix );
    var_prefix = NULL;

    Py_XDECREF( var_plen );
    var_plen = NULL;

    Py_XDECREF( var_strings_rev );
    var_strings_rev = NULL;

    Py_XDECREF( var_suffix );
    var_suffix = NULL;

    CHECK_OBJECT( (PyObject *)var_slen );
    Py_DECREF( var_slen );
    var_slen = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_strings );
    Py_DECREF( par_strings );
    par_strings = NULL;

    CHECK_OBJECT( (PyObject *)par_open_paren );
    Py_DECREF( par_open_paren );
    par_open_paren = NULL;

    Py_XDECREF( var_close_paren );
    var_close_paren = NULL;

    CHECK_OBJECT( (PyObject *)var_first );
    Py_DECREF( var_first );
    var_first = NULL;

    Py_XDECREF( var_oneletter );
    var_oneletter = NULL;

    Py_XDECREF( var_rest );
    var_rest = NULL;

    Py_XDECREF( var_s );
    var_s = NULL;

    Py_XDECREF( var_prefix );
    var_prefix = NULL;

    Py_XDECREF( var_plen );
    var_plen = NULL;

    Py_XDECREF( var_strings_rev );
    var_strings_rev = NULL;

    Py_XDECREF( var_suffix );
    var_suffix = NULL;

    CHECK_OBJECT( (PyObject *)var_slen );
    Py_DECREF( var_slen );
    var_slen = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_1_genexpr_locals {
    PyObject *var_s;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_1_genexpr_locals *generator_heap = (struct pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_s = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_a5c233acb2de4313fccd2cffd6ace119, module_pygments$regexopt, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noc";
                generator_heap->exception_lineno = 73;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_s;
            generator_heap->var_s = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_s );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_step_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_s );
        tmp_subscribed_name_1 = generator_heap->var_s;
        tmp_start_name_1 = Py_None;
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "slen" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 73;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }

        tmp_operand_name_1 = PyCell_GET( generator->m_closure[1] );
        tmp_stop_name_1 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
        if ( tmp_stop_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 73;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        Py_DECREF( tmp_stop_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_expression_name_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 73;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), &tmp_start_name_1, sizeof(PyObject *), &tmp_stop_name_1, sizeof(PyObject *), &tmp_operand_name_1, sizeof(PyObject *), &tmp_step_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), &tmp_start_name_1, sizeof(PyObject *), &tmp_stop_name_1, sizeof(PyObject *), &tmp_operand_name_1, sizeof(PyObject *), &tmp_step_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 73;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 73;
        generator_heap->type_description_1 = "Noc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_s,
            generator->m_closure[1]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_s );
    generator_heap->var_s = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_s );
    generator_heap->var_s = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_1_genexpr_context,
        module_pygments$regexopt,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_0a1d3ec37c4216862507e32dcc18487f,
#endif
        codeobj_a5c233acb2de4313fccd2cffd6ace119,
        2,
        sizeof(struct pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_pygments$regexopt$$$function_2_regex_opt_inner$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_s = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_aa56ee63a94d87385eded3153043b17e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_aa56ee63a94d87385eded3153043b17e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_aa56ee63a94d87385eded3153043b17e, codeobj_aa56ee63a94d87385eded3153043b17e, module_pygments$regexopt, sizeof(void *)+sizeof(void *) );
    frame_aa56ee63a94d87385eded3153043b17e = cache_frame_aa56ee63a94d87385eded3153043b17e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_aa56ee63a94d87385eded3153043b17e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_aa56ee63a94d87385eded3153043b17e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_subscript_name_2;
        CHECK_OBJECT( par_s );
        tmp_subscribed_name_1 = par_s;
        tmp_subscript_name_1 = const_int_0;
        tmp_compexpr_left_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_compexpr_left_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "first" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 79;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_subscribed_name_2 = PyCell_GET( self->m_closure[0] );
        tmp_subscript_name_2 = const_int_0;
        tmp_compexpr_right_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_compexpr_left_1 );

            exception_lineno = 79;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_return_value = RICH_COMPARE_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa56ee63a94d87385eded3153043b17e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa56ee63a94d87385eded3153043b17e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa56ee63a94d87385eded3153043b17e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_aa56ee63a94d87385eded3153043b17e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_aa56ee63a94d87385eded3153043b17e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_aa56ee63a94d87385eded3153043b17e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_aa56ee63a94d87385eded3153043b17e,
        type_description_1,
        par_s,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_aa56ee63a94d87385eded3153043b17e == cache_frame_aa56ee63a94d87385eded3153043b17e )
    {
        Py_DECREF( frame_aa56ee63a94d87385eded3153043b17e );
    }
    cache_frame_aa56ee63a94d87385eded3153043b17e = NULL;

    assertFrameObject( frame_aa56ee63a94d87385eded3153043b17e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner$$$function_1_lambda );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_s );
    Py_DECREF( par_s );
    par_s = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_2_regex_opt_inner$$$function_1_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_2_genexpr_locals {
    PyObject *var_group;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_2_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_2_genexpr_locals *generator_heap = (struct pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_2_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_group = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_2612fdefdb533b33ab494dd312882cd1, module_pygments$regexopt, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 78;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_group;
            generator_heap->var_group = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_group );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_args_element_name_2;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "regex_opt_inner" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 78;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( generator_heap->var_group );
        tmp_subscribed_name_1 = generator_heap->var_group;
        tmp_subscript_name_1 = const_int_pos_1;
        tmp_list_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 78;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_args_element_name_1 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 78;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_args_element_name_2 = const_str_empty;
        generator->m_frame->m_frame.f_lineno = 78;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_expression_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 78;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_list_arg_1, sizeof(PyObject *), &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), &tmp_args_element_name_1, sizeof(PyObject *), &tmp_list_arg_1, sizeof(PyObject *), &tmp_subscribed_name_1, sizeof(PyObject *), &tmp_subscript_name_1, sizeof(PyObject *), &tmp_args_element_name_2, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 78;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 78;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_group
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_group );
    generator_heap->var_group = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_group );
    generator_heap->var_group = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_2_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_2_genexpr_context,
        module_pygments$regexopt,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_0a1d3ec37c4216862507e32dcc18487f,
#endif
        codeobj_2612fdefdb533b33ab494dd312882cd1,
        1,
        sizeof(struct pygments$regexopt$$$function_2_regex_opt_inner$$$genexpr_2_genexpr_locals)
    );
}


static PyObject *impl_pygments$regexopt$$$function_3_regex_opt( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_strings = python_pars[ 0 ];
    PyObject *par_prefix = python_pars[ 1 ];
    PyObject *par_suffix = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_6ccda6d91de4f7cceac77b4e07a3d00b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_6ccda6d91de4f7cceac77b4e07a3d00b = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6ccda6d91de4f7cceac77b4e07a3d00b, codeobj_6ccda6d91de4f7cceac77b4e07a3d00b, module_pygments$regexopt, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6ccda6d91de4f7cceac77b4e07a3d00b = cache_frame_6ccda6d91de4f7cceac77b4e07a3d00b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6ccda6d91de4f7cceac77b4e07a3d00b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6ccda6d91de4f7cceac77b4e07a3d00b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_element_name_1;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_sorted );
        assert( tmp_called_name_1 != NULL );
        CHECK_OBJECT( par_strings );
        tmp_args_element_name_1 = par_strings;
        frame_6ccda6d91de4f7cceac77b4e07a3d00b->m_frame.f_lineno = 91;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_strings;
            assert( old != NULL );
            par_strings = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_right_name_2;
        CHECK_OBJECT( par_prefix );
        tmp_left_name_2 = par_prefix;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_regex_opt_inner );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "regex_opt_inner" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 92;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        CHECK_OBJECT( par_strings );
        tmp_args_element_name_2 = par_strings;
        tmp_args_element_name_3 = const_str_chr_40;
        frame_6ccda6d91de4f7cceac77b4e07a3d00b->m_frame.f_lineno = 92;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_right_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_suffix );
        tmp_right_name_2 = par_suffix;
        tmp_return_value = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6ccda6d91de4f7cceac77b4e07a3d00b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6ccda6d91de4f7cceac77b4e07a3d00b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6ccda6d91de4f7cceac77b4e07a3d00b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6ccda6d91de4f7cceac77b4e07a3d00b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6ccda6d91de4f7cceac77b4e07a3d00b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6ccda6d91de4f7cceac77b4e07a3d00b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6ccda6d91de4f7cceac77b4e07a3d00b,
        type_description_1,
        par_strings,
        par_prefix,
        par_suffix
    );


    // Release cached frame.
    if ( frame_6ccda6d91de4f7cceac77b4e07a3d00b == cache_frame_6ccda6d91de4f7cceac77b4e07a3d00b )
    {
        Py_DECREF( frame_6ccda6d91de4f7cceac77b4e07a3d00b );
    }
    cache_frame_6ccda6d91de4f7cceac77b4e07a3d00b = NULL;

    assertFrameObject( frame_6ccda6d91de4f7cceac77b4e07a3d00b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_3_regex_opt );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_strings );
    Py_DECREF( par_strings );
    par_strings = NULL;

    CHECK_OBJECT( (PyObject *)par_prefix );
    Py_DECREF( par_prefix );
    par_prefix = NULL;

    CHECK_OBJECT( (PyObject *)par_suffix );
    Py_DECREF( par_suffix );
    par_suffix = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_strings );
    Py_DECREF( par_strings );
    par_strings = NULL;

    CHECK_OBJECT( (PyObject *)par_prefix );
    Py_DECREF( par_prefix );
    par_prefix = NULL;

    CHECK_OBJECT( (PyObject *)par_suffix );
    Py_DECREF( par_suffix );
    par_suffix = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( pygments$regexopt$$$function_3_regex_opt );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_pygments$regexopt$$$function_1_make_charset(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$regexopt$$$function_1_make_charset,
        const_str_plain_make_charset,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_458a46c6911aeeda0098d40d0444831d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$regexopt,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$regexopt$$$function_1_make_charset$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$regexopt$$$function_1_make_charset$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_7ddfb0de4e167dc289be4b4442e18e68,
#endif
        codeobj_4ae53562437f62ba2121875b06f4be02,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$regexopt,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$regexopt$$$function_2_regex_opt_inner(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$regexopt$$$function_2_regex_opt_inner,
        const_str_plain_regex_opt_inner,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9b3f37882c907843562bd5760b0b39ed,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$regexopt,
        const_str_digest_a591309809460e293fa6e61fa316544f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$regexopt$$$function_2_regex_opt_inner$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$regexopt$$$function_2_regex_opt_inner$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_7a6bdb3f76a16962004591e54418e1a2,
#endif
        codeobj_aa56ee63a94d87385eded3153043b17e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$regexopt,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_pygments$regexopt$$$function_3_regex_opt( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_pygments$regexopt$$$function_3_regex_opt,
        const_str_plain_regex_opt,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6ccda6d91de4f7cceac77b4e07a3d00b,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_pygments$regexopt,
        const_str_digest_8642ce1dc5a05e0c723e523581f32262,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_pygments$regexopt =
{
    PyModuleDef_HEAD_INIT,
    "pygments.regexopt",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(pygments$regexopt)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(pygments$regexopt)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_pygments$regexopt );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("pygments.regexopt: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pygments.regexopt: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("pygments.regexopt: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initpygments$regexopt" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_pygments$regexopt = Py_InitModule4(
        "pygments.regexopt",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_pygments$regexopt = PyModule_Create( &mdef_pygments$regexopt );
#endif

    moduledict_pygments$regexopt = MODULE_DICT( module_pygments$regexopt );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_pygments$regexopt,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_pygments$regexopt,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_pygments$regexopt,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_pygments$regexopt,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_pygments$regexopt );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_ba02bab26133df200d8839dde6036acb, module_pygments$regexopt );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_7b214c6996713caf50702de8719b6701;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_61b3fcba8d7123eba0f194d03f1d0f75;
        UPDATE_STRING_DICT0( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_7b214c6996713caf50702de8719b6701 = MAKE_MODULE_FRAME( codeobj_7b214c6996713caf50702de8719b6701, module_pygments$regexopt );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_7b214c6996713caf50702de8719b6701 );
    assert( Py_REFCNT( frame_7b214c6996713caf50702de8719b6701 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_re;
        tmp_globals_name_1 = (PyObject *)moduledict_pygments$regexopt;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_7b214c6996713caf50702de8719b6701->m_frame.f_lineno = 13;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_re, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_re;
        tmp_globals_name_2 = (PyObject *)moduledict_pygments$regexopt;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_escape_tuple;
        tmp_level_name_2 = const_int_0;
        frame_7b214c6996713caf50702de8719b6701->m_frame.f_lineno = 14;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_escape );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_escape, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_e399ba4554180f37de594a6743234f17;
        tmp_globals_name_3 = (PyObject *)moduledict_pygments$regexopt;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_commonprefix_tuple;
        tmp_level_name_3 = const_int_0;
        frame_7b214c6996713caf50702de8719b6701->m_frame.f_lineno = 15;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_commonprefix );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_commonprefix, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_itertools;
        tmp_globals_name_4 = (PyObject *)moduledict_pygments$regexopt;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_groupby_tuple;
        tmp_level_name_4 = const_int_0;
        frame_7b214c6996713caf50702de8719b6701->m_frame.f_lineno = 16;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        assert( !(tmp_import_name_from_3 == NULL) );
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_groupby );
        Py_DECREF( tmp_import_name_from_3 );
        assert( !(tmp_assign_source_7 == NULL) );
        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_groupby, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_operator;
        tmp_globals_name_5 = (PyObject *)moduledict_pygments$regexopt;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_itemgetter_tuple;
        tmp_level_name_5 = const_int_0;
        frame_7b214c6996713caf50702de8719b6701->m_frame.f_lineno = 17;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_itemgetter );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_itemgetter, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_re );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_re );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "re" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 19;

            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_3;
        frame_7b214c6996713caf50702de8719b6701->m_frame.f_lineno = 19;
        tmp_assign_source_9 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_compile, &PyTuple_GET_ITEM( const_tuple_str_digest_c2bbb78996da99693dc4e3fc4fcbf0dd_tuple, 0 ) );

        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 19;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_CS_ESCAPE, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_itemgetter );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_itemgetter );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "itemgetter" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 20;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_4;
        frame_7b214c6996713caf50702de8719b6701->m_frame.f_lineno = 20;
        tmp_assign_source_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_FIRST_ELEMENT, tmp_assign_source_10 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b214c6996713caf50702de8719b6701 );
#endif
    popFrameStack();

    assertFrameObject( frame_7b214c6996713caf50702de8719b6701 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_7b214c6996713caf50702de8719b6701 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7b214c6996713caf50702de8719b6701, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7b214c6996713caf50702de8719b6701->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7b214c6996713caf50702de8719b6701, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_pygments$regexopt$$$function_1_make_charset(  );



        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_make_charset, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_pygments$regexopt$$$function_2_regex_opt_inner(  );



        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_regex_opt_inner, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_str_empty_str_empty_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_13 = MAKE_FUNCTION_pygments$regexopt$$$function_3_regex_opt( tmp_defaults_1 );



        UPDATE_STRING_DICT1( moduledict_pygments$regexopt, (Nuitka_StringObject *)const_str_plain_regex_opt, tmp_assign_source_13 );
    }

    return MOD_RETURN_VALUE( module_pygments$regexopt );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
