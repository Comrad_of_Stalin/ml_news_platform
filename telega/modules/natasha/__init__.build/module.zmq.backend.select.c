/* Generated code for Python module 'zmq.backend.select'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_zmq$backend$select" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_zmq$backend$select;
PyDictObject *moduledict_zmq$backend$select;

/* The declarations of module constants used, if any. */
extern PyObject *const_tuple_str_plain_reraise_tuple;
static PyObject *const_str_plain_curve_public;
extern PyObject *const_str_plain_zmq_version_info;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_digest_bfaed2f206a5cff83965655b2364e9a3;
extern PyObject *const_str_plain_sys;
extern PyObject *const_str_plain_strerror;
extern PyObject *const_str_plain_device;
extern PyObject *const_str_plain_Context;
static PyObject *const_str_plain_IPC_PATH_MAX_LEN;
extern PyObject *const_str_plain_Message;
extern PyObject *const_str_plain_constants;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_digest_fd947c308ccedccc85b10a795b6136ec;
extern PyObject *const_str_digest_db1ae78ab8269b36fe118584040c858d;
extern PyObject *const_str_plain_key;
extern PyObject *const_int_0;
static PyObject *const_str_digest_3ee00b4114f50de82731043565e91062;
extern PyObject *const_str_plain_select_backend;
static PyObject *const_str_digest_a5e27b23f246d3ab23bb3bd8e7748b5e;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_digest_b02ad234820ee046133451a5a61ca33a;
extern PyObject *const_str_plain_public_api;
extern PyObject *const_str_plain_zmq_errno;
extern PyObject *const_str_plain_ns;
static PyObject *const_list_a94c096f8db53fa1b16e7950f3381e3e_list;
static PyObject *const_str_plain_proxy_steerable;
extern PyObject *const_str_plain_exc_info;
extern PyObject *const_str_plain_Frame;
extern PyObject *const_str_plain_has;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_mod;
extern PyObject *const_str_plain_Socket;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain_curve_keypair;
extern PyObject *const_int_pos_2;
static PyObject *const_tuple_1343138888d28222f77547cb31203eaa_tuple;
extern PyObject *const_str_plain_name;
extern PyObject *const_str_plain_reraise;
extern PyObject *const_str_plain_proxy;
static PyObject *const_str_digest_36f97cb83a2b20c57acd75f2415c981f;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_zmq_poll;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain_curve_public = UNSTREAM_STRING_ASCII( &constant_bin[ 5779189 ], 12, 1 );
    const_str_digest_bfaed2f206a5cff83965655b2364e9a3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5779201 ], 27, 0 );
    const_str_plain_IPC_PATH_MAX_LEN = UNSTREAM_STRING_ASCII( &constant_bin[ 5779228 ], 16, 1 );
    const_str_digest_fd947c308ccedccc85b10a795b6136ec = UNSTREAM_STRING_ASCII( &constant_bin[ 5779244 ], 21, 0 );
    const_str_digest_3ee00b4114f50de82731043565e91062 = UNSTREAM_STRING_ASCII( &constant_bin[ 5779209 ], 18, 0 );
    const_str_digest_a5e27b23f246d3ab23bb3bd8e7748b5e = UNSTREAM_STRING_ASCII( &constant_bin[ 5779265 ], 27, 0 );
    const_list_a94c096f8db53fa1b16e7950f3381e3e_list = PyList_New( 16 );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 0, const_str_plain_Context ); Py_INCREF( const_str_plain_Context );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 1, const_str_plain_Socket ); Py_INCREF( const_str_plain_Socket );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 2, const_str_plain_Frame ); Py_INCREF( const_str_plain_Frame );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 3, const_str_plain_Message ); Py_INCREF( const_str_plain_Message );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 4, const_str_plain_device ); Py_INCREF( const_str_plain_device );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 5, const_str_plain_proxy ); Py_INCREF( const_str_plain_proxy );
    const_str_plain_proxy_steerable = UNSTREAM_STRING_ASCII( &constant_bin[ 5779047 ], 15, 1 );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 6, const_str_plain_proxy_steerable ); Py_INCREF( const_str_plain_proxy_steerable );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 7, const_str_plain_zmq_poll ); Py_INCREF( const_str_plain_zmq_poll );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 8, const_str_plain_strerror ); Py_INCREF( const_str_plain_strerror );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 9, const_str_plain_zmq_errno ); Py_INCREF( const_str_plain_zmq_errno );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 10, const_str_plain_has ); Py_INCREF( const_str_plain_has );
    const_str_plain_curve_keypair = UNSTREAM_STRING_ASCII( &constant_bin[ 5779292 ], 13, 1 );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 11, const_str_plain_curve_keypair ); Py_INCREF( const_str_plain_curve_keypair );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 12, const_str_plain_curve_public ); Py_INCREF( const_str_plain_curve_public );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 13, const_str_plain_constants ); Py_INCREF( const_str_plain_constants );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 14, const_str_plain_zmq_version_info ); Py_INCREF( const_str_plain_zmq_version_info );
    PyList_SET_ITEM( const_list_a94c096f8db53fa1b16e7950f3381e3e_list, 15, const_str_plain_IPC_PATH_MAX_LEN ); Py_INCREF( const_str_plain_IPC_PATH_MAX_LEN );
    const_tuple_1343138888d28222f77547cb31203eaa_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_1343138888d28222f77547cb31203eaa_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_1343138888d28222f77547cb31203eaa_tuple, 1, const_str_plain_mod ); Py_INCREF( const_str_plain_mod );
    PyTuple_SET_ITEM( const_tuple_1343138888d28222f77547cb31203eaa_tuple, 2, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    PyTuple_SET_ITEM( const_tuple_1343138888d28222f77547cb31203eaa_tuple, 3, const_str_plain_sys ); Py_INCREF( const_str_plain_sys );
    PyTuple_SET_ITEM( const_tuple_1343138888d28222f77547cb31203eaa_tuple, 4, const_str_plain_reraise ); Py_INCREF( const_str_plain_reraise );
    PyTuple_SET_ITEM( const_tuple_1343138888d28222f77547cb31203eaa_tuple, 5, const_str_plain_exc_info ); Py_INCREF( const_str_plain_exc_info );
    PyTuple_SET_ITEM( const_tuple_1343138888d28222f77547cb31203eaa_tuple, 6, const_str_plain_ns ); Py_INCREF( const_str_plain_ns );
    PyTuple_SET_ITEM( const_tuple_1343138888d28222f77547cb31203eaa_tuple, 7, const_str_plain_key ); Py_INCREF( const_str_plain_key );
    const_str_digest_36f97cb83a2b20c57acd75f2415c981f = UNSTREAM_STRING_ASCII( &constant_bin[ 5779305 ], 24, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_zmq$backend$select( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_21071ac29b7cfa7e6ff99ef0daeb4184;
static PyCodeObject *codeobj_8f222a0be348806bbdf911edd1c153cc;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_fd947c308ccedccc85b10a795b6136ec );
    codeobj_21071ac29b7cfa7e6ff99ef0daeb4184 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_bfaed2f206a5cff83965655b2364e9a3, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_8f222a0be348806bbdf911edd1c153cc = MAKE_CODEOBJ( module_filename_obj, const_str_plain_select_backend, 25, const_tuple_1343138888d28222f77547cb31203eaa_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_zmq$backend$select$$$function_1_select_backend(  );


// The module function definitions.
static PyObject *impl_zmq$backend$select$$$function_1_select_backend( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    PyObject *var_mod = NULL;
    PyObject *var_e = NULL;
    PyObject *var_sys = NULL;
    PyObject *var_reraise = NULL;
    PyObject *var_exc_info = NULL;
    PyObject *var_ns = NULL;
    PyObject *var_key = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_8f222a0be348806bbdf911edd1c153cc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    static struct Nuitka_FrameObject *cache_frame_8f222a0be348806bbdf911edd1c153cc = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8f222a0be348806bbdf911edd1c153cc, codeobj_8f222a0be348806bbdf911edd1c153cc, module_zmq$backend$select, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8f222a0be348806bbdf911edd1c153cc = cache_frame_8f222a0be348806bbdf911edd1c153cc;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8f222a0be348806bbdf911edd1c153cc );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8f222a0be348806bbdf911edd1c153cc ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_name );
        tmp_name_name_1 = par_name;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain_public_api );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_public_api );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "public_api" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 28;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }

        tmp_fromlist_name_1 = tmp_mvar_value_1;
        frame_8f222a0be348806bbdf911edd1c153cc->m_frame.f_lineno = 28;
        tmp_assign_source_1 = IMPORT_MODULE_KW( tmp_name_name_1, NULL, NULL, tmp_fromlist_name_1, NULL );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        assert( var_mod == NULL );
        var_mod = tmp_assign_source_1;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_8f222a0be348806bbdf911edd1c153cc, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_8f222a0be348806bbdf911edd1c153cc, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_ImportError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 30;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_8f222a0be348806bbdf911edd1c153cc->m_frame) frame_8f222a0be348806bbdf911edd1c153cc->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooooo";
        goto try_except_handler_3;
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
            tmp_compexpr_right_2 = PyExc_Exception;
            tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 31;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                tmp_assign_source_2 = EXC_VALUE(PyThreadState_GET());
                assert( var_e == NULL );
                Py_INCREF( tmp_assign_source_2 );
                var_e = tmp_assign_source_2;
            }
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_name_name_2;
                PyObject *tmp_globals_name_1;
                PyObject *tmp_locals_name_1;
                PyObject *tmp_fromlist_name_2;
                PyObject *tmp_level_name_1;
                tmp_name_name_2 = const_str_plain_sys;
                tmp_globals_name_1 = (PyObject *)moduledict_zmq$backend$select;
                tmp_locals_name_1 = Py_None;
                tmp_fromlist_name_2 = Py_None;
                tmp_level_name_1 = const_int_0;
                frame_8f222a0be348806bbdf911edd1c153cc->m_frame.f_lineno = 32;
                tmp_assign_source_3 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_2, tmp_level_name_1 );
                assert( !(tmp_assign_source_3 == NULL) );
                assert( var_sys == NULL );
                var_sys = tmp_assign_source_3;
            }
            // Tried code:
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_import_name_from_1;
                PyObject *tmp_name_name_3;
                PyObject *tmp_globals_name_2;
                PyObject *tmp_locals_name_2;
                PyObject *tmp_fromlist_name_3;
                PyObject *tmp_level_name_2;
                tmp_name_name_3 = const_str_digest_b02ad234820ee046133451a5a61ca33a;
                tmp_globals_name_2 = (PyObject *)moduledict_zmq$backend$select;
                tmp_locals_name_2 = Py_None;
                tmp_fromlist_name_3 = const_tuple_str_plain_reraise_tuple;
                tmp_level_name_2 = const_int_0;
                frame_8f222a0be348806bbdf911edd1c153cc->m_frame.f_lineno = 33;
                tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_3, tmp_level_name_2 );
                if ( tmp_import_name_from_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 33;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_4;
                }
                tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_reraise );
                Py_DECREF( tmp_import_name_from_1 );
                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 33;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_4;
                }
                assert( var_reraise == NULL );
                var_reraise = tmp_assign_source_4;
            }
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_called_instance_1;
                CHECK_OBJECT( var_sys );
                tmp_called_instance_1 = var_sys;
                frame_8f222a0be348806bbdf911edd1c153cc->m_frame.f_lineno = 34;
                tmp_assign_source_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_exc_info );
                if ( tmp_assign_source_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 34;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_4;
                }
                assert( var_exc_info == NULL );
                var_exc_info = tmp_assign_source_5;
            }
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_make_exception_arg_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_subscript_name_1;
                CHECK_OBJECT( var_reraise );
                tmp_called_name_1 = var_reraise;
                tmp_args_element_name_1 = PyExc_ImportError;
                tmp_left_name_1 = const_str_digest_a5e27b23f246d3ab23bb3bd8e7748b5e;
                CHECK_OBJECT( par_name );
                tmp_tuple_element_1 = par_name;
                tmp_right_name_1 = PyTuple_New( 2 );
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
                CHECK_OBJECT( var_e );
                tmp_tuple_element_1 = var_e;
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
                tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_make_exception_arg_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 35;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_4;
                }
                frame_8f222a0be348806bbdf911edd1c153cc->m_frame.f_lineno = 35;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_1 };
                    tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ImportError, call_args );
                }

                Py_DECREF( tmp_make_exception_arg_1 );
                assert( !(tmp_args_element_name_2 == NULL) );
                CHECK_OBJECT( var_exc_info );
                tmp_subscribed_name_1 = var_exc_info;
                tmp_subscript_name_1 = const_int_pos_2;
                tmp_args_element_name_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 2 );
                if ( tmp_args_element_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_args_element_name_2 );

                    exception_lineno = 35;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_4;
                }
                frame_8f222a0be348806bbdf911edd1c153cc->m_frame.f_lineno = 35;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                    tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_args_element_name_2 );
                Py_DECREF( tmp_args_element_name_3 );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 35;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_4;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            goto try_end_2;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( var_e );
            var_e = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto try_except_handler_3;
            // End of try:
            try_end_2:;
            Py_XDECREF( var_e );
            var_e = NULL;

            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 27;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_8f222a0be348806bbdf911edd1c153cc->m_frame) frame_8f222a0be348806bbdf911edd1c153cc->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
            branch_end_2:;
        }
        branch_end_1:;
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_1;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( zmq$backend$select$$$function_1_select_backend );
    return NULL;
    // End of try:
    try_end_1:;
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = PyDict_New();
        assert( var_ns == NULL );
        var_ns = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_mvar_value_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain_public_api );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_public_api );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "public_api" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 38;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_iter_arg_1 = tmp_mvar_value_2;
        tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_7;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 38;
                goto try_except_handler_5;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_9 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_key;
            var_key = tmp_assign_source_9;
            Py_INCREF( var_key );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        if ( var_mod == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "mod" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 39;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }

        tmp_getattr_target_1 = var_mod;
        CHECK_OBJECT( var_key );
        tmp_getattr_attr_1 = var_key;
        tmp_dictset_value = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        if ( tmp_dictset_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }
        CHECK_OBJECT( var_ns );
        tmp_dictset_dict = var_ns;
        CHECK_OBJECT( var_key );
        tmp_dictset_key = var_key;
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 38;
        type_description_1 = "oooooooo";
        goto try_except_handler_5;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f222a0be348806bbdf911edd1c153cc );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8f222a0be348806bbdf911edd1c153cc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8f222a0be348806bbdf911edd1c153cc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8f222a0be348806bbdf911edd1c153cc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8f222a0be348806bbdf911edd1c153cc, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8f222a0be348806bbdf911edd1c153cc,
        type_description_1,
        par_name,
        var_mod,
        var_e,
        var_sys,
        var_reraise,
        var_exc_info,
        var_ns,
        var_key
    );


    // Release cached frame.
    if ( frame_8f222a0be348806bbdf911edd1c153cc == cache_frame_8f222a0be348806bbdf911edd1c153cc )
    {
        Py_DECREF( frame_8f222a0be348806bbdf911edd1c153cc );
    }
    cache_frame_8f222a0be348806bbdf911edd1c153cc = NULL;

    assertFrameObject( frame_8f222a0be348806bbdf911edd1c153cc );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    CHECK_OBJECT( var_ns );
    tmp_return_value = var_ns;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( zmq$backend$select$$$function_1_select_backend );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_mod );
    var_mod = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    Py_XDECREF( var_sys );
    var_sys = NULL;

    Py_XDECREF( var_reraise );
    var_reraise = NULL;

    Py_XDECREF( var_exc_info );
    var_exc_info = NULL;

    CHECK_OBJECT( (PyObject *)var_ns );
    Py_DECREF( var_ns );
    var_ns = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_mod );
    var_mod = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    Py_XDECREF( var_sys );
    var_sys = NULL;

    Py_XDECREF( var_reraise );
    var_reraise = NULL;

    Py_XDECREF( var_exc_info );
    var_exc_info = NULL;

    Py_XDECREF( var_ns );
    var_ns = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( zmq$backend$select$$$function_1_select_backend );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_zmq$backend$select$$$function_1_select_backend(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_zmq$backend$select$$$function_1_select_backend,
        const_str_plain_select_backend,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8f222a0be348806bbdf911edd1c153cc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_zmq$backend$select,
        const_str_digest_36f97cb83a2b20c57acd75f2415c981f,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_zmq$backend$select =
{
    PyModuleDef_HEAD_INIT,
    "zmq.backend.select",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(zmq$backend$select)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(zmq$backend$select)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_zmq$backend$select );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("zmq.backend.select: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("zmq.backend.select: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("zmq.backend.select: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initzmq$backend$select" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_zmq$backend$select = Py_InitModule4(
        "zmq.backend.select",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_zmq$backend$select = PyModule_Create( &mdef_zmq$backend$select );
#endif

    moduledict_zmq$backend$select = MODULE_DICT( module_zmq$backend$select );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_zmq$backend$select,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_zmq$backend$select,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_zmq$backend$select,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_zmq$backend$select,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_zmq$backend$select );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_3ee00b4114f50de82731043565e91062, module_zmq$backend$select );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_21071ac29b7cfa7e6ff99ef0daeb4184;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_db1ae78ab8269b36fe118584040c858d;
        UPDATE_STRING_DICT0( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_21071ac29b7cfa7e6ff99ef0daeb4184 = MAKE_MODULE_FRAME( codeobj_21071ac29b7cfa7e6ff99ef0daeb4184, module_zmq$backend$select );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_21071ac29b7cfa7e6ff99ef0daeb4184 );
    assert( Py_REFCNT( frame_21071ac29b7cfa7e6ff99ef0daeb4184 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_21071ac29b7cfa7e6ff99ef0daeb4184 );
#endif
    popFrameStack();

    assertFrameObject( frame_21071ac29b7cfa7e6ff99ef0daeb4184 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_21071ac29b7cfa7e6ff99ef0daeb4184 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_21071ac29b7cfa7e6ff99ef0daeb4184, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_21071ac29b7cfa7e6ff99ef0daeb4184->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_21071ac29b7cfa7e6ff99ef0daeb4184, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = LIST_COPY( const_list_a94c096f8db53fa1b16e7950f3381e3e_list );
        UPDATE_STRING_DICT1( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain_public_api, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = MAKE_FUNCTION_zmq$backend$select$$$function_1_select_backend(  );



        UPDATE_STRING_DICT1( moduledict_zmq$backend$select, (Nuitka_StringObject *)const_str_plain_select_backend, tmp_assign_source_5 );
    }

    return MOD_RETURN_VALUE( module_zmq$backend$select );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
