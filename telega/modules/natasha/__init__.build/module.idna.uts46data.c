/* Generated code for Python module 'idna.uts46data'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_idna$uts46data" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_idna$uts46data;
PyDictObject *moduledict_idna$uts46data;

/* The declarations of module constants used, if any. */
static PyObject *const_str_plain__seg_44;
static PyObject *const_str_plain__seg_14;
extern PyObject *const_str_plain___spec__;
static PyObject *const_str_plain__seg_11;
static PyObject *const_str_plain__seg_73;
static PyObject *const_str_plain__seg_77;
static PyObject *const_str_plain__seg_35;
static PyObject *const_list_05eaf4095605f0ad2dac780ae2672c70_list;
static PyObject *const_str_plain__seg_49;
static PyObject *const_str_plain__seg_72;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_uts46data;
static PyObject *const_list_4ed37e673347c8bf4b1af9d04a4d2d32_list;
static PyObject *const_str_plain__seg_5;
static PyObject *const_list_834d28e04b8448e93bd1330606542306_list;
static PyObject *const_list_cf5f62a7137ededb7d4ae16b4e4fce3c_list;
static PyObject *const_str_plain__seg_2;
static PyObject *const_str_plain__seg_10;
static PyObject *const_str_plain__seg_57;
static PyObject *const_str_digest_dd792d04ec2314d7746fad73a142a1d6;
static PyObject *const_list_913ba5cfbf54f76f4ec142820765e5fb_list;
static PyObject *const_list_c9a124af4be98e21a9e979763c20be3d_list;
static PyObject *const_list_de909d1f27fedb734717ab0a1d4c2224_list;
static PyObject *const_list_4ed64b4aae3567efb08519f6f5a49fc9_list;
static PyObject *const_str_plain__seg_68;
static PyObject *const_str_plain__seg_60;
static PyObject *const_list_b723fc0ab5a71b0a5ca9d030a15109ab_list;
static PyObject *const_list_39b054c05ecf5f1f8c6e8f9ae5ef0167_list;
static PyObject *const_str_plain__seg_7;
static PyObject *const_str_plain__seg_76;
static PyObject *const_list_b4116cfcedf3ea732539192ad256af24_list;
static PyObject *const_list_2736fe6cc96aa566d56961d47c48dbfb_list;
static PyObject *const_list_620ff1df99240c181055b82c662a2267_list;
extern PyObject *const_str_plain___doc__;
static PyObject *const_list_3b0d8345b89b2bb8237230c2540ce284_list;
static PyObject *const_str_digest_609d5cd493e23c8a091cb4af34871bed;
static PyObject *const_list_364ebb0cc6fa35d2c7b949f79bb64808_list;
static PyObject *const_str_plain__seg_9;
static PyObject *const_str_plain__seg_59;
static PyObject *const_list_8bea44642d8d24e04538a8d6c5a416d1_list;
static PyObject *const_str_plain__seg_24;
static PyObject *const_str_plain__seg_75;
static PyObject *const_list_b8b3e54ff0597402c49a05b800bc40f8_list;
static PyObject *const_int_pos_918000;
static PyObject *const_list_500d74b2d4e0a84c11657191a8eafb94_list;
static PyObject *const_str_plain__seg_71;
static PyObject *const_str_plain__seg_43;
static PyObject *const_str_plain__seg_70;
static PyObject *const_str_plain__seg_4;
static PyObject *const_str_plain__seg_74;
static PyObject *const_str_plain__seg_19;
static PyObject *const_list_841383587ad1c6acad0160afef32ea70_list;
static PyObject *const_list_b3f22f503a8b8c93c689bf5fa1b2dc74_list;
static PyObject *const_str_plain__seg_37;
static PyObject *const_list_4e0b2849ead24970c2eb2f47aa203488_list;
static PyObject *const_list_2566a888585dd4c23d86f43c107fc50b_list;
static PyObject *const_list_bfcde6b01b4421752dbf73a52acff230_list;
static PyObject *const_str_plain__seg_20;
static PyObject *const_list_3d2356b7a28999101b4d311516202e09_list;
static PyObject *const_list_2dec150fcfacb481fbb383bd2c964a6c_list;
static PyObject *const_list_eade55862f70223a4e086f058a539edc_list;
static PyObject *const_list_c1e14abbb7f8f66399d221fde1406ebb_list;
static PyObject *const_str_plain__seg_62;
static PyObject *const_list_697c7ec3ea67cab55b4df07096d97e10_list;
static PyObject *const_list_2a12fbd0883ee636681b7e810af71859_list;
static PyObject *const_list_cec91d3bbe0581822efbc5cc69733a5c_list;
static PyObject *const_str_plain__seg_54;
static PyObject *const_str_plain__seg_26;
extern PyObject *const_tuple_empty;
static PyObject *const_list_b23b414f71daf645f78619fa6b76b377_list;
static PyObject *const_str_plain__seg_17;
static PyObject *const_list_7a830ab5723f631a55283b742be54ee5_list;
static PyObject *const_list_d89cca9c1788a355a1f92664b03ba3c4_list;
static PyObject *const_list_a60fd13e0dfc718a11f99c77fdc34d3f_list;
static PyObject *const_str_plain__seg_31;
static PyObject *const_str_plain__seg_28;
static PyObject *const_str_plain__seg_58;
static PyObject *const_list_0937f6e05bcda235a9ba8b3483786ae3_list;
static PyObject *const_str_plain__seg_51;
static PyObject *const_str_plain__seg_48;
static PyObject *const_str_digest_236a2076b696ba5c64a198901f584c91;
static PyObject *const_list_76153aeb9ffa3b3e10bcad00033ce49c_list;
static PyObject *const_str_plain__seg_56;
static PyObject *const_list_2c52bf29bc0001545dd958adc3c692e7_list;
static PyObject *const_str_digest_90aeeeb56b6ad24f622a22b3f5b7b9f0;
static PyObject *const_str_plain__seg_6;
static PyObject *const_list_b4bbbd39d4971392300501122d75a44a_list;
static PyObject *const_list_3c60d2641990376381daef7fde8c40f6_list;
static PyObject *const_list_7a2efee3689e35d10be61a40814096ee_list;
static PyObject *const_list_cdc5834c1d67355ffe8aacc68a7c8f1e_list;
static PyObject *const_str_plain__seg_45;
static PyObject *const_list_dec83c9440f2587b57c0a6a866f9fce3_list;
static PyObject *const_str_plain__seg_53;
static PyObject *const_list_3327521294d970637a7842147266821f_list;
extern PyObject *const_str_plain___version__;
extern PyObject *const_str_digest_ad7ee6951262f9b87a63ca77e4f7b77a;
static PyObject *const_str_plain__seg_32;
static PyObject *const_list_88e6fcc4a85a8b789bdb10cd34f13231_list;
static PyObject *const_list_507d3b119902f9a32efeae3d13ce7b94_list;
static PyObject *const_str_plain__seg_33;
static PyObject *const_list_4b51502d75f797e49ac01f8005ed5015_list;
static PyObject *const_str_plain__seg_46;
static PyObject *const_str_plain__seg_64;
static PyObject *const_str_plain__seg_3;
static PyObject *const_list_e918b4359dd64b05e94b631a739da7fb_list;
static PyObject *const_str_plain__seg_25;
static PyObject *const_list_80f77b61bc302da336f54cd3d02e66c2_list;
static PyObject *const_str_plain__seg_12;
static PyObject *const_str_plain__seg_40;
static PyObject *const_str_plain__seg_52;
static PyObject *const_list_9dd90874fdca627eb7884b611e674d40_list;
static PyObject *const_str_plain__seg_55;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_plain__seg_22;
static PyObject *const_str_plain__seg_66;
static PyObject *const_list_e038faf866b75a13cb44589226ba8b1e_list;
static PyObject *const_list_f2f1464539d48a834a15a86a1067601b_list;
static PyObject *const_str_plain__seg_78;
static PyObject *const_str_plain__seg_16;
static PyObject *const_str_plain__seg_8;
static PyObject *const_str_plain__seg_39;
static PyObject *const_list_c6a2f6389c2c8a309c095678e367c0c6_list;
static PyObject *const_str_plain__seg_65;
static PyObject *const_str_plain__seg_69;
static PyObject *const_str_plain__seg_34;
static PyObject *const_list_37181eaefb4415e1a84bc21a874d648c_list;
extern PyObject *const_str_plain___cached__;
static PyObject *const_list_1e8ddb769f99b58df84b3609f1e28a8a_list;
static PyObject *const_str_plain__seg_38;
static PyObject *const_list_2ec405aa5530099c2f60a957bb101ebe_list;
static PyObject *const_list_b3c793656eb3530d6aea0774ce0ccd67_list;
static PyObject *const_str_plain__seg_13;
static PyObject *const_list_7c0e16ff88fa2e97e00acb891606e3d1_list;
static PyObject *const_list_e5ae6436e9f1f1c0946f9356d444b9d8_list;
static PyObject *const_list_7ef4561a8f74e1f78856734054f82136_list;
static PyObject *const_str_plain__seg_29;
static PyObject *const_list_1168636a8439e4563e51a0fdffdec990_list;
static PyObject *const_list_tuple_int_pos_918000_str_plain_X_tuple_list;
static PyObject *const_str_plain__seg_0;
static PyObject *const_str_plain__seg_61;
static PyObject *const_str_plain__seg_41;
static PyObject *const_str_plain__seg_27;
static PyObject *const_str_plain__seg_30;
static PyObject *const_str_plain__seg_21;
extern PyObject *const_str_plain_X;
static PyObject *const_str_plain__seg_15;
static PyObject *const_str_plain__seg_1;
static PyObject *const_list_b274a869469641974eba1bf7476e2455_list;
static PyObject *const_list_02281edcc8812ba530b7f5e50cfd0b5b_list;
static PyObject *const_list_424867e854cb15c8496bad1642a6debc_list;
static PyObject *const_tuple_int_pos_918000_str_plain_X_tuple;
static PyObject *const_str_plain__seg_42;
static PyObject *const_str_plain__seg_18;
static PyObject *const_list_a18637cb06641239354016225b957dea_list;
static PyObject *const_list_4cc72e3d20ab29c610a1a1ca0fcfb627_list;
static PyObject *const_list_b0fab71a482dc0ec2efac423749ff2e7_list;
static PyObject *const_list_9729ba2a30ec942286e4e92e6b760cdb_list;
static PyObject *const_list_dce937c4840f7bb385611a0c40bbcc90_list;
static PyObject *const_list_b1ec42585634ef4d921253250ffa0755_list;
static PyObject *const_list_1d4baaef1060e348a3bc5bb3cbb59a9d_list;
extern PyObject *const_str_plain_has_location;
static PyObject *const_str_plain__seg_50;
static PyObject *const_str_plain__seg_67;
static PyObject *const_list_cc1b9b162a62ee2af72bcb5e058aaf0f_list;
static PyObject *const_list_8a84f5c25cdbc3d25e4d678ceece54b3_list;
static PyObject *const_list_3d84360e8b415e78d7ac72ccb1b0a5c0_list;
static PyObject *const_list_b847d9463d5f422ca6cbb349d8b346fd_list;
static PyObject *const_str_plain__seg_36;
static PyObject *const_str_plain__seg_23;
static PyObject *const_list_e041e871a674c9177e0fdbc54c1d1db0_list;
static PyObject *const_list_376db34ecd4d068e67d2c67c052515df_list;
static PyObject *const_list_f37369242f97e41323b9db993947450f_list;
static PyObject *const_str_plain__seg_63;
static PyObject *const_list_305d275268a3e362fa0a8c0eaac904aa_list;
static PyObject *const_str_plain__seg_47;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_plain__seg_44 = UNSTREAM_STRING_ASCII( &constant_bin[ 713132 ], 7, 1 );
    const_str_plain__seg_14 = UNSTREAM_STRING_ASCII( &constant_bin[ 713139 ], 7, 1 );
    const_str_plain__seg_11 = UNSTREAM_STRING_ASCII( &constant_bin[ 713146 ], 7, 1 );
    const_str_plain__seg_73 = UNSTREAM_STRING_ASCII( &constant_bin[ 713153 ], 7, 1 );
    const_str_plain__seg_77 = UNSTREAM_STRING_ASCII( &constant_bin[ 713160 ], 7, 1 );
    const_str_plain__seg_35 = UNSTREAM_STRING_ASCII( &constant_bin[ 713167 ], 7, 1 );
    const_list_05eaf4095605f0ad2dac780ae2672c70_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 713174 ], 1245 );
    const_str_plain__seg_49 = UNSTREAM_STRING_ASCII( &constant_bin[ 714419 ], 7, 1 );
    const_str_plain__seg_72 = UNSTREAM_STRING_ASCII( &constant_bin[ 714426 ], 7, 1 );
    const_list_4ed37e673347c8bf4b1af9d04a4d2d32_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 714433 ], 1549 );
    const_str_plain__seg_5 = UNSTREAM_STRING_ASCII( &constant_bin[ 715982 ], 6, 1 );
    const_list_834d28e04b8448e93bd1330606542306_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 715988 ], 1277 );
    const_list_cf5f62a7137ededb7d4ae16b4e4fce3c_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 717265 ], 1750 );
    const_str_plain__seg_2 = UNSTREAM_STRING_ASCII( &constant_bin[ 719015 ], 6, 1 );
    const_str_plain__seg_10 = UNSTREAM_STRING_ASCII( &constant_bin[ 719021 ], 7, 1 );
    const_str_plain__seg_57 = UNSTREAM_STRING_ASCII( &constant_bin[ 719028 ], 7, 1 );
    const_str_digest_dd792d04ec2314d7746fad73a142a1d6 = UNSTREAM_STRING_ASCII( &constant_bin[ 719035 ], 30, 0 );
    const_list_913ba5cfbf54f76f4ec142820765e5fb_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 719065 ], 2014 );
    const_list_c9a124af4be98e21a9e979763c20be3d_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 721079 ], 1487 );
    const_list_de909d1f27fedb734717ab0a1d4c2224_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 722566 ], 2003 );
    const_list_4ed64b4aae3567efb08519f6f5a49fc9_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 724569 ], 1600 );
    const_str_plain__seg_68 = UNSTREAM_STRING_ASCII( &constant_bin[ 726169 ], 7, 1 );
    const_str_plain__seg_60 = UNSTREAM_STRING_ASCII( &constant_bin[ 726176 ], 7, 1 );
    const_list_b723fc0ab5a71b0a5ca9d030a15109ab_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 726183 ], 1705 );
    const_list_39b054c05ecf5f1f8c6e8f9ae5ef0167_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 727888 ], 1607 );
    const_str_plain__seg_7 = UNSTREAM_STRING_ASCII( &constant_bin[ 713153 ], 6, 1 );
    const_str_plain__seg_76 = UNSTREAM_STRING_ASCII( &constant_bin[ 729495 ], 7, 1 );
    const_list_b4116cfcedf3ea732539192ad256af24_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 729502 ], 2288 );
    const_list_2736fe6cc96aa566d56961d47c48dbfb_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 731790 ], 2103 );
    const_list_620ff1df99240c181055b82c662a2267_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 733893 ], 1714 );
    const_list_3b0d8345b89b2bb8237230c2540ce284_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 735607 ], 2003 );
    const_str_digest_609d5cd493e23c8a091cb4af34871bed = UNSTREAM_STRING_ASCII( &constant_bin[ 737610 ], 17, 0 );
    const_list_364ebb0cc6fa35d2c7b949f79bb64808_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 737627 ], 1945 );
    const_str_plain__seg_9 = UNSTREAM_STRING_ASCII( &constant_bin[ 739572 ], 6, 1 );
    const_str_plain__seg_59 = UNSTREAM_STRING_ASCII( &constant_bin[ 739578 ], 7, 1 );
    const_list_8bea44642d8d24e04538a8d6c5a416d1_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 739585 ], 1500 );
    const_str_plain__seg_24 = UNSTREAM_STRING_ASCII( &constant_bin[ 741085 ], 7, 1 );
    const_str_plain__seg_75 = UNSTREAM_STRING_ASCII( &constant_bin[ 741092 ], 7, 1 );
    const_list_b8b3e54ff0597402c49a05b800bc40f8_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 741099 ], 1289 );
    const_int_pos_918000 = PyLong_FromUnsignedLong( 918000ul );
    const_list_500d74b2d4e0a84c11657191a8eafb94_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 742388 ], 1845 );
    const_str_plain__seg_71 = UNSTREAM_STRING_ASCII( &constant_bin[ 744233 ], 7, 1 );
    const_str_plain__seg_43 = UNSTREAM_STRING_ASCII( &constant_bin[ 744240 ], 7, 1 );
    const_str_plain__seg_70 = UNSTREAM_STRING_ASCII( &constant_bin[ 744247 ], 7, 1 );
    const_str_plain__seg_4 = UNSTREAM_STRING_ASCII( &constant_bin[ 713132 ], 6, 1 );
    const_str_plain__seg_74 = UNSTREAM_STRING_ASCII( &constant_bin[ 744254 ], 7, 1 );
    const_str_plain__seg_19 = UNSTREAM_STRING_ASCII( &constant_bin[ 744261 ], 7, 1 );
    const_list_841383587ad1c6acad0160afef32ea70_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 744268 ], 1865 );
    const_list_b3f22f503a8b8c93c689bf5fa1b2dc74_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 746133 ], 1681 );
    const_str_plain__seg_37 = UNSTREAM_STRING_ASCII( &constant_bin[ 747814 ], 7, 1 );
    const_list_4e0b2849ead24970c2eb2f47aa203488_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 747821 ], 1402 );
    const_list_2566a888585dd4c23d86f43c107fc50b_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 749223 ], 2127 );
    const_list_bfcde6b01b4421752dbf73a52acff230_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 751350 ], 1998 );
    const_str_plain__seg_20 = UNSTREAM_STRING_ASCII( &constant_bin[ 753348 ], 7, 1 );
    const_list_3d2356b7a28999101b4d311516202e09_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 753355 ], 1340 );
    const_list_2dec150fcfacb481fbb383bd2c964a6c_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 754695 ], 1599 );
    const_list_eade55862f70223a4e086f058a539edc_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 756294 ], 1994 );
    const_list_c1e14abbb7f8f66399d221fde1406ebb_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 758288 ], 1624 );
    const_str_plain__seg_62 = UNSTREAM_STRING_ASCII( &constant_bin[ 759912 ], 7, 1 );
    const_list_697c7ec3ea67cab55b4df07096d97e10_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 759919 ], 2284 );
    const_list_2a12fbd0883ee636681b7e810af71859_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 762203 ], 1811 );
    const_list_cec91d3bbe0581822efbc5cc69733a5c_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 764014 ], 1606 );
    const_str_plain__seg_54 = UNSTREAM_STRING_ASCII( &constant_bin[ 765620 ], 7, 1 );
    const_str_plain__seg_26 = UNSTREAM_STRING_ASCII( &constant_bin[ 765627 ], 7, 1 );
    const_list_b23b414f71daf645f78619fa6b76b377_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 765634 ], 1906 );
    const_str_plain__seg_17 = UNSTREAM_STRING_ASCII( &constant_bin[ 767540 ], 7, 1 );
    const_list_7a830ab5723f631a55283b742be54ee5_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 767547 ], 1605 );
    const_list_d89cca9c1788a355a1f92664b03ba3c4_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 769152 ], 1479 );
    const_list_a60fd13e0dfc718a11f99c77fdc34d3f_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 770631 ], 1755 );
    const_str_plain__seg_31 = UNSTREAM_STRING_ASCII( &constant_bin[ 772386 ], 7, 1 );
    const_str_plain__seg_28 = UNSTREAM_STRING_ASCII( &constant_bin[ 772393 ], 7, 1 );
    const_str_plain__seg_58 = UNSTREAM_STRING_ASCII( &constant_bin[ 772400 ], 7, 1 );
    const_list_0937f6e05bcda235a9ba8b3483786ae3_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 772407 ], 1232 );
    const_str_plain__seg_51 = UNSTREAM_STRING_ASCII( &constant_bin[ 773639 ], 7, 1 );
    const_str_plain__seg_48 = UNSTREAM_STRING_ASCII( &constant_bin[ 773646 ], 7, 1 );
    const_str_digest_236a2076b696ba5c64a198901f584c91 = UNSTREAM_STRING_ASCII( &constant_bin[ 773653 ], 14, 0 );
    const_list_76153aeb9ffa3b3e10bcad00033ce49c_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 773667 ], 1712 );
    const_str_plain__seg_56 = UNSTREAM_STRING_ASCII( &constant_bin[ 775379 ], 7, 1 );
    const_list_2c52bf29bc0001545dd958adc3c692e7_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 775386 ], 1602 );
    const_str_digest_90aeeeb56b6ad24f622a22b3f5b7b9f0 = UNSTREAM_STRING_ASCII( &constant_bin[ 776988 ], 23, 0 );
    const_str_plain__seg_6 = UNSTREAM_STRING_ASCII( &constant_bin[ 726169 ], 6, 1 );
    const_list_b4bbbd39d4971392300501122d75a44a_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 777011 ], 1975 );
    const_list_3c60d2641990376381daef7fde8c40f6_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 778986 ], 2319 );
    const_list_7a2efee3689e35d10be61a40814096ee_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 781305 ], 1605 );
    const_list_cdc5834c1d67355ffe8aacc68a7c8f1e_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 782910 ], 1688 );
    const_str_plain__seg_45 = UNSTREAM_STRING_ASCII( &constant_bin[ 784598 ], 7, 1 );
    const_list_dec83c9440f2587b57c0a6a866f9fce3_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 784605 ], 1795 );
    const_str_plain__seg_53 = UNSTREAM_STRING_ASCII( &constant_bin[ 786400 ], 7, 1 );
    const_list_3327521294d970637a7842147266821f_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 786407 ], 2003 );
    const_str_plain__seg_32 = UNSTREAM_STRING_ASCII( &constant_bin[ 788410 ], 7, 1 );
    const_list_88e6fcc4a85a8b789bdb10cd34f13231_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 788417 ], 1624 );
    const_list_507d3b119902f9a32efeae3d13ce7b94_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 790041 ], 1995 );
    const_str_plain__seg_33 = UNSTREAM_STRING_ASCII( &constant_bin[ 792036 ], 7, 1 );
    const_list_4b51502d75f797e49ac01f8005ed5015_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 792043 ], 1694 );
    const_str_plain__seg_46 = UNSTREAM_STRING_ASCII( &constant_bin[ 793737 ], 7, 1 );
    const_str_plain__seg_64 = UNSTREAM_STRING_ASCII( &constant_bin[ 793744 ], 7, 1 );
    const_str_plain__seg_3 = UNSTREAM_STRING_ASCII( &constant_bin[ 713167 ], 6, 1 );
    const_list_e918b4359dd64b05e94b631a739da7fb_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 793751 ], 1618 );
    const_str_plain__seg_25 = UNSTREAM_STRING_ASCII( &constant_bin[ 795369 ], 7, 1 );
    const_list_80f77b61bc302da336f54cd3d02e66c2_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 795376 ], 1631 );
    const_str_plain__seg_12 = UNSTREAM_STRING_ASCII( &constant_bin[ 797007 ], 7, 1 );
    const_str_plain__seg_40 = UNSTREAM_STRING_ASCII( &constant_bin[ 797014 ], 7, 1 );
    const_str_plain__seg_52 = UNSTREAM_STRING_ASCII( &constant_bin[ 797021 ], 7, 1 );
    const_list_9dd90874fdca627eb7884b611e674d40_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 797028 ], 1710 );
    const_str_plain__seg_55 = UNSTREAM_STRING_ASCII( &constant_bin[ 798738 ], 7, 1 );
    const_str_plain__seg_22 = UNSTREAM_STRING_ASCII( &constant_bin[ 798745 ], 7, 1 );
    const_str_plain__seg_66 = UNSTREAM_STRING_ASCII( &constant_bin[ 798752 ], 7, 1 );
    const_list_e038faf866b75a13cb44589226ba8b1e_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 798759 ], 1511 );
    const_list_f2f1464539d48a834a15a86a1067601b_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 800270 ], 1589 );
    const_str_plain__seg_78 = UNSTREAM_STRING_ASCII( &constant_bin[ 801859 ], 7, 1 );
    const_str_plain__seg_16 = UNSTREAM_STRING_ASCII( &constant_bin[ 801866 ], 7, 1 );
    const_str_plain__seg_8 = UNSTREAM_STRING_ASCII( &constant_bin[ 801873 ], 6, 1 );
    const_str_plain__seg_39 = UNSTREAM_STRING_ASCII( &constant_bin[ 801879 ], 7, 1 );
    const_list_c6a2f6389c2c8a309c095678e367c0c6_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 801886 ], 1749 );
    const_str_plain__seg_65 = UNSTREAM_STRING_ASCII( &constant_bin[ 803635 ], 7, 1 );
    const_str_plain__seg_69 = UNSTREAM_STRING_ASCII( &constant_bin[ 803642 ], 7, 1 );
    const_str_plain__seg_34 = UNSTREAM_STRING_ASCII( &constant_bin[ 803649 ], 7, 1 );
    const_list_37181eaefb4415e1a84bc21a874d648c_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 803656 ], 1528 );
    const_list_1e8ddb769f99b58df84b3609f1e28a8a_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 805184 ], 1678 );
    const_str_plain__seg_38 = UNSTREAM_STRING_ASCII( &constant_bin[ 806862 ], 7, 1 );
    const_list_2ec405aa5530099c2f60a957bb101ebe_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 806869 ], 1411 );
    const_list_b3c793656eb3530d6aea0774ce0ccd67_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 808280 ], 1614 );
    const_str_plain__seg_13 = UNSTREAM_STRING_ASCII( &constant_bin[ 809894 ], 7, 1 );
    const_list_7c0e16ff88fa2e97e00acb891606e3d1_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 809901 ], 1906 );
    const_list_e5ae6436e9f1f1c0946f9356d444b9d8_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 811807 ], 1568 );
    const_list_7ef4561a8f74e1f78856734054f82136_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 813375 ], 2018 );
    const_str_plain__seg_29 = UNSTREAM_STRING_ASCII( &constant_bin[ 815393 ], 7, 1 );
    const_list_1168636a8439e4563e51a0fdffdec990_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 815400 ], 1651 );
    const_list_tuple_int_pos_918000_str_plain_X_tuple_list = PyList_New( 1 );
    const_tuple_int_pos_918000_str_plain_X_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_int_pos_918000_str_plain_X_tuple, 0, const_int_pos_918000 ); Py_INCREF( const_int_pos_918000 );
    PyTuple_SET_ITEM( const_tuple_int_pos_918000_str_plain_X_tuple, 1, const_str_plain_X ); Py_INCREF( const_str_plain_X );
    PyList_SET_ITEM( const_list_tuple_int_pos_918000_str_plain_X_tuple_list, 0, const_tuple_int_pos_918000_str_plain_X_tuple ); Py_INCREF( const_tuple_int_pos_918000_str_plain_X_tuple );
    const_str_plain__seg_0 = UNSTREAM_STRING_ASCII( &constant_bin[ 817051 ], 6, 1 );
    const_str_plain__seg_61 = UNSTREAM_STRING_ASCII( &constant_bin[ 817057 ], 7, 1 );
    const_str_plain__seg_41 = UNSTREAM_STRING_ASCII( &constant_bin[ 817064 ], 7, 1 );
    const_str_plain__seg_27 = UNSTREAM_STRING_ASCII( &constant_bin[ 817071 ], 7, 1 );
    const_str_plain__seg_30 = UNSTREAM_STRING_ASCII( &constant_bin[ 817078 ], 7, 1 );
    const_str_plain__seg_21 = UNSTREAM_STRING_ASCII( &constant_bin[ 817085 ], 7, 1 );
    const_str_plain__seg_15 = UNSTREAM_STRING_ASCII( &constant_bin[ 817092 ], 7, 1 );
    const_str_plain__seg_1 = UNSTREAM_STRING_ASCII( &constant_bin[ 713139 ], 6, 1 );
    const_list_b274a869469641974eba1bf7476e2455_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 817099 ], 1919 );
    const_list_02281edcc8812ba530b7f5e50cfd0b5b_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 819018 ], 1586 );
    const_list_424867e854cb15c8496bad1642a6debc_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 820604 ], 1651 );
    const_str_plain__seg_42 = UNSTREAM_STRING_ASCII( &constant_bin[ 822255 ], 7, 1 );
    const_str_plain__seg_18 = UNSTREAM_STRING_ASCII( &constant_bin[ 822262 ], 7, 1 );
    const_list_a18637cb06641239354016225b957dea_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 822269 ], 1602 );
    const_list_4cc72e3d20ab29c610a1a1ca0fcfb627_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 823871 ], 1947 );
    const_list_b0fab71a482dc0ec2efac423749ff2e7_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 825818 ], 1628 );
    const_list_9729ba2a30ec942286e4e92e6b760cdb_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 827446 ], 1929 );
    const_list_dce937c4840f7bb385611a0c40bbcc90_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 829375 ], 1647 );
    const_list_b1ec42585634ef4d921253250ffa0755_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 831022 ], 1630 );
    const_list_1d4baaef1060e348a3bc5bb3cbb59a9d_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 832652 ], 1883 );
    const_str_plain__seg_50 = UNSTREAM_STRING_ASCII( &constant_bin[ 834535 ], 7, 1 );
    const_str_plain__seg_67 = UNSTREAM_STRING_ASCII( &constant_bin[ 834542 ], 7, 1 );
    const_list_cc1b9b162a62ee2af72bcb5e058aaf0f_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 834549 ], 1624 );
    const_list_8a84f5c25cdbc3d25e4d678ceece54b3_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 836173 ], 2018 );
    const_list_3d84360e8b415e78d7ac72ccb1b0a5c0_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 838191 ], 1907 );
    const_list_b847d9463d5f422ca6cbb349d8b346fd_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 840098 ], 2021 );
    const_str_plain__seg_36 = UNSTREAM_STRING_ASCII( &constant_bin[ 842119 ], 7, 1 );
    const_str_plain__seg_23 = UNSTREAM_STRING_ASCII( &constant_bin[ 842126 ], 7, 1 );
    const_list_e041e871a674c9177e0fdbc54c1d1db0_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 842133 ], 2001 );
    const_list_376db34ecd4d068e67d2c67c052515df_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 844134 ], 2107 );
    const_list_f37369242f97e41323b9db993947450f_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 846241 ], 1904 );
    const_str_plain__seg_63 = UNSTREAM_STRING_ASCII( &constant_bin[ 848145 ], 7, 1 );
    const_list_305d275268a3e362fa0a8c0eaac904aa_list = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 848152 ], 1210 );
    const_str_plain__seg_47 = UNSTREAM_STRING_ASCII( &constant_bin[ 849362 ], 7, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_idna$uts46data( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_94d7e88b3efd3989bb5abf95c99f44bc;
static PyCodeObject *codeobj_f05138f301c5e1cbae49ccc8b94ff3f4;
static PyCodeObject *codeobj_f0ec25fcb1c0c5e94e2bc5e07133dab3;
static PyCodeObject *codeobj_7936120e99cf475c370e71648f45b0a7;
static PyCodeObject *codeobj_c953168c5ea4e8152ec1078ae0a27f08;
static PyCodeObject *codeobj_e98ac2b55af5f8235a7dc68bb7fae209;
static PyCodeObject *codeobj_1173a60c1c1c1350466b2da579175a57;
static PyCodeObject *codeobj_00d17529705f2e82fc9b43db0458e03f;
static PyCodeObject *codeobj_961a97093f83203a22fed03da98ebb77;
static PyCodeObject *codeobj_9ed1d4fc9ba0d7da1049f0c12750680c;
static PyCodeObject *codeobj_337cd7e711a6663472f5687f9e058c0b;
static PyCodeObject *codeobj_4bc2ccc38c50eb0579549c0866aa6ea9;
static PyCodeObject *codeobj_bc63ca518e0757c99e81ce88d40866a7;
static PyCodeObject *codeobj_2af09a1eb192107c1e533bd50284e121;
static PyCodeObject *codeobj_92d83e1ee6dbe85538fdf7d35a2f03dc;
static PyCodeObject *codeobj_155c7c3448a6c5ecd68b3382cc8ff4fb;
static PyCodeObject *codeobj_fa043977a3fb83d1f60e51e44d6ec572;
static PyCodeObject *codeobj_6d8fc104aeee5a5800b192f366ce15f9;
static PyCodeObject *codeobj_73753216b2497c4f559605fa29991059;
static PyCodeObject *codeobj_6ef43de128a647618791048f1ea4c03f;
static PyCodeObject *codeobj_6beb788f1632454b704b9197fa4199d2;
static PyCodeObject *codeobj_cd9249167320a02f41d2c3345ad56096;
static PyCodeObject *codeobj_250b86254c302586860d409ae182e3c5;
static PyCodeObject *codeobj_e23e463d563c676139697935a1a80932;
static PyCodeObject *codeobj_dc3f19deb1061d194d9552a67b85c02c;
static PyCodeObject *codeobj_e1b9902b034e3ebda29429d8c83f8607;
static PyCodeObject *codeobj_f1507cbe8bb86c8af95e25be0c080b29;
static PyCodeObject *codeobj_7ca9c3fc5d90e1feccec2222149b33f0;
static PyCodeObject *codeobj_f9256c4b9a0964cd34f7577e3324e0f9;
static PyCodeObject *codeobj_ac24b1920eee8fa6295d867935179476;
static PyCodeObject *codeobj_e55b5488384b89187cb2e78948f9ed7b;
static PyCodeObject *codeobj_ac294e8b444760f049eaf9390d179a60;
static PyCodeObject *codeobj_276756a628d9916c9fcb703ad7b9efa2;
static PyCodeObject *codeobj_b5e6dcdaaa10345b83f37c66e1bb8029;
static PyCodeObject *codeobj_bae7ccbb373f7829ca5540829bd05fb6;
static PyCodeObject *codeobj_d6864a0fbf3ac1b484407477af49555d;
static PyCodeObject *codeobj_93bc895e8fbf0c86fcf3574089f1e6bb;
static PyCodeObject *codeobj_e47afb089ce671345d720a7ba7c03ea9;
static PyCodeObject *codeobj_306de7336b8f117df34477678b0fcc3e;
static PyCodeObject *codeobj_9c3ab0a715c0236fcf40cad34dac3f44;
static PyCodeObject *codeobj_0bd5872adb4144aa3119946f31e4b8de;
static PyCodeObject *codeobj_a90f97c57d371861ad15930750624791;
static PyCodeObject *codeobj_6bd2336abb8f1d3fda6b81eb544b71ed;
static PyCodeObject *codeobj_5202007547e11436e13e0b8c468e8b21;
static PyCodeObject *codeobj_fa079ab7c1b5a43dd83777db6d5e1716;
static PyCodeObject *codeobj_669b0e7f202378d49b31d1d9e2a22c33;
static PyCodeObject *codeobj_5afdf61ece171774320ec792667b3650;
static PyCodeObject *codeobj_62a4843e10452bef004e8bf54cc73022;
static PyCodeObject *codeobj_a3f570306278a46057db0d4d71834e37;
static PyCodeObject *codeobj_7a1cc1394f347b8b403173ce673bcd51;
static PyCodeObject *codeobj_672a3b68909bf701c262b889c129a0c3;
static PyCodeObject *codeobj_eb168ab0bd16341de75f158f8fbb6b03;
static PyCodeObject *codeobj_b78114e050df667e706712486f8726cc;
static PyCodeObject *codeobj_7c30347e8809be8c2e9d57f11b47c271;
static PyCodeObject *codeobj_6cb6ecd9915a45d38606a89ea70cffbf;
static PyCodeObject *codeobj_b66113f34f015345992f6d285852838b;
static PyCodeObject *codeobj_adfb27a45faf8b9da43b1caa3bc79b21;
static PyCodeObject *codeobj_4c7e193c0173774f0df300b89ee3041a;
static PyCodeObject *codeobj_e0080e54f96f570b4161a8234f5e30e4;
static PyCodeObject *codeobj_97216d86558448da6a422f5ec5ddda37;
static PyCodeObject *codeobj_b69622bcf94469d9087eb9cf657fde50;
static PyCodeObject *codeobj_baf151f1c824fdb9121c91b9f5b9d42d;
static PyCodeObject *codeobj_1a87871ac85a87ca0b713a429ac466b7;
static PyCodeObject *codeobj_3779b20bf62f93955f19a01f1f02bc3c;
static PyCodeObject *codeobj_92128ef943e5b225211334efd4374d9d;
static PyCodeObject *codeobj_a24e092aa40f97eb9453bca23883ab67;
static PyCodeObject *codeobj_0f4307b63f4bffd677deffd80acdda73;
static PyCodeObject *codeobj_532e3b5972d928da4cbacc9b0a839e99;
static PyCodeObject *codeobj_73601843dcad5bd33cb1730a7eeea121;
static PyCodeObject *codeobj_2ff74c29095335cae6182c5cb3112ea1;
static PyCodeObject *codeobj_f36396bf69fc1cdb77c078e61b476ac2;
static PyCodeObject *codeobj_c28bfe29093246e69eb238a8ffe684b0;
static PyCodeObject *codeobj_3f94c0b2ee14fdb07aed9f2b4e5d169b;
static PyCodeObject *codeobj_96537a3567e3c9b6e5524a4149ff607c;
static PyCodeObject *codeobj_e60117d1b39cd0a61c4d72789562af3e;
static PyCodeObject *codeobj_f93adcd37c54c2900833b58780c4b419;
static PyCodeObject *codeobj_503331b9d50b3252cb1ade63dea607aa;
static PyCodeObject *codeobj_c828630c384b397c213155991373d206;
static PyCodeObject *codeobj_e8f3445cb6f8f9d5388c5f4add13e42e;
static PyCodeObject *codeobj_04a0f9d63b0c757c42219f422cc82e4c;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_609d5cd493e23c8a091cb4af34871bed );
    codeobj_94d7e88b3efd3989bb5abf95c99f44bc = MAKE_CODEOBJ( module_filename_obj, const_str_digest_90aeeeb56b6ad24f622a22b3f5b7b9f0, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_f05138f301c5e1cbae49ccc8b94ff3f4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_0, 8, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f0ec25fcb1c0c5e94e2bc5e07133dab3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_1, 112, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7936120e99cf475c370e71648f45b0a7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_10, 1048, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c953168c5ea4e8152ec1078ae0a27f08 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_11, 1152, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e98ac2b55af5f8235a7dc68bb7fae209 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_12, 1256, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1173a60c1c1c1350466b2da579175a57 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_13, 1360, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_00d17529705f2e82fc9b43db0458e03f = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_14, 1464, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_961a97093f83203a22fed03da98ebb77 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_15, 1568, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9ed1d4fc9ba0d7da1049f0c12750680c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_16, 1672, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_337cd7e711a6663472f5687f9e058c0b = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_17, 1776, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4bc2ccc38c50eb0579549c0866aa6ea9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_18, 1880, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bc63ca518e0757c99e81ce88d40866a7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_19, 1984, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2af09a1eb192107c1e533bd50284e121 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_2, 216, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_92d83e1ee6dbe85538fdf7d35a2f03dc = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_20, 2088, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_155c7c3448a6c5ecd68b3382cc8ff4fb = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_21, 2192, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fa043977a3fb83d1f60e51e44d6ec572 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_22, 2296, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6d8fc104aeee5a5800b192f366ce15f9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_23, 2400, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_73753216b2497c4f559605fa29991059 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_24, 2504, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6ef43de128a647618791048f1ea4c03f = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_25, 2608, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6beb788f1632454b704b9197fa4199d2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_26, 2712, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cd9249167320a02f41d2c3345ad56096 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_27, 2816, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_250b86254c302586860d409ae182e3c5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_28, 2920, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e23e463d563c676139697935a1a80932 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_29, 3024, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_dc3f19deb1061d194d9552a67b85c02c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_3, 320, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e1b9902b034e3ebda29429d8c83f8607 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_30, 3128, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f1507cbe8bb86c8af95e25be0c080b29 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_31, 3232, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7ca9c3fc5d90e1feccec2222149b33f0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_32, 3336, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f9256c4b9a0964cd34f7577e3324e0f9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_33, 3440, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ac24b1920eee8fa6295d867935179476 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_34, 3544, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e55b5488384b89187cb2e78948f9ed7b = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_35, 3648, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ac294e8b444760f049eaf9390d179a60 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_36, 3752, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_276756a628d9916c9fcb703ad7b9efa2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_37, 3856, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b5e6dcdaaa10345b83f37c66e1bb8029 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_38, 3960, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bae7ccbb373f7829ca5540829bd05fb6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_39, 4064, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d6864a0fbf3ac1b484407477af49555d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_4, 424, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_93bc895e8fbf0c86fcf3574089f1e6bb = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_40, 4168, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e47afb089ce671345d720a7ba7c03ea9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_41, 4272, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_306de7336b8f117df34477678b0fcc3e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_42, 4376, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9c3ab0a715c0236fcf40cad34dac3f44 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_43, 4480, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0bd5872adb4144aa3119946f31e4b8de = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_44, 4584, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a90f97c57d371861ad15930750624791 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_45, 4688, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6bd2336abb8f1d3fda6b81eb544b71ed = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_46, 4792, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5202007547e11436e13e0b8c468e8b21 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_47, 4896, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fa079ab7c1b5a43dd83777db6d5e1716 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_48, 5000, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_669b0e7f202378d49b31d1d9e2a22c33 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_49, 5104, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_5afdf61ece171774320ec792667b3650 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_5, 528, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_62a4843e10452bef004e8bf54cc73022 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_50, 5208, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a3f570306278a46057db0d4d71834e37 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_51, 5312, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7a1cc1394f347b8b403173ce673bcd51 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_52, 5416, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_672a3b68909bf701c262b889c129a0c3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_53, 5520, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_eb168ab0bd16341de75f158f8fbb6b03 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_54, 5624, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b78114e050df667e706712486f8726cc = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_55, 5728, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_7c30347e8809be8c2e9d57f11b47c271 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_56, 5832, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6cb6ecd9915a45d38606a89ea70cffbf = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_57, 5936, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b66113f34f015345992f6d285852838b = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_58, 6040, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_adfb27a45faf8b9da43b1caa3bc79b21 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_59, 6144, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4c7e193c0173774f0df300b89ee3041a = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_6, 632, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e0080e54f96f570b4161a8234f5e30e4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_60, 6248, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_97216d86558448da6a422f5ec5ddda37 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_61, 6352, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b69622bcf94469d9087eb9cf657fde50 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_62, 6456, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_baf151f1c824fdb9121c91b9f5b9d42d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_63, 6560, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1a87871ac85a87ca0b713a429ac466b7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_64, 6664, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3779b20bf62f93955f19a01f1f02bc3c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_65, 6768, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_92128ef943e5b225211334efd4374d9d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_66, 6872, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a24e092aa40f97eb9453bca23883ab67 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_67, 6976, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0f4307b63f4bffd677deffd80acdda73 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_68, 7080, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_532e3b5972d928da4cbacc9b0a839e99 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_69, 7184, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_73601843dcad5bd33cb1730a7eeea121 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_7, 736, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2ff74c29095335cae6182c5cb3112ea1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_70, 7288, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f36396bf69fc1cdb77c078e61b476ac2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_71, 7392, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c28bfe29093246e69eb238a8ffe684b0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_72, 7496, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3f94c0b2ee14fdb07aed9f2b4e5d169b = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_73, 7600, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_96537a3567e3c9b6e5524a4149ff607c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_74, 7704, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e60117d1b39cd0a61c4d72789562af3e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_75, 7808, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f93adcd37c54c2900833b58780c4b419 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_76, 7912, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_503331b9d50b3252cb1ade63dea607aa = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_77, 8016, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c828630c384b397c213155991373d206 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_78, 8120, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e8f3445cb6f8f9d5388c5f4add13e42e = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_8, 840, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_04a0f9d63b0c757c42219f422cc82e4c = MAKE_CODEOBJ( module_filename_obj, const_str_plain__seg_9, 944, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_10__seg_9(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_11__seg_10(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_12__seg_11(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_13__seg_12(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_14__seg_13(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_15__seg_14(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_16__seg_15(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_17__seg_16(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_18__seg_17(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_19__seg_18(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_1__seg_0(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_20__seg_19(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_21__seg_20(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_22__seg_21(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_23__seg_22(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_24__seg_23(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_25__seg_24(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_26__seg_25(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_27__seg_26(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_28__seg_27(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_29__seg_28(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_2__seg_1(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_30__seg_29(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_31__seg_30(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_32__seg_31(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_33__seg_32(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_34__seg_33(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_35__seg_34(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_36__seg_35(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_37__seg_36(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_38__seg_37(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_39__seg_38(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_3__seg_2(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_40__seg_39(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_41__seg_40(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_42__seg_41(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_43__seg_42(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_44__seg_43(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_45__seg_44(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_46__seg_45(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_47__seg_46(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_48__seg_47(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_49__seg_48(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_4__seg_3(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_50__seg_49(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_51__seg_50(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_52__seg_51(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_53__seg_52(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_54__seg_53(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_55__seg_54(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_56__seg_55(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_57__seg_56(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_58__seg_57(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_59__seg_58(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_5__seg_4(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_60__seg_59(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_61__seg_60(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_62__seg_61(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_63__seg_62(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_64__seg_63(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_65__seg_64(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_66__seg_65(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_67__seg_66(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_68__seg_67(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_69__seg_68(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_6__seg_5(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_70__seg_69(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_71__seg_70(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_72__seg_71(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_73__seg_72(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_74__seg_73(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_75__seg_74(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_76__seg_75(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_77__seg_76(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_78__seg_77(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_79__seg_78(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_7__seg_6(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_8__seg_7(  );


static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_9__seg_8(  );


// The module function definitions.
static PyObject *impl_idna$uts46data$$$function_1__seg_0( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_834d28e04b8448e93bd1330606542306_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_1__seg_0 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_2__seg_1( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_3d2356b7a28999101b4d311516202e09_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_2__seg_1 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_3__seg_2( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_e038faf866b75a13cb44589226ba8b1e_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_3__seg_2 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_4__seg_3( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_f2f1464539d48a834a15a86a1067601b_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_4__seg_3 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_5__seg_4( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_2dec150fcfacb481fbb383bd2c964a6c_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_5__seg_4 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_6__seg_5( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_a18637cb06641239354016225b957dea_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_6__seg_5 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_7__seg_6( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_620ff1df99240c181055b82c662a2267_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_7__seg_6 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_8__seg_7( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b723fc0ab5a71b0a5ca9d030a15109ab_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_8__seg_7 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_9__seg_8( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_4ed37e673347c8bf4b1af9d04a4d2d32_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_9__seg_8 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_10__seg_9( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b0fab71a482dc0ec2efac423749ff2e7_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_10__seg_9 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_11__seg_10( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_2ec405aa5530099c2f60a957bb101ebe_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_11__seg_10 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_12__seg_11( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_0937f6e05bcda235a9ba8b3483786ae3_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_12__seg_11 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_13__seg_12( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_305d275268a3e362fa0a8c0eaac904aa_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_13__seg_12 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_14__seg_13( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_d89cca9c1788a355a1f92664b03ba3c4_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_14__seg_13 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_15__seg_14( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_05eaf4095605f0ad2dac780ae2672c70_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_15__seg_14 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_16__seg_15( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_cec91d3bbe0581822efbc5cc69733a5c_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_16__seg_15 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_17__seg_16( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b3f22f503a8b8c93c689bf5fa1b2dc74_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_17__seg_16 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_18__seg_17( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_7a830ab5723f631a55283b742be54ee5_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_18__seg_17 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_19__seg_18( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_80f77b61bc302da336f54cd3d02e66c2_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_19__seg_18 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_20__seg_19( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_f37369242f97e41323b9db993947450f_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_20__seg_19 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_21__seg_20( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_500d74b2d4e0a84c11657191a8eafb94_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_21__seg_20 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_22__seg_21( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_37181eaefb4415e1a84bc21a874d648c_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_22__seg_21 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_23__seg_22( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_4b51502d75f797e49ac01f8005ed5015_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_23__seg_22 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_24__seg_23( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b1ec42585634ef4d921253250ffa0755_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_24__seg_23 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_25__seg_24( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_cf5f62a7137ededb7d4ae16b4e4fce3c_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_25__seg_24 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_26__seg_25( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_7a2efee3689e35d10be61a40814096ee_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_26__seg_25 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_27__seg_26( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_dce937c4840f7bb385611a0c40bbcc90_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_27__seg_26 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_28__seg_27( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_de909d1f27fedb734717ab0a1d4c2224_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_28__seg_27 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_29__seg_28( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_1d4baaef1060e348a3bc5bb3cbb59a9d_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_29__seg_28 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_30__seg_29( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b4bbbd39d4971392300501122d75a44a_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_30__seg_29 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_31__seg_30( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_7ef4561a8f74e1f78856734054f82136_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_31__seg_30 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_32__seg_31( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_4cc72e3d20ab29c610a1a1ca0fcfb627_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_32__seg_31 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_33__seg_32( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_3c60d2641990376381daef7fde8c40f6_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_33__seg_32 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_34__seg_33( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b4116cfcedf3ea732539192ad256af24_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_34__seg_33 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_35__seg_34( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_dec83c9440f2587b57c0a6a866f9fce3_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_35__seg_34 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_36__seg_35( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_cc1b9b162a62ee2af72bcb5e058aaf0f_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_36__seg_35 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_37__seg_36( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_4ed64b4aae3567efb08519f6f5a49fc9_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_37__seg_36 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_38__seg_37( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_8bea44642d8d24e04538a8d6c5a416d1_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_38__seg_37 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_39__seg_38( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b274a869469641974eba1bf7476e2455_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_39__seg_38 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_40__seg_39( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_3327521294d970637a7842147266821f_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_40__seg_39 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_41__seg_40( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_3b0d8345b89b2bb8237230c2540ce284_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_41__seg_40 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_42__seg_41( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_364ebb0cc6fa35d2c7b949f79bb64808_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_42__seg_41 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_43__seg_42( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_eade55862f70223a4e086f058a539edc_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_43__seg_42 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_44__seg_43( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_9729ba2a30ec942286e4e92e6b760cdb_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_44__seg_43 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_45__seg_44( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_e041e871a674c9177e0fdbc54c1d1db0_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_45__seg_44 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_46__seg_45( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_376db34ecd4d068e67d2c67c052515df_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_46__seg_45 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_47__seg_46( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_2736fe6cc96aa566d56961d47c48dbfb_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_47__seg_46 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_48__seg_47( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_2566a888585dd4c23d86f43c107fc50b_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_48__seg_47 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_49__seg_48( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_697c7ec3ea67cab55b4df07096d97e10_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_49__seg_48 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_50__seg_49( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_a60fd13e0dfc718a11f99c77fdc34d3f_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_50__seg_49 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_51__seg_50( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_2c52bf29bc0001545dd958adc3c692e7_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_51__seg_50 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_52__seg_51( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_7c0e16ff88fa2e97e00acb891606e3d1_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_52__seg_51 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_53__seg_52( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_02281edcc8812ba530b7f5e50cfd0b5b_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_53__seg_52 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_54__seg_53( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_841383587ad1c6acad0160afef32ea70_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_54__seg_53 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_55__seg_54( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_e5ae6436e9f1f1c0946f9356d444b9d8_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_55__seg_54 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_56__seg_55( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b8b3e54ff0597402c49a05b800bc40f8_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_56__seg_55 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_57__seg_56( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_c9a124af4be98e21a9e979763c20be3d_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_57__seg_56 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_58__seg_57( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_4e0b2849ead24970c2eb2f47aa203488_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_58__seg_57 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_59__seg_58( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_39b054c05ecf5f1f8c6e8f9ae5ef0167_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_59__seg_58 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_60__seg_59( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b3c793656eb3530d6aea0774ce0ccd67_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_60__seg_59 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_61__seg_60( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_88e6fcc4a85a8b789bdb10cd34f13231_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_61__seg_60 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_62__seg_61( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_c1e14abbb7f8f66399d221fde1406ebb_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_62__seg_61 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_63__seg_62( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_1168636a8439e4563e51a0fdffdec990_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_63__seg_62 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_64__seg_63( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_424867e854cb15c8496bad1642a6debc_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_64__seg_63 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_65__seg_64( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_cdc5834c1d67355ffe8aacc68a7c8f1e_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_65__seg_64 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_66__seg_65( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_3d84360e8b415e78d7ac72ccb1b0a5c0_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_66__seg_65 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_67__seg_66( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b23b414f71daf645f78619fa6b76b377_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_67__seg_66 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_68__seg_67( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_2a12fbd0883ee636681b7e810af71859_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_68__seg_67 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_69__seg_68( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_9dd90874fdca627eb7884b611e674d40_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_69__seg_68 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_70__seg_69( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_76153aeb9ffa3b3e10bcad00033ce49c_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_70__seg_69 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_71__seg_70( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_1e8ddb769f99b58df84b3609f1e28a8a_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_71__seg_70 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_72__seg_71( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_c6a2f6389c2c8a309c095678e367c0c6_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_72__seg_71 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_73__seg_72( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_e918b4359dd64b05e94b631a739da7fb_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_73__seg_72 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_74__seg_73( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_507d3b119902f9a32efeae3d13ce7b94_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_74__seg_73 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_75__seg_74( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_913ba5cfbf54f76f4ec142820765e5fb_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_75__seg_74 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_76__seg_75( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_b847d9463d5f422ca6cbb349d8b346fd_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_76__seg_75 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_77__seg_76( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_8a84f5c25cdbc3d25e4d678ceece54b3_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_77__seg_76 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_78__seg_77( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_bfcde6b01b4421752dbf73a52acff230_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_78__seg_77 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_idna$uts46data$$$function_79__seg_78( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = LIST_COPY( const_list_tuple_int_pos_918000_str_plain_X_tuple_list );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( idna$uts46data$$$function_79__seg_78 );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_10__seg_9(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_10__seg_9,
        const_str_plain__seg_9,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_04a0f9d63b0c757c42219f422cc82e4c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_11__seg_10(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_11__seg_10,
        const_str_plain__seg_10,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7936120e99cf475c370e71648f45b0a7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_12__seg_11(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_12__seg_11,
        const_str_plain__seg_11,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c953168c5ea4e8152ec1078ae0a27f08,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_13__seg_12(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_13__seg_12,
        const_str_plain__seg_12,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e98ac2b55af5f8235a7dc68bb7fae209,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_14__seg_13(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_14__seg_13,
        const_str_plain__seg_13,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1173a60c1c1c1350466b2da579175a57,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_15__seg_14(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_15__seg_14,
        const_str_plain__seg_14,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_00d17529705f2e82fc9b43db0458e03f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_16__seg_15(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_16__seg_15,
        const_str_plain__seg_15,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_961a97093f83203a22fed03da98ebb77,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_17__seg_16(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_17__seg_16,
        const_str_plain__seg_16,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9ed1d4fc9ba0d7da1049f0c12750680c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_18__seg_17(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_18__seg_17,
        const_str_plain__seg_17,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_337cd7e711a6663472f5687f9e058c0b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_19__seg_18(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_19__seg_18,
        const_str_plain__seg_18,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4bc2ccc38c50eb0579549c0866aa6ea9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_1__seg_0(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_1__seg_0,
        const_str_plain__seg_0,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f05138f301c5e1cbae49ccc8b94ff3f4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_20__seg_19(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_20__seg_19,
        const_str_plain__seg_19,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bc63ca518e0757c99e81ce88d40866a7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_21__seg_20(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_21__seg_20,
        const_str_plain__seg_20,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_92d83e1ee6dbe85538fdf7d35a2f03dc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_22__seg_21(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_22__seg_21,
        const_str_plain__seg_21,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_155c7c3448a6c5ecd68b3382cc8ff4fb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_23__seg_22(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_23__seg_22,
        const_str_plain__seg_22,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_fa043977a3fb83d1f60e51e44d6ec572,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_24__seg_23(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_24__seg_23,
        const_str_plain__seg_23,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6d8fc104aeee5a5800b192f366ce15f9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_25__seg_24(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_25__seg_24,
        const_str_plain__seg_24,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_73753216b2497c4f559605fa29991059,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_26__seg_25(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_26__seg_25,
        const_str_plain__seg_25,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6ef43de128a647618791048f1ea4c03f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_27__seg_26(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_27__seg_26,
        const_str_plain__seg_26,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6beb788f1632454b704b9197fa4199d2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_28__seg_27(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_28__seg_27,
        const_str_plain__seg_27,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cd9249167320a02f41d2c3345ad56096,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_29__seg_28(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_29__seg_28,
        const_str_plain__seg_28,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_250b86254c302586860d409ae182e3c5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_2__seg_1(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_2__seg_1,
        const_str_plain__seg_1,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f0ec25fcb1c0c5e94e2bc5e07133dab3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_30__seg_29(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_30__seg_29,
        const_str_plain__seg_29,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e23e463d563c676139697935a1a80932,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_31__seg_30(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_31__seg_30,
        const_str_plain__seg_30,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e1b9902b034e3ebda29429d8c83f8607,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_32__seg_31(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_32__seg_31,
        const_str_plain__seg_31,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f1507cbe8bb86c8af95e25be0c080b29,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_33__seg_32(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_33__seg_32,
        const_str_plain__seg_32,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7ca9c3fc5d90e1feccec2222149b33f0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_34__seg_33(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_34__seg_33,
        const_str_plain__seg_33,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f9256c4b9a0964cd34f7577e3324e0f9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_35__seg_34(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_35__seg_34,
        const_str_plain__seg_34,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ac24b1920eee8fa6295d867935179476,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_36__seg_35(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_36__seg_35,
        const_str_plain__seg_35,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e55b5488384b89187cb2e78948f9ed7b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_37__seg_36(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_37__seg_36,
        const_str_plain__seg_36,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_ac294e8b444760f049eaf9390d179a60,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_38__seg_37(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_38__seg_37,
        const_str_plain__seg_37,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_276756a628d9916c9fcb703ad7b9efa2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_39__seg_38(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_39__seg_38,
        const_str_plain__seg_38,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b5e6dcdaaa10345b83f37c66e1bb8029,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_3__seg_2(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_3__seg_2,
        const_str_plain__seg_2,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2af09a1eb192107c1e533bd50284e121,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_40__seg_39(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_40__seg_39,
        const_str_plain__seg_39,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_bae7ccbb373f7829ca5540829bd05fb6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_41__seg_40(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_41__seg_40,
        const_str_plain__seg_40,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_93bc895e8fbf0c86fcf3574089f1e6bb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_42__seg_41(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_42__seg_41,
        const_str_plain__seg_41,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e47afb089ce671345d720a7ba7c03ea9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_43__seg_42(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_43__seg_42,
        const_str_plain__seg_42,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_306de7336b8f117df34477678b0fcc3e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_44__seg_43(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_44__seg_43,
        const_str_plain__seg_43,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9c3ab0a715c0236fcf40cad34dac3f44,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_45__seg_44(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_45__seg_44,
        const_str_plain__seg_44,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0bd5872adb4144aa3119946f31e4b8de,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_46__seg_45(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_46__seg_45,
        const_str_plain__seg_45,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a90f97c57d371861ad15930750624791,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_47__seg_46(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_47__seg_46,
        const_str_plain__seg_46,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6bd2336abb8f1d3fda6b81eb544b71ed,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_48__seg_47(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_48__seg_47,
        const_str_plain__seg_47,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5202007547e11436e13e0b8c468e8b21,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_49__seg_48(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_49__seg_48,
        const_str_plain__seg_48,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_fa079ab7c1b5a43dd83777db6d5e1716,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_4__seg_3(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_4__seg_3,
        const_str_plain__seg_3,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_dc3f19deb1061d194d9552a67b85c02c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_50__seg_49(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_50__seg_49,
        const_str_plain__seg_49,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_669b0e7f202378d49b31d1d9e2a22c33,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_51__seg_50(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_51__seg_50,
        const_str_plain__seg_50,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_62a4843e10452bef004e8bf54cc73022,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_52__seg_51(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_52__seg_51,
        const_str_plain__seg_51,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a3f570306278a46057db0d4d71834e37,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_53__seg_52(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_53__seg_52,
        const_str_plain__seg_52,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7a1cc1394f347b8b403173ce673bcd51,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_54__seg_53(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_54__seg_53,
        const_str_plain__seg_53,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_672a3b68909bf701c262b889c129a0c3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_55__seg_54(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_55__seg_54,
        const_str_plain__seg_54,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_eb168ab0bd16341de75f158f8fbb6b03,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_56__seg_55(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_56__seg_55,
        const_str_plain__seg_55,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b78114e050df667e706712486f8726cc,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_57__seg_56(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_57__seg_56,
        const_str_plain__seg_56,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_7c30347e8809be8c2e9d57f11b47c271,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_58__seg_57(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_58__seg_57,
        const_str_plain__seg_57,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6cb6ecd9915a45d38606a89ea70cffbf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_59__seg_58(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_59__seg_58,
        const_str_plain__seg_58,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b66113f34f015345992f6d285852838b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_5__seg_4(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_5__seg_4,
        const_str_plain__seg_4,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d6864a0fbf3ac1b484407477af49555d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_60__seg_59(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_60__seg_59,
        const_str_plain__seg_59,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_adfb27a45faf8b9da43b1caa3bc79b21,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_61__seg_60(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_61__seg_60,
        const_str_plain__seg_60,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e0080e54f96f570b4161a8234f5e30e4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_62__seg_61(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_62__seg_61,
        const_str_plain__seg_61,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_97216d86558448da6a422f5ec5ddda37,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_63__seg_62(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_63__seg_62,
        const_str_plain__seg_62,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_b69622bcf94469d9087eb9cf657fde50,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_64__seg_63(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_64__seg_63,
        const_str_plain__seg_63,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_baf151f1c824fdb9121c91b9f5b9d42d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_65__seg_64(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_65__seg_64,
        const_str_plain__seg_64,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1a87871ac85a87ca0b713a429ac466b7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_66__seg_65(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_66__seg_65,
        const_str_plain__seg_65,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3779b20bf62f93955f19a01f1f02bc3c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_67__seg_66(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_67__seg_66,
        const_str_plain__seg_66,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_92128ef943e5b225211334efd4374d9d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_68__seg_67(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_68__seg_67,
        const_str_plain__seg_67,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a24e092aa40f97eb9453bca23883ab67,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_69__seg_68(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_69__seg_68,
        const_str_plain__seg_68,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0f4307b63f4bffd677deffd80acdda73,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_6__seg_5(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_6__seg_5,
        const_str_plain__seg_5,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5afdf61ece171774320ec792667b3650,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_70__seg_69(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_70__seg_69,
        const_str_plain__seg_69,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_532e3b5972d928da4cbacc9b0a839e99,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_71__seg_70(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_71__seg_70,
        const_str_plain__seg_70,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2ff74c29095335cae6182c5cb3112ea1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_72__seg_71(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_72__seg_71,
        const_str_plain__seg_71,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f36396bf69fc1cdb77c078e61b476ac2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_73__seg_72(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_73__seg_72,
        const_str_plain__seg_72,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c28bfe29093246e69eb238a8ffe684b0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_74__seg_73(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_74__seg_73,
        const_str_plain__seg_73,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3f94c0b2ee14fdb07aed9f2b4e5d169b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_75__seg_74(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_75__seg_74,
        const_str_plain__seg_74,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_96537a3567e3c9b6e5524a4149ff607c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_76__seg_75(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_76__seg_75,
        const_str_plain__seg_75,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e60117d1b39cd0a61c4d72789562af3e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_77__seg_76(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_77__seg_76,
        const_str_plain__seg_76,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_f93adcd37c54c2900833b58780c4b419,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_78__seg_77(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_78__seg_77,
        const_str_plain__seg_77,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_503331b9d50b3252cb1ade63dea607aa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_79__seg_78(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_79__seg_78,
        const_str_plain__seg_78,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c828630c384b397c213155991373d206,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_7__seg_6(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_7__seg_6,
        const_str_plain__seg_6,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4c7e193c0173774f0df300b89ee3041a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_8__seg_7(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_8__seg_7,
        const_str_plain__seg_7,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_73601843dcad5bd33cb1730a7eeea121,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_idna$uts46data$$$function_9__seg_8(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_idna$uts46data$$$function_9__seg_8,
        const_str_plain__seg_8,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e8f3445cb6f8f9d5388c5f4add13e42e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_idna$uts46data,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_idna$uts46data =
{
    PyModuleDef_HEAD_INIT,
    "idna.uts46data",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(idna$uts46data)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(idna$uts46data)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_idna$uts46data );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("idna.uts46data: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("idna.uts46data: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("idna.uts46data: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initidna$uts46data" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_idna$uts46data = Py_InitModule4(
        "idna.uts46data",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_idna$uts46data = PyModule_Create( &mdef_idna$uts46data );
#endif

    moduledict_idna$uts46data = MODULE_DICT( module_idna$uts46data );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_idna$uts46data,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_idna$uts46data,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_idna$uts46data,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_idna$uts46data,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_idna$uts46data );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_236a2076b696ba5c64a198901f584c91, module_idna$uts46data );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    struct Nuitka_FrameObject *frame_94d7e88b3efd3989bb5abf95c99f44bc;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_dd792d04ec2314d7746fad73a142a1d6;
        UPDATE_STRING_DICT0( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_94d7e88b3efd3989bb5abf95c99f44bc = MAKE_MODULE_FRAME( codeobj_94d7e88b3efd3989bb5abf95c99f44bc, module_idna$uts46data );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_94d7e88b3efd3989bb5abf95c99f44bc );
    assert( Py_REFCNT( frame_94d7e88b3efd3989bb5abf95c99f44bc ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = const_str_digest_ad7ee6951262f9b87a63ca77e4f7b77a;
        UPDATE_STRING_DICT0( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain___version__, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        tmp_assign_source_5 = MAKE_FUNCTION_idna$uts46data$$$function_1__seg_0(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_0, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        tmp_assign_source_6 = MAKE_FUNCTION_idna$uts46data$$$function_2__seg_1(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_1, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        tmp_assign_source_7 = MAKE_FUNCTION_idna$uts46data$$$function_3__seg_2(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_2, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        tmp_assign_source_8 = MAKE_FUNCTION_idna$uts46data$$$function_4__seg_3(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_3, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        tmp_assign_source_9 = MAKE_FUNCTION_idna$uts46data$$$function_5__seg_4(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_4, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        tmp_assign_source_10 = MAKE_FUNCTION_idna$uts46data$$$function_6__seg_5(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_5, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = MAKE_FUNCTION_idna$uts46data$$$function_7__seg_6(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_6, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = MAKE_FUNCTION_idna$uts46data$$$function_8__seg_7(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_7, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = MAKE_FUNCTION_idna$uts46data$$$function_9__seg_8(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_8, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_idna$uts46data$$$function_10__seg_9(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_9, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        tmp_assign_source_15 = MAKE_FUNCTION_idna$uts46data$$$function_11__seg_10(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_10, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = MAKE_FUNCTION_idna$uts46data$$$function_12__seg_11(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_11, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = MAKE_FUNCTION_idna$uts46data$$$function_13__seg_12(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_12, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_idna$uts46data$$$function_14__seg_13(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_13, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_idna$uts46data$$$function_15__seg_14(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_14, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = MAKE_FUNCTION_idna$uts46data$$$function_16__seg_15(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_15, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        tmp_assign_source_21 = MAKE_FUNCTION_idna$uts46data$$$function_17__seg_16(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_16, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = MAKE_FUNCTION_idna$uts46data$$$function_18__seg_17(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_17, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = MAKE_FUNCTION_idna$uts46data$$$function_19__seg_18(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_18, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_idna$uts46data$$$function_20__seg_19(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_19, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = MAKE_FUNCTION_idna$uts46data$$$function_21__seg_20(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_20, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        tmp_assign_source_26 = MAKE_FUNCTION_idna$uts46data$$$function_22__seg_21(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_21, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        tmp_assign_source_27 = MAKE_FUNCTION_idna$uts46data$$$function_23__seg_22(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_22, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        tmp_assign_source_28 = MAKE_FUNCTION_idna$uts46data$$$function_24__seg_23(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_23, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        tmp_assign_source_29 = MAKE_FUNCTION_idna$uts46data$$$function_25__seg_24(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_24, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        tmp_assign_source_30 = MAKE_FUNCTION_idna$uts46data$$$function_26__seg_25(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_25, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        tmp_assign_source_31 = MAKE_FUNCTION_idna$uts46data$$$function_27__seg_26(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_26, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        tmp_assign_source_32 = MAKE_FUNCTION_idna$uts46data$$$function_28__seg_27(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_27, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        tmp_assign_source_33 = MAKE_FUNCTION_idna$uts46data$$$function_29__seg_28(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_28, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        tmp_assign_source_34 = MAKE_FUNCTION_idna$uts46data$$$function_30__seg_29(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_29, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        tmp_assign_source_35 = MAKE_FUNCTION_idna$uts46data$$$function_31__seg_30(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_30, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        tmp_assign_source_36 = MAKE_FUNCTION_idna$uts46data$$$function_32__seg_31(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_31, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        tmp_assign_source_37 = MAKE_FUNCTION_idna$uts46data$$$function_33__seg_32(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_32, tmp_assign_source_37 );
    }
    {
        PyObject *tmp_assign_source_38;
        tmp_assign_source_38 = MAKE_FUNCTION_idna$uts46data$$$function_34__seg_33(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_33, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        tmp_assign_source_39 = MAKE_FUNCTION_idna$uts46data$$$function_35__seg_34(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_34, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        tmp_assign_source_40 = MAKE_FUNCTION_idna$uts46data$$$function_36__seg_35(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_35, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        tmp_assign_source_41 = MAKE_FUNCTION_idna$uts46data$$$function_37__seg_36(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_36, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        tmp_assign_source_42 = MAKE_FUNCTION_idna$uts46data$$$function_38__seg_37(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_37, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        tmp_assign_source_43 = MAKE_FUNCTION_idna$uts46data$$$function_39__seg_38(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_38, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        tmp_assign_source_44 = MAKE_FUNCTION_idna$uts46data$$$function_40__seg_39(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_39, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        tmp_assign_source_45 = MAKE_FUNCTION_idna$uts46data$$$function_41__seg_40(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_40, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        tmp_assign_source_46 = MAKE_FUNCTION_idna$uts46data$$$function_42__seg_41(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_41, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        tmp_assign_source_47 = MAKE_FUNCTION_idna$uts46data$$$function_43__seg_42(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_42, tmp_assign_source_47 );
    }
    {
        PyObject *tmp_assign_source_48;
        tmp_assign_source_48 = MAKE_FUNCTION_idna$uts46data$$$function_44__seg_43(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_43, tmp_assign_source_48 );
    }
    {
        PyObject *tmp_assign_source_49;
        tmp_assign_source_49 = MAKE_FUNCTION_idna$uts46data$$$function_45__seg_44(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_44, tmp_assign_source_49 );
    }
    {
        PyObject *tmp_assign_source_50;
        tmp_assign_source_50 = MAKE_FUNCTION_idna$uts46data$$$function_46__seg_45(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_45, tmp_assign_source_50 );
    }
    {
        PyObject *tmp_assign_source_51;
        tmp_assign_source_51 = MAKE_FUNCTION_idna$uts46data$$$function_47__seg_46(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_46, tmp_assign_source_51 );
    }
    {
        PyObject *tmp_assign_source_52;
        tmp_assign_source_52 = MAKE_FUNCTION_idna$uts46data$$$function_48__seg_47(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_47, tmp_assign_source_52 );
    }
    {
        PyObject *tmp_assign_source_53;
        tmp_assign_source_53 = MAKE_FUNCTION_idna$uts46data$$$function_49__seg_48(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_48, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        tmp_assign_source_54 = MAKE_FUNCTION_idna$uts46data$$$function_50__seg_49(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_49, tmp_assign_source_54 );
    }
    {
        PyObject *tmp_assign_source_55;
        tmp_assign_source_55 = MAKE_FUNCTION_idna$uts46data$$$function_51__seg_50(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_50, tmp_assign_source_55 );
    }
    {
        PyObject *tmp_assign_source_56;
        tmp_assign_source_56 = MAKE_FUNCTION_idna$uts46data$$$function_52__seg_51(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_51, tmp_assign_source_56 );
    }
    {
        PyObject *tmp_assign_source_57;
        tmp_assign_source_57 = MAKE_FUNCTION_idna$uts46data$$$function_53__seg_52(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_52, tmp_assign_source_57 );
    }
    {
        PyObject *tmp_assign_source_58;
        tmp_assign_source_58 = MAKE_FUNCTION_idna$uts46data$$$function_54__seg_53(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_53, tmp_assign_source_58 );
    }
    {
        PyObject *tmp_assign_source_59;
        tmp_assign_source_59 = MAKE_FUNCTION_idna$uts46data$$$function_55__seg_54(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_54, tmp_assign_source_59 );
    }
    {
        PyObject *tmp_assign_source_60;
        tmp_assign_source_60 = MAKE_FUNCTION_idna$uts46data$$$function_56__seg_55(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_55, tmp_assign_source_60 );
    }
    {
        PyObject *tmp_assign_source_61;
        tmp_assign_source_61 = MAKE_FUNCTION_idna$uts46data$$$function_57__seg_56(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_56, tmp_assign_source_61 );
    }
    {
        PyObject *tmp_assign_source_62;
        tmp_assign_source_62 = MAKE_FUNCTION_idna$uts46data$$$function_58__seg_57(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_57, tmp_assign_source_62 );
    }
    {
        PyObject *tmp_assign_source_63;
        tmp_assign_source_63 = MAKE_FUNCTION_idna$uts46data$$$function_59__seg_58(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_58, tmp_assign_source_63 );
    }
    {
        PyObject *tmp_assign_source_64;
        tmp_assign_source_64 = MAKE_FUNCTION_idna$uts46data$$$function_60__seg_59(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_59, tmp_assign_source_64 );
    }
    {
        PyObject *tmp_assign_source_65;
        tmp_assign_source_65 = MAKE_FUNCTION_idna$uts46data$$$function_61__seg_60(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_60, tmp_assign_source_65 );
    }
    {
        PyObject *tmp_assign_source_66;
        tmp_assign_source_66 = MAKE_FUNCTION_idna$uts46data$$$function_62__seg_61(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_61, tmp_assign_source_66 );
    }
    {
        PyObject *tmp_assign_source_67;
        tmp_assign_source_67 = MAKE_FUNCTION_idna$uts46data$$$function_63__seg_62(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_62, tmp_assign_source_67 );
    }
    {
        PyObject *tmp_assign_source_68;
        tmp_assign_source_68 = MAKE_FUNCTION_idna$uts46data$$$function_64__seg_63(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_63, tmp_assign_source_68 );
    }
    {
        PyObject *tmp_assign_source_69;
        tmp_assign_source_69 = MAKE_FUNCTION_idna$uts46data$$$function_65__seg_64(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_64, tmp_assign_source_69 );
    }
    {
        PyObject *tmp_assign_source_70;
        tmp_assign_source_70 = MAKE_FUNCTION_idna$uts46data$$$function_66__seg_65(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_65, tmp_assign_source_70 );
    }
    {
        PyObject *tmp_assign_source_71;
        tmp_assign_source_71 = MAKE_FUNCTION_idna$uts46data$$$function_67__seg_66(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_66, tmp_assign_source_71 );
    }
    {
        PyObject *tmp_assign_source_72;
        tmp_assign_source_72 = MAKE_FUNCTION_idna$uts46data$$$function_68__seg_67(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_67, tmp_assign_source_72 );
    }
    {
        PyObject *tmp_assign_source_73;
        tmp_assign_source_73 = MAKE_FUNCTION_idna$uts46data$$$function_69__seg_68(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_68, tmp_assign_source_73 );
    }
    {
        PyObject *tmp_assign_source_74;
        tmp_assign_source_74 = MAKE_FUNCTION_idna$uts46data$$$function_70__seg_69(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_69, tmp_assign_source_74 );
    }
    {
        PyObject *tmp_assign_source_75;
        tmp_assign_source_75 = MAKE_FUNCTION_idna$uts46data$$$function_71__seg_70(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_70, tmp_assign_source_75 );
    }
    {
        PyObject *tmp_assign_source_76;
        tmp_assign_source_76 = MAKE_FUNCTION_idna$uts46data$$$function_72__seg_71(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_71, tmp_assign_source_76 );
    }
    {
        PyObject *tmp_assign_source_77;
        tmp_assign_source_77 = MAKE_FUNCTION_idna$uts46data$$$function_73__seg_72(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_72, tmp_assign_source_77 );
    }
    {
        PyObject *tmp_assign_source_78;
        tmp_assign_source_78 = MAKE_FUNCTION_idna$uts46data$$$function_74__seg_73(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_73, tmp_assign_source_78 );
    }
    {
        PyObject *tmp_assign_source_79;
        tmp_assign_source_79 = MAKE_FUNCTION_idna$uts46data$$$function_75__seg_74(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_74, tmp_assign_source_79 );
    }
    {
        PyObject *tmp_assign_source_80;
        tmp_assign_source_80 = MAKE_FUNCTION_idna$uts46data$$$function_76__seg_75(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_75, tmp_assign_source_80 );
    }
    {
        PyObject *tmp_assign_source_81;
        tmp_assign_source_81 = MAKE_FUNCTION_idna$uts46data$$$function_77__seg_76(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_76, tmp_assign_source_81 );
    }
    {
        PyObject *tmp_assign_source_82;
        tmp_assign_source_82 = MAKE_FUNCTION_idna$uts46data$$$function_78__seg_77(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_77, tmp_assign_source_82 );
    }
    {
        PyObject *tmp_assign_source_83;
        tmp_assign_source_83 = MAKE_FUNCTION_idna$uts46data$$$function_79__seg_78(  );



        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_78, tmp_assign_source_83 );
    }
    {
        PyObject *tmp_assign_source_84;
        PyObject *tmp_tuple_arg_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_left_name_2;
        PyObject *tmp_left_name_3;
        PyObject *tmp_left_name_4;
        PyObject *tmp_left_name_5;
        PyObject *tmp_left_name_6;
        PyObject *tmp_left_name_7;
        PyObject *tmp_left_name_8;
        PyObject *tmp_left_name_9;
        PyObject *tmp_left_name_10;
        PyObject *tmp_left_name_11;
        PyObject *tmp_left_name_12;
        PyObject *tmp_left_name_13;
        PyObject *tmp_left_name_14;
        PyObject *tmp_left_name_15;
        PyObject *tmp_left_name_16;
        PyObject *tmp_left_name_17;
        PyObject *tmp_left_name_18;
        PyObject *tmp_left_name_19;
        PyObject *tmp_left_name_20;
        PyObject *tmp_left_name_21;
        PyObject *tmp_left_name_22;
        PyObject *tmp_left_name_23;
        PyObject *tmp_left_name_24;
        PyObject *tmp_left_name_25;
        PyObject *tmp_left_name_26;
        PyObject *tmp_left_name_27;
        PyObject *tmp_left_name_28;
        PyObject *tmp_left_name_29;
        PyObject *tmp_left_name_30;
        PyObject *tmp_left_name_31;
        PyObject *tmp_left_name_32;
        PyObject *tmp_left_name_33;
        PyObject *tmp_left_name_34;
        PyObject *tmp_left_name_35;
        PyObject *tmp_left_name_36;
        PyObject *tmp_left_name_37;
        PyObject *tmp_left_name_38;
        PyObject *tmp_left_name_39;
        PyObject *tmp_left_name_40;
        PyObject *tmp_left_name_41;
        PyObject *tmp_left_name_42;
        PyObject *tmp_left_name_43;
        PyObject *tmp_left_name_44;
        PyObject *tmp_left_name_45;
        PyObject *tmp_left_name_46;
        PyObject *tmp_left_name_47;
        PyObject *tmp_left_name_48;
        PyObject *tmp_left_name_49;
        PyObject *tmp_left_name_50;
        PyObject *tmp_left_name_51;
        PyObject *tmp_left_name_52;
        PyObject *tmp_left_name_53;
        PyObject *tmp_left_name_54;
        PyObject *tmp_left_name_55;
        PyObject *tmp_left_name_56;
        PyObject *tmp_left_name_57;
        PyObject *tmp_left_name_58;
        PyObject *tmp_left_name_59;
        PyObject *tmp_left_name_60;
        PyObject *tmp_left_name_61;
        PyObject *tmp_left_name_62;
        PyObject *tmp_left_name_63;
        PyObject *tmp_left_name_64;
        PyObject *tmp_left_name_65;
        PyObject *tmp_left_name_66;
        PyObject *tmp_left_name_67;
        PyObject *tmp_left_name_68;
        PyObject *tmp_left_name_69;
        PyObject *tmp_left_name_70;
        PyObject *tmp_left_name_71;
        PyObject *tmp_left_name_72;
        PyObject *tmp_left_name_73;
        PyObject *tmp_left_name_74;
        PyObject *tmp_left_name_75;
        PyObject *tmp_left_name_76;
        PyObject *tmp_left_name_77;
        PyObject *tmp_left_name_78;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_right_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_right_name_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_right_name_4;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_right_name_5;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_right_name_6;
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_right_name_7;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_right_name_8;
        PyObject *tmp_called_name_9;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_right_name_9;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_right_name_10;
        PyObject *tmp_called_name_11;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_right_name_11;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_right_name_12;
        PyObject *tmp_called_name_13;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_right_name_13;
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_right_name_14;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_right_name_15;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_right_name_16;
        PyObject *tmp_called_name_17;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_right_name_17;
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_right_name_18;
        PyObject *tmp_called_name_19;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_right_name_19;
        PyObject *tmp_called_name_20;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_right_name_20;
        PyObject *tmp_called_name_21;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_right_name_21;
        PyObject *tmp_called_name_22;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_right_name_22;
        PyObject *tmp_called_name_23;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_right_name_23;
        PyObject *tmp_called_name_24;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_right_name_24;
        PyObject *tmp_called_name_25;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_right_name_25;
        PyObject *tmp_called_name_26;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_right_name_26;
        PyObject *tmp_called_name_27;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_right_name_27;
        PyObject *tmp_called_name_28;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_right_name_28;
        PyObject *tmp_called_name_29;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_right_name_29;
        PyObject *tmp_called_name_30;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_right_name_30;
        PyObject *tmp_called_name_31;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_right_name_31;
        PyObject *tmp_called_name_32;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_right_name_32;
        PyObject *tmp_called_name_33;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_right_name_33;
        PyObject *tmp_called_name_34;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_right_name_34;
        PyObject *tmp_called_name_35;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_right_name_35;
        PyObject *tmp_called_name_36;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_right_name_36;
        PyObject *tmp_called_name_37;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_right_name_37;
        PyObject *tmp_called_name_38;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_right_name_38;
        PyObject *tmp_called_name_39;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_right_name_39;
        PyObject *tmp_called_name_40;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_right_name_40;
        PyObject *tmp_called_name_41;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_right_name_41;
        PyObject *tmp_called_name_42;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_right_name_42;
        PyObject *tmp_called_name_43;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_right_name_43;
        PyObject *tmp_called_name_44;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_right_name_44;
        PyObject *tmp_called_name_45;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_right_name_45;
        PyObject *tmp_called_name_46;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_right_name_46;
        PyObject *tmp_called_name_47;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_right_name_47;
        PyObject *tmp_called_name_48;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_right_name_48;
        PyObject *tmp_called_name_49;
        PyObject *tmp_mvar_value_51;
        PyObject *tmp_right_name_49;
        PyObject *tmp_called_name_50;
        PyObject *tmp_mvar_value_52;
        PyObject *tmp_right_name_50;
        PyObject *tmp_called_name_51;
        PyObject *tmp_mvar_value_53;
        PyObject *tmp_right_name_51;
        PyObject *tmp_called_name_52;
        PyObject *tmp_mvar_value_54;
        PyObject *tmp_right_name_52;
        PyObject *tmp_called_name_53;
        PyObject *tmp_mvar_value_55;
        PyObject *tmp_right_name_53;
        PyObject *tmp_called_name_54;
        PyObject *tmp_mvar_value_56;
        PyObject *tmp_right_name_54;
        PyObject *tmp_called_name_55;
        PyObject *tmp_mvar_value_57;
        PyObject *tmp_right_name_55;
        PyObject *tmp_called_name_56;
        PyObject *tmp_mvar_value_58;
        PyObject *tmp_right_name_56;
        PyObject *tmp_called_name_57;
        PyObject *tmp_mvar_value_59;
        PyObject *tmp_right_name_57;
        PyObject *tmp_called_name_58;
        PyObject *tmp_mvar_value_60;
        PyObject *tmp_right_name_58;
        PyObject *tmp_called_name_59;
        PyObject *tmp_mvar_value_61;
        PyObject *tmp_right_name_59;
        PyObject *tmp_called_name_60;
        PyObject *tmp_mvar_value_62;
        PyObject *tmp_right_name_60;
        PyObject *tmp_called_name_61;
        PyObject *tmp_mvar_value_63;
        PyObject *tmp_right_name_61;
        PyObject *tmp_called_name_62;
        PyObject *tmp_mvar_value_64;
        PyObject *tmp_right_name_62;
        PyObject *tmp_called_name_63;
        PyObject *tmp_mvar_value_65;
        PyObject *tmp_right_name_63;
        PyObject *tmp_called_name_64;
        PyObject *tmp_mvar_value_66;
        PyObject *tmp_right_name_64;
        PyObject *tmp_called_name_65;
        PyObject *tmp_mvar_value_67;
        PyObject *tmp_right_name_65;
        PyObject *tmp_called_name_66;
        PyObject *tmp_mvar_value_68;
        PyObject *tmp_right_name_66;
        PyObject *tmp_called_name_67;
        PyObject *tmp_mvar_value_69;
        PyObject *tmp_right_name_67;
        PyObject *tmp_called_name_68;
        PyObject *tmp_mvar_value_70;
        PyObject *tmp_right_name_68;
        PyObject *tmp_called_name_69;
        PyObject *tmp_mvar_value_71;
        PyObject *tmp_right_name_69;
        PyObject *tmp_called_name_70;
        PyObject *tmp_mvar_value_72;
        PyObject *tmp_right_name_70;
        PyObject *tmp_called_name_71;
        PyObject *tmp_mvar_value_73;
        PyObject *tmp_right_name_71;
        PyObject *tmp_called_name_72;
        PyObject *tmp_mvar_value_74;
        PyObject *tmp_right_name_72;
        PyObject *tmp_called_name_73;
        PyObject *tmp_mvar_value_75;
        PyObject *tmp_right_name_73;
        PyObject *tmp_called_name_74;
        PyObject *tmp_mvar_value_76;
        PyObject *tmp_right_name_74;
        PyObject *tmp_called_name_75;
        PyObject *tmp_mvar_value_77;
        PyObject *tmp_right_name_75;
        PyObject *tmp_called_name_76;
        PyObject *tmp_mvar_value_78;
        PyObject *tmp_right_name_76;
        PyObject *tmp_called_name_77;
        PyObject *tmp_mvar_value_79;
        PyObject *tmp_right_name_77;
        PyObject *tmp_called_name_78;
        PyObject *tmp_mvar_value_80;
        PyObject *tmp_right_name_78;
        PyObject *tmp_called_name_79;
        PyObject *tmp_mvar_value_81;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_0 );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_0 );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_1 = tmp_mvar_value_3;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8126;
        tmp_left_name_78 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_left_name_78 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8126;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_1 );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_1 );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_left_name_78 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_1" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8127;

            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_4;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8127;
        tmp_right_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_78 );

            exception_lineno = 8127;

            goto frame_exception_exit_1;
        }
        tmp_left_name_77 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_78, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_78 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_left_name_77 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8126;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_2 );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_2 );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_left_name_77 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_2" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8128;

            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_5;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8128;
        tmp_right_name_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_77 );

            exception_lineno = 8128;

            goto frame_exception_exit_1;
        }
        tmp_left_name_76 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_77, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_77 );
        Py_DECREF( tmp_right_name_2 );
        if ( tmp_left_name_76 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8128;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_3 );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_3 );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_left_name_76 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_3" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8129;

            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_6;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8129;
        tmp_right_name_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_4 );
        if ( tmp_right_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_76 );

            exception_lineno = 8129;

            goto frame_exception_exit_1;
        }
        tmp_left_name_75 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_76, tmp_right_name_3 );
        Py_DECREF( tmp_left_name_76 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_left_name_75 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8129;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_4 );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_4 );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_left_name_75 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_4" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8130;

            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_7;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8130;
        tmp_right_name_4 = CALL_FUNCTION_NO_ARGS( tmp_called_name_5 );
        if ( tmp_right_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_75 );

            exception_lineno = 8130;

            goto frame_exception_exit_1;
        }
        tmp_left_name_74 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_75, tmp_right_name_4 );
        Py_DECREF( tmp_left_name_75 );
        Py_DECREF( tmp_right_name_4 );
        if ( tmp_left_name_74 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8130;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_5 );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_5 );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_left_name_74 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_5" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8131;

            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_8;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8131;
        tmp_right_name_5 = CALL_FUNCTION_NO_ARGS( tmp_called_name_6 );
        if ( tmp_right_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_74 );

            exception_lineno = 8131;

            goto frame_exception_exit_1;
        }
        tmp_left_name_73 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_74, tmp_right_name_5 );
        Py_DECREF( tmp_left_name_74 );
        Py_DECREF( tmp_right_name_5 );
        if ( tmp_left_name_73 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8131;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_6 );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_6 );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_left_name_73 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_6" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8132;

            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_9;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8132;
        tmp_right_name_6 = CALL_FUNCTION_NO_ARGS( tmp_called_name_7 );
        if ( tmp_right_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_73 );

            exception_lineno = 8132;

            goto frame_exception_exit_1;
        }
        tmp_left_name_72 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_73, tmp_right_name_6 );
        Py_DECREF( tmp_left_name_73 );
        Py_DECREF( tmp_right_name_6 );
        if ( tmp_left_name_72 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8132;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_7 );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_7 );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_left_name_72 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_7" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8133;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_10;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8133;
        tmp_right_name_7 = CALL_FUNCTION_NO_ARGS( tmp_called_name_8 );
        if ( tmp_right_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_72 );

            exception_lineno = 8133;

            goto frame_exception_exit_1;
        }
        tmp_left_name_71 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_72, tmp_right_name_7 );
        Py_DECREF( tmp_left_name_72 );
        Py_DECREF( tmp_right_name_7 );
        if ( tmp_left_name_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8133;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_8 );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_8 );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_left_name_71 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_8" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8134;

            goto frame_exception_exit_1;
        }

        tmp_called_name_9 = tmp_mvar_value_11;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8134;
        tmp_right_name_8 = CALL_FUNCTION_NO_ARGS( tmp_called_name_9 );
        if ( tmp_right_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_71 );

            exception_lineno = 8134;

            goto frame_exception_exit_1;
        }
        tmp_left_name_70 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_71, tmp_right_name_8 );
        Py_DECREF( tmp_left_name_71 );
        Py_DECREF( tmp_right_name_8 );
        if ( tmp_left_name_70 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8134;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_9 );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_9 );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_left_name_70 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_9" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8135;

            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_12;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8135;
        tmp_right_name_9 = CALL_FUNCTION_NO_ARGS( tmp_called_name_10 );
        if ( tmp_right_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_70 );

            exception_lineno = 8135;

            goto frame_exception_exit_1;
        }
        tmp_left_name_69 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_70, tmp_right_name_9 );
        Py_DECREF( tmp_left_name_70 );
        Py_DECREF( tmp_right_name_9 );
        if ( tmp_left_name_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8135;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_10 );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_10 );
        }

        if ( tmp_mvar_value_13 == NULL )
        {
            Py_DECREF( tmp_left_name_69 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_10" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8136;

            goto frame_exception_exit_1;
        }

        tmp_called_name_11 = tmp_mvar_value_13;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8136;
        tmp_right_name_10 = CALL_FUNCTION_NO_ARGS( tmp_called_name_11 );
        if ( tmp_right_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_69 );

            exception_lineno = 8136;

            goto frame_exception_exit_1;
        }
        tmp_left_name_68 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_69, tmp_right_name_10 );
        Py_DECREF( tmp_left_name_69 );
        Py_DECREF( tmp_right_name_10 );
        if ( tmp_left_name_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8136;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_11 );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_11 );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_left_name_68 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_11" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8137;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_14;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8137;
        tmp_right_name_11 = CALL_FUNCTION_NO_ARGS( tmp_called_name_12 );
        if ( tmp_right_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_68 );

            exception_lineno = 8137;

            goto frame_exception_exit_1;
        }
        tmp_left_name_67 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_68, tmp_right_name_11 );
        Py_DECREF( tmp_left_name_68 );
        Py_DECREF( tmp_right_name_11 );
        if ( tmp_left_name_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8137;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_12 );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_12 );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_left_name_67 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_12" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8138;

            goto frame_exception_exit_1;
        }

        tmp_called_name_13 = tmp_mvar_value_15;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8138;
        tmp_right_name_12 = CALL_FUNCTION_NO_ARGS( tmp_called_name_13 );
        if ( tmp_right_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_67 );

            exception_lineno = 8138;

            goto frame_exception_exit_1;
        }
        tmp_left_name_66 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_67, tmp_right_name_12 );
        Py_DECREF( tmp_left_name_67 );
        Py_DECREF( tmp_right_name_12 );
        if ( tmp_left_name_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8138;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_13 );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_13 );
        }

        if ( tmp_mvar_value_16 == NULL )
        {
            Py_DECREF( tmp_left_name_66 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_13" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8139;

            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_16;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8139;
        tmp_right_name_13 = CALL_FUNCTION_NO_ARGS( tmp_called_name_14 );
        if ( tmp_right_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_66 );

            exception_lineno = 8139;

            goto frame_exception_exit_1;
        }
        tmp_left_name_65 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_66, tmp_right_name_13 );
        Py_DECREF( tmp_left_name_66 );
        Py_DECREF( tmp_right_name_13 );
        if ( tmp_left_name_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8139;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_14 );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_14 );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_left_name_65 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_14" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8140;

            goto frame_exception_exit_1;
        }

        tmp_called_name_15 = tmp_mvar_value_17;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8140;
        tmp_right_name_14 = CALL_FUNCTION_NO_ARGS( tmp_called_name_15 );
        if ( tmp_right_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_65 );

            exception_lineno = 8140;

            goto frame_exception_exit_1;
        }
        tmp_left_name_64 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_65, tmp_right_name_14 );
        Py_DECREF( tmp_left_name_65 );
        Py_DECREF( tmp_right_name_14 );
        if ( tmp_left_name_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8140;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_15 );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_15 );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_left_name_64 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_15" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8141;

            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_18;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8141;
        tmp_right_name_15 = CALL_FUNCTION_NO_ARGS( tmp_called_name_16 );
        if ( tmp_right_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_64 );

            exception_lineno = 8141;

            goto frame_exception_exit_1;
        }
        tmp_left_name_63 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_64, tmp_right_name_15 );
        Py_DECREF( tmp_left_name_64 );
        Py_DECREF( tmp_right_name_15 );
        if ( tmp_left_name_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8141;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_16 );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_16 );
        }

        if ( tmp_mvar_value_19 == NULL )
        {
            Py_DECREF( tmp_left_name_63 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_16" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8142;

            goto frame_exception_exit_1;
        }

        tmp_called_name_17 = tmp_mvar_value_19;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8142;
        tmp_right_name_16 = CALL_FUNCTION_NO_ARGS( tmp_called_name_17 );
        if ( tmp_right_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_63 );

            exception_lineno = 8142;

            goto frame_exception_exit_1;
        }
        tmp_left_name_62 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_63, tmp_right_name_16 );
        Py_DECREF( tmp_left_name_63 );
        Py_DECREF( tmp_right_name_16 );
        if ( tmp_left_name_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8142;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_17 );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_17 );
        }

        if ( tmp_mvar_value_20 == NULL )
        {
            Py_DECREF( tmp_left_name_62 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_17" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8143;

            goto frame_exception_exit_1;
        }

        tmp_called_name_18 = tmp_mvar_value_20;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8143;
        tmp_right_name_17 = CALL_FUNCTION_NO_ARGS( tmp_called_name_18 );
        if ( tmp_right_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_62 );

            exception_lineno = 8143;

            goto frame_exception_exit_1;
        }
        tmp_left_name_61 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_62, tmp_right_name_17 );
        Py_DECREF( tmp_left_name_62 );
        Py_DECREF( tmp_right_name_17 );
        if ( tmp_left_name_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8143;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_18 );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_18 );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_left_name_61 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_18" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8144;

            goto frame_exception_exit_1;
        }

        tmp_called_name_19 = tmp_mvar_value_21;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8144;
        tmp_right_name_18 = CALL_FUNCTION_NO_ARGS( tmp_called_name_19 );
        if ( tmp_right_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_61 );

            exception_lineno = 8144;

            goto frame_exception_exit_1;
        }
        tmp_left_name_60 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_61, tmp_right_name_18 );
        Py_DECREF( tmp_left_name_61 );
        Py_DECREF( tmp_right_name_18 );
        if ( tmp_left_name_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8144;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_19 );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_19 );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_left_name_60 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_19" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8145;

            goto frame_exception_exit_1;
        }

        tmp_called_name_20 = tmp_mvar_value_22;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8145;
        tmp_right_name_19 = CALL_FUNCTION_NO_ARGS( tmp_called_name_20 );
        if ( tmp_right_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_60 );

            exception_lineno = 8145;

            goto frame_exception_exit_1;
        }
        tmp_left_name_59 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_60, tmp_right_name_19 );
        Py_DECREF( tmp_left_name_60 );
        Py_DECREF( tmp_right_name_19 );
        if ( tmp_left_name_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8145;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_20 );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_20 );
        }

        if ( tmp_mvar_value_23 == NULL )
        {
            Py_DECREF( tmp_left_name_59 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_20" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8146;

            goto frame_exception_exit_1;
        }

        tmp_called_name_21 = tmp_mvar_value_23;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8146;
        tmp_right_name_20 = CALL_FUNCTION_NO_ARGS( tmp_called_name_21 );
        if ( tmp_right_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_59 );

            exception_lineno = 8146;

            goto frame_exception_exit_1;
        }
        tmp_left_name_58 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_59, tmp_right_name_20 );
        Py_DECREF( tmp_left_name_59 );
        Py_DECREF( tmp_right_name_20 );
        if ( tmp_left_name_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8146;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_21 );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_21 );
        }

        if ( tmp_mvar_value_24 == NULL )
        {
            Py_DECREF( tmp_left_name_58 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_21" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8147;

            goto frame_exception_exit_1;
        }

        tmp_called_name_22 = tmp_mvar_value_24;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8147;
        tmp_right_name_21 = CALL_FUNCTION_NO_ARGS( tmp_called_name_22 );
        if ( tmp_right_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_58 );

            exception_lineno = 8147;

            goto frame_exception_exit_1;
        }
        tmp_left_name_57 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_58, tmp_right_name_21 );
        Py_DECREF( tmp_left_name_58 );
        Py_DECREF( tmp_right_name_21 );
        if ( tmp_left_name_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8147;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_22 );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_22 );
        }

        if ( tmp_mvar_value_25 == NULL )
        {
            Py_DECREF( tmp_left_name_57 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_22" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8148;

            goto frame_exception_exit_1;
        }

        tmp_called_name_23 = tmp_mvar_value_25;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8148;
        tmp_right_name_22 = CALL_FUNCTION_NO_ARGS( tmp_called_name_23 );
        if ( tmp_right_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_57 );

            exception_lineno = 8148;

            goto frame_exception_exit_1;
        }
        tmp_left_name_56 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_57, tmp_right_name_22 );
        Py_DECREF( tmp_left_name_57 );
        Py_DECREF( tmp_right_name_22 );
        if ( tmp_left_name_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8148;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_23 );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_23 );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_left_name_56 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_23" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8149;

            goto frame_exception_exit_1;
        }

        tmp_called_name_24 = tmp_mvar_value_26;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8149;
        tmp_right_name_23 = CALL_FUNCTION_NO_ARGS( tmp_called_name_24 );
        if ( tmp_right_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_56 );

            exception_lineno = 8149;

            goto frame_exception_exit_1;
        }
        tmp_left_name_55 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_56, tmp_right_name_23 );
        Py_DECREF( tmp_left_name_56 );
        Py_DECREF( tmp_right_name_23 );
        if ( tmp_left_name_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8149;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_24 );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_24 );
        }

        if ( tmp_mvar_value_27 == NULL )
        {
            Py_DECREF( tmp_left_name_55 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_24" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8150;

            goto frame_exception_exit_1;
        }

        tmp_called_name_25 = tmp_mvar_value_27;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8150;
        tmp_right_name_24 = CALL_FUNCTION_NO_ARGS( tmp_called_name_25 );
        if ( tmp_right_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_55 );

            exception_lineno = 8150;

            goto frame_exception_exit_1;
        }
        tmp_left_name_54 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_55, tmp_right_name_24 );
        Py_DECREF( tmp_left_name_55 );
        Py_DECREF( tmp_right_name_24 );
        if ( tmp_left_name_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8150;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_25 );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_25 );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_left_name_54 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_25" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8151;

            goto frame_exception_exit_1;
        }

        tmp_called_name_26 = tmp_mvar_value_28;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8151;
        tmp_right_name_25 = CALL_FUNCTION_NO_ARGS( tmp_called_name_26 );
        if ( tmp_right_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_54 );

            exception_lineno = 8151;

            goto frame_exception_exit_1;
        }
        tmp_left_name_53 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_54, tmp_right_name_25 );
        Py_DECREF( tmp_left_name_54 );
        Py_DECREF( tmp_right_name_25 );
        if ( tmp_left_name_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8151;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_26 );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_26 );
        }

        if ( tmp_mvar_value_29 == NULL )
        {
            Py_DECREF( tmp_left_name_53 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_26" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8152;

            goto frame_exception_exit_1;
        }

        tmp_called_name_27 = tmp_mvar_value_29;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8152;
        tmp_right_name_26 = CALL_FUNCTION_NO_ARGS( tmp_called_name_27 );
        if ( tmp_right_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_53 );

            exception_lineno = 8152;

            goto frame_exception_exit_1;
        }
        tmp_left_name_52 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_53, tmp_right_name_26 );
        Py_DECREF( tmp_left_name_53 );
        Py_DECREF( tmp_right_name_26 );
        if ( tmp_left_name_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8152;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_27 );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_27 );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_left_name_52 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_27" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8153;

            goto frame_exception_exit_1;
        }

        tmp_called_name_28 = tmp_mvar_value_30;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8153;
        tmp_right_name_27 = CALL_FUNCTION_NO_ARGS( tmp_called_name_28 );
        if ( tmp_right_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_52 );

            exception_lineno = 8153;

            goto frame_exception_exit_1;
        }
        tmp_left_name_51 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_52, tmp_right_name_27 );
        Py_DECREF( tmp_left_name_52 );
        Py_DECREF( tmp_right_name_27 );
        if ( tmp_left_name_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8153;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_28 );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_28 );
        }

        if ( tmp_mvar_value_31 == NULL )
        {
            Py_DECREF( tmp_left_name_51 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_28" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8154;

            goto frame_exception_exit_1;
        }

        tmp_called_name_29 = tmp_mvar_value_31;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8154;
        tmp_right_name_28 = CALL_FUNCTION_NO_ARGS( tmp_called_name_29 );
        if ( tmp_right_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_51 );

            exception_lineno = 8154;

            goto frame_exception_exit_1;
        }
        tmp_left_name_50 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_51, tmp_right_name_28 );
        Py_DECREF( tmp_left_name_51 );
        Py_DECREF( tmp_right_name_28 );
        if ( tmp_left_name_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8154;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_29 );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_29 );
        }

        if ( tmp_mvar_value_32 == NULL )
        {
            Py_DECREF( tmp_left_name_50 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_29" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8155;

            goto frame_exception_exit_1;
        }

        tmp_called_name_30 = tmp_mvar_value_32;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8155;
        tmp_right_name_29 = CALL_FUNCTION_NO_ARGS( tmp_called_name_30 );
        if ( tmp_right_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_50 );

            exception_lineno = 8155;

            goto frame_exception_exit_1;
        }
        tmp_left_name_49 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_50, tmp_right_name_29 );
        Py_DECREF( tmp_left_name_50 );
        Py_DECREF( tmp_right_name_29 );
        if ( tmp_left_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8155;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_30 );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_30 );
        }

        if ( tmp_mvar_value_33 == NULL )
        {
            Py_DECREF( tmp_left_name_49 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_30" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8156;

            goto frame_exception_exit_1;
        }

        tmp_called_name_31 = tmp_mvar_value_33;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8156;
        tmp_right_name_30 = CALL_FUNCTION_NO_ARGS( tmp_called_name_31 );
        if ( tmp_right_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_49 );

            exception_lineno = 8156;

            goto frame_exception_exit_1;
        }
        tmp_left_name_48 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_49, tmp_right_name_30 );
        Py_DECREF( tmp_left_name_49 );
        Py_DECREF( tmp_right_name_30 );
        if ( tmp_left_name_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8156;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_31 );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_31 );
        }

        if ( tmp_mvar_value_34 == NULL )
        {
            Py_DECREF( tmp_left_name_48 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_31" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8157;

            goto frame_exception_exit_1;
        }

        tmp_called_name_32 = tmp_mvar_value_34;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8157;
        tmp_right_name_31 = CALL_FUNCTION_NO_ARGS( tmp_called_name_32 );
        if ( tmp_right_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_48 );

            exception_lineno = 8157;

            goto frame_exception_exit_1;
        }
        tmp_left_name_47 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_48, tmp_right_name_31 );
        Py_DECREF( tmp_left_name_48 );
        Py_DECREF( tmp_right_name_31 );
        if ( tmp_left_name_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8157;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_32 );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_32 );
        }

        if ( tmp_mvar_value_35 == NULL )
        {
            Py_DECREF( tmp_left_name_47 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_32" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8158;

            goto frame_exception_exit_1;
        }

        tmp_called_name_33 = tmp_mvar_value_35;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8158;
        tmp_right_name_32 = CALL_FUNCTION_NO_ARGS( tmp_called_name_33 );
        if ( tmp_right_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_47 );

            exception_lineno = 8158;

            goto frame_exception_exit_1;
        }
        tmp_left_name_46 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_47, tmp_right_name_32 );
        Py_DECREF( tmp_left_name_47 );
        Py_DECREF( tmp_right_name_32 );
        if ( tmp_left_name_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8158;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_33 );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_33 );
        }

        if ( tmp_mvar_value_36 == NULL )
        {
            Py_DECREF( tmp_left_name_46 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_33" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8159;

            goto frame_exception_exit_1;
        }

        tmp_called_name_34 = tmp_mvar_value_36;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8159;
        tmp_right_name_33 = CALL_FUNCTION_NO_ARGS( tmp_called_name_34 );
        if ( tmp_right_name_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_46 );

            exception_lineno = 8159;

            goto frame_exception_exit_1;
        }
        tmp_left_name_45 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_46, tmp_right_name_33 );
        Py_DECREF( tmp_left_name_46 );
        Py_DECREF( tmp_right_name_33 );
        if ( tmp_left_name_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8159;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_34 );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_34 );
        }

        if ( tmp_mvar_value_37 == NULL )
        {
            Py_DECREF( tmp_left_name_45 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_34" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8160;

            goto frame_exception_exit_1;
        }

        tmp_called_name_35 = tmp_mvar_value_37;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8160;
        tmp_right_name_34 = CALL_FUNCTION_NO_ARGS( tmp_called_name_35 );
        if ( tmp_right_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_45 );

            exception_lineno = 8160;

            goto frame_exception_exit_1;
        }
        tmp_left_name_44 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_45, tmp_right_name_34 );
        Py_DECREF( tmp_left_name_45 );
        Py_DECREF( tmp_right_name_34 );
        if ( tmp_left_name_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8160;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_35 );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_35 );
        }

        if ( tmp_mvar_value_38 == NULL )
        {
            Py_DECREF( tmp_left_name_44 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_35" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8161;

            goto frame_exception_exit_1;
        }

        tmp_called_name_36 = tmp_mvar_value_38;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8161;
        tmp_right_name_35 = CALL_FUNCTION_NO_ARGS( tmp_called_name_36 );
        if ( tmp_right_name_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_44 );

            exception_lineno = 8161;

            goto frame_exception_exit_1;
        }
        tmp_left_name_43 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_44, tmp_right_name_35 );
        Py_DECREF( tmp_left_name_44 );
        Py_DECREF( tmp_right_name_35 );
        if ( tmp_left_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8161;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_36 );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_36 );
        }

        if ( tmp_mvar_value_39 == NULL )
        {
            Py_DECREF( tmp_left_name_43 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_36" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8162;

            goto frame_exception_exit_1;
        }

        tmp_called_name_37 = tmp_mvar_value_39;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8162;
        tmp_right_name_36 = CALL_FUNCTION_NO_ARGS( tmp_called_name_37 );
        if ( tmp_right_name_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_43 );

            exception_lineno = 8162;

            goto frame_exception_exit_1;
        }
        tmp_left_name_42 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_43, tmp_right_name_36 );
        Py_DECREF( tmp_left_name_43 );
        Py_DECREF( tmp_right_name_36 );
        if ( tmp_left_name_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8162;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_37 );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_37 );
        }

        if ( tmp_mvar_value_40 == NULL )
        {
            Py_DECREF( tmp_left_name_42 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_37" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8163;

            goto frame_exception_exit_1;
        }

        tmp_called_name_38 = tmp_mvar_value_40;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8163;
        tmp_right_name_37 = CALL_FUNCTION_NO_ARGS( tmp_called_name_38 );
        if ( tmp_right_name_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_42 );

            exception_lineno = 8163;

            goto frame_exception_exit_1;
        }
        tmp_left_name_41 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_42, tmp_right_name_37 );
        Py_DECREF( tmp_left_name_42 );
        Py_DECREF( tmp_right_name_37 );
        if ( tmp_left_name_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8163;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_38 );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_38 );
        }

        if ( tmp_mvar_value_41 == NULL )
        {
            Py_DECREF( tmp_left_name_41 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_38" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8164;

            goto frame_exception_exit_1;
        }

        tmp_called_name_39 = tmp_mvar_value_41;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8164;
        tmp_right_name_38 = CALL_FUNCTION_NO_ARGS( tmp_called_name_39 );
        if ( tmp_right_name_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_41 );

            exception_lineno = 8164;

            goto frame_exception_exit_1;
        }
        tmp_left_name_40 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_41, tmp_right_name_38 );
        Py_DECREF( tmp_left_name_41 );
        Py_DECREF( tmp_right_name_38 );
        if ( tmp_left_name_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8164;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_39 );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_39 );
        }

        if ( tmp_mvar_value_42 == NULL )
        {
            Py_DECREF( tmp_left_name_40 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_39" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8165;

            goto frame_exception_exit_1;
        }

        tmp_called_name_40 = tmp_mvar_value_42;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8165;
        tmp_right_name_39 = CALL_FUNCTION_NO_ARGS( tmp_called_name_40 );
        if ( tmp_right_name_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_40 );

            exception_lineno = 8165;

            goto frame_exception_exit_1;
        }
        tmp_left_name_39 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_40, tmp_right_name_39 );
        Py_DECREF( tmp_left_name_40 );
        Py_DECREF( tmp_right_name_39 );
        if ( tmp_left_name_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8165;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_40 );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_40 );
        }

        if ( tmp_mvar_value_43 == NULL )
        {
            Py_DECREF( tmp_left_name_39 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_40" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8166;

            goto frame_exception_exit_1;
        }

        tmp_called_name_41 = tmp_mvar_value_43;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8166;
        tmp_right_name_40 = CALL_FUNCTION_NO_ARGS( tmp_called_name_41 );
        if ( tmp_right_name_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_39 );

            exception_lineno = 8166;

            goto frame_exception_exit_1;
        }
        tmp_left_name_38 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_39, tmp_right_name_40 );
        Py_DECREF( tmp_left_name_39 );
        Py_DECREF( tmp_right_name_40 );
        if ( tmp_left_name_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8166;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_41 );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_41 );
        }

        if ( tmp_mvar_value_44 == NULL )
        {
            Py_DECREF( tmp_left_name_38 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_41" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8167;

            goto frame_exception_exit_1;
        }

        tmp_called_name_42 = tmp_mvar_value_44;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8167;
        tmp_right_name_41 = CALL_FUNCTION_NO_ARGS( tmp_called_name_42 );
        if ( tmp_right_name_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_38 );

            exception_lineno = 8167;

            goto frame_exception_exit_1;
        }
        tmp_left_name_37 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_38, tmp_right_name_41 );
        Py_DECREF( tmp_left_name_38 );
        Py_DECREF( tmp_right_name_41 );
        if ( tmp_left_name_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8167;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_42 );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_42 );
        }

        if ( tmp_mvar_value_45 == NULL )
        {
            Py_DECREF( tmp_left_name_37 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_42" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8168;

            goto frame_exception_exit_1;
        }

        tmp_called_name_43 = tmp_mvar_value_45;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8168;
        tmp_right_name_42 = CALL_FUNCTION_NO_ARGS( tmp_called_name_43 );
        if ( tmp_right_name_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_37 );

            exception_lineno = 8168;

            goto frame_exception_exit_1;
        }
        tmp_left_name_36 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_37, tmp_right_name_42 );
        Py_DECREF( tmp_left_name_37 );
        Py_DECREF( tmp_right_name_42 );
        if ( tmp_left_name_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8168;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_43 );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_43 );
        }

        if ( tmp_mvar_value_46 == NULL )
        {
            Py_DECREF( tmp_left_name_36 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_43" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8169;

            goto frame_exception_exit_1;
        }

        tmp_called_name_44 = tmp_mvar_value_46;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8169;
        tmp_right_name_43 = CALL_FUNCTION_NO_ARGS( tmp_called_name_44 );
        if ( tmp_right_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_36 );

            exception_lineno = 8169;

            goto frame_exception_exit_1;
        }
        tmp_left_name_35 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_36, tmp_right_name_43 );
        Py_DECREF( tmp_left_name_36 );
        Py_DECREF( tmp_right_name_43 );
        if ( tmp_left_name_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8169;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_44 );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_44 );
        }

        if ( tmp_mvar_value_47 == NULL )
        {
            Py_DECREF( tmp_left_name_35 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_44" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8170;

            goto frame_exception_exit_1;
        }

        tmp_called_name_45 = tmp_mvar_value_47;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8170;
        tmp_right_name_44 = CALL_FUNCTION_NO_ARGS( tmp_called_name_45 );
        if ( tmp_right_name_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_35 );

            exception_lineno = 8170;

            goto frame_exception_exit_1;
        }
        tmp_left_name_34 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_35, tmp_right_name_44 );
        Py_DECREF( tmp_left_name_35 );
        Py_DECREF( tmp_right_name_44 );
        if ( tmp_left_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8170;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_45 );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_45 );
        }

        if ( tmp_mvar_value_48 == NULL )
        {
            Py_DECREF( tmp_left_name_34 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_45" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8171;

            goto frame_exception_exit_1;
        }

        tmp_called_name_46 = tmp_mvar_value_48;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8171;
        tmp_right_name_45 = CALL_FUNCTION_NO_ARGS( tmp_called_name_46 );
        if ( tmp_right_name_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_34 );

            exception_lineno = 8171;

            goto frame_exception_exit_1;
        }
        tmp_left_name_33 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_34, tmp_right_name_45 );
        Py_DECREF( tmp_left_name_34 );
        Py_DECREF( tmp_right_name_45 );
        if ( tmp_left_name_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8171;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_46 );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_46 );
        }

        if ( tmp_mvar_value_49 == NULL )
        {
            Py_DECREF( tmp_left_name_33 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_46" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8172;

            goto frame_exception_exit_1;
        }

        tmp_called_name_47 = tmp_mvar_value_49;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8172;
        tmp_right_name_46 = CALL_FUNCTION_NO_ARGS( tmp_called_name_47 );
        if ( tmp_right_name_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_33 );

            exception_lineno = 8172;

            goto frame_exception_exit_1;
        }
        tmp_left_name_32 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_33, tmp_right_name_46 );
        Py_DECREF( tmp_left_name_33 );
        Py_DECREF( tmp_right_name_46 );
        if ( tmp_left_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8172;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_47 );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_47 );
        }

        if ( tmp_mvar_value_50 == NULL )
        {
            Py_DECREF( tmp_left_name_32 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_47" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8173;

            goto frame_exception_exit_1;
        }

        tmp_called_name_48 = tmp_mvar_value_50;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8173;
        tmp_right_name_47 = CALL_FUNCTION_NO_ARGS( tmp_called_name_48 );
        if ( tmp_right_name_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_32 );

            exception_lineno = 8173;

            goto frame_exception_exit_1;
        }
        tmp_left_name_31 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_32, tmp_right_name_47 );
        Py_DECREF( tmp_left_name_32 );
        Py_DECREF( tmp_right_name_47 );
        if ( tmp_left_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8173;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_48 );

        if (unlikely( tmp_mvar_value_51 == NULL ))
        {
            tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_48 );
        }

        if ( tmp_mvar_value_51 == NULL )
        {
            Py_DECREF( tmp_left_name_31 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_48" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8174;

            goto frame_exception_exit_1;
        }

        tmp_called_name_49 = tmp_mvar_value_51;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8174;
        tmp_right_name_48 = CALL_FUNCTION_NO_ARGS( tmp_called_name_49 );
        if ( tmp_right_name_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_31 );

            exception_lineno = 8174;

            goto frame_exception_exit_1;
        }
        tmp_left_name_30 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_31, tmp_right_name_48 );
        Py_DECREF( tmp_left_name_31 );
        Py_DECREF( tmp_right_name_48 );
        if ( tmp_left_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8174;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_49 );

        if (unlikely( tmp_mvar_value_52 == NULL ))
        {
            tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_49 );
        }

        if ( tmp_mvar_value_52 == NULL )
        {
            Py_DECREF( tmp_left_name_30 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_49" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8175;

            goto frame_exception_exit_1;
        }

        tmp_called_name_50 = tmp_mvar_value_52;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8175;
        tmp_right_name_49 = CALL_FUNCTION_NO_ARGS( tmp_called_name_50 );
        if ( tmp_right_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_30 );

            exception_lineno = 8175;

            goto frame_exception_exit_1;
        }
        tmp_left_name_29 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_30, tmp_right_name_49 );
        Py_DECREF( tmp_left_name_30 );
        Py_DECREF( tmp_right_name_49 );
        if ( tmp_left_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8175;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_50 );

        if (unlikely( tmp_mvar_value_53 == NULL ))
        {
            tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_50 );
        }

        if ( tmp_mvar_value_53 == NULL )
        {
            Py_DECREF( tmp_left_name_29 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_50" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8176;

            goto frame_exception_exit_1;
        }

        tmp_called_name_51 = tmp_mvar_value_53;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8176;
        tmp_right_name_50 = CALL_FUNCTION_NO_ARGS( tmp_called_name_51 );
        if ( tmp_right_name_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_29 );

            exception_lineno = 8176;

            goto frame_exception_exit_1;
        }
        tmp_left_name_28 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_29, tmp_right_name_50 );
        Py_DECREF( tmp_left_name_29 );
        Py_DECREF( tmp_right_name_50 );
        if ( tmp_left_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8176;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_51 );

        if (unlikely( tmp_mvar_value_54 == NULL ))
        {
            tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_51 );
        }

        if ( tmp_mvar_value_54 == NULL )
        {
            Py_DECREF( tmp_left_name_28 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_51" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8177;

            goto frame_exception_exit_1;
        }

        tmp_called_name_52 = tmp_mvar_value_54;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8177;
        tmp_right_name_51 = CALL_FUNCTION_NO_ARGS( tmp_called_name_52 );
        if ( tmp_right_name_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_28 );

            exception_lineno = 8177;

            goto frame_exception_exit_1;
        }
        tmp_left_name_27 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_28, tmp_right_name_51 );
        Py_DECREF( tmp_left_name_28 );
        Py_DECREF( tmp_right_name_51 );
        if ( tmp_left_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8177;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_52 );

        if (unlikely( tmp_mvar_value_55 == NULL ))
        {
            tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_52 );
        }

        if ( tmp_mvar_value_55 == NULL )
        {
            Py_DECREF( tmp_left_name_27 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_52" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8178;

            goto frame_exception_exit_1;
        }

        tmp_called_name_53 = tmp_mvar_value_55;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8178;
        tmp_right_name_52 = CALL_FUNCTION_NO_ARGS( tmp_called_name_53 );
        if ( tmp_right_name_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_27 );

            exception_lineno = 8178;

            goto frame_exception_exit_1;
        }
        tmp_left_name_26 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_27, tmp_right_name_52 );
        Py_DECREF( tmp_left_name_27 );
        Py_DECREF( tmp_right_name_52 );
        if ( tmp_left_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8178;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_53 );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_53 );
        }

        if ( tmp_mvar_value_56 == NULL )
        {
            Py_DECREF( tmp_left_name_26 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_53" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8179;

            goto frame_exception_exit_1;
        }

        tmp_called_name_54 = tmp_mvar_value_56;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8179;
        tmp_right_name_53 = CALL_FUNCTION_NO_ARGS( tmp_called_name_54 );
        if ( tmp_right_name_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_26 );

            exception_lineno = 8179;

            goto frame_exception_exit_1;
        }
        tmp_left_name_25 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_26, tmp_right_name_53 );
        Py_DECREF( tmp_left_name_26 );
        Py_DECREF( tmp_right_name_53 );
        if ( tmp_left_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8179;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_54 );

        if (unlikely( tmp_mvar_value_57 == NULL ))
        {
            tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_54 );
        }

        if ( tmp_mvar_value_57 == NULL )
        {
            Py_DECREF( tmp_left_name_25 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_54" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8180;

            goto frame_exception_exit_1;
        }

        tmp_called_name_55 = tmp_mvar_value_57;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8180;
        tmp_right_name_54 = CALL_FUNCTION_NO_ARGS( tmp_called_name_55 );
        if ( tmp_right_name_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_25 );

            exception_lineno = 8180;

            goto frame_exception_exit_1;
        }
        tmp_left_name_24 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_25, tmp_right_name_54 );
        Py_DECREF( tmp_left_name_25 );
        Py_DECREF( tmp_right_name_54 );
        if ( tmp_left_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8180;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_55 );

        if (unlikely( tmp_mvar_value_58 == NULL ))
        {
            tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_55 );
        }

        if ( tmp_mvar_value_58 == NULL )
        {
            Py_DECREF( tmp_left_name_24 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_55" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8181;

            goto frame_exception_exit_1;
        }

        tmp_called_name_56 = tmp_mvar_value_58;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8181;
        tmp_right_name_55 = CALL_FUNCTION_NO_ARGS( tmp_called_name_56 );
        if ( tmp_right_name_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_24 );

            exception_lineno = 8181;

            goto frame_exception_exit_1;
        }
        tmp_left_name_23 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_24, tmp_right_name_55 );
        Py_DECREF( tmp_left_name_24 );
        Py_DECREF( tmp_right_name_55 );
        if ( tmp_left_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8181;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_56 );

        if (unlikely( tmp_mvar_value_59 == NULL ))
        {
            tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_56 );
        }

        if ( tmp_mvar_value_59 == NULL )
        {
            Py_DECREF( tmp_left_name_23 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_56" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8182;

            goto frame_exception_exit_1;
        }

        tmp_called_name_57 = tmp_mvar_value_59;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8182;
        tmp_right_name_56 = CALL_FUNCTION_NO_ARGS( tmp_called_name_57 );
        if ( tmp_right_name_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_23 );

            exception_lineno = 8182;

            goto frame_exception_exit_1;
        }
        tmp_left_name_22 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_23, tmp_right_name_56 );
        Py_DECREF( tmp_left_name_23 );
        Py_DECREF( tmp_right_name_56 );
        if ( tmp_left_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8182;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_57 );

        if (unlikely( tmp_mvar_value_60 == NULL ))
        {
            tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_57 );
        }

        if ( tmp_mvar_value_60 == NULL )
        {
            Py_DECREF( tmp_left_name_22 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_57" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8183;

            goto frame_exception_exit_1;
        }

        tmp_called_name_58 = tmp_mvar_value_60;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8183;
        tmp_right_name_57 = CALL_FUNCTION_NO_ARGS( tmp_called_name_58 );
        if ( tmp_right_name_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_22 );

            exception_lineno = 8183;

            goto frame_exception_exit_1;
        }
        tmp_left_name_21 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_22, tmp_right_name_57 );
        Py_DECREF( tmp_left_name_22 );
        Py_DECREF( tmp_right_name_57 );
        if ( tmp_left_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8183;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_58 );

        if (unlikely( tmp_mvar_value_61 == NULL ))
        {
            tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_58 );
        }

        if ( tmp_mvar_value_61 == NULL )
        {
            Py_DECREF( tmp_left_name_21 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_58" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8184;

            goto frame_exception_exit_1;
        }

        tmp_called_name_59 = tmp_mvar_value_61;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8184;
        tmp_right_name_58 = CALL_FUNCTION_NO_ARGS( tmp_called_name_59 );
        if ( tmp_right_name_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_21 );

            exception_lineno = 8184;

            goto frame_exception_exit_1;
        }
        tmp_left_name_20 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_21, tmp_right_name_58 );
        Py_DECREF( tmp_left_name_21 );
        Py_DECREF( tmp_right_name_58 );
        if ( tmp_left_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8184;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_62 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_59 );

        if (unlikely( tmp_mvar_value_62 == NULL ))
        {
            tmp_mvar_value_62 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_59 );
        }

        if ( tmp_mvar_value_62 == NULL )
        {
            Py_DECREF( tmp_left_name_20 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_59" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8185;

            goto frame_exception_exit_1;
        }

        tmp_called_name_60 = tmp_mvar_value_62;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8185;
        tmp_right_name_59 = CALL_FUNCTION_NO_ARGS( tmp_called_name_60 );
        if ( tmp_right_name_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_20 );

            exception_lineno = 8185;

            goto frame_exception_exit_1;
        }
        tmp_left_name_19 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_20, tmp_right_name_59 );
        Py_DECREF( tmp_left_name_20 );
        Py_DECREF( tmp_right_name_59 );
        if ( tmp_left_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8185;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_63 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_60 );

        if (unlikely( tmp_mvar_value_63 == NULL ))
        {
            tmp_mvar_value_63 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_60 );
        }

        if ( tmp_mvar_value_63 == NULL )
        {
            Py_DECREF( tmp_left_name_19 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_60" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8186;

            goto frame_exception_exit_1;
        }

        tmp_called_name_61 = tmp_mvar_value_63;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8186;
        tmp_right_name_60 = CALL_FUNCTION_NO_ARGS( tmp_called_name_61 );
        if ( tmp_right_name_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_19 );

            exception_lineno = 8186;

            goto frame_exception_exit_1;
        }
        tmp_left_name_18 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_19, tmp_right_name_60 );
        Py_DECREF( tmp_left_name_19 );
        Py_DECREF( tmp_right_name_60 );
        if ( tmp_left_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8186;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_64 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_61 );

        if (unlikely( tmp_mvar_value_64 == NULL ))
        {
            tmp_mvar_value_64 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_61 );
        }

        if ( tmp_mvar_value_64 == NULL )
        {
            Py_DECREF( tmp_left_name_18 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_61" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8187;

            goto frame_exception_exit_1;
        }

        tmp_called_name_62 = tmp_mvar_value_64;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8187;
        tmp_right_name_61 = CALL_FUNCTION_NO_ARGS( tmp_called_name_62 );
        if ( tmp_right_name_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_18 );

            exception_lineno = 8187;

            goto frame_exception_exit_1;
        }
        tmp_left_name_17 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_18, tmp_right_name_61 );
        Py_DECREF( tmp_left_name_18 );
        Py_DECREF( tmp_right_name_61 );
        if ( tmp_left_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8187;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_65 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_62 );

        if (unlikely( tmp_mvar_value_65 == NULL ))
        {
            tmp_mvar_value_65 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_62 );
        }

        if ( tmp_mvar_value_65 == NULL )
        {
            Py_DECREF( tmp_left_name_17 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_62" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8188;

            goto frame_exception_exit_1;
        }

        tmp_called_name_63 = tmp_mvar_value_65;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8188;
        tmp_right_name_62 = CALL_FUNCTION_NO_ARGS( tmp_called_name_63 );
        if ( tmp_right_name_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_17 );

            exception_lineno = 8188;

            goto frame_exception_exit_1;
        }
        tmp_left_name_16 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_17, tmp_right_name_62 );
        Py_DECREF( tmp_left_name_17 );
        Py_DECREF( tmp_right_name_62 );
        if ( tmp_left_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8188;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_66 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_63 );

        if (unlikely( tmp_mvar_value_66 == NULL ))
        {
            tmp_mvar_value_66 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_63 );
        }

        if ( tmp_mvar_value_66 == NULL )
        {
            Py_DECREF( tmp_left_name_16 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_63" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8189;

            goto frame_exception_exit_1;
        }

        tmp_called_name_64 = tmp_mvar_value_66;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8189;
        tmp_right_name_63 = CALL_FUNCTION_NO_ARGS( tmp_called_name_64 );
        if ( tmp_right_name_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_16 );

            exception_lineno = 8189;

            goto frame_exception_exit_1;
        }
        tmp_left_name_15 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_16, tmp_right_name_63 );
        Py_DECREF( tmp_left_name_16 );
        Py_DECREF( tmp_right_name_63 );
        if ( tmp_left_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8189;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_67 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_64 );

        if (unlikely( tmp_mvar_value_67 == NULL ))
        {
            tmp_mvar_value_67 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_64 );
        }

        if ( tmp_mvar_value_67 == NULL )
        {
            Py_DECREF( tmp_left_name_15 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_64" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8190;

            goto frame_exception_exit_1;
        }

        tmp_called_name_65 = tmp_mvar_value_67;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8190;
        tmp_right_name_64 = CALL_FUNCTION_NO_ARGS( tmp_called_name_65 );
        if ( tmp_right_name_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_15 );

            exception_lineno = 8190;

            goto frame_exception_exit_1;
        }
        tmp_left_name_14 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_15, tmp_right_name_64 );
        Py_DECREF( tmp_left_name_15 );
        Py_DECREF( tmp_right_name_64 );
        if ( tmp_left_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8190;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_68 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_65 );

        if (unlikely( tmp_mvar_value_68 == NULL ))
        {
            tmp_mvar_value_68 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_65 );
        }

        if ( tmp_mvar_value_68 == NULL )
        {
            Py_DECREF( tmp_left_name_14 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_65" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8191;

            goto frame_exception_exit_1;
        }

        tmp_called_name_66 = tmp_mvar_value_68;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8191;
        tmp_right_name_65 = CALL_FUNCTION_NO_ARGS( tmp_called_name_66 );
        if ( tmp_right_name_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_14 );

            exception_lineno = 8191;

            goto frame_exception_exit_1;
        }
        tmp_left_name_13 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_14, tmp_right_name_65 );
        Py_DECREF( tmp_left_name_14 );
        Py_DECREF( tmp_right_name_65 );
        if ( tmp_left_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8191;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_69 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_66 );

        if (unlikely( tmp_mvar_value_69 == NULL ))
        {
            tmp_mvar_value_69 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_66 );
        }

        if ( tmp_mvar_value_69 == NULL )
        {
            Py_DECREF( tmp_left_name_13 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_66" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8192;

            goto frame_exception_exit_1;
        }

        tmp_called_name_67 = tmp_mvar_value_69;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8192;
        tmp_right_name_66 = CALL_FUNCTION_NO_ARGS( tmp_called_name_67 );
        if ( tmp_right_name_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_13 );

            exception_lineno = 8192;

            goto frame_exception_exit_1;
        }
        tmp_left_name_12 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_13, tmp_right_name_66 );
        Py_DECREF( tmp_left_name_13 );
        Py_DECREF( tmp_right_name_66 );
        if ( tmp_left_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8192;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_70 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_67 );

        if (unlikely( tmp_mvar_value_70 == NULL ))
        {
            tmp_mvar_value_70 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_67 );
        }

        if ( tmp_mvar_value_70 == NULL )
        {
            Py_DECREF( tmp_left_name_12 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_67" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8193;

            goto frame_exception_exit_1;
        }

        tmp_called_name_68 = tmp_mvar_value_70;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8193;
        tmp_right_name_67 = CALL_FUNCTION_NO_ARGS( tmp_called_name_68 );
        if ( tmp_right_name_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_12 );

            exception_lineno = 8193;

            goto frame_exception_exit_1;
        }
        tmp_left_name_11 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_12, tmp_right_name_67 );
        Py_DECREF( tmp_left_name_12 );
        Py_DECREF( tmp_right_name_67 );
        if ( tmp_left_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8193;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_71 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_68 );

        if (unlikely( tmp_mvar_value_71 == NULL ))
        {
            tmp_mvar_value_71 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_68 );
        }

        if ( tmp_mvar_value_71 == NULL )
        {
            Py_DECREF( tmp_left_name_11 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_68" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8194;

            goto frame_exception_exit_1;
        }

        tmp_called_name_69 = tmp_mvar_value_71;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8194;
        tmp_right_name_68 = CALL_FUNCTION_NO_ARGS( tmp_called_name_69 );
        if ( tmp_right_name_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_11 );

            exception_lineno = 8194;

            goto frame_exception_exit_1;
        }
        tmp_left_name_10 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_11, tmp_right_name_68 );
        Py_DECREF( tmp_left_name_11 );
        Py_DECREF( tmp_right_name_68 );
        if ( tmp_left_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8194;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_72 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_69 );

        if (unlikely( tmp_mvar_value_72 == NULL ))
        {
            tmp_mvar_value_72 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_69 );
        }

        if ( tmp_mvar_value_72 == NULL )
        {
            Py_DECREF( tmp_left_name_10 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_69" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8195;

            goto frame_exception_exit_1;
        }

        tmp_called_name_70 = tmp_mvar_value_72;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8195;
        tmp_right_name_69 = CALL_FUNCTION_NO_ARGS( tmp_called_name_70 );
        if ( tmp_right_name_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_10 );

            exception_lineno = 8195;

            goto frame_exception_exit_1;
        }
        tmp_left_name_9 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_10, tmp_right_name_69 );
        Py_DECREF( tmp_left_name_10 );
        Py_DECREF( tmp_right_name_69 );
        if ( tmp_left_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8195;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_73 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_70 );

        if (unlikely( tmp_mvar_value_73 == NULL ))
        {
            tmp_mvar_value_73 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_70 );
        }

        if ( tmp_mvar_value_73 == NULL )
        {
            Py_DECREF( tmp_left_name_9 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_70" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8196;

            goto frame_exception_exit_1;
        }

        tmp_called_name_71 = tmp_mvar_value_73;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8196;
        tmp_right_name_70 = CALL_FUNCTION_NO_ARGS( tmp_called_name_71 );
        if ( tmp_right_name_70 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_9 );

            exception_lineno = 8196;

            goto frame_exception_exit_1;
        }
        tmp_left_name_8 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_9, tmp_right_name_70 );
        Py_DECREF( tmp_left_name_9 );
        Py_DECREF( tmp_right_name_70 );
        if ( tmp_left_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8196;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_74 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_71 );

        if (unlikely( tmp_mvar_value_74 == NULL ))
        {
            tmp_mvar_value_74 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_71 );
        }

        if ( tmp_mvar_value_74 == NULL )
        {
            Py_DECREF( tmp_left_name_8 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_71" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8197;

            goto frame_exception_exit_1;
        }

        tmp_called_name_72 = tmp_mvar_value_74;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8197;
        tmp_right_name_71 = CALL_FUNCTION_NO_ARGS( tmp_called_name_72 );
        if ( tmp_right_name_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_8 );

            exception_lineno = 8197;

            goto frame_exception_exit_1;
        }
        tmp_left_name_7 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_8, tmp_right_name_71 );
        Py_DECREF( tmp_left_name_8 );
        Py_DECREF( tmp_right_name_71 );
        if ( tmp_left_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8197;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_75 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_72 );

        if (unlikely( tmp_mvar_value_75 == NULL ))
        {
            tmp_mvar_value_75 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_72 );
        }

        if ( tmp_mvar_value_75 == NULL )
        {
            Py_DECREF( tmp_left_name_7 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_72" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8198;

            goto frame_exception_exit_1;
        }

        tmp_called_name_73 = tmp_mvar_value_75;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8198;
        tmp_right_name_72 = CALL_FUNCTION_NO_ARGS( tmp_called_name_73 );
        if ( tmp_right_name_72 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_7 );

            exception_lineno = 8198;

            goto frame_exception_exit_1;
        }
        tmp_left_name_6 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_7, tmp_right_name_72 );
        Py_DECREF( tmp_left_name_7 );
        Py_DECREF( tmp_right_name_72 );
        if ( tmp_left_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8198;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_76 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_73 );

        if (unlikely( tmp_mvar_value_76 == NULL ))
        {
            tmp_mvar_value_76 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_73 );
        }

        if ( tmp_mvar_value_76 == NULL )
        {
            Py_DECREF( tmp_left_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_73" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8199;

            goto frame_exception_exit_1;
        }

        tmp_called_name_74 = tmp_mvar_value_76;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8199;
        tmp_right_name_73 = CALL_FUNCTION_NO_ARGS( tmp_called_name_74 );
        if ( tmp_right_name_73 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_6 );

            exception_lineno = 8199;

            goto frame_exception_exit_1;
        }
        tmp_left_name_5 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_6, tmp_right_name_73 );
        Py_DECREF( tmp_left_name_6 );
        Py_DECREF( tmp_right_name_73 );
        if ( tmp_left_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8199;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_77 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_74 );

        if (unlikely( tmp_mvar_value_77 == NULL ))
        {
            tmp_mvar_value_77 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_74 );
        }

        if ( tmp_mvar_value_77 == NULL )
        {
            Py_DECREF( tmp_left_name_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_74" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8200;

            goto frame_exception_exit_1;
        }

        tmp_called_name_75 = tmp_mvar_value_77;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8200;
        tmp_right_name_74 = CALL_FUNCTION_NO_ARGS( tmp_called_name_75 );
        if ( tmp_right_name_74 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_5 );

            exception_lineno = 8200;

            goto frame_exception_exit_1;
        }
        tmp_left_name_4 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_5, tmp_right_name_74 );
        Py_DECREF( tmp_left_name_5 );
        Py_DECREF( tmp_right_name_74 );
        if ( tmp_left_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8200;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_78 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_75 );

        if (unlikely( tmp_mvar_value_78 == NULL ))
        {
            tmp_mvar_value_78 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_75 );
        }

        if ( tmp_mvar_value_78 == NULL )
        {
            Py_DECREF( tmp_left_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_75" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8201;

            goto frame_exception_exit_1;
        }

        tmp_called_name_76 = tmp_mvar_value_78;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8201;
        tmp_right_name_75 = CALL_FUNCTION_NO_ARGS( tmp_called_name_76 );
        if ( tmp_right_name_75 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_4 );

            exception_lineno = 8201;

            goto frame_exception_exit_1;
        }
        tmp_left_name_3 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_4, tmp_right_name_75 );
        Py_DECREF( tmp_left_name_4 );
        Py_DECREF( tmp_right_name_75 );
        if ( tmp_left_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8201;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_79 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_76 );

        if (unlikely( tmp_mvar_value_79 == NULL ))
        {
            tmp_mvar_value_79 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_76 );
        }

        if ( tmp_mvar_value_79 == NULL )
        {
            Py_DECREF( tmp_left_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_76" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8202;

            goto frame_exception_exit_1;
        }

        tmp_called_name_77 = tmp_mvar_value_79;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8202;
        tmp_right_name_76 = CALL_FUNCTION_NO_ARGS( tmp_called_name_77 );
        if ( tmp_right_name_76 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_3 );

            exception_lineno = 8202;

            goto frame_exception_exit_1;
        }
        tmp_left_name_2 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_3, tmp_right_name_76 );
        Py_DECREF( tmp_left_name_3 );
        Py_DECREF( tmp_right_name_76 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8202;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_80 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_77 );

        if (unlikely( tmp_mvar_value_80 == NULL ))
        {
            tmp_mvar_value_80 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_77 );
        }

        if ( tmp_mvar_value_80 == NULL )
        {
            Py_DECREF( tmp_left_name_2 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_77" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8203;

            goto frame_exception_exit_1;
        }

        tmp_called_name_78 = tmp_mvar_value_80;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8203;
        tmp_right_name_77 = CALL_FUNCTION_NO_ARGS( tmp_called_name_78 );
        if ( tmp_right_name_77 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_2 );

            exception_lineno = 8203;

            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_77 );
        Py_DECREF( tmp_left_name_2 );
        Py_DECREF( tmp_right_name_77 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8203;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_81 = GET_STRING_DICT_VALUE( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain__seg_78 );

        if (unlikely( tmp_mvar_value_81 == NULL ))
        {
            tmp_mvar_value_81 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__seg_78 );
        }

        if ( tmp_mvar_value_81 == NULL )
        {
            Py_DECREF( tmp_left_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_seg_78" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 8204;

            goto frame_exception_exit_1;
        }

        tmp_called_name_79 = tmp_mvar_value_81;
        frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame.f_lineno = 8204;
        tmp_right_name_78 = CALL_FUNCTION_NO_ARGS( tmp_called_name_79 );
        if ( tmp_right_name_78 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 8204;

            goto frame_exception_exit_1;
        }
        tmp_tuple_arg_1 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_78 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_78 );
        if ( tmp_tuple_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8204;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_84 = PySequence_Tuple( tmp_tuple_arg_1 );
        Py_DECREF( tmp_tuple_arg_1 );
        if ( tmp_assign_source_84 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8125;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_idna$uts46data, (Nuitka_StringObject *)const_str_plain_uts46data, tmp_assign_source_84 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_94d7e88b3efd3989bb5abf95c99f44bc );
#endif
    popFrameStack();

    assertFrameObject( frame_94d7e88b3efd3989bb5abf95c99f44bc );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_94d7e88b3efd3989bb5abf95c99f44bc );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_94d7e88b3efd3989bb5abf95c99f44bc, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_94d7e88b3efd3989bb5abf95c99f44bc->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_94d7e88b3efd3989bb5abf95c99f44bc, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_idna$uts46data );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
