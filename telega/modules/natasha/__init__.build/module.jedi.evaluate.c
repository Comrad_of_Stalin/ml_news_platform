/* Generated code for Python module 'jedi.evaluate'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jedi$evaluate" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jedi$evaluate;
PyDictObject *moduledict_jedi$evaluate;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_eb61ded08f95ef617dbf07a9230a3e2a;
extern PyObject *const_str_plain_evaluator_function_cache;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain___name__;
extern PyObject *const_dict_empty;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain_object;
static PyObject *const_tuple_e5482bbe7f665d5acc1634c0bcc5874e_tuple;
extern PyObject *const_str_plain_default;
extern PyObject *const_str_plain_node_is_context;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_end_pos;
extern PyObject *const_str_plain_inferred_element_counts;
extern PyObject *const_tuple_none_none_none_tuple;
extern PyObject *const_int_pos_16;
extern PyObject *const_str_plain_eval_element;
extern PyObject *const_str_plain_is_scope;
static PyObject *const_str_digest_0d48b2b4f5b9085e0fa7992bfcc8735d;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_evaluate;
extern PyObject *const_str_angle_genexpr;
static PyObject *const_str_digest_e1ac7c5ecbc2db84fb55808bd519106e;
extern PyObject *const_str_plain_BoundMethod;
extern PyObject *const_str_plain_string_name;
extern PyObject *const_str_plain_get_grammar;
extern PyObject *const_str_plain__get_sys_path;
extern PyObject *const_str_plain_goto_definitions;
extern PyObject *const_str_plain_path;
static PyObject *const_str_plain_str_element_names;
extern PyObject *const_str_digest_74a64e2326d0e94475afe0623d07b03d;
extern PyObject *const_str_plain_deep_ast_copy;
extern PyObject *const_str_plain_infer_import;
extern PyObject *const_str_plain_name;
static PyObject *const_tuple_str_plain_power_str_plain_trailer_tuple;
static PyObject *const_str_plain__eval_element_if_evaluated;
extern PyObject *const_str_plain_scope_node;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_goto;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_unite;
extern PyObject *const_str_digest_c075052d723d6707083e869a0e3659bb;
static PyObject *const_str_plain__eval_element_cached;
extern PyObject *const_str_plain_python_bytes_to_unicode;
extern PyObject *const_str_plain_pop;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_code;
extern PyObject *const_tuple_str_plain_helpers_tuple;
static PyObject *const_tuple_2b54426e6fec59b7053349007edebb94_tuple;
static PyObject *const_str_digest_387b7fddc472ee6c95ed1bbe113171cf;
extern PyObject *const_int_pos_4;
extern PyObject *const_tuple_str_plain_python_bytes_to_unicode_tuple;
extern PyObject *const_str_plain_eval_trailer;
extern PyObject *const_str_plain_type;
extern PyObject *const_str_plain_base_context;
extern PyObject *const_str_plain_comp_for;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_for_stmt;
static PyObject *const_str_digest_59db0359c689702cccb495bcc3f37c20;
extern PyObject *const_str_plain_arglist;
extern PyObject *const_str_plain_class_context;
extern PyObject *const_str_digest_236865aa7b8536387f5cdcdb248aea1d;
static PyObject *const_tuple_none_none_str_digest_c075052d723d6707083e869a0e3659bb_tuple;
extern PyObject *const_tuple_str_plain_compiled_tuple;
extern PyObject *const_str_plain_definitions;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_replace;
static PyObject *const_str_plain_if_stmt_test;
extern PyObject *const_str_plain_evaluate_call_of_leaf;
extern PyObject *const_str_plain_jedi;
extern PyObject *const_tuple_str_plain_TreeNameDefinition_str_plain_ParamName_tuple;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_digest_0c6a1637c5fdafe468f822c51ad84a09;
extern PyObject *const_str_plain_recursion_detector;
extern PyObject *const_str_plain_kwargs;
extern PyObject *const_str_plain_check_tuple_assignments;
extern PyObject *const_tuple_str_plain_imports_tuple;
extern PyObject *const_str_plain_property;
extern PyObject *const_str_plain_decorator;
extern PyObject *const_str_plain_search_global;
extern PyObject *const_str_plain_get_definition;
extern PyObject *const_str_plain_index;
extern PyObject *const_str_plain_infer_enabled;
extern PyObject *const_str_digest_c32a307eb4cbe1d85147469cdfd3f906;
extern PyObject *const_str_plain_await;
extern PyObject *const_str_plain_position;
extern PyObject *const_str_plain_helpers;
extern PyObject *const_slice_int_pos_1_none_none;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_tree_node;
extern PyObject *const_tuple_false_false_tuple;
extern PyObject *const_str_plain_parso;
extern PyObject *const_str_plain_from_context;
extern PyObject *const_str_plain_cn;
extern PyObject *const_str_chr_61;
static PyObject *const_str_plain_is_funcdef;
extern PyObject *const_str_plain_func;
extern PyObject *const_str_plain_RecursionDetector;
extern PyObject *const_str_plain_builtins_module;
extern PyObject *const_str_plain_read;
extern PyObject *const_str_plain_children;
extern PyObject *const_str_plain___orig_bases__;
static PyObject *const_str_digest_0ad84cb297d873a7a5c80311221c5f60;
extern PyObject *const_str_plain_import_name;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_dict_c420771dfdd3ba8cae6376435c084e1a;
extern PyObject *const_str_plain_classdef;
extern PyObject *const_str_plain_n;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_dotted_name;
extern PyObject *const_tuple_str_plain_evaluator_function_cache_tuple;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_plain_context;
static PyObject *const_str_digest_126bdc407864ab2aedc0013c87731837;
extern PyObject *const_str_plain_enumerate;
static PyObject *const_str_plain_if_name;
extern PyObject *const_str_plain_errors;
extern PyObject *const_str_plain_power;
extern PyObject *const_str_plain_grammar;
extern PyObject *const_str_plain_search_ancestor;
static PyObject *const_str_plain_new_name_dicts;
extern PyObject *const_str_plain_is_analysis;
extern PyObject *const_str_plain_if_stmt;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_plain_f;
extern PyObject *const_str_plain_memoize_cache;
extern PyObject *const_str_plain_recursion;
extern PyObject *const_str_plain_expr_stmt;
static PyObject *const_str_plain_def_;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_get_names_of_node;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_module_cache;
extern PyObject *const_dict_796c375bab0f2dff97770527fabdc7a2;
extern PyObject *const_str_plain_parser_utils;
extern PyObject *const_str_plain_execution_recursion_detector;
static PyObject *const_tuple_728cd20ef98f8748f06808da502e6c90_tuple;
extern PyObject *const_str_digest_785ebe15053a4fa731c98e296de8edd7;
extern PyObject *const_str_plain_load_grammar;
static PyObject *const_str_plain_allow_different_encoding;
extern PyObject *const_str_plain_get_evaluator_subprocess;
extern PyObject *const_str_plain_copy;
extern PyObject *const_str_plain_get_function_execution;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_parse;
extern PyObject *const_str_plain_import_from;
static PyObject *const_str_digest_87d77a9776f0dd25e126e9a6ca99fb9d;
extern PyObject *const_str_plain_get_environment;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_version;
extern PyObject *const_str_plain_dynamic_params_depth;
extern PyObject *const_tuple_none_none_tuple;
extern PyObject *const_str_plain_result;
extern PyObject *const_str_plain_analysis;
extern PyObject *const_str_plain_lambdef;
extern PyObject *const_str_plain_AnonymousInstance;
static PyObject *const_tuple_str_plain_CompForContext_tuple;
extern PyObject *const_str_plain_eval_expr_stmt;
extern PyObject *const_str_plain_tree;
extern PyObject *const_str_plain_base_node;
extern PyObject *const_str_plain_import_name_always;
extern PyObject *const_list_dict_empty_list;
extern PyObject *const_int_neg_1;
static PyObject *const_tuple_87bb68f9132f41b698d8e8be88202bce_tuple;
static PyObject *const_str_digest_1d3c1156eed89cb991ed261e39b0f847;
extern PyObject *const_str_plain_from_comp_for;
extern PyObject *const_str_plain_compiled_subprocess;
extern PyObject *const_str_plain_name_dict;
extern PyObject *const_str_digest_3e1604d04ce104740ab4a0cd2cf306db;
extern PyObject *const_str_plain_cut_own_trailer;
static PyObject *const_tuple_a0d826e7a9de91577fc56f9543c6b3fd_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_iterate_contexts;
extern PyObject *const_str_plain_predefine_names;
static PyObject *const_tuple_cec70f1fa2ae7e584824a9fedf6caf5c_tuple;
static PyObject *const_str_plain_original_name_dicts;
extern PyObject *const_str_plain_definition;
extern PyObject *const_str_plain_create_context;
extern PyObject *const_str_plain_node;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
static PyObject *const_str_digest_3bf0f9fb9b6b71031574a6f750b452a3;
extern PyObject *const_tuple_str_plain_import_from_str_plain_import_name_tuple;
static PyObject *const_str_plain_element_names;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_NUITKA_PACKAGE_jedi;
extern PyObject *const_str_plain_parent;
extern PyObject *const_str_plain_environment;
static PyObject *const_str_digest_cf63cd7b5c10423b185dcfbc1879e8c4;
extern PyObject *const_str_plain_NO_CONTEXTS;
extern PyObject *const_str_plain_script_path;
extern PyObject *const_str_plain_node_is_object;
extern PyObject *const_str_plain___path__;
extern PyObject *const_tuple_empty;
static PyObject *const_str_digest_d5eb4398d75eba489f30149dd13bbd58;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
extern PyObject *const_tuple_str_plain_e_tuple;
extern PyObject *const_str_plain_funcdef;
static PyObject *const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple;
static PyObject *const_tuple_b8159c278fcd64fe119d68019c1f221e_tuple;
static PyObject *const_str_plain_is_simple_name;
extern PyObject *const_str_plain_get_special_object;
extern PyObject *const_str_plain_get_sys_path;
static PyObject *const_tuple_7b3742dc867bcc357319bd1a86c29de0_tuple;
extern PyObject *const_str_plain_trailer;
extern PyObject *const_tuple_none_true_false_tuple;
extern PyObject *const_str_plain_is_nested;
extern PyObject *const_str_plain_parse_and_get_code;
extern PyObject *const_tuple_f0f15781590b4fd8bce07dd7072b2735_tuple;
extern PyObject *const_str_plain_CompForContext;
extern PyObject *const_str_plain_dirname;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain_dbg;
extern PyObject *const_str_plain_debug;
extern PyObject *const_str_plain_is_goto;
static PyObject *const_str_digest_8416bd7f7b8d9d064776ccaff84429d1;
static PyObject *const_str_digest_f1486e7943f41f76e310574f0b0f1191;
extern PyObject *const_str_plain_predefined_names;
static PyObject *const_dict_7435f891c23645b1e0e50cd95b5f0389;
extern PyObject *const_str_plain_compiled;
extern PyObject *const_str_plain_Evaluator;
extern PyObject *const_str_plain_argument;
extern PyObject *const_str_plain_function;
extern PyObject *const_str_digest_5bfaf90dbd407b4fc29090c8f6415242;
static PyObject *const_str_plain_compiled_cache;
extern PyObject *const_str_plain_latest_grammar;
extern PyObject *const_tuple_str_plain_funcdef_str_plain_lambdef_tuple;
static PyObject *const_str_plain_parent_scope;
extern PyObject *const_str_plain_get;
extern PyObject *const_str_plain_dictorsetmaker;
extern PyObject *const_str_plain_ParamName;
extern PyObject *const_str_plain_klass;
extern PyObject *const_str_plain_FunctionContext;
extern PyObject *const_str_plain_metaclass;
static PyObject *const_str_plain_for_types;
static PyObject *const_tuple_str_plain_self_str_plain_context_str_plain_element_tuple;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_dict_32fe95870011cc07e909fb05ac91cbab;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain___exit__;
extern PyObject *const_tuple_str_plain_self_str_plain_args_str_plain_kwargs_tuple;
extern PyObject *const_str_plain_ContextualizedName;
static PyObject *const_str_plain_container_types;
extern PyObject *const_str_plain_param;
extern PyObject *const_str_plain_instance;
static PyObject *const_str_digest_dfa371e33dd702765033c7673aa2ad23;
extern PyObject *const_str_plain___enter__;
extern PyObject *const_str_plain_type_;
extern PyObject *const_str_plain_join;
static PyObject *const_str_digest_e2ef0d02bd7e929c6eac3df1e414ebca;
static PyObject *const_tuple_str_plain_node_str_plain_n_tuple;
static PyObject *const_str_plain_from_scope_node;
static PyObject *const_str_plain_child_is_funcdef;
extern PyObject *const_str_dot;
extern PyObject *const_tuple_str_plain_unite_tuple;
extern PyObject *const_str_plain_environ;
static PyObject *const_str_digest_a8ac27e834fc869b2d620e1cc177f3c5;
extern PyObject *const_str_plain_reset_recursion_limitations;
static PyObject *const_tuple_str_plain_argument_str_plain_testlist_comp_tuple;
static PyObject *const_str_digest_28cc99970a69c65c35603c2837677ddc;
extern PyObject *const_str_plain_ExecutionRecursionDetector;
extern PyObject *const_str_plain_ModuleCache;
extern PyObject *const_str_plain_rb;
extern PyObject *const_str_plain_mixed_cache;
extern PyObject *const_tuple_str_plain_debug_tuple;
extern PyObject *const_str_plain_BUILTINS;
extern PyObject *const_str_plain_get_parent_scope;
static PyObject *const_str_plain_predefined_if_name_dict;
static PyObject *const_str_digest_77ddca8ef5b9b2c4ddac9b11d8ce85e1;
extern PyObject *const_str_plain_element;
extern PyObject *const_str_digest_cd4a43d41990fe11ed21029f9707bb7b;
extern PyObject *const_str_plain_NUITKA_PACKAGE_jedi_evaluate;
extern PyObject *const_str_plain_eval_node;
extern PyObject *const_str_plain_start_pos;
extern PyObject *const_str_plain_ContextualizedNode;
extern PyObject *const_tuple_str_plain_parser_utils_tuple;
extern PyObject *const_str_plain_project;
extern PyObject *const_str_plain_encoding;
extern PyObject *const_str_plain_ClassContext;
static PyObject *const_str_digest_317b70c9d1c8e8e333fd4abb8e1dfd47;
extern PyObject *const_str_plain_get_param_names;
static PyObject *const_str_digest_f0c4d91ae1589d7e44e5490f7304cdd0;
static PyObject *const_tuple_str_plain_if_stmt_str_plain_for_stmt_tuple;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_imports;
extern PyObject *const_str_plain_TreeNameDefinition;
extern PyObject *const_str_digest_c3884810e9ca35af32bf1ca927da66bd;
extern PyObject *const_tuple_str_plain_funcdef_str_plain_classdef_tuple;
extern PyObject *const_tuple_str_plain_tree_tuple;
static PyObject *const_tuple_1fb1936ccc202489519ba0d22191373f_tuple;
extern PyObject *const_slice_int_pos_1_int_pos_4_none;
extern PyObject *const_str_plain_ContextSet;
static PyObject *const_tuple_16551354c56ef38b8e132ec98bb0851d_tuple;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain_name_context;
static PyObject *const_str_digest_5d7df07faa87c6bbb70540a2ea005867;
static PyObject *const_str_plain_access_cache;
static PyObject *const_tuple_070dc7245afeae0cb5fe183113521768_tuple;
extern PyObject *const_str_plain_c_node;
extern PyObject *const_int_pos_3;
extern PyObject *const_str_plain_self;
static PyObject *const_str_plain_if_names;
extern PyObject *const_str_digest_3d8c90d9a9bb20202fbddcdf6f6b7d19;
static PyObject *const_str_plain_name_dicts;
extern PyObject *const_str_plain_testlist_comp;
static PyObject *const_str_digest_f2b6cee7509904702185a89cc332ceaa;
extern PyObject *const_str_plain_parent_context;
extern PyObject *const_tuple_f5dfc96f66e16a3f34903f1a50794749_tuple;
extern PyObject *const_str_plain_py__getattribute__;
static PyObject *const_tuple_c0e2edd1e7f598687121f66b43e217b3_tuple;
extern PyObject *const_tuple_str_plain_recursion_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_eb61ded08f95ef617dbf07a9230a3e2a = UNSTREAM_STRING_ASCII( &constant_bin[ 959553 ], 22, 0 );
    const_tuple_e5482bbe7f665d5acc1634c0bcc5874e_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_e5482bbe7f665d5acc1634c0bcc5874e_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_e5482bbe7f665d5acc1634c0bcc5874e_tuple, 1, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    const_str_plain_str_element_names = UNSTREAM_STRING_ASCII( &constant_bin[ 959575 ], 17, 1 );
    PyTuple_SET_ITEM( const_tuple_e5482bbe7f665d5acc1634c0bcc5874e_tuple, 2, const_str_plain_str_element_names ); Py_INCREF( const_str_plain_str_element_names );
    const_str_digest_0d48b2b4f5b9085e0fa7992bfcc8735d = UNSTREAM_STRING_ASCII( &constant_bin[ 959592 ], 14, 0 );
    const_str_digest_e1ac7c5ecbc2db84fb55808bd519106e = UNSTREAM_STRING_ASCII( &constant_bin[ 959606 ], 46, 0 );
    const_tuple_str_plain_power_str_plain_trailer_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_power_str_plain_trailer_tuple, 0, const_str_plain_power ); Py_INCREF( const_str_plain_power );
    PyTuple_SET_ITEM( const_tuple_str_plain_power_str_plain_trailer_tuple, 1, const_str_plain_trailer ); Py_INCREF( const_str_plain_trailer );
    const_str_plain__eval_element_if_evaluated = UNSTREAM_STRING_ASCII( &constant_bin[ 959652 ], 26, 1 );
    const_str_plain__eval_element_cached = UNSTREAM_STRING_ASCII( &constant_bin[ 959678 ], 20, 1 );
    const_tuple_2b54426e6fec59b7053349007edebb94_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_2b54426e6fec59b7053349007edebb94_tuple, 0, const_str_plain_ContextualizedName ); Py_INCREF( const_str_plain_ContextualizedName );
    PyTuple_SET_ITEM( const_tuple_2b54426e6fec59b7053349007edebb94_tuple, 1, const_str_plain_ContextualizedNode ); Py_INCREF( const_str_plain_ContextualizedNode );
    PyTuple_SET_ITEM( const_tuple_2b54426e6fec59b7053349007edebb94_tuple, 2, const_str_plain_ContextSet ); Py_INCREF( const_str_plain_ContextSet );
    PyTuple_SET_ITEM( const_tuple_2b54426e6fec59b7053349007edebb94_tuple, 3, const_str_plain_NO_CONTEXTS ); Py_INCREF( const_str_plain_NO_CONTEXTS );
    PyTuple_SET_ITEM( const_tuple_2b54426e6fec59b7053349007edebb94_tuple, 4, const_str_plain_iterate_contexts ); Py_INCREF( const_str_plain_iterate_contexts );
    const_str_digest_387b7fddc472ee6c95ed1bbe113171cf = UNSTREAM_STRING_ASCII( &constant_bin[ 959698 ], 41, 0 );
    const_str_digest_59db0359c689702cccb495bcc3f37c20 = UNSTREAM_STRING_ASCII( &constant_bin[ 959739 ], 18, 0 );
    const_tuple_none_none_str_digest_c075052d723d6707083e869a0e3659bb_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_none_none_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 0, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 1, Py_None ); Py_INCREF( Py_None );
    PyTuple_SET_ITEM( const_tuple_none_none_str_digest_c075052d723d6707083e869a0e3659bb_tuple, 2, const_str_digest_c075052d723d6707083e869a0e3659bb ); Py_INCREF( const_str_digest_c075052d723d6707083e869a0e3659bb );
    const_str_plain_if_stmt_test = UNSTREAM_STRING_ASCII( &constant_bin[ 959757 ], 12, 1 );
    const_str_plain_is_funcdef = UNSTREAM_STRING_ASCII( &constant_bin[ 959769 ], 10, 1 );
    const_str_digest_0ad84cb297d873a7a5c80311221c5f60 = UNSTREAM_STRING_ASCII( &constant_bin[ 959779 ], 30, 0 );
    const_str_digest_126bdc407864ab2aedc0013c87731837 = UNSTREAM_STRING_ASCII( &constant_bin[ 959809 ], 36, 0 );
    const_str_plain_if_name = UNSTREAM_STRING_ASCII( &constant_bin[ 959845 ], 7, 1 );
    const_str_plain_new_name_dicts = UNSTREAM_STRING_ASCII( &constant_bin[ 959852 ], 14, 1 );
    const_str_plain_def_ = UNSTREAM_STRING_ASCII( &constant_bin[ 190060 ], 4, 1 );
    const_tuple_728cd20ef98f8748f06808da502e6c90_tuple = PyTuple_New( 14 );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 0, const_str_plain_scope_node ); Py_INCREF( const_str_plain_scope_node );
    const_str_plain_child_is_funcdef = UNSTREAM_STRING_ASCII( &constant_bin[ 959866 ], 16, 1 );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 1, const_str_plain_child_is_funcdef ); Py_INCREF( const_str_plain_child_is_funcdef );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 2, const_str_plain_is_nested ); Py_INCREF( const_str_plain_is_nested );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 3, const_str_plain_node_is_object ); Py_INCREF( const_str_plain_node_is_object );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 4, const_str_plain_is_funcdef ); Py_INCREF( const_str_plain_is_funcdef );
    const_str_plain_parent_scope = UNSTREAM_STRING_ASCII( &constant_bin[ 959640 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 5, const_str_plain_parent_scope ); Py_INCREF( const_str_plain_parent_scope );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 6, const_str_plain_parent_context ); Py_INCREF( const_str_plain_parent_context );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 7, const_str_plain_func ); Py_INCREF( const_str_plain_func );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 8, const_str_plain_class_context ); Py_INCREF( const_str_plain_class_context );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 9, const_str_plain_base_node ); Py_INCREF( const_str_plain_base_node );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 10, const_str_plain_base_context ); Py_INCREF( const_str_plain_base_context );
    const_str_plain_from_scope_node = UNSTREAM_STRING_ASCII( &constant_bin[ 959882 ], 15, 1 );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 11, const_str_plain_from_scope_node ); Py_INCREF( const_str_plain_from_scope_node );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 12, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 13, const_str_plain_node ); Py_INCREF( const_str_plain_node );
    const_str_plain_allow_different_encoding = UNSTREAM_STRING_ASCII( &constant_bin[ 959897 ], 24, 1 );
    const_str_digest_87d77a9776f0dd25e126e9a6ca99fb9d = UNSTREAM_STRING_ASCII( &constant_bin[ 959921 ], 37, 0 );
    const_tuple_str_plain_CompForContext_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_CompForContext_tuple, 0, const_str_plain_CompForContext ); Py_INCREF( const_str_plain_CompForContext );
    const_tuple_87bb68f9132f41b698d8e8be88202bce_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_87bb68f9132f41b698d8e8be88202bce_tuple, 0, const_str_plain_ClassContext ); Py_INCREF( const_str_plain_ClassContext );
    PyTuple_SET_ITEM( const_tuple_87bb68f9132f41b698d8e8be88202bce_tuple, 1, const_str_plain_FunctionContext ); Py_INCREF( const_str_plain_FunctionContext );
    PyTuple_SET_ITEM( const_tuple_87bb68f9132f41b698d8e8be88202bce_tuple, 2, const_str_plain_AnonymousInstance ); Py_INCREF( const_str_plain_AnonymousInstance );
    PyTuple_SET_ITEM( const_tuple_87bb68f9132f41b698d8e8be88202bce_tuple, 3, const_str_plain_BoundMethod ); Py_INCREF( const_str_plain_BoundMethod );
    const_str_digest_1d3c1156eed89cb991ed261e39b0f847 = UNSTREAM_STRING_ASCII( &constant_bin[ 959958 ], 26, 0 );
    const_tuple_a0d826e7a9de91577fc56f9543c6b3fd_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_a0d826e7a9de91577fc56f9543c6b3fd_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_a0d826e7a9de91577fc56f9543c6b3fd_tuple, 1, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    PyTuple_SET_ITEM( const_tuple_a0d826e7a9de91577fc56f9543c6b3fd_tuple, 2, const_str_plain_path ); Py_INCREF( const_str_plain_path );
    PyTuple_SET_ITEM( const_tuple_a0d826e7a9de91577fc56f9543c6b3fd_tuple, 3, const_str_plain_encoding ); Py_INCREF( const_str_plain_encoding );
    PyTuple_SET_ITEM( const_tuple_a0d826e7a9de91577fc56f9543c6b3fd_tuple, 4, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_a0d826e7a9de91577fc56f9543c6b3fd_tuple, 5, const_str_plain_f ); Py_INCREF( const_str_plain_f );
    const_tuple_cec70f1fa2ae7e584824a9fedf6caf5c_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_cec70f1fa2ae7e584824a9fedf6caf5c_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_cec70f1fa2ae7e584824a9fedf6caf5c_tuple, 1, const_str_plain_project ); Py_INCREF( const_str_plain_project );
    PyTuple_SET_ITEM( const_tuple_cec70f1fa2ae7e584824a9fedf6caf5c_tuple, 2, const_str_plain_environment ); Py_INCREF( const_str_plain_environment );
    PyTuple_SET_ITEM( const_tuple_cec70f1fa2ae7e584824a9fedf6caf5c_tuple, 3, const_str_plain_script_path ); Py_INCREF( const_str_plain_script_path );
    const_str_plain_original_name_dicts = UNSTREAM_STRING_ASCII( &constant_bin[ 959984 ], 19, 1 );
    const_str_digest_3bf0f9fb9b6b71031574a6f750b452a3 = UNSTREAM_STRING_ASCII( &constant_bin[ 960003 ], 2698, 0 );
    const_str_plain_element_names = UNSTREAM_STRING_ASCII( &constant_bin[ 959579 ], 13, 1 );
    const_str_digest_cf63cd7b5c10423b185dcfbc1879e8c4 = UNSTREAM_STRING_ASCII( &constant_bin[ 962701 ], 75, 0 );
    const_str_digest_d5eb4398d75eba489f30149dd13bbd58 = UNSTREAM_STRING_ASCII( &constant_bin[ 959606 ], 24, 0 );
    const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple = PyTuple_New( 18 );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 1, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 2, const_str_plain_element ); Py_INCREF( const_str_plain_element );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 3, const_str_plain_if_stmt ); Py_INCREF( const_str_plain_if_stmt );
    const_str_plain_predefined_if_name_dict = UNSTREAM_STRING_ASCII( &constant_bin[ 962776 ], 23, 1 );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 4, const_str_plain_predefined_if_name_dict ); Py_INCREF( const_str_plain_predefined_if_name_dict );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 5, const_str_plain_if_stmt_test ); Py_INCREF( const_str_plain_if_stmt_test );
    const_str_plain_name_dicts = UNSTREAM_STRING_ASCII( &constant_bin[ 959856 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 6, const_str_plain_name_dicts ); Py_INCREF( const_str_plain_name_dicts );
    const_str_plain_if_names = UNSTREAM_STRING_ASCII( &constant_bin[ 962799 ], 8, 1 );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 7, const_str_plain_if_names ); Py_INCREF( const_str_plain_if_names );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 8, const_str_plain_element_names ); Py_INCREF( const_str_plain_element_names );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 9, const_str_plain_str_element_names ); Py_INCREF( const_str_plain_str_element_names );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 10, const_str_plain_if_name ); Py_INCREF( const_str_plain_if_name );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 11, const_str_plain_definitions ); Py_INCREF( const_str_plain_definitions );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 12, const_str_plain_original_name_dicts ); Py_INCREF( const_str_plain_original_name_dicts );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 13, const_str_plain_definition ); Py_INCREF( const_str_plain_definition );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 14, const_str_plain_new_name_dicts ); Py_INCREF( const_str_plain_new_name_dicts );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 15, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 16, const_str_plain_name_dict ); Py_INCREF( const_str_plain_name_dict );
    PyTuple_SET_ITEM( const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 17, const_str_plain_result ); Py_INCREF( const_str_plain_result );
    const_tuple_b8159c278fcd64fe119d68019c1f221e_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b8159c278fcd64fe119d68019c1f221e_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_b8159c278fcd64fe119d68019c1f221e_tuple, 1, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_b8159c278fcd64fe119d68019c1f221e_tuple, 2, const_str_plain_element ); Py_INCREF( const_str_plain_element );
    PyTuple_SET_ITEM( const_tuple_b8159c278fcd64fe119d68019c1f221e_tuple, 3, const_str_plain_parent ); Py_INCREF( const_str_plain_parent );
    PyTuple_SET_ITEM( const_tuple_b8159c278fcd64fe119d68019c1f221e_tuple, 4, const_str_plain_predefined_if_name_dict ); Py_INCREF( const_str_plain_predefined_if_name_dict );
    const_str_plain_is_simple_name = UNSTREAM_STRING_ASCII( &constant_bin[ 962807 ], 14, 1 );
    const_tuple_7b3742dc867bcc357319bd1a86c29de0_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_7b3742dc867bcc357319bd1a86c29de0_tuple, 0, const_str_digest_b9c4baf879ebd882d40843df3a4dead7 ); Py_INCREF( const_str_digest_b9c4baf879ebd882d40843df3a4dead7 );
    PyTuple_SET_ITEM( const_tuple_7b3742dc867bcc357319bd1a86c29de0_tuple, 1, const_str_plain_value ); Py_INCREF( const_str_plain_value );
    PyTuple_SET_ITEM( const_tuple_7b3742dc867bcc357319bd1a86c29de0_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_7b3742dc867bcc357319bd1a86c29de0_tuple, 3, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    const_str_digest_8416bd7f7b8d9d064776ccaff84429d1 = UNSTREAM_STRING_ASCII( &constant_bin[ 962821 ], 45, 0 );
    const_str_digest_f1486e7943f41f76e310574f0b0f1191 = UNSTREAM_STRING_ASCII( &constant_bin[ 962866 ], 25, 0 );
    const_dict_7435f891c23645b1e0e50cd95b5f0389 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_7435f891c23645b1e0e50cd95b5f0389, const_str_plain_is_goto, Py_True );
    assert( PyDict_Size( const_dict_7435f891c23645b1e0e50cd95b5f0389 ) == 1 );
    const_str_plain_compiled_cache = UNSTREAM_STRING_ASCII( &constant_bin[ 962891 ], 14, 1 );
    const_str_plain_for_types = UNSTREAM_STRING_ASCII( &constant_bin[ 962905 ], 9, 1 );
    const_tuple_str_plain_self_str_plain_context_str_plain_element_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_context_str_plain_element_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_context_str_plain_element_tuple, 1, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_context_str_plain_element_tuple, 2, const_str_plain_element ); Py_INCREF( const_str_plain_element );
    const_str_plain_container_types = UNSTREAM_STRING_ASCII( &constant_bin[ 962914 ], 15, 1 );
    const_str_digest_dfa371e33dd702765033c7673aa2ad23 = UNSTREAM_STRING_ASCII( &constant_bin[ 962929 ], 33, 0 );
    const_str_digest_e2ef0d02bd7e929c6eac3df1e414ebca = UNSTREAM_STRING_ASCII( &constant_bin[ 962962 ], 49, 0 );
    const_tuple_str_plain_node_str_plain_n_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_node_str_plain_n_tuple, 0, const_str_plain_node ); Py_INCREF( const_str_plain_node );
    PyTuple_SET_ITEM( const_tuple_str_plain_node_str_plain_n_tuple, 1, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    const_str_digest_a8ac27e834fc869b2d620e1cc177f3c5 = UNSTREAM_STRING_ASCII( &constant_bin[ 959698 ], 22, 0 );
    const_tuple_str_plain_argument_str_plain_testlist_comp_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_argument_str_plain_testlist_comp_tuple, 0, const_str_plain_argument ); Py_INCREF( const_str_plain_argument );
    PyTuple_SET_ITEM( const_tuple_str_plain_argument_str_plain_testlist_comp_tuple, 1, const_str_plain_testlist_comp ); Py_INCREF( const_str_plain_testlist_comp );
    const_str_digest_28cc99970a69c65c35603c2837677ddc = UNSTREAM_STRING_ASCII( &constant_bin[ 963011 ], 15, 0 );
    const_str_digest_77ddca8ef5b9b2c4ddac9b11d8ce85e1 = UNSTREAM_STRING_ASCII( &constant_bin[ 963026 ], 22, 0 );
    const_str_digest_317b70c9d1c8e8e333fd4abb8e1dfd47 = UNSTREAM_STRING_ASCII( &constant_bin[ 963048 ], 20, 0 );
    const_str_digest_f0c4d91ae1589d7e44e5490f7304cdd0 = UNSTREAM_STRING_ASCII( &constant_bin[ 963068 ], 28, 0 );
    const_tuple_str_plain_if_stmt_str_plain_for_stmt_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_if_stmt_str_plain_for_stmt_tuple, 0, const_str_plain_if_stmt ); Py_INCREF( const_str_plain_if_stmt );
    PyTuple_SET_ITEM( const_tuple_str_plain_if_stmt_str_plain_for_stmt_tuple, 1, const_str_plain_for_stmt ); Py_INCREF( const_str_plain_for_stmt );
    const_tuple_1fb1936ccc202489519ba0d22191373f_tuple = PyTuple_New( 10 );
    PyTuple_SET_ITEM( const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 1, const_str_plain_context ); Py_INCREF( const_str_plain_context );
    PyTuple_SET_ITEM( const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 2, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 3, const_str_plain_def_ ); Py_INCREF( const_str_plain_def_ );
    PyTuple_SET_ITEM( const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 4, const_str_plain_type_ ); Py_INCREF( const_str_plain_type_ );
    PyTuple_SET_ITEM( const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 5, const_str_plain_is_simple_name ); Py_INCREF( const_str_plain_is_simple_name );
    PyTuple_SET_ITEM( const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 6, const_str_plain_container_types ); Py_INCREF( const_str_plain_container_types );
    PyTuple_SET_ITEM( const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 7, const_str_plain_cn ); Py_INCREF( const_str_plain_cn );
    PyTuple_SET_ITEM( const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 8, const_str_plain_for_types ); Py_INCREF( const_str_plain_for_types );
    PyTuple_SET_ITEM( const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 9, const_str_plain_c_node ); Py_INCREF( const_str_plain_c_node );
    const_tuple_16551354c56ef38b8e132ec98bb0851d_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 963096 ], 201 );
    const_str_digest_5d7df07faa87c6bbb70540a2ea005867 = UNSTREAM_STRING_ASCII( &constant_bin[ 963297 ], 25, 0 );
    const_str_plain_access_cache = UNSTREAM_STRING_ASCII( &constant_bin[ 963322 ], 12, 1 );
    const_tuple_070dc7245afeae0cb5fe183113521768_tuple = PyTuple_New( 9 );
    PyTuple_SET_ITEM( const_tuple_070dc7245afeae0cb5fe183113521768_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_070dc7245afeae0cb5fe183113521768_tuple, 1, const_str_plain_base_context ); Py_INCREF( const_str_plain_base_context );
    PyTuple_SET_ITEM( const_tuple_070dc7245afeae0cb5fe183113521768_tuple, 2, const_str_plain_node ); Py_INCREF( const_str_plain_node );
    PyTuple_SET_ITEM( const_tuple_070dc7245afeae0cb5fe183113521768_tuple, 3, const_str_plain_node_is_context ); Py_INCREF( const_str_plain_node_is_context );
    PyTuple_SET_ITEM( const_tuple_070dc7245afeae0cb5fe183113521768_tuple, 4, const_str_plain_node_is_object ); Py_INCREF( const_str_plain_node_is_object );
    PyTuple_SET_ITEM( const_tuple_070dc7245afeae0cb5fe183113521768_tuple, 5, const_str_plain_parent_scope ); Py_INCREF( const_str_plain_parent_scope );
    PyTuple_SET_ITEM( const_tuple_070dc7245afeae0cb5fe183113521768_tuple, 6, const_str_plain_from_scope_node ); Py_INCREF( const_str_plain_from_scope_node );
    PyTuple_SET_ITEM( const_tuple_070dc7245afeae0cb5fe183113521768_tuple, 7, const_str_plain_base_node ); Py_INCREF( const_str_plain_base_node );
    PyTuple_SET_ITEM( const_tuple_070dc7245afeae0cb5fe183113521768_tuple, 8, const_str_plain_scope_node ); Py_INCREF( const_str_plain_scope_node );
    const_str_digest_f2b6cee7509904702185a89cc332ceaa = UNSTREAM_STRING_ASCII( &constant_bin[ 963334 ], 37, 0 );
    const_tuple_c0e2edd1e7f598687121f66b43e217b3_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_c0e2edd1e7f598687121f66b43e217b3_tuple, 0, const_str_plain_eval_trailer ); Py_INCREF( const_str_plain_eval_trailer );
    PyTuple_SET_ITEM( const_tuple_c0e2edd1e7f598687121f66b43e217b3_tuple, 1, const_str_plain_eval_expr_stmt ); Py_INCREF( const_str_plain_eval_expr_stmt );
    PyTuple_SET_ITEM( const_tuple_c0e2edd1e7f598687121f66b43e217b3_tuple, 2, const_str_plain_eval_node ); Py_INCREF( const_str_plain_eval_node );
    PyTuple_SET_ITEM( const_tuple_c0e2edd1e7f598687121f66b43e217b3_tuple, 3, const_str_plain_check_tuple_assignments ); Py_INCREF( const_str_plain_check_tuple_assignments );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jedi$evaluate( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_0a37e2250f2dffd74910d10ed3e9b15c;
static PyCodeObject *codeobj_ad55f0c204b0a557a78f19bf6dc913b3;
static PyCodeObject *codeobj_bcac2ed3740e56ca7545ffd0614eccad;
static PyCodeObject *codeobj_fdbc5100e91af914c95c4dd3a109a648;
static PyCodeObject *codeobj_e0a1c8056d1a3ed2db3342956d8927fb;
static PyCodeObject *codeobj_528e69644582a95e5c6c292b45da64dd;
static PyCodeObject *codeobj_f74afcd1307791ef9813981c113d2a3e;
static PyCodeObject *codeobj_a3a4783c51555953fecb657c25a9d529;
static PyCodeObject *codeobj_f4732f2d02c8981381ba2a86f8ad96ad;
static PyCodeObject *codeobj_2dc089a51f40a6a717e824f4d36286f4;
static PyCodeObject *codeobj_1abbc533a789ddedcff7fa702a374e16;
static PyCodeObject *codeobj_96fcd1e076c30934fa2f8245d9f4ab88;
static PyCodeObject *codeobj_c7a63dba09606a0f5e83c3d23b96e279;
static PyCodeObject *codeobj_3c082f47970dd06c3f9fdaf39b85b64f;
static PyCodeObject *codeobj_83ec4da756226c82e4def57f9169641d;
static PyCodeObject *codeobj_6236a960357bfdbc01ec332bbb5c85ed;
static PyCodeObject *codeobj_676dba2f48047b5bd58871ef44c2b334;
static PyCodeObject *codeobj_18dc33aae5596c8fe54321958a326ffe;
static PyCodeObject *codeobj_1b56503422e36b4df7737050f8cb3c09;
static PyCodeObject *codeobj_34366af6610c2510392812acd68c38c8;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_5d7df07faa87c6bbb70540a2ea005867 );
    codeobj_0a37e2250f2dffd74910d10ed3e9b15c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 161, const_tuple_e5482bbe7f665d5acc1634c0bcc5874e_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ad55f0c204b0a557a78f19bf6dc913b3 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 296, const_tuple_7b3742dc867bcc357319bd1a86c29de0_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_bcac2ed3740e56ca7545ffd0614eccad = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 303, const_tuple_7b3742dc867bcc357319bd1a86c29de0_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fdbc5100e91af914c95c4dd3a109a648 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 160, const_tuple_str_plain_e_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e0a1c8056d1a3ed2db3342956d8927fb = MAKE_CODEOBJ( module_filename_obj, const_str_digest_77ddca8ef5b9b2c4ddac9b11d8ce85e1, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_528e69644582a95e5c6c292b45da64dd = MAKE_CODEOBJ( module_filename_obj, const_str_plain_Evaluator, 88, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_f74afcd1307791ef9813981c113d2a3e = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 89, const_tuple_cec70f1fa2ae7e584824a9fedf6caf5c_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_a3a4783c51555953fecb657c25a9d529 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__eval_element_cached, 214, const_tuple_str_plain_self_str_plain_context_str_plain_element_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_f4732f2d02c8981381ba2a86f8ad96ad = MAKE_CODEOBJ( module_filename_obj, const_str_plain__eval_element_if_evaluated, 202, const_tuple_b8159c278fcd64fe119d68019c1f221e_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2dc089a51f40a6a717e824f4d36286f4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_builtins_module, 115, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_1abbc533a789ddedcff7fa702a374e16 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_create_context, 318, const_tuple_070dc7245afeae0cb5fe183113521768_tuple, 5, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_96fcd1e076c30934fa2f8245d9f4ab88 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_eval_element, 128, const_tuple_1bc799dc8fa9b9ca5b4d2082e4690603_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c7a63dba09606a0f5e83c3d23b96e279 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_from_scope_node, 334, const_tuple_728cd20ef98f8748f06808da502e6c90_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_3c082f47970dd06c3f9fdaf39b85b64f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_sys_path, 124, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_83ec4da756226c82e4def57f9169641d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_goto, 242, const_tuple_16551354c56ef38b8e132ec98bb0851d_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6236a960357bfdbc01ec332bbb5c85ed = MAKE_CODEOBJ( module_filename_obj, const_str_plain_goto_definitions, 218, const_tuple_1fb1936ccc202489519ba0d22191373f_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_676dba2f48047b5bd58871ef44c2b334 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_parent_scope, 319, const_tuple_str_plain_node_str_plain_n_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_18dc33aae5596c8fe54321958a326ffe = MAKE_CODEOBJ( module_filename_obj, const_str_plain_parse, 390, const_tuple_str_plain_self_str_plain_args_str_plain_kwargs_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARARGS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_1b56503422e36b4df7737050f8cb3c09 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_parse_and_get_code, 381, const_tuple_a0d826e7a9de91577fc56f9543c6b3fd_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_34366af6610c2510392812acd68c38c8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_reset_recursion_limitations, 120, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *jedi$evaluate$$$function_5_eval_element$$$genexpr_1_genexpr_maker( void );


static PyObject *jedi$evaluate$$$function_9_goto$$$genexpr_1_genexpr_maker( void );


static PyObject *jedi$evaluate$$$function_9_goto$$$genexpr_2_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_10_complex_call_helper_keywords_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_10_create_context( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_10_create_context$$$function_1_parent_scope(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_10_create_context$$$function_2_from_scope_node( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_11_parse_and_get_code( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_12_parse(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_1___init__( PyObject *defaults );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_2_builtins_module(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_3_reset_recursion_limitations(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_4_get_sys_path(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_5_eval_element(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_6__eval_element_if_evaluated(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_7__eval_element_cached(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_8_goto_definitions(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_9_goto(  );


// The module function definitions.
static PyObject *impl_jedi$evaluate$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_project = python_pars[ 1 ];
    PyObject *par_environment = python_pars[ 2 ];
    PyObject *par_script_path = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_f74afcd1307791ef9813981c113d2a3e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_f74afcd1307791ef9813981c113d2a3e = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f74afcd1307791ef9813981c113d2a3e, codeobj_f74afcd1307791ef9813981c113d2a3e, module_jedi$evaluate, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f74afcd1307791ef9813981c113d2a3e = cache_frame_f74afcd1307791ef9813981c113d2a3e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f74afcd1307791ef9813981c113d2a3e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f74afcd1307791ef9813981c113d2a3e ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_environment );
        tmp_compexpr_left_1 = par_environment;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_instance_1;
            CHECK_OBJECT( par_project );
            tmp_called_instance_1 = par_project;
            frame_f74afcd1307791ef9813981c113d2a3e->m_frame.f_lineno = 91;
            tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_environment );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 91;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_environment;
                assert( old != NULL );
                par_environment = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_environment );
        tmp_assattr_name_1 = par_environment;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_environment, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 92;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_script_path );
        tmp_assattr_name_2 = par_script_path;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_script_path, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_environment );
        tmp_called_instance_2 = par_environment;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        frame_f74afcd1307791ef9813981c113d2a3e->m_frame.f_lineno = 94;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_get_evaluator_subprocess, call_args );
        }

        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_compiled_subprocess, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 94;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_called_instance_3;
        PyObject *tmp_assattr_target_4;
        CHECK_OBJECT( par_environment );
        tmp_called_instance_3 = par_environment;
        frame_f74afcd1307791ef9813981c113d2a3e->m_frame.f_lineno = 95;
        tmp_assattr_name_4 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_grammar );
        if ( tmp_assattr_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_grammar, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_assattr_target_5;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_parso );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parso );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parso" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_load_grammar );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_796c375bab0f2dff97770527fabdc7a2 );
        frame_f74afcd1307791ef9813981c113d2a3e->m_frame.f_lineno = 97;
        tmp_assattr_name_5 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assattr_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_latest_grammar, tmp_assattr_name_5 );
        Py_DECREF( tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_assattr_target_6;
        tmp_assattr_name_6 = PyDict_New();
        CHECK_OBJECT( par_self );
        tmp_assattr_target_6 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain_memoize_cache, tmp_assattr_name_6 );
        Py_DECREF( tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 98;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_called_instance_4;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_assattr_target_7;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_imports );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_imports );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "imports" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 99;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_4 = tmp_mvar_value_2;
        frame_f74afcd1307791ef9813981c113d2a3e->m_frame.f_lineno = 99;
        tmp_assattr_name_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_ModuleCache );
        if ( tmp_assattr_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_7 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain_module_cache, tmp_assattr_name_7 );
        Py_DECREF( tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 99;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_8;
        PyObject *tmp_assattr_target_8;
        tmp_assattr_name_8 = PyDict_New();
        CHECK_OBJECT( par_self );
        tmp_assattr_target_8 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain_compiled_cache, tmp_assattr_name_8 );
        Py_DECREF( tmp_assattr_name_8 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 100;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_9;
        PyObject *tmp_assattr_target_9;
        tmp_assattr_name_9 = PyDict_New();
        CHECK_OBJECT( par_self );
        tmp_assattr_target_9 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_9, const_str_plain_inferred_element_counts, tmp_assattr_name_9 );
        Py_DECREF( tmp_assattr_name_9 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 101;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_10;
        PyObject *tmp_assattr_target_10;
        tmp_assattr_name_10 = PyDict_New();
        CHECK_OBJECT( par_self );
        tmp_assattr_target_10 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_10, const_str_plain_mixed_cache, tmp_assattr_name_10 );
        Py_DECREF( tmp_assattr_name_10 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 102;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_11;
        PyObject *tmp_assattr_target_11;
        tmp_assattr_name_11 = PyList_New( 0 );
        CHECK_OBJECT( par_self );
        tmp_assattr_target_11 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_11, const_str_plain_analysis, tmp_assattr_name_11 );
        Py_DECREF( tmp_assattr_name_11 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_12;
        PyObject *tmp_assattr_target_12;
        tmp_assattr_name_12 = const_int_0;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_12 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_12, const_str_plain_dynamic_params_depth, tmp_assattr_name_12 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_13;
        PyObject *tmp_assattr_target_13;
        tmp_assattr_name_13 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_13 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_13, const_str_plain_is_analysis, tmp_assattr_name_13 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 105;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_14;
        PyObject *tmp_assattr_target_14;
        CHECK_OBJECT( par_project );
        tmp_assattr_name_14 = par_project;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_14 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_14, const_str_plain_project, tmp_assattr_name_14 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_15;
        PyObject *tmp_assattr_target_15;
        tmp_assattr_name_15 = PyDict_New();
        CHECK_OBJECT( par_self );
        tmp_assattr_target_15 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_15, const_str_plain_access_cache, tmp_assattr_name_15 );
        Py_DECREF( tmp_assattr_name_15 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_16;
        PyObject *tmp_assattr_target_16;
        tmp_assattr_name_16 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_16 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_16, const_str_plain_infer_enabled, tmp_assattr_name_16 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_5;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_5 = par_self;
        frame_f74afcd1307791ef9813981c113d2a3e->m_frame.f_lineno = 112;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_reset_recursion_limitations );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 112;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_17;
        PyObject *tmp_assattr_target_17;
        tmp_assattr_name_17 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_17 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_17, const_str_plain_allow_different_encoding, tmp_assattr_name_17 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 113;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f74afcd1307791ef9813981c113d2a3e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f74afcd1307791ef9813981c113d2a3e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f74afcd1307791ef9813981c113d2a3e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f74afcd1307791ef9813981c113d2a3e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f74afcd1307791ef9813981c113d2a3e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f74afcd1307791ef9813981c113d2a3e,
        type_description_1,
        par_self,
        par_project,
        par_environment,
        par_script_path
    );


    // Release cached frame.
    if ( frame_f74afcd1307791ef9813981c113d2a3e == cache_frame_f74afcd1307791ef9813981c113d2a3e )
    {
        Py_DECREF( frame_f74afcd1307791ef9813981c113d2a3e );
    }
    cache_frame_f74afcd1307791ef9813981c113d2a3e = NULL;

    assertFrameObject( frame_f74afcd1307791ef9813981c113d2a3e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_project );
    Py_DECREF( par_project );
    par_project = NULL;

    CHECK_OBJECT( (PyObject *)par_environment );
    Py_DECREF( par_environment );
    par_environment = NULL;

    CHECK_OBJECT( (PyObject *)par_script_path );
    Py_DECREF( par_script_path );
    par_script_path = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_project );
    Py_DECREF( par_project );
    par_project = NULL;

    Py_XDECREF( par_environment );
    par_environment = NULL;

    CHECK_OBJECT( (PyObject *)par_script_path );
    Py_DECREF( par_script_path );
    par_script_path = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_2_builtins_module( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_2dc089a51f40a6a717e824f4d36286f4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2dc089a51f40a6a717e824f4d36286f4 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2dc089a51f40a6a717e824f4d36286f4, codeobj_2dc089a51f40a6a717e824f4d36286f4, module_jedi$evaluate, sizeof(void *) );
    frame_2dc089a51f40a6a717e824f4d36286f4 = cache_frame_2dc089a51f40a6a717e824f4d36286f4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2dc089a51f40a6a717e824f4d36286f4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2dc089a51f40a6a717e824f4d36286f4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_compiled );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_compiled );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "compiled" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        tmp_args_element_name_2 = const_str_plain_BUILTINS;
        frame_2dc089a51f40a6a717e824f4d36286f4->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get_special_object, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2dc089a51f40a6a717e824f4d36286f4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2dc089a51f40a6a717e824f4d36286f4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2dc089a51f40a6a717e824f4d36286f4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2dc089a51f40a6a717e824f4d36286f4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2dc089a51f40a6a717e824f4d36286f4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2dc089a51f40a6a717e824f4d36286f4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2dc089a51f40a6a717e824f4d36286f4,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_2dc089a51f40a6a717e824f4d36286f4 == cache_frame_2dc089a51f40a6a717e824f4d36286f4 )
    {
        Py_DECREF( frame_2dc089a51f40a6a717e824f4d36286f4 );
    }
    cache_frame_2dc089a51f40a6a717e824f4d36286f4 = NULL;

    assertFrameObject( frame_2dc089a51f40a6a717e824f4d36286f4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_2_builtins_module );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_2_builtins_module );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_3_reset_recursion_limitations( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_34366af6610c2510392812acd68c38c8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_34366af6610c2510392812acd68c38c8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_34366af6610c2510392812acd68c38c8, codeobj_34366af6610c2510392812acd68c38c8, module_jedi$evaluate, sizeof(void *) );
    frame_34366af6610c2510392812acd68c38c8 = cache_frame_34366af6610c2510392812acd68c38c8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_34366af6610c2510392812acd68c38c8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_34366af6610c2510392812acd68c38c8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_recursion );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_recursion );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "recursion" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_34366af6610c2510392812acd68c38c8->m_frame.f_lineno = 121;
        tmp_assattr_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_RecursionDetector );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_recursion_detector, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_assattr_target_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_recursion );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_recursion );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "recursion" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 122;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_2;
        CHECK_OBJECT( par_self );
        tmp_args_element_name_1 = par_self;
        frame_34366af6610c2510392812acd68c38c8->m_frame.f_lineno = 122;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_ExecutionRecursionDetector, call_args );
        }

        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_execution_recursion_detector, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 122;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_34366af6610c2510392812acd68c38c8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_34366af6610c2510392812acd68c38c8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_34366af6610c2510392812acd68c38c8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_34366af6610c2510392812acd68c38c8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_34366af6610c2510392812acd68c38c8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_34366af6610c2510392812acd68c38c8,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_34366af6610c2510392812acd68c38c8 == cache_frame_34366af6610c2510392812acd68c38c8 )
    {
        Py_DECREF( frame_34366af6610c2510392812acd68c38c8 );
    }
    cache_frame_34366af6610c2510392812acd68c38c8 = NULL;

    assertFrameObject( frame_34366af6610c2510392812acd68c38c8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_3_reset_recursion_limitations );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_3_reset_recursion_limitations );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_4_get_sys_path( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3c082f47970dd06c3f9fdaf39b85b64f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_3c082f47970dd06c3f9fdaf39b85b64f = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3c082f47970dd06c3f9fdaf39b85b64f, codeobj_3c082f47970dd06c3f9fdaf39b85b64f, module_jedi$evaluate, sizeof(void *) );
    frame_3c082f47970dd06c3f9fdaf39b85b64f = cache_frame_3c082f47970dd06c3f9fdaf39b85b64f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3c082f47970dd06c3f9fdaf39b85b64f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3c082f47970dd06c3f9fdaf39b85b64f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_project );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__get_sys_path );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_tuple_element_1 = par_self;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_environment;
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_environment );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 126;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_3c082f47970dd06c3f9fdaf39b85b64f->m_frame.f_lineno = 126;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 126;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3c082f47970dd06c3f9fdaf39b85b64f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3c082f47970dd06c3f9fdaf39b85b64f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3c082f47970dd06c3f9fdaf39b85b64f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3c082f47970dd06c3f9fdaf39b85b64f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3c082f47970dd06c3f9fdaf39b85b64f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3c082f47970dd06c3f9fdaf39b85b64f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3c082f47970dd06c3f9fdaf39b85b64f,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_3c082f47970dd06c3f9fdaf39b85b64f == cache_frame_3c082f47970dd06c3f9fdaf39b85b64f )
    {
        Py_DECREF( frame_3c082f47970dd06c3f9fdaf39b85b64f );
    }
    cache_frame_3c082f47970dd06c3f9fdaf39b85b64f = NULL;

    assertFrameObject( frame_3c082f47970dd06c3f9fdaf39b85b64f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_4_get_sys_path );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_4_get_sys_path );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_5_eval_element( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_context = python_pars[ 1 ];
    PyObject *par_element = python_pars[ 2 ];
    PyObject *var_if_stmt = NULL;
    PyObject *var_predefined_if_name_dict = NULL;
    PyObject *var_if_stmt_test = NULL;
    PyObject *var_name_dicts = NULL;
    PyObject *var_if_names = NULL;
    PyObject *var_element_names = NULL;
    struct Nuitka_CellObject *var_str_element_names = PyCell_EMPTY();
    PyObject *var_if_name = NULL;
    PyObject *var_definitions = NULL;
    PyObject *var_original_name_dicts = NULL;
    PyObject *var_definition = NULL;
    PyObject *var_new_name_dicts = NULL;
    PyObject *var_i = NULL;
    PyObject *var_name_dict = NULL;
    PyObject *var_result = NULL;
    PyObject *outline_0_var_e = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_for_loop_4__for_iterator = NULL;
    PyObject *tmp_for_loop_4__iter_value = NULL;
    PyObject *tmp_for_loop_5__for_iterator = NULL;
    PyObject *tmp_for_loop_5__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    PyObject *tmp_with_1__indicator = NULL;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_96fcd1e076c30934fa2f8245d9f4ab88;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    struct Nuitka_FrameObject *frame_fdbc5100e91af914c95c4dd3a109a648_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_fdbc5100e91af914c95c4dd3a109a648_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    bool tmp_result;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    static struct Nuitka_FrameObject *cache_frame_96fcd1e076c30934fa2f8245d9f4ab88 = NULL;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_96fcd1e076c30934fa2f8245d9f4ab88, codeobj_96fcd1e076c30934fa2f8245d9f4ab88, module_jedi$evaluate, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_96fcd1e076c30934fa2f8245d9f4ab88 = cache_frame_96fcd1e076c30934fa2f8245d9f4ab88;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_96fcd1e076c30934fa2f8245d9f4ab88 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_96fcd1e076c30934fa2f8245d9f4ab88 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_infer_enabled );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_mvar_value_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 130;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }

            tmp_return_value = tmp_mvar_value_1;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_2;
        CHECK_OBJECT( par_context );
        tmp_isinstance_inst_1 = par_context;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_CompForContext );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompForContext );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompForContext" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_2;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_eval_node );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eval_node );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eval_node" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 133;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_3;
            CHECK_OBJECT( par_context );
            tmp_args_element_name_1 = par_context;
            CHECK_OBJECT( par_element );
            tmp_args_element_name_2 = par_element;
            frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 133;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 133;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( par_element );
        tmp_assign_source_1 = par_element;
        assert( var_if_stmt == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var_if_stmt = tmp_assign_source_1;
    }
    loop_start_1:;
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_if_stmt );
        tmp_compexpr_left_1 = var_if_stmt;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_3 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        goto loop_end_1;
        branch_no_3:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_if_stmt );
        tmp_source_name_2 = var_if_stmt;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_parent );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 137;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_if_stmt;
            assert( old != NULL );
            var_if_stmt = tmp_assign_source_2;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_if_stmt );
        tmp_source_name_3 = var_if_stmt;
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_type );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_tuple_str_plain_if_stmt_str_plain_for_stmt_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_4 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        goto loop_end_1;
        branch_no_4:;
    }
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        int tmp_truth_name_1;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_parser_utils );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parser_utils );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parser_utils" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 140;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_4;
        CHECK_OBJECT( var_if_stmt );
        tmp_args_element_name_3 = var_if_stmt;
        frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 140;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_is_scope, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 140;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 140;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_5 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = Py_None;
            {
                PyObject *old = var_if_stmt;
                assert( old != NULL );
                var_if_stmt = tmp_assign_source_3;
                Py_INCREF( var_if_stmt );
                Py_DECREF( old );
            }

        }
        goto loop_end_1;
        branch_no_5:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 136;
        type_description_1 = "ooooooooocoooooooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_4;
        CHECK_OBJECT( par_context );
        tmp_source_name_5 = par_context;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_predefined_names );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_get );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        if ( var_if_stmt == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "if_stmt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 143;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_4 = var_if_stmt;
        frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 143;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_predefined_if_name_dict == NULL );
        var_predefined_if_name_dict = tmp_assign_source_4;
    }
    {
        nuitka_bool tmp_condition_result_6;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        int tmp_and_left_truth_2;
        nuitka_bool tmp_and_left_value_2;
        nuitka_bool tmp_and_right_value_2;
        int tmp_truth_name_2;
        int tmp_and_left_truth_3;
        nuitka_bool tmp_and_left_value_3;
        nuitka_bool tmp_and_right_value_3;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        PyObject *tmp_source_name_6;
        PyObject *tmp_source_name_7;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_3;
        CHECK_OBJECT( var_predefined_if_name_dict );
        tmp_compexpr_left_3 = var_predefined_if_name_dict;
        tmp_compexpr_right_3 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        if ( var_if_stmt == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "if_stmt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }

        tmp_truth_name_2 = CHECK_IF_TRUE( var_if_stmt );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        if ( var_if_stmt == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "if_stmt" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 148;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = var_if_stmt;
        tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_type );
        if ( tmp_compexpr_left_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_4 = const_str_plain_if_stmt;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        Py_DECREF( tmp_compexpr_left_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_3 = tmp_and_left_value_3 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_3 == 1 )
        {
            goto and_right_3;
        }
        else
        {
            goto and_left_3;
        }
        and_right_3:;
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_is_analysis );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 148;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 148;
            type_description_1 = "ooooooooocoooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_3 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        tmp_and_right_value_2 = tmp_and_right_value_3;
        goto and_end_3;
        and_left_3:;
        tmp_and_right_value_2 = tmp_and_left_value_3;
        and_end_3:;
        tmp_and_right_value_1 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_and_right_value_1 = tmp_and_left_value_2;
        and_end_2:;
        tmp_condition_result_6 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_6 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_source_name_8;
            PyObject *tmp_subscript_name_1;
            if ( var_if_stmt == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "if_stmt" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 149;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_8 = var_if_stmt;
            tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_children );
            if ( tmp_subscribed_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 149;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }
            tmp_subscript_name_1 = const_int_pos_1;
            tmp_assign_source_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
            Py_DECREF( tmp_subscribed_name_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 149;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_if_stmt_test == NULL );
            var_if_stmt_test = tmp_assign_source_5;
        }
        {
            PyObject *tmp_assign_source_6;
            tmp_assign_source_6 = DEEP_COPY( const_list_dict_empty_list );
            assert( var_name_dicts == NULL );
            var_name_dicts = tmp_assign_source_6;
        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            PyObject *tmp_source_name_9;
            PyObject *tmp_source_name_10;
            CHECK_OBJECT( par_element );
            tmp_source_name_9 = par_element;
            tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_start_pos );
            if ( tmp_compexpr_left_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_if_stmt_test );
            tmp_source_name_10 = var_if_stmt_test;
            tmp_compexpr_right_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_end_pos );
            if ( tmp_compexpr_right_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_compexpr_left_5 );

                exception_lineno = 155;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }
            tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            Py_DECREF( tmp_compexpr_left_5 );
            Py_DECREF( tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 155;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_assign_source_7;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_args_element_name_5;
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_helpers );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 158;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_2 = tmp_mvar_value_5;
                CHECK_OBJECT( var_if_stmt_test );
                tmp_args_element_name_5 = var_if_stmt_test;
                frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 158;
                {
                    PyObject *call_args[] = { tmp_args_element_name_5 };
                    tmp_assign_source_7 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_get_names_of_node, call_args );
                }

                if ( tmp_assign_source_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 158;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_if_names == NULL );
                var_if_names = tmp_assign_source_7;
            }
            {
                PyObject *tmp_assign_source_8;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_mvar_value_6;
                PyObject *tmp_args_element_name_6;
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_helpers );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 159;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_3 = tmp_mvar_value_6;
                CHECK_OBJECT( par_element );
                tmp_args_element_name_6 = par_element;
                frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 159;
                {
                    PyObject *call_args[] = { tmp_args_element_name_6 };
                    tmp_assign_source_8 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_get_names_of_node, call_args );
                }

                if ( tmp_assign_source_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 159;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_element_names == NULL );
                var_element_names = tmp_assign_source_8;
            }
            {
                PyObject *tmp_assign_source_9;
                // Tried code:
                {
                    PyObject *tmp_assign_source_10;
                    PyObject *tmp_iter_arg_1;
                    CHECK_OBJECT( var_element_names );
                    tmp_iter_arg_1 = var_element_names;
                    tmp_assign_source_10 = MAKE_ITERATOR( tmp_iter_arg_1 );
                    if ( tmp_assign_source_10 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 160;
                        type_description_1 = "ooooooooocoooooooo";
                        goto try_except_handler_2;
                    }
                    assert( tmp_listcomp_1__$0 == NULL );
                    tmp_listcomp_1__$0 = tmp_assign_source_10;
                }
                {
                    PyObject *tmp_assign_source_11;
                    tmp_assign_source_11 = PyList_New( 0 );
                    assert( tmp_listcomp_1__contraction == NULL );
                    tmp_listcomp_1__contraction = tmp_assign_source_11;
                }
                MAKE_OR_REUSE_FRAME( cache_frame_fdbc5100e91af914c95c4dd3a109a648_2, codeobj_fdbc5100e91af914c95c4dd3a109a648, module_jedi$evaluate, sizeof(void *) );
                frame_fdbc5100e91af914c95c4dd3a109a648_2 = cache_frame_fdbc5100e91af914c95c4dd3a109a648_2;

                // Push the new frame as the currently active one.
                pushFrameStack( frame_fdbc5100e91af914c95c4dd3a109a648_2 );

                // Mark the frame object as in use, ref count 1 will be up for reuse.
                assert( Py_REFCNT( frame_fdbc5100e91af914c95c4dd3a109a648_2 ) == 2 ); // Frame stack

                // Framed code:
                // Tried code:
                loop_start_2:;
                {
                    PyObject *tmp_next_source_1;
                    PyObject *tmp_assign_source_12;
                    CHECK_OBJECT( tmp_listcomp_1__$0 );
                    tmp_next_source_1 = tmp_listcomp_1__$0;
                    tmp_assign_source_12 = ITERATOR_NEXT( tmp_next_source_1 );
                    if ( tmp_assign_source_12 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_2;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_2 = "o";
                            exception_lineno = 160;
                            goto try_except_handler_3;
                        }
                    }

                    {
                        PyObject *old = tmp_listcomp_1__iter_value_0;
                        tmp_listcomp_1__iter_value_0 = tmp_assign_source_12;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_13;
                    CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
                    tmp_assign_source_13 = tmp_listcomp_1__iter_value_0;
                    {
                        PyObject *old = outline_0_var_e;
                        outline_0_var_e = tmp_assign_source_13;
                        Py_INCREF( outline_0_var_e );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_append_list_1;
                    PyObject *tmp_append_value_1;
                    PyObject *tmp_source_name_11;
                    CHECK_OBJECT( tmp_listcomp_1__contraction );
                    tmp_append_list_1 = tmp_listcomp_1__contraction;
                    CHECK_OBJECT( outline_0_var_e );
                    tmp_source_name_11 = outline_0_var_e;
                    tmp_append_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_value );
                    if ( tmp_append_value_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 160;
                        type_description_2 = "o";
                        goto try_except_handler_3;
                    }
                    assert( PyList_Check( tmp_append_list_1 ) );
                    tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
                    Py_DECREF( tmp_append_value_1 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 160;
                        type_description_2 = "o";
                        goto try_except_handler_3;
                    }
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 160;
                    type_description_2 = "o";
                    goto try_except_handler_3;
                }
                goto loop_start_2;
                loop_end_2:;
                CHECK_OBJECT( tmp_listcomp_1__contraction );
                tmp_assign_source_9 = tmp_listcomp_1__contraction;
                Py_INCREF( tmp_assign_source_9 );
                goto try_return_handler_3;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_5_eval_element );
                return NULL;
                // Return handler code:
                try_return_handler_3:;
                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                goto frame_return_exit_2;
                // Exception handler code:
                try_except_handler_3:;
                exception_keeper_type_1 = exception_type;
                exception_keeper_value_1 = exception_value;
                exception_keeper_tb_1 = exception_tb;
                exception_keeper_lineno_1 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
                Py_DECREF( tmp_listcomp_1__$0 );
                tmp_listcomp_1__$0 = NULL;

                CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
                Py_DECREF( tmp_listcomp_1__contraction );
                tmp_listcomp_1__contraction = NULL;

                Py_XDECREF( tmp_listcomp_1__iter_value_0 );
                tmp_listcomp_1__iter_value_0 = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_1;
                exception_value = exception_keeper_value_1;
                exception_tb = exception_keeper_tb_1;
                exception_lineno = exception_keeper_lineno_1;

                goto frame_exception_exit_2;
                // End of try:

#if 0
                RESTORE_FRAME_EXCEPTION( frame_fdbc5100e91af914c95c4dd3a109a648_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto frame_no_exception_1;

                frame_return_exit_2:;
#if 0
                RESTORE_FRAME_EXCEPTION( frame_fdbc5100e91af914c95c4dd3a109a648_2 );
#endif

                // Put the previous frame back on top.
                popFrameStack();

                goto try_return_handler_2;

                frame_exception_exit_2:;

#if 0
                RESTORE_FRAME_EXCEPTION( frame_fdbc5100e91af914c95c4dd3a109a648_2 );
#endif

                if ( exception_tb == NULL )
                {
                    exception_tb = MAKE_TRACEBACK( frame_fdbc5100e91af914c95c4dd3a109a648_2, exception_lineno );
                }
                else if ( exception_tb->tb_frame != &frame_fdbc5100e91af914c95c4dd3a109a648_2->m_frame )
                {
                    exception_tb = ADD_TRACEBACK( exception_tb, frame_fdbc5100e91af914c95c4dd3a109a648_2, exception_lineno );
                }

                // Attachs locals to frame if any.
                Nuitka_Frame_AttachLocals(
                    (struct Nuitka_FrameObject *)frame_fdbc5100e91af914c95c4dd3a109a648_2,
                    type_description_2,
                    outline_0_var_e
                );


                // Release cached frame.
                if ( frame_fdbc5100e91af914c95c4dd3a109a648_2 == cache_frame_fdbc5100e91af914c95c4dd3a109a648_2 )
                {
                    Py_DECREF( frame_fdbc5100e91af914c95c4dd3a109a648_2 );
                }
                cache_frame_fdbc5100e91af914c95c4dd3a109a648_2 = NULL;

                assertFrameObject( frame_fdbc5100e91af914c95c4dd3a109a648_2 );

                // Put the previous frame back on top.
                popFrameStack();

                // Return the error.
                goto nested_frame_exit_1;

                frame_no_exception_1:;
                goto skip_nested_handling_1;
                nested_frame_exit_1:;
                type_description_1 = "ooooooooocoooooooo";
                goto try_except_handler_2;
                skip_nested_handling_1:;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_5_eval_element );
                return NULL;
                // Return handler code:
                try_return_handler_2:;
                Py_XDECREF( outline_0_var_e );
                outline_0_var_e = NULL;

                goto outline_result_1;
                // Exception handler code:
                try_except_handler_2:;
                exception_keeper_type_2 = exception_type;
                exception_keeper_value_2 = exception_value;
                exception_keeper_tb_2 = exception_tb;
                exception_keeper_lineno_2 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( outline_0_var_e );
                outline_0_var_e = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_2;
                exception_value = exception_keeper_value_2;
                exception_tb = exception_keeper_tb_2;
                exception_lineno = exception_keeper_lineno_2;

                goto outline_exception_1;
                // End of try:
                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_5_eval_element );
                return NULL;
                outline_exception_1:;
                exception_lineno = 160;
                goto frame_exception_exit_1;
                outline_result_1:;
                assert( PyCell_GET( var_str_element_names ) == NULL );
                PyCell_SET( var_str_element_names, tmp_assign_source_9 );

            }
            {
                nuitka_bool tmp_condition_result_8;
                PyObject *tmp_any_arg_1;
                PyObject *tmp_capi_result_1;
                int tmp_truth_name_4;
                {
                    PyObject *tmp_assign_source_14;
                    PyObject *tmp_iter_arg_2;
                    CHECK_OBJECT( var_if_names );
                    tmp_iter_arg_2 = var_if_names;
                    tmp_assign_source_14 = MAKE_ITERATOR( tmp_iter_arg_2 );
                    if ( tmp_assign_source_14 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 161;
                        type_description_1 = "ooooooooocoooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( tmp_genexpr_1__$0 == NULL );
                    tmp_genexpr_1__$0 = tmp_assign_source_14;
                }
                // Tried code:
                tmp_any_arg_1 = jedi$evaluate$$$function_5_eval_element$$$genexpr_1_genexpr_maker();

                ((struct Nuitka_GeneratorObject *)tmp_any_arg_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );
                ((struct Nuitka_GeneratorObject *)tmp_any_arg_1)->m_closure[1] = var_str_element_names;
                Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_any_arg_1)->m_closure[1] );


                goto try_return_handler_4;
                // tried codes exits in all cases
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_5_eval_element );
                return NULL;
                // Return handler code:
                try_return_handler_4:;
                CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                Py_DECREF( tmp_genexpr_1__$0 );
                tmp_genexpr_1__$0 = NULL;

                goto outline_result_2;
                // End of try:
                CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                Py_DECREF( tmp_genexpr_1__$0 );
                tmp_genexpr_1__$0 = NULL;

                // Return statement must have exited already.
                NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_5_eval_element );
                return NULL;
                outline_result_2:;
                tmp_capi_result_1 = BUILTIN_ANY( tmp_any_arg_1 );
                Py_DECREF( tmp_any_arg_1 );
                if ( tmp_capi_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 161;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_truth_name_4 = CHECK_IF_TRUE( tmp_capi_result_1 );
                if ( tmp_truth_name_4 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_capi_result_1 );

                    exception_lineno = 161;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_8 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_capi_result_1 );
                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                {
                    PyObject *tmp_assign_source_15;
                    PyObject *tmp_iter_arg_3;
                    CHECK_OBJECT( var_if_names );
                    tmp_iter_arg_3 = var_if_names;
                    tmp_assign_source_15 = MAKE_ITERATOR( tmp_iter_arg_3 );
                    if ( tmp_assign_source_15 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 162;
                        type_description_1 = "ooooooooocoooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( tmp_for_loop_1__for_iterator == NULL );
                    tmp_for_loop_1__for_iterator = tmp_assign_source_15;
                }
                // Tried code:
                loop_start_3:;
                {
                    PyObject *tmp_next_source_2;
                    PyObject *tmp_assign_source_16;
                    CHECK_OBJECT( tmp_for_loop_1__for_iterator );
                    tmp_next_source_2 = tmp_for_loop_1__for_iterator;
                    tmp_assign_source_16 = ITERATOR_NEXT( tmp_next_source_2 );
                    if ( tmp_assign_source_16 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_3;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "ooooooooocoooooooo";
                            exception_lineno = 162;
                            goto try_except_handler_5;
                        }
                    }

                    {
                        PyObject *old = tmp_for_loop_1__iter_value;
                        tmp_for_loop_1__iter_value = tmp_assign_source_16;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_17;
                    CHECK_OBJECT( tmp_for_loop_1__iter_value );
                    tmp_assign_source_17 = tmp_for_loop_1__iter_value;
                    {
                        PyObject *old = var_if_name;
                        var_if_name = tmp_assign_source_17;
                        Py_INCREF( var_if_name );
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_18;
                    PyObject *tmp_called_instance_4;
                    PyObject *tmp_args_element_name_7;
                    PyObject *tmp_args_element_name_8;
                    CHECK_OBJECT( par_self );
                    tmp_called_instance_4 = par_self;
                    CHECK_OBJECT( par_context );
                    tmp_args_element_name_7 = par_context;
                    CHECK_OBJECT( var_if_name );
                    tmp_args_element_name_8 = var_if_name;
                    frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 163;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8 };
                        tmp_assign_source_18 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_goto_definitions, call_args );
                    }

                    if ( tmp_assign_source_18 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 163;
                        type_description_1 = "ooooooooocoooooooo";
                        goto try_except_handler_5;
                    }
                    {
                        PyObject *old = var_definitions;
                        var_definitions = tmp_assign_source_18;
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_9;
                    PyObject *tmp_compexpr_left_6;
                    PyObject *tmp_compexpr_right_6;
                    PyObject *tmp_len_arg_1;
                    CHECK_OBJECT( var_definitions );
                    tmp_len_arg_1 = var_definitions;
                    tmp_compexpr_left_6 = BUILTIN_LEN( tmp_len_arg_1 );
                    if ( tmp_compexpr_left_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 167;
                        type_description_1 = "ooooooooocoooooooo";
                        goto try_except_handler_5;
                    }
                    tmp_compexpr_right_6 = const_int_pos_1;
                    tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
                    Py_DECREF( tmp_compexpr_left_6 );
                    assert( !(tmp_res == -1) );
                    tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_9;
                    }
                    else
                    {
                        goto branch_no_9;
                    }
                    branch_yes_9:;
                    {
                        nuitka_bool tmp_condition_result_10;
                        PyObject *tmp_compexpr_left_7;
                        PyObject *tmp_compexpr_right_7;
                        PyObject *tmp_left_name_1;
                        PyObject *tmp_len_arg_2;
                        PyObject *tmp_right_name_1;
                        PyObject *tmp_len_arg_3;
                        if ( var_name_dicts == NULL )
                        {

                            exception_type = PyExc_UnboundLocalError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "name_dicts" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 168;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_5;
                        }

                        tmp_len_arg_2 = var_name_dicts;
                        tmp_left_name_1 = BUILTIN_LEN( tmp_len_arg_2 );
                        if ( tmp_left_name_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 168;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_5;
                        }
                        CHECK_OBJECT( var_definitions );
                        tmp_len_arg_3 = var_definitions;
                        tmp_right_name_1 = BUILTIN_LEN( tmp_len_arg_3 );
                        if ( tmp_right_name_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_left_name_1 );

                            exception_lineno = 168;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_5;
                        }
                        tmp_compexpr_left_7 = BINARY_OPERATION_MUL_LONG_LONG( tmp_left_name_1, tmp_right_name_1 );
                        Py_DECREF( tmp_left_name_1 );
                        Py_DECREF( tmp_right_name_1 );
                        assert( !(tmp_compexpr_left_7 == NULL) );
                        tmp_compexpr_right_7 = const_int_pos_16;
                        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
                        Py_DECREF( tmp_compexpr_left_7 );
                        assert( !(tmp_res == -1) );
                        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_10;
                        }
                        else
                        {
                            goto branch_no_10;
                        }
                        branch_yes_10:;
                        {
                            PyObject *tmp_called_name_3;
                            PyObject *tmp_source_name_12;
                            PyObject *tmp_mvar_value_7;
                            PyObject *tmp_call_result_2;
                            PyObject *tmp_args_element_name_9;
                            PyObject *tmp_args_element_name_10;
                            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_debug );

                            if (unlikely( tmp_mvar_value_7 == NULL ))
                            {
                                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_debug );
                            }

                            if ( tmp_mvar_value_7 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "debug" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 169;
                                type_description_1 = "ooooooooocoooooooo";
                                goto try_except_handler_5;
                            }

                            tmp_source_name_12 = tmp_mvar_value_7;
                            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_dbg );
                            if ( tmp_called_name_3 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 169;
                                type_description_1 = "ooooooooocoooooooo";
                                goto try_except_handler_5;
                            }
                            tmp_args_element_name_9 = const_str_digest_8416bd7f7b8d9d064776ccaff84429d1;
                            if ( var_if_stmt == NULL )
                            {
                                Py_DECREF( tmp_called_name_3 );
                                exception_type = PyExc_UnboundLocalError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "if_stmt" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 169;
                                type_description_1 = "ooooooooocoooooooo";
                                goto try_except_handler_5;
                            }

                            tmp_args_element_name_10 = var_if_stmt;
                            frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 169;
                            {
                                PyObject *call_args[] = { tmp_args_element_name_9, tmp_args_element_name_10 };
                                tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
                            }

                            Py_DECREF( tmp_called_name_3 );
                            if ( tmp_call_result_2 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 169;
                                type_description_1 = "ooooooooocoooooooo";
                                goto try_except_handler_5;
                            }
                            Py_DECREF( tmp_call_result_2 );
                        }
                        {
                            PyObject *tmp_assign_source_19;
                            tmp_assign_source_19 = DEEP_COPY( const_list_dict_empty_list );
                            {
                                PyObject *old = var_name_dicts;
                                var_name_dicts = tmp_assign_source_19;
                                Py_XDECREF( old );
                            }

                        }
                        goto loop_end_3;
                        branch_no_10:;
                    }
                    {
                        PyObject *tmp_assign_source_20;
                        PyObject *tmp_list_arg_1;
                        if ( var_name_dicts == NULL )
                        {

                            exception_type = PyExc_UnboundLocalError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "name_dicts" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 176;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_5;
                        }

                        tmp_list_arg_1 = var_name_dicts;
                        tmp_assign_source_20 = PySequence_List( tmp_list_arg_1 );
                        if ( tmp_assign_source_20 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 176;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_5;
                        }
                        {
                            PyObject *old = var_original_name_dicts;
                            var_original_name_dicts = tmp_assign_source_20;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_21;
                        tmp_assign_source_21 = PyList_New( 0 );
                        {
                            PyObject *old = var_name_dicts;
                            var_name_dicts = tmp_assign_source_21;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_22;
                        PyObject *tmp_iter_arg_4;
                        CHECK_OBJECT( var_definitions );
                        tmp_iter_arg_4 = var_definitions;
                        tmp_assign_source_22 = MAKE_ITERATOR( tmp_iter_arg_4 );
                        if ( tmp_assign_source_22 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 178;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_5;
                        }
                        {
                            PyObject *old = tmp_for_loop_2__for_iterator;
                            tmp_for_loop_2__for_iterator = tmp_assign_source_22;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    loop_start_4:;
                    {
                        PyObject *tmp_next_source_3;
                        PyObject *tmp_assign_source_23;
                        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
                        tmp_next_source_3 = tmp_for_loop_2__for_iterator;
                        tmp_assign_source_23 = ITERATOR_NEXT( tmp_next_source_3 );
                        if ( tmp_assign_source_23 == NULL )
                        {
                            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                            {

                                goto loop_end_4;
                            }
                            else
                            {

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                type_description_1 = "ooooooooocoooooooo";
                                exception_lineno = 178;
                                goto try_except_handler_6;
                            }
                        }

                        {
                            PyObject *old = tmp_for_loop_2__iter_value;
                            tmp_for_loop_2__iter_value = tmp_assign_source_23;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_24;
                        CHECK_OBJECT( tmp_for_loop_2__iter_value );
                        tmp_assign_source_24 = tmp_for_loop_2__iter_value;
                        {
                            PyObject *old = var_definition;
                            var_definition = tmp_assign_source_24;
                            Py_INCREF( var_definition );
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_25;
                        PyObject *tmp_list_arg_2;
                        CHECK_OBJECT( var_original_name_dicts );
                        tmp_list_arg_2 = var_original_name_dicts;
                        tmp_assign_source_25 = PySequence_List( tmp_list_arg_2 );
                        if ( tmp_assign_source_25 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 179;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_6;
                        }
                        {
                            PyObject *old = var_new_name_dicts;
                            var_new_name_dicts = tmp_assign_source_25;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_26;
                        PyObject *tmp_iter_arg_5;
                        PyObject *tmp_called_name_4;
                        PyObject *tmp_args_element_name_11;
                        tmp_called_name_4 = (PyObject *)&PyEnum_Type;
                        CHECK_OBJECT( var_new_name_dicts );
                        tmp_args_element_name_11 = var_new_name_dicts;
                        frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 180;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_11 };
                            tmp_iter_arg_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
                        }

                        if ( tmp_iter_arg_5 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 180;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_6;
                        }
                        tmp_assign_source_26 = MAKE_ITERATOR( tmp_iter_arg_5 );
                        Py_DECREF( tmp_iter_arg_5 );
                        if ( tmp_assign_source_26 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 180;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_6;
                        }
                        {
                            PyObject *old = tmp_for_loop_3__for_iterator;
                            tmp_for_loop_3__for_iterator = tmp_assign_source_26;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    loop_start_5:;
                    {
                        PyObject *tmp_next_source_4;
                        PyObject *tmp_assign_source_27;
                        CHECK_OBJECT( tmp_for_loop_3__for_iterator );
                        tmp_next_source_4 = tmp_for_loop_3__for_iterator;
                        tmp_assign_source_27 = ITERATOR_NEXT( tmp_next_source_4 );
                        if ( tmp_assign_source_27 == NULL )
                        {
                            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                            {

                                goto loop_end_5;
                            }
                            else
                            {

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                type_description_1 = "ooooooooocoooooooo";
                                exception_lineno = 180;
                                goto try_except_handler_7;
                            }
                        }

                        {
                            PyObject *old = tmp_for_loop_3__iter_value;
                            tmp_for_loop_3__iter_value = tmp_assign_source_27;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_28;
                        PyObject *tmp_iter_arg_6;
                        CHECK_OBJECT( tmp_for_loop_3__iter_value );
                        tmp_iter_arg_6 = tmp_for_loop_3__iter_value;
                        tmp_assign_source_28 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_6 );
                        if ( tmp_assign_source_28 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 180;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_8;
                        }
                        {
                            PyObject *old = tmp_tuple_unpack_1__source_iter;
                            tmp_tuple_unpack_1__source_iter = tmp_assign_source_28;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    {
                        PyObject *tmp_assign_source_29;
                        PyObject *tmp_unpack_1;
                        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
                        tmp_assign_source_29 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
                        if ( tmp_assign_source_29 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooooooocoooooooo";
                            exception_lineno = 180;
                            goto try_except_handler_9;
                        }
                        {
                            PyObject *old = tmp_tuple_unpack_1__element_1;
                            tmp_tuple_unpack_1__element_1 = tmp_assign_source_29;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_30;
                        PyObject *tmp_unpack_2;
                        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
                        tmp_assign_source_30 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
                        if ( tmp_assign_source_30 == NULL )
                        {
                            if ( !ERROR_OCCURRED() )
                            {
                                exception_type = PyExc_StopIteration;
                                Py_INCREF( exception_type );
                                exception_value = NULL;
                                exception_tb = NULL;
                            }
                            else
                            {
                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            }


                            type_description_1 = "ooooooooocoooooooo";
                            exception_lineno = 180;
                            goto try_except_handler_9;
                        }
                        {
                            PyObject *old = tmp_tuple_unpack_1__element_2;
                            tmp_tuple_unpack_1__element_2 = tmp_assign_source_30;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_iterator_name_1;
                        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
                        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
                        // Check if iterator has left-over elements.
                        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

                        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

                        if (likely( tmp_iterator_attempt == NULL ))
                        {
                            PyObject *error = GET_ERROR_OCCURRED();

                            if ( error != NULL )
                            {
                                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                                {
                                    CLEAR_ERROR_OCCURRED();
                                }
                                else
                                {
                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                                    type_description_1 = "ooooooooocoooooooo";
                                    exception_lineno = 180;
                                    goto try_except_handler_9;
                                }
                            }
                        }
                        else
                        {
                            Py_DECREF( tmp_iterator_attempt );

                            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "ooooooooocoooooooo";
                            exception_lineno = 180;
                            goto try_except_handler_9;
                        }
                    }
                    goto try_end_1;
                    // Exception handler code:
                    try_except_handler_9:;
                    exception_keeper_type_3 = exception_type;
                    exception_keeper_value_3 = exception_value;
                    exception_keeper_tb_3 = exception_tb;
                    exception_keeper_lineno_3 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
                    Py_DECREF( tmp_tuple_unpack_1__source_iter );
                    tmp_tuple_unpack_1__source_iter = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_3;
                    exception_value = exception_keeper_value_3;
                    exception_tb = exception_keeper_tb_3;
                    exception_lineno = exception_keeper_lineno_3;

                    goto try_except_handler_8;
                    // End of try:
                    try_end_1:;
                    goto try_end_2;
                    // Exception handler code:
                    try_except_handler_8:;
                    exception_keeper_type_4 = exception_type;
                    exception_keeper_value_4 = exception_value;
                    exception_keeper_tb_4 = exception_tb;
                    exception_keeper_lineno_4 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
                    tmp_tuple_unpack_1__element_1 = NULL;

                    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
                    tmp_tuple_unpack_1__element_2 = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_4;
                    exception_value = exception_keeper_value_4;
                    exception_tb = exception_keeper_tb_4;
                    exception_lineno = exception_keeper_lineno_4;

                    goto try_except_handler_7;
                    // End of try:
                    try_end_2:;
                    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
                    Py_DECREF( tmp_tuple_unpack_1__source_iter );
                    tmp_tuple_unpack_1__source_iter = NULL;

                    {
                        PyObject *tmp_assign_source_31;
                        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
                        tmp_assign_source_31 = tmp_tuple_unpack_1__element_1;
                        {
                            PyObject *old = var_i;
                            var_i = tmp_assign_source_31;
                            Py_INCREF( var_i );
                            Py_XDECREF( old );
                        }

                    }
                    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
                    tmp_tuple_unpack_1__element_1 = NULL;

                    {
                        PyObject *tmp_assign_source_32;
                        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
                        tmp_assign_source_32 = tmp_tuple_unpack_1__element_2;
                        {
                            PyObject *old = var_name_dict;
                            var_name_dict = tmp_assign_source_32;
                            Py_INCREF( var_name_dict );
                            Py_XDECREF( old );
                        }

                    }
                    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
                    tmp_tuple_unpack_1__element_2 = NULL;

                    {
                        PyObject *tmp_ass_subvalue_1;
                        PyObject *tmp_called_instance_5;
                        PyObject *tmp_ass_subscribed_1;
                        PyObject *tmp_ass_subscript_1;
                        CHECK_OBJECT( var_name_dict );
                        tmp_called_instance_5 = var_name_dict;
                        frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 181;
                        tmp_ass_subvalue_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_5, const_str_plain_copy );
                        if ( tmp_ass_subvalue_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 181;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_7;
                        }
                        CHECK_OBJECT( var_new_name_dicts );
                        tmp_ass_subscribed_1 = var_new_name_dicts;
                        CHECK_OBJECT( var_i );
                        tmp_ass_subscript_1 = var_i;
                        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
                        Py_DECREF( tmp_ass_subvalue_1 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 181;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_7;
                        }
                    }
                    {
                        PyObject *tmp_ass_subvalue_2;
                        PyObject *tmp_called_name_5;
                        PyObject *tmp_mvar_value_8;
                        PyObject *tmp_args_element_name_12;
                        PyObject *tmp_ass_subscribed_2;
                        PyObject *tmp_subscribed_name_2;
                        PyObject *tmp_subscript_name_2;
                        PyObject *tmp_ass_subscript_2;
                        PyObject *tmp_source_name_13;
                        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ContextSet );

                        if (unlikely( tmp_mvar_value_8 == NULL ))
                        {
                            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextSet );
                        }

                        if ( tmp_mvar_value_8 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextSet" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 182;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_7;
                        }

                        tmp_called_name_5 = tmp_mvar_value_8;
                        CHECK_OBJECT( var_definition );
                        tmp_args_element_name_12 = var_definition;
                        frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 182;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_12 };
                            tmp_ass_subvalue_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
                        }

                        if ( tmp_ass_subvalue_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 182;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_7;
                        }
                        CHECK_OBJECT( var_new_name_dicts );
                        tmp_subscribed_name_2 = var_new_name_dicts;
                        CHECK_OBJECT( var_i );
                        tmp_subscript_name_2 = var_i;
                        tmp_ass_subscribed_2 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
                        if ( tmp_ass_subscribed_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_ass_subvalue_2 );

                            exception_lineno = 182;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_7;
                        }
                        CHECK_OBJECT( var_if_name );
                        tmp_source_name_13 = var_if_name;
                        tmp_ass_subscript_2 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_value );
                        if ( tmp_ass_subscript_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            Py_DECREF( tmp_ass_subvalue_2 );
                            Py_DECREF( tmp_ass_subscribed_2 );

                            exception_lineno = 182;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_7;
                        }
                        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_2, tmp_ass_subscript_2, tmp_ass_subvalue_2 );
                        Py_DECREF( tmp_ass_subscribed_2 );
                        Py_DECREF( tmp_ass_subscript_2 );
                        Py_DECREF( tmp_ass_subvalue_2 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 182;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_7;
                        }
                    }
                    if ( CONSIDER_THREADING() == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 180;
                        type_description_1 = "ooooooooocoooooooo";
                        goto try_except_handler_7;
                    }
                    goto loop_start_5;
                    loop_end_5:;
                    goto try_end_3;
                    // Exception handler code:
                    try_except_handler_7:;
                    exception_keeper_type_5 = exception_type;
                    exception_keeper_value_5 = exception_value;
                    exception_keeper_tb_5 = exception_tb;
                    exception_keeper_lineno_5 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_for_loop_3__iter_value );
                    tmp_for_loop_3__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
                    Py_DECREF( tmp_for_loop_3__for_iterator );
                    tmp_for_loop_3__for_iterator = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_5;
                    exception_value = exception_keeper_value_5;
                    exception_tb = exception_keeper_tb_5;
                    exception_lineno = exception_keeper_lineno_5;

                    goto try_except_handler_6;
                    // End of try:
                    try_end_3:;
                    Py_XDECREF( tmp_for_loop_3__iter_value );
                    tmp_for_loop_3__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
                    Py_DECREF( tmp_for_loop_3__for_iterator );
                    tmp_for_loop_3__for_iterator = NULL;

                    {
                        PyObject *tmp_assign_source_33;
                        PyObject *tmp_left_name_2;
                        PyObject *tmp_right_name_2;
                        CHECK_OBJECT( var_name_dicts );
                        tmp_left_name_2 = var_name_dicts;
                        CHECK_OBJECT( var_new_name_dicts );
                        tmp_right_name_2 = var_new_name_dicts;
                        tmp_result = BINARY_OPERATION_ADD_OBJECT_LIST_INPLACE( &tmp_left_name_2, tmp_right_name_2 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 184;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_6;
                        }
                        tmp_assign_source_33 = tmp_left_name_2;
                        var_name_dicts = tmp_assign_source_33;

                    }
                    if ( CONSIDER_THREADING() == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 178;
                        type_description_1 = "ooooooooocoooooooo";
                        goto try_except_handler_6;
                    }
                    goto loop_start_4;
                    loop_end_4:;
                    goto try_end_4;
                    // Exception handler code:
                    try_except_handler_6:;
                    exception_keeper_type_6 = exception_type;
                    exception_keeper_value_6 = exception_value;
                    exception_keeper_tb_6 = exception_tb;
                    exception_keeper_lineno_6 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_for_loop_2__iter_value );
                    tmp_for_loop_2__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
                    Py_DECREF( tmp_for_loop_2__for_iterator );
                    tmp_for_loop_2__for_iterator = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_6;
                    exception_value = exception_keeper_value_6;
                    exception_tb = exception_keeper_tb_6;
                    exception_lineno = exception_keeper_lineno_6;

                    goto try_except_handler_5;
                    // End of try:
                    try_end_4:;
                    Py_XDECREF( tmp_for_loop_2__iter_value );
                    tmp_for_loop_2__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
                    Py_DECREF( tmp_for_loop_2__for_iterator );
                    tmp_for_loop_2__for_iterator = NULL;

                    goto branch_end_9;
                    branch_no_9:;
                    {
                        PyObject *tmp_assign_source_34;
                        PyObject *tmp_iter_arg_7;
                        if ( var_name_dicts == NULL )
                        {

                            exception_type = PyExc_UnboundLocalError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "name_dicts" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 186;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_5;
                        }

                        tmp_iter_arg_7 = var_name_dicts;
                        tmp_assign_source_34 = MAKE_ITERATOR( tmp_iter_arg_7 );
                        if ( tmp_assign_source_34 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 186;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_5;
                        }
                        {
                            PyObject *old = tmp_for_loop_4__for_iterator;
                            tmp_for_loop_4__for_iterator = tmp_assign_source_34;
                            Py_XDECREF( old );
                        }

                    }
                    // Tried code:
                    loop_start_6:;
                    {
                        PyObject *tmp_next_source_5;
                        PyObject *tmp_assign_source_35;
                        CHECK_OBJECT( tmp_for_loop_4__for_iterator );
                        tmp_next_source_5 = tmp_for_loop_4__for_iterator;
                        tmp_assign_source_35 = ITERATOR_NEXT( tmp_next_source_5 );
                        if ( tmp_assign_source_35 == NULL )
                        {
                            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                            {

                                goto loop_end_6;
                            }
                            else
                            {

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                type_description_1 = "ooooooooocoooooooo";
                                exception_lineno = 186;
                                goto try_except_handler_10;
                            }
                        }

                        {
                            PyObject *old = tmp_for_loop_4__iter_value;
                            tmp_for_loop_4__iter_value = tmp_assign_source_35;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_assign_source_36;
                        CHECK_OBJECT( tmp_for_loop_4__iter_value );
                        tmp_assign_source_36 = tmp_for_loop_4__iter_value;
                        {
                            PyObject *old = var_name_dict;
                            var_name_dict = tmp_assign_source_36;
                            Py_INCREF( var_name_dict );
                            Py_XDECREF( old );
                        }

                    }
                    {
                        PyObject *tmp_ass_subvalue_3;
                        PyObject *tmp_ass_subscribed_3;
                        PyObject *tmp_ass_subscript_3;
                        PyObject *tmp_source_name_14;
                        CHECK_OBJECT( var_definitions );
                        tmp_ass_subvalue_3 = var_definitions;
                        CHECK_OBJECT( var_name_dict );
                        tmp_ass_subscribed_3 = var_name_dict;
                        CHECK_OBJECT( var_if_name );
                        tmp_source_name_14 = var_if_name;
                        tmp_ass_subscript_3 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_value );
                        if ( tmp_ass_subscript_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 187;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_10;
                        }
                        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_3, tmp_ass_subscript_3, tmp_ass_subvalue_3 );
                        Py_DECREF( tmp_ass_subscript_3 );
                        if ( tmp_result == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 187;
                            type_description_1 = "ooooooooocoooooooo";
                            goto try_except_handler_10;
                        }
                    }
                    if ( CONSIDER_THREADING() == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 186;
                        type_description_1 = "ooooooooocoooooooo";
                        goto try_except_handler_10;
                    }
                    goto loop_start_6;
                    loop_end_6:;
                    goto try_end_5;
                    // Exception handler code:
                    try_except_handler_10:;
                    exception_keeper_type_7 = exception_type;
                    exception_keeper_value_7 = exception_value;
                    exception_keeper_tb_7 = exception_tb;
                    exception_keeper_lineno_7 = exception_lineno;
                    exception_type = NULL;
                    exception_value = NULL;
                    exception_tb = NULL;
                    exception_lineno = 0;

                    Py_XDECREF( tmp_for_loop_4__iter_value );
                    tmp_for_loop_4__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
                    Py_DECREF( tmp_for_loop_4__for_iterator );
                    tmp_for_loop_4__for_iterator = NULL;

                    // Re-raise.
                    exception_type = exception_keeper_type_7;
                    exception_value = exception_keeper_value_7;
                    exception_tb = exception_keeper_tb_7;
                    exception_lineno = exception_keeper_lineno_7;

                    goto try_except_handler_5;
                    // End of try:
                    try_end_5:;
                    Py_XDECREF( tmp_for_loop_4__iter_value );
                    tmp_for_loop_4__iter_value = NULL;

                    CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
                    Py_DECREF( tmp_for_loop_4__for_iterator );
                    tmp_for_loop_4__for_iterator = NULL;

                    branch_end_9:;
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 162;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_5;
                }
                goto loop_start_3;
                loop_end_3:;
                goto try_end_6;
                // Exception handler code:
                try_except_handler_5:;
                exception_keeper_type_8 = exception_type;
                exception_keeper_value_8 = exception_value;
                exception_keeper_tb_8 = exception_tb;
                exception_keeper_lineno_8 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_for_loop_1__iter_value );
                tmp_for_loop_1__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                Py_DECREF( tmp_for_loop_1__for_iterator );
                tmp_for_loop_1__for_iterator = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_8;
                exception_value = exception_keeper_value_8;
                exception_tb = exception_keeper_tb_8;
                exception_lineno = exception_keeper_lineno_8;

                goto frame_exception_exit_1;
                // End of try:
                try_end_6:;
                Py_XDECREF( tmp_for_loop_1__iter_value );
                tmp_for_loop_1__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                Py_DECREF( tmp_for_loop_1__for_iterator );
                tmp_for_loop_1__for_iterator = NULL;

                branch_no_8:;
            }
            branch_no_7:;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_compexpr_left_8;
            PyObject *tmp_compexpr_right_8;
            PyObject *tmp_len_arg_4;
            if ( var_name_dicts == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "name_dicts" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 188;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }

            tmp_len_arg_4 = var_name_dicts;
            tmp_compexpr_left_8 = BUILTIN_LEN( tmp_len_arg_4 );
            if ( tmp_compexpr_left_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 188;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_8 = const_int_pos_1;
            tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
            Py_DECREF( tmp_compexpr_left_8 );
            assert( !(tmp_res == -1) );
            tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_assign_source_37;
                PyObject *tmp_called_name_6;
                PyObject *tmp_mvar_value_9;
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ContextSet );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextSet );
                }

                if ( tmp_mvar_value_9 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextSet" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 189;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_6 = tmp_mvar_value_9;
                frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 189;
                tmp_assign_source_37 = CALL_FUNCTION_NO_ARGS( tmp_called_name_6 );
                if ( tmp_assign_source_37 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 189;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_result == NULL );
                var_result = tmp_assign_source_37;
            }
            {
                PyObject *tmp_assign_source_38;
                PyObject *tmp_iter_arg_8;
                if ( var_name_dicts == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "name_dicts" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 190;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_iter_arg_8 = var_name_dicts;
                tmp_assign_source_38 = MAKE_ITERATOR( tmp_iter_arg_8 );
                if ( tmp_assign_source_38 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 190;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_for_loop_5__for_iterator == NULL );
                tmp_for_loop_5__for_iterator = tmp_assign_source_38;
            }
            // Tried code:
            loop_start_7:;
            {
                PyObject *tmp_next_source_6;
                PyObject *tmp_assign_source_39;
                CHECK_OBJECT( tmp_for_loop_5__for_iterator );
                tmp_next_source_6 = tmp_for_loop_5__for_iterator;
                tmp_assign_source_39 = ITERATOR_NEXT( tmp_next_source_6 );
                if ( tmp_assign_source_39 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_7;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "ooooooooocoooooooo";
                        exception_lineno = 190;
                        goto try_except_handler_11;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_5__iter_value;
                    tmp_for_loop_5__iter_value = tmp_assign_source_39;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_40;
                CHECK_OBJECT( tmp_for_loop_5__iter_value );
                tmp_assign_source_40 = tmp_for_loop_5__iter_value;
                {
                    PyObject *old = var_name_dict;
                    var_name_dict = tmp_assign_source_40;
                    Py_INCREF( var_name_dict );
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_41;
                PyObject *tmp_called_name_7;
                PyObject *tmp_source_name_15;
                PyObject *tmp_mvar_value_10;
                PyObject *tmp_args_element_name_13;
                PyObject *tmp_args_element_name_14;
                PyObject *tmp_args_element_name_15;
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_helpers );

                if (unlikely( tmp_mvar_value_10 == NULL ))
                {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
                }

                if ( tmp_mvar_value_10 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 191;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_12;
                }

                tmp_source_name_15 = tmp_mvar_value_10;
                tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_predefine_names );
                if ( tmp_called_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 191;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_12;
                }
                CHECK_OBJECT( par_context );
                tmp_args_element_name_13 = par_context;
                if ( var_if_stmt == NULL )
                {
                    Py_DECREF( tmp_called_name_7 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "if_stmt" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 191;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_12;
                }

                tmp_args_element_name_14 = var_if_stmt;
                CHECK_OBJECT( var_name_dict );
                tmp_args_element_name_15 = var_name_dict;
                frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 191;
                {
                    PyObject *call_args[] = { tmp_args_element_name_13, tmp_args_element_name_14, tmp_args_element_name_15 };
                    tmp_assign_source_41 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, call_args );
                }

                Py_DECREF( tmp_called_name_7 );
                if ( tmp_assign_source_41 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 191;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_12;
                }
                {
                    PyObject *old = tmp_with_1__source;
                    tmp_with_1__source = tmp_assign_source_41;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_42;
                PyObject *tmp_called_name_8;
                PyObject *tmp_source_name_16;
                CHECK_OBJECT( tmp_with_1__source );
                tmp_source_name_16 = tmp_with_1__source;
                tmp_called_name_8 = LOOKUP_SPECIAL( tmp_source_name_16, const_str_plain___enter__ );
                if ( tmp_called_name_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 191;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_12;
                }
                frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 191;
                tmp_assign_source_42 = CALL_FUNCTION_NO_ARGS( tmp_called_name_8 );
                Py_DECREF( tmp_called_name_8 );
                if ( tmp_assign_source_42 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 191;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_12;
                }
                {
                    PyObject *old = tmp_with_1__enter;
                    tmp_with_1__enter = tmp_assign_source_42;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_43;
                PyObject *tmp_source_name_17;
                CHECK_OBJECT( tmp_with_1__source );
                tmp_source_name_17 = tmp_with_1__source;
                tmp_assign_source_43 = LOOKUP_SPECIAL( tmp_source_name_17, const_str_plain___exit__ );
                if ( tmp_assign_source_43 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 191;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_12;
                }
                {
                    PyObject *old = tmp_with_1__exit;
                    tmp_with_1__exit = tmp_assign_source_43;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_44;
                tmp_assign_source_44 = Py_True;
                {
                    PyObject *old = tmp_with_1__indicator;
                    tmp_with_1__indicator = tmp_assign_source_44;
                    Py_INCREF( tmp_with_1__indicator );
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            // Tried code:
            {
                PyObject *tmp_assign_source_45;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_called_name_9;
                PyObject *tmp_mvar_value_11;
                PyObject *tmp_args_element_name_16;
                PyObject *tmp_args_element_name_17;
                if ( var_result == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "result" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 192;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_14;
                }

                tmp_left_name_3 = var_result;
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_eval_node );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eval_node );
                }

                if ( tmp_mvar_value_11 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eval_node" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 192;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_14;
                }

                tmp_called_name_9 = tmp_mvar_value_11;
                CHECK_OBJECT( par_context );
                tmp_args_element_name_16 = par_context;
                CHECK_OBJECT( par_element );
                tmp_args_element_name_17 = par_element;
                frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 192;
                {
                    PyObject *call_args[] = { tmp_args_element_name_16, tmp_args_element_name_17 };
                    tmp_right_name_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_9, call_args );
                }

                if ( tmp_right_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_14;
                }
                tmp_result = BINARY_OPERATION_INPLACE( PyNumber_InPlaceOr, &tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 192;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_14;
                }
                tmp_assign_source_45 = tmp_left_name_3;
                var_result = tmp_assign_source_45;

            }
            goto try_end_7;
            // Exception handler code:
            try_except_handler_14:;
            exception_keeper_type_9 = exception_type;
            exception_keeper_value_9 = exception_value;
            exception_keeper_tb_9 = exception_tb;
            exception_keeper_lineno_9 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Preserve existing published exception.
            exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_type_1 );
            exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_value_1 );
            exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( exception_preserved_tb_1 );

            if ( exception_keeper_tb_9 == NULL )
            {
                exception_keeper_tb_9 = MAKE_TRACEBACK( frame_96fcd1e076c30934fa2f8245d9f4ab88, exception_keeper_lineno_9 );
            }
            else if ( exception_keeper_lineno_9 != 0 )
            {
                exception_keeper_tb_9 = ADD_TRACEBACK( exception_keeper_tb_9, frame_96fcd1e076c30934fa2f8245d9f4ab88, exception_keeper_lineno_9 );
            }

            NORMALIZE_EXCEPTION( &exception_keeper_type_9, &exception_keeper_value_9, &exception_keeper_tb_9 );
            PyException_SetTraceback( exception_keeper_value_9, (PyObject *)exception_keeper_tb_9 );
            PUBLISH_EXCEPTION( &exception_keeper_type_9, &exception_keeper_value_9, &exception_keeper_tb_9 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_12;
                PyObject *tmp_compexpr_left_9;
                PyObject *tmp_compexpr_right_9;
                tmp_compexpr_left_9 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_9 = PyExc_BaseException;
                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_9, tmp_compexpr_right_9 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 191;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_15;
                }
                tmp_condition_result_12 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_12;
                }
                else
                {
                    goto branch_no_12;
                }
                branch_yes_12:;
                {
                    PyObject *tmp_assign_source_46;
                    tmp_assign_source_46 = Py_False;
                    {
                        PyObject *old = tmp_with_1__indicator;
                        assert( old != NULL );
                        tmp_with_1__indicator = tmp_assign_source_46;
                        Py_INCREF( tmp_with_1__indicator );
                        Py_DECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_13;
                    PyObject *tmp_operand_name_2;
                    PyObject *tmp_called_name_10;
                    PyObject *tmp_args_element_name_18;
                    PyObject *tmp_args_element_name_19;
                    PyObject *tmp_args_element_name_20;
                    CHECK_OBJECT( tmp_with_1__exit );
                    tmp_called_name_10 = tmp_with_1__exit;
                    tmp_args_element_name_18 = EXC_TYPE(PyThreadState_GET());
                    tmp_args_element_name_19 = EXC_VALUE(PyThreadState_GET());
                    tmp_args_element_name_20 = EXC_TRACEBACK(PyThreadState_GET());
                    frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 192;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_18, tmp_args_element_name_19, tmp_args_element_name_20 };
                        tmp_operand_name_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_10, call_args );
                    }

                    if ( tmp_operand_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 192;
                        type_description_1 = "ooooooooocoooooooo";
                        goto try_except_handler_15;
                    }
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                    Py_DECREF( tmp_operand_name_2 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 192;
                        type_description_1 = "ooooooooocoooooooo";
                        goto try_except_handler_15;
                    }
                    tmp_condition_result_13 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_13;
                    }
                    else
                    {
                        goto branch_no_13;
                    }
                    branch_yes_13:;
                    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    if (unlikely( tmp_result == false ))
                    {
                        exception_lineno = 192;
                    }

                    if (exception_tb && exception_tb->tb_frame == &frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame) frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = exception_tb->tb_lineno;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_15;
                    branch_no_13:;
                }
                goto branch_end_12;
                branch_no_12:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 191;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame) frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "ooooooooocoooooooo";
                goto try_except_handler_15;
                branch_end_12:;
            }
            goto try_end_8;
            // Exception handler code:
            try_except_handler_15:;
            exception_keeper_type_10 = exception_type;
            exception_keeper_value_10 = exception_value;
            exception_keeper_tb_10 = exception_tb;
            exception_keeper_lineno_10 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            // Re-raise.
            exception_type = exception_keeper_type_10;
            exception_value = exception_keeper_value_10;
            exception_tb = exception_keeper_tb_10;
            exception_lineno = exception_keeper_lineno_10;

            goto try_except_handler_13;
            // End of try:
            try_end_8:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            goto try_end_7;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_5_eval_element );
            return NULL;
            // End of try:
            try_end_7:;
            goto try_end_9;
            // Exception handler code:
            try_except_handler_13:;
            exception_keeper_type_11 = exception_type;
            exception_keeper_value_11 = exception_value;
            exception_keeper_tb_11 = exception_tb;
            exception_keeper_lineno_11 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            {
                nuitka_bool tmp_condition_result_14;
                nuitka_bool tmp_compexpr_left_10;
                nuitka_bool tmp_compexpr_right_10;
                int tmp_truth_name_5;
                CHECK_OBJECT( tmp_with_1__indicator );
                tmp_truth_name_5 = CHECK_IF_TRUE( tmp_with_1__indicator );
                if ( tmp_truth_name_5 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    Py_DECREF( exception_keeper_type_11 );
                    Py_XDECREF( exception_keeper_value_11 );
                    Py_XDECREF( exception_keeper_tb_11 );

                    exception_lineno = 191;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_12;
                }
                tmp_compexpr_left_10 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_compexpr_right_10 = NUITKA_BOOL_TRUE;
                tmp_condition_result_14 = ( tmp_compexpr_left_10 == tmp_compexpr_right_10 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_14;
                }
                else
                {
                    goto branch_no_14;
                }
                branch_yes_14:;
                {
                    PyObject *tmp_called_name_11;
                    PyObject *tmp_call_result_3;
                    CHECK_OBJECT( tmp_with_1__exit );
                    tmp_called_name_11 = tmp_with_1__exit;
                    frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 192;
                    tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                    if ( tmp_call_result_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        Py_DECREF( exception_keeper_type_11 );
                        Py_XDECREF( exception_keeper_value_11 );
                        Py_XDECREF( exception_keeper_tb_11 );

                        exception_lineno = 192;
                        type_description_1 = "ooooooooocoooooooo";
                        goto try_except_handler_12;
                    }
                    Py_DECREF( tmp_call_result_3 );
                }
                branch_no_14:;
            }
            // Re-raise.
            exception_type = exception_keeper_type_11;
            exception_value = exception_keeper_value_11;
            exception_tb = exception_keeper_tb_11;
            exception_lineno = exception_keeper_lineno_11;

            goto try_except_handler_12;
            // End of try:
            try_end_9:;
            {
                nuitka_bool tmp_condition_result_15;
                nuitka_bool tmp_compexpr_left_11;
                nuitka_bool tmp_compexpr_right_11;
                int tmp_truth_name_6;
                CHECK_OBJECT( tmp_with_1__indicator );
                tmp_truth_name_6 = CHECK_IF_TRUE( tmp_with_1__indicator );
                if ( tmp_truth_name_6 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 191;
                    type_description_1 = "ooooooooocoooooooo";
                    goto try_except_handler_12;
                }
                tmp_compexpr_left_11 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_compexpr_right_11 = NUITKA_BOOL_TRUE;
                tmp_condition_result_15 = ( tmp_compexpr_left_11 == tmp_compexpr_right_11 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_15;
                }
                else
                {
                    goto branch_no_15;
                }
                branch_yes_15:;
                {
                    PyObject *tmp_called_name_12;
                    PyObject *tmp_call_result_4;
                    CHECK_OBJECT( tmp_with_1__exit );
                    tmp_called_name_12 = tmp_with_1__exit;
                    frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 192;
                    tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                    if ( tmp_call_result_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 192;
                        type_description_1 = "ooooooooocoooooooo";
                        goto try_except_handler_12;
                    }
                    Py_DECREF( tmp_call_result_4 );
                }
                branch_no_15:;
            }
            goto try_end_10;
            // Exception handler code:
            try_except_handler_12:;
            exception_keeper_type_12 = exception_type;
            exception_keeper_value_12 = exception_value;
            exception_keeper_tb_12 = exception_tb;
            exception_keeper_lineno_12 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_with_1__source );
            tmp_with_1__source = NULL;

            Py_XDECREF( tmp_with_1__enter );
            tmp_with_1__enter = NULL;

            Py_XDECREF( tmp_with_1__exit );
            tmp_with_1__exit = NULL;

            Py_XDECREF( tmp_with_1__indicator );
            tmp_with_1__indicator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_12;
            exception_value = exception_keeper_value_12;
            exception_tb = exception_keeper_tb_12;
            exception_lineno = exception_keeper_lineno_12;

            goto try_except_handler_11;
            // End of try:
            try_end_10:;
            CHECK_OBJECT( (PyObject *)tmp_with_1__source );
            Py_DECREF( tmp_with_1__source );
            tmp_with_1__source = NULL;

            CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
            Py_DECREF( tmp_with_1__enter );
            tmp_with_1__enter = NULL;

            CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
            Py_DECREF( tmp_with_1__exit );
            tmp_with_1__exit = NULL;

            Py_XDECREF( tmp_with_1__indicator );
            tmp_with_1__indicator = NULL;

            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;
                type_description_1 = "ooooooooocoooooooo";
                goto try_except_handler_11;
            }
            goto loop_start_7;
            loop_end_7:;
            goto try_end_11;
            // Exception handler code:
            try_except_handler_11:;
            exception_keeper_type_13 = exception_type;
            exception_keeper_value_13 = exception_value;
            exception_keeper_tb_13 = exception_tb;
            exception_keeper_lineno_13 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_5__iter_value );
            tmp_for_loop_5__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_5__for_iterator );
            Py_DECREF( tmp_for_loop_5__for_iterator );
            tmp_for_loop_5__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_13;
            exception_value = exception_keeper_value_13;
            exception_tb = exception_keeper_tb_13;
            exception_lineno = exception_keeper_lineno_13;

            goto frame_exception_exit_1;
            // End of try:
            try_end_11:;
            Py_XDECREF( tmp_for_loop_5__iter_value );
            tmp_for_loop_5__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_5__for_iterator );
            Py_DECREF( tmp_for_loop_5__for_iterator );
            tmp_for_loop_5__for_iterator = NULL;

            if ( var_result == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "result" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 193;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }

            tmp_return_value = var_result;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            goto branch_end_11;
            branch_no_11:;
            {
                PyObject *tmp_called_instance_6;
                PyObject *tmp_args_element_name_21;
                PyObject *tmp_args_element_name_22;
                CHECK_OBJECT( par_self );
                tmp_called_instance_6 = par_self;
                CHECK_OBJECT( par_context );
                tmp_args_element_name_21 = par_context;
                CHECK_OBJECT( par_element );
                tmp_args_element_name_22 = par_element;
                frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 195;
                {
                    PyObject *call_args[] = { tmp_args_element_name_21, tmp_args_element_name_22 };
                    tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_6, const_str_plain__eval_element_if_evaluated, call_args );
                }

                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 195;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            branch_end_11:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            nuitka_bool tmp_condition_result_16;
            int tmp_truth_name_7;
            CHECK_OBJECT( var_predefined_if_name_dict );
            tmp_truth_name_7 = CHECK_IF_TRUE( var_predefined_if_name_dict );
            if ( tmp_truth_name_7 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 197;
                type_description_1 = "ooooooooocoooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_16 = tmp_truth_name_7 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_16;
            }
            else
            {
                goto branch_no_16;
            }
            branch_yes_16:;
            {
                PyObject *tmp_called_name_13;
                PyObject *tmp_mvar_value_12;
                PyObject *tmp_args_element_name_23;
                PyObject *tmp_args_element_name_24;
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_eval_node );

                if (unlikely( tmp_mvar_value_12 == NULL ))
                {
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eval_node );
                }

                if ( tmp_mvar_value_12 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eval_node" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 198;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_13 = tmp_mvar_value_12;
                CHECK_OBJECT( par_context );
                tmp_args_element_name_23 = par_context;
                CHECK_OBJECT( par_element );
                tmp_args_element_name_24 = par_element;
                frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 198;
                {
                    PyObject *call_args[] = { tmp_args_element_name_23, tmp_args_element_name_24 };
                    tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_13, call_args );
                }

                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 198;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            goto branch_end_16;
            branch_no_16:;
            {
                PyObject *tmp_called_instance_7;
                PyObject *tmp_args_element_name_25;
                PyObject *tmp_args_element_name_26;
                CHECK_OBJECT( par_self );
                tmp_called_instance_7 = par_self;
                CHECK_OBJECT( par_context );
                tmp_args_element_name_25 = par_context;
                CHECK_OBJECT( par_element );
                tmp_args_element_name_26 = par_element;
                frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame.f_lineno = 200;
                {
                    PyObject *call_args[] = { tmp_args_element_name_25, tmp_args_element_name_26 };
                    tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_7, const_str_plain__eval_element_if_evaluated, call_args );
                }

                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 200;
                    type_description_1 = "ooooooooocoooooooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            branch_end_16:;
        }
        branch_end_6:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_96fcd1e076c30934fa2f8245d9f4ab88 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_96fcd1e076c30934fa2f8245d9f4ab88 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_96fcd1e076c30934fa2f8245d9f4ab88 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_96fcd1e076c30934fa2f8245d9f4ab88, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_96fcd1e076c30934fa2f8245d9f4ab88->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_96fcd1e076c30934fa2f8245d9f4ab88, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_96fcd1e076c30934fa2f8245d9f4ab88,
        type_description_1,
        par_self,
        par_context,
        par_element,
        var_if_stmt,
        var_predefined_if_name_dict,
        var_if_stmt_test,
        var_name_dicts,
        var_if_names,
        var_element_names,
        var_str_element_names,
        var_if_name,
        var_definitions,
        var_original_name_dicts,
        var_definition,
        var_new_name_dicts,
        var_i,
        var_name_dict,
        var_result
    );


    // Release cached frame.
    if ( frame_96fcd1e076c30934fa2f8245d9f4ab88 == cache_frame_96fcd1e076c30934fa2f8245d9f4ab88 )
    {
        Py_DECREF( frame_96fcd1e076c30934fa2f8245d9f4ab88 );
    }
    cache_frame_96fcd1e076c30934fa2f8245d9f4ab88 = NULL;

    assertFrameObject( frame_96fcd1e076c30934fa2f8245d9f4ab88 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_5_eval_element );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_element );
    Py_DECREF( par_element );
    par_element = NULL;

    Py_XDECREF( var_if_stmt );
    var_if_stmt = NULL;

    Py_XDECREF( var_predefined_if_name_dict );
    var_predefined_if_name_dict = NULL;

    Py_XDECREF( var_if_stmt_test );
    var_if_stmt_test = NULL;

    Py_XDECREF( var_name_dicts );
    var_name_dicts = NULL;

    Py_XDECREF( var_if_names );
    var_if_names = NULL;

    Py_XDECREF( var_element_names );
    var_element_names = NULL;

    CHECK_OBJECT( (PyObject *)var_str_element_names );
    Py_DECREF( var_str_element_names );
    var_str_element_names = NULL;

    Py_XDECREF( var_if_name );
    var_if_name = NULL;

    Py_XDECREF( var_definitions );
    var_definitions = NULL;

    Py_XDECREF( var_original_name_dicts );
    var_original_name_dicts = NULL;

    Py_XDECREF( var_definition );
    var_definition = NULL;

    Py_XDECREF( var_new_name_dicts );
    var_new_name_dicts = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_name_dict );
    var_name_dict = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_14 = exception_type;
    exception_keeper_value_14 = exception_value;
    exception_keeper_tb_14 = exception_tb;
    exception_keeper_lineno_14 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_element );
    Py_DECREF( par_element );
    par_element = NULL;

    Py_XDECREF( var_if_stmt );
    var_if_stmt = NULL;

    Py_XDECREF( var_predefined_if_name_dict );
    var_predefined_if_name_dict = NULL;

    Py_XDECREF( var_if_stmt_test );
    var_if_stmt_test = NULL;

    Py_XDECREF( var_name_dicts );
    var_name_dicts = NULL;

    Py_XDECREF( var_if_names );
    var_if_names = NULL;

    Py_XDECREF( var_element_names );
    var_element_names = NULL;

    CHECK_OBJECT( (PyObject *)var_str_element_names );
    Py_DECREF( var_str_element_names );
    var_str_element_names = NULL;

    Py_XDECREF( var_if_name );
    var_if_name = NULL;

    Py_XDECREF( var_definitions );
    var_definitions = NULL;

    Py_XDECREF( var_original_name_dicts );
    var_original_name_dicts = NULL;

    Py_XDECREF( var_definition );
    var_definition = NULL;

    Py_XDECREF( var_new_name_dicts );
    var_new_name_dicts = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_name_dict );
    var_name_dict = NULL;

    Py_XDECREF( var_result );
    var_result = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_14;
    exception_value = exception_keeper_value_14;
    exception_tb = exception_keeper_tb_14;
    exception_lineno = exception_keeper_lineno_14;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_5_eval_element );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$$$function_5_eval_element$$$genexpr_1_genexpr_locals {
    PyObject *var_i;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$$$function_5_eval_element$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$$$function_5_eval_element$$$genexpr_1_genexpr_locals *generator_heap = (struct jedi$evaluate$$$function_5_eval_element$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_i = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_0a37e2250f2dffd74910d10ed3e9b15c, module_jedi$evaluate, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Noc";
                generator_heap->exception_lineno = 161;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_i;
            generator_heap->var_i = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_i );
        tmp_source_name_1 = generator_heap->var_i;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_value );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 161;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        if ( PyCell_GET( generator->m_closure[1] ) == NULL )
        {
            Py_DECREF( tmp_compexpr_left_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "str_element_names" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 161;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }

        tmp_compexpr_right_1 = PyCell_GET( generator->m_closure[1] );
        generator_heap->tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( generator_heap->tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 161;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        tmp_expression_name_1 = ( generator_heap->tmp_res == 1 ) ? Py_True : Py_False;
        Py_INCREF( tmp_expression_name_1 );
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_compexpr_left_1, sizeof(PyObject *), &tmp_compexpr_right_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 161;
            generator_heap->type_description_1 = "Noc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 161;
        generator_heap->type_description_1 = "Noc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_i,
            generator->m_closure[1]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$$$function_5_eval_element$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$$$function_5_eval_element$$$genexpr_1_genexpr_context,
        module_jedi$evaluate,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_387b7fddc472ee6c95ed1bbe113171cf,
#endif
        codeobj_0a37e2250f2dffd74910d10ed3e9b15c,
        2,
        sizeof(struct jedi$evaluate$$$function_5_eval_element$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_jedi$evaluate$$$function_6__eval_element_if_evaluated( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_context = python_pars[ 1 ];
    PyObject *par_element = python_pars[ 2 ];
    PyObject *var_parent = NULL;
    PyObject *var_predefined_if_name_dict = NULL;
    struct Nuitka_FrameObject *frame_f4732f2d02c8981381ba2a86f8ad96ad;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_f4732f2d02c8981381ba2a86f8ad96ad = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( par_element );
        tmp_assign_source_1 = par_element;
        assert( var_parent == NULL );
        Py_INCREF( tmp_assign_source_1 );
        var_parent = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_f4732f2d02c8981381ba2a86f8ad96ad, codeobj_f4732f2d02c8981381ba2a86f8ad96ad, module_jedi$evaluate, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_f4732f2d02c8981381ba2a86f8ad96ad = cache_frame_f4732f2d02c8981381ba2a86f8ad96ad;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_f4732f2d02c8981381ba2a86f8ad96ad );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_f4732f2d02c8981381ba2a86f8ad96ad ) == 2 ); // Frame stack

    // Framed code:
    loop_start_1:;
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_parent );
        tmp_compexpr_left_1 = var_parent;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_end_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( var_parent );
        tmp_source_name_1 = var_parent;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_parent );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 208;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_parent;
            assert( old != NULL );
            var_parent = tmp_assign_source_2;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_context );
        tmp_source_name_2 = par_context;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_predefined_names );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 209;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_parent );
        tmp_args_element_name_1 = var_parent;
        frame_f4732f2d02c8981381ba2a86f8ad96ad->m_frame.f_lineno = 209;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 209;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_predefined_if_name_dict;
            var_predefined_if_name_dict = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_predefined_if_name_dict );
        tmp_compexpr_left_2 = var_predefined_if_name_dict;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_eval_node );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eval_node );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eval_node" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 211;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_context );
            tmp_args_element_name_2 = par_context;
            CHECK_OBJECT( par_element );
            tmp_args_element_name_3 = par_element;
            frame_f4732f2d02c8981381ba2a86f8ad96ad->m_frame.f_lineno = 211;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_2:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 207;
        type_description_1 = "ooooo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;
    loop_end_1:;
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        CHECK_OBJECT( par_self );
        tmp_called_instance_2 = par_self;
        CHECK_OBJECT( par_context );
        tmp_args_element_name_4 = par_context;
        CHECK_OBJECT( par_element );
        tmp_args_element_name_5 = par_element;
        frame_f4732f2d02c8981381ba2a86f8ad96ad->m_frame.f_lineno = 212;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain__eval_element_cached, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4732f2d02c8981381ba2a86f8ad96ad );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4732f2d02c8981381ba2a86f8ad96ad );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_f4732f2d02c8981381ba2a86f8ad96ad );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_f4732f2d02c8981381ba2a86f8ad96ad, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_f4732f2d02c8981381ba2a86f8ad96ad->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_f4732f2d02c8981381ba2a86f8ad96ad, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_f4732f2d02c8981381ba2a86f8ad96ad,
        type_description_1,
        par_self,
        par_context,
        par_element,
        var_parent,
        var_predefined_if_name_dict
    );


    // Release cached frame.
    if ( frame_f4732f2d02c8981381ba2a86f8ad96ad == cache_frame_f4732f2d02c8981381ba2a86f8ad96ad )
    {
        Py_DECREF( frame_f4732f2d02c8981381ba2a86f8ad96ad );
    }
    cache_frame_f4732f2d02c8981381ba2a86f8ad96ad = NULL;

    assertFrameObject( frame_f4732f2d02c8981381ba2a86f8ad96ad );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_6__eval_element_if_evaluated );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_element );
    Py_DECREF( par_element );
    par_element = NULL;

    Py_XDECREF( var_parent );
    var_parent = NULL;

    Py_XDECREF( var_predefined_if_name_dict );
    var_predefined_if_name_dict = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_element );
    Py_DECREF( par_element );
    par_element = NULL;

    Py_XDECREF( var_parent );
    var_parent = NULL;

    Py_XDECREF( var_predefined_if_name_dict );
    var_predefined_if_name_dict = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_6__eval_element_if_evaluated );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_7__eval_element_cached( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_context = python_pars[ 1 ];
    PyObject *par_element = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_a3a4783c51555953fecb657c25a9d529;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a3a4783c51555953fecb657c25a9d529 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a3a4783c51555953fecb657c25a9d529, codeobj_a3a4783c51555953fecb657c25a9d529, module_jedi$evaluate, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a3a4783c51555953fecb657c25a9d529 = cache_frame_a3a4783c51555953fecb657c25a9d529;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a3a4783c51555953fecb657c25a9d529 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a3a4783c51555953fecb657c25a9d529 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_eval_node );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eval_node );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eval_node" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 216;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_context );
        tmp_args_element_name_1 = par_context;
        CHECK_OBJECT( par_element );
        tmp_args_element_name_2 = par_element;
        frame_a3a4783c51555953fecb657c25a9d529->m_frame.f_lineno = 216;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a3a4783c51555953fecb657c25a9d529 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_a3a4783c51555953fecb657c25a9d529 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a3a4783c51555953fecb657c25a9d529 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a3a4783c51555953fecb657c25a9d529, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a3a4783c51555953fecb657c25a9d529->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a3a4783c51555953fecb657c25a9d529, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a3a4783c51555953fecb657c25a9d529,
        type_description_1,
        par_self,
        par_context,
        par_element
    );


    // Release cached frame.
    if ( frame_a3a4783c51555953fecb657c25a9d529 == cache_frame_a3a4783c51555953fecb657c25a9d529 )
    {
        Py_DECREF( frame_a3a4783c51555953fecb657c25a9d529 );
    }
    cache_frame_a3a4783c51555953fecb657c25a9d529 = NULL;

    assertFrameObject( frame_a3a4783c51555953fecb657c25a9d529 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_7__eval_element_cached );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_element );
    Py_DECREF( par_element );
    par_element = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_element );
    Py_DECREF( par_element );
    par_element = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_7__eval_element_cached );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_8_goto_definitions( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_context = python_pars[ 1 ];
    PyObject *par_name = python_pars[ 2 ];
    PyObject *var_def_ = NULL;
    PyObject *var_type_ = NULL;
    nuitka_bool var_is_simple_name = NUITKA_BOOL_UNASSIGNED;
    PyObject *var_container_types = NULL;
    PyObject *var_cn = NULL;
    PyObject *var_for_types = NULL;
    PyObject *var_c_node = NULL;
    struct Nuitka_FrameObject *frame_6236a960357bfdbc01ec332bbb5c85ed;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_6236a960357bfdbc01ec332bbb5c85ed = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6236a960357bfdbc01ec332bbb5c85ed, codeobj_6236a960357bfdbc01ec332bbb5c85ed, module_jedi$evaluate, sizeof(nuitka_bool)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6236a960357bfdbc01ec332bbb5c85ed = cache_frame_6236a960357bfdbc01ec332bbb5c85ed;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6236a960357bfdbc01ec332bbb5c85ed );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6236a960357bfdbc01ec332bbb5c85ed ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( par_name );
        tmp_source_name_1 = par_name;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_definition );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;
            type_description_1 = "oooooboooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_c420771dfdd3ba8cae6376435c084e1a );
        frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 219;
        tmp_assign_source_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;
            type_description_1 = "oooooboooo";
            goto frame_exception_exit_1;
        }
        assert( var_def_ == NULL );
        var_def_ = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_def_ );
        tmp_compexpr_left_1 = var_def_;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( var_def_ );
            tmp_source_name_2 = var_def_;
            tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_type );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 221;
                type_description_1 = "oooooboooo";
                goto frame_exception_exit_1;
            }
            assert( var_type_ == NULL );
            var_type_ = tmp_assign_source_2;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( var_type_ );
            tmp_compexpr_left_2 = var_type_;
            tmp_compexpr_right_2 = const_str_plain_classdef;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 222;
                type_description_1 = "oooooboooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_list_element_1;
                PyObject *tmp_called_name_2;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_args_element_name_3;
                PyObject *tmp_source_name_3;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ClassContext );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ClassContext );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ClassContext" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 223;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_2 = tmp_mvar_value_1;
                CHECK_OBJECT( par_self );
                tmp_args_element_name_1 = par_self;
                CHECK_OBJECT( par_context );
                tmp_args_element_name_2 = par_context;
                CHECK_OBJECT( par_name );
                tmp_source_name_3 = par_name;
                tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_parent );
                if ( tmp_args_element_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 223;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 223;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                    tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
                }

                Py_DECREF( tmp_args_element_name_3 );
                if ( tmp_list_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 223;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                tmp_return_value = PyList_New( 1 );
                PyList_SET_ITEM( tmp_return_value, 0, tmp_list_element_1 );
                goto frame_return_exit_1;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                CHECK_OBJECT( var_type_ );
                tmp_compexpr_left_3 = var_type_;
                tmp_compexpr_right_3 = const_str_plain_funcdef;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 224;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_list_element_2;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_source_name_4;
                    PyObject *tmp_mvar_value_2;
                    PyObject *tmp_args_element_name_4;
                    PyObject *tmp_args_element_name_5;
                    PyObject *tmp_source_name_5;
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_FunctionContext );

                    if (unlikely( tmp_mvar_value_2 == NULL ))
                    {
                        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FunctionContext );
                    }

                    if ( tmp_mvar_value_2 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FunctionContext" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 225;
                        type_description_1 = "oooooboooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_4 = tmp_mvar_value_2;
                    tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_from_context );
                    if ( tmp_called_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 225;
                        type_description_1 = "oooooboooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( par_context );
                    tmp_args_element_name_4 = par_context;
                    CHECK_OBJECT( par_name );
                    tmp_source_name_5 = par_name;
                    tmp_args_element_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_parent );
                    if ( tmp_args_element_name_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_3 );

                        exception_lineno = 225;
                        type_description_1 = "oooooboooo";
                        goto frame_exception_exit_1;
                    }
                    frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 225;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5 };
                        tmp_list_element_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
                    }

                    Py_DECREF( tmp_called_name_3 );
                    Py_DECREF( tmp_args_element_name_5 );
                    if ( tmp_list_element_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 225;
                        type_description_1 = "oooooboooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_return_value = PyList_New( 1 );
                    PyList_SET_ITEM( tmp_return_value, 0, tmp_list_element_2 );
                    goto frame_return_exit_1;
                }
                branch_no_3:;
            }
            branch_end_2:;
        }
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            CHECK_OBJECT( var_type_ );
            tmp_compexpr_left_4 = var_type_;
            tmp_compexpr_right_4 = const_str_plain_expr_stmt;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 227;
                type_description_1 = "oooooboooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                nuitka_bool tmp_assign_source_3;
                PyObject *tmp_compexpr_left_5;
                PyObject *tmp_compexpr_right_5;
                PyObject *tmp_source_name_6;
                PyObject *tmp_source_name_7;
                CHECK_OBJECT( par_name );
                tmp_source_name_7 = par_name;
                tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_parent );
                if ( tmp_source_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 228;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_type );
                Py_DECREF( tmp_source_name_6 );
                if ( tmp_compexpr_left_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 228;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_5 = const_tuple_str_plain_power_str_plain_trailer_tuple;
                tmp_res = PySequence_Contains( tmp_compexpr_right_5, tmp_compexpr_left_5 );
                Py_DECREF( tmp_compexpr_left_5 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 228;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                var_is_simple_name = tmp_assign_source_3;
            }
            {
                nuitka_bool tmp_condition_result_5;
                assert( var_is_simple_name != NUITKA_BOOL_UNASSIGNED);
                tmp_condition_result_5 = var_is_simple_name;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_called_name_4;
                    PyObject *tmp_mvar_value_3;
                    PyObject *tmp_args_element_name_6;
                    PyObject *tmp_args_element_name_7;
                    PyObject *tmp_args_element_name_8;
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_eval_expr_stmt );

                    if (unlikely( tmp_mvar_value_3 == NULL ))
                    {
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eval_expr_stmt );
                    }

                    if ( tmp_mvar_value_3 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eval_expr_stmt" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 230;
                        type_description_1 = "oooooboooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_4 = tmp_mvar_value_3;
                    CHECK_OBJECT( par_context );
                    tmp_args_element_name_6 = par_context;
                    CHECK_OBJECT( var_def_ );
                    tmp_args_element_name_7 = var_def_;
                    CHECK_OBJECT( par_name );
                    tmp_args_element_name_8 = par_name;
                    frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 230;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7, tmp_args_element_name_8 };
                        tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
                    }

                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 230;
                        type_description_1 = "oooooboooo";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                branch_no_5:;
            }
            branch_no_4:;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( var_type_ );
            tmp_compexpr_left_6 = var_type_;
            tmp_compexpr_right_6 = const_str_plain_for_stmt;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;
                type_description_1 = "oooooboooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_6;
            }
            else
            {
                goto branch_no_6;
            }
            branch_yes_6:;
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_called_name_5;
                PyObject *tmp_source_name_8;
                PyObject *tmp_args_element_name_9;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_source_name_9;
                PyObject *tmp_subscript_name_1;
                CHECK_OBJECT( par_context );
                tmp_source_name_8 = par_context;
                tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_eval_node );
                if ( tmp_called_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 232;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_def_ );
                tmp_source_name_9 = var_def_;
                tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_children );
                if ( tmp_subscribed_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_5 );

                    exception_lineno = 232;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                tmp_subscript_name_1 = const_int_pos_3;
                tmp_args_element_name_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 3 );
                Py_DECREF( tmp_subscribed_name_1 );
                if ( tmp_args_element_name_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_5 );

                    exception_lineno = 232;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 232;
                {
                    PyObject *call_args[] = { tmp_args_element_name_9 };
                    tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
                }

                Py_DECREF( tmp_called_name_5 );
                Py_DECREF( tmp_args_element_name_9 );
                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 232;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                assert( var_container_types == NULL );
                var_container_types = tmp_assign_source_4;
            }
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_called_name_6;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_args_element_name_10;
                PyObject *tmp_args_element_name_11;
                PyObject *tmp_subscribed_name_2;
                PyObject *tmp_source_name_10;
                PyObject *tmp_subscript_name_2;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ContextualizedNode );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextualizedNode );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextualizedNode" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 233;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_6 = tmp_mvar_value_4;
                CHECK_OBJECT( par_context );
                tmp_args_element_name_10 = par_context;
                CHECK_OBJECT( var_def_ );
                tmp_source_name_10 = var_def_;
                tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_children );
                if ( tmp_subscribed_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 233;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                tmp_subscript_name_2 = const_int_pos_3;
                tmp_args_element_name_11 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 3 );
                Py_DECREF( tmp_subscribed_name_2 );
                if ( tmp_args_element_name_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 233;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 233;
                {
                    PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11 };
                    tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
                }

                Py_DECREF( tmp_args_element_name_11 );
                if ( tmp_assign_source_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 233;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                assert( var_cn == NULL );
                var_cn = tmp_assign_source_5;
            }
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_called_name_7;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_args_element_name_12;
                PyObject *tmp_args_element_name_13;
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_iterate_contexts );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iterate_contexts );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iterate_contexts" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 234;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_7 = tmp_mvar_value_5;
                CHECK_OBJECT( var_container_types );
                tmp_args_element_name_12 = var_container_types;
                CHECK_OBJECT( var_cn );
                tmp_args_element_name_13 = var_cn;
                frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 234;
                {
                    PyObject *call_args[] = { tmp_args_element_name_12, tmp_args_element_name_13 };
                    tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_7, call_args );
                }

                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                assert( var_for_types == NULL );
                var_for_types = tmp_assign_source_6;
            }
            {
                PyObject *tmp_assign_source_7;
                PyObject *tmp_called_name_8;
                PyObject *tmp_mvar_value_6;
                PyObject *tmp_args_element_name_14;
                PyObject *tmp_args_element_name_15;
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ContextualizedName );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ContextualizedName );
                }

                if ( tmp_mvar_value_6 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ContextualizedName" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 235;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_8 = tmp_mvar_value_6;
                CHECK_OBJECT( par_context );
                tmp_args_element_name_14 = par_context;
                CHECK_OBJECT( par_name );
                tmp_args_element_name_15 = par_name;
                frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 235;
                {
                    PyObject *call_args[] = { tmp_args_element_name_14, tmp_args_element_name_15 };
                    tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, call_args );
                }

                if ( tmp_assign_source_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 235;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                assert( var_c_node == NULL );
                var_c_node = tmp_assign_source_7;
            }
            {
                PyObject *tmp_called_name_9;
                PyObject *tmp_mvar_value_7;
                PyObject *tmp_args_element_name_16;
                PyObject *tmp_args_element_name_17;
                PyObject *tmp_args_element_name_18;
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_check_tuple_assignments );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_check_tuple_assignments );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "check_tuple_assignments" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 236;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_9 = tmp_mvar_value_7;
                CHECK_OBJECT( par_self );
                tmp_args_element_name_16 = par_self;
                CHECK_OBJECT( var_c_node );
                tmp_args_element_name_17 = var_c_node;
                CHECK_OBJECT( var_for_types );
                tmp_args_element_name_18 = var_for_types;
                frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 236;
                {
                    PyObject *call_args[] = { tmp_args_element_name_16, tmp_args_element_name_17, tmp_args_element_name_18 };
                    tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_9, call_args );
                }

                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 236;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            branch_no_6:;
        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            CHECK_OBJECT( var_type_ );
            tmp_compexpr_left_7 = var_type_;
            tmp_compexpr_right_7 = const_tuple_str_plain_import_from_str_plain_import_name_tuple;
            tmp_res = PySequence_Contains( tmp_compexpr_right_7, tmp_compexpr_left_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 237;
                type_description_1 = "oooooboooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_7 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_called_instance_1;
                PyObject *tmp_mvar_value_8;
                PyObject *tmp_args_element_name_19;
                PyObject *tmp_args_element_name_20;
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_imports );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_imports );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "imports" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 238;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_instance_1 = tmp_mvar_value_8;
                CHECK_OBJECT( par_context );
                tmp_args_element_name_19 = par_context;
                CHECK_OBJECT( par_name );
                tmp_args_element_name_20 = par_name;
                frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 238;
                {
                    PyObject *call_args[] = { tmp_args_element_name_19, tmp_args_element_name_20 };
                    tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_infer_import, call_args );
                }

                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 238;
                    type_description_1 = "oooooboooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            branch_no_7:;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_args_element_name_22;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_helpers );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 240;
            type_description_1 = "oooooboooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_9;
        CHECK_OBJECT( par_context );
        tmp_args_element_name_21 = par_context;
        CHECK_OBJECT( par_name );
        tmp_args_element_name_22 = par_name;
        frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame.f_lineno = 240;
        {
            PyObject *call_args[] = { tmp_args_element_name_21, tmp_args_element_name_22 };
            tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_evaluate_call_of_leaf, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 240;
            type_description_1 = "oooooboooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6236a960357bfdbc01ec332bbb5c85ed );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6236a960357bfdbc01ec332bbb5c85ed );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6236a960357bfdbc01ec332bbb5c85ed );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6236a960357bfdbc01ec332bbb5c85ed, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6236a960357bfdbc01ec332bbb5c85ed->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6236a960357bfdbc01ec332bbb5c85ed, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6236a960357bfdbc01ec332bbb5c85ed,
        type_description_1,
        par_self,
        par_context,
        par_name,
        var_def_,
        var_type_,
        (int)var_is_simple_name,
        var_container_types,
        var_cn,
        var_for_types,
        var_c_node
    );


    // Release cached frame.
    if ( frame_6236a960357bfdbc01ec332bbb5c85ed == cache_frame_6236a960357bfdbc01ec332bbb5c85ed )
    {
        Py_DECREF( frame_6236a960357bfdbc01ec332bbb5c85ed );
    }
    cache_frame_6236a960357bfdbc01ec332bbb5c85ed = NULL;

    assertFrameObject( frame_6236a960357bfdbc01ec332bbb5c85ed );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_8_goto_definitions );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)var_def_ );
    Py_DECREF( var_def_ );
    var_def_ = NULL;

    Py_XDECREF( var_type_ );
    var_type_ = NULL;

    Py_XDECREF( var_container_types );
    var_container_types = NULL;

    Py_XDECREF( var_cn );
    var_cn = NULL;

    Py_XDECREF( var_for_types );
    var_for_types = NULL;

    Py_XDECREF( var_c_node );
    var_c_node = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_def_ );
    var_def_ = NULL;

    Py_XDECREF( var_type_ );
    var_type_ = NULL;

    Py_XDECREF( var_container_types );
    var_container_types = NULL;

    Py_XDECREF( var_cn );
    var_cn = NULL;

    Py_XDECREF( var_for_types );
    var_for_types = NULL;

    Py_XDECREF( var_c_node );
    var_c_node = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_8_goto_definitions );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_9_goto( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_CellObject *par_context = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_name = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *var_definition = NULL;
    PyObject *var_type_ = NULL;
    nuitka_bool var_is_simple_name = NUITKA_BOOL_UNASSIGNED;
    PyObject *var_module_names = NULL;
    PyObject *var_par = NULL;
    PyObject *var_node_type = NULL;
    PyObject *var_trailer = NULL;
    PyObject *var_context_set = NULL;
    PyObject *var_i = NULL;
    PyObject *var_to_evaluate = NULL;
    PyObject *var_param_names = NULL;
    PyObject *var_get_param_names = NULL;
    PyObject *var_param_name = NULL;
    PyObject *var_index = NULL;
    PyObject *var_new_dotted = NULL;
    PyObject *var_values = NULL;
    PyObject *var_stmt = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    PyObject *tmp_genexpr_2__$0 = NULL;
    PyObject *tmp_try_except_1__unhandled_indicator = NULL;
    struct Nuitka_FrameObject *frame_83ec4da756226c82e4def57f9169641d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    static struct Nuitka_FrameObject *cache_frame_83ec4da756226c82e4def57f9169641d = NULL;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_83ec4da756226c82e4def57f9169641d, codeobj_83ec4da756226c82e4def57f9169641d, module_jedi$evaluate, sizeof(nuitka_bool)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_83ec4da756226c82e4def57f9169641d = cache_frame_83ec4da756226c82e4def57f9169641d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_83ec4da756226c82e4def57f9169641d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_83ec4da756226c82e4def57f9169641d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( PyCell_GET( par_name ) );
        tmp_source_name_1 = PyCell_GET( par_name );
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_get_definition );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 243;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_c420771dfdd3ba8cae6376435c084e1a );
        frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 243;
        tmp_assign_source_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 243;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_definition == NULL );
        var_definition = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_definition );
        tmp_compexpr_left_1 = var_definition;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( var_definition );
            tmp_source_name_2 = var_definition;
            tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_type );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 245;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_type_ == NULL );
            var_type_ = tmp_assign_source_2;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( var_type_ );
            tmp_compexpr_left_2 = var_type_;
            tmp_compexpr_right_2 = const_str_plain_expr_stmt;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 246;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                nuitka_bool tmp_assign_source_3;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                PyObject *tmp_source_name_3;
                PyObject *tmp_source_name_4;
                CHECK_OBJECT( PyCell_GET( par_name ) );
                tmp_source_name_4 = PyCell_GET( par_name );
                tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_parent );
                if ( tmp_source_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 249;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_type );
                Py_DECREF( tmp_source_name_3 );
                if ( tmp_compexpr_left_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 249;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_3 = const_tuple_str_plain_power_str_plain_trailer_tuple;
                tmp_res = PySequence_Contains( tmp_compexpr_right_3, tmp_compexpr_left_3 );
                Py_DECREF( tmp_compexpr_left_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 249;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_3 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                var_is_simple_name = tmp_assign_source_3;
            }
            {
                nuitka_bool tmp_condition_result_3;
                assert( var_is_simple_name != NUITKA_BOOL_UNASSIGNED);
                tmp_condition_result_3 = var_is_simple_name;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_list_element_1;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_mvar_value_1;
                    PyObject *tmp_args_element_name_1;
                    PyObject *tmp_args_element_name_2;
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_TreeNameDefinition );

                    if (unlikely( tmp_mvar_value_1 == NULL ))
                    {
                        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TreeNameDefinition );
                    }

                    if ( tmp_mvar_value_1 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TreeNameDefinition" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 251;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_2 = tmp_mvar_value_1;
                    CHECK_OBJECT( PyCell_GET( par_context ) );
                    tmp_args_element_name_1 = PyCell_GET( par_context );
                    CHECK_OBJECT( PyCell_GET( par_name ) );
                    tmp_args_element_name_2 = PyCell_GET( par_name );
                    frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 251;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                        tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
                    }

                    if ( tmp_list_element_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 251;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_return_value = PyList_New( 1 );
                    PyList_SET_ITEM( tmp_return_value, 0, tmp_list_element_1 );
                    goto frame_return_exit_1;
                }
                branch_no_3:;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                CHECK_OBJECT( var_type_ );
                tmp_compexpr_left_4 = var_type_;
                tmp_compexpr_right_4 = const_str_plain_param;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 252;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_list_element_2;
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_mvar_value_2;
                    PyObject *tmp_args_element_name_3;
                    PyObject *tmp_args_element_name_4;
                    tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ParamName );

                    if (unlikely( tmp_mvar_value_2 == NULL ))
                    {
                        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ParamName );
                    }

                    if ( tmp_mvar_value_2 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ParamName" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 253;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_3 = tmp_mvar_value_2;
                    CHECK_OBJECT( PyCell_GET( par_context ) );
                    tmp_args_element_name_3 = PyCell_GET( par_context );
                    CHECK_OBJECT( PyCell_GET( par_name ) );
                    tmp_args_element_name_4 = PyCell_GET( par_name );
                    frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 253;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
                        tmp_list_element_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
                    }

                    if ( tmp_list_element_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 253;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_return_value = PyList_New( 1 );
                    PyList_SET_ITEM( tmp_return_value, 0, tmp_list_element_2 );
                    goto frame_return_exit_1;
                }
                goto branch_end_4;
                branch_no_4:;
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    CHECK_OBJECT( var_type_ );
                    tmp_compexpr_left_5 = var_type_;
                    tmp_compexpr_right_5 = const_tuple_str_plain_funcdef_str_plain_classdef_tuple;
                    tmp_res = PySequence_Contains( tmp_compexpr_right_5, tmp_compexpr_left_5 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 254;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_5 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    {
                        PyObject *tmp_list_element_3;
                        PyObject *tmp_called_name_4;
                        PyObject *tmp_mvar_value_3;
                        PyObject *tmp_args_element_name_5;
                        PyObject *tmp_args_element_name_6;
                        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_TreeNameDefinition );

                        if (unlikely( tmp_mvar_value_3 == NULL ))
                        {
                            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_TreeNameDefinition );
                        }

                        if ( tmp_mvar_value_3 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "TreeNameDefinition" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 255;
                            type_description_1 = "occooboooooooooooooo";
                            goto frame_exception_exit_1;
                        }

                        tmp_called_name_4 = tmp_mvar_value_3;
                        CHECK_OBJECT( PyCell_GET( par_context ) );
                        tmp_args_element_name_5 = PyCell_GET( par_context );
                        CHECK_OBJECT( PyCell_GET( par_name ) );
                        tmp_args_element_name_6 = PyCell_GET( par_name );
                        frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 255;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
                            tmp_list_element_3 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
                        }

                        if ( tmp_list_element_3 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 255;
                            type_description_1 = "occooboooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_return_value = PyList_New( 1 );
                        PyList_SET_ITEM( tmp_return_value, 0, tmp_list_element_3 );
                        goto frame_return_exit_1;
                    }
                    goto branch_end_5;
                    branch_no_5:;
                    {
                        nuitka_bool tmp_condition_result_6;
                        PyObject *tmp_compexpr_left_6;
                        PyObject *tmp_compexpr_right_6;
                        CHECK_OBJECT( var_type_ );
                        tmp_compexpr_left_6 = var_type_;
                        tmp_compexpr_right_6 = const_tuple_str_plain_import_from_str_plain_import_name_tuple;
                        tmp_res = PySequence_Contains( tmp_compexpr_right_6, tmp_compexpr_left_6 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 256;
                            type_description_1 = "occooboooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_6;
                        }
                        else
                        {
                            goto branch_no_6;
                        }
                        branch_yes_6:;
                        {
                            PyObject *tmp_assign_source_4;
                            PyObject *tmp_called_name_5;
                            PyObject *tmp_source_name_5;
                            PyObject *tmp_mvar_value_4;
                            PyObject *tmp_args_name_1;
                            PyObject *tmp_tuple_element_1;
                            PyObject *tmp_kw_name_2;
                            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_imports );

                            if (unlikely( tmp_mvar_value_4 == NULL ))
                            {
                                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_imports );
                            }

                            if ( tmp_mvar_value_4 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "imports" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 257;
                                type_description_1 = "occooboooooooooooooo";
                                goto frame_exception_exit_1;
                            }

                            tmp_source_name_5 = tmp_mvar_value_4;
                            tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_infer_import );
                            if ( tmp_called_name_5 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 257;
                                type_description_1 = "occooboooooooooooooo";
                                goto frame_exception_exit_1;
                            }
                            CHECK_OBJECT( PyCell_GET( par_context ) );
                            tmp_tuple_element_1 = PyCell_GET( par_context );
                            tmp_args_name_1 = PyTuple_New( 2 );
                            Py_INCREF( tmp_tuple_element_1 );
                            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
                            CHECK_OBJECT( PyCell_GET( par_name ) );
                            tmp_tuple_element_1 = PyCell_GET( par_name );
                            Py_INCREF( tmp_tuple_element_1 );
                            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
                            tmp_kw_name_2 = PyDict_Copy( const_dict_7435f891c23645b1e0e50cd95b5f0389 );
                            frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 257;
                            tmp_assign_source_4 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_1, tmp_kw_name_2 );
                            Py_DECREF( tmp_called_name_5 );
                            Py_DECREF( tmp_args_name_1 );
                            Py_DECREF( tmp_kw_name_2 );
                            if ( tmp_assign_source_4 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 257;
                                type_description_1 = "occooboooooooooooooo";
                                goto frame_exception_exit_1;
                            }
                            assert( var_module_names == NULL );
                            var_module_names = tmp_assign_source_4;
                        }
                        CHECK_OBJECT( var_module_names );
                        tmp_return_value = var_module_names;
                        Py_INCREF( tmp_return_value );
                        goto frame_return_exit_1;
                        branch_no_6:;
                    }
                    branch_end_5:;
                }
                branch_end_4:;
            }
            branch_end_2:;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_source_name_6;
        CHECK_OBJECT( PyCell_GET( par_name ) );
        tmp_source_name_6 = PyCell_GET( par_name );
        tmp_assign_source_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_parent );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 260;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_par == NULL );
        var_par = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( var_par );
        tmp_source_name_7 = var_par;
        tmp_assign_source_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_type );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 261;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_node_type == NULL );
        var_node_type = tmp_assign_source_6;
    }
    {
        nuitka_bool tmp_condition_result_7;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_7;
        PyObject *tmp_compexpr_right_7;
        int tmp_and_left_truth_2;
        nuitka_bool tmp_and_left_value_2;
        nuitka_bool tmp_and_right_value_2;
        PyObject *tmp_compexpr_left_8;
        PyObject *tmp_compexpr_right_8;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_8;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_compexpr_left_9;
        PyObject *tmp_compexpr_right_9;
        PyObject *tmp_subscribed_name_2;
        PyObject *tmp_source_name_9;
        PyObject *tmp_subscript_name_2;
        CHECK_OBJECT( var_node_type );
        tmp_compexpr_left_7 = var_node_type;
        tmp_compexpr_right_7 = const_str_plain_argument;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_par );
        tmp_source_name_8 = var_par;
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_children );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_pos_1;
        tmp_compexpr_left_8 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_compexpr_left_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_8 = const_str_chr_61;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_8, tmp_compexpr_right_8 );
        Py_DECREF( tmp_compexpr_left_8 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_2 == 1 )
        {
            goto and_right_2;
        }
        else
        {
            goto and_left_2;
        }
        and_right_2:;
        CHECK_OBJECT( var_par );
        tmp_source_name_9 = var_par;
        tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_children );
        if ( tmp_subscribed_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_2 = const_int_0;
        tmp_compexpr_left_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
        Py_DECREF( tmp_subscribed_name_2 );
        if ( tmp_compexpr_left_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( PyCell_GET( par_name ) );
        tmp_compexpr_right_9 = PyCell_GET( par_name );
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_9, tmp_compexpr_right_9 );
        Py_DECREF( tmp_compexpr_left_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 262;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_right_value_1 = tmp_and_right_value_2;
        goto and_end_2;
        and_left_2:;
        tmp_and_right_value_1 = tmp_and_left_value_2;
        and_end_2:;
        tmp_condition_result_7 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_7 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_7;
        }
        else
        {
            goto branch_no_7;
        }
        branch_yes_7:;
        {
            PyObject *tmp_assign_source_7;
            PyObject *tmp_source_name_10;
            CHECK_OBJECT( var_par );
            tmp_source_name_10 = var_par;
            tmp_assign_source_7 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_parent );
            if ( tmp_assign_source_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 264;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_trailer == NULL );
            var_trailer = tmp_assign_source_7;
        }
        {
            nuitka_bool tmp_condition_result_8;
            PyObject *tmp_compexpr_left_10;
            PyObject *tmp_compexpr_right_10;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( var_trailer );
            tmp_source_name_11 = var_trailer;
            tmp_compexpr_left_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_type );
            if ( tmp_compexpr_left_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 265;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_10 = const_str_plain_arglist;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_10, tmp_compexpr_right_10 );
            Py_DECREF( tmp_compexpr_left_10 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 265;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            {
                PyObject *tmp_assign_source_8;
                PyObject *tmp_source_name_12;
                CHECK_OBJECT( var_trailer );
                tmp_source_name_12 = var_trailer;
                tmp_assign_source_8 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_parent );
                if ( tmp_assign_source_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 266;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_trailer;
                    assert( old != NULL );
                    var_trailer = tmp_assign_source_8;
                    Py_DECREF( old );
                }

            }
            branch_no_8:;
        }
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_11;
            PyObject *tmp_compexpr_right_11;
            PyObject *tmp_source_name_13;
            CHECK_OBJECT( var_trailer );
            tmp_source_name_13 = var_trailer;
            tmp_compexpr_left_11 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_type );
            if ( tmp_compexpr_left_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 267;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_11 = const_str_plain_classdef;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_11, tmp_compexpr_right_11 );
            Py_DECREF( tmp_compexpr_left_11 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 267;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            {
                nuitka_bool tmp_condition_result_10;
                PyObject *tmp_compexpr_left_12;
                PyObject *tmp_compexpr_right_12;
                PyObject *tmp_source_name_14;
                CHECK_OBJECT( var_trailer );
                tmp_source_name_14 = var_trailer;
                tmp_compexpr_left_12 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_type );
                if ( tmp_compexpr_left_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 268;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_12 = const_str_plain_decorator;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_12, tmp_compexpr_right_12 );
                Py_DECREF( tmp_compexpr_left_12 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 268;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_10;
                }
                else
                {
                    goto branch_no_10;
                }
                branch_yes_10:;
                {
                    PyObject *tmp_assign_source_9;
                    PyObject *tmp_called_name_6;
                    PyObject *tmp_source_name_15;
                    PyObject *tmp_args_element_name_7;
                    PyObject *tmp_subscribed_name_3;
                    PyObject *tmp_source_name_16;
                    PyObject *tmp_subscript_name_3;
                    CHECK_OBJECT( PyCell_GET( par_context ) );
                    tmp_source_name_15 = PyCell_GET( par_context );
                    tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_eval_node );
                    if ( tmp_called_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 269;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_trailer );
                    tmp_source_name_16 = var_trailer;
                    tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_children );
                    if ( tmp_subscribed_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_6 );

                        exception_lineno = 269;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_subscript_name_3 = const_int_pos_1;
                    tmp_args_element_name_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_3, tmp_subscript_name_3, 1 );
                    Py_DECREF( tmp_subscribed_name_3 );
                    if ( tmp_args_element_name_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_6 );

                        exception_lineno = 269;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 269;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_7 };
                        tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
                    }

                    Py_DECREF( tmp_called_name_6 );
                    Py_DECREF( tmp_args_element_name_7 );
                    if ( tmp_assign_source_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 269;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_context_set == NULL );
                    var_context_set = tmp_assign_source_9;
                }
                goto branch_end_10;
                branch_no_10:;
                {
                    PyObject *tmp_assign_source_10;
                    PyObject *tmp_called_instance_1;
                    PyObject *tmp_source_name_17;
                    PyObject *tmp_source_name_18;
                    PyObject *tmp_args_element_name_8;
                    CHECK_OBJECT( var_trailer );
                    tmp_source_name_18 = var_trailer;
                    tmp_source_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_parent );
                    if ( tmp_source_name_17 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 271;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_children );
                    Py_DECREF( tmp_source_name_17 );
                    if ( tmp_called_instance_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 271;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_trailer );
                    tmp_args_element_name_8 = var_trailer;
                    frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 271;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_8 };
                        tmp_assign_source_10 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_index, call_args );
                    }

                    Py_DECREF( tmp_called_instance_1 );
                    if ( tmp_assign_source_10 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 271;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_i == NULL );
                    var_i = tmp_assign_source_10;
                }
                {
                    PyObject *tmp_assign_source_11;
                    PyObject *tmp_subscribed_name_4;
                    PyObject *tmp_source_name_19;
                    PyObject *tmp_source_name_20;
                    PyObject *tmp_subscript_name_4;
                    PyObject *tmp_start_name_1;
                    PyObject *tmp_stop_name_1;
                    PyObject *tmp_step_name_1;
                    CHECK_OBJECT( var_trailer );
                    tmp_source_name_20 = var_trailer;
                    tmp_source_name_19 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_parent );
                    if ( tmp_source_name_19 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 272;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_subscribed_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_children );
                    Py_DECREF( tmp_source_name_19 );
                    if ( tmp_subscribed_name_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 272;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_start_name_1 = Py_None;
                    CHECK_OBJECT( var_i );
                    tmp_stop_name_1 = var_i;
                    tmp_step_name_1 = Py_None;
                    tmp_subscript_name_4 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
                    assert( !(tmp_subscript_name_4 == NULL) );
                    tmp_assign_source_11 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_4, tmp_subscript_name_4 );
                    Py_DECREF( tmp_subscribed_name_4 );
                    Py_DECREF( tmp_subscript_name_4 );
                    if ( tmp_assign_source_11 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 272;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_to_evaluate == NULL );
                    var_to_evaluate = tmp_assign_source_11;
                }
                {
                    nuitka_bool tmp_condition_result_11;
                    PyObject *tmp_compexpr_left_13;
                    PyObject *tmp_compexpr_right_13;
                    PyObject *tmp_subscribed_name_5;
                    PyObject *tmp_subscript_name_5;
                    CHECK_OBJECT( var_to_evaluate );
                    tmp_subscribed_name_5 = var_to_evaluate;
                    tmp_subscript_name_5 = const_int_0;
                    tmp_compexpr_left_13 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_5, tmp_subscript_name_5, 0 );
                    if ( tmp_compexpr_left_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 273;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_13 = const_str_plain_await;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_13, tmp_compexpr_right_13 );
                    Py_DECREF( tmp_compexpr_left_13 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 273;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_11 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_11;
                    }
                    else
                    {
                        goto branch_no_11;
                    }
                    branch_yes_11:;
                    {
                        PyObject *tmp_called_instance_2;
                        PyObject *tmp_call_result_1;
                        CHECK_OBJECT( var_to_evaluate );
                        tmp_called_instance_2 = var_to_evaluate;
                        frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 274;
                        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_pop, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

                        if ( tmp_call_result_1 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 274;
                            type_description_1 = "occooboooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        Py_DECREF( tmp_call_result_1 );
                    }
                    branch_no_11:;
                }
                {
                    PyObject *tmp_assign_source_12;
                    PyObject *tmp_called_name_7;
                    PyObject *tmp_source_name_21;
                    PyObject *tmp_args_element_name_9;
                    PyObject *tmp_subscribed_name_6;
                    PyObject *tmp_subscript_name_6;
                    CHECK_OBJECT( PyCell_GET( par_context ) );
                    tmp_source_name_21 = PyCell_GET( par_context );
                    tmp_called_name_7 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_eval_node );
                    if ( tmp_called_name_7 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 275;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_to_evaluate );
                    tmp_subscribed_name_6 = var_to_evaluate;
                    tmp_subscript_name_6 = const_int_0;
                    tmp_args_element_name_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_6, tmp_subscript_name_6, 0 );
                    if ( tmp_args_element_name_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_called_name_7 );

                        exception_lineno = 275;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 275;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_9 };
                        tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
                    }

                    Py_DECREF( tmp_called_name_7 );
                    Py_DECREF( tmp_args_element_name_9 );
                    if ( tmp_assign_source_12 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 275;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_context_set == NULL );
                    var_context_set = tmp_assign_source_12;
                }
                {
                    PyObject *tmp_assign_source_13;
                    PyObject *tmp_iter_arg_1;
                    PyObject *tmp_subscribed_name_7;
                    PyObject *tmp_subscript_name_7;
                    CHECK_OBJECT( var_to_evaluate );
                    tmp_subscribed_name_7 = var_to_evaluate;
                    tmp_subscript_name_7 = const_slice_int_pos_1_none_none;
                    tmp_iter_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
                    if ( tmp_iter_arg_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 276;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_13 = MAKE_ITERATOR( tmp_iter_arg_1 );
                    Py_DECREF( tmp_iter_arg_1 );
                    if ( tmp_assign_source_13 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 276;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( tmp_for_loop_1__for_iterator == NULL );
                    tmp_for_loop_1__for_iterator = tmp_assign_source_13;
                }
                // Tried code:
                loop_start_1:;
                {
                    PyObject *tmp_next_source_1;
                    PyObject *tmp_assign_source_14;
                    CHECK_OBJECT( tmp_for_loop_1__for_iterator );
                    tmp_next_source_1 = tmp_for_loop_1__for_iterator;
                    tmp_assign_source_14 = ITERATOR_NEXT( tmp_next_source_1 );
                    if ( tmp_assign_source_14 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_1;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "occooboooooooooooooo";
                            exception_lineno = 276;
                            goto try_except_handler_2;
                        }
                    }

                    {
                        PyObject *old = tmp_for_loop_1__iter_value;
                        tmp_for_loop_1__iter_value = tmp_assign_source_14;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_15;
                    CHECK_OBJECT( tmp_for_loop_1__iter_value );
                    tmp_assign_source_15 = tmp_for_loop_1__iter_value;
                    {
                        PyObject *old = var_trailer;
                        assert( old != NULL );
                        var_trailer = tmp_assign_source_15;
                        Py_INCREF( var_trailer );
                        Py_DECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_16;
                    PyObject *tmp_called_name_8;
                    PyObject *tmp_mvar_value_5;
                    PyObject *tmp_args_element_name_10;
                    PyObject *tmp_args_element_name_11;
                    PyObject *tmp_args_element_name_12;
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_eval_trailer );

                    if (unlikely( tmp_mvar_value_5 == NULL ))
                    {
                        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_eval_trailer );
                    }

                    if ( tmp_mvar_value_5 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "eval_trailer" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 277;
                        type_description_1 = "occooboooooooooooooo";
                        goto try_except_handler_2;
                    }

                    tmp_called_name_8 = tmp_mvar_value_5;
                    CHECK_OBJECT( PyCell_GET( par_context ) );
                    tmp_args_element_name_10 = PyCell_GET( par_context );
                    CHECK_OBJECT( var_context_set );
                    tmp_args_element_name_11 = var_context_set;
                    CHECK_OBJECT( var_trailer );
                    tmp_args_element_name_12 = var_trailer;
                    frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 277;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11, tmp_args_element_name_12 };
                        tmp_assign_source_16 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_8, call_args );
                    }

                    if ( tmp_assign_source_16 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 277;
                        type_description_1 = "occooboooooooooooooo";
                        goto try_except_handler_2;
                    }
                    {
                        PyObject *old = var_context_set;
                        assert( old != NULL );
                        var_context_set = tmp_assign_source_16;
                        Py_DECREF( old );
                    }

                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 276;
                    type_description_1 = "occooboooooooooooooo";
                    goto try_except_handler_2;
                }
                goto loop_start_1;
                loop_end_1:;
                goto try_end_1;
                // Exception handler code:
                try_except_handler_2:;
                exception_keeper_type_1 = exception_type;
                exception_keeper_value_1 = exception_value;
                exception_keeper_tb_1 = exception_tb;
                exception_keeper_lineno_1 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_for_loop_1__iter_value );
                tmp_for_loop_1__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                Py_DECREF( tmp_for_loop_1__for_iterator );
                tmp_for_loop_1__for_iterator = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_1;
                exception_value = exception_keeper_value_1;
                exception_tb = exception_keeper_tb_1;
                exception_lineno = exception_keeper_lineno_1;

                goto frame_exception_exit_1;
                // End of try:
                try_end_1:;
                Py_XDECREF( tmp_for_loop_1__iter_value );
                tmp_for_loop_1__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                Py_DECREF( tmp_for_loop_1__for_iterator );
                tmp_for_loop_1__for_iterator = NULL;

                branch_end_10:;
            }
            {
                PyObject *tmp_assign_source_17;
                tmp_assign_source_17 = PyList_New( 0 );
                assert( var_param_names == NULL );
                var_param_names = tmp_assign_source_17;
            }
            {
                PyObject *tmp_assign_source_18;
                PyObject *tmp_iter_arg_2;
                if ( var_context_set == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "context_set" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 279;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_iter_arg_2 = var_context_set;
                tmp_assign_source_18 = MAKE_ITERATOR( tmp_iter_arg_2 );
                if ( tmp_assign_source_18 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 279;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_for_loop_2__for_iterator == NULL );
                tmp_for_loop_2__for_iterator = tmp_assign_source_18;
            }
            // Tried code:
            loop_start_2:;
            {
                PyObject *tmp_next_source_2;
                PyObject *tmp_assign_source_19;
                CHECK_OBJECT( tmp_for_loop_2__for_iterator );
                tmp_next_source_2 = tmp_for_loop_2__for_iterator;
                tmp_assign_source_19 = ITERATOR_NEXT( tmp_next_source_2 );
                if ( tmp_assign_source_19 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_2;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "occooboooooooooooooo";
                        exception_lineno = 279;
                        goto try_except_handler_3;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_2__iter_value;
                    tmp_for_loop_2__iter_value = tmp_assign_source_19;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_20;
                CHECK_OBJECT( tmp_for_loop_2__iter_value );
                tmp_assign_source_20 = tmp_for_loop_2__iter_value;
                {
                    PyObject *old = PyCell_GET( par_context );
                    PyCell_SET( par_context, tmp_assign_source_20 );
                    Py_INCREF( tmp_assign_source_20 );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_21;
                tmp_assign_source_21 = Py_True;
                {
                    PyObject *old = tmp_try_except_1__unhandled_indicator;
                    tmp_try_except_1__unhandled_indicator = tmp_assign_source_21;
                    Py_INCREF( tmp_try_except_1__unhandled_indicator );
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            // Tried code:
            {
                PyObject *tmp_assign_source_22;
                PyObject *tmp_source_name_22;
                CHECK_OBJECT( PyCell_GET( par_context ) );
                tmp_source_name_22 = PyCell_GET( par_context );
                tmp_assign_source_22 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_get_param_names );
                if ( tmp_assign_source_22 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 281;
                    type_description_1 = "occooboooooooooooooo";
                    goto try_except_handler_5;
                }
                {
                    PyObject *old = var_get_param_names;
                    var_get_param_names = tmp_assign_source_22;
                    Py_XDECREF( old );
                }

            }
            goto try_end_2;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            {
                PyObject *tmp_assign_source_23;
                tmp_assign_source_23 = Py_False;
                {
                    PyObject *old = tmp_try_except_1__unhandled_indicator;
                    assert( old != NULL );
                    tmp_try_except_1__unhandled_indicator = tmp_assign_source_23;
                    Py_INCREF( tmp_try_except_1__unhandled_indicator );
                    Py_DECREF( old );
                }

            }
            // Preserve existing published exception.
            exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_type_1 );
            exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_value_1 );
            exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( exception_preserved_tb_1 );

            if ( exception_keeper_tb_2 == NULL )
            {
                exception_keeper_tb_2 = MAKE_TRACEBACK( frame_83ec4da756226c82e4def57f9169641d, exception_keeper_lineno_2 );
            }
            else if ( exception_keeper_lineno_2 != 0 )
            {
                exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_83ec4da756226c82e4def57f9169641d, exception_keeper_lineno_2 );
            }

            NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
            PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
            PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_12;
                PyObject *tmp_operand_name_1;
                PyObject *tmp_compexpr_left_14;
                PyObject *tmp_compexpr_right_14;
                tmp_compexpr_left_14 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_14 = PyExc_AttributeError;
                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_14, tmp_compexpr_right_14 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 282;
                    type_description_1 = "occooboooooooooooooo";
                    goto try_except_handler_6;
                }
                tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
                tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 282;
                    type_description_1 = "occooboooooooooooooo";
                    goto try_except_handler_6;
                }
                tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_12;
                }
                else
                {
                    goto branch_no_12;
                }
                branch_yes_12:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 280;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_83ec4da756226c82e4def57f9169641d->m_frame) frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "occooboooooooooooooo";
                goto try_except_handler_6;
                branch_no_12:;
            }
            goto try_end_3;
            // Exception handler code:
            try_except_handler_6:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto try_except_handler_4;
            // End of try:
            try_end_3:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            goto try_end_2;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_9_goto );
            return NULL;
            // End of try:
            try_end_2:;
            {
                nuitka_bool tmp_condition_result_13;
                nuitka_bool tmp_compexpr_left_15;
                nuitka_bool tmp_compexpr_right_15;
                int tmp_truth_name_1;
                CHECK_OBJECT( tmp_try_except_1__unhandled_indicator );
                tmp_truth_name_1 = CHECK_IF_TRUE( tmp_try_except_1__unhandled_indicator );
                if ( tmp_truth_name_1 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 280;
                    type_description_1 = "occooboooooooooooooo";
                    goto try_except_handler_4;
                }
                tmp_compexpr_left_15 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                tmp_compexpr_right_15 = NUITKA_BOOL_TRUE;
                tmp_condition_result_13 = ( tmp_compexpr_left_15 == tmp_compexpr_right_15 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_13;
                }
                else
                {
                    goto branch_no_13;
                }
                branch_yes_13:;
                {
                    PyObject *tmp_assign_source_24;
                    PyObject *tmp_iter_arg_3;
                    PyObject *tmp_called_name_9;
                    if ( var_get_param_names == NULL )
                    {

                        exception_type = PyExc_UnboundLocalError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "get_param_names" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 285;
                        type_description_1 = "occooboooooooooooooo";
                        goto try_except_handler_4;
                    }

                    tmp_called_name_9 = var_get_param_names;
                    frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 285;
                    tmp_iter_arg_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_9 );
                    if ( tmp_iter_arg_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 285;
                        type_description_1 = "occooboooooooooooooo";
                        goto try_except_handler_4;
                    }
                    tmp_assign_source_24 = MAKE_ITERATOR( tmp_iter_arg_3 );
                    Py_DECREF( tmp_iter_arg_3 );
                    if ( tmp_assign_source_24 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 285;
                        type_description_1 = "occooboooooooooooooo";
                        goto try_except_handler_4;
                    }
                    {
                        PyObject *old = tmp_for_loop_3__for_iterator;
                        tmp_for_loop_3__for_iterator = tmp_assign_source_24;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                loop_start_3:;
                {
                    PyObject *tmp_next_source_3;
                    PyObject *tmp_assign_source_25;
                    CHECK_OBJECT( tmp_for_loop_3__for_iterator );
                    tmp_next_source_3 = tmp_for_loop_3__for_iterator;
                    tmp_assign_source_25 = ITERATOR_NEXT( tmp_next_source_3 );
                    if ( tmp_assign_source_25 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_3;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "occooboooooooooooooo";
                            exception_lineno = 285;
                            goto try_except_handler_7;
                        }
                    }

                    {
                        PyObject *old = tmp_for_loop_3__iter_value;
                        tmp_for_loop_3__iter_value = tmp_assign_source_25;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_26;
                    CHECK_OBJECT( tmp_for_loop_3__iter_value );
                    tmp_assign_source_26 = tmp_for_loop_3__iter_value;
                    {
                        PyObject *old = var_param_name;
                        var_param_name = tmp_assign_source_26;
                        Py_INCREF( var_param_name );
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_14;
                    PyObject *tmp_compexpr_left_16;
                    PyObject *tmp_compexpr_right_16;
                    PyObject *tmp_source_name_23;
                    PyObject *tmp_source_name_24;
                    CHECK_OBJECT( var_param_name );
                    tmp_source_name_23 = var_param_name;
                    tmp_compexpr_left_16 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_string_name );
                    if ( tmp_compexpr_left_16 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 286;
                        type_description_1 = "occooboooooooooooooo";
                        goto try_except_handler_7;
                    }
                    CHECK_OBJECT( PyCell_GET( par_name ) );
                    tmp_source_name_24 = PyCell_GET( par_name );
                    tmp_compexpr_right_16 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_value );
                    if ( tmp_compexpr_right_16 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_compexpr_left_16 );

                        exception_lineno = 286;
                        type_description_1 = "occooboooooooooooooo";
                        goto try_except_handler_7;
                    }
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_16, tmp_compexpr_right_16 );
                    Py_DECREF( tmp_compexpr_left_16 );
                    Py_DECREF( tmp_compexpr_right_16 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 286;
                        type_description_1 = "occooboooooooooooooo";
                        goto try_except_handler_7;
                    }
                    tmp_condition_result_14 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_14;
                    }
                    else
                    {
                        goto branch_no_14;
                    }
                    branch_yes_14:;
                    {
                        PyObject *tmp_called_instance_3;
                        PyObject *tmp_call_result_2;
                        PyObject *tmp_args_element_name_13;
                        CHECK_OBJECT( var_param_names );
                        tmp_called_instance_3 = var_param_names;
                        CHECK_OBJECT( var_param_name );
                        tmp_args_element_name_13 = var_param_name;
                        frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 287;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_13 };
                            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_append, call_args );
                        }

                        if ( tmp_call_result_2 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 287;
                            type_description_1 = "occooboooooooooooooo";
                            goto try_except_handler_7;
                        }
                        Py_DECREF( tmp_call_result_2 );
                    }
                    branch_no_14:;
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 285;
                    type_description_1 = "occooboooooooooooooo";
                    goto try_except_handler_7;
                }
                goto loop_start_3;
                loop_end_3:;
                goto try_end_4;
                // Exception handler code:
                try_except_handler_7:;
                exception_keeper_type_4 = exception_type;
                exception_keeper_value_4 = exception_value;
                exception_keeper_tb_4 = exception_tb;
                exception_keeper_lineno_4 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_for_loop_3__iter_value );
                tmp_for_loop_3__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
                Py_DECREF( tmp_for_loop_3__for_iterator );
                tmp_for_loop_3__for_iterator = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_4;
                exception_value = exception_keeper_value_4;
                exception_tb = exception_keeper_tb_4;
                exception_lineno = exception_keeper_lineno_4;

                goto try_except_handler_4;
                // End of try:
                try_end_4:;
                Py_XDECREF( tmp_for_loop_3__iter_value );
                tmp_for_loop_3__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
                Py_DECREF( tmp_for_loop_3__for_iterator );
                tmp_for_loop_3__for_iterator = NULL;

                branch_no_13:;
            }
            goto try_end_5;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_5 = exception_type;
            exception_keeper_value_5 = exception_value;
            exception_keeper_tb_5 = exception_tb;
            exception_keeper_lineno_5 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_try_except_1__unhandled_indicator );
            tmp_try_except_1__unhandled_indicator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_5;
            exception_value = exception_keeper_value_5;
            exception_tb = exception_keeper_tb_5;
            exception_lineno = exception_keeper_lineno_5;

            goto try_except_handler_3;
            // End of try:
            try_end_5:;
            CHECK_OBJECT( (PyObject *)tmp_try_except_1__unhandled_indicator );
            Py_DECREF( tmp_try_except_1__unhandled_indicator );
            tmp_try_except_1__unhandled_indicator = NULL;

            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 279;
                type_description_1 = "occooboooooooooooooo";
                goto try_except_handler_3;
            }
            goto loop_start_2;
            loop_end_2:;
            goto try_end_6;
            // Exception handler code:
            try_except_handler_3:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_2__iter_value );
            tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
            Py_DECREF( tmp_for_loop_2__for_iterator );
            tmp_for_loop_2__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto frame_exception_exit_1;
            // End of try:
            try_end_6:;
            Py_XDECREF( tmp_for_loop_2__iter_value );
            tmp_for_loop_2__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
            Py_DECREF( tmp_for_loop_2__for_iterator );
            tmp_for_loop_2__for_iterator = NULL;

            CHECK_OBJECT( var_param_names );
            tmp_return_value = var_param_names;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            branch_no_9:;
        }
        goto branch_end_7;
        branch_no_7:;
        {
            nuitka_bool tmp_condition_result_15;
            PyObject *tmp_compexpr_left_17;
            PyObject *tmp_compexpr_right_17;
            CHECK_OBJECT( var_node_type );
            tmp_compexpr_left_17 = var_node_type;
            tmp_compexpr_right_17 = const_str_plain_dotted_name;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_17, tmp_compexpr_right_17 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 289;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_15 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_15;
            }
            else
            {
                goto branch_no_15;
            }
            branch_yes_15:;
            {
                PyObject *tmp_assign_source_27;
                PyObject *tmp_called_instance_4;
                PyObject *tmp_source_name_25;
                PyObject *tmp_args_element_name_14;
                CHECK_OBJECT( var_par );
                tmp_source_name_25 = var_par;
                tmp_called_instance_4 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_children );
                if ( tmp_called_instance_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 290;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( PyCell_GET( par_name ) );
                tmp_args_element_name_14 = PyCell_GET( par_name );
                frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 290;
                {
                    PyObject *call_args[] = { tmp_args_element_name_14 };
                    tmp_assign_source_27 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_4, const_str_plain_index, call_args );
                }

                Py_DECREF( tmp_called_instance_4 );
                if ( tmp_assign_source_27 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 290;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( var_index == NULL );
                var_index = tmp_assign_source_27;
            }
            {
                nuitka_bool tmp_condition_result_16;
                PyObject *tmp_compexpr_left_18;
                PyObject *tmp_compexpr_right_18;
                CHECK_OBJECT( var_index );
                tmp_compexpr_left_18 = var_index;
                tmp_compexpr_right_18 = const_int_0;
                tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_18, tmp_compexpr_right_18 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 291;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_16;
                }
                else
                {
                    goto branch_no_16;
                }
                branch_yes_16:;
                {
                    PyObject *tmp_assign_source_28;
                    PyObject *tmp_called_instance_5;
                    PyObject *tmp_mvar_value_6;
                    PyObject *tmp_args_element_name_15;
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_helpers );

                    if (unlikely( tmp_mvar_value_6 == NULL ))
                    {
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
                    }

                    if ( tmp_mvar_value_6 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 292;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_instance_5 = tmp_mvar_value_6;
                    CHECK_OBJECT( var_par );
                    tmp_args_element_name_15 = var_par;
                    frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 292;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_15 };
                        tmp_assign_source_28 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_deep_ast_copy, call_args );
                    }

                    if ( tmp_assign_source_28 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 292;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_new_dotted == NULL );
                    var_new_dotted = tmp_assign_source_28;
                }
                {
                    PyObject *tmp_ass_subvalue_1;
                    PyObject *tmp_ass_subscribed_1;
                    PyObject *tmp_source_name_26;
                    PyObject *tmp_ass_subscript_1;
                    PyObject *tmp_start_name_2;
                    PyObject *tmp_left_name_1;
                    PyObject *tmp_right_name_1;
                    PyObject *tmp_stop_name_2;
                    PyObject *tmp_step_name_2;
                    tmp_ass_subvalue_1 = PyList_New( 0 );
                    CHECK_OBJECT( var_new_dotted );
                    tmp_source_name_26 = var_new_dotted;
                    tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_26, const_str_plain_children );
                    if ( tmp_ass_subscribed_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_ass_subvalue_1 );

                        exception_lineno = 293;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( var_index );
                    tmp_left_name_1 = var_index;
                    tmp_right_name_1 = const_int_pos_1;
                    tmp_start_name_2 = BINARY_OPERATION_SUB_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
                    if ( tmp_start_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_ass_subvalue_1 );
                        Py_DECREF( tmp_ass_subscribed_1 );

                        exception_lineno = 293;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    tmp_stop_name_2 = Py_None;
                    tmp_step_name_2 = Py_None;
                    tmp_ass_subscript_1 = MAKE_SLICEOBJ3( tmp_start_name_2, tmp_stop_name_2, tmp_step_name_2 );
                    Py_DECREF( tmp_start_name_2 );
                    assert( !(tmp_ass_subscript_1 == NULL) );
                    tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
                    Py_DECREF( tmp_ass_subscribed_1 );
                    Py_DECREF( tmp_ass_subscript_1 );
                    Py_DECREF( tmp_ass_subvalue_1 );
                    if ( tmp_result == false )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 293;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                }
                {
                    PyObject *tmp_assign_source_29;
                    PyObject *tmp_called_instance_6;
                    PyObject *tmp_args_element_name_16;
                    CHECK_OBJECT( PyCell_GET( par_context ) );
                    tmp_called_instance_6 = PyCell_GET( par_context );
                    CHECK_OBJECT( var_new_dotted );
                    tmp_args_element_name_16 = var_new_dotted;
                    frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 294;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_16 };
                        tmp_assign_source_29 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_eval_node, call_args );
                    }

                    if ( tmp_assign_source_29 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 294;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_values == NULL );
                    var_values = tmp_assign_source_29;
                }
                {
                    PyObject *tmp_called_name_10;
                    PyObject *tmp_mvar_value_7;
                    PyObject *tmp_args_element_name_17;
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_unite );

                    if (unlikely( tmp_mvar_value_7 == NULL ))
                    {
                        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unite );
                    }

                    if ( tmp_mvar_value_7 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unite" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 295;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_10 = tmp_mvar_value_7;
                    {
                        PyObject *tmp_assign_source_30;
                        PyObject *tmp_iter_arg_4;
                        CHECK_OBJECT( var_values );
                        tmp_iter_arg_4 = var_values;
                        tmp_assign_source_30 = MAKE_ITERATOR( tmp_iter_arg_4 );
                        if ( tmp_assign_source_30 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 296;
                            type_description_1 = "occooboooooooooooooo";
                            goto frame_exception_exit_1;
                        }
                        assert( tmp_genexpr_1__$0 == NULL );
                        tmp_genexpr_1__$0 = tmp_assign_source_30;
                    }
                    // Tried code:
                    tmp_args_element_name_17 = jedi$evaluate$$$function_9_goto$$$genexpr_1_genexpr_maker();

                    ((struct Nuitka_GeneratorObject *)tmp_args_element_name_17)->m_closure[0] = par_context;
                    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_17)->m_closure[0] );
                    ((struct Nuitka_GeneratorObject *)tmp_args_element_name_17)->m_closure[1] = PyCell_NEW0( tmp_genexpr_1__$0 );
                    ((struct Nuitka_GeneratorObject *)tmp_args_element_name_17)->m_closure[2] = par_name;
                    Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_17)->m_closure[2] );


                    goto try_return_handler_8;
                    // tried codes exits in all cases
                    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_9_goto );
                    return NULL;
                    // Return handler code:
                    try_return_handler_8:;
                    CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                    Py_DECREF( tmp_genexpr_1__$0 );
                    tmp_genexpr_1__$0 = NULL;

                    goto outline_result_1;
                    // End of try:
                    CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
                    Py_DECREF( tmp_genexpr_1__$0 );
                    tmp_genexpr_1__$0 = NULL;

                    // Return statement must have exited already.
                    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_9_goto );
                    return NULL;
                    outline_result_1:;
                    frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 295;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_17 };
                        tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_17 );
                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 295;
                        type_description_1 = "occooboooooooooooooo";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                branch_no_16:;
            }
            branch_no_15:;
        }
        branch_end_7:;
    }
    {
        nuitka_bool tmp_condition_result_17;
        int tmp_and_left_truth_3;
        nuitka_bool tmp_and_left_value_3;
        nuitka_bool tmp_and_right_value_3;
        PyObject *tmp_compexpr_left_19;
        PyObject *tmp_compexpr_right_19;
        PyObject *tmp_compexpr_left_20;
        PyObject *tmp_compexpr_right_20;
        PyObject *tmp_subscribed_name_8;
        PyObject *tmp_source_name_27;
        PyObject *tmp_subscript_name_8;
        CHECK_OBJECT( var_node_type );
        tmp_compexpr_left_19 = var_node_type;
        tmp_compexpr_right_19 = const_str_plain_trailer;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_19, tmp_compexpr_right_19 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 300;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_3 = tmp_and_left_value_3 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_3 == 1 )
        {
            goto and_right_3;
        }
        else
        {
            goto and_left_3;
        }
        and_right_3:;
        CHECK_OBJECT( var_par );
        tmp_source_name_27 = var_par;
        tmp_subscribed_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain_children );
        if ( tmp_subscribed_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 300;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_8 = const_int_0;
        tmp_compexpr_left_20 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_8, tmp_subscript_name_8, 0 );
        Py_DECREF( tmp_subscribed_name_8 );
        if ( tmp_compexpr_left_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 300;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_20 = const_str_dot;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_20, tmp_compexpr_right_20 );
        Py_DECREF( tmp_compexpr_left_20 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 300;
            type_description_1 = "occooboooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_17 = tmp_and_right_value_3;
        goto and_end_3;
        and_left_3:;
        tmp_condition_result_17 = tmp_and_left_value_3;
        and_end_3:;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_17;
        }
        else
        {
            goto branch_no_17;
        }
        branch_yes_17:;
        {
            PyObject *tmp_assign_source_31;
            PyObject *tmp_called_name_11;
            PyObject *tmp_source_name_28;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_2;
            PyObject *tmp_kw_name_3;
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_helpers );

            if (unlikely( tmp_mvar_value_8 == NULL ))
            {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_helpers );
            }

            if ( tmp_mvar_value_8 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "helpers" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 301;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_28 = tmp_mvar_value_8;
            tmp_called_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_28, const_str_plain_evaluate_call_of_leaf );
            if ( tmp_called_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 301;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( PyCell_GET( par_context ) );
            tmp_tuple_element_2 = PyCell_GET( par_context );
            tmp_args_name_2 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_2 );
            CHECK_OBJECT( PyCell_GET( par_name ) );
            tmp_tuple_element_2 = PyCell_GET( par_name );
            Py_INCREF( tmp_tuple_element_2 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_2 );
            tmp_kw_name_3 = PyDict_Copy( const_dict_32fe95870011cc07e909fb05ac91cbab );
            frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 301;
            tmp_assign_source_31 = CALL_FUNCTION( tmp_called_name_11, tmp_args_name_2, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_name_2 );
            Py_DECREF( tmp_kw_name_3 );
            if ( tmp_assign_source_31 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 301;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( var_values == NULL );
            var_values = tmp_assign_source_31;
        }
        {
            PyObject *tmp_called_name_12;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_args_element_name_18;
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_unite );

            if (unlikely( tmp_mvar_value_9 == NULL ))
            {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unite );
            }

            if ( tmp_mvar_value_9 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unite" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 302;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_12 = tmp_mvar_value_9;
            {
                PyObject *tmp_assign_source_32;
                PyObject *tmp_iter_arg_5;
                CHECK_OBJECT( var_values );
                tmp_iter_arg_5 = var_values;
                tmp_assign_source_32 = MAKE_ITERATOR( tmp_iter_arg_5 );
                if ( tmp_assign_source_32 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 303;
                    type_description_1 = "occooboooooooooooooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_genexpr_2__$0 == NULL );
                tmp_genexpr_2__$0 = tmp_assign_source_32;
            }
            // Tried code:
            tmp_args_element_name_18 = jedi$evaluate$$$function_9_goto$$$genexpr_2_genexpr_maker();

            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_18)->m_closure[0] = par_context;
            Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_18)->m_closure[0] );
            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_18)->m_closure[1] = PyCell_NEW0( tmp_genexpr_2__$0 );
            ((struct Nuitka_GeneratorObject *)tmp_args_element_name_18)->m_closure[2] = par_name;
            Py_INCREF( ((struct Nuitka_GeneratorObject *)tmp_args_element_name_18)->m_closure[2] );


            goto try_return_handler_9;
            // tried codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_9_goto );
            return NULL;
            // Return handler code:
            try_return_handler_9:;
            CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
            Py_DECREF( tmp_genexpr_2__$0 );
            tmp_genexpr_2__$0 = NULL;

            goto outline_result_2;
            // End of try:
            CHECK_OBJECT( (PyObject *)tmp_genexpr_2__$0 );
            Py_DECREF( tmp_genexpr_2__$0 );
            tmp_genexpr_2__$0 = NULL;

            // Return statement must have exited already.
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_9_goto );
            return NULL;
            outline_result_2:;
            frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 302;
            {
                PyObject *call_args[] = { tmp_args_element_name_18 };
                tmp_return_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
            }

            Py_DECREF( tmp_args_element_name_18 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 302;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        goto branch_end_17;
        branch_no_17:;
        {
            PyObject *tmp_assign_source_33;
            int tmp_or_left_truth_1;
            PyObject *tmp_or_left_value_1;
            PyObject *tmp_or_right_value_1;
            PyObject *tmp_called_instance_7;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_args_element_name_19;
            PyObject *tmp_args_element_name_20;
            PyObject *tmp_args_element_name_21;
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_tree );

            if (unlikely( tmp_mvar_value_10 == NULL ))
            {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_tree );
            }

            if ( tmp_mvar_value_10 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "tree" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 307;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_7 = tmp_mvar_value_10;
            CHECK_OBJECT( PyCell_GET( par_name ) );
            tmp_args_element_name_19 = PyCell_GET( par_name );
            tmp_args_element_name_20 = const_str_plain_expr_stmt;
            tmp_args_element_name_21 = const_str_plain_lambdef;
            frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 307;
            {
                PyObject *call_args[] = { tmp_args_element_name_19, tmp_args_element_name_20, tmp_args_element_name_21 };
                tmp_or_left_value_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_7, const_str_plain_search_ancestor, call_args );
            }

            if ( tmp_or_left_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 307;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
            if ( tmp_or_left_truth_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_or_left_value_1 );

                exception_lineno = 309;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            if ( tmp_or_left_truth_1 == 1 )
            {
                goto or_left_1;
            }
            else
            {
                goto or_right_1;
            }
            or_right_1:;
            Py_DECREF( tmp_or_left_value_1 );
            CHECK_OBJECT( PyCell_GET( par_name ) );
            tmp_or_right_value_1 = PyCell_GET( par_name );
            Py_INCREF( tmp_or_right_value_1 );
            tmp_assign_source_33 = tmp_or_right_value_1;
            goto or_end_1;
            or_left_1:;
            tmp_assign_source_33 = tmp_or_left_value_1;
            or_end_1:;
            assert( var_stmt == NULL );
            var_stmt = tmp_assign_source_33;
        }
        {
            nuitka_bool tmp_condition_result_18;
            PyObject *tmp_compexpr_left_21;
            PyObject *tmp_compexpr_right_21;
            PyObject *tmp_source_name_29;
            CHECK_OBJECT( var_stmt );
            tmp_source_name_29 = var_stmt;
            tmp_compexpr_left_21 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain_type );
            if ( tmp_compexpr_left_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 310;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_21 = const_str_plain_lambdef;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_21, tmp_compexpr_right_21 );
            Py_DECREF( tmp_compexpr_left_21 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 310;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_18 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_18;
            }
            else
            {
                goto branch_no_18;
            }
            branch_yes_18:;
            {
                PyObject *tmp_assign_source_34;
                CHECK_OBJECT( PyCell_GET( par_name ) );
                tmp_assign_source_34 = PyCell_GET( par_name );
                {
                    PyObject *old = var_stmt;
                    assert( old != NULL );
                    var_stmt = tmp_assign_source_34;
                    Py_INCREF( var_stmt );
                    Py_DECREF( old );
                }

            }
            branch_no_18:;
        }
        {
            PyObject *tmp_called_name_13;
            PyObject *tmp_source_name_30;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_4;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_source_name_31;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            CHECK_OBJECT( PyCell_GET( par_context ) );
            tmp_source_name_30 = PyCell_GET( par_context );
            tmp_called_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_30, const_str_plain_py__getattribute__ );
            if ( tmp_called_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 312;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( PyCell_GET( par_name ) );
            tmp_tuple_element_3 = PyCell_GET( par_name );
            tmp_args_name_3 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_3 );
            tmp_dict_key_1 = const_str_plain_position;
            CHECK_OBJECT( var_stmt );
            tmp_source_name_31 = var_stmt;
            tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain_start_pos );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_13 );
                Py_DECREF( tmp_args_name_3 );

                exception_lineno = 314;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_4 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_2 = const_str_plain_search_global;
            tmp_dict_value_2 = Py_True;
            tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_2, tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_3 = const_str_plain_is_goto;
            tmp_dict_value_3 = Py_True;
            tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_3, tmp_dict_value_3 );
            assert( !(tmp_res != 0) );
            frame_83ec4da756226c82e4def57f9169641d->m_frame.f_lineno = 312;
            tmp_return_value = CALL_FUNCTION( tmp_called_name_13, tmp_args_name_3, tmp_kw_name_4 );
            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_args_name_3 );
            Py_DECREF( tmp_kw_name_4 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 312;
                type_description_1 = "occooboooooooooooooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_end_17:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_83ec4da756226c82e4def57f9169641d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_83ec4da756226c82e4def57f9169641d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_83ec4da756226c82e4def57f9169641d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_83ec4da756226c82e4def57f9169641d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_83ec4da756226c82e4def57f9169641d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_83ec4da756226c82e4def57f9169641d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_83ec4da756226c82e4def57f9169641d,
        type_description_1,
        par_self,
        par_context,
        par_name,
        var_definition,
        var_type_,
        (int)var_is_simple_name,
        var_module_names,
        var_par,
        var_node_type,
        var_trailer,
        var_context_set,
        var_i,
        var_to_evaluate,
        var_param_names,
        var_get_param_names,
        var_param_name,
        var_index,
        var_new_dotted,
        var_values,
        var_stmt
    );


    // Release cached frame.
    if ( frame_83ec4da756226c82e4def57f9169641d == cache_frame_83ec4da756226c82e4def57f9169641d )
    {
        Py_DECREF( frame_83ec4da756226c82e4def57f9169641d );
    }
    cache_frame_83ec4da756226c82e4def57f9169641d = NULL;

    assertFrameObject( frame_83ec4da756226c82e4def57f9169641d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_9_goto );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)var_definition );
    Py_DECREF( var_definition );
    var_definition = NULL;

    Py_XDECREF( var_type_ );
    var_type_ = NULL;

    Py_XDECREF( var_module_names );
    var_module_names = NULL;

    Py_XDECREF( var_par );
    var_par = NULL;

    Py_XDECREF( var_node_type );
    var_node_type = NULL;

    Py_XDECREF( var_trailer );
    var_trailer = NULL;

    Py_XDECREF( var_context_set );
    var_context_set = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_to_evaluate );
    var_to_evaluate = NULL;

    Py_XDECREF( var_param_names );
    var_param_names = NULL;

    Py_XDECREF( var_get_param_names );
    var_get_param_names = NULL;

    Py_XDECREF( var_param_name );
    var_param_name = NULL;

    Py_XDECREF( var_index );
    var_index = NULL;

    Py_XDECREF( var_new_dotted );
    var_new_dotted = NULL;

    Py_XDECREF( var_values );
    var_values = NULL;

    Py_XDECREF( var_stmt );
    var_stmt = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_context );
    Py_DECREF( par_context );
    par_context = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_definition );
    var_definition = NULL;

    Py_XDECREF( var_type_ );
    var_type_ = NULL;

    Py_XDECREF( var_par );
    var_par = NULL;

    Py_XDECREF( var_node_type );
    var_node_type = NULL;

    Py_XDECREF( var_trailer );
    var_trailer = NULL;

    Py_XDECREF( var_context_set );
    var_context_set = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_to_evaluate );
    var_to_evaluate = NULL;

    Py_XDECREF( var_param_names );
    var_param_names = NULL;

    Py_XDECREF( var_get_param_names );
    var_get_param_names = NULL;

    Py_XDECREF( var_param_name );
    var_param_name = NULL;

    Py_XDECREF( var_index );
    var_index = NULL;

    Py_XDECREF( var_new_dotted );
    var_new_dotted = NULL;

    Py_XDECREF( var_values );
    var_values = NULL;

    Py_XDECREF( var_stmt );
    var_stmt = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_9_goto );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$$$function_9_goto$$$genexpr_1_genexpr_locals {
    PyObject *var_value;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$$$function_9_goto$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$$$function_9_goto$$$genexpr_1_genexpr_locals *generator_heap = (struct jedi$evaluate$$$function_9_goto$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_value = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_ad55f0c204b0a557a78f19bf6dc913b3, module_jedi$evaluate, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[1] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[1] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Nocc";
                generator_heap->exception_lineno = 296;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_value;
            generator_heap->var_value = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_value );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_value );
        tmp_source_name_1 = generator_heap->var_value;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_py__getattribute__ );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 296;
            generator_heap->type_description_1 = "Nocc";
            goto try_except_handler_2;
        }
        if ( PyCell_GET( generator->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 296;
            generator_heap->type_description_1 = "Nocc";
            goto try_except_handler_2;
        }

        tmp_tuple_element_1 = PyCell_GET( generator->m_closure[2] );
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_name_context;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "context" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 296;
            generator_heap->type_description_1 = "Nocc";
            goto try_except_handler_2;
        }

        tmp_dict_value_1 = PyCell_GET( generator->m_closure[0] );
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(generator_heap->tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_is_goto;
        tmp_dict_value_2 = Py_True;
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(generator_heap->tmp_res != 0) );
        generator->m_frame->m_frame.f_lineno = 296;
        tmp_expression_name_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 296;
            generator_heap->type_description_1 = "Nocc";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_args_name_1, sizeof(PyObject *), &tmp_tuple_element_1, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), &tmp_dict_key_1, sizeof(PyObject *), &tmp_dict_value_1, sizeof(PyObject *), &tmp_dict_key_2, sizeof(PyObject *), &tmp_dict_value_2, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_args_name_1, sizeof(PyObject *), &tmp_tuple_element_1, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), &tmp_dict_key_1, sizeof(PyObject *), &tmp_dict_value_1, sizeof(PyObject *), &tmp_dict_key_2, sizeof(PyObject *), &tmp_dict_value_2, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 296;
            generator_heap->type_description_1 = "Nocc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 296;
        generator_heap->type_description_1 = "Nocc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_value,
            generator->m_closure[2],
            generator->m_closure[0]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_value );
    generator_heap->var_value = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_value );
    generator_heap->var_value = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$$$function_9_goto$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$$$function_9_goto$$$genexpr_1_genexpr_context,
        module_jedi$evaluate,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_dfa371e33dd702765033c7673aa2ad23,
#endif
        codeobj_ad55f0c204b0a557a78f19bf6dc913b3,
        3,
        sizeof(struct jedi$evaluate$$$function_9_goto$$$genexpr_1_genexpr_locals)
    );
}



struct jedi$evaluate$$$function_9_goto$$$genexpr_2_genexpr_locals {
    PyObject *var_value;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    int tmp_res;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$$$function_9_goto$$$genexpr_2_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$$$function_9_goto$$$genexpr_2_genexpr_locals *generator_heap = (struct jedi$evaluate$$$function_9_goto$$$genexpr_2_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_value = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_bcac2ed3740e56ca7545ffd0614eccad, module_jedi$evaluate, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[1] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[1] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "Nocc";
                generator_heap->exception_lineno = 303;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_value;
            generator_heap->var_value = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_value );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        CHECK_OBJECT( generator_heap->var_value );
        tmp_source_name_1 = generator_heap->var_value;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_py__getattribute__ );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 303;
            generator_heap->type_description_1 = "Nocc";
            goto try_except_handler_2;
        }
        if ( PyCell_GET( generator->m_closure[2] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 303;
            generator_heap->type_description_1 = "Nocc";
            goto try_except_handler_2;
        }

        tmp_tuple_element_1 = PyCell_GET( generator->m_closure[2] );
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_name_context;
        if ( PyCell_GET( generator->m_closure[0] ) == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            generator_heap->exception_type = PyExc_NameError;
            Py_INCREF( generator_heap->exception_type );
            generator_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "context" );
            generator_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            CHAIN_EXCEPTION( generator_heap->exception_value );

            generator_heap->exception_lineno = 303;
            generator_heap->type_description_1 = "Nocc";
            goto try_except_handler_2;
        }

        tmp_dict_value_1 = PyCell_GET( generator->m_closure[0] );
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(generator_heap->tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_is_goto;
        tmp_dict_value_2 = Py_True;
        generator_heap->tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(generator_heap->tmp_res != 0) );
        generator->m_frame->m_frame.f_lineno = 303;
        tmp_expression_name_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 303;
            generator_heap->type_description_1 = "Nocc";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_args_name_1, sizeof(PyObject *), &tmp_tuple_element_1, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), &tmp_dict_key_1, sizeof(PyObject *), &tmp_dict_value_1, sizeof(PyObject *), &tmp_dict_key_2, sizeof(PyObject *), &tmp_dict_value_2, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_called_name_1, sizeof(PyObject *), &tmp_source_name_1, sizeof(PyObject *), &tmp_args_name_1, sizeof(PyObject *), &tmp_tuple_element_1, sizeof(PyObject *), &tmp_kw_name_1, sizeof(PyObject *), &tmp_dict_key_1, sizeof(PyObject *), &tmp_dict_value_1, sizeof(PyObject *), &tmp_dict_key_2, sizeof(PyObject *), &tmp_dict_value_2, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 303;
            generator_heap->type_description_1 = "Nocc";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 303;
        generator_heap->type_description_1 = "Nocc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_value,
            generator->m_closure[2],
            generator->m_closure[0]
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_value );
    generator_heap->var_value = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_value );
    generator_heap->var_value = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$$$function_9_goto$$$genexpr_2_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$$$function_9_goto$$$genexpr_2_genexpr_context,
        module_jedi$evaluate,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_dfa371e33dd702765033c7673aa2ad23,
#endif
        codeobj_bcac2ed3740e56ca7545ffd0614eccad,
        3,
        sizeof(struct jedi$evaluate$$$function_9_goto$$$genexpr_2_genexpr_locals)
    );
}


static PyObject *impl_jedi$evaluate$$$function_10_create_context( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_self = PyCell_NEW1( python_pars[ 0 ] );
    struct Nuitka_CellObject *par_base_context = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_node = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *par_node_is_context = python_pars[ 3 ];
    PyObject *par_node_is_object = python_pars[ 4 ];
    PyObject *var_parent_scope = NULL;
    struct Nuitka_CellObject *var_from_scope_node = PyCell_EMPTY();
    struct Nuitka_CellObject *var_base_node = PyCell_EMPTY();
    PyObject *var_scope_node = NULL;
    struct Nuitka_FrameObject *frame_1abbc533a789ddedcff7fa702a374e16;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_1abbc533a789ddedcff7fa702a374e16 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_jedi$evaluate$$$function_10_create_context$$$function_1_parent_scope(  );



        assert( var_parent_scope == NULL );
        var_parent_scope = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_defaults_1;
        tmp_defaults_1 = const_tuple_none_true_false_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_assign_source_2 = MAKE_FUNCTION_jedi$evaluate$$$function_10_create_context$$$function_2_from_scope_node( tmp_defaults_1 );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = par_base_context;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] = var_base_node;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[2] = var_from_scope_node;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[2] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[3] = par_node;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[3] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[4] = par_self;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[4] );


        assert( PyCell_GET( var_from_scope_node ) == NULL );
        PyCell_SET( var_from_scope_node, tmp_assign_source_2 );

    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1abbc533a789ddedcff7fa702a374e16, codeobj_1abbc533a789ddedcff7fa702a374e16, module_jedi$evaluate, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1abbc533a789ddedcff7fa702a374e16 = cache_frame_1abbc533a789ddedcff7fa702a374e16;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1abbc533a789ddedcff7fa702a374e16 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1abbc533a789ddedcff7fa702a374e16 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( PyCell_GET( par_base_context ) );
        tmp_source_name_1 = PyCell_GET( par_base_context );
        tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_tree_node );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;
            type_description_1 = "cccooocco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_base_node ) == NULL );
        PyCell_SET( var_base_node, tmp_assign_source_3 );

    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        int tmp_truth_name_2;
        CHECK_OBJECT( par_node_is_context );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_node_is_context );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 371;
            type_description_1 = "cccooocco";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_parser_utils );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parser_utils );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parser_utils" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 371;
            type_description_1 = "cccooocco";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( PyCell_GET( par_node ) );
        tmp_args_element_name_1 = PyCell_GET( par_node );
        frame_1abbc533a789ddedcff7fa702a374e16->m_frame.f_lineno = 371;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_is_scope, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 371;
            type_description_1 = "cccooocco";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 371;
            type_description_1 = "cccooocco";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_4;
            CHECK_OBJECT( PyCell_GET( par_node ) );
            tmp_assign_source_4 = PyCell_GET( par_node );
            assert( var_scope_node == NULL );
            Py_INCREF( tmp_assign_source_4 );
            var_scope_node = tmp_assign_source_4;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_and_left_truth_2;
            nuitka_bool tmp_and_left_value_2;
            nuitka_bool tmp_and_right_value_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_4;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( PyCell_GET( par_node ) );
            tmp_source_name_3 = PyCell_GET( par_node );
            tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_parent );
            if ( tmp_source_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_1 = "cccooocco";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_type );
            Py_DECREF( tmp_source_name_2 );
            if ( tmp_compexpr_left_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_1 = "cccooocco";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_1 = const_tuple_str_plain_funcdef_str_plain_classdef_tuple;
            tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
            Py_DECREF( tmp_compexpr_left_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_1 = "cccooocco";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_2 = tmp_and_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_2 == 1 )
            {
                goto and_right_2;
            }
            else
            {
                goto and_left_2;
            }
            and_right_2:;
            CHECK_OBJECT( PyCell_GET( par_node ) );
            tmp_source_name_5 = PyCell_GET( par_node );
            tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_parent );
            if ( tmp_source_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_1 = "cccooocco";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_name );
            Py_DECREF( tmp_source_name_4 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_1 = "cccooocco";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( PyCell_GET( par_node ) );
            tmp_compexpr_right_2 = PyCell_GET( par_node );
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 374;
                type_description_1 = "cccooocco";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_and_right_value_2;
            goto and_end_2;
            and_left_2:;
            tmp_condition_result_2 = tmp_and_left_value_2;
            and_end_2:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_source_name_6;
                CHECK_OBJECT( PyCell_GET( par_node ) );
                tmp_source_name_6 = PyCell_GET( par_node );
                tmp_assign_source_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_parent );
                if ( tmp_assign_source_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 377;
                    type_description_1 = "cccooocco";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = PyCell_GET( par_node );
                    PyCell_SET( par_node, tmp_assign_source_5 );
                    Py_XDECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_6;
            PyObject *tmp_called_name_1;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( var_parent_scope );
            tmp_called_name_1 = var_parent_scope;
            CHECK_OBJECT( PyCell_GET( par_node ) );
            tmp_args_element_name_2 = PyCell_GET( par_node );
            frame_1abbc533a789ddedcff7fa702a374e16->m_frame.f_lineno = 378;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            if ( tmp_assign_source_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 378;
                type_description_1 = "cccooocco";
                goto frame_exception_exit_1;
            }
            assert( var_scope_node == NULL );
            var_scope_node = tmp_assign_source_6;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        CHECK_OBJECT( PyCell_GET( var_from_scope_node ) );
        tmp_called_name_2 = PyCell_GET( var_from_scope_node );
        CHECK_OBJECT( var_scope_node );
        tmp_tuple_element_1 = var_scope_node;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_is_nested;
        tmp_dict_value_1 = Py_True;
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_node_is_object;
        CHECK_OBJECT( par_node_is_object );
        tmp_dict_value_2 = par_node_is_object;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_1abbc533a789ddedcff7fa702a374e16->m_frame.f_lineno = 379;
        tmp_return_value = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 379;
            type_description_1 = "cccooocco";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1abbc533a789ddedcff7fa702a374e16 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1abbc533a789ddedcff7fa702a374e16 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1abbc533a789ddedcff7fa702a374e16 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1abbc533a789ddedcff7fa702a374e16, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1abbc533a789ddedcff7fa702a374e16->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1abbc533a789ddedcff7fa702a374e16, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1abbc533a789ddedcff7fa702a374e16,
        type_description_1,
        par_self,
        par_base_context,
        par_node,
        par_node_is_context,
        par_node_is_object,
        var_parent_scope,
        var_from_scope_node,
        var_base_node,
        var_scope_node
    );


    // Release cached frame.
    if ( frame_1abbc533a789ddedcff7fa702a374e16 == cache_frame_1abbc533a789ddedcff7fa702a374e16 )
    {
        Py_DECREF( frame_1abbc533a789ddedcff7fa702a374e16 );
    }
    cache_frame_1abbc533a789ddedcff7fa702a374e16 = NULL;

    assertFrameObject( frame_1abbc533a789ddedcff7fa702a374e16 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_10_create_context );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_base_context );
    Py_DECREF( par_base_context );
    par_base_context = NULL;

    CHECK_OBJECT( (PyObject *)par_node );
    Py_DECREF( par_node );
    par_node = NULL;

    CHECK_OBJECT( (PyObject *)par_node_is_context );
    Py_DECREF( par_node_is_context );
    par_node_is_context = NULL;

    CHECK_OBJECT( (PyObject *)par_node_is_object );
    Py_DECREF( par_node_is_object );
    par_node_is_object = NULL;

    CHECK_OBJECT( (PyObject *)var_parent_scope );
    Py_DECREF( var_parent_scope );
    var_parent_scope = NULL;

    CHECK_OBJECT( (PyObject *)var_from_scope_node );
    Py_DECREF( var_from_scope_node );
    var_from_scope_node = NULL;

    CHECK_OBJECT( (PyObject *)var_base_node );
    Py_DECREF( var_base_node );
    var_base_node = NULL;

    CHECK_OBJECT( (PyObject *)var_scope_node );
    Py_DECREF( var_scope_node );
    var_scope_node = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_base_context );
    Py_DECREF( par_base_context );
    par_base_context = NULL;

    CHECK_OBJECT( (PyObject *)par_node );
    Py_DECREF( par_node );
    par_node = NULL;

    CHECK_OBJECT( (PyObject *)par_node_is_context );
    Py_DECREF( par_node_is_context );
    par_node_is_context = NULL;

    CHECK_OBJECT( (PyObject *)par_node_is_object );
    Py_DECREF( par_node_is_object );
    par_node_is_object = NULL;

    CHECK_OBJECT( (PyObject *)var_parent_scope );
    Py_DECREF( var_parent_scope );
    var_parent_scope = NULL;

    CHECK_OBJECT( (PyObject *)var_from_scope_node );
    Py_DECREF( var_from_scope_node );
    var_from_scope_node = NULL;

    CHECK_OBJECT( (PyObject *)var_base_node );
    Py_DECREF( var_base_node );
    var_base_node = NULL;

    Py_XDECREF( var_scope_node );
    var_scope_node = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_10_create_context );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_10_create_context$$$function_1_parent_scope( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_node = python_pars[ 0 ];
    PyObject *var_n = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_676dba2f48047b5bd58871ef44c2b334;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_676dba2f48047b5bd58871ef44c2b334 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_676dba2f48047b5bd58871ef44c2b334, codeobj_676dba2f48047b5bd58871ef44c2b334, module_jedi$evaluate, sizeof(void *)+sizeof(void *) );
    frame_676dba2f48047b5bd58871ef44c2b334 = cache_frame_676dba2f48047b5bd58871ef44c2b334;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_676dba2f48047b5bd58871ef44c2b334 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_676dba2f48047b5bd58871ef44c2b334 ) == 2 ); // Frame stack

    // Framed code:
    loop_start_1:;
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_node );
        tmp_source_name_1 = par_node;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_parent );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 321;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = par_node;
            assert( old != NULL );
            par_node = tmp_assign_source_1;
            Py_DECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        int tmp_truth_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_parser_utils );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parser_utils );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parser_utils" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 323;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_node );
        tmp_args_element_name_1 = par_node;
        frame_676dba2f48047b5bd58871ef44c2b334->m_frame.f_lineno = 323;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_is_scope, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 323;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 323;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( par_node );
        tmp_return_value = par_node;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_node );
            tmp_source_name_2 = par_node;
            tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_type );
            if ( tmp_compexpr_left_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 325;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_1 = const_tuple_str_plain_argument_str_plain_testlist_comp_tuple;
            tmp_res = PySequence_Contains( tmp_compexpr_right_1, tmp_compexpr_left_1 );
            Py_DECREF( tmp_compexpr_left_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 325;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                PyObject *tmp_source_name_3;
                PyObject *tmp_subscribed_name_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_subscript_name_1;
                CHECK_OBJECT( par_node );
                tmp_source_name_4 = par_node;
                tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_children );
                if ( tmp_subscribed_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 326;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_subscript_name_1 = const_int_pos_1;
                tmp_source_name_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 1 );
                Py_DECREF( tmp_subscribed_name_1 );
                if ( tmp_source_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 326;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_type );
                Py_DECREF( tmp_source_name_3 );
                if ( tmp_compexpr_left_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 326;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_2 = const_str_plain_comp_for;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                Py_DECREF( tmp_compexpr_left_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 326;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_subscribed_name_2;
                    PyObject *tmp_source_name_5;
                    PyObject *tmp_subscript_name_2;
                    CHECK_OBJECT( par_node );
                    tmp_source_name_5 = par_node;
                    tmp_subscribed_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_children );
                    if ( tmp_subscribed_name_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 327;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    tmp_subscript_name_2 = const_int_pos_1;
                    tmp_return_value = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 1 );
                    Py_DECREF( tmp_subscribed_name_2 );
                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 327;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                branch_no_3:;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_4;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                PyObject *tmp_source_name_6;
                CHECK_OBJECT( par_node );
                tmp_source_name_6 = par_node;
                tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_type );
                if ( tmp_compexpr_left_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 328;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_3 = const_str_plain_dictorsetmaker;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
                Py_DECREF( tmp_compexpr_left_3 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 328;
                    type_description_1 = "oo";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_4;
                }
                else
                {
                    goto branch_no_4;
                }
                branch_yes_4:;
                {
                    PyObject *tmp_assign_source_2;
                    PyObject *tmp_iter_arg_1;
                    PyObject *tmp_subscribed_name_3;
                    PyObject *tmp_source_name_7;
                    PyObject *tmp_subscript_name_3;
                    CHECK_OBJECT( par_node );
                    tmp_source_name_7 = par_node;
                    tmp_subscribed_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_children );
                    if ( tmp_subscribed_name_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 329;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    tmp_subscript_name_3 = const_slice_int_pos_1_int_pos_4_none;
                    tmp_iter_arg_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
                    Py_DECREF( tmp_subscribed_name_3 );
                    if ( tmp_iter_arg_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 329;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
                    Py_DECREF( tmp_iter_arg_1 );
                    if ( tmp_assign_source_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 329;
                        type_description_1 = "oo";
                        goto frame_exception_exit_1;
                    }
                    {
                        PyObject *old = tmp_for_loop_1__for_iterator;
                        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
                        Py_XDECREF( old );
                    }

                }
                // Tried code:
                loop_start_2:;
                {
                    PyObject *tmp_next_source_1;
                    PyObject *tmp_assign_source_3;
                    CHECK_OBJECT( tmp_for_loop_1__for_iterator );
                    tmp_next_source_1 = tmp_for_loop_1__for_iterator;
                    tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
                    if ( tmp_assign_source_3 == NULL )
                    {
                        if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                        {

                            goto loop_end_1;
                        }
                        else
                        {

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                            type_description_1 = "oo";
                            exception_lineno = 329;
                            goto try_except_handler_2;
                        }
                    }

                    {
                        PyObject *old = tmp_for_loop_1__iter_value;
                        tmp_for_loop_1__iter_value = tmp_assign_source_3;
                        Py_XDECREF( old );
                    }

                }
                {
                    PyObject *tmp_assign_source_4;
                    CHECK_OBJECT( tmp_for_loop_1__iter_value );
                    tmp_assign_source_4 = tmp_for_loop_1__iter_value;
                    {
                        PyObject *old = var_n;
                        var_n = tmp_assign_source_4;
                        Py_INCREF( var_n );
                        Py_XDECREF( old );
                    }

                }
                {
                    nuitka_bool tmp_condition_result_5;
                    PyObject *tmp_compexpr_left_4;
                    PyObject *tmp_compexpr_right_4;
                    PyObject *tmp_source_name_8;
                    CHECK_OBJECT( var_n );
                    tmp_source_name_8 = var_n;
                    tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_type );
                    if ( tmp_compexpr_left_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 331;
                        type_description_1 = "oo";
                        goto try_except_handler_2;
                    }
                    tmp_compexpr_right_4 = const_str_plain_comp_for;
                    tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                    Py_DECREF( tmp_compexpr_left_4 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 331;
                        type_description_1 = "oo";
                        goto try_except_handler_2;
                    }
                    tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_5;
                    }
                    else
                    {
                        goto branch_no_5;
                    }
                    branch_yes_5:;
                    CHECK_OBJECT( var_n );
                    tmp_return_value = var_n;
                    Py_INCREF( tmp_return_value );
                    goto try_return_handler_2;
                    branch_no_5:;
                }
                if ( CONSIDER_THREADING() == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 329;
                    type_description_1 = "oo";
                    goto try_except_handler_2;
                }
                goto loop_start_2;
                loop_end_1:;
                goto try_end_1;
                // Return handler code:
                try_return_handler_2:;
                CHECK_OBJECT( (PyObject *)tmp_for_loop_1__iter_value );
                Py_DECREF( tmp_for_loop_1__iter_value );
                tmp_for_loop_1__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                Py_DECREF( tmp_for_loop_1__for_iterator );
                tmp_for_loop_1__for_iterator = NULL;

                goto frame_return_exit_1;
                // Exception handler code:
                try_except_handler_2:;
                exception_keeper_type_1 = exception_type;
                exception_keeper_value_1 = exception_value;
                exception_keeper_tb_1 = exception_tb;
                exception_keeper_lineno_1 = exception_lineno;
                exception_type = NULL;
                exception_value = NULL;
                exception_tb = NULL;
                exception_lineno = 0;

                Py_XDECREF( tmp_for_loop_1__iter_value );
                tmp_for_loop_1__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                Py_DECREF( tmp_for_loop_1__for_iterator );
                tmp_for_loop_1__for_iterator = NULL;

                // Re-raise.
                exception_type = exception_keeper_type_1;
                exception_value = exception_keeper_value_1;
                exception_tb = exception_keeper_tb_1;
                exception_lineno = exception_keeper_lineno_1;

                goto frame_exception_exit_1;
                // End of try:
                try_end_1:;
                Py_XDECREF( tmp_for_loop_1__iter_value );
                tmp_for_loop_1__iter_value = NULL;

                CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
                Py_DECREF( tmp_for_loop_1__for_iterator );
                tmp_for_loop_1__for_iterator = NULL;

                branch_no_4:;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 320;
        type_description_1 = "oo";
        goto frame_exception_exit_1;
    }
    goto loop_start_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_676dba2f48047b5bd58871ef44c2b334 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_676dba2f48047b5bd58871ef44c2b334 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_676dba2f48047b5bd58871ef44c2b334 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_676dba2f48047b5bd58871ef44c2b334, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_676dba2f48047b5bd58871ef44c2b334->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_676dba2f48047b5bd58871ef44c2b334, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_676dba2f48047b5bd58871ef44c2b334,
        type_description_1,
        par_node,
        var_n
    );


    // Release cached frame.
    if ( frame_676dba2f48047b5bd58871ef44c2b334 == cache_frame_676dba2f48047b5bd58871ef44c2b334 )
    {
        Py_DECREF( frame_676dba2f48047b5bd58871ef44c2b334 );
    }
    cache_frame_676dba2f48047b5bd58871ef44c2b334 = NULL;

    assertFrameObject( frame_676dba2f48047b5bd58871ef44c2b334 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_10_create_context$$$function_1_parent_scope );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_node );
    Py_DECREF( par_node );
    par_node = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( par_node );
    par_node = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_10_create_context$$$function_1_parent_scope );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_10_create_context$$$function_2_from_scope_node( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_scope_node = python_pars[ 0 ];
    PyObject *par_child_is_funcdef = python_pars[ 1 ];
    PyObject *par_is_nested = python_pars[ 2 ];
    PyObject *par_node_is_object = python_pars[ 3 ];
    nuitka_bool var_is_funcdef = NUITKA_BOOL_UNASSIGNED;
    PyObject *var_parent_scope = NULL;
    PyObject *var_parent_context = NULL;
    PyObject *var_func = NULL;
    PyObject *var_class_context = NULL;
    struct Nuitka_FrameObject *frame_c7a63dba09606a0f5e83c3d23b96e279;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_c7a63dba09606a0f5e83c3d23b96e279 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c7a63dba09606a0f5e83c3d23b96e279, codeobj_c7a63dba09606a0f5e83c3d23b96e279, module_jedi$evaluate, sizeof(nuitka_bool)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c7a63dba09606a0f5e83c3d23b96e279 = cache_frame_c7a63dba09606a0f5e83c3d23b96e279;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c7a63dba09606a0f5e83c3d23b96e279 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c7a63dba09606a0f5e83c3d23b96e279 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_scope_node );
        tmp_compexpr_left_1 = par_scope_node;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "base_node" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 335;
            type_description_1 = "oooobooooccccc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_right_1 = PyCell_GET( self->m_closure[1] );
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 335;
            type_description_1 = "oooobooooccccc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "base_context" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 336;
            type_description_1 = "oooobooooccccc";
            goto frame_exception_exit_1;
        }

        tmp_return_value = PyCell_GET( self->m_closure[0] );
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        nuitka_bool tmp_assign_source_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_scope_node );
        tmp_source_name_1 = par_scope_node;
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_type );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 338;
            type_description_1 = "oooobooooccccc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_tuple_str_plain_funcdef_str_plain_lambdef_tuple;
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 338;
            type_description_1 = "oooobooooccccc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        var_is_funcdef = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_parser_utils );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_parser_utils );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "parser_utils" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 339;
            type_description_1 = "oooobooooccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_scope_node );
        tmp_args_element_name_1 = par_scope_node;
        frame_c7a63dba09606a0f5e83c3d23b96e279->m_frame.f_lineno = 339;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_get_parent_scope, call_args );
        }

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 339;
            type_description_1 = "oooobooooccccc";
            goto frame_exception_exit_1;
        }
        assert( var_parent_scope == NULL );
        var_parent_scope = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        if ( PyCell_GET( self->m_closure[2] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "from_scope_node" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 340;
            type_description_1 = "oooobooooccccc";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = PyCell_GET( self->m_closure[2] );
        CHECK_OBJECT( var_parent_scope );
        tmp_tuple_element_1 = var_parent_scope;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_child_is_funcdef;
        assert( var_is_funcdef != NUITKA_BOOL_UNASSIGNED);
        tmp_dict_value_1 = ( var_is_funcdef == NUITKA_BOOL_TRUE ) ? Py_True : Py_False;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_c7a63dba09606a0f5e83c3d23b96e279->m_frame.f_lineno = 340;
        tmp_assign_source_3 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 340;
            type_description_1 = "oooobooooccccc";
            goto frame_exception_exit_1;
        }
        assert( var_parent_context == NULL );
        var_parent_context = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_2;
        assert( var_is_funcdef != NUITKA_BOOL_UNASSIGNED);
        tmp_condition_result_2 = var_is_funcdef;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_FunctionContext );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_FunctionContext );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "FunctionContext" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 343;
                type_description_1 = "oooobooooccccc";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_2 = tmp_mvar_value_2;
            CHECK_OBJECT( var_parent_context );
            tmp_args_element_name_2 = var_parent_context;
            CHECK_OBJECT( par_scope_node );
            tmp_args_element_name_3 = par_scope_node;
            frame_c7a63dba09606a0f5e83c3d23b96e279->m_frame.f_lineno = 343;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
                tmp_assign_source_4 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_from_context, call_args );
            }

            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 343;
                type_description_1 = "oooobooooccccc";
                goto frame_exception_exit_1;
            }
            assert( var_func == NULL );
            var_func = tmp_assign_source_4;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_isinstance_inst_1;
            PyObject *tmp_isinstance_cls_1;
            PyObject *tmp_mvar_value_3;
            CHECK_OBJECT( var_parent_context );
            tmp_isinstance_inst_1 = var_parent_context;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_AnonymousInstance );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnonymousInstance );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnonymousInstance" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 347;
                type_description_1 = "oooobooooccccc";
                goto frame_exception_exit_1;
            }

            tmp_isinstance_cls_1 = tmp_mvar_value_3;
            tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 347;
                type_description_1 = "oooobooooccccc";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_called_name_2;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_kw_name_2;
                PyObject *tmp_dict_key_2;
                PyObject *tmp_dict_value_2;
                PyObject *tmp_dict_key_3;
                PyObject *tmp_dict_value_3;
                PyObject *tmp_source_name_2;
                PyObject *tmp_dict_key_4;
                PyObject *tmp_dict_value_4;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_BoundMethod );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BoundMethod );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BoundMethod" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 348;
                    type_description_1 = "oooobooooccccc";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_2 = tmp_mvar_value_4;
                tmp_dict_key_2 = const_str_plain_instance;
                CHECK_OBJECT( var_parent_context );
                tmp_dict_value_2 = var_parent_context;
                tmp_kw_name_2 = _PyDict_NewPresized( 3 );
                tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
                assert( !(tmp_res != 0) );
                tmp_dict_key_3 = const_str_plain_klass;
                CHECK_OBJECT( var_parent_context );
                tmp_source_name_2 = var_parent_context;
                tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_class_context );
                if ( tmp_dict_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_kw_name_2 );

                    exception_lineno = 350;
                    type_description_1 = "oooobooooccccc";
                    goto frame_exception_exit_1;
                }
                tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
                Py_DECREF( tmp_dict_value_3 );
                assert( !(tmp_res != 0) );
                tmp_dict_key_4 = const_str_plain_function;
                CHECK_OBJECT( var_func );
                tmp_dict_value_4 = var_func;
                tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_4, tmp_dict_value_4 );
                assert( !(tmp_res != 0) );
                frame_c7a63dba09606a0f5e83c3d23b96e279->m_frame.f_lineno = 348;
                tmp_assign_source_5 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
                Py_DECREF( tmp_kw_name_2 );
                if ( tmp_assign_source_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 348;
                    type_description_1 = "oooobooooccccc";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_func;
                    assert( old != NULL );
                    var_func = tmp_assign_source_5;
                    Py_DECREF( old );
                }

            }
            branch_no_3:;
        }
        {
            nuitka_bool tmp_condition_result_4;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            int tmp_truth_name_1;
            PyObject *tmp_operand_name_1;
            CHECK_OBJECT( par_is_nested );
            tmp_truth_name_1 = CHECK_IF_TRUE( par_is_nested );
            if ( tmp_truth_name_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 353;
                type_description_1 = "oooobooooccccc";
                goto frame_exception_exit_1;
            }
            tmp_and_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( par_node_is_object );
            tmp_operand_name_1 = par_node_is_object;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 353;
                type_description_1 = "oooobooooccccc";
                goto frame_exception_exit_1;
            }
            tmp_and_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_4 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_4 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_called_instance_3;
                CHECK_OBJECT( var_func );
                tmp_called_instance_3 = var_func;
                frame_c7a63dba09606a0f5e83c3d23b96e279->m_frame.f_lineno = 354;
                tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_function_execution );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 354;
                    type_description_1 = "oooobooooccccc";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            branch_no_4:;
        }
        CHECK_OBJECT( var_func );
        tmp_return_value = var_func;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( par_scope_node );
            tmp_source_name_3 = par_scope_node;
            tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_type );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 356;
                type_description_1 = "oooobooooccccc";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_3 = const_str_plain_classdef;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 356;
                type_description_1 = "oooobooooccccc";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_5 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_called_name_3;
                PyObject *tmp_mvar_value_5;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_args_element_name_5;
                PyObject *tmp_args_element_name_6;
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ClassContext );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ClassContext );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ClassContext" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 357;
                    type_description_1 = "oooobooooccccc";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_3 = tmp_mvar_value_5;
                if ( PyCell_GET( self->m_closure[4] ) == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 357;
                    type_description_1 = "oooobooooccccc";
                    goto frame_exception_exit_1;
                }

                tmp_args_element_name_4 = PyCell_GET( self->m_closure[4] );
                CHECK_OBJECT( var_parent_context );
                tmp_args_element_name_5 = var_parent_context;
                CHECK_OBJECT( par_scope_node );
                tmp_args_element_name_6 = par_scope_node;
                frame_c7a63dba09606a0f5e83c3d23b96e279->m_frame.f_lineno = 357;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
                    tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, call_args );
                }

                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 357;
                    type_description_1 = "oooobooooccccc";
                    goto frame_exception_exit_1;
                }
                assert( var_class_context == NULL );
                var_class_context = tmp_assign_source_6;
            }
            {
                nuitka_bool tmp_condition_result_6;
                int tmp_truth_name_2;
                CHECK_OBJECT( par_child_is_funcdef );
                tmp_truth_name_2 = CHECK_IF_TRUE( par_child_is_funcdef );
                if ( tmp_truth_name_2 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 358;
                    type_description_1 = "oooobooooccccc";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_6 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_6;
                }
                else
                {
                    goto branch_no_6;
                }
                branch_yes_6:;
                {
                    PyObject *tmp_called_name_4;
                    PyObject *tmp_mvar_value_6;
                    PyObject *tmp_args_element_name_7;
                    PyObject *tmp_args_element_name_8;
                    PyObject *tmp_args_element_name_9;
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_AnonymousInstance );

                    if (unlikely( tmp_mvar_value_6 == NULL ))
                    {
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AnonymousInstance );
                    }

                    if ( tmp_mvar_value_6 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AnonymousInstance" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 360;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_4 = tmp_mvar_value_6;
                    if ( PyCell_GET( self->m_closure[4] ) == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "self" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 360;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }

                    tmp_args_element_name_7 = PyCell_GET( self->m_closure[4] );
                    CHECK_OBJECT( var_parent_context );
                    tmp_args_element_name_8 = var_parent_context;
                    CHECK_OBJECT( var_class_context );
                    tmp_args_element_name_9 = var_class_context;
                    frame_c7a63dba09606a0f5e83c3d23b96e279->m_frame.f_lineno = 360;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_7, tmp_args_element_name_8, tmp_args_element_name_9 };
                        tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
                    }

                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 360;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                goto branch_end_6;
                branch_no_6:;
                CHECK_OBJECT( var_class_context );
                tmp_return_value = var_class_context;
                Py_INCREF( tmp_return_value );
                goto frame_return_exit_1;
                branch_end_6:;
            }
            goto branch_end_5;
            branch_no_5:;
            {
                nuitka_bool tmp_condition_result_7;
                PyObject *tmp_compexpr_left_4;
                PyObject *tmp_compexpr_right_4;
                PyObject *tmp_source_name_4;
                CHECK_OBJECT( par_scope_node );
                tmp_source_name_4 = par_scope_node;
                tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_type );
                if ( tmp_compexpr_left_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 363;
                    type_description_1 = "oooobooooccccc";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_4 = const_str_plain_comp_for;
                tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
                Py_DECREF( tmp_compexpr_left_4 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 363;
                    type_description_1 = "oooobooooccccc";
                    goto frame_exception_exit_1;
                }
                tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_7;
                }
                else
                {
                    goto branch_no_7;
                }
                branch_yes_7:;
                {
                    nuitka_bool tmp_condition_result_8;
                    PyObject *tmp_compexpr_left_5;
                    PyObject *tmp_compexpr_right_5;
                    PyObject *tmp_source_name_5;
                    PyObject *tmp_source_name_6;
                    PyObject *tmp_subscribed_name_1;
                    PyObject *tmp_source_name_7;
                    PyObject *tmp_subscript_name_1;
                    if ( PyCell_GET( self->m_closure[3] ) == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "node" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 364;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }

                    tmp_source_name_5 = PyCell_GET( self->m_closure[3] );
                    tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_start_pos );
                    if ( tmp_compexpr_left_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 364;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( par_scope_node );
                    tmp_source_name_7 = par_scope_node;
                    tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_children );
                    if ( tmp_subscribed_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_compexpr_left_5 );

                        exception_lineno = 364;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }
                    tmp_subscript_name_1 = const_int_neg_1;
                    tmp_source_name_6 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, -1 );
                    Py_DECREF( tmp_subscribed_name_1 );
                    if ( tmp_source_name_6 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_compexpr_left_5 );

                        exception_lineno = 364;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }
                    tmp_compexpr_right_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_start_pos );
                    Py_DECREF( tmp_source_name_6 );
                    if ( tmp_compexpr_right_5 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_compexpr_left_5 );

                        exception_lineno = 364;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }
                    tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
                    Py_DECREF( tmp_compexpr_left_5 );
                    Py_DECREF( tmp_compexpr_right_5 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 364;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }
                    tmp_condition_result_8 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_8;
                    }
                    else
                    {
                        goto branch_no_8;
                    }
                    branch_yes_8:;
                    CHECK_OBJECT( var_parent_context );
                    tmp_return_value = var_parent_context;
                    Py_INCREF( tmp_return_value );
                    goto frame_return_exit_1;
                    branch_no_8:;
                }
                {
                    PyObject *tmp_called_instance_4;
                    PyObject *tmp_mvar_value_7;
                    PyObject *tmp_args_element_name_10;
                    PyObject *tmp_args_element_name_11;
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_CompForContext );

                    if (unlikely( tmp_mvar_value_7 == NULL ))
                    {
                        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_CompForContext );
                    }

                    if ( tmp_mvar_value_7 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "CompForContext" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 366;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_instance_4 = tmp_mvar_value_7;
                    CHECK_OBJECT( var_parent_context );
                    tmp_args_element_name_10 = var_parent_context;
                    CHECK_OBJECT( par_scope_node );
                    tmp_args_element_name_11 = par_scope_node;
                    frame_c7a63dba09606a0f5e83c3d23b96e279->m_frame.f_lineno = 366;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11 };
                        tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_4, const_str_plain_from_comp_for, call_args );
                    }

                    if ( tmp_return_value == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 366;
                        type_description_1 = "oooobooooccccc";
                        goto frame_exception_exit_1;
                    }
                    goto frame_return_exit_1;
                }
                branch_no_7:;
            }
            branch_end_5:;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_raise_type_1;
        PyObject *tmp_make_exception_arg_1;
        tmp_make_exception_arg_1 = const_str_digest_f2b6cee7509904702185a89cc332ceaa;
        frame_c7a63dba09606a0f5e83c3d23b96e279->m_frame.f_lineno = 367;
        {
            PyObject *call_args[] = { tmp_make_exception_arg_1 };
            tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_Exception, call_args );
        }

        assert( !(tmp_raise_type_1 == NULL) );
        exception_type = tmp_raise_type_1;
        exception_lineno = 367;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "oooobooooccccc";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c7a63dba09606a0f5e83c3d23b96e279 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c7a63dba09606a0f5e83c3d23b96e279 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c7a63dba09606a0f5e83c3d23b96e279 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c7a63dba09606a0f5e83c3d23b96e279, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c7a63dba09606a0f5e83c3d23b96e279->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c7a63dba09606a0f5e83c3d23b96e279, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c7a63dba09606a0f5e83c3d23b96e279,
        type_description_1,
        par_scope_node,
        par_child_is_funcdef,
        par_is_nested,
        par_node_is_object,
        (int)var_is_funcdef,
        var_parent_scope,
        var_parent_context,
        var_func,
        var_class_context,
        self->m_closure[1],
        self->m_closure[0],
        self->m_closure[2],
        self->m_closure[4],
        self->m_closure[3]
    );


    // Release cached frame.
    if ( frame_c7a63dba09606a0f5e83c3d23b96e279 == cache_frame_c7a63dba09606a0f5e83c3d23b96e279 )
    {
        Py_DECREF( frame_c7a63dba09606a0f5e83c3d23b96e279 );
    }
    cache_frame_c7a63dba09606a0f5e83c3d23b96e279 = NULL;

    assertFrameObject( frame_c7a63dba09606a0f5e83c3d23b96e279 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_10_create_context$$$function_2_from_scope_node );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_scope_node );
    Py_DECREF( par_scope_node );
    par_scope_node = NULL;

    CHECK_OBJECT( (PyObject *)par_child_is_funcdef );
    Py_DECREF( par_child_is_funcdef );
    par_child_is_funcdef = NULL;

    CHECK_OBJECT( (PyObject *)par_is_nested );
    Py_DECREF( par_is_nested );
    par_is_nested = NULL;

    CHECK_OBJECT( (PyObject *)par_node_is_object );
    Py_DECREF( par_node_is_object );
    par_node_is_object = NULL;

    Py_XDECREF( var_parent_scope );
    var_parent_scope = NULL;

    Py_XDECREF( var_parent_context );
    var_parent_context = NULL;

    Py_XDECREF( var_func );
    var_func = NULL;

    Py_XDECREF( var_class_context );
    var_class_context = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_scope_node );
    Py_DECREF( par_scope_node );
    par_scope_node = NULL;

    CHECK_OBJECT( (PyObject *)par_child_is_funcdef );
    Py_DECREF( par_child_is_funcdef );
    par_child_is_funcdef = NULL;

    CHECK_OBJECT( (PyObject *)par_is_nested );
    Py_DECREF( par_is_nested );
    par_is_nested = NULL;

    CHECK_OBJECT( (PyObject *)par_node_is_object );
    Py_DECREF( par_node_is_object );
    par_node_is_object = NULL;

    Py_XDECREF( var_parent_scope );
    var_parent_scope = NULL;

    Py_XDECREF( var_parent_context );
    var_parent_context = NULL;

    Py_XDECREF( var_func );
    var_func = NULL;

    Py_XDECREF( var_class_context );
    var_class_context = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_10_create_context$$$function_2_from_scope_node );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_11_parse_and_get_code( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_code = python_pars[ 1 ];
    PyObject *par_path = python_pars[ 2 ];
    PyObject *par_encoding = python_pars[ 3 ];
    PyObject *par_kwargs = python_pars[ 4 ];
    PyObject *var_f = NULL;
    PyObject *tmp_with_1__enter = NULL;
    PyObject *tmp_with_1__exit = NULL;
    nuitka_bool tmp_with_1__indicator = NUITKA_BOOL_UNASSIGNED;
    PyObject *tmp_with_1__source = NULL;
    struct Nuitka_FrameObject *frame_1b56503422e36b4df7737050f8cb3c09;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_1b56503422e36b4df7737050f8cb3c09 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1b56503422e36b4df7737050f8cb3c09, codeobj_1b56503422e36b4df7737050f8cb3c09, module_jedi$evaluate, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1b56503422e36b4df7737050f8cb3c09 = cache_frame_1b56503422e36b4df7737050f8cb3c09;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1b56503422e36b4df7737050f8cb3c09 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1b56503422e36b4df7737050f8cb3c09 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_allow_different_encoding );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 382;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 382;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( par_code );
            tmp_compexpr_left_1 = par_code;
            tmp_compexpr_right_1 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            // Tried code:
            {
                PyObject *tmp_assign_source_1;
                PyObject *tmp_open_filename_1;
                PyObject *tmp_open_mode_1;
                CHECK_OBJECT( par_path );
                tmp_open_filename_1 = par_path;
                tmp_open_mode_1 = const_str_plain_rb;
                tmp_assign_source_1 = BUILTIN_OPEN( tmp_open_filename_1, tmp_open_mode_1, NULL, NULL, NULL, NULL, NULL, NULL );
                if ( tmp_assign_source_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 384;
                    type_description_1 = "oooooo";
                    goto try_except_handler_2;
                }
                assert( tmp_with_1__source == NULL );
                tmp_with_1__source = tmp_assign_source_1;
            }
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_called_name_1;
                PyObject *tmp_source_name_2;
                CHECK_OBJECT( tmp_with_1__source );
                tmp_source_name_2 = tmp_with_1__source;
                tmp_called_name_1 = LOOKUP_SPECIAL( tmp_source_name_2, const_str_plain___enter__ );
                if ( tmp_called_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 384;
                    type_description_1 = "oooooo";
                    goto try_except_handler_2;
                }
                frame_1b56503422e36b4df7737050f8cb3c09->m_frame.f_lineno = 384;
                tmp_assign_source_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
                Py_DECREF( tmp_called_name_1 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 384;
                    type_description_1 = "oooooo";
                    goto try_except_handler_2;
                }
                assert( tmp_with_1__enter == NULL );
                tmp_with_1__enter = tmp_assign_source_2;
            }
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_source_name_3;
                CHECK_OBJECT( tmp_with_1__source );
                tmp_source_name_3 = tmp_with_1__source;
                tmp_assign_source_3 = LOOKUP_SPECIAL( tmp_source_name_3, const_str_plain___exit__ );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 384;
                    type_description_1 = "oooooo";
                    goto try_except_handler_2;
                }
                assert( tmp_with_1__exit == NULL );
                tmp_with_1__exit = tmp_assign_source_3;
            }
            {
                nuitka_bool tmp_assign_source_4;
                tmp_assign_source_4 = NUITKA_BOOL_TRUE;
                tmp_with_1__indicator = tmp_assign_source_4;
            }
            {
                PyObject *tmp_assign_source_5;
                CHECK_OBJECT( tmp_with_1__enter );
                tmp_assign_source_5 = tmp_with_1__enter;
                assert( var_f == NULL );
                Py_INCREF( tmp_assign_source_5 );
                var_f = tmp_assign_source_5;
            }
            // Tried code:
            // Tried code:
            {
                PyObject *tmp_assign_source_6;
                PyObject *tmp_called_instance_1;
                CHECK_OBJECT( var_f );
                tmp_called_instance_1 = var_f;
                frame_1b56503422e36b4df7737050f8cb3c09->m_frame.f_lineno = 385;
                tmp_assign_source_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_read );
                if ( tmp_assign_source_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 385;
                    type_description_1 = "oooooo";
                    goto try_except_handler_4;
                }
                {
                    PyObject *old = par_code;
                    assert( old != NULL );
                    par_code = tmp_assign_source_6;
                    Py_DECREF( old );
                }

            }
            goto try_end_1;
            // Exception handler code:
            try_except_handler_4:;
            exception_keeper_type_1 = exception_type;
            exception_keeper_value_1 = exception_value;
            exception_keeper_tb_1 = exception_tb;
            exception_keeper_lineno_1 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Preserve existing published exception.
            exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_type_1 );
            exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
            Py_XINCREF( exception_preserved_value_1 );
            exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
            Py_XINCREF( exception_preserved_tb_1 );

            if ( exception_keeper_tb_1 == NULL )
            {
                exception_keeper_tb_1 = MAKE_TRACEBACK( frame_1b56503422e36b4df7737050f8cb3c09, exception_keeper_lineno_1 );
            }
            else if ( exception_keeper_lineno_1 != 0 )
            {
                exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_1b56503422e36b4df7737050f8cb3c09, exception_keeper_lineno_1 );
            }

            NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
            PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
            PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
            // Tried code:
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_2;
                PyObject *tmp_compexpr_right_2;
                tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
                tmp_compexpr_right_2 = PyExc_BaseException;
                tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
                if ( tmp_res == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 384;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                }
                tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    nuitka_bool tmp_assign_source_7;
                    tmp_assign_source_7 = NUITKA_BOOL_FALSE;
                    tmp_with_1__indicator = tmp_assign_source_7;
                }
                {
                    nuitka_bool tmp_condition_result_4;
                    PyObject *tmp_operand_name_1;
                    PyObject *tmp_called_name_2;
                    PyObject *tmp_args_element_name_1;
                    PyObject *tmp_args_element_name_2;
                    PyObject *tmp_args_element_name_3;
                    CHECK_OBJECT( tmp_with_1__exit );
                    tmp_called_name_2 = tmp_with_1__exit;
                    tmp_args_element_name_1 = EXC_TYPE(PyThreadState_GET());
                    tmp_args_element_name_2 = EXC_VALUE(PyThreadState_GET());
                    tmp_args_element_name_3 = EXC_TRACEBACK(PyThreadState_GET());
                    frame_1b56503422e36b4df7737050f8cb3c09->m_frame.f_lineno = 385;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3 };
                        tmp_operand_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
                    }

                    if ( tmp_operand_name_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 385;
                        type_description_1 = "oooooo";
                        goto try_except_handler_5;
                    }
                    tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
                    Py_DECREF( tmp_operand_name_1 );
                    if ( tmp_res == -1 )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 385;
                        type_description_1 = "oooooo";
                        goto try_except_handler_5;
                    }
                    tmp_condition_result_4 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_4;
                    }
                    else
                    {
                        goto branch_no_4;
                    }
                    branch_yes_4:;
                    tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    if (unlikely( tmp_result == false ))
                    {
                        exception_lineno = 385;
                    }

                    if (exception_tb && exception_tb->tb_frame == &frame_1b56503422e36b4df7737050f8cb3c09->m_frame) frame_1b56503422e36b4df7737050f8cb3c09->m_frame.f_lineno = exception_tb->tb_lineno;
                    type_description_1 = "oooooo";
                    goto try_except_handler_5;
                    branch_no_4:;
                }
                goto branch_end_3;
                branch_no_3:;
                tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                if (unlikely( tmp_result == false ))
                {
                    exception_lineno = 384;
                }

                if (exception_tb && exception_tb->tb_frame == &frame_1b56503422e36b4df7737050f8cb3c09->m_frame) frame_1b56503422e36b4df7737050f8cb3c09->m_frame.f_lineno = exception_tb->tb_lineno;
                type_description_1 = "oooooo";
                goto try_except_handler_5;
                branch_end_3:;
            }
            goto try_end_2;
            // Exception handler code:
            try_except_handler_5:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto try_except_handler_3;
            // End of try:
            try_end_2:;
            // Restore previous exception.
            SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
            goto try_end_1;
            // exception handler codes exits in all cases
            NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_11_parse_and_get_code );
            return NULL;
            // End of try:
            try_end_1:;
            goto try_end_3;
            // Exception handler code:
            try_except_handler_3:;
            exception_keeper_type_3 = exception_type;
            exception_keeper_value_3 = exception_value;
            exception_keeper_tb_3 = exception_tb;
            exception_keeper_lineno_3 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            {
                nuitka_bool tmp_condition_result_5;
                nuitka_bool tmp_compexpr_left_3;
                nuitka_bool tmp_compexpr_right_3;
                assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
                tmp_compexpr_left_3 = tmp_with_1__indicator;
                tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
                tmp_condition_result_5 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_5;
                }
                else
                {
                    goto branch_no_5;
                }
                branch_yes_5:;
                {
                    PyObject *tmp_called_name_3;
                    PyObject *tmp_call_result_1;
                    CHECK_OBJECT( tmp_with_1__exit );
                    tmp_called_name_3 = tmp_with_1__exit;
                    frame_1b56503422e36b4df7737050f8cb3c09->m_frame.f_lineno = 385;
                    tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                    if ( tmp_call_result_1 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        Py_DECREF( exception_keeper_type_3 );
                        Py_XDECREF( exception_keeper_value_3 );
                        Py_XDECREF( exception_keeper_tb_3 );

                        exception_lineno = 385;
                        type_description_1 = "oooooo";
                        goto try_except_handler_2;
                    }
                    Py_DECREF( tmp_call_result_1 );
                }
                branch_no_5:;
            }
            // Re-raise.
            exception_type = exception_keeper_type_3;
            exception_value = exception_keeper_value_3;
            exception_tb = exception_keeper_tb_3;
            exception_lineno = exception_keeper_lineno_3;

            goto try_except_handler_2;
            // End of try:
            try_end_3:;
            {
                nuitka_bool tmp_condition_result_6;
                nuitka_bool tmp_compexpr_left_4;
                nuitka_bool tmp_compexpr_right_4;
                assert( tmp_with_1__indicator != NUITKA_BOOL_UNASSIGNED);
                tmp_compexpr_left_4 = tmp_with_1__indicator;
                tmp_compexpr_right_4 = NUITKA_BOOL_TRUE;
                tmp_condition_result_6 = ( tmp_compexpr_left_4 == tmp_compexpr_right_4 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_6;
                }
                else
                {
                    goto branch_no_6;
                }
                branch_yes_6:;
                {
                    PyObject *tmp_called_name_4;
                    PyObject *tmp_call_result_2;
                    CHECK_OBJECT( tmp_with_1__exit );
                    tmp_called_name_4 = tmp_with_1__exit;
                    frame_1b56503422e36b4df7737050f8cb3c09->m_frame.f_lineno = 385;
                    tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_none_none_none_tuple, 0 ) );

                    if ( tmp_call_result_2 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 385;
                        type_description_1 = "oooooo";
                        goto try_except_handler_2;
                    }
                    Py_DECREF( tmp_call_result_2 );
                }
                branch_no_6:;
            }
            goto try_end_4;
            // Exception handler code:
            try_except_handler_2:;
            exception_keeper_type_4 = exception_type;
            exception_keeper_value_4 = exception_value;
            exception_keeper_tb_4 = exception_tb;
            exception_keeper_lineno_4 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_with_1__source );
            tmp_with_1__source = NULL;

            Py_XDECREF( tmp_with_1__enter );
            tmp_with_1__enter = NULL;

            Py_XDECREF( tmp_with_1__exit );
            tmp_with_1__exit = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_4;
            exception_value = exception_keeper_value_4;
            exception_tb = exception_keeper_tb_4;
            exception_lineno = exception_keeper_lineno_4;

            goto frame_exception_exit_1;
            // End of try:
            try_end_4:;
            CHECK_OBJECT( (PyObject *)tmp_with_1__source );
            Py_DECREF( tmp_with_1__source );
            tmp_with_1__source = NULL;

            CHECK_OBJECT( (PyObject *)tmp_with_1__enter );
            Py_DECREF( tmp_with_1__enter );
            tmp_with_1__enter = NULL;

            CHECK_OBJECT( (PyObject *)tmp_with_1__exit );
            Py_DECREF( tmp_with_1__exit );
            tmp_with_1__exit = NULL;

            branch_no_2:;
        }
        {
            PyObject *tmp_assign_source_8;
            PyObject *tmp_called_name_5;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_python_bytes_to_unicode );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_python_bytes_to_unicode );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "python_bytes_to_unicode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 386;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_5 = tmp_mvar_value_1;
            if ( par_code == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "code" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 386;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }

            tmp_tuple_element_1 = par_code;
            tmp_args_name_1 = PyTuple_New( 1 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_dict_key_1 = const_str_plain_encoding;
            CHECK_OBJECT( par_encoding );
            tmp_dict_value_1 = par_encoding;
            tmp_kw_name_1 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_2 = const_str_plain_errors;
            tmp_dict_value_2 = const_str_plain_replace;
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            frame_1b56503422e36b4df7737050f8cb3c09->m_frame.f_lineno = 386;
            tmp_assign_source_8 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_assign_source_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 386;
                type_description_1 = "oooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_code;
                par_code = tmp_assign_source_8;
                Py_XDECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_grammar );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 388;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_parse );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 388;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_3 = const_str_plain_code;
        CHECK_OBJECT( par_code );
        tmp_dict_value_3 = par_code;
        tmp_dircall_arg2_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_dircall_arg2_1, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_path;
        CHECK_OBJECT( par_path );
        tmp_dict_value_4 = par_path;
        tmp_res = PyDict_SetItem( tmp_dircall_arg2_1, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_tuple_element_2 = impl___internal__$$$function_10_complex_call_helper_keywords_star_dict( dir_call_args );
        }
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 388;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_return_value = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_return_value, 0, tmp_tuple_element_2 );
        CHECK_OBJECT( par_code );
        tmp_tuple_element_2 = par_code;
        Py_INCREF( tmp_tuple_element_2 );
        PyTuple_SET_ITEM( tmp_return_value, 1, tmp_tuple_element_2 );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b56503422e36b4df7737050f8cb3c09 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b56503422e36b4df7737050f8cb3c09 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1b56503422e36b4df7737050f8cb3c09 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1b56503422e36b4df7737050f8cb3c09, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1b56503422e36b4df7737050f8cb3c09->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1b56503422e36b4df7737050f8cb3c09, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1b56503422e36b4df7737050f8cb3c09,
        type_description_1,
        par_self,
        par_code,
        par_path,
        par_encoding,
        par_kwargs,
        var_f
    );


    // Release cached frame.
    if ( frame_1b56503422e36b4df7737050f8cb3c09 == cache_frame_1b56503422e36b4df7737050f8cb3c09 )
    {
        Py_DECREF( frame_1b56503422e36b4df7737050f8cb3c09 );
    }
    cache_frame_1b56503422e36b4df7737050f8cb3c09 = NULL;

    assertFrameObject( frame_1b56503422e36b4df7737050f8cb3c09 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_11_parse_and_get_code );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_code );
    par_code = NULL;

    CHECK_OBJECT( (PyObject *)par_path );
    Py_DECREF( par_path );
    par_path = NULL;

    CHECK_OBJECT( (PyObject *)par_encoding );
    Py_DECREF( par_encoding );
    par_encoding = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_f );
    var_f = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_11_parse_and_get_code );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$$$function_12_parse( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_args = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_18dc33aae5596c8fe54321958a326ffe;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_18dc33aae5596c8fe54321958a326ffe = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_18dc33aae5596c8fe54321958a326ffe, codeobj_18dc33aae5596c8fe54321958a326ffe, module_jedi$evaluate, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_18dc33aae5596c8fe54321958a326ffe = cache_frame_18dc33aae5596c8fe54321958a326ffe;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_18dc33aae5596c8fe54321958a326ffe );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_18dc33aae5596c8fe54321958a326ffe ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_parse_and_get_code );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 391;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_args );
        tmp_dircall_arg2_1 = par_args;
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg2_1 );
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_subscribed_name_1 = impl___internal__$$$function_8_complex_call_helper_star_list_star_dict( dir_call_args );
        }
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 391;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_subscript_name_1 = const_int_0;
        tmp_return_value = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        Py_DECREF( tmp_subscribed_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 391;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18dc33aae5596c8fe54321958a326ffe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_18dc33aae5596c8fe54321958a326ffe );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18dc33aae5596c8fe54321958a326ffe );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_18dc33aae5596c8fe54321958a326ffe, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_18dc33aae5596c8fe54321958a326ffe->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_18dc33aae5596c8fe54321958a326ffe, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_18dc33aae5596c8fe54321958a326ffe,
        type_description_1,
        par_self,
        par_args,
        par_kwargs
    );


    // Release cached frame.
    if ( frame_18dc33aae5596c8fe54321958a326ffe == cache_frame_18dc33aae5596c8fe54321958a326ffe )
    {
        Py_DECREF( frame_18dc33aae5596c8fe54321958a326ffe );
    }
    cache_frame_18dc33aae5596c8fe54321958a326ffe = NULL;

    assertFrameObject( frame_18dc33aae5596c8fe54321958a326ffe );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_12_parse );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_args );
    Py_DECREF( par_args );
    par_args = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$$$function_12_parse );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_10_create_context( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_10_create_context,
        const_str_plain_create_context,
#if PYTHON_VERSION >= 300
        const_str_digest_d5eb4398d75eba489f30149dd13bbd58,
#endif
        codeobj_1abbc533a789ddedcff7fa702a374e16,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_10_create_context$$$function_1_parent_scope(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_10_create_context$$$function_1_parent_scope,
        const_str_plain_parent_scope,
#if PYTHON_VERSION >= 300
        const_str_digest_e1ac7c5ecbc2db84fb55808bd519106e,
#endif
        codeobj_676dba2f48047b5bd58871ef44c2b334,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_10_create_context$$$function_2_from_scope_node( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_10_create_context$$$function_2_from_scope_node,
        const_str_plain_from_scope_node,
#if PYTHON_VERSION >= 300
        const_str_digest_e2ef0d02bd7e929c6eac3df1e414ebca,
#endif
        codeobj_c7a63dba09606a0f5e83c3d23b96e279,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        5
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_11_parse_and_get_code( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_11_parse_and_get_code,
        const_str_plain_parse_and_get_code,
#if PYTHON_VERSION >= 300
        const_str_digest_f0c4d91ae1589d7e44e5490f7304cdd0,
#endif
        codeobj_1b56503422e36b4df7737050f8cb3c09,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_12_parse(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_12_parse,
        const_str_plain_parse,
#if PYTHON_VERSION >= 300
        const_str_digest_28cc99970a69c65c35603c2837677ddc,
#endif
        codeobj_18dc33aae5596c8fe54321958a326ffe,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_1___init__( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_59db0359c689702cccb495bcc3f37c20,
#endif
        codeobj_f74afcd1307791ef9813981c113d2a3e,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_2_builtins_module(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_2_builtins_module,
        const_str_plain_builtins_module,
#if PYTHON_VERSION >= 300
        const_str_digest_f1486e7943f41f76e310574f0b0f1191,
#endif
        codeobj_2dc089a51f40a6a717e824f4d36286f4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_3_reset_recursion_limitations(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_3_reset_recursion_limitations,
        const_str_plain_reset_recursion_limitations,
#if PYTHON_VERSION >= 300
        const_str_digest_87d77a9776f0dd25e126e9a6ca99fb9d,
#endif
        codeobj_34366af6610c2510392812acd68c38c8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_4_get_sys_path(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_4_get_sys_path,
        const_str_plain_get_sys_path,
#if PYTHON_VERSION >= 300
        const_str_digest_eb61ded08f95ef617dbf07a9230a3e2a,
#endif
        codeobj_3c082f47970dd06c3f9fdaf39b85b64f,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        const_str_digest_317b70c9d1c8e8e333fd4abb8e1dfd47,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_5_eval_element(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_5_eval_element,
        const_str_plain_eval_element,
#if PYTHON_VERSION >= 300
        const_str_digest_a8ac27e834fc869b2d620e1cc177f3c5,
#endif
        codeobj_96fcd1e076c30934fa2f8245d9f4ab88,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_6__eval_element_if_evaluated(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_6__eval_element_if_evaluated,
        const_str_plain__eval_element_if_evaluated,
#if PYTHON_VERSION >= 300
        const_str_digest_126bdc407864ab2aedc0013c87731837,
#endif
        codeobj_f4732f2d02c8981381ba2a86f8ad96ad,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        const_str_digest_cf63cd7b5c10423b185dcfbc1879e8c4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_7__eval_element_cached(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_7__eval_element_cached,
        const_str_plain__eval_element_cached,
#if PYTHON_VERSION >= 300
        const_str_digest_0ad84cb297d873a7a5c80311221c5f60,
#endif
        codeobj_a3a4783c51555953fecb657c25a9d529,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_8_goto_definitions(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_8_goto_definitions,
        const_str_plain_goto_definitions,
#if PYTHON_VERSION >= 300
        const_str_digest_1d3c1156eed89cb991ed261e39b0f847,
#endif
        codeobj_6236a960357bfdbc01ec332bbb5c85ed,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$$$function_9_goto(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$$$function_9_goto,
        const_str_plain_goto,
#if PYTHON_VERSION >= 300
        const_str_digest_0d48b2b4f5b9085e0fa7992bfcc8735d,
#endif
        codeobj_83ec4da756226c82e4def57f9169641d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jedi$evaluate =
{
    PyModuleDef_HEAD_INIT,
    "jedi.evaluate",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jedi$evaluate)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jedi$evaluate)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jedi$evaluate );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjedi$evaluate" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jedi$evaluate = Py_InitModule4(
        "jedi.evaluate",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jedi$evaluate = PyModule_Create( &mdef_jedi$evaluate );
#endif

    moduledict_jedi$evaluate = MODULE_DICT( module_jedi$evaluate );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jedi$evaluate,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 1
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jedi$evaluate,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jedi$evaluate,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jedi$evaluate,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jedi$evaluate );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01, module_jedi$evaluate );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 1
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    PyObject *tmp_import_from_4__module = NULL;
    struct Nuitka_FrameObject *frame_e0a1c8056d1a3ed2db3342956d8927fb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_jedi$evaluate_88 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_528e69644582a95e5c6c292b45da64dd_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_528e69644582a95e5c6c292b45da64dd_2 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_3bf0f9fb9b6b71031574a6f750b452a3;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_e0a1c8056d1a3ed2db3342956d8927fb = MAKE_MODULE_FRAME( codeobj_e0a1c8056d1a3ed2db3342956d8927fb, module_jedi$evaluate );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_e0a1c8056d1a3ed2db3342956d8927fb );
    assert( Py_REFCNT( frame_e0a1c8056d1a3ed2db3342956d8927fb ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_list_element_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_instance_2;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_source_name_1 = PyObject_GetAttr( module, const_str_plain_path );
            }
            else
            {
                tmp_source_name_1 = NULL;
            }
        }

        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_dirname );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = module_filename_obj;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 1;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = PyList_New( 3 );
        PyList_SET_ITEM( tmp_assign_source_3, 0, tmp_list_element_1 );
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_source_name_2 = PyObject_GetAttr( module, const_str_plain_path );
            }
            else
            {
                tmp_source_name_2 = NULL;
            }
        }

        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_join );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_called_instance_1 = PyObject_GetAttr( module, const_str_plain_environ );
            }
            else
            {
                tmp_called_instance_1 = NULL;
            }
        }

        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 1;
        tmp_args_element_name_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_f0f15781590b4fd8bce07dd7072b2735_tuple, 0 ) );

        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = const_str_plain_evaluate;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 1;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_list_element_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assign_source_3, 1, tmp_list_element_1 );
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 1;
        {
            PyObject *module = PyImport_ImportModule("os");
            if (likely( module != NULL ))
            {
                tmp_called_instance_2 = PyObject_GetAttr( module, const_str_plain_environ );
            }
            else
            {
                tmp_called_instance_2 = NULL;
            }
        }

        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 1;
        tmp_list_element_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_get, &PyTuple_GET_ITEM( const_tuple_f5dfc96f66e16a3f34903f1a50794749_tuple, 0 ) );

        if ( tmp_list_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assign_source_3 );

            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        PyList_SET_ITEM( tmp_assign_source_3, 2, tmp_list_element_1 );
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___path__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_4;
        tmp_assign_source_4 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_3d8c90d9a9bb20202fbddcdf6f6b7d19;
        tmp_globals_name_1 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_tree_tuple;
        tmp_level_name_1 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 66;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_tree );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_tree, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_parso;
        tmp_globals_name_2 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 67;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_parso, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_parso;
        tmp_globals_name_3 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_python_bytes_to_unicode_tuple;
        tmp_level_name_3 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 68;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_python_bytes_to_unicode );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_python_bytes_to_unicode, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_jedi;
        tmp_globals_name_4 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_debug_tuple;
        tmp_level_name_4 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 70;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_debug );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_debug, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_jedi;
        tmp_globals_name_5 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_parser_utils_tuple;
        tmp_level_name_5 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 71;
        tmp_import_name_from_4 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_parser_utils );
        Py_DECREF( tmp_import_name_from_4 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 71;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_parser_utils, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_0c6a1637c5fdafe468f822c51ad84a09;
        tmp_globals_name_6 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_unite_tuple;
        tmp_level_name_6 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 72;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_unite );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 72;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_unite, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_7 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_imports_tuple;
        tmp_level_name_7 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 73;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_imports );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_imports, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_8 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_recursion_tuple;
        tmp_level_name_8 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 74;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_recursion );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_recursion, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_c3884810e9ca35af32bf1ca927da66bd;
        tmp_globals_name_9 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_evaluator_function_cache_tuple;
        tmp_level_name_9 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 75;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_evaluator_function_cache );
        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 75;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_evaluator_function_cache, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_9;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_10 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_compiled_tuple;
        tmp_level_name_10 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 76;
        tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_import_name_from_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_compiled );
        Py_DECREF( tmp_import_name_from_9 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 76;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_compiled, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_10;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_11 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = const_tuple_str_plain_helpers_tuple;
        tmp_level_name_11 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 77;
        tmp_import_name_from_10 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_import_name_from_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_helpers );
        Py_DECREF( tmp_import_name_from_10 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_helpers, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_digest_c32a307eb4cbe1d85147469cdfd3f906;
        tmp_globals_name_12 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = const_tuple_str_plain_TreeNameDefinition_str_plain_ParamName_tuple;
        tmp_level_name_12 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 78;
        tmp_assign_source_16 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_16;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_11 = tmp_import_from_1__module;
        tmp_assign_source_17 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_TreeNameDefinition );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_TreeNameDefinition, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_12 = tmp_import_from_1__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_ParamName );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 78;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ParamName, tmp_assign_source_18 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_name_name_13;
        PyObject *tmp_globals_name_13;
        PyObject *tmp_locals_name_13;
        PyObject *tmp_fromlist_name_13;
        PyObject *tmp_level_name_13;
        tmp_name_name_13 = const_str_digest_74a64e2326d0e94475afe0623d07b03d;
        tmp_globals_name_13 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_13 = Py_None;
        tmp_fromlist_name_13 = const_tuple_2b54426e6fec59b7053349007edebb94_tuple;
        tmp_level_name_13 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 79;
        tmp_assign_source_19 = IMPORT_MODULE5( tmp_name_name_13, tmp_globals_name_13, tmp_locals_name_13, tmp_fromlist_name_13, tmp_level_name_13 );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_19;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_13 = tmp_import_from_2__module;
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_ContextualizedName );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ContextualizedName, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_14 = tmp_import_from_2__module;
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_ContextualizedNode );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ContextualizedNode, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_import_name_from_15;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_15 = tmp_import_from_2__module;
        tmp_assign_source_22 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_ContextSet );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ContextSet, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_16;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_16 = tmp_import_from_2__module;
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_NO_CONTEXTS );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_17;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_17 = tmp_import_from_2__module;
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_iterate_contexts );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 79;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_iterate_contexts, tmp_assign_source_24 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_name_name_14;
        PyObject *tmp_globals_name_14;
        PyObject *tmp_locals_name_14;
        PyObject *tmp_fromlist_name_14;
        PyObject *tmp_level_name_14;
        tmp_name_name_14 = const_str_digest_785ebe15053a4fa731c98e296de8edd7;
        tmp_globals_name_14 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_14 = Py_None;
        tmp_fromlist_name_14 = const_tuple_87bb68f9132f41b698d8e8be88202bce_tuple;
        tmp_level_name_14 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 81;
        tmp_assign_source_25 = IMPORT_MODULE5( tmp_name_name_14, tmp_globals_name_14, tmp_locals_name_14, tmp_fromlist_name_14, tmp_level_name_14 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_25;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_import_name_from_18;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_18 = tmp_import_from_3__module;
        tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_ClassContext );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_ClassContext, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_import_name_from_19;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_19 = tmp_import_from_3__module;
        tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_19, const_str_plain_FunctionContext );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_FunctionContext, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_import_name_from_20;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_20 = tmp_import_from_3__module;
        tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_20, const_str_plain_AnonymousInstance );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_AnonymousInstance, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_import_name_from_21;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_21 = tmp_import_from_3__module;
        tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_21, const_str_plain_BoundMethod );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_BoundMethod, tmp_assign_source_29 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_import_name_from_22;
        PyObject *tmp_name_name_15;
        PyObject *tmp_globals_name_15;
        PyObject *tmp_locals_name_15;
        PyObject *tmp_fromlist_name_15;
        PyObject *tmp_level_name_15;
        tmp_name_name_15 = const_str_digest_cd4a43d41990fe11ed21029f9707bb7b;
        tmp_globals_name_15 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_15 = Py_None;
        tmp_fromlist_name_15 = const_tuple_str_plain_CompForContext_tuple;
        tmp_level_name_15 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 83;
        tmp_import_name_from_22 = IMPORT_MODULE5( tmp_name_name_15, tmp_globals_name_15, tmp_locals_name_15, tmp_fromlist_name_15, tmp_level_name_15 );
        if ( tmp_import_name_from_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_30 = IMPORT_NAME( tmp_import_name_from_22, const_str_plain_CompForContext );
        Py_DECREF( tmp_import_name_from_22 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 83;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_CompForContext, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_name_name_16;
        PyObject *tmp_globals_name_16;
        PyObject *tmp_locals_name_16;
        PyObject *tmp_fromlist_name_16;
        PyObject *tmp_level_name_16;
        tmp_name_name_16 = const_str_digest_3e1604d04ce104740ab4a0cd2cf306db;
        tmp_globals_name_16 = (PyObject *)moduledict_jedi$evaluate;
        tmp_locals_name_16 = Py_None;
        tmp_fromlist_name_16 = const_tuple_c0e2edd1e7f598687121f66b43e217b3_tuple;
        tmp_level_name_16 = const_int_0;
        frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 84;
        tmp_assign_source_31 = IMPORT_MODULE5( tmp_name_name_16, tmp_globals_name_16, tmp_locals_name_16, tmp_fromlist_name_16, tmp_level_name_16 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_4__module == NULL );
        tmp_import_from_4__module = tmp_assign_source_31;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_import_name_from_23;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_23 = tmp_import_from_4__module;
        tmp_assign_source_32 = IMPORT_NAME( tmp_import_name_from_23, const_str_plain_eval_trailer );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_eval_trailer, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_import_name_from_24;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_24 = tmp_import_from_4__module;
        tmp_assign_source_33 = IMPORT_NAME( tmp_import_name_from_24, const_str_plain_eval_expr_stmt );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_eval_expr_stmt, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_import_name_from_25;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_25 = tmp_import_from_4__module;
        tmp_assign_source_34 = IMPORT_NAME( tmp_import_name_from_25, const_str_plain_eval_node );
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_eval_node, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_import_name_from_26;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_26 = tmp_import_from_4__module;
        tmp_assign_source_35 = IMPORT_NAME( tmp_import_name_from_26, const_str_plain_check_tuple_assignments );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_check_tuple_assignments, tmp_assign_source_35 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_36 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_36;
    }
    {
        PyObject *tmp_assign_source_37;
        tmp_assign_source_37 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_37;
    }
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_5;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_5;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_5;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_5;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_38 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_38;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_5;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_5;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_3 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_39;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_4 = tmp_class_creation_1__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;

                goto try_except_handler_5;
            }
            tmp_tuple_element_1 = const_str_plain_Evaluator;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 88;
            tmp_assign_source_39 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_39 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_39;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_5 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;

                goto try_except_handler_5;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_6;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 88;

                    goto try_except_handler_5;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_6 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_6 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_6 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 88;

                    goto try_except_handler_5;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 88;

                    goto try_except_handler_5;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 88;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_5;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_40;
            tmp_assign_source_40 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_40;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_41;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_jedi$evaluate_88 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_plain_Evaluator;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 88;

            goto try_except_handler_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_528e69644582a95e5c6c292b45da64dd_2, codeobj_528e69644582a95e5c6c292b45da64dd, module_jedi$evaluate, sizeof(void *) );
        frame_528e69644582a95e5c6c292b45da64dd_2 = cache_frame_528e69644582a95e5c6c292b45da64dd_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_528e69644582a95e5c6c292b45da64dd_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_528e69644582a95e5c6c292b45da64dd_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_defaults_1;
            tmp_defaults_1 = const_tuple_none_none_tuple;
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$$$function_1___init__( tmp_defaults_1 );



            tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_called_name_5;
            PyObject *tmp_called_name_6;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_called_name_8;
            PyObject *tmp_called_name_9;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_args_element_name_7;
            tmp_res = MAPPING_HAS_ITEM( locals_jedi$evaluate_88, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_called_name_4 = PyObject_GetItem( locals_jedi$evaluate_88, const_str_plain_property );

            if ( tmp_called_name_4 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 115;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_called_name_6 = PyObject_GetItem( locals_jedi$evaluate_88, const_str_plain_evaluator_function_cache );

            if ( tmp_called_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_evaluator_function_cache );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_evaluator_function_cache );
                }

                if ( tmp_mvar_value_3 == NULL )
                {
                    Py_DECREF( tmp_called_name_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "evaluator_function_cache" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 116;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_6 = tmp_mvar_value_3;
                Py_INCREF( tmp_called_name_6 );
                }
            }

            frame_528e69644582a95e5c6c292b45da64dd_2->m_frame.f_lineno = 116;
            tmp_called_name_5 = CALL_FUNCTION_NO_ARGS( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_6 );
            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_5 = MAKE_FUNCTION_jedi$evaluate$$$function_2_builtins_module(  );



            frame_528e69644582a95e5c6c292b45da64dd_2->m_frame.f_lineno = 116;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_args_element_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_args_element_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            frame_528e69644582a95e5c6c292b45da64dd_2->m_frame.f_lineno = 115;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            tmp_called_name_7 = (PyObject *)&PyProperty_Type;
            tmp_called_name_9 = PyObject_GetItem( locals_jedi$evaluate_88, const_str_plain_evaluator_function_cache );

            if ( tmp_called_name_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_evaluator_function_cache );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_evaluator_function_cache );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "evaluator_function_cache" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 116;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_9 = tmp_mvar_value_4;
                Py_INCREF( tmp_called_name_9 );
                }
            }

            frame_528e69644582a95e5c6c292b45da64dd_2->m_frame.f_lineno = 116;
            tmp_called_name_8 = CALL_FUNCTION_NO_ARGS( tmp_called_name_9 );
            Py_DECREF( tmp_called_name_9 );
            if ( tmp_called_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_7 = MAKE_FUNCTION_jedi$evaluate$$$function_2_builtins_module(  );



            frame_528e69644582a95e5c6c292b45da64dd_2->m_frame.f_lineno = 116;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_args_element_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
            }

            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_args_element_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            frame_528e69644582a95e5c6c292b45da64dd_2->m_frame.f_lineno = 115;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            condexpr_end_3:;
            tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain_builtins_module, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 115;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$$$function_3_reset_recursion_limitations(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain_reset_recursion_limitations, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$$$function_4_get_sys_path(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain_get_sys_path, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$$$function_5_eval_element(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain_eval_element, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 128;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$$$function_6__eval_element_if_evaluated(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain__eval_element_if_evaluated, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_called_name_10;
            PyObject *tmp_called_name_11;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_element_name_8;
            tmp_called_name_11 = PyObject_GetItem( locals_jedi$evaluate_88, const_str_plain_evaluator_function_cache );

            if ( tmp_called_name_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_evaluator_function_cache );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_evaluator_function_cache );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "evaluator_function_cache" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 214;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_called_name_11 = tmp_mvar_value_5;
                Py_INCREF( tmp_called_name_11 );
                }
            }

            tmp_dict_key_1 = const_str_plain_default;
            tmp_dict_value_1 = PyObject_GetItem( locals_jedi$evaluate_88, const_str_plain_NO_CONTEXTS );

            if ( tmp_dict_value_1 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_NO_CONTEXTS );
                }

                if ( tmp_mvar_value_6 == NULL )
                {
                    Py_DECREF( tmp_called_name_11 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "NO_CONTEXTS" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 214;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_dict_value_1 = tmp_mvar_value_6;
                Py_INCREF( tmp_dict_value_1 );
                }
            }

            tmp_kw_name_2 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_528e69644582a95e5c6c292b45da64dd_2->m_frame.f_lineno = 214;
            tmp_called_name_10 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_11, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_called_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_8 = MAKE_FUNCTION_jedi$evaluate$$$function_7__eval_element_cached(  );



            frame_528e69644582a95e5c6c292b45da64dd_2->m_frame.f_lineno = 214;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
            }

            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain__eval_element_cached, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$$$function_8_goto_definitions(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain_goto_definitions, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 218;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$$$function_9_goto(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain_goto, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 242;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            PyObject *tmp_defaults_2;
            tmp_defaults_2 = const_tuple_false_false_tuple;
            Py_INCREF( tmp_defaults_2 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$$$function_10_create_context( tmp_defaults_2 );



            tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain_create_context, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 318;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_defaults_3;
            tmp_defaults_3 = const_tuple_none_none_str_digest_c075052d723d6707083e869a0e3659bb_tuple;
            Py_INCREF( tmp_defaults_3 );
            tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$$$function_11_parse_and_get_code( tmp_defaults_3 );



            tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain_parse_and_get_code, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 381;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$$$function_12_parse(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain_parse, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 390;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_528e69644582a95e5c6c292b45da64dd_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_528e69644582a95e5c6c292b45da64dd_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_528e69644582a95e5c6c292b45da64dd_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_528e69644582a95e5c6c292b45da64dd_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_528e69644582a95e5c6c292b45da64dd_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_528e69644582a95e5c6c292b45da64dd_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_528e69644582a95e5c6c292b45da64dd_2 == cache_frame_528e69644582a95e5c6c292b45da64dd_2 )
        {
            Py_DECREF( frame_528e69644582a95e5c6c292b45da64dd_2 );
        }
        cache_frame_528e69644582a95e5c6c292b45da64dd_2 = NULL;

        assertFrameObject( frame_528e69644582a95e5c6c292b45da64dd_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_7;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;

                goto try_except_handler_7;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_jedi$evaluate_88, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;

                goto try_except_handler_7;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_42;
            PyObject *tmp_called_name_12;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_12 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_Evaluator;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_jedi$evaluate_88;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_1__class_decl_dict;
            frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame.f_lineno = 88;
            tmp_assign_source_42 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_2, tmp_kw_name_3 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_42 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 88;

                goto try_except_handler_7;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_42;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_41 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_41 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        Py_DECREF( locals_jedi$evaluate_88 );
        locals_jedi$evaluate_88 = NULL;
        goto try_return_handler_6;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jedi$evaluate_88 );
        locals_jedi$evaluate_88 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_6;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 88;
        goto try_except_handler_5;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate, (Nuitka_StringObject *)const_str_plain_Evaluator, tmp_assign_source_41 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e0a1c8056d1a3ed2db3342956d8927fb );
#endif
    popFrameStack();

    assertFrameObject( frame_e0a1c8056d1a3ed2db3342956d8927fb );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e0a1c8056d1a3ed2db3342956d8927fb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e0a1c8056d1a3ed2db3342956d8927fb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e0a1c8056d1a3ed2db3342956d8927fb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e0a1c8056d1a3ed2db3342956d8927fb, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;


    return MOD_RETURN_VALUE( module_jedi$evaluate );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
