/* Generated code for Python module 'jedi.evaluate.param'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_jedi$evaluate$param" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_jedi$evaluate$param;
PyDictObject *moduledict_jedi$evaluate$param;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_69f03acf45a5dfb1e854cc8195a90a7f;
static PyObject *const_str_plain__add_argument_issue;
extern PyObject *const_str_plain_docstrings;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_analysis;
extern PyObject *const_str_plain___name__;
extern PyObject *const_tuple_str_plain_iterable_tuple;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_str_plain_PushBackIterator;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_str_plain_infer;
extern PyObject *const_str_plain_iterable;
static PyObject *const_str_digest_8a7edfc928fd9c2fdc576a6d3471afeb;
extern PyObject *const_str_plain___file__;
static PyObject *const_tuple_f49ea026f8417e27b30381165f47c8f6_tuple;
extern PyObject *const_str_plain_object;
static PyObject *const_str_digest_ef3dcd37150646a8d7121feec734f4eb;
extern PyObject *const_str_plain_tree_node;
extern PyObject *const_str_plain_execution_context;
extern PyObject *const_str_plain_unpack;
extern PyObject *const_str_plain_default;
static PyObject *const_str_digest_252003da09b0ede0d6f744d3f87b2317;
static PyObject *const_tuple_895188e0cb915515e42f718053e6a15d_tuple;
extern PyObject *const_int_neg_1;
extern PyObject *const_str_plain_items;
static PyObject *const_str_digest_08a0f2bcc635be90bc55e171853bfcb4;
static PyObject *const_str_plain__create_default_param;
static PyObject *const_str_digest_4a42bcd377c5a3697d3b045032591a82;
extern PyObject *const_str_plain_param;
extern PyObject *const_str_plain_defaultdict;
extern PyObject *const_str_plain_push_back;
extern PyObject *const_str_digest_4b6ae0a4a7c5ef73221de5bb2b8ff0e4;
static PyObject *const_str_digest_cb3b60b7344ba4c88cec50f7ecb6f6e3;
extern PyObject *const_str_plain_None;
static PyObject *const_tuple_a9e8fa7dc2dad952de019a0f79a561ff_tuple;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_p_tuple;
static PyObject *const_tuple_b70a0dc794b6dbfcf300f592f19a04a8_tuple;
extern PyObject *const_tuple_str_plain_pep0484_tuple;
extern PyObject *const_str_plain___doc__;
static PyObject *const_str_digest_2a0cb3d99fb95fe90ffb825515898f23;
extern PyObject *const_str_digest_f088166ebe9aa8754d7a4327a52e54aa;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_data;
extern PyObject *const_str_plain_add;
static PyObject *const_str_plain__error_argument_count;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_node;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_str_plain_LazyUnknownContext;
extern PyObject *const_str_plain_string_name;
extern PyObject *const_str_plain_p;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_FakeSequence;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_collections;
extern PyObject *const_str_plain_parent;
extern PyObject *const_tuple_str_plain_analysis_tuple;
extern PyObject *const_str_plain_ExecutedParam;
extern PyObject *const_str_plain_message;
extern PyObject *const_str_plain_var_args;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_get_calling_nodes;
extern PyObject *const_str_plain_append;
extern PyObject *const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
static PyObject *const_tuple_str_plain_self_str_plain_pep0484_hints_str_plain_doc_params_tuple;
extern PyObject *const_str_plain_name;
static PyObject *const_str_digest_f9c119ad8dadc6a9942c560c6f8f7351;
extern PyObject *const_str_plain_infer_param;
static PyObject *const_str_digest_98012501ffa0c54f4941a1f51b78b07e;
static PyObject *const_tuple_str_plain_p_str_plain_execution_context_tuple;
extern PyObject *const_str_plain_funcdef;
static PyObject *const_tuple_str_plain_PushBackIterator_tuple;
static PyObject *const_str_plain_actual_count;
extern PyObject *const_str_plain___getitem__;
extern PyObject *const_str_digest_b49b5885202e0ab6a26adeea68b85343;
extern PyObject *const_str_digest_46f532b0e491ef8ae810afebc89c9807;
extern PyObject *const_int_0;
static PyObject *const_str_plain__execution_context;
extern PyObject *const_str_digest_c8310735198f243c63e074ef2322d2ed;
extern PyObject *const_str_digest_f1edf9952adf39791783535a6530e3a6;
extern PyObject *const_str_plain_origin;
static PyObject *const_str_digest_22c8a36ad67f85fabcaa348b123e8ed7;
extern PyObject *const_str_plain_get_executed_params;
static PyObject *const_str_plain_default_arguments;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_angle_listcomp;
extern PyObject *const_str_plain_lazy_context;
extern PyObject *const_str_digest_9762bd19b18a11af1c0649d1b042a42f;
static PyObject *const_str_plain__param_node;
static PyObject *const_tuple_str_plain_execution_context_str_plain_funcdef_tuple;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_str_plain_type;
static PyObject *const_str_digest_d0546682c6fc69fd38514cddf9143ae2;
extern PyObject *const_str_plain_error_name;
static PyObject *const_str_plain_result_arg;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_da514e395925ccdc8d9efb373cdf2394;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_LazyTreeContext;
extern PyObject *const_str_plain_star_count;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_digest_785ebe15053a4fa731c98e296de8edd7;
extern PyObject *const_str_plain___module__;
static PyObject *const_str_plain_pep0484_hints;
extern PyObject *const_str_plain_params;
extern PyObject *const_str_plain_LazyKnownContext;
static PyObject *const_str_digest_7170865724764010f0a458fa8f3199ee;
extern PyObject *const_int_pos_1;
static PyObject *const_tuple_c7b8012e9d1689178b1383b84a4ad08a_tuple;
static PyObject *const_str_plain_doc_params;
extern PyObject *const_str_plain_tuple;
extern PyObject *const_str_plain_before;
extern PyObject *const_tuple_str_plain_defaultdict_tuple;
extern PyObject *const_str_plain___prepare__;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_get_params;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_argument;
extern PyObject *const_str_digest_0c6a1637c5fdafe468f822c51ad84a09;
static PyObject *const_tuple_c3e444965d716684477fb53efde962b1_tuple;
extern PyObject *const_str_plain___repr__;
extern PyObject *const_str_plain_create_default_params;
extern PyObject *const_str_plain_pep0484;
extern PyObject *const_str_plain_evaluator;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_property;
extern PyObject *const_str_plain_parent_context;
extern PyObject *const_str_plain_FakeDict;
extern PyObject *const_tuple_none_none_tuple;
extern PyObject *const_tuple_str_plain_docstrings_tuple;
static PyObject *const_str_plain__lazy_context;
extern PyObject *const_str_plain_param_node;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_69f03acf45a5dfb1e854cc8195a90a7f = UNSTREAM_STRING_ASCII( &constant_bin[ 985453 ], 22, 0 );
    const_str_plain__add_argument_issue = UNSTREAM_STRING_ASCII( &constant_bin[ 1011990 ], 19, 1 );
    const_str_digest_8a7edfc928fd9c2fdc576a6d3471afeb = UNSTREAM_STRING_ASCII( &constant_bin[ 1012009 ], 11, 0 );
    const_tuple_f49ea026f8417e27b30381165f47c8f6_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 1012020 ], 313 );
    const_str_digest_ef3dcd37150646a8d7121feec734f4eb = UNSTREAM_STRING_ASCII( &constant_bin[ 1012333 ], 22, 0 );
    const_str_digest_252003da09b0ede0d6f744d3f87b2317 = UNSTREAM_STRING_ASCII( &constant_bin[ 1012355 ], 32, 0 );
    const_tuple_895188e0cb915515e42f718053e6a15d_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_895188e0cb915515e42f718053e6a15d_tuple, 0, const_str_plain_execution_context ); Py_INCREF( const_str_plain_execution_context );
    PyTuple_SET_ITEM( const_tuple_895188e0cb915515e42f718053e6a15d_tuple, 1, const_str_plain_param ); Py_INCREF( const_str_plain_param );
    const_str_plain_result_arg = UNSTREAM_STRING_ASCII( &constant_bin[ 1012269 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_895188e0cb915515e42f718053e6a15d_tuple, 2, const_str_plain_result_arg ); Py_INCREF( const_str_plain_result_arg );
    const_str_digest_08a0f2bcc635be90bc55e171853bfcb4 = UNSTREAM_STRING_ASCII( &constant_bin[ 1012387 ], 22, 0 );
    const_str_plain__create_default_param = UNSTREAM_STRING_ASCII( &constant_bin[ 1012409 ], 21, 1 );
    const_str_digest_4a42bcd377c5a3697d3b045032591a82 = UNSTREAM_STRING_ASCII( &constant_bin[ 984613 ], 19, 0 );
    const_str_digest_cb3b60b7344ba4c88cec50f7ecb6f6e3 = UNSTREAM_STRING_ASCII( &constant_bin[ 1012430 ], 56, 0 );
    const_tuple_a9e8fa7dc2dad952de019a0f79a561ff_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_a9e8fa7dc2dad952de019a0f79a561ff_tuple, 0, const_str_plain_LazyKnownContext ); Py_INCREF( const_str_plain_LazyKnownContext );
    PyTuple_SET_ITEM( const_tuple_a9e8fa7dc2dad952de019a0f79a561ff_tuple, 1, const_str_plain_LazyTreeContext ); Py_INCREF( const_str_plain_LazyTreeContext );
    PyTuple_SET_ITEM( const_tuple_a9e8fa7dc2dad952de019a0f79a561ff_tuple, 2, const_str_plain_LazyUnknownContext ); Py_INCREF( const_str_plain_LazyUnknownContext );
    const_tuple_b70a0dc794b6dbfcf300f592f19a04a8_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_b70a0dc794b6dbfcf300f592f19a04a8_tuple, 0, const_str_plain_parent_context ); Py_INCREF( const_str_plain_parent_context );
    PyTuple_SET_ITEM( const_tuple_b70a0dc794b6dbfcf300f592f19a04a8_tuple, 1, const_str_plain_error_name ); Py_INCREF( const_str_plain_error_name );
    PyTuple_SET_ITEM( const_tuple_b70a0dc794b6dbfcf300f592f19a04a8_tuple, 2, const_str_plain_lazy_context ); Py_INCREF( const_str_plain_lazy_context );
    PyTuple_SET_ITEM( const_tuple_b70a0dc794b6dbfcf300f592f19a04a8_tuple, 3, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_b70a0dc794b6dbfcf300f592f19a04a8_tuple, 4, const_str_plain_node ); Py_INCREF( const_str_plain_node );
    const_str_digest_2a0cb3d99fb95fe90ffb825515898f23 = UNSTREAM_STRING_ASCII( &constant_bin[ 1012486 ], 37, 0 );
    const_str_plain__error_argument_count = UNSTREAM_STRING_ASCII( &constant_bin[ 1012523 ], 21, 1 );
    const_tuple_str_plain_self_str_plain_pep0484_hints_str_plain_doc_params_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_pep0484_hints_str_plain_doc_params_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    const_str_plain_pep0484_hints = UNSTREAM_STRING_ASCII( &constant_bin[ 1012544 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_pep0484_hints_str_plain_doc_params_tuple, 1, const_str_plain_pep0484_hints ); Py_INCREF( const_str_plain_pep0484_hints );
    const_str_plain_doc_params = UNSTREAM_STRING_ASCII( &constant_bin[ 1012557 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_pep0484_hints_str_plain_doc_params_tuple, 2, const_str_plain_doc_params ); Py_INCREF( const_str_plain_doc_params );
    const_str_digest_f9c119ad8dadc6a9942c560c6f8f7351 = UNSTREAM_STRING_ASCII( &constant_bin[ 1012567 ], 62, 0 );
    const_str_digest_98012501ffa0c54f4941a1f51b78b07e = UNSTREAM_STRING_ASCII( &constant_bin[ 1012629 ], 48, 0 );
    const_tuple_str_plain_p_str_plain_execution_context_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_p_str_plain_execution_context_tuple, 0, const_str_plain_p ); Py_INCREF( const_str_plain_p );
    PyTuple_SET_ITEM( const_tuple_str_plain_p_str_plain_execution_context_tuple, 1, const_str_plain_execution_context ); Py_INCREF( const_str_plain_execution_context );
    const_tuple_str_plain_PushBackIterator_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PushBackIterator_tuple, 0, const_str_plain_PushBackIterator ); Py_INCREF( const_str_plain_PushBackIterator );
    const_str_plain_actual_count = UNSTREAM_STRING_ASCII( &constant_bin[ 1012677 ], 12, 1 );
    const_str_plain__execution_context = UNSTREAM_STRING_ASCII( &constant_bin[ 987356 ], 18, 1 );
    const_str_digest_22c8a36ad67f85fabcaa348b123e8ed7 = UNSTREAM_STRING_ASCII( &constant_bin[ 1012689 ], 22, 0 );
    const_str_plain_default_arguments = UNSTREAM_STRING_ASCII( &constant_bin[ 1012711 ], 17, 1 );
    const_str_plain__param_node = UNSTREAM_STRING_ASCII( &constant_bin[ 1012728 ], 11, 1 );
    const_tuple_str_plain_execution_context_str_plain_funcdef_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_execution_context_str_plain_funcdef_tuple, 0, const_str_plain_execution_context ); Py_INCREF( const_str_plain_execution_context );
    PyTuple_SET_ITEM( const_tuple_str_plain_execution_context_str_plain_funcdef_tuple, 1, const_str_plain_funcdef ); Py_INCREF( const_str_plain_funcdef );
    const_str_digest_d0546682c6fc69fd38514cddf9143ae2 = UNSTREAM_STRING_ASCII( &constant_bin[ 1012739 ], 40, 0 );
    const_str_digest_da514e395925ccdc8d9efb373cdf2394 = UNSTREAM_STRING_ASCII( &constant_bin[ 106482 ], 8, 0 );
    const_str_digest_7170865724764010f0a458fa8f3199ee = UNSTREAM_STRING_ASCII( &constant_bin[ 1012779 ], 28, 0 );
    const_tuple_c7b8012e9d1689178b1383b84a4ad08a_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_c7b8012e9d1689178b1383b84a4ad08a_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_c7b8012e9d1689178b1383b84a4ad08a_tuple, 1, const_str_plain_execution_context ); Py_INCREF( const_str_plain_execution_context );
    PyTuple_SET_ITEM( const_tuple_c7b8012e9d1689178b1383b84a4ad08a_tuple, 2, const_str_plain_param_node ); Py_INCREF( const_str_plain_param_node );
    PyTuple_SET_ITEM( const_tuple_c7b8012e9d1689178b1383b84a4ad08a_tuple, 3, const_str_plain_lazy_context ); Py_INCREF( const_str_plain_lazy_context );
    const_tuple_c3e444965d716684477fb53efde962b1_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_c3e444965d716684477fb53efde962b1_tuple, 0, const_str_plain_funcdef ); Py_INCREF( const_str_plain_funcdef );
    PyTuple_SET_ITEM( const_tuple_c3e444965d716684477fb53efde962b1_tuple, 1, const_str_plain_actual_count ); Py_INCREF( const_str_plain_actual_count );
    PyTuple_SET_ITEM( const_tuple_c3e444965d716684477fb53efde962b1_tuple, 2, const_str_plain_params ); Py_INCREF( const_str_plain_params );
    PyTuple_SET_ITEM( const_tuple_c3e444965d716684477fb53efde962b1_tuple, 3, const_str_plain_default_arguments ); Py_INCREF( const_str_plain_default_arguments );
    PyTuple_SET_ITEM( const_tuple_c3e444965d716684477fb53efde962b1_tuple, 4, const_str_plain_before ); Py_INCREF( const_str_plain_before );
    const_str_plain__lazy_context = UNSTREAM_STRING_ASCII( &constant_bin[ 983079 ], 13, 1 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_jedi$evaluate$param( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_1886a4bdb2d0224302a6673c87c076b1;
static PyCodeObject *codeobj_e229169c9215cd14be01aa110dcc585a;
static PyCodeObject *codeobj_2a34d259845e388eab25e74ab42d09b0;
static PyCodeObject *codeobj_218ccbfd5cd7a66e68cab969a71adaff;
static PyCodeObject *codeobj_67012a2ac90fee717ae3700e2c510f1a;
static PyCodeObject *codeobj_2870d48c4315235ce0207c76dc33fe48;
static PyCodeObject *codeobj_3cbb6e578499c8962954ee66ebe62957;
static PyCodeObject *codeobj_cc969c9ab99669eea55a3ca9936be151;
static PyCodeObject *codeobj_105bad8ab37151945c4a28a9a55508c2;
static PyCodeObject *codeobj_34c427d9c4bb8dbbc2ed7daafa7fa16a;
static PyCodeObject *codeobj_27cbe2b1328e9129a03855b4e485f0bd;
static PyCodeObject *codeobj_c9150d2e8299e357a752bb5c8ba8c265;
static PyCodeObject *codeobj_e5b113fb49adc89c901900020b1dba36;
static PyCodeObject *codeobj_b2120c76cc089754da499cac66a4bbde;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_08a0f2bcc635be90bc55e171853bfcb4 );
    codeobj_1886a4bdb2d0224302a6673c87c076b1 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 166, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_p_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e229169c9215cd14be01aa110dcc585a = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 55, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2a34d259845e388eab25e74ab42d09b0 = MAKE_CODEOBJ( module_filename_obj, const_str_angle_listcomp, 193, const_tuple_str_plain_p_str_plain_execution_context_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_218ccbfd5cd7a66e68cab969a71adaff = MAKE_CODEOBJ( module_filename_obj, const_str_digest_7170865724764010f0a458fa8f3199ee, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_67012a2ac90fee717ae3700e2c510f1a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_ExecutedParam, 20, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_2870d48c4315235ce0207c76dc33fe48 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 22, const_tuple_c7b8012e9d1689178b1383b84a4ad08a_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3cbb6e578499c8962954ee66ebe62957 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 40, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cc969c9ab99669eea55a3ca9936be151 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__add_argument_issue, 12, const_tuple_b70a0dc794b6dbfcf300f592f19a04a8_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_105bad8ab37151945c4a28a9a55508c2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__create_default_param, 176, const_tuple_895188e0cb915515e42f718053e6a15d_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_34c427d9c4bb8dbbc2ed7daafa7fa16a = MAKE_CODEOBJ( module_filename_obj, const_str_plain__error_argument_count, 164, const_tuple_c3e444965d716684477fb53efde962b1_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_27cbe2b1328e9129a03855b4e485f0bd = MAKE_CODEOBJ( module_filename_obj, const_str_plain_create_default_params, 192, const_tuple_str_plain_execution_context_str_plain_funcdef_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c9150d2e8299e357a752bb5c8ba8c265 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_executed_params, 44, const_tuple_f49ea026f8417e27b30381165f47c8f6_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e5b113fb49adc89c901900020b1dba36 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_infer, 28, const_tuple_str_plain_self_str_plain_pep0484_hints_str_plain_doc_params_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b2120c76cc089754da499cac66a4bbde = MAKE_CODEOBJ( module_filename_obj, const_str_plain_var_args, 36, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *jedi$evaluate$param$$$function_7__error_argument_count$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_1__add_argument_issue(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_2___init__(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_3_infer(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_4_var_args(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_5___repr__(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_6_get_executed_params(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_6_get_executed_params$$$function_1_lambda(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_7__error_argument_count(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_8__create_default_param(  );


static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_9_create_default_params(  );


// The module function definitions.
static PyObject *impl_jedi$evaluate$param$$$function_1__add_argument_issue( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_parent_context = python_pars[ 0 ];
    PyObject *par_error_name = python_pars[ 1 ];
    PyObject *par_lazy_context = python_pars[ 2 ];
    PyObject *par_message = python_pars[ 3 ];
    PyObject *var_node = NULL;
    struct Nuitka_FrameObject *frame_cc969c9ab99669eea55a3ca9936be151;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_cc969c9ab99669eea55a3ca9936be151 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cc969c9ab99669eea55a3ca9936be151, codeobj_cc969c9ab99669eea55a3ca9936be151, module_jedi$evaluate$param, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_cc969c9ab99669eea55a3ca9936be151 = cache_frame_cc969c9ab99669eea55a3ca9936be151;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cc969c9ab99669eea55a3ca9936be151 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cc969c9ab99669eea55a3ca9936be151 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_lazy_context );
        tmp_isinstance_inst_1 = par_lazy_context;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyTreeContext" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 13;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_source_name_1;
            CHECK_OBJECT( par_lazy_context );
            tmp_source_name_1 = par_lazy_context;
            tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_data );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 14;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_node == NULL );
            var_node = tmp_assign_source_1;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( var_node );
            tmp_source_name_3 = var_node;
            tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_parent );
            if ( tmp_source_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 15;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_type );
            Py_DECREF( tmp_source_name_2 );
            if ( tmp_compexpr_left_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 15;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_1 = const_str_plain_argument;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            Py_DECREF( tmp_compexpr_left_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 15;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_source_name_4;
                CHECK_OBJECT( var_node );
                tmp_source_name_4 = var_node;
                tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_parent );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 16;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_node;
                    assert( old != NULL );
                    var_node = tmp_assign_source_2;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_analysis );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_analysis );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "analysis" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 17;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_2;
            CHECK_OBJECT( par_parent_context );
            tmp_args_element_name_1 = par_parent_context;
            CHECK_OBJECT( par_error_name );
            tmp_args_element_name_2 = par_error_name;
            CHECK_OBJECT( var_node );
            tmp_args_element_name_3 = var_node;
            CHECK_OBJECT( par_message );
            tmp_args_element_name_4 = par_message;
            frame_cc969c9ab99669eea55a3ca9936be151->m_frame.f_lineno = 17;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS4( tmp_called_instance_1, const_str_plain_add, call_args );
            }

            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 17;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cc969c9ab99669eea55a3ca9936be151 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cc969c9ab99669eea55a3ca9936be151 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cc969c9ab99669eea55a3ca9936be151, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cc969c9ab99669eea55a3ca9936be151->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cc969c9ab99669eea55a3ca9936be151, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cc969c9ab99669eea55a3ca9936be151,
        type_description_1,
        par_parent_context,
        par_error_name,
        par_lazy_context,
        par_message,
        var_node
    );


    // Release cached frame.
    if ( frame_cc969c9ab99669eea55a3ca9936be151 == cache_frame_cc969c9ab99669eea55a3ca9936be151 )
    {
        Py_DECREF( frame_cc969c9ab99669eea55a3ca9936be151 );
    }
    cache_frame_cc969c9ab99669eea55a3ca9936be151 = NULL;

    assertFrameObject( frame_cc969c9ab99669eea55a3ca9936be151 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_1__add_argument_issue );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_parent_context );
    Py_DECREF( par_parent_context );
    par_parent_context = NULL;

    CHECK_OBJECT( (PyObject *)par_error_name );
    Py_DECREF( par_error_name );
    par_error_name = NULL;

    CHECK_OBJECT( (PyObject *)par_lazy_context );
    Py_DECREF( par_lazy_context );
    par_lazy_context = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    Py_XDECREF( var_node );
    var_node = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_parent_context );
    Py_DECREF( par_parent_context );
    par_parent_context = NULL;

    CHECK_OBJECT( (PyObject *)par_error_name );
    Py_DECREF( par_error_name );
    par_error_name = NULL;

    CHECK_OBJECT( (PyObject *)par_lazy_context );
    Py_DECREF( par_lazy_context );
    par_lazy_context = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    Py_XDECREF( var_node );
    var_node = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_1__add_argument_issue );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$param$$$function_2___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_execution_context = python_pars[ 1 ];
    PyObject *par_param_node = python_pars[ 2 ];
    PyObject *par_lazy_context = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_2870d48c4315235ce0207c76dc33fe48;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2870d48c4315235ce0207c76dc33fe48 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2870d48c4315235ce0207c76dc33fe48, codeobj_2870d48c4315235ce0207c76dc33fe48, module_jedi$evaluate$param, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2870d48c4315235ce0207c76dc33fe48 = cache_frame_2870d48c4315235ce0207c76dc33fe48;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2870d48c4315235ce0207c76dc33fe48 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2870d48c4315235ce0207c76dc33fe48 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_execution_context );
        tmp_assattr_name_1 = par_execution_context;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__execution_context, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 23;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_param_node );
        tmp_assattr_name_2 = par_param_node;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__param_node, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_lazy_context );
        tmp_assattr_name_3 = par_lazy_context;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__lazy_context, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_assattr_target_4;
        CHECK_OBJECT( par_param_node );
        tmp_source_name_2 = par_param_node;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_name );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assattr_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_value );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assattr_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_string_name, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2870d48c4315235ce0207c76dc33fe48 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2870d48c4315235ce0207c76dc33fe48 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2870d48c4315235ce0207c76dc33fe48, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2870d48c4315235ce0207c76dc33fe48->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2870d48c4315235ce0207c76dc33fe48, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2870d48c4315235ce0207c76dc33fe48,
        type_description_1,
        par_self,
        par_execution_context,
        par_param_node,
        par_lazy_context
    );


    // Release cached frame.
    if ( frame_2870d48c4315235ce0207c76dc33fe48 == cache_frame_2870d48c4315235ce0207c76dc33fe48 )
    {
        Py_DECREF( frame_2870d48c4315235ce0207c76dc33fe48 );
    }
    cache_frame_2870d48c4315235ce0207c76dc33fe48 = NULL;

    assertFrameObject( frame_2870d48c4315235ce0207c76dc33fe48 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_2___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    CHECK_OBJECT( (PyObject *)par_param_node );
    Py_DECREF( par_param_node );
    par_param_node = NULL;

    CHECK_OBJECT( (PyObject *)par_lazy_context );
    Py_DECREF( par_lazy_context );
    par_lazy_context = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    CHECK_OBJECT( (PyObject *)par_param_node );
    Py_DECREF( par_param_node );
    par_param_node = NULL;

    CHECK_OBJECT( (PyObject *)par_lazy_context );
    Py_DECREF( par_lazy_context );
    par_lazy_context = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_2___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$param$$$function_3_infer( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_pep0484_hints = NULL;
    PyObject *var_doc_params = NULL;
    struct Nuitka_FrameObject *frame_e5b113fb49adc89c901900020b1dba36;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_e5b113fb49adc89c901900020b1dba36 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e5b113fb49adc89c901900020b1dba36, codeobj_e5b113fb49adc89c901900020b1dba36, module_jedi$evaluate$param, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e5b113fb49adc89c901900020b1dba36 = cache_frame_e5b113fb49adc89c901900020b1dba36;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e5b113fb49adc89c901900020b1dba36 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e5b113fb49adc89c901900020b1dba36 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_source_name_3;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_pep0484 );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_pep0484 );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "pep0484" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 29;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_infer_param );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__execution_context );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 29;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__param_node );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );

            exception_lineno = 29;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_e5b113fb49adc89c901900020b1dba36->m_frame.f_lineno = 29;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 29;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_pep0484_hints == NULL );
        var_pep0484_hints = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_5;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_source_name_6;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_docstrings );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_docstrings );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "docstrings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 30;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_2;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_infer_param );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__execution_context );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 30;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_6 = par_self;
        tmp_args_element_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain__param_node );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_3 );

            exception_lineno = 30;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_e5b113fb49adc89c901900020b1dba36->m_frame.f_lineno = 30;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_3 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 30;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_doc_params == NULL );
        var_doc_params = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        int tmp_truth_name_1;
        int tmp_truth_name_2;
        CHECK_OBJECT( var_pep0484_hints );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_pep0484_hints );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_or_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( var_doc_params );
        tmp_truth_name_2 = CHECK_IF_TRUE( var_doc_params );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 31;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_or_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_1 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            CHECK_OBJECT( var_pep0484_hints );
            tmp_left_name_1 = var_pep0484_hints;
            CHECK_OBJECT( var_doc_params );
            tmp_right_name_1 = var_doc_params;
            tmp_return_value = BINARY_OPERATION( PyNumber_Or, tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 32;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_7;
        CHECK_OBJECT( par_self );
        tmp_source_name_7 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain__lazy_context );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_e5b113fb49adc89c901900020b1dba36->m_frame.f_lineno = 34;
        tmp_return_value = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_infer );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 34;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e5b113fb49adc89c901900020b1dba36 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e5b113fb49adc89c901900020b1dba36 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e5b113fb49adc89c901900020b1dba36 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e5b113fb49adc89c901900020b1dba36, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e5b113fb49adc89c901900020b1dba36->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e5b113fb49adc89c901900020b1dba36, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e5b113fb49adc89c901900020b1dba36,
        type_description_1,
        par_self,
        var_pep0484_hints,
        var_doc_params
    );


    // Release cached frame.
    if ( frame_e5b113fb49adc89c901900020b1dba36 == cache_frame_e5b113fb49adc89c901900020b1dba36 )
    {
        Py_DECREF( frame_e5b113fb49adc89c901900020b1dba36 );
    }
    cache_frame_e5b113fb49adc89c901900020b1dba36 = NULL;

    assertFrameObject( frame_e5b113fb49adc89c901900020b1dba36 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_3_infer );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_pep0484_hints );
    Py_DECREF( var_pep0484_hints );
    var_pep0484_hints = NULL;

    CHECK_OBJECT( (PyObject *)var_doc_params );
    Py_DECREF( var_doc_params );
    var_doc_params = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_pep0484_hints );
    var_pep0484_hints = NULL;

    Py_XDECREF( var_doc_params );
    var_doc_params = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_3_infer );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$param$$$function_4_var_args( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b2120c76cc089754da499cac66a4bbde;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b2120c76cc089754da499cac66a4bbde = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b2120c76cc089754da499cac66a4bbde, codeobj_b2120c76cc089754da499cac66a4bbde, module_jedi$evaluate$param, sizeof(void *) );
    frame_b2120c76cc089754da499cac66a4bbde = cache_frame_b2120c76cc089754da499cac66a4bbde;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b2120c76cc089754da499cac66a4bbde );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b2120c76cc089754da499cac66a4bbde ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__execution_context );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_var_args );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 38;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b2120c76cc089754da499cac66a4bbde );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b2120c76cc089754da499cac66a4bbde );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b2120c76cc089754da499cac66a4bbde );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b2120c76cc089754da499cac66a4bbde, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b2120c76cc089754da499cac66a4bbde->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b2120c76cc089754da499cac66a4bbde, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b2120c76cc089754da499cac66a4bbde,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_b2120c76cc089754da499cac66a4bbde == cache_frame_b2120c76cc089754da499cac66a4bbde )
    {
        Py_DECREF( frame_b2120c76cc089754da499cac66a4bbde );
    }
    cache_frame_b2120c76cc089754da499cac66a4bbde = NULL;

    assertFrameObject( frame_b2120c76cc089754da499cac66a4bbde );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_4_var_args );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_4_var_args );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$param$$$function_5___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3cbb6e578499c8962954ee66ebe62957;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3cbb6e578499c8962954ee66ebe62957 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3cbb6e578499c8962954ee66ebe62957, codeobj_3cbb6e578499c8962954ee66ebe62957, module_jedi$evaluate$param, sizeof(void *) );
    frame_3cbb6e578499c8962954ee66ebe62957 = cache_frame_3cbb6e578499c8962954ee66ebe62957;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3cbb6e578499c8962954ee66ebe62957 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3cbb6e578499c8962954ee66ebe62957 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        tmp_left_name_1 = const_str_digest_f088166ebe9aa8754d7a4327a52e54aa;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_3 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_string_name );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 41;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3cbb6e578499c8962954ee66ebe62957 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3cbb6e578499c8962954ee66ebe62957 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3cbb6e578499c8962954ee66ebe62957 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3cbb6e578499c8962954ee66ebe62957, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3cbb6e578499c8962954ee66ebe62957->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3cbb6e578499c8962954ee66ebe62957, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3cbb6e578499c8962954ee66ebe62957,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_3cbb6e578499c8962954ee66ebe62957 == cache_frame_3cbb6e578499c8962954ee66ebe62957 )
    {
        Py_DECREF( frame_3cbb6e578499c8962954ee66ebe62957 );
    }
    cache_frame_3cbb6e578499c8962954ee66ebe62957 = NULL;

    assertFrameObject( frame_3cbb6e578499c8962954ee66ebe62957 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_5___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_5___repr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$param$$$function_6_get_executed_params( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_execution_context = python_pars[ 0 ];
    PyObject *par_var_args = python_pars[ 1 ];
    PyObject *var_result_params = NULL;
    PyObject *var_param_dict = NULL;
    PyObject *var_funcdef = NULL;
    PyObject *var_parent_context = NULL;
    PyObject *var_param = NULL;
    PyObject *var_unpacked_va = NULL;
    PyObject *var_var_arg_iterator = NULL;
    PyObject *var_non_matching_keys = NULL;
    PyObject *var_keys_used = NULL;
    PyObject *var_keys_only = NULL;
    PyObject *var_had_multiple_value_error = NULL;
    PyObject *var_key = NULL;
    PyObject *var_argument = NULL;
    PyObject *var_key_param = NULL;
    PyObject *var_m = NULL;
    PyObject *var_node = NULL;
    PyObject *var_lazy_context_list = NULL;
    PyObject *var_seq = NULL;
    PyObject *var_result_arg = NULL;
    PyObject *var_dct = NULL;
    PyObject *var_k = NULL;
    PyObject *var_lazy_context = NULL;
    PyObject *var_remaining_arguments = NULL;
    PyObject *var_first_key = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_for_loop_2__for_iterator = NULL;
    PyObject *tmp_for_loop_2__iter_value = NULL;
    PyObject *tmp_for_loop_3__for_iterator = NULL;
    PyObject *tmp_for_loop_3__iter_value = NULL;
    PyObject *tmp_for_loop_4__for_iterator = NULL;
    PyObject *tmp_for_loop_4__iter_value = NULL;
    PyObject *tmp_for_loop_5__for_iterator = NULL;
    PyObject *tmp_for_loop_5__iter_value = NULL;
    PyObject *tmp_for_loop_6__for_iterator = NULL;
    PyObject *tmp_for_loop_6__iter_value = NULL;
    PyObject *tmp_for_loop_7__for_iterator = NULL;
    PyObject *tmp_for_loop_7__iter_value = NULL;
    PyObject *tmp_for_loop_8__for_iterator = NULL;
    PyObject *tmp_for_loop_8__iter_value = NULL;
    PyObject *tmp_try_except_1__unhandled_indicator = NULL;
    PyObject *tmp_tuple_unpack_1__element_1 = NULL;
    PyObject *tmp_tuple_unpack_1__element_2 = NULL;
    PyObject *tmp_tuple_unpack_1__source_iter = NULL;
    PyObject *tmp_tuple_unpack_2__element_1 = NULL;
    PyObject *tmp_tuple_unpack_2__element_2 = NULL;
    PyObject *tmp_tuple_unpack_2__source_iter = NULL;
    PyObject *tmp_tuple_unpack_3__element_1 = NULL;
    PyObject *tmp_tuple_unpack_3__element_2 = NULL;
    PyObject *tmp_tuple_unpack_3__source_iter = NULL;
    PyObject *tmp_tuple_unpack_4__element_1 = NULL;
    PyObject *tmp_tuple_unpack_4__element_2 = NULL;
    PyObject *tmp_tuple_unpack_4__source_iter = NULL;
    PyObject *tmp_tuple_unpack_5__element_1 = NULL;
    PyObject *tmp_tuple_unpack_5__element_2 = NULL;
    PyObject *tmp_tuple_unpack_5__source_iter = NULL;
    struct Nuitka_FrameObject *frame_c9150d2e8299e357a752bb5c8ba8c265;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_dictset_value;
    PyObject *tmp_dictset_dict;
    PyObject *tmp_dictset_key;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *tmp_iterator_attempt;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *exception_preserved_type_2;
    PyObject *exception_preserved_value_2;
    PyTracebackObject *exception_preserved_tb_2;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;
    PyObject *exception_keeper_type_22;
    PyObject *exception_keeper_value_22;
    PyTracebackObject *exception_keeper_tb_22;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_22;
    PyObject *exception_keeper_type_23;
    PyObject *exception_keeper_value_23;
    PyTracebackObject *exception_keeper_tb_23;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_23;
    static struct Nuitka_FrameObject *cache_frame_c9150d2e8299e357a752bb5c8ba8c265 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_24;
    PyObject *exception_keeper_value_24;
    PyTracebackObject *exception_keeper_tb_24;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_24;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = PyList_New( 0 );
        assert( var_result_params == NULL );
        var_result_params = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyDict_New();
        assert( var_param_dict == NULL );
        var_param_dict = tmp_assign_source_2;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c9150d2e8299e357a752bb5c8ba8c265, codeobj_c9150d2e8299e357a752bb5c8ba8c265, module_jedi$evaluate$param, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c9150d2e8299e357a752bb5c8ba8c265 = cache_frame_c9150d2e8299e357a752bb5c8ba8c265;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c9150d2e8299e357a752bb5c8ba8c265 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c9150d2e8299e357a752bb5c8ba8c265 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_execution_context );
        tmp_source_name_1 = par_execution_context;
        tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_tree_node );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_funcdef == NULL );
        var_funcdef = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_execution_context );
        tmp_source_name_2 = par_execution_context;
        tmp_assign_source_4 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_parent_context );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 48;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_parent_context == NULL );
        var_parent_context = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_funcdef );
        tmp_called_instance_1 = var_funcdef;
        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 50;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_params );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 50;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_5;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_6;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_6 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooooooooooooooooo";
                exception_lineno = 50;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_7 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_param;
            var_param = tmp_assign_source_7;
            Py_INCREF( var_param );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( var_param );
        tmp_dictset_value = var_param;
        CHECK_OBJECT( var_param_dict );
        tmp_dictset_dict = var_param_dict;
        CHECK_OBJECT( var_param );
        tmp_source_name_4 = var_param;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_name );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_dictset_key = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_value );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_dictset_key == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
        tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
        Py_DECREF( tmp_dictset_key );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_2;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 50;
        type_description_1 = "oooooooooooooooooooooooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_list_arg_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_var_args );
        tmp_called_instance_2 = par_var_args;
        CHECK_OBJECT( var_funcdef );
        tmp_args_element_name_1 = var_funcdef;
        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 52;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_list_arg_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_unpack, call_args );
        }

        if ( tmp_list_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_8 = PySequence_List( tmp_list_arg_1 );
        Py_DECREF( tmp_list_arg_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_unpacked_va == NULL );
        var_unpacked_va = tmp_assign_source_8;
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_iter_arg_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_PushBackIterator );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PushBackIterator );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PushBackIterator" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 53;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( var_unpacked_va );
        tmp_iter_arg_2 = var_unpacked_va;
        tmp_args_element_name_2 = MAKE_ITERATOR( tmp_iter_arg_2 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 53;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_var_arg_iterator == NULL );
        var_var_arg_iterator = tmp_assign_source_9;
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_defaultdict );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_defaultdict );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "defaultdict" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 55;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        tmp_args_element_name_3 = MAKE_FUNCTION_jedi$evaluate$param$$$function_6_get_executed_params$$$function_1_lambda(  );



        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 55;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 55;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_non_matching_keys == NULL );
        var_non_matching_keys = tmp_assign_source_10;
    }
    {
        PyObject *tmp_assign_source_11;
        tmp_assign_source_11 = PyDict_New();
        assert( var_keys_used == NULL );
        var_keys_used = tmp_assign_source_11;
    }
    {
        PyObject *tmp_assign_source_12;
        tmp_assign_source_12 = Py_False;
        assert( var_keys_only == NULL );
        Py_INCREF( tmp_assign_source_12 );
        var_keys_only = tmp_assign_source_12;
    }
    {
        PyObject *tmp_assign_source_13;
        tmp_assign_source_13 = Py_False;
        assert( var_had_multiple_value_error == NULL );
        Py_INCREF( tmp_assign_source_13 );
        var_had_multiple_value_error = tmp_assign_source_13;
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_iter_arg_3;
        PyObject *tmp_called_instance_3;
        CHECK_OBJECT( var_funcdef );
        tmp_called_instance_3 = var_funcdef;
        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 59;
        tmp_iter_arg_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_params );
        if ( tmp_iter_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_14 = MAKE_ITERATOR( tmp_iter_arg_3 );
        Py_DECREF( tmp_iter_arg_3 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_2__for_iterator == NULL );
        tmp_for_loop_2__for_iterator = tmp_assign_source_14;
    }
    // Tried code:
    loop_start_2:;
    {
        PyObject *tmp_next_source_2;
        PyObject *tmp_assign_source_15;
        CHECK_OBJECT( tmp_for_loop_2__for_iterator );
        tmp_next_source_2 = tmp_for_loop_2__for_iterator;
        tmp_assign_source_15 = ITERATOR_NEXT( tmp_next_source_2 );
        if ( tmp_assign_source_15 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_2;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooooooooooooooooo";
                exception_lineno = 59;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_2__iter_value;
            tmp_for_loop_2__iter_value = tmp_assign_source_15;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_16;
        CHECK_OBJECT( tmp_for_loop_2__iter_value );
        tmp_assign_source_16 = tmp_for_loop_2__iter_value;
        {
            PyObject *old = var_param;
            var_param = tmp_assign_source_16;
            Py_INCREF( var_param );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_iter_arg_4;
        PyObject *tmp_next_arg_1;
        PyObject *tmp_next_default_1;
        CHECK_OBJECT( var_var_arg_iterator );
        tmp_next_arg_1 = var_var_arg_iterator;
        tmp_next_default_1 = const_tuple_none_none_tuple;
        tmp_iter_arg_4 = BUILTIN_NEXT2( tmp_next_arg_1, tmp_next_default_1 );
        if ( tmp_iter_arg_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_4;
        }
        tmp_assign_source_17 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_4 );
        Py_DECREF( tmp_iter_arg_4 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 64;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_4;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__source_iter;
            tmp_tuple_unpack_1__source_iter = tmp_assign_source_17;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_unpack_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_1 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_18 = UNPACK_NEXT( tmp_unpack_1, 0, 2 );
        if ( tmp_assign_source_18 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooo";
            exception_lineno = 64;
            goto try_except_handler_5;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_1;
            tmp_tuple_unpack_1__element_1 = tmp_assign_source_18;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_unpack_2;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_unpack_2 = tmp_tuple_unpack_1__source_iter;
        tmp_assign_source_19 = UNPACK_NEXT( tmp_unpack_2, 1, 2 );
        if ( tmp_assign_source_19 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooo";
            exception_lineno = 64;
            goto try_except_handler_5;
        }
        {
            PyObject *old = tmp_tuple_unpack_1__element_2;
            tmp_tuple_unpack_1__element_2 = tmp_assign_source_19;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_1;
        CHECK_OBJECT( tmp_tuple_unpack_1__source_iter );
        tmp_iterator_name_1 = tmp_tuple_unpack_1__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_1 ); assert( HAS_ITERNEXT( tmp_iterator_name_1 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_1 )->tp_iternext)( tmp_iterator_name_1 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooooooooooooooo";
                    exception_lineno = 64;
                    goto try_except_handler_5;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooooooooooooooo";
            exception_lineno = 64;
            goto try_except_handler_5;
        }
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto try_except_handler_4;
    // End of try:
    try_end_2:;
    goto try_end_3;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto try_except_handler_3;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_1__source_iter );
    Py_DECREF( tmp_tuple_unpack_1__source_iter );
    tmp_tuple_unpack_1__source_iter = NULL;

    {
        PyObject *tmp_assign_source_20;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_1 );
        tmp_assign_source_20 = tmp_tuple_unpack_1__element_1;
        {
            PyObject *old = var_key;
            var_key = tmp_assign_source_20;
            Py_INCREF( var_key );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_1 );
    tmp_tuple_unpack_1__element_1 = NULL;

    {
        PyObject *tmp_assign_source_21;
        CHECK_OBJECT( tmp_tuple_unpack_1__element_2 );
        tmp_assign_source_21 = tmp_tuple_unpack_1__element_2;
        {
            PyObject *old = var_argument;
            var_argument = tmp_assign_source_21;
            Py_INCREF( var_argument );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_1__element_2 );
    tmp_tuple_unpack_1__element_2 = NULL;

    loop_start_3:;
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_key );
        tmp_compexpr_left_1 = var_key;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        goto loop_end_3;
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = Py_True;
        {
            PyObject *old = var_keys_only;
            assert( old != NULL );
            var_keys_only = tmp_assign_source_22;
            Py_INCREF( var_keys_only );
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = Py_True;
        {
            PyObject *old = tmp_try_except_1__unhandled_indicator;
            tmp_try_except_1__unhandled_indicator = tmp_assign_source_23;
            Py_INCREF( tmp_try_except_1__unhandled_indicator );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    // Tried code:
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_key_name_1;
        CHECK_OBJECT( var_param_dict );
        tmp_dict_name_1 = var_param_dict;
        CHECK_OBJECT( var_key );
        tmp_key_name_1 = var_key;
        tmp_assign_source_24 = DICT_GET_ITEM( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 68;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_7;
        }
        {
            PyObject *old = var_key_param;
            var_key_param = tmp_assign_source_24;
            Py_XDECREF( old );
        }

    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_7:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = Py_False;
        {
            PyObject *old = tmp_try_except_1__unhandled_indicator;
            assert( old != NULL );
            tmp_try_except_1__unhandled_indicator = tmp_assign_source_25;
            Py_INCREF( tmp_try_except_1__unhandled_indicator );
            Py_DECREF( old );
        }

    }
    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_4 == NULL )
    {
        exception_keeper_tb_4 = MAKE_TRACEBACK( frame_c9150d2e8299e357a752bb5c8ba8c265, exception_keeper_lineno_4 );
    }
    else if ( exception_keeper_lineno_4 != 0 )
    {
        exception_keeper_tb_4 = ADD_TRACEBACK( exception_keeper_tb_4, frame_c9150d2e8299e357a752bb5c8ba8c265, exception_keeper_lineno_4 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
    PyException_SetTraceback( exception_keeper_value_4, (PyObject *)exception_keeper_tb_4 );
    PUBLISH_EXCEPTION( &exception_keeper_type_4, &exception_keeper_value_4, &exception_keeper_tb_4 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        tmp_compexpr_left_2 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_2 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 69;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_8;
        }
        tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            CHECK_OBJECT( var_argument );
            tmp_ass_subvalue_1 = var_argument;
            if ( var_non_matching_keys == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "non_matching_keys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 70;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_8;
            }

            tmp_ass_subscribed_1 = var_non_matching_keys;
            CHECK_OBJECT( var_key );
            tmp_ass_subscript_1 = var_key;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 70;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_8;
            }
        }
        goto branch_end_2;
        branch_no_2:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 67;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame) frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooooooooooooooooooooooo";
        goto try_except_handler_8;
        branch_end_2:;
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto try_except_handler_6;
    // End of try:
    try_end_5:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_4;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_6_get_executed_params );
    return NULL;
    // End of try:
    try_end_4:;
    {
        nuitka_bool tmp_condition_result_3;
        nuitka_bool tmp_compexpr_left_3;
        nuitka_bool tmp_compexpr_right_3;
        int tmp_truth_name_1;
        CHECK_OBJECT( tmp_try_except_1__unhandled_indicator );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_try_except_1__unhandled_indicator );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_6;
        }
        tmp_compexpr_left_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_compexpr_right_3 = NUITKA_BOOL_TRUE;
        tmp_condition_result_3 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            nuitka_bool tmp_condition_result_4;
            PyObject *tmp_key_name_2;
            PyObject *tmp_dict_name_2;
            CHECK_OBJECT( var_key );
            tmp_key_name_2 = var_key;
            CHECK_OBJECT( var_keys_used );
            tmp_dict_name_2 = var_keys_used;
            tmp_res = PyDict_Contains( tmp_dict_name_2, tmp_key_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 72;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_6;
            }
            tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            {
                PyObject *tmp_assign_source_26;
                tmp_assign_source_26 = Py_True;
                {
                    PyObject *old = var_had_multiple_value_error;
                    var_had_multiple_value_error = tmp_assign_source_26;
                    Py_INCREF( var_had_multiple_value_error );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_27;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_source_name_5;
                tmp_left_name_1 = const_str_digest_f9c119ad8dadc6a9942c560c6f8f7351;
                CHECK_OBJECT( var_funcdef );
                tmp_source_name_5 = var_funcdef;
                tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_name );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 75;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_6;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
                CHECK_OBJECT( var_key );
                tmp_tuple_element_1 = var_key;
                Py_INCREF( tmp_tuple_element_1 );
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
                tmp_assign_source_27 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_assign_source_27 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 74;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_6;
                }
                {
                    PyObject *old = var_m;
                    var_m = tmp_assign_source_27;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_28;
                PyObject *tmp_iter_arg_5;
                PyObject *tmp_called_instance_4;
                CHECK_OBJECT( par_var_args );
                tmp_called_instance_4 = par_var_args;
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 76;
                tmp_iter_arg_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_4, const_str_plain_get_calling_nodes );
                if ( tmp_iter_arg_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 76;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_6;
                }
                tmp_assign_source_28 = MAKE_ITERATOR( tmp_iter_arg_5 );
                Py_DECREF( tmp_iter_arg_5 );
                if ( tmp_assign_source_28 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 76;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_6;
                }
                {
                    PyObject *old = tmp_for_loop_3__for_iterator;
                    tmp_for_loop_3__for_iterator = tmp_assign_source_28;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            loop_start_4:;
            {
                PyObject *tmp_next_source_3;
                PyObject *tmp_assign_source_29;
                CHECK_OBJECT( tmp_for_loop_3__for_iterator );
                tmp_next_source_3 = tmp_for_loop_3__for_iterator;
                tmp_assign_source_29 = ITERATOR_NEXT( tmp_next_source_3 );
                if ( tmp_assign_source_29 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_4;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "oooooooooooooooooooooooooo";
                        exception_lineno = 76;
                        goto try_except_handler_9;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_3__iter_value;
                    tmp_for_loop_3__iter_value = tmp_assign_source_29;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_30;
                CHECK_OBJECT( tmp_for_loop_3__iter_value );
                tmp_assign_source_30 = tmp_for_loop_3__iter_value;
                {
                    PyObject *old = var_node;
                    var_node = tmp_assign_source_30;
                    Py_INCREF( var_node );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_name_3;
                PyObject *tmp_source_name_6;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_kw_name_1;
                PyObject *tmp_dict_key_1;
                PyObject *tmp_dict_value_1;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_analysis );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_analysis );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "analysis" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 77;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_9;
                }

                tmp_source_name_6 = tmp_mvar_value_3;
                tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_add );
                if ( tmp_called_name_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 77;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_9;
                }
                CHECK_OBJECT( var_parent_context );
                tmp_tuple_element_2 = var_parent_context;
                tmp_args_name_1 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_2 );
                tmp_tuple_element_2 = const_str_digest_c8310735198f243c63e074ef2322d2ed;
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_2 );
                CHECK_OBJECT( var_node );
                tmp_tuple_element_2 = var_node;
                Py_INCREF( tmp_tuple_element_2 );
                PyTuple_SET_ITEM( tmp_args_name_1, 2, tmp_tuple_element_2 );
                tmp_dict_key_1 = const_str_plain_message;
                CHECK_OBJECT( var_m );
                tmp_dict_value_1 = var_m;
                tmp_kw_name_1 = _PyDict_NewPresized( 1 );
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
                assert( !(tmp_res != 0) );
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 77;
                tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_called_name_3 );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 77;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_9;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 76;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_9;
            }
            goto loop_start_4;
            loop_end_4:;
            goto try_end_6;
            // Exception handler code:
            try_except_handler_9:;
            exception_keeper_type_6 = exception_type;
            exception_keeper_value_6 = exception_value;
            exception_keeper_tb_6 = exception_tb;
            exception_keeper_lineno_6 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_3__iter_value );
            tmp_for_loop_3__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
            Py_DECREF( tmp_for_loop_3__for_iterator );
            tmp_for_loop_3__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_6;
            exception_value = exception_keeper_value_6;
            exception_tb = exception_keeper_tb_6;
            exception_lineno = exception_keeper_lineno_6;

            goto try_except_handler_6;
            // End of try:
            try_end_6:;
            Py_XDECREF( tmp_for_loop_3__iter_value );
            tmp_for_loop_3__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_3__for_iterator );
            Py_DECREF( tmp_for_loop_3__for_iterator );
            tmp_for_loop_3__for_iterator = NULL;

            goto branch_end_4;
            branch_no_4:;
            {
                PyObject *tmp_called_name_4;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_args_element_name_4;
                PyObject *tmp_args_element_name_5;
                PyObject *tmp_args_element_name_6;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_ExecutedParam );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ExecutedParam );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ExecutedParam" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 80;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_6;
                }

                tmp_called_name_4 = tmp_mvar_value_4;
                CHECK_OBJECT( par_execution_context );
                tmp_args_element_name_4 = par_execution_context;
                if ( var_key_param == NULL )
                {

                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "key_param" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 80;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_6;
                }

                tmp_args_element_name_5 = var_key_param;
                CHECK_OBJECT( var_argument );
                tmp_args_element_name_6 = var_argument;
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 80;
                {
                    PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_5, tmp_args_element_name_6 };
                    tmp_dictset_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_4, call_args );
                }

                if ( tmp_dictset_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 80;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_6;
                }
                CHECK_OBJECT( var_keys_used );
                tmp_dictset_dict = var_keys_used;
                CHECK_OBJECT( var_key );
                tmp_dictset_key = var_key;
                tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
                Py_DECREF( tmp_dictset_value );
                if ( tmp_res != 0 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 80;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_6;
                }
            }
            branch_end_4:;
        }
        branch_no_3:;
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_6:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_try_except_1__unhandled_indicator );
    tmp_try_except_1__unhandled_indicator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto try_except_handler_3;
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_try_except_1__unhandled_indicator );
    Py_DECREF( tmp_try_except_1__unhandled_indicator );
    tmp_try_except_1__unhandled_indicator = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_iter_arg_6;
        PyObject *tmp_next_arg_2;
        PyObject *tmp_next_default_2;
        CHECK_OBJECT( var_var_arg_iterator );
        tmp_next_arg_2 = var_var_arg_iterator;
        tmp_next_default_2 = const_tuple_none_none_tuple;
        tmp_iter_arg_6 = BUILTIN_NEXT2( tmp_next_arg_2, tmp_next_default_2 );
        if ( tmp_iter_arg_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_10;
        }
        tmp_assign_source_31 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_6 );
        Py_DECREF( tmp_iter_arg_6 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_10;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__source_iter;
            tmp_tuple_unpack_2__source_iter = tmp_assign_source_31;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_unpack_3;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_3 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_32 = UNPACK_NEXT( tmp_unpack_3, 0, 2 );
        if ( tmp_assign_source_32 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooo";
            exception_lineno = 81;
            goto try_except_handler_11;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_1;
            tmp_tuple_unpack_2__element_1 = tmp_assign_source_32;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_unpack_4;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_unpack_4 = tmp_tuple_unpack_2__source_iter;
        tmp_assign_source_33 = UNPACK_NEXT( tmp_unpack_4, 1, 2 );
        if ( tmp_assign_source_33 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooo";
            exception_lineno = 81;
            goto try_except_handler_11;
        }
        {
            PyObject *old = tmp_tuple_unpack_2__element_2;
            tmp_tuple_unpack_2__element_2 = tmp_assign_source_33;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_2;
        CHECK_OBJECT( tmp_tuple_unpack_2__source_iter );
        tmp_iterator_name_2 = tmp_tuple_unpack_2__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_2 ); assert( HAS_ITERNEXT( tmp_iterator_name_2 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_2 )->tp_iternext)( tmp_iterator_name_2 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooooooooooooooo";
                    exception_lineno = 81;
                    goto try_except_handler_11;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooooooooooooooo";
            exception_lineno = 81;
            goto try_except_handler_11;
        }
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_8 = exception_type;
    exception_keeper_value_8 = exception_value;
    exception_keeper_tb_8 = exception_tb;
    exception_keeper_lineno_8 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_8;
    exception_value = exception_keeper_value_8;
    exception_tb = exception_keeper_tb_8;
    exception_lineno = exception_keeper_lineno_8;

    goto try_except_handler_10;
    // End of try:
    try_end_8:;
    goto try_end_9;
    // Exception handler code:
    try_except_handler_10:;
    exception_keeper_type_9 = exception_type;
    exception_keeper_value_9 = exception_value;
    exception_keeper_tb_9 = exception_tb;
    exception_keeper_lineno_9 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_9;
    exception_value = exception_keeper_value_9;
    exception_tb = exception_keeper_tb_9;
    exception_lineno = exception_keeper_lineno_9;

    goto try_except_handler_3;
    // End of try:
    try_end_9:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_2__source_iter );
    Py_DECREF( tmp_tuple_unpack_2__source_iter );
    tmp_tuple_unpack_2__source_iter = NULL;

    {
        PyObject *tmp_assign_source_34;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_1 );
        tmp_assign_source_34 = tmp_tuple_unpack_2__element_1;
        {
            PyObject *old = var_key;
            assert( old != NULL );
            var_key = tmp_assign_source_34;
            Py_INCREF( var_key );
            Py_DECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_1 );
    tmp_tuple_unpack_2__element_1 = NULL;

    {
        PyObject *tmp_assign_source_35;
        CHECK_OBJECT( tmp_tuple_unpack_2__element_2 );
        tmp_assign_source_35 = tmp_tuple_unpack_2__element_2;
        {
            PyObject *old = var_argument;
            assert( old != NULL );
            var_argument = tmp_assign_source_35;
            Py_INCREF( var_argument );
            Py_DECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_2__element_2 );
    tmp_tuple_unpack_2__element_2 = NULL;

    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 65;
        type_description_1 = "oooooooooooooooooooooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_3;
    loop_end_3:;
    // Tried code:
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_source_name_7;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_dict_name_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_source_name_8;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( var_result_params );
        tmp_source_name_7 = var_result_params;
        tmp_called_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_append );
        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_12;
        }
        CHECK_OBJECT( var_keys_used );
        tmp_dict_name_3 = var_keys_used;
        CHECK_OBJECT( var_param );
        tmp_source_name_9 = var_param;
        tmp_source_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_name );
        if ( tmp_source_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 84;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_12;
        }
        tmp_key_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_value );
        Py_DECREF( tmp_source_name_8 );
        if ( tmp_key_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 84;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_12;
        }
        tmp_args_element_name_7 = DICT_GET_ITEM( tmp_dict_name_3, tmp_key_name_3 );
        Py_DECREF( tmp_key_name_3 );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_5 );

            exception_lineno = 84;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_12;
        }
        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 84;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_12;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    goto loop_start_2;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_6_get_executed_params );
    return NULL;
    // Exception handler code:
    try_except_handler_12:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_2 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_2 );
    exception_preserved_value_2 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_2 );
    exception_preserved_tb_2 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_2 );

    if ( exception_keeper_tb_10 == NULL )
    {
        exception_keeper_tb_10 = MAKE_TRACEBACK( frame_c9150d2e8299e357a752bb5c8ba8c265, exception_keeper_lineno_10 );
    }
    else if ( exception_keeper_lineno_10 != 0 )
    {
        exception_keeper_tb_10 = ADD_TRACEBACK( exception_keeper_tb_10, frame_c9150d2e8299e357a752bb5c8ba8c265, exception_keeper_lineno_10 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_10, &exception_keeper_value_10, &exception_keeper_tb_10 );
    PyException_SetTraceback( exception_keeper_value_10, (PyObject *)exception_keeper_tb_10 );
    PUBLISH_EXCEPTION( &exception_keeper_type_10, &exception_keeper_value_10, &exception_keeper_tb_10 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_5;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_compexpr_left_4;
        PyObject *tmp_compexpr_right_4;
        tmp_compexpr_left_4 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_4 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_4, tmp_compexpr_right_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_13;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 86;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_13;
        }
        tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 83;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame) frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooooooooooooooooooooooo";
        goto try_except_handler_13;
        branch_no_5:;
    }
    goto try_end_10;
    // Exception handler code:
    try_except_handler_13:;
    exception_keeper_type_11 = exception_type;
    exception_keeper_value_11 = exception_value;
    exception_keeper_tb_11 = exception_tb;
    exception_keeper_lineno_11 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    // Re-raise.
    exception_type = exception_keeper_type_11;
    exception_value = exception_keeper_value_11;
    exception_tb = exception_keeper_tb_11;
    exception_lineno = exception_keeper_lineno_11;

    goto try_except_handler_3;
    // End of try:
    try_end_10:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_2, exception_preserved_value_2, exception_preserved_tb_2 );
    goto try_end_11;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_6_get_executed_params );
    return NULL;
    // End of try:
    try_end_11:;
    {
        nuitka_bool tmp_condition_result_6;
        PyObject *tmp_compexpr_left_5;
        PyObject *tmp_compexpr_right_5;
        PyObject *tmp_source_name_10;
        CHECK_OBJECT( var_param );
        tmp_source_name_10 = var_param;
        tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_star_count );
        if ( tmp_compexpr_left_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_compexpr_right_5 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
        Py_DECREF( tmp_compexpr_left_5 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 89;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_36;
            tmp_assign_source_36 = PyList_New( 0 );
            {
                PyObject *old = var_lazy_context_list;
                var_lazy_context_list = tmp_assign_source_36;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( var_argument );
            tmp_compexpr_left_6 = var_argument;
            tmp_compexpr_right_6 = Py_None;
            tmp_condition_result_7 = ( tmp_compexpr_left_6 != tmp_compexpr_right_6 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_called_instance_5;
                PyObject *tmp_call_result_3;
                PyObject *tmp_args_element_name_8;
                CHECK_OBJECT( var_lazy_context_list );
                tmp_called_instance_5 = var_lazy_context_list;
                CHECK_OBJECT( var_argument );
                tmp_args_element_name_8 = var_argument;
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 93;
                {
                    PyObject *call_args[] = { tmp_args_element_name_8 };
                    tmp_call_result_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_5, const_str_plain_append, call_args );
                }

                if ( tmp_call_result_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 93;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_3;
                }
                Py_DECREF( tmp_call_result_3 );
            }
            {
                PyObject *tmp_assign_source_37;
                PyObject *tmp_iter_arg_7;
                CHECK_OBJECT( var_var_arg_iterator );
                tmp_iter_arg_7 = var_var_arg_iterator;
                tmp_assign_source_37 = MAKE_ITERATOR( tmp_iter_arg_7 );
                if ( tmp_assign_source_37 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 94;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = tmp_for_loop_4__for_iterator;
                    tmp_for_loop_4__for_iterator = tmp_assign_source_37;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            loop_start_5:;
            {
                PyObject *tmp_next_source_4;
                PyObject *tmp_assign_source_38;
                CHECK_OBJECT( tmp_for_loop_4__for_iterator );
                tmp_next_source_4 = tmp_for_loop_4__for_iterator;
                tmp_assign_source_38 = ITERATOR_NEXT( tmp_next_source_4 );
                if ( tmp_assign_source_38 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_5;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "oooooooooooooooooooooooooo";
                        exception_lineno = 94;
                        goto try_except_handler_14;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_4__iter_value;
                    tmp_for_loop_4__iter_value = tmp_assign_source_38;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_39;
                PyObject *tmp_iter_arg_8;
                CHECK_OBJECT( tmp_for_loop_4__iter_value );
                tmp_iter_arg_8 = tmp_for_loop_4__iter_value;
                tmp_assign_source_39 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_8 );
                if ( tmp_assign_source_39 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 94;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_15;
                }
                {
                    PyObject *old = tmp_tuple_unpack_3__source_iter;
                    tmp_tuple_unpack_3__source_iter = tmp_assign_source_39;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            {
                PyObject *tmp_assign_source_40;
                PyObject *tmp_unpack_5;
                CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                tmp_unpack_5 = tmp_tuple_unpack_3__source_iter;
                tmp_assign_source_40 = UNPACK_NEXT( tmp_unpack_5, 0, 2 );
                if ( tmp_assign_source_40 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "oooooooooooooooooooooooooo";
                    exception_lineno = 94;
                    goto try_except_handler_16;
                }
                {
                    PyObject *old = tmp_tuple_unpack_3__element_1;
                    tmp_tuple_unpack_3__element_1 = tmp_assign_source_40;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_41;
                PyObject *tmp_unpack_6;
                CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                tmp_unpack_6 = tmp_tuple_unpack_3__source_iter;
                tmp_assign_source_41 = UNPACK_NEXT( tmp_unpack_6, 1, 2 );
                if ( tmp_assign_source_41 == NULL )
                {
                    if ( !ERROR_OCCURRED() )
                    {
                        exception_type = PyExc_StopIteration;
                        Py_INCREF( exception_type );
                        exception_value = NULL;
                        exception_tb = NULL;
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    }


                    type_description_1 = "oooooooooooooooooooooooooo";
                    exception_lineno = 94;
                    goto try_except_handler_16;
                }
                {
                    PyObject *old = tmp_tuple_unpack_3__element_2;
                    tmp_tuple_unpack_3__element_2 = tmp_assign_source_41;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_iterator_name_3;
                CHECK_OBJECT( tmp_tuple_unpack_3__source_iter );
                tmp_iterator_name_3 = tmp_tuple_unpack_3__source_iter;
                // Check if iterator has left-over elements.
                CHECK_OBJECT( tmp_iterator_name_3 ); assert( HAS_ITERNEXT( tmp_iterator_name_3 ) );

                tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_3 )->tp_iternext)( tmp_iterator_name_3 );

                if (likely( tmp_iterator_attempt == NULL ))
                {
                    PyObject *error = GET_ERROR_OCCURRED();

                    if ( error != NULL )
                    {
                        if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                        {
                            CLEAR_ERROR_OCCURRED();
                        }
                        else
                        {
                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                            type_description_1 = "oooooooooooooooooooooooooo";
                            exception_lineno = 94;
                            goto try_except_handler_16;
                        }
                    }
                }
                else
                {
                    Py_DECREF( tmp_iterator_attempt );

                    // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                    PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                    PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooooooooooooooo";
                    exception_lineno = 94;
                    goto try_except_handler_16;
                }
            }
            goto try_end_12;
            // Exception handler code:
            try_except_handler_16:;
            exception_keeper_type_12 = exception_type;
            exception_keeper_value_12 = exception_value;
            exception_keeper_tb_12 = exception_tb;
            exception_keeper_lineno_12 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
            Py_DECREF( tmp_tuple_unpack_3__source_iter );
            tmp_tuple_unpack_3__source_iter = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_12;
            exception_value = exception_keeper_value_12;
            exception_tb = exception_keeper_tb_12;
            exception_lineno = exception_keeper_lineno_12;

            goto try_except_handler_15;
            // End of try:
            try_end_12:;
            goto try_end_13;
            // Exception handler code:
            try_except_handler_15:;
            exception_keeper_type_13 = exception_type;
            exception_keeper_value_13 = exception_value;
            exception_keeper_tb_13 = exception_tb;
            exception_keeper_lineno_13 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_tuple_unpack_3__element_1 );
            tmp_tuple_unpack_3__element_1 = NULL;

            Py_XDECREF( tmp_tuple_unpack_3__element_2 );
            tmp_tuple_unpack_3__element_2 = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_13;
            exception_value = exception_keeper_value_13;
            exception_tb = exception_keeper_tb_13;
            exception_lineno = exception_keeper_lineno_13;

            goto try_except_handler_14;
            // End of try:
            try_end_13:;
            CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_3__source_iter );
            Py_DECREF( tmp_tuple_unpack_3__source_iter );
            tmp_tuple_unpack_3__source_iter = NULL;

            {
                PyObject *tmp_assign_source_42;
                CHECK_OBJECT( tmp_tuple_unpack_3__element_1 );
                tmp_assign_source_42 = tmp_tuple_unpack_3__element_1;
                {
                    PyObject *old = var_key;
                    assert( old != NULL );
                    var_key = tmp_assign_source_42;
                    Py_INCREF( var_key );
                    Py_DECREF( old );
                }

            }
            Py_XDECREF( tmp_tuple_unpack_3__element_1 );
            tmp_tuple_unpack_3__element_1 = NULL;

            {
                PyObject *tmp_assign_source_43;
                CHECK_OBJECT( tmp_tuple_unpack_3__element_2 );
                tmp_assign_source_43 = tmp_tuple_unpack_3__element_2;
                {
                    PyObject *old = var_argument;
                    assert( old != NULL );
                    var_argument = tmp_assign_source_43;
                    Py_INCREF( var_argument );
                    Py_DECREF( old );
                }

            }
            Py_XDECREF( tmp_tuple_unpack_3__element_2 );
            tmp_tuple_unpack_3__element_2 = NULL;

            {
                nuitka_bool tmp_condition_result_8;
                int tmp_truth_name_2;
                CHECK_OBJECT( var_key );
                tmp_truth_name_2 = CHECK_IF_TRUE( var_key );
                if ( tmp_truth_name_2 == -1 )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 96;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_14;
                }
                tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_8;
                }
                else
                {
                    goto branch_no_8;
                }
                branch_yes_8:;
                {
                    PyObject *tmp_called_instance_6;
                    PyObject *tmp_call_result_4;
                    PyObject *tmp_args_element_name_9;
                    PyObject *tmp_tuple_element_3;
                    CHECK_OBJECT( var_var_arg_iterator );
                    tmp_called_instance_6 = var_var_arg_iterator;
                    CHECK_OBJECT( var_key );
                    tmp_tuple_element_3 = var_key;
                    tmp_args_element_name_9 = PyTuple_New( 2 );
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_args_element_name_9, 0, tmp_tuple_element_3 );
                    CHECK_OBJECT( var_argument );
                    tmp_tuple_element_3 = var_argument;
                    Py_INCREF( tmp_tuple_element_3 );
                    PyTuple_SET_ITEM( tmp_args_element_name_9, 1, tmp_tuple_element_3 );
                    frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 97;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_9 };
                        tmp_call_result_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_6, const_str_plain_push_back, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_9 );
                    if ( tmp_call_result_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 97;
                        type_description_1 = "oooooooooooooooooooooooooo";
                        goto try_except_handler_14;
                    }
                    Py_DECREF( tmp_call_result_4 );
                }
                goto loop_end_5;
                branch_no_8:;
            }
            {
                PyObject *tmp_called_instance_7;
                PyObject *tmp_call_result_5;
                PyObject *tmp_args_element_name_10;
                CHECK_OBJECT( var_lazy_context_list );
                tmp_called_instance_7 = var_lazy_context_list;
                CHECK_OBJECT( var_argument );
                tmp_args_element_name_10 = var_argument;
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 99;
                {
                    PyObject *call_args[] = { tmp_args_element_name_10 };
                    tmp_call_result_5 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_7, const_str_plain_append, call_args );
                }

                if ( tmp_call_result_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 99;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_14;
                }
                Py_DECREF( tmp_call_result_5 );
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_14;
            }
            goto loop_start_5;
            loop_end_5:;
            goto try_end_14;
            // Exception handler code:
            try_except_handler_14:;
            exception_keeper_type_14 = exception_type;
            exception_keeper_value_14 = exception_value;
            exception_keeper_tb_14 = exception_tb;
            exception_keeper_lineno_14 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_4__iter_value );
            tmp_for_loop_4__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
            Py_DECREF( tmp_for_loop_4__for_iterator );
            tmp_for_loop_4__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_14;
            exception_value = exception_keeper_value_14;
            exception_tb = exception_keeper_tb_14;
            exception_lineno = exception_keeper_lineno_14;

            goto try_except_handler_3;
            // End of try:
            try_end_14:;
            Py_XDECREF( tmp_for_loop_4__iter_value );
            tmp_for_loop_4__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_4__for_iterator );
            Py_DECREF( tmp_for_loop_4__for_iterator );
            tmp_for_loop_4__for_iterator = NULL;

            branch_no_7:;
        }
        {
            PyObject *tmp_assign_source_44;
            PyObject *tmp_called_name_6;
            PyObject *tmp_source_name_11;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_source_name_12;
            PyObject *tmp_args_element_name_12;
            PyObject *tmp_args_element_name_13;
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_iterable );

            if (unlikely( tmp_mvar_value_5 == NULL ))
            {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iterable );
            }

            if ( tmp_mvar_value_5 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iterable" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 100;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_source_name_11 = tmp_mvar_value_5;
            tmp_called_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_FakeSequence );
            if ( tmp_called_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( par_execution_context );
            tmp_source_name_12 = par_execution_context;
            tmp_args_element_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_evaluator );
            if ( tmp_args_element_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_6 );

                exception_lineno = 100;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            tmp_args_element_name_12 = const_str_plain_tuple;
            CHECK_OBJECT( var_lazy_context_list );
            tmp_args_element_name_13 = var_lazy_context_list;
            frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 100;
            {
                PyObject *call_args[] = { tmp_args_element_name_11, tmp_args_element_name_12, tmp_args_element_name_13 };
                tmp_assign_source_44 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_element_name_11 );
            if ( tmp_assign_source_44 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 100;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_seq;
                var_seq = tmp_assign_source_44;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_45;
            PyObject *tmp_called_name_7;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_args_element_name_14;
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyKnownContext );

            if (unlikely( tmp_mvar_value_6 == NULL ))
            {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyKnownContext );
            }

            if ( tmp_mvar_value_6 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyKnownContext" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 101;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_7 = tmp_mvar_value_6;
            CHECK_OBJECT( var_seq );
            tmp_args_element_name_14 = var_seq;
            frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 101;
            {
                PyObject *call_args[] = { tmp_args_element_name_14 };
                tmp_assign_source_45 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            if ( tmp_assign_source_45 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 101;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            {
                PyObject *old = var_result_arg;
                var_result_arg = tmp_assign_source_45;
                Py_XDECREF( old );
            }

        }
        goto branch_end_6;
        branch_no_6:;
        {
            nuitka_bool tmp_condition_result_9;
            PyObject *tmp_compexpr_left_7;
            PyObject *tmp_compexpr_right_7;
            PyObject *tmp_source_name_13;
            CHECK_OBJECT( var_param );
            tmp_source_name_13 = var_param;
            tmp_compexpr_left_7 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_star_count );
            if ( tmp_compexpr_left_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            tmp_compexpr_right_7 = const_int_pos_2;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_7, tmp_compexpr_right_7 );
            Py_DECREF( tmp_compexpr_left_7 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 102;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_9;
            }
            else
            {
                goto branch_no_9;
            }
            branch_yes_9:;
            {
                PyObject *tmp_assign_source_46;
                PyObject *tmp_called_name_8;
                PyObject *tmp_source_name_14;
                PyObject *tmp_mvar_value_7;
                PyObject *tmp_args_element_name_15;
                PyObject *tmp_source_name_15;
                PyObject *tmp_args_element_name_16;
                PyObject *tmp_dict_seq_1;
                tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_iterable );

                if (unlikely( tmp_mvar_value_7 == NULL ))
                {
                    tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iterable );
                }

                if ( tmp_mvar_value_7 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iterable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 104;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_3;
                }

                tmp_source_name_14 = tmp_mvar_value_7;
                tmp_called_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_FakeDict );
                if ( tmp_called_name_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 104;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_3;
                }
                CHECK_OBJECT( par_execution_context );
                tmp_source_name_15 = par_execution_context;
                tmp_args_element_name_15 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_evaluator );
                if ( tmp_args_element_name_15 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_8 );

                    exception_lineno = 104;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_3;
                }
                if ( var_non_matching_keys == NULL )
                {
                    Py_DECREF( tmp_called_name_8 );
                    Py_DECREF( tmp_args_element_name_15 );
                    exception_type = PyExc_UnboundLocalError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "non_matching_keys" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 104;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_3;
                }

                tmp_dict_seq_1 = var_non_matching_keys;
                tmp_args_element_name_16 = TO_DICT( tmp_dict_seq_1, NULL );
                if ( tmp_args_element_name_16 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_8 );
                    Py_DECREF( tmp_args_element_name_15 );

                    exception_lineno = 104;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_3;
                }
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 104;
                {
                    PyObject *call_args[] = { tmp_args_element_name_15, tmp_args_element_name_16 };
                    tmp_assign_source_46 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_8, call_args );
                }

                Py_DECREF( tmp_called_name_8 );
                Py_DECREF( tmp_args_element_name_15 );
                Py_DECREF( tmp_args_element_name_16 );
                if ( tmp_assign_source_46 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 104;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = var_dct;
                    var_dct = tmp_assign_source_46;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_47;
                PyObject *tmp_called_name_9;
                PyObject *tmp_mvar_value_8;
                PyObject *tmp_args_element_name_17;
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyKnownContext );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyKnownContext );
                }

                if ( tmp_mvar_value_8 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyKnownContext" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 105;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_3;
                }

                tmp_called_name_9 = tmp_mvar_value_8;
                CHECK_OBJECT( var_dct );
                tmp_args_element_name_17 = var_dct;
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 105;
                {
                    PyObject *call_args[] = { tmp_args_element_name_17 };
                    tmp_assign_source_47 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
                }

                if ( tmp_assign_source_47 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 105;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_3;
                }
                {
                    PyObject *old = var_result_arg;
                    var_result_arg = tmp_assign_source_47;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_48;
                tmp_assign_source_48 = PyDict_New();
                {
                    PyObject *old = var_non_matching_keys;
                    var_non_matching_keys = tmp_assign_source_48;
                    Py_XDECREF( old );
                }

            }
            goto branch_end_9;
            branch_no_9:;
            {
                nuitka_bool tmp_condition_result_10;
                PyObject *tmp_compexpr_left_8;
                PyObject *tmp_compexpr_right_8;
                CHECK_OBJECT( var_argument );
                tmp_compexpr_left_8 = var_argument;
                tmp_compexpr_right_8 = Py_None;
                tmp_condition_result_10 = ( tmp_compexpr_left_8 == tmp_compexpr_right_8 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_10;
                }
                else
                {
                    goto branch_no_10;
                }
                branch_yes_10:;
                {
                    nuitka_bool tmp_condition_result_11;
                    PyObject *tmp_compexpr_left_9;
                    PyObject *tmp_compexpr_right_9;
                    PyObject *tmp_source_name_16;
                    CHECK_OBJECT( var_param );
                    tmp_source_name_16 = var_param;
                    tmp_compexpr_left_9 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_default );
                    if ( tmp_compexpr_left_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 111;
                        type_description_1 = "oooooooooooooooooooooooooo";
                        goto try_except_handler_3;
                    }
                    tmp_compexpr_right_9 = Py_None;
                    tmp_condition_result_11 = ( tmp_compexpr_left_9 == tmp_compexpr_right_9 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                    Py_DECREF( tmp_compexpr_left_9 );
                    if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
                    {
                        goto branch_yes_11;
                    }
                    else
                    {
                        goto branch_no_11;
                    }
                    branch_yes_11:;
                    {
                        PyObject *tmp_assign_source_49;
                        PyObject *tmp_called_name_10;
                        PyObject *tmp_mvar_value_9;
                        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyUnknownContext );

                        if (unlikely( tmp_mvar_value_9 == NULL ))
                        {
                            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyUnknownContext );
                        }

                        if ( tmp_mvar_value_9 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyUnknownContext" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 112;
                            type_description_1 = "oooooooooooooooooooooooooo";
                            goto try_except_handler_3;
                        }

                        tmp_called_name_10 = tmp_mvar_value_9;
                        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 112;
                        tmp_assign_source_49 = CALL_FUNCTION_NO_ARGS( tmp_called_name_10 );
                        if ( tmp_assign_source_49 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 112;
                            type_description_1 = "oooooooooooooooooooooooooo";
                            goto try_except_handler_3;
                        }
                        {
                            PyObject *old = var_result_arg;
                            var_result_arg = tmp_assign_source_49;
                            Py_XDECREF( old );
                        }

                    }
                    {
                        nuitka_bool tmp_condition_result_12;
                        PyObject *tmp_operand_name_2;
                        CHECK_OBJECT( var_keys_only );
                        tmp_operand_name_2 = var_keys_only;
                        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
                        if ( tmp_res == -1 )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 113;
                            type_description_1 = "oooooooooooooooooooooooooo";
                            goto try_except_handler_3;
                        }
                        tmp_condition_result_12 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                        if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
                        {
                            goto branch_yes_12;
                        }
                        else
                        {
                            goto branch_no_12;
                        }
                        branch_yes_12:;
                        {
                            PyObject *tmp_assign_source_50;
                            PyObject *tmp_iter_arg_9;
                            PyObject *tmp_called_instance_8;
                            CHECK_OBJECT( par_var_args );
                            tmp_called_instance_8 = par_var_args;
                            frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 114;
                            tmp_iter_arg_9 = CALL_METHOD_NO_ARGS( tmp_called_instance_8, const_str_plain_get_calling_nodes );
                            if ( tmp_iter_arg_9 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 114;
                                type_description_1 = "oooooooooooooooooooooooooo";
                                goto try_except_handler_3;
                            }
                            tmp_assign_source_50 = MAKE_ITERATOR( tmp_iter_arg_9 );
                            Py_DECREF( tmp_iter_arg_9 );
                            if ( tmp_assign_source_50 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 114;
                                type_description_1 = "oooooooooooooooooooooooooo";
                                goto try_except_handler_3;
                            }
                            {
                                PyObject *old = tmp_for_loop_5__for_iterator;
                                tmp_for_loop_5__for_iterator = tmp_assign_source_50;
                                Py_XDECREF( old );
                            }

                        }
                        // Tried code:
                        loop_start_6:;
                        {
                            PyObject *tmp_next_source_5;
                            PyObject *tmp_assign_source_51;
                            CHECK_OBJECT( tmp_for_loop_5__for_iterator );
                            tmp_next_source_5 = tmp_for_loop_5__for_iterator;
                            tmp_assign_source_51 = ITERATOR_NEXT( tmp_next_source_5 );
                            if ( tmp_assign_source_51 == NULL )
                            {
                                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                                {

                                    goto loop_end_6;
                                }
                                else
                                {

                                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                                    type_description_1 = "oooooooooooooooooooooooooo";
                                    exception_lineno = 114;
                                    goto try_except_handler_17;
                                }
                            }

                            {
                                PyObject *old = tmp_for_loop_5__iter_value;
                                tmp_for_loop_5__iter_value = tmp_assign_source_51;
                                Py_XDECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_assign_source_52;
                            CHECK_OBJECT( tmp_for_loop_5__iter_value );
                            tmp_assign_source_52 = tmp_for_loop_5__iter_value;
                            {
                                PyObject *old = var_node;
                                var_node = tmp_assign_source_52;
                                Py_INCREF( var_node );
                                Py_XDECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_assign_source_53;
                            PyObject *tmp_called_name_11;
                            PyObject *tmp_mvar_value_10;
                            PyObject *tmp_args_element_name_18;
                            PyObject *tmp_args_element_name_19;
                            PyObject *tmp_len_arg_1;
                            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain__error_argument_count );

                            if (unlikely( tmp_mvar_value_10 == NULL ))
                            {
                                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__error_argument_count );
                            }

                            if ( tmp_mvar_value_10 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_error_argument_count" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 115;
                                type_description_1 = "oooooooooooooooooooooooooo";
                                goto try_except_handler_17;
                            }

                            tmp_called_name_11 = tmp_mvar_value_10;
                            CHECK_OBJECT( var_funcdef );
                            tmp_args_element_name_18 = var_funcdef;
                            CHECK_OBJECT( var_unpacked_va );
                            tmp_len_arg_1 = var_unpacked_va;
                            tmp_args_element_name_19 = BUILTIN_LEN( tmp_len_arg_1 );
                            assert( !(tmp_args_element_name_19 == NULL) );
                            frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 115;
                            {
                                PyObject *call_args[] = { tmp_args_element_name_18, tmp_args_element_name_19 };
                                tmp_assign_source_53 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_11, call_args );
                            }

                            Py_DECREF( tmp_args_element_name_19 );
                            if ( tmp_assign_source_53 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 115;
                                type_description_1 = "oooooooooooooooooooooooooo";
                                goto try_except_handler_17;
                            }
                            {
                                PyObject *old = var_m;
                                var_m = tmp_assign_source_53;
                                Py_XDECREF( old );
                            }

                        }
                        {
                            PyObject *tmp_called_name_12;
                            PyObject *tmp_source_name_17;
                            PyObject *tmp_mvar_value_11;
                            PyObject *tmp_call_result_6;
                            PyObject *tmp_args_name_2;
                            PyObject *tmp_tuple_element_4;
                            PyObject *tmp_kw_name_2;
                            PyObject *tmp_dict_key_2;
                            PyObject *tmp_dict_value_2;
                            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_analysis );

                            if (unlikely( tmp_mvar_value_11 == NULL ))
                            {
                                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_analysis );
                            }

                            if ( tmp_mvar_value_11 == NULL )
                            {

                                exception_type = PyExc_NameError;
                                Py_INCREF( exception_type );
                                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "analysis" );
                                exception_tb = NULL;
                                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                                CHAIN_EXCEPTION( exception_value );

                                exception_lineno = 116;
                                type_description_1 = "oooooooooooooooooooooooooo";
                                goto try_except_handler_17;
                            }

                            tmp_source_name_17 = tmp_mvar_value_11;
                            tmp_called_name_12 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_add );
                            if ( tmp_called_name_12 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 116;
                                type_description_1 = "oooooooooooooooooooooooooo";
                                goto try_except_handler_17;
                            }
                            CHECK_OBJECT( var_parent_context );
                            tmp_tuple_element_4 = var_parent_context;
                            tmp_args_name_2 = PyTuple_New( 3 );
                            Py_INCREF( tmp_tuple_element_4 );
                            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
                            tmp_tuple_element_4 = const_str_digest_f1edf9952adf39791783535a6530e3a6;
                            Py_INCREF( tmp_tuple_element_4 );
                            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
                            CHECK_OBJECT( var_node );
                            tmp_tuple_element_4 = var_node;
                            Py_INCREF( tmp_tuple_element_4 );
                            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
                            tmp_dict_key_2 = const_str_plain_message;
                            CHECK_OBJECT( var_m );
                            tmp_dict_value_2 = var_m;
                            tmp_kw_name_2 = _PyDict_NewPresized( 1 );
                            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
                            assert( !(tmp_res != 0) );
                            frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 116;
                            tmp_call_result_6 = CALL_FUNCTION( tmp_called_name_12, tmp_args_name_2, tmp_kw_name_2 );
                            Py_DECREF( tmp_called_name_12 );
                            Py_DECREF( tmp_args_name_2 );
                            Py_DECREF( tmp_kw_name_2 );
                            if ( tmp_call_result_6 == NULL )
                            {
                                assert( ERROR_OCCURRED() );

                                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                                exception_lineno = 116;
                                type_description_1 = "oooooooooooooooooooooooooo";
                                goto try_except_handler_17;
                            }
                            Py_DECREF( tmp_call_result_6 );
                        }
                        if ( CONSIDER_THREADING() == false )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 114;
                            type_description_1 = "oooooooooooooooooooooooooo";
                            goto try_except_handler_17;
                        }
                        goto loop_start_6;
                        loop_end_6:;
                        goto try_end_15;
                        // Exception handler code:
                        try_except_handler_17:;
                        exception_keeper_type_15 = exception_type;
                        exception_keeper_value_15 = exception_value;
                        exception_keeper_tb_15 = exception_tb;
                        exception_keeper_lineno_15 = exception_lineno;
                        exception_type = NULL;
                        exception_value = NULL;
                        exception_tb = NULL;
                        exception_lineno = 0;

                        Py_XDECREF( tmp_for_loop_5__iter_value );
                        tmp_for_loop_5__iter_value = NULL;

                        CHECK_OBJECT( (PyObject *)tmp_for_loop_5__for_iterator );
                        Py_DECREF( tmp_for_loop_5__for_iterator );
                        tmp_for_loop_5__for_iterator = NULL;

                        // Re-raise.
                        exception_type = exception_keeper_type_15;
                        exception_value = exception_keeper_value_15;
                        exception_tb = exception_keeper_tb_15;
                        exception_lineno = exception_keeper_lineno_15;

                        goto try_except_handler_3;
                        // End of try:
                        try_end_15:;
                        Py_XDECREF( tmp_for_loop_5__iter_value );
                        tmp_for_loop_5__iter_value = NULL;

                        CHECK_OBJECT( (PyObject *)tmp_for_loop_5__for_iterator );
                        Py_DECREF( tmp_for_loop_5__for_iterator );
                        tmp_for_loop_5__for_iterator = NULL;

                        branch_no_12:;
                    }
                    goto branch_end_11;
                    branch_no_11:;
                    {
                        PyObject *tmp_assign_source_54;
                        PyObject *tmp_called_name_13;
                        PyObject *tmp_mvar_value_12;
                        PyObject *tmp_args_element_name_20;
                        PyObject *tmp_args_element_name_21;
                        PyObject *tmp_source_name_18;
                        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );

                        if (unlikely( tmp_mvar_value_12 == NULL ))
                        {
                            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );
                        }

                        if ( tmp_mvar_value_12 == NULL )
                        {

                            exception_type = PyExc_NameError;
                            Py_INCREF( exception_type );
                            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyTreeContext" );
                            exception_tb = NULL;
                            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                            CHAIN_EXCEPTION( exception_value );

                            exception_lineno = 119;
                            type_description_1 = "oooooooooooooooooooooooooo";
                            goto try_except_handler_3;
                        }

                        tmp_called_name_13 = tmp_mvar_value_12;
                        CHECK_OBJECT( var_parent_context );
                        tmp_args_element_name_20 = var_parent_context;
                        CHECK_OBJECT( var_param );
                        tmp_source_name_18 = var_param;
                        tmp_args_element_name_21 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_default );
                        if ( tmp_args_element_name_21 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 119;
                            type_description_1 = "oooooooooooooooooooooooooo";
                            goto try_except_handler_3;
                        }
                        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 119;
                        {
                            PyObject *call_args[] = { tmp_args_element_name_20, tmp_args_element_name_21 };
                            tmp_assign_source_54 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_13, call_args );
                        }

                        Py_DECREF( tmp_args_element_name_21 );
                        if ( tmp_assign_source_54 == NULL )
                        {
                            assert( ERROR_OCCURRED() );

                            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                            exception_lineno = 119;
                            type_description_1 = "oooooooooooooooooooooooooo";
                            goto try_except_handler_3;
                        }
                        {
                            PyObject *old = var_result_arg;
                            var_result_arg = tmp_assign_source_54;
                            Py_XDECREF( old );
                        }

                    }
                    branch_end_11:;
                }
                goto branch_end_10;
                branch_no_10:;
                {
                    PyObject *tmp_assign_source_55;
                    CHECK_OBJECT( var_argument );
                    tmp_assign_source_55 = var_argument;
                    {
                        PyObject *old = var_result_arg;
                        var_result_arg = tmp_assign_source_55;
                        Py_INCREF( var_result_arg );
                        Py_XDECREF( old );
                    }

                }
                branch_end_10:;
            }
            branch_end_9:;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_called_name_14;
        PyObject *tmp_source_name_19;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_called_name_15;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_args_element_name_24;
        PyObject *tmp_args_element_name_25;
        CHECK_OBJECT( var_result_params );
        tmp_source_name_19 = var_result_params;
        tmp_called_name_14 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_append );
        if ( tmp_called_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_ExecutedParam );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ExecutedParam );
        }

        if ( tmp_mvar_value_13 == NULL )
        {
            Py_DECREF( tmp_called_name_14 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ExecutedParam" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }

        tmp_called_name_15 = tmp_mvar_value_13;
        CHECK_OBJECT( par_execution_context );
        tmp_args_element_name_23 = par_execution_context;
        CHECK_OBJECT( var_param );
        tmp_args_element_name_24 = var_param;
        if ( var_result_arg == NULL )
        {
            Py_DECREF( tmp_called_name_14 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "result_arg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }

        tmp_args_element_name_25 = var_result_arg;
        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 123;
        {
            PyObject *call_args[] = { tmp_args_element_name_23, tmp_args_element_name_24, tmp_args_element_name_25 };
            tmp_args_element_name_22 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_15, call_args );
        }

        if ( tmp_args_element_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_14 );

            exception_lineno = 123;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 123;
        {
            PyObject *call_args[] = { tmp_args_element_name_22 };
            tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_called_name_14 );
        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    {
        nuitka_bool tmp_condition_result_13;
        PyObject *tmp_operand_name_3;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_14;
        if ( var_result_arg == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "result_arg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }

        tmp_isinstance_inst_1 = var_result_arg;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyUnknownContext );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyUnknownContext );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyUnknownContext" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_14;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_13 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_13;
        }
        else
        {
            goto branch_no_13;
        }
        branch_yes_13:;
        {
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            PyObject *tmp_source_name_20;
            PyObject *tmp_source_name_21;
            CHECK_OBJECT( var_result_params );
            tmp_subscribed_name_1 = var_result_params;
            tmp_subscript_name_1 = const_int_neg_1;
            tmp_dictset_value = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, -1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 125;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            CHECK_OBJECT( var_keys_used );
            tmp_dictset_dict = var_keys_used;
            CHECK_OBJECT( var_param );
            tmp_source_name_21 = var_param;
            tmp_source_name_20 = LOOKUP_ATTRIBUTE( tmp_source_name_21, const_str_plain_name );
            if ( tmp_source_name_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_dictset_value );

                exception_lineno = 125;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            tmp_dictset_key = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_value );
            Py_DECREF( tmp_source_name_20 );
            if ( tmp_dictset_key == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_dictset_value );

                exception_lineno = 125;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
            tmp_res = PyDict_SetItem( tmp_dictset_dict, tmp_dictset_key, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            Py_DECREF( tmp_dictset_key );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 125;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_3;
            }
        }
        branch_no_13:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 59;
        type_description_1 = "oooooooooooooooooooooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_2;
    loop_end_2:;
    goto try_end_16;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_16 = exception_type;
    exception_keeper_value_16 = exception_value;
    exception_keeper_tb_16 = exception_tb;
    exception_keeper_lineno_16 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_16;
    exception_value = exception_keeper_value_16;
    exception_tb = exception_keeper_tb_16;
    exception_lineno = exception_keeper_lineno_16;

    goto frame_exception_exit_1;
    // End of try:
    try_end_16:;
    Py_XDECREF( tmp_for_loop_2__iter_value );
    tmp_for_loop_2__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_2__for_iterator );
    Py_DECREF( tmp_for_loop_2__for_iterator );
    tmp_for_loop_2__for_iterator = NULL;

    {
        nuitka_bool tmp_condition_result_14;
        int tmp_truth_name_3;
        CHECK_OBJECT( var_keys_only );
        tmp_truth_name_3 = CHECK_IF_TRUE( var_keys_only );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_14 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            PyObject *tmp_assign_source_56;
            PyObject *tmp_iter_arg_10;
            PyObject *tmp_left_name_2;
            PyObject *tmp_set_arg_1;
            PyObject *tmp_right_name_2;
            PyObject *tmp_set_arg_2;
            CHECK_OBJECT( var_param_dict );
            tmp_set_arg_1 = var_param_dict;
            tmp_left_name_2 = PySet_New( tmp_set_arg_1 );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_keys_used );
            tmp_set_arg_2 = var_keys_used;
            tmp_right_name_2 = PySet_New( tmp_set_arg_2 );
            if ( tmp_right_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_left_name_2 );

                exception_lineno = 131;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_iter_arg_10 = BINARY_OPERATION_SUB_OBJECT_OBJECT( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_2 );
            Py_DECREF( tmp_right_name_2 );
            assert( !(tmp_iter_arg_10 == NULL) );
            tmp_assign_source_56 = MAKE_ITERATOR( tmp_iter_arg_10 );
            Py_DECREF( tmp_iter_arg_10 );
            if ( tmp_assign_source_56 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 131;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_for_loop_6__for_iterator == NULL );
            tmp_for_loop_6__for_iterator = tmp_assign_source_56;
        }
        // Tried code:
        loop_start_7:;
        {
            PyObject *tmp_next_source_6;
            PyObject *tmp_assign_source_57;
            CHECK_OBJECT( tmp_for_loop_6__for_iterator );
            tmp_next_source_6 = tmp_for_loop_6__for_iterator;
            tmp_assign_source_57 = ITERATOR_NEXT( tmp_next_source_6 );
            if ( tmp_assign_source_57 == NULL )
            {
                if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                {

                    goto loop_end_7;
                }
                else
                {

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    type_description_1 = "oooooooooooooooooooooooooo";
                    exception_lineno = 131;
                    goto try_except_handler_18;
                }
            }

            {
                PyObject *old = tmp_for_loop_6__iter_value;
                tmp_for_loop_6__iter_value = tmp_assign_source_57;
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_58;
            CHECK_OBJECT( tmp_for_loop_6__iter_value );
            tmp_assign_source_58 = tmp_for_loop_6__iter_value;
            {
                PyObject *old = var_k;
                var_k = tmp_assign_source_58;
                Py_INCREF( var_k );
                Py_XDECREF( old );
            }

        }
        {
            PyObject *tmp_assign_source_59;
            PyObject *tmp_dict_name_4;
            PyObject *tmp_key_name_4;
            CHECK_OBJECT( var_param_dict );
            tmp_dict_name_4 = var_param_dict;
            CHECK_OBJECT( var_k );
            tmp_key_name_4 = var_k;
            tmp_assign_source_59 = DICT_GET_ITEM( tmp_dict_name_4, tmp_key_name_4 );
            if ( tmp_assign_source_59 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 132;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }
            {
                PyObject *old = var_param;
                var_param = tmp_assign_source_59;
                Py_XDECREF( old );
            }

        }
        {
            nuitka_bool tmp_condition_result_15;
            PyObject *tmp_operand_name_4;
            int tmp_or_left_truth_1;
            PyObject *tmp_or_left_value_1;
            PyObject *tmp_or_right_value_1;
            int tmp_or_left_truth_2;
            PyObject *tmp_or_left_value_2;
            PyObject *tmp_or_right_value_2;
            int tmp_or_left_truth_3;
            PyObject *tmp_or_left_value_3;
            PyObject *tmp_or_right_value_3;
            PyObject *tmp_source_name_22;
            PyObject *tmp_source_name_23;
            if ( var_non_matching_keys == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "non_matching_keys" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 134;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }

            tmp_or_left_value_1 = var_non_matching_keys;
            tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
            if ( tmp_or_left_truth_1 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }
            if ( tmp_or_left_truth_1 == 1 )
            {
                goto or_left_1;
            }
            else
            {
                goto or_right_1;
            }
            or_right_1:;
            if ( var_had_multiple_value_error == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "had_multiple_value_error" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 134;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }

            tmp_or_left_value_2 = var_had_multiple_value_error;
            tmp_or_left_truth_2 = CHECK_IF_TRUE( tmp_or_left_value_2 );
            if ( tmp_or_left_truth_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }
            if ( tmp_or_left_truth_2 == 1 )
            {
                goto or_left_2;
            }
            else
            {
                goto or_right_2;
            }
            or_right_2:;
            CHECK_OBJECT( var_param );
            tmp_source_name_22 = var_param;
            tmp_or_left_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain_star_count );
            if ( tmp_or_left_value_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }
            tmp_or_left_truth_3 = CHECK_IF_TRUE( tmp_or_left_value_3 );
            if ( tmp_or_left_truth_3 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_or_left_value_3 );

                exception_lineno = 135;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }
            if ( tmp_or_left_truth_3 == 1 )
            {
                goto or_left_3;
            }
            else
            {
                goto or_right_3;
            }
            or_right_3:;
            Py_DECREF( tmp_or_left_value_3 );
            CHECK_OBJECT( var_param );
            tmp_source_name_23 = var_param;
            tmp_or_right_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_23, const_str_plain_default );
            if ( tmp_or_right_value_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 135;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }
            tmp_or_right_value_2 = tmp_or_right_value_3;
            goto or_end_3;
            or_left_3:;
            tmp_or_right_value_2 = tmp_or_left_value_3;
            or_end_3:;
            tmp_or_right_value_1 = tmp_or_right_value_2;
            goto or_end_2;
            or_left_2:;
            Py_INCREF( tmp_or_left_value_2 );
            tmp_or_right_value_1 = tmp_or_left_value_2;
            or_end_2:;
            tmp_operand_name_4 = tmp_or_right_value_1;
            goto or_end_1;
            or_left_1:;
            Py_INCREF( tmp_or_left_value_1 );
            tmp_operand_name_4 = tmp_or_left_value_1;
            or_end_1:;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            Py_DECREF( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 134;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_18;
            }
            tmp_condition_result_15 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_15;
            }
            else
            {
                goto branch_no_15;
            }
            branch_yes_15:;
            {
                PyObject *tmp_assign_source_60;
                PyObject *tmp_iter_arg_11;
                PyObject *tmp_called_instance_9;
                CHECK_OBJECT( par_var_args );
                tmp_called_instance_9 = par_var_args;
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 137;
                tmp_iter_arg_11 = CALL_METHOD_NO_ARGS( tmp_called_instance_9, const_str_plain_get_calling_nodes );
                if ( tmp_iter_arg_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 137;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_18;
                }
                tmp_assign_source_60 = MAKE_ITERATOR( tmp_iter_arg_11 );
                Py_DECREF( tmp_iter_arg_11 );
                if ( tmp_assign_source_60 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 137;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_18;
                }
                {
                    PyObject *old = tmp_for_loop_7__for_iterator;
                    tmp_for_loop_7__for_iterator = tmp_assign_source_60;
                    Py_XDECREF( old );
                }

            }
            // Tried code:
            loop_start_8:;
            {
                PyObject *tmp_next_source_7;
                PyObject *tmp_assign_source_61;
                CHECK_OBJECT( tmp_for_loop_7__for_iterator );
                tmp_next_source_7 = tmp_for_loop_7__for_iterator;
                tmp_assign_source_61 = ITERATOR_NEXT( tmp_next_source_7 );
                if ( tmp_assign_source_61 == NULL )
                {
                    if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
                    {

                        goto loop_end_8;
                    }
                    else
                    {

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        type_description_1 = "oooooooooooooooooooooooooo";
                        exception_lineno = 137;
                        goto try_except_handler_19;
                    }
                }

                {
                    PyObject *old = tmp_for_loop_7__iter_value;
                    tmp_for_loop_7__iter_value = tmp_assign_source_61;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_62;
                CHECK_OBJECT( tmp_for_loop_7__iter_value );
                tmp_assign_source_62 = tmp_for_loop_7__iter_value;
                {
                    PyObject *old = var_node;
                    var_node = tmp_assign_source_62;
                    Py_INCREF( var_node );
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_assign_source_63;
                PyObject *tmp_called_name_16;
                PyObject *tmp_mvar_value_15;
                PyObject *tmp_args_element_name_26;
                PyObject *tmp_args_element_name_27;
                PyObject *tmp_len_arg_2;
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain__error_argument_count );

                if (unlikely( tmp_mvar_value_15 == NULL ))
                {
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__error_argument_count );
                }

                if ( tmp_mvar_value_15 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_error_argument_count" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 138;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_19;
                }

                tmp_called_name_16 = tmp_mvar_value_15;
                CHECK_OBJECT( var_funcdef );
                tmp_args_element_name_26 = var_funcdef;
                CHECK_OBJECT( var_unpacked_va );
                tmp_len_arg_2 = var_unpacked_va;
                tmp_args_element_name_27 = BUILTIN_LEN( tmp_len_arg_2 );
                assert( !(tmp_args_element_name_27 == NULL) );
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 138;
                {
                    PyObject *call_args[] = { tmp_args_element_name_26, tmp_args_element_name_27 };
                    tmp_assign_source_63 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_16, call_args );
                }

                Py_DECREF( tmp_args_element_name_27 );
                if ( tmp_assign_source_63 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 138;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_19;
                }
                {
                    PyObject *old = var_m;
                    var_m = tmp_assign_source_63;
                    Py_XDECREF( old );
                }

            }
            {
                PyObject *tmp_called_name_17;
                PyObject *tmp_source_name_24;
                PyObject *tmp_mvar_value_16;
                PyObject *tmp_call_result_8;
                PyObject *tmp_args_name_3;
                PyObject *tmp_tuple_element_5;
                PyObject *tmp_kw_name_3;
                PyObject *tmp_dict_key_3;
                PyObject *tmp_dict_value_3;
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_analysis );

                if (unlikely( tmp_mvar_value_16 == NULL ))
                {
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_analysis );
                }

                if ( tmp_mvar_value_16 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "analysis" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 139;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_19;
                }

                tmp_source_name_24 = tmp_mvar_value_16;
                tmp_called_name_17 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain_add );
                if ( tmp_called_name_17 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 139;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_19;
                }
                CHECK_OBJECT( var_parent_context );
                tmp_tuple_element_5 = var_parent_context;
                tmp_args_name_3 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_5 );
                PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_5 );
                tmp_tuple_element_5 = const_str_digest_f1edf9952adf39791783535a6530e3a6;
                Py_INCREF( tmp_tuple_element_5 );
                PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_5 );
                CHECK_OBJECT( var_node );
                tmp_tuple_element_5 = var_node;
                Py_INCREF( tmp_tuple_element_5 );
                PyTuple_SET_ITEM( tmp_args_name_3, 2, tmp_tuple_element_5 );
                tmp_dict_key_3 = const_str_plain_message;
                CHECK_OBJECT( var_m );
                tmp_dict_value_3 = var_m;
                tmp_kw_name_3 = _PyDict_NewPresized( 1 );
                tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_3, tmp_dict_value_3 );
                assert( !(tmp_res != 0) );
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 139;
                tmp_call_result_8 = CALL_FUNCTION( tmp_called_name_17, tmp_args_name_3, tmp_kw_name_3 );
                Py_DECREF( tmp_called_name_17 );
                Py_DECREF( tmp_args_name_3 );
                Py_DECREF( tmp_kw_name_3 );
                if ( tmp_call_result_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 139;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto try_except_handler_19;
                }
                Py_DECREF( tmp_call_result_8 );
            }
            if ( CONSIDER_THREADING() == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 137;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_19;
            }
            goto loop_start_8;
            loop_end_8:;
            goto try_end_17;
            // Exception handler code:
            try_except_handler_19:;
            exception_keeper_type_17 = exception_type;
            exception_keeper_value_17 = exception_value;
            exception_keeper_tb_17 = exception_tb;
            exception_keeper_lineno_17 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            Py_XDECREF( tmp_for_loop_7__iter_value );
            tmp_for_loop_7__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_7__for_iterator );
            Py_DECREF( tmp_for_loop_7__for_iterator );
            tmp_for_loop_7__for_iterator = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_17;
            exception_value = exception_keeper_value_17;
            exception_tb = exception_keeper_tb_17;
            exception_lineno = exception_keeper_lineno_17;

            goto try_except_handler_18;
            // End of try:
            try_end_17:;
            Py_XDECREF( tmp_for_loop_7__iter_value );
            tmp_for_loop_7__iter_value = NULL;

            CHECK_OBJECT( (PyObject *)tmp_for_loop_7__for_iterator );
            Py_DECREF( tmp_for_loop_7__for_iterator );
            tmp_for_loop_7__for_iterator = NULL;

            branch_no_15:;
        }
        if ( CONSIDER_THREADING() == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_18;
        }
        goto loop_start_7;
        loop_end_7:;
        goto try_end_18;
        // Exception handler code:
        try_except_handler_18:;
        exception_keeper_type_18 = exception_type;
        exception_keeper_value_18 = exception_value;
        exception_keeper_tb_18 = exception_tb;
        exception_keeper_lineno_18 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_for_loop_6__iter_value );
        tmp_for_loop_6__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_6__for_iterator );
        Py_DECREF( tmp_for_loop_6__for_iterator );
        tmp_for_loop_6__for_iterator = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_18;
        exception_value = exception_keeper_value_18;
        exception_tb = exception_keeper_tb_18;
        exception_lineno = exception_keeper_lineno_18;

        goto frame_exception_exit_1;
        // End of try:
        try_end_18:;
        Py_XDECREF( tmp_for_loop_6__iter_value );
        tmp_for_loop_6__iter_value = NULL;

        CHECK_OBJECT( (PyObject *)tmp_for_loop_6__for_iterator );
        Py_DECREF( tmp_for_loop_6__for_iterator );
        tmp_for_loop_6__for_iterator = NULL;

        branch_no_14:;
    }
    {
        PyObject *tmp_assign_source_64;
        PyObject *tmp_iter_arg_12;
        PyObject *tmp_called_instance_10;
        if ( var_non_matching_keys == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "non_matching_keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_10 = var_non_matching_keys;
        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 142;
        tmp_iter_arg_12 = CALL_METHOD_NO_ARGS( tmp_called_instance_10, const_str_plain_items );
        if ( tmp_iter_arg_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_64 = MAKE_ITERATOR( tmp_iter_arg_12 );
        Py_DECREF( tmp_iter_arg_12 );
        if ( tmp_assign_source_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_8__for_iterator == NULL );
        tmp_for_loop_8__for_iterator = tmp_assign_source_64;
    }
    // Tried code:
    loop_start_9:;
    {
        PyObject *tmp_next_source_8;
        PyObject *tmp_assign_source_65;
        CHECK_OBJECT( tmp_for_loop_8__for_iterator );
        tmp_next_source_8 = tmp_for_loop_8__for_iterator;
        tmp_assign_source_65 = ITERATOR_NEXT( tmp_next_source_8 );
        if ( tmp_assign_source_65 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_9;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooooooooooooooooooooo";
                exception_lineno = 142;
                goto try_except_handler_20;
            }
        }

        {
            PyObject *old = tmp_for_loop_8__iter_value;
            tmp_for_loop_8__iter_value = tmp_assign_source_65;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_66;
        PyObject *tmp_iter_arg_13;
        CHECK_OBJECT( tmp_for_loop_8__iter_value );
        tmp_iter_arg_13 = tmp_for_loop_8__iter_value;
        tmp_assign_source_66 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_13 );
        if ( tmp_assign_source_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_21;
        }
        {
            PyObject *old = tmp_tuple_unpack_4__source_iter;
            tmp_tuple_unpack_4__source_iter = tmp_assign_source_66;
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_67;
        PyObject *tmp_unpack_7;
        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
        tmp_unpack_7 = tmp_tuple_unpack_4__source_iter;
        tmp_assign_source_67 = UNPACK_NEXT( tmp_unpack_7, 0, 2 );
        if ( tmp_assign_source_67 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooo";
            exception_lineno = 142;
            goto try_except_handler_22;
        }
        {
            PyObject *old = tmp_tuple_unpack_4__element_1;
            tmp_tuple_unpack_4__element_1 = tmp_assign_source_67;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_68;
        PyObject *tmp_unpack_8;
        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
        tmp_unpack_8 = tmp_tuple_unpack_4__source_iter;
        tmp_assign_source_68 = UNPACK_NEXT( tmp_unpack_8, 1, 2 );
        if ( tmp_assign_source_68 == NULL )
        {
            if ( !ERROR_OCCURRED() )
            {
                exception_type = PyExc_StopIteration;
                Py_INCREF( exception_type );
                exception_value = NULL;
                exception_tb = NULL;
            }
            else
            {
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            }


            type_description_1 = "oooooooooooooooooooooooooo";
            exception_lineno = 142;
            goto try_except_handler_22;
        }
        {
            PyObject *old = tmp_tuple_unpack_4__element_2;
            tmp_tuple_unpack_4__element_2 = tmp_assign_source_68;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_iterator_name_4;
        CHECK_OBJECT( tmp_tuple_unpack_4__source_iter );
        tmp_iterator_name_4 = tmp_tuple_unpack_4__source_iter;
        // Check if iterator has left-over elements.
        CHECK_OBJECT( tmp_iterator_name_4 ); assert( HAS_ITERNEXT( tmp_iterator_name_4 ) );

        tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_4 )->tp_iternext)( tmp_iterator_name_4 );

        if (likely( tmp_iterator_attempt == NULL ))
        {
            PyObject *error = GET_ERROR_OCCURRED();

            if ( error != NULL )
            {
                if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                {
                    CLEAR_ERROR_OCCURRED();
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                    type_description_1 = "oooooooooooooooooooooooooo";
                    exception_lineno = 142;
                    goto try_except_handler_22;
                }
            }
        }
        else
        {
            Py_DECREF( tmp_iterator_attempt );

            // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
            PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
            PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

            type_description_1 = "oooooooooooooooooooooooooo";
            exception_lineno = 142;
            goto try_except_handler_22;
        }
    }
    goto try_end_19;
    // Exception handler code:
    try_except_handler_22:;
    exception_keeper_type_19 = exception_type;
    exception_keeper_value_19 = exception_value;
    exception_keeper_tb_19 = exception_tb;
    exception_keeper_lineno_19 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
    Py_DECREF( tmp_tuple_unpack_4__source_iter );
    tmp_tuple_unpack_4__source_iter = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_19;
    exception_value = exception_keeper_value_19;
    exception_tb = exception_keeper_tb_19;
    exception_lineno = exception_keeper_lineno_19;

    goto try_except_handler_21;
    // End of try:
    try_end_19:;
    goto try_end_20;
    // Exception handler code:
    try_except_handler_21:;
    exception_keeper_type_20 = exception_type;
    exception_keeper_value_20 = exception_value;
    exception_keeper_tb_20 = exception_tb;
    exception_keeper_lineno_20 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_tuple_unpack_4__element_1 );
    tmp_tuple_unpack_4__element_1 = NULL;

    Py_XDECREF( tmp_tuple_unpack_4__element_2 );
    tmp_tuple_unpack_4__element_2 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_20;
    exception_value = exception_keeper_value_20;
    exception_tb = exception_keeper_tb_20;
    exception_lineno = exception_keeper_lineno_20;

    goto try_except_handler_20;
    // End of try:
    try_end_20:;
    CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_4__source_iter );
    Py_DECREF( tmp_tuple_unpack_4__source_iter );
    tmp_tuple_unpack_4__source_iter = NULL;

    {
        PyObject *tmp_assign_source_69;
        CHECK_OBJECT( tmp_tuple_unpack_4__element_1 );
        tmp_assign_source_69 = tmp_tuple_unpack_4__element_1;
        {
            PyObject *old = var_key;
            var_key = tmp_assign_source_69;
            Py_INCREF( var_key );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_4__element_1 );
    tmp_tuple_unpack_4__element_1 = NULL;

    {
        PyObject *tmp_assign_source_70;
        CHECK_OBJECT( tmp_tuple_unpack_4__element_2 );
        tmp_assign_source_70 = tmp_tuple_unpack_4__element_2;
        {
            PyObject *old = var_lazy_context;
            var_lazy_context = tmp_assign_source_70;
            Py_INCREF( var_lazy_context );
            Py_XDECREF( old );
        }

    }
    Py_XDECREF( tmp_tuple_unpack_4__element_2 );
    tmp_tuple_unpack_4__element_2 = NULL;

    {
        PyObject *tmp_assign_source_71;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_tuple_element_6;
        PyObject *tmp_source_name_25;
        tmp_left_name_3 = const_str_digest_cb3b60b7344ba4c88cec50f7ecb6f6e3;
        CHECK_OBJECT( var_funcdef );
        tmp_source_name_25 = var_funcdef;
        tmp_tuple_element_6 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_name );
        if ( tmp_tuple_element_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_20;
        }
        tmp_right_name_3 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_6 );
        CHECK_OBJECT( var_key );
        tmp_tuple_element_6 = var_key;
        Py_INCREF( tmp_tuple_element_6 );
        PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_6 );
        tmp_assign_source_71 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_assign_source_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 143;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_20;
        }
        {
            PyObject *old = var_m;
            var_m = tmp_assign_source_71;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_call_result_9;
        PyObject *tmp_args_name_4;
        PyObject *tmp_tuple_element_7;
        PyObject *tmp_kw_name_4;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain__add_argument_issue );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__add_argument_issue );
        }

        if ( tmp_mvar_value_17 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_add_argument_issue" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 145;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_20;
        }

        tmp_called_name_18 = tmp_mvar_value_17;
        CHECK_OBJECT( var_parent_context );
        tmp_tuple_element_7 = var_parent_context;
        tmp_args_name_4 = PyTuple_New( 3 );
        Py_INCREF( tmp_tuple_element_7 );
        PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_7 );
        tmp_tuple_element_7 = const_str_digest_b49b5885202e0ab6a26adeea68b85343;
        Py_INCREF( tmp_tuple_element_7 );
        PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_7 );
        CHECK_OBJECT( var_lazy_context );
        tmp_tuple_element_7 = var_lazy_context;
        Py_INCREF( tmp_tuple_element_7 );
        PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_7 );
        tmp_dict_key_4 = const_str_plain_message;
        CHECK_OBJECT( var_m );
        tmp_dict_value_4 = var_m;
        tmp_kw_name_4 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 145;
        tmp_call_result_9 = CALL_FUNCTION( tmp_called_name_18, tmp_args_name_4, tmp_kw_name_4 );
        Py_DECREF( tmp_args_name_4 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_call_result_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 145;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto try_except_handler_20;
        }
        Py_DECREF( tmp_call_result_9 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 142;
        type_description_1 = "oooooooooooooooooooooooooo";
        goto try_except_handler_20;
    }
    goto loop_start_9;
    loop_end_9:;
    goto try_end_21;
    // Exception handler code:
    try_except_handler_20:;
    exception_keeper_type_21 = exception_type;
    exception_keeper_value_21 = exception_value;
    exception_keeper_tb_21 = exception_tb;
    exception_keeper_lineno_21 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_8__iter_value );
    tmp_for_loop_8__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_8__for_iterator );
    Py_DECREF( tmp_for_loop_8__for_iterator );
    tmp_for_loop_8__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_21;
    exception_value = exception_keeper_value_21;
    exception_tb = exception_keeper_tb_21;
    exception_lineno = exception_keeper_lineno_21;

    goto frame_exception_exit_1;
    // End of try:
    try_end_21:;
    Py_XDECREF( tmp_for_loop_8__iter_value );
    tmp_for_loop_8__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_8__for_iterator );
    Py_DECREF( tmp_for_loop_8__for_iterator );
    tmp_for_loop_8__for_iterator = NULL;

    {
        PyObject *tmp_assign_source_72;
        PyObject *tmp_list_arg_2;
        CHECK_OBJECT( var_var_arg_iterator );
        tmp_list_arg_2 = var_var_arg_iterator;
        tmp_assign_source_72 = PySequence_List( tmp_list_arg_2 );
        if ( tmp_assign_source_72 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "oooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_remaining_arguments == NULL );
        var_remaining_arguments = tmp_assign_source_72;
    }
    {
        nuitka_bool tmp_condition_result_16;
        int tmp_truth_name_4;
        CHECK_OBJECT( var_remaining_arguments );
        tmp_truth_name_4 = CHECK_IF_TRUE( var_remaining_arguments );
        assert( !(tmp_truth_name_4 == -1) );
        tmp_condition_result_16 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_16;
        }
        else
        {
            goto branch_no_16;
        }
        branch_yes_16:;
        {
            PyObject *tmp_assign_source_73;
            PyObject *tmp_called_name_19;
            PyObject *tmp_mvar_value_18;
            PyObject *tmp_args_element_name_28;
            PyObject *tmp_args_element_name_29;
            PyObject *tmp_len_arg_3;
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain__error_argument_count );

            if (unlikely( tmp_mvar_value_18 == NULL ))
            {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__error_argument_count );
            }

            if ( tmp_mvar_value_18 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_error_argument_count" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 154;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_19 = tmp_mvar_value_18;
            CHECK_OBJECT( var_funcdef );
            tmp_args_element_name_28 = var_funcdef;
            CHECK_OBJECT( var_unpacked_va );
            tmp_len_arg_3 = var_unpacked_va;
            tmp_args_element_name_29 = BUILTIN_LEN( tmp_len_arg_3 );
            assert( !(tmp_args_element_name_29 == NULL) );
            frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 154;
            {
                PyObject *call_args[] = { tmp_args_element_name_28, tmp_args_element_name_29 };
                tmp_assign_source_73 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_19, call_args );
            }

            Py_DECREF( tmp_args_element_name_29 );
            if ( tmp_assign_source_73 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 154;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_m;
                var_m = tmp_assign_source_73;
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            PyObject *tmp_assign_source_74;
            PyObject *tmp_iter_arg_14;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_subscript_name_2;
            CHECK_OBJECT( var_remaining_arguments );
            tmp_subscribed_name_2 = var_remaining_arguments;
            tmp_subscript_name_2 = const_int_0;
            tmp_iter_arg_14 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_2, tmp_subscript_name_2, 0 );
            if ( tmp_iter_arg_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_23;
            }
            tmp_assign_source_74 = MAKE_UNPACK_ITERATOR( tmp_iter_arg_14 );
            Py_DECREF( tmp_iter_arg_14 );
            if ( tmp_assign_source_74 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 157;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto try_except_handler_23;
            }
            assert( tmp_tuple_unpack_5__source_iter == NULL );
            tmp_tuple_unpack_5__source_iter = tmp_assign_source_74;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_75;
            PyObject *tmp_unpack_9;
            CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
            tmp_unpack_9 = tmp_tuple_unpack_5__source_iter;
            tmp_assign_source_75 = UNPACK_NEXT( tmp_unpack_9, 0, 2 );
            if ( tmp_assign_source_75 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooo";
                exception_lineno = 157;
                goto try_except_handler_24;
            }
            assert( tmp_tuple_unpack_5__element_1 == NULL );
            tmp_tuple_unpack_5__element_1 = tmp_assign_source_75;
        }
        {
            PyObject *tmp_assign_source_76;
            PyObject *tmp_unpack_10;
            CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
            tmp_unpack_10 = tmp_tuple_unpack_5__source_iter;
            tmp_assign_source_76 = UNPACK_NEXT( tmp_unpack_10, 1, 2 );
            if ( tmp_assign_source_76 == NULL )
            {
                if ( !ERROR_OCCURRED() )
                {
                    exception_type = PyExc_StopIteration;
                    Py_INCREF( exception_type );
                    exception_value = NULL;
                    exception_tb = NULL;
                }
                else
                {
                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                }


                type_description_1 = "oooooooooooooooooooooooooo";
                exception_lineno = 157;
                goto try_except_handler_24;
            }
            assert( tmp_tuple_unpack_5__element_2 == NULL );
            tmp_tuple_unpack_5__element_2 = tmp_assign_source_76;
        }
        {
            PyObject *tmp_iterator_name_5;
            CHECK_OBJECT( tmp_tuple_unpack_5__source_iter );
            tmp_iterator_name_5 = tmp_tuple_unpack_5__source_iter;
            // Check if iterator has left-over elements.
            CHECK_OBJECT( tmp_iterator_name_5 ); assert( HAS_ITERNEXT( tmp_iterator_name_5 ) );

            tmp_iterator_attempt = (*Py_TYPE( tmp_iterator_name_5 )->tp_iternext)( tmp_iterator_name_5 );

            if (likely( tmp_iterator_attempt == NULL ))
            {
                PyObject *error = GET_ERROR_OCCURRED();

                if ( error != NULL )
                {
                    if ( EXCEPTION_MATCH_BOOL_SINGLE( error, PyExc_StopIteration ))
                    {
                        CLEAR_ERROR_OCCURRED();
                    }
                    else
                    {
                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                        type_description_1 = "oooooooooooooooooooooooooo";
                        exception_lineno = 157;
                        goto try_except_handler_24;
                    }
                }
            }
            else
            {
                Py_DECREF( tmp_iterator_attempt );

                // TODO: Could avoid PyErr_Format.
#if PYTHON_VERSION < 300
                PyErr_Format( PyExc_ValueError, "too many values to unpack" );
#else
                PyErr_Format( PyExc_ValueError, "too many values to unpack (expected 2)" );
#endif
                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );

                type_description_1 = "oooooooooooooooooooooooooo";
                exception_lineno = 157;
                goto try_except_handler_24;
            }
        }
        goto try_end_22;
        // Exception handler code:
        try_except_handler_24:;
        exception_keeper_type_22 = exception_type;
        exception_keeper_value_22 = exception_value;
        exception_keeper_tb_22 = exception_tb;
        exception_keeper_lineno_22 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
        Py_DECREF( tmp_tuple_unpack_5__source_iter );
        tmp_tuple_unpack_5__source_iter = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_22;
        exception_value = exception_keeper_value_22;
        exception_tb = exception_keeper_tb_22;
        exception_lineno = exception_keeper_lineno_22;

        goto try_except_handler_23;
        // End of try:
        try_end_22:;
        goto try_end_23;
        // Exception handler code:
        try_except_handler_23:;
        exception_keeper_type_23 = exception_type;
        exception_keeper_value_23 = exception_value;
        exception_keeper_tb_23 = exception_tb;
        exception_keeper_lineno_23 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( tmp_tuple_unpack_5__element_1 );
        tmp_tuple_unpack_5__element_1 = NULL;

        Py_XDECREF( tmp_tuple_unpack_5__element_2 );
        tmp_tuple_unpack_5__element_2 = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_23;
        exception_value = exception_keeper_value_23;
        exception_tb = exception_keeper_tb_23;
        exception_lineno = exception_keeper_lineno_23;

        goto frame_exception_exit_1;
        // End of try:
        try_end_23:;
        CHECK_OBJECT( (PyObject *)tmp_tuple_unpack_5__source_iter );
        Py_DECREF( tmp_tuple_unpack_5__source_iter );
        tmp_tuple_unpack_5__source_iter = NULL;

        {
            PyObject *tmp_assign_source_77;
            CHECK_OBJECT( tmp_tuple_unpack_5__element_1 );
            tmp_assign_source_77 = tmp_tuple_unpack_5__element_1;
            assert( var_first_key == NULL );
            Py_INCREF( tmp_assign_source_77 );
            var_first_key = tmp_assign_source_77;
        }
        Py_XDECREF( tmp_tuple_unpack_5__element_1 );
        tmp_tuple_unpack_5__element_1 = NULL;

        {
            PyObject *tmp_assign_source_78;
            CHECK_OBJECT( tmp_tuple_unpack_5__element_2 );
            tmp_assign_source_78 = tmp_tuple_unpack_5__element_2;
            {
                PyObject *old = var_lazy_context;
                var_lazy_context = tmp_assign_source_78;
                Py_INCREF( var_lazy_context );
                Py_XDECREF( old );
            }

        }
        Py_XDECREF( tmp_tuple_unpack_5__element_2 );
        tmp_tuple_unpack_5__element_2 = NULL;

        {
            nuitka_bool tmp_condition_result_17;
            PyObject *tmp_called_instance_11;
            PyObject *tmp_call_result_10;
            int tmp_truth_name_5;
            CHECK_OBJECT( par_var_args );
            tmp_called_instance_11 = par_var_args;
            frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 158;
            tmp_call_result_10 = CALL_METHOD_NO_ARGS( tmp_called_instance_11, const_str_plain_get_calling_nodes );
            if ( tmp_call_result_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 158;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_5 = CHECK_IF_TRUE( tmp_call_result_10 );
            if ( tmp_truth_name_5 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_call_result_10 );

                exception_lineno = 158;
                type_description_1 = "oooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_17 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_call_result_10 );
            if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_17;
            }
            else
            {
                goto branch_no_17;
            }
            branch_yes_17:;
            {
                PyObject *tmp_called_name_20;
                PyObject *tmp_mvar_value_19;
                PyObject *tmp_call_result_11;
                PyObject *tmp_args_name_5;
                PyObject *tmp_tuple_element_8;
                PyObject *tmp_kw_name_5;
                PyObject *tmp_dict_key_5;
                PyObject *tmp_dict_value_5;
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain__add_argument_issue );

                if (unlikely( tmp_mvar_value_19 == NULL ))
                {
                    tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__add_argument_issue );
                }

                if ( tmp_mvar_value_19 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_add_argument_issue" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 160;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_20 = tmp_mvar_value_19;
                CHECK_OBJECT( var_parent_context );
                tmp_tuple_element_8 = var_parent_context;
                tmp_args_name_5 = PyTuple_New( 3 );
                Py_INCREF( tmp_tuple_element_8 );
                PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_8 );
                tmp_tuple_element_8 = const_str_digest_46f532b0e491ef8ae810afebc89c9807;
                Py_INCREF( tmp_tuple_element_8 );
                PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_8 );
                CHECK_OBJECT( var_lazy_context );
                tmp_tuple_element_8 = var_lazy_context;
                Py_INCREF( tmp_tuple_element_8 );
                PyTuple_SET_ITEM( tmp_args_name_5, 2, tmp_tuple_element_8 );
                tmp_dict_key_5 = const_str_plain_message;
                CHECK_OBJECT( var_m );
                tmp_dict_value_5 = var_m;
                tmp_kw_name_5 = _PyDict_NewPresized( 1 );
                tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_5, tmp_dict_value_5 );
                assert( !(tmp_res != 0) );
                frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame.f_lineno = 160;
                tmp_call_result_11 = CALL_FUNCTION( tmp_called_name_20, tmp_args_name_5, tmp_kw_name_5 );
                Py_DECREF( tmp_args_name_5 );
                Py_DECREF( tmp_kw_name_5 );
                if ( tmp_call_result_11 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 160;
                    type_description_1 = "oooooooooooooooooooooooooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_11 );
            }
            branch_no_17:;
        }
        branch_no_16:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c9150d2e8299e357a752bb5c8ba8c265 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c9150d2e8299e357a752bb5c8ba8c265 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c9150d2e8299e357a752bb5c8ba8c265, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c9150d2e8299e357a752bb5c8ba8c265->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c9150d2e8299e357a752bb5c8ba8c265, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c9150d2e8299e357a752bb5c8ba8c265,
        type_description_1,
        par_execution_context,
        par_var_args,
        var_result_params,
        var_param_dict,
        var_funcdef,
        var_parent_context,
        var_param,
        var_unpacked_va,
        var_var_arg_iterator,
        var_non_matching_keys,
        var_keys_used,
        var_keys_only,
        var_had_multiple_value_error,
        var_key,
        var_argument,
        var_key_param,
        var_m,
        var_node,
        var_lazy_context_list,
        var_seq,
        var_result_arg,
        var_dct,
        var_k,
        var_lazy_context,
        var_remaining_arguments,
        var_first_key
    );


    // Release cached frame.
    if ( frame_c9150d2e8299e357a752bb5c8ba8c265 == cache_frame_c9150d2e8299e357a752bb5c8ba8c265 )
    {
        Py_DECREF( frame_c9150d2e8299e357a752bb5c8ba8c265 );
    }
    cache_frame_c9150d2e8299e357a752bb5c8ba8c265 = NULL;

    assertFrameObject( frame_c9150d2e8299e357a752bb5c8ba8c265 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_result_params );
    tmp_return_value = var_result_params;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_6_get_executed_params );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    CHECK_OBJECT( (PyObject *)par_var_args );
    Py_DECREF( par_var_args );
    par_var_args = NULL;

    CHECK_OBJECT( (PyObject *)var_result_params );
    Py_DECREF( var_result_params );
    var_result_params = NULL;

    CHECK_OBJECT( (PyObject *)var_param_dict );
    Py_DECREF( var_param_dict );
    var_param_dict = NULL;

    CHECK_OBJECT( (PyObject *)var_funcdef );
    Py_DECREF( var_funcdef );
    var_funcdef = NULL;

    CHECK_OBJECT( (PyObject *)var_parent_context );
    Py_DECREF( var_parent_context );
    var_parent_context = NULL;

    Py_XDECREF( var_param );
    var_param = NULL;

    CHECK_OBJECT( (PyObject *)var_unpacked_va );
    Py_DECREF( var_unpacked_va );
    var_unpacked_va = NULL;

    CHECK_OBJECT( (PyObject *)var_var_arg_iterator );
    Py_DECREF( var_var_arg_iterator );
    var_var_arg_iterator = NULL;

    Py_XDECREF( var_non_matching_keys );
    var_non_matching_keys = NULL;

    CHECK_OBJECT( (PyObject *)var_keys_used );
    Py_DECREF( var_keys_used );
    var_keys_used = NULL;

    CHECK_OBJECT( (PyObject *)var_keys_only );
    Py_DECREF( var_keys_only );
    var_keys_only = NULL;

    Py_XDECREF( var_had_multiple_value_error );
    var_had_multiple_value_error = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_argument );
    var_argument = NULL;

    Py_XDECREF( var_key_param );
    var_key_param = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_node );
    var_node = NULL;

    Py_XDECREF( var_lazy_context_list );
    var_lazy_context_list = NULL;

    Py_XDECREF( var_seq );
    var_seq = NULL;

    Py_XDECREF( var_result_arg );
    var_result_arg = NULL;

    Py_XDECREF( var_dct );
    var_dct = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    CHECK_OBJECT( (PyObject *)var_remaining_arguments );
    Py_DECREF( var_remaining_arguments );
    var_remaining_arguments = NULL;

    Py_XDECREF( var_first_key );
    var_first_key = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_24 = exception_type;
    exception_keeper_value_24 = exception_value;
    exception_keeper_tb_24 = exception_tb;
    exception_keeper_lineno_24 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    CHECK_OBJECT( (PyObject *)par_var_args );
    Py_DECREF( par_var_args );
    par_var_args = NULL;

    CHECK_OBJECT( (PyObject *)var_result_params );
    Py_DECREF( var_result_params );
    var_result_params = NULL;

    CHECK_OBJECT( (PyObject *)var_param_dict );
    Py_DECREF( var_param_dict );
    var_param_dict = NULL;

    Py_XDECREF( var_funcdef );
    var_funcdef = NULL;

    Py_XDECREF( var_parent_context );
    var_parent_context = NULL;

    Py_XDECREF( var_param );
    var_param = NULL;

    Py_XDECREF( var_unpacked_va );
    var_unpacked_va = NULL;

    Py_XDECREF( var_var_arg_iterator );
    var_var_arg_iterator = NULL;

    Py_XDECREF( var_non_matching_keys );
    var_non_matching_keys = NULL;

    Py_XDECREF( var_keys_used );
    var_keys_used = NULL;

    Py_XDECREF( var_keys_only );
    var_keys_only = NULL;

    Py_XDECREF( var_had_multiple_value_error );
    var_had_multiple_value_error = NULL;

    Py_XDECREF( var_key );
    var_key = NULL;

    Py_XDECREF( var_argument );
    var_argument = NULL;

    Py_XDECREF( var_key_param );
    var_key_param = NULL;

    Py_XDECREF( var_m );
    var_m = NULL;

    Py_XDECREF( var_node );
    var_node = NULL;

    Py_XDECREF( var_lazy_context_list );
    var_lazy_context_list = NULL;

    Py_XDECREF( var_seq );
    var_seq = NULL;

    Py_XDECREF( var_result_arg );
    var_result_arg = NULL;

    Py_XDECREF( var_dct );
    var_dct = NULL;

    Py_XDECREF( var_k );
    var_k = NULL;

    Py_XDECREF( var_lazy_context );
    var_lazy_context = NULL;

    Py_XDECREF( var_remaining_arguments );
    var_remaining_arguments = NULL;

    Py_XDECREF( var_first_key );
    var_first_key = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_24;
    exception_value = exception_keeper_value_24;
    exception_tb = exception_keeper_tb_24;
    exception_lineno = exception_keeper_lineno_24;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_6_get_executed_params );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$param$$$function_6_get_executed_params$$$function_1_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = PyList_New( 0 );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_6_get_executed_params$$$function_1_lambda );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$param$$$function_7__error_argument_count( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_funcdef = python_pars[ 0 ];
    PyObject *par_actual_count = python_pars[ 1 ];
    PyObject *var_params = NULL;
    PyObject *var_default_arguments = NULL;
    PyObject *var_before = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_34c427d9c4bb8dbbc2ed7daafa7fa16a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_34c427d9c4bb8dbbc2ed7daafa7fa16a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_34c427d9c4bb8dbbc2ed7daafa7fa16a, codeobj_34c427d9c4bb8dbbc2ed7daafa7fa16a, module_jedi$evaluate$param, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_34c427d9c4bb8dbbc2ed7daafa7fa16a = cache_frame_34c427d9c4bb8dbbc2ed7daafa7fa16a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_34c427d9c4bb8dbbc2ed7daafa7fa16a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_34c427d9c4bb8dbbc2ed7daafa7fa16a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_funcdef );
        tmp_called_instance_1 = par_funcdef;
        frame_34c427d9c4bb8dbbc2ed7daafa7fa16a->m_frame.f_lineno = 165;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_params );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 165;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_params == NULL );
        var_params = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_sum_sequence_1;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_iter_arg_1;
            CHECK_OBJECT( var_params );
            tmp_iter_arg_1 = var_params;
            tmp_assign_source_3 = MAKE_ITERATOR( tmp_iter_arg_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 166;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_3;
        }
        // Tried code:
        tmp_sum_sequence_1 = jedi$evaluate$param$$$function_7__error_argument_count$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_sum_sequence_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_7__error_argument_count );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_7__error_argument_count );
        return NULL;
        outline_result_1:;
        tmp_assign_source_2 = BUILTIN_SUM1( tmp_sum_sequence_1 );
        Py_DECREF( tmp_sum_sequence_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_default_arguments == NULL );
        var_default_arguments = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_default_arguments );
        tmp_compexpr_left_1 = var_default_arguments;
        tmp_compexpr_right_1 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_4;
            tmp_assign_source_4 = const_str_digest_da514e395925ccdc8d9efb373cdf2394;
            assert( var_before == NULL );
            Py_INCREF( tmp_assign_source_4 );
            var_before = tmp_assign_source_4;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_5;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            PyObject *tmp_left_name_2;
            PyObject *tmp_len_arg_1;
            PyObject *tmp_right_name_2;
            tmp_left_name_1 = const_str_digest_8a7edfc928fd9c2fdc576a6d3471afeb;
            CHECK_OBJECT( var_params );
            tmp_len_arg_1 = var_params;
            tmp_left_name_2 = BUILTIN_LEN( tmp_len_arg_1 );
            if ( tmp_left_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_default_arguments );
            tmp_right_name_2 = var_default_arguments;
            tmp_right_name_1 = BINARY_OPERATION_SUB_LONG_OBJECT( tmp_left_name_2, tmp_right_name_2 );
            Py_DECREF( tmp_left_name_2 );
            if ( tmp_right_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            Py_DECREF( tmp_right_name_1 );
            if ( tmp_assign_source_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 171;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_before == NULL );
            var_before = tmp_assign_source_5;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_len_arg_2;
        tmp_left_name_3 = const_str_digest_98012501ffa0c54f4941a1f51b78b07e;
        CHECK_OBJECT( par_funcdef );
        tmp_source_name_1 = par_funcdef;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_name );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 173;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_3 = PyTuple_New( 4 );
        PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_before );
        tmp_tuple_element_1 = var_before;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( var_params );
        tmp_len_arg_2 = var_params;
        tmp_tuple_element_1 = BUILTIN_LEN( tmp_len_arg_2 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_3 );

            exception_lineno = 173;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_3, 2, tmp_tuple_element_1 );
        CHECK_OBJECT( par_actual_count );
        tmp_tuple_element_1 = par_actual_count;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_3, 3, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
        Py_DECREF( tmp_right_name_3 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_34c427d9c4bb8dbbc2ed7daafa7fa16a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_34c427d9c4bb8dbbc2ed7daafa7fa16a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_34c427d9c4bb8dbbc2ed7daafa7fa16a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_34c427d9c4bb8dbbc2ed7daafa7fa16a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_34c427d9c4bb8dbbc2ed7daafa7fa16a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_34c427d9c4bb8dbbc2ed7daafa7fa16a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_34c427d9c4bb8dbbc2ed7daafa7fa16a,
        type_description_1,
        par_funcdef,
        par_actual_count,
        var_params,
        var_default_arguments,
        var_before
    );


    // Release cached frame.
    if ( frame_34c427d9c4bb8dbbc2ed7daafa7fa16a == cache_frame_34c427d9c4bb8dbbc2ed7daafa7fa16a )
    {
        Py_DECREF( frame_34c427d9c4bb8dbbc2ed7daafa7fa16a );
    }
    cache_frame_34c427d9c4bb8dbbc2ed7daafa7fa16a = NULL;

    assertFrameObject( frame_34c427d9c4bb8dbbc2ed7daafa7fa16a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_7__error_argument_count );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    CHECK_OBJECT( (PyObject *)par_actual_count );
    Py_DECREF( par_actual_count );
    par_actual_count = NULL;

    CHECK_OBJECT( (PyObject *)var_params );
    Py_DECREF( var_params );
    var_params = NULL;

    CHECK_OBJECT( (PyObject *)var_default_arguments );
    Py_DECREF( var_default_arguments );
    var_default_arguments = NULL;

    CHECK_OBJECT( (PyObject *)var_before );
    Py_DECREF( var_before );
    var_before = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    CHECK_OBJECT( (PyObject *)par_actual_count );
    Py_DECREF( par_actual_count );
    par_actual_count = NULL;

    Py_XDECREF( var_params );
    var_params = NULL;

    Py_XDECREF( var_default_arguments );
    var_default_arguments = NULL;

    Py_XDECREF( var_before );
    var_before = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_7__error_argument_count );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct jedi$evaluate$param$$$function_7__error_argument_count$$$genexpr_1_genexpr_locals {
    PyObject *var_p;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *jedi$evaluate$param$$$function_7__error_argument_count$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct jedi$evaluate$param$$$function_7__error_argument_count$$$genexpr_1_genexpr_locals *generator_heap = (struct jedi$evaluate$param$$$function_7__error_argument_count$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_p = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_1886a4bdb2d0224302a6673c87c076b1, module_jedi$evaluate$param, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 166;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_p;
            generator_heap->var_p = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_p );
            Py_XDECREF( old );
        }

    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_or_left_truth_1;
        nuitka_bool tmp_or_left_value_1;
        nuitka_bool tmp_or_right_value_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_attribute_value_2;
        int tmp_truth_name_2;
        CHECK_OBJECT( generator_heap->var_p );
        tmp_source_name_1 = generator_heap->var_p;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_default );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 166;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            generator_heap->exception_lineno = 166;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_or_left_value_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        CHECK_OBJECT( generator_heap->var_p );
        tmp_source_name_2 = generator_heap->var_p;
        tmp_attribute_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_star_count );
        if ( tmp_attribute_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 166;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
            Py_DECREF( tmp_attribute_value_2 );

            generator_heap->exception_lineno = 166;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_or_right_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_2 );
        tmp_condition_result_1 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        tmp_condition_result_1 = tmp_or_left_value_1;
        or_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_expression_name_1;
            NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
            tmp_expression_name_1 = const_int_pos_1;
            Py_INCREF( tmp_expression_name_1 );
            Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_or_left_truth_1, sizeof(int), &tmp_or_left_value_1, sizeof(nuitka_bool), &tmp_or_right_value_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_value_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_source_name_2, sizeof(PyObject *), &tmp_attribute_value_2, sizeof(PyObject *), &tmp_truth_name_2, sizeof(int), NULL );
            generator->m_yield_return_index = 1;
            return tmp_expression_name_1;
            yield_return_1:
            Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_condition_result_1, sizeof(nuitka_bool), &tmp_or_left_truth_1, sizeof(int), &tmp_or_left_value_1, sizeof(nuitka_bool), &tmp_or_right_value_1, sizeof(nuitka_bool), &tmp_source_name_1, sizeof(PyObject *), &tmp_attribute_value_1, sizeof(PyObject *), &tmp_truth_name_1, sizeof(int), &tmp_source_name_2, sizeof(PyObject *), &tmp_attribute_value_2, sizeof(PyObject *), &tmp_truth_name_2, sizeof(int), NULL );
            if ( yield_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


                generator_heap->exception_lineno = 166;
                generator_heap->type_description_1 = "No";
                goto try_except_handler_2;
            }
            tmp_yield_result_1 = yield_return_value;
        }
        branch_no_1:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 166;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_p
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_p );
    generator_heap->var_p = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_p );
    generator_heap->var_p = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *jedi$evaluate$param$$$function_7__error_argument_count$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        jedi$evaluate$param$$$function_7__error_argument_count$$$genexpr_1_genexpr_context,
        module_jedi$evaluate$param,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_d0546682c6fc69fd38514cddf9143ae2,
#endif
        codeobj_1886a4bdb2d0224302a6673c87c076b1,
        1,
        sizeof(struct jedi$evaluate$param$$$function_7__error_argument_count$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_jedi$evaluate$param$$$function_8__create_default_param( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_execution_context = python_pars[ 0 ];
    PyObject *par_param = python_pars[ 1 ];
    PyObject *var_result_arg = NULL;
    struct Nuitka_FrameObject *frame_105bad8ab37151945c4a28a9a55508c2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_105bad8ab37151945c4a28a9a55508c2 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_105bad8ab37151945c4a28a9a55508c2, codeobj_105bad8ab37151945c4a28a9a55508c2, module_jedi$evaluate$param, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_105bad8ab37151945c4a28a9a55508c2 = cache_frame_105bad8ab37151945c4a28a9a55508c2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_105bad8ab37151945c4a28a9a55508c2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_105bad8ab37151945c4a28a9a55508c2 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_param );
        tmp_source_name_1 = par_param;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_star_count );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_args_element_name_4;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyKnownContext );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyKnownContext );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyKnownContext" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 178;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_called_name_1 = tmp_mvar_value_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_iterable );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iterable );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iterable" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 179;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_2 = tmp_mvar_value_2;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_FakeSequence );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 179;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_execution_context );
            tmp_source_name_3 = par_execution_context;
            tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_evaluator );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 179;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_3 = const_str_plain_tuple;
            tmp_args_element_name_4 = PyList_New( 0 );
            frame_105bad8ab37151945c4a28a9a55508c2->m_frame.f_lineno = 179;
            {
                PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3, tmp_args_element_name_4 };
                tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_2 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 179;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_105bad8ab37151945c4a28a9a55508c2->m_frame.f_lineno = 178;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 178;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( var_result_arg == NULL );
            var_result_arg = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_param );
            tmp_source_name_4 = par_param;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_star_count );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 181;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_int_pos_2;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 181;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_2;
                PyObject *tmp_called_name_3;
                PyObject *tmp_mvar_value_3;
                PyObject *tmp_args_element_name_5;
                PyObject *tmp_called_name_4;
                PyObject *tmp_source_name_5;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_args_element_name_6;
                PyObject *tmp_source_name_6;
                PyObject *tmp_args_element_name_7;
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyKnownContext );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyKnownContext );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyKnownContext" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 182;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_3 = tmp_mvar_value_3;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_iterable );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_iterable );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "iterable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 183;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }

                tmp_source_name_5 = tmp_mvar_value_4;
                tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_FakeDict );
                if ( tmp_called_name_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 183;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_execution_context );
                tmp_source_name_6 = par_execution_context;
                tmp_args_element_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_evaluator );
                if ( tmp_args_element_name_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_4 );

                    exception_lineno = 183;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                tmp_args_element_name_7 = PyDict_New();
                frame_105bad8ab37151945c4a28a9a55508c2->m_frame.f_lineno = 183;
                {
                    PyObject *call_args[] = { tmp_args_element_name_6, tmp_args_element_name_7 };
                    tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
                }

                Py_DECREF( tmp_called_name_4 );
                Py_DECREF( tmp_args_element_name_6 );
                Py_DECREF( tmp_args_element_name_7 );
                if ( tmp_args_element_name_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 183;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                frame_105bad8ab37151945c4a28a9a55508c2->m_frame.f_lineno = 182;
                {
                    PyObject *call_args[] = { tmp_args_element_name_5 };
                    tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
                }

                Py_DECREF( tmp_args_element_name_5 );
                if ( tmp_assign_source_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 182;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                assert( var_result_arg == NULL );
                var_result_arg = tmp_assign_source_2;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                nuitka_bool tmp_condition_result_3;
                PyObject *tmp_compexpr_left_3;
                PyObject *tmp_compexpr_right_3;
                PyObject *tmp_source_name_7;
                CHECK_OBJECT( par_param );
                tmp_source_name_7 = par_param;
                tmp_compexpr_left_3 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_default );
                if ( tmp_compexpr_left_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 185;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                tmp_compexpr_right_3 = Py_None;
                tmp_condition_result_3 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
                Py_DECREF( tmp_compexpr_left_3 );
                if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
                {
                    goto branch_yes_3;
                }
                else
                {
                    goto branch_no_3;
                }
                branch_yes_3:;
                {
                    PyObject *tmp_assign_source_3;
                    PyObject *tmp_called_name_5;
                    PyObject *tmp_mvar_value_5;
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyUnknownContext );

                    if (unlikely( tmp_mvar_value_5 == NULL ))
                    {
                        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyUnknownContext );
                    }

                    if ( tmp_mvar_value_5 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyUnknownContext" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 186;
                        type_description_1 = "ooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_5 = tmp_mvar_value_5;
                    frame_105bad8ab37151945c4a28a9a55508c2->m_frame.f_lineno = 186;
                    tmp_assign_source_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_5 );
                    if ( tmp_assign_source_3 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 186;
                        type_description_1 = "ooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_result_arg == NULL );
                    var_result_arg = tmp_assign_source_3;
                }
                goto branch_end_3;
                branch_no_3:;
                {
                    PyObject *tmp_assign_source_4;
                    PyObject *tmp_called_name_6;
                    PyObject *tmp_mvar_value_6;
                    PyObject *tmp_args_element_name_8;
                    PyObject *tmp_source_name_8;
                    PyObject *tmp_args_element_name_9;
                    PyObject *tmp_source_name_9;
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );

                    if (unlikely( tmp_mvar_value_6 == NULL ))
                    {
                        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_LazyTreeContext );
                    }

                    if ( tmp_mvar_value_6 == NULL )
                    {

                        exception_type = PyExc_NameError;
                        Py_INCREF( exception_type );
                        exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "LazyTreeContext" );
                        exception_tb = NULL;
                        NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                        CHAIN_EXCEPTION( exception_value );

                        exception_lineno = 188;
                        type_description_1 = "ooo";
                        goto frame_exception_exit_1;
                    }

                    tmp_called_name_6 = tmp_mvar_value_6;
                    CHECK_OBJECT( par_execution_context );
                    tmp_source_name_8 = par_execution_context;
                    tmp_args_element_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_parent_context );
                    if ( tmp_args_element_name_8 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 188;
                        type_description_1 = "ooo";
                        goto frame_exception_exit_1;
                    }
                    CHECK_OBJECT( par_param );
                    tmp_source_name_9 = par_param;
                    tmp_args_element_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_default );
                    if ( tmp_args_element_name_9 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                        Py_DECREF( tmp_args_element_name_8 );

                        exception_lineno = 188;
                        type_description_1 = "ooo";
                        goto frame_exception_exit_1;
                    }
                    frame_105bad8ab37151945c4a28a9a55508c2->m_frame.f_lineno = 188;
                    {
                        PyObject *call_args[] = { tmp_args_element_name_8, tmp_args_element_name_9 };
                        tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_6, call_args );
                    }

                    Py_DECREF( tmp_args_element_name_8 );
                    Py_DECREF( tmp_args_element_name_9 );
                    if ( tmp_assign_source_4 == NULL )
                    {
                        assert( ERROR_OCCURRED() );

                        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                        exception_lineno = 188;
                        type_description_1 = "ooo";
                        goto frame_exception_exit_1;
                    }
                    assert( var_result_arg == NULL );
                    var_result_arg = tmp_assign_source_4;
                }
                branch_end_3:;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_called_name_7;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_args_element_name_12;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_ExecutedParam );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ExecutedParam );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ExecutedParam" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 189;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_7 = tmp_mvar_value_7;
        CHECK_OBJECT( par_execution_context );
        tmp_args_element_name_10 = par_execution_context;
        CHECK_OBJECT( par_param );
        tmp_args_element_name_11 = par_param;
        if ( var_result_arg == NULL )
        {

            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "result_arg" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 189;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_12 = var_result_arg;
        frame_105bad8ab37151945c4a28a9a55508c2->m_frame.f_lineno = 189;
        {
            PyObject *call_args[] = { tmp_args_element_name_10, tmp_args_element_name_11, tmp_args_element_name_12 };
            tmp_return_value = CALL_FUNCTION_WITH_ARGS3( tmp_called_name_7, call_args );
        }

        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 189;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_105bad8ab37151945c4a28a9a55508c2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_105bad8ab37151945c4a28a9a55508c2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_105bad8ab37151945c4a28a9a55508c2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_105bad8ab37151945c4a28a9a55508c2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_105bad8ab37151945c4a28a9a55508c2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_105bad8ab37151945c4a28a9a55508c2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_105bad8ab37151945c4a28a9a55508c2,
        type_description_1,
        par_execution_context,
        par_param,
        var_result_arg
    );


    // Release cached frame.
    if ( frame_105bad8ab37151945c4a28a9a55508c2 == cache_frame_105bad8ab37151945c4a28a9a55508c2 )
    {
        Py_DECREF( frame_105bad8ab37151945c4a28a9a55508c2 );
    }
    cache_frame_105bad8ab37151945c4a28a9a55508c2 = NULL;

    assertFrameObject( frame_105bad8ab37151945c4a28a9a55508c2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_8__create_default_param );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    CHECK_OBJECT( (PyObject *)par_param );
    Py_DECREF( par_param );
    par_param = NULL;

    Py_XDECREF( var_result_arg );
    var_result_arg = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    CHECK_OBJECT( (PyObject *)par_param );
    Py_DECREF( par_param );
    par_param = NULL;

    Py_XDECREF( var_result_arg );
    var_result_arg = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_8__create_default_param );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_jedi$evaluate$param$$$function_9_create_default_params( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_execution_context = python_pars[ 0 ];
    PyObject *par_funcdef = python_pars[ 1 ];
    PyObject *outline_0_var_p = NULL;
    PyObject *tmp_listcomp_1__$0 = NULL;
    PyObject *tmp_listcomp_1__contraction = NULL;
    PyObject *tmp_listcomp_1__iter_value_0 = NULL;
    struct Nuitka_FrameObject *frame_27cbe2b1328e9129a03855b4e485f0bd;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    struct Nuitka_FrameObject *frame_2a34d259845e388eab25e74ab42d09b0_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    int tmp_res;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_2a34d259845e388eab25e74ab42d09b0_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_27cbe2b1328e9129a03855b4e485f0bd = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_27cbe2b1328e9129a03855b4e485f0bd, codeobj_27cbe2b1328e9129a03855b4e485f0bd, module_jedi$evaluate$param, sizeof(void *)+sizeof(void *) );
    frame_27cbe2b1328e9129a03855b4e485f0bd = cache_frame_27cbe2b1328e9129a03855b4e485f0bd;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_27cbe2b1328e9129a03855b4e485f0bd );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_27cbe2b1328e9129a03855b4e485f0bd ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( par_funcdef );
        tmp_called_instance_1 = par_funcdef;
        frame_27cbe2b1328e9129a03855b4e485f0bd->m_frame.f_lineno = 194;
        tmp_iter_arg_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_params );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_listcomp_1__$0 == NULL );
        tmp_listcomp_1__$0 = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = PyList_New( 0 );
        assert( tmp_listcomp_1__contraction == NULL );
        tmp_listcomp_1__contraction = tmp_assign_source_2;
    }
    MAKE_OR_REUSE_FRAME( cache_frame_2a34d259845e388eab25e74ab42d09b0_2, codeobj_2a34d259845e388eab25e74ab42d09b0, module_jedi$evaluate$param, sizeof(void *)+sizeof(void *) );
    frame_2a34d259845e388eab25e74ab42d09b0_2 = cache_frame_2a34d259845e388eab25e74ab42d09b0_2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2a34d259845e388eab25e74ab42d09b0_2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2a34d259845e388eab25e74ab42d09b0_2 ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_listcomp_1__$0 );
        tmp_next_source_1 = tmp_listcomp_1__$0;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_2 = "oo";
                exception_lineno = 193;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_listcomp_1__iter_value_0;
            tmp_listcomp_1__iter_value_0 = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_listcomp_1__iter_value_0 );
        tmp_assign_source_4 = tmp_listcomp_1__iter_value_0;
        {
            PyObject *old = outline_0_var_p;
            outline_0_var_p = tmp_assign_source_4;
            Py_INCREF( outline_0_var_p );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_append_list_1;
        PyObject *tmp_append_value_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        CHECK_OBJECT( tmp_listcomp_1__contraction );
        tmp_append_list_1 = tmp_listcomp_1__contraction;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain__create_default_param );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__create_default_param );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_create_default_param" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 193;
            type_description_2 = "oo";
            goto try_except_handler_3;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_execution_context );
        tmp_args_element_name_1 = par_execution_context;
        CHECK_OBJECT( outline_0_var_p );
        tmp_args_element_name_2 = outline_0_var_p;
        frame_2a34d259845e388eab25e74ab42d09b0_2->m_frame.f_lineno = 193;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_append_value_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
        }

        if ( tmp_append_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_2 = "oo";
            goto try_except_handler_3;
        }
        assert( PyList_Check( tmp_append_list_1 ) );
        tmp_res = PyList_Append( tmp_append_list_1, tmp_append_value_1 );
        Py_DECREF( tmp_append_value_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 193;
            type_description_2 = "oo";
            goto try_except_handler_3;
        }
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 193;
        type_description_2 = "oo";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    CHECK_OBJECT( tmp_listcomp_1__contraction );
    tmp_return_value = tmp_listcomp_1__contraction;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_3;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_9_create_default_params );
    return NULL;
    // Return handler code:
    try_return_handler_3:;
    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    goto frame_return_exit_2;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__$0 );
    Py_DECREF( tmp_listcomp_1__$0 );
    tmp_listcomp_1__$0 = NULL;

    CHECK_OBJECT( (PyObject *)tmp_listcomp_1__contraction );
    Py_DECREF( tmp_listcomp_1__contraction );
    tmp_listcomp_1__contraction = NULL;

    Py_XDECREF( tmp_listcomp_1__iter_value_0 );
    tmp_listcomp_1__iter_value_0 = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_2;
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a34d259845e388eab25e74ab42d09b0_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_2:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a34d259845e388eab25e74ab42d09b0_2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_2;

    frame_exception_exit_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2a34d259845e388eab25e74ab42d09b0_2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2a34d259845e388eab25e74ab42d09b0_2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2a34d259845e388eab25e74ab42d09b0_2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2a34d259845e388eab25e74ab42d09b0_2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2a34d259845e388eab25e74ab42d09b0_2,
        type_description_2,
        outline_0_var_p,
        par_execution_context
    );


    // Release cached frame.
    if ( frame_2a34d259845e388eab25e74ab42d09b0_2 == cache_frame_2a34d259845e388eab25e74ab42d09b0_2 )
    {
        Py_DECREF( frame_2a34d259845e388eab25e74ab42d09b0_2 );
    }
    cache_frame_2a34d259845e388eab25e74ab42d09b0_2 = NULL;

    assertFrameObject( frame_2a34d259845e388eab25e74ab42d09b0_2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto nested_frame_exit_1;

    frame_no_exception_1:;
    goto skip_nested_handling_1;
    nested_frame_exit_1:;
    type_description_1 = "oo";
    goto try_except_handler_2;
    skip_nested_handling_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_9_create_default_params );
    return NULL;
    // Return handler code:
    try_return_handler_2:;
    Py_XDECREF( outline_0_var_p );
    outline_0_var_p = NULL;

    goto outline_result_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( outline_0_var_p );
    outline_0_var_p = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto outline_exception_1;
    // End of try:
    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_9_create_default_params );
    return NULL;
    outline_exception_1:;
    exception_lineno = 193;
    goto frame_exception_exit_1;
    outline_result_1:;
    goto frame_return_exit_1;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27cbe2b1328e9129a03855b4e485f0bd );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_2;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_27cbe2b1328e9129a03855b4e485f0bd );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_27cbe2b1328e9129a03855b4e485f0bd );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_27cbe2b1328e9129a03855b4e485f0bd, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_27cbe2b1328e9129a03855b4e485f0bd->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_27cbe2b1328e9129a03855b4e485f0bd, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_27cbe2b1328e9129a03855b4e485f0bd,
        type_description_1,
        par_execution_context,
        par_funcdef
    );


    // Release cached frame.
    if ( frame_27cbe2b1328e9129a03855b4e485f0bd == cache_frame_27cbe2b1328e9129a03855b4e485f0bd )
    {
        Py_DECREF( frame_27cbe2b1328e9129a03855b4e485f0bd );
    }
    cache_frame_27cbe2b1328e9129a03855b4e485f0bd = NULL;

    assertFrameObject( frame_27cbe2b1328e9129a03855b4e485f0bd );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_2:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_9_create_default_params );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_execution_context );
    Py_DECREF( par_execution_context );
    par_execution_context = NULL;

    CHECK_OBJECT( (PyObject *)par_funcdef );
    Py_DECREF( par_funcdef );
    par_funcdef = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( jedi$evaluate$param$$$function_9_create_default_params );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_1__add_argument_issue(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$param$$$function_1__add_argument_issue,
        const_str_plain__add_argument_issue,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_cc969c9ab99669eea55a3ca9936be151,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$param,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_2___init__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$param$$$function_2___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_69f03acf45a5dfb1e854cc8195a90a7f,
#endif
        codeobj_2870d48c4315235ce0207c76dc33fe48,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$param,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_3_infer(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$param$$$function_3_infer,
        const_str_plain_infer,
#if PYTHON_VERSION >= 300
        const_str_digest_4a42bcd377c5a3697d3b045032591a82,
#endif
        codeobj_e5b113fb49adc89c901900020b1dba36,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$param,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_4_var_args(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$param$$$function_4_var_args,
        const_str_plain_var_args,
#if PYTHON_VERSION >= 300
        const_str_digest_22c8a36ad67f85fabcaa348b123e8ed7,
#endif
        codeobj_b2120c76cc089754da499cac66a4bbde,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$param,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_5___repr__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$param$$$function_5___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_ef3dcd37150646a8d7121feec734f4eb,
#endif
        codeobj_3cbb6e578499c8962954ee66ebe62957,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$param,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_6_get_executed_params(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$param$$$function_6_get_executed_params,
        const_str_plain_get_executed_params,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c9150d2e8299e357a752bb5c8ba8c265,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$param,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_6_get_executed_params$$$function_1_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$param$$$function_6_get_executed_params$$$function_1_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_2a0cb3d99fb95fe90ffb825515898f23,
#endif
        codeobj_e229169c9215cd14be01aa110dcc585a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$param,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_7__error_argument_count(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$param$$$function_7__error_argument_count,
        const_str_plain__error_argument_count,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_34c427d9c4bb8dbbc2ed7daafa7fa16a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$param,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_8__create_default_param(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$param$$$function_8__create_default_param,
        const_str_plain__create_default_param,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_105bad8ab37151945c4a28a9a55508c2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$param,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_jedi$evaluate$param$$$function_9_create_default_params(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_jedi$evaluate$param$$$function_9_create_default_params,
        const_str_plain_create_default_params,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_27cbe2b1328e9129a03855b4e485f0bd,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_jedi$evaluate$param,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_jedi$evaluate$param =
{
    PyModuleDef_HEAD_INIT,
    "jedi.evaluate.param",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(jedi$evaluate$param)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(jedi$evaluate$param)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_jedi$evaluate$param );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate.param: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate.param: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("jedi.evaluate.param: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initjedi$evaluate$param" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_jedi$evaluate$param = Py_InitModule4(
        "jedi.evaluate.param",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_jedi$evaluate$param = PyModule_Create( &mdef_jedi$evaluate$param );
#endif

    moduledict_jedi$evaluate$param = MODULE_DICT( module_jedi$evaluate$param );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_jedi$evaluate$param,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_jedi$evaluate$param,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_jedi$evaluate$param,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_jedi$evaluate$param,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_jedi$evaluate$param );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_9762bd19b18a11af1c0649d1b042a42f, module_jedi$evaluate$param );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_218ccbfd5cd7a66e68cab969a71adaff;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_jedi$evaluate$param_20 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_67012a2ac90fee717ae3700e2c510f1a_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_67012a2ac90fee717ae3700e2c510f1a_2 = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_218ccbfd5cd7a66e68cab969a71adaff = MAKE_MODULE_FRAME( codeobj_218ccbfd5cd7a66e68cab969a71adaff, module_jedi$evaluate$param );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_218ccbfd5cd7a66e68cab969a71adaff );
    assert( Py_REFCNT( frame_218ccbfd5cd7a66e68cab969a71adaff ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_collections;
        tmp_globals_name_1 = (PyObject *)moduledict_jedi$evaluate$param;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_defaultdict_tuple;
        tmp_level_name_1 = const_int_0;
        frame_218ccbfd5cd7a66e68cab969a71adaff->m_frame.f_lineno = 1;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_defaultdict );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_defaultdict, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_0c6a1637c5fdafe468f822c51ad84a09;
        tmp_globals_name_2 = (PyObject *)moduledict_jedi$evaluate$param;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_str_plain_PushBackIterator_tuple;
        tmp_level_name_2 = const_int_0;
        frame_218ccbfd5cd7a66e68cab969a71adaff->m_frame.f_lineno = 3;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_PushBackIterator );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 3;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_PushBackIterator, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_3;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_3 = (PyObject *)moduledict_jedi$evaluate$param;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_analysis_tuple;
        tmp_level_name_3 = const_int_0;
        frame_218ccbfd5cd7a66e68cab969a71adaff->m_frame.f_lineno = 4;
        tmp_import_name_from_3 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_analysis );
        Py_DECREF( tmp_import_name_from_3 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_analysis, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_4b6ae0a4a7c5ef73221de5bb2b8ff0e4;
        tmp_globals_name_4 = (PyObject *)moduledict_jedi$evaluate$param;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_a9e8fa7dc2dad952de019a0f79a561ff_tuple;
        tmp_level_name_4 = const_int_0;
        frame_218ccbfd5cd7a66e68cab969a71adaff->m_frame.f_lineno = 5;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_7;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_LazyKnownContext );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyKnownContext, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_LazyTreeContext );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyTreeContext, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_6 = tmp_import_from_1__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_LazyUnknownContext );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_LazyUnknownContext, tmp_assign_source_10 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_5 = (PyObject *)moduledict_jedi$evaluate$param;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_docstrings_tuple;
        tmp_level_name_5 = const_int_0;
        frame_218ccbfd5cd7a66e68cab969a71adaff->m_frame.f_lineno = 7;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_docstrings );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_docstrings, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_7b5836a4d0c5d98a75de2bb25d92bf01;
        tmp_globals_name_6 = (PyObject *)moduledict_jedi$evaluate$param;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_pep0484_tuple;
        tmp_level_name_6 = const_int_0;
        frame_218ccbfd5cd7a66e68cab969a71adaff->m_frame.f_lineno = 8;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_pep0484 );
        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_pep0484, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_9;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_785ebe15053a4fa731c98e296de8edd7;
        tmp_globals_name_7 = (PyObject *)moduledict_jedi$evaluate$param;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_iterable_tuple;
        tmp_level_name_7 = const_int_0;
        frame_218ccbfd5cd7a66e68cab969a71adaff->m_frame.f_lineno = 9;
        tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_iterable );
        Py_DECREF( tmp_import_name_from_9 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_iterable, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        tmp_assign_source_14 = MAKE_FUNCTION_jedi$evaluate$param$$$function_1__add_argument_issue(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain__add_argument_issue, tmp_assign_source_14 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_15 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_15;
    }
    {
        PyObject *tmp_assign_source_16;
        tmp_assign_source_16 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_16;
    }
    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_17 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_17;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_2;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_18;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;

                goto try_except_handler_2;
            }
            tmp_tuple_element_1 = const_str_plain_ExecutedParam;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_218ccbfd5cd7a66e68cab969a71adaff->m_frame.f_lineno = 20;
            tmp_assign_source_18 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;

                goto try_except_handler_2;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_18;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;

                goto try_except_handler_2;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 20;

                    goto try_except_handler_2;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 20;

                    goto try_except_handler_2;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 20;

                    goto try_except_handler_2;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 20;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_2;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_19;
            tmp_assign_source_19 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_19;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_20;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_jedi$evaluate$param_20 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_9762bd19b18a11af1c0649d1b042a42f;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$param_20, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_digest_252003da09b0ede0d6f744d3f87b2317;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$param_20, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_4;
        }
        tmp_dictset_value = const_str_plain_ExecutedParam;
        tmp_res = PyObject_SetItem( locals_jedi$evaluate$param_20, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;

            goto try_except_handler_4;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_67012a2ac90fee717ae3700e2c510f1a_2, codeobj_67012a2ac90fee717ae3700e2c510f1a, module_jedi$evaluate$param, sizeof(void *) );
        frame_67012a2ac90fee717ae3700e2c510f1a_2 = cache_frame_67012a2ac90fee717ae3700e2c510f1a_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_67012a2ac90fee717ae3700e2c510f1a_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_67012a2ac90fee717ae3700e2c510f1a_2 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$param$$$function_2___init__(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$param_20, const_str_plain___init__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 22;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$param$$$function_3_infer(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$param_20, const_str_plain_infer, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_called_name_3;
            PyObject *tmp_args_element_name_2;
            tmp_res = MAPPING_HAS_ITEM( locals_jedi$evaluate$param_20, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_condition_result_6 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_3;
            }
            else
            {
                goto condexpr_false_3;
            }
            condexpr_true_3:;
            tmp_called_name_2 = PyObject_GetItem( locals_jedi$evaluate$param_20, const_str_plain_property );

            if ( tmp_called_name_2 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }

            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_args_element_name_1 = MAKE_FUNCTION_jedi$evaluate$param$$$function_4_var_args(  );



            frame_67012a2ac90fee717ae3700e2c510f1a_2->m_frame.f_lineno = 36;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            goto condexpr_end_3;
            condexpr_false_3:;
            tmp_called_name_3 = (PyObject *)&PyProperty_Type;
            tmp_args_element_name_2 = MAKE_FUNCTION_jedi$evaluate$param$$$function_4_var_args(  );



            frame_67012a2ac90fee717ae3700e2c510f1a_2->m_frame.f_lineno = 36;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            condexpr_end_3:;
            tmp_res = PyObject_SetItem( locals_jedi$evaluate$param_20, const_str_plain_var_args, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 36;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        tmp_dictset_value = MAKE_FUNCTION_jedi$evaluate$param$$$function_5___repr__(  );



        tmp_res = PyObject_SetItem( locals_jedi$evaluate$param_20, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;
            type_description_2 = "o";
            goto frame_exception_exit_2;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_67012a2ac90fee717ae3700e2c510f1a_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_67012a2ac90fee717ae3700e2c510f1a_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_67012a2ac90fee717ae3700e2c510f1a_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_67012a2ac90fee717ae3700e2c510f1a_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_67012a2ac90fee717ae3700e2c510f1a_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_67012a2ac90fee717ae3700e2c510f1a_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_67012a2ac90fee717ae3700e2c510f1a_2 == cache_frame_67012a2ac90fee717ae3700e2c510f1a_2 )
        {
            Py_DECREF( frame_67012a2ac90fee717ae3700e2c510f1a_2 );
        }
        cache_frame_67012a2ac90fee717ae3700e2c510f1a_2 = NULL;

        assertFrameObject( frame_67012a2ac90fee717ae3700e2c510f1a_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_4;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_7;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;

                goto try_except_handler_4;
            }
            tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_jedi$evaluate$param_20, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;

                goto try_except_handler_4;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_21;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_4 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_3 = const_str_plain_ExecutedParam;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_3 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_3 );
            tmp_tuple_element_3 = locals_jedi$evaluate$param_20;
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_3 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_218ccbfd5cd7a66e68cab969a71adaff->m_frame.f_lineno = 20;
            tmp_assign_source_21 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 20;

                goto try_except_handler_4;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_21;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_20 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_20 );
        goto try_return_handler_4;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$param );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_4:;
        Py_DECREF( locals_jedi$evaluate$param_20 );
        locals_jedi$evaluate$param_20 = NULL;
        goto try_return_handler_3;
        // Exception handler code:
        try_except_handler_4:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_jedi$evaluate$param_20 );
        locals_jedi$evaluate$param_20 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto try_except_handler_3;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$param );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_3:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( jedi$evaluate$param );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 20;
        goto try_except_handler_2;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_ExecutedParam, tmp_assign_source_20 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_218ccbfd5cd7a66e68cab969a71adaff );
#endif
    popFrameStack();

    assertFrameObject( frame_218ccbfd5cd7a66e68cab969a71adaff );

    goto frame_no_exception_2;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_218ccbfd5cd7a66e68cab969a71adaff );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_218ccbfd5cd7a66e68cab969a71adaff, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_218ccbfd5cd7a66e68cab969a71adaff->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_218ccbfd5cd7a66e68cab969a71adaff, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_2:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    {
        PyObject *tmp_assign_source_22;
        tmp_assign_source_22 = MAKE_FUNCTION_jedi$evaluate$param$$$function_6_get_executed_params(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_get_executed_params, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        tmp_assign_source_23 = MAKE_FUNCTION_jedi$evaluate$param$$$function_7__error_argument_count(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain__error_argument_count, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        tmp_assign_source_24 = MAKE_FUNCTION_jedi$evaluate$param$$$function_8__create_default_param(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain__create_default_param, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        tmp_assign_source_25 = MAKE_FUNCTION_jedi$evaluate$param$$$function_9_create_default_params(  );



        UPDATE_STRING_DICT1( moduledict_jedi$evaluate$param, (Nuitka_StringObject *)const_str_plain_create_default_params, tmp_assign_source_25 );
    }

    return MOD_RETURN_VALUE( module_jedi$evaluate$param );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
