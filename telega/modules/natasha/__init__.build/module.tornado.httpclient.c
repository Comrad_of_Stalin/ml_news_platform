/* Generated code for Python module 'tornado.httpclient'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_tornado$httpclient" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_tornado$httpclient;
PyDictObject *moduledict_tornado$httpclient;

/* The declarations of module constants used, if any. */
extern PyObject *const_str_plain_headers;
static PyObject *const_str_digest_501ac84050535bbb2ffaca075c15b490;
extern PyObject *const_tuple_str_plain_self_str_plain_args_tuple;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain___del__;
extern PyObject *const_str_plain_dict;
extern PyObject *const_str_plain___name__;
extern PyObject *const_str_plain_IOLoop;
extern PyObject *const_str_plain_sorted;
extern PyObject *const_str_plain_i;
extern PyObject *const_str_plain_object;
extern PyObject *const_str_plain_bool;
extern PyObject *const_str_plain_default;
static PyObject *const_tuple_ec7d9dd540745381d3553e815d2b49ff_tuple;
static PyObject *const_str_digest_eb7897416f79c1314be69b89f4343646;
static PyObject *const_tuple_str_plain_validate_cert_tuple;
extern PyObject *const_str_digest_e077944e15accbec54ecb40fd81dafde;
extern PyObject *const_str_plain_None;
extern PyObject *const_str_plain_Optional;
extern PyObject *const_int_pos_5;
static PyObject *const_str_digest_0ac86c737ebdecf2bdc0ee7a1913d4c6;
static PyObject *const_str_digest_110520dd84acf1f2f0cc4300fbe8f98b;
static PyObject *const_tuple_str_plain_print_headers_tuple;
static PyObject *const_str_digest_f09643320473256ce56cdb2dfb67dd6b;
extern PyObject *const_str_plain_typing;
extern PyObject *const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_i_tuple;
extern PyObject *const_str_plain__body;
static PyObject *const_str_plain_HTTPClient;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_plain_time;
extern PyObject *const_str_plain_allow_nonstandard_methods;
extern PyObject *const_str_angle_genexpr;
extern PyObject *const_str_plain_str;
extern PyObject *const_str_plain__closed;
static PyObject *const_str_plain_handle_response;
extern PyObject *const_str_plain_AsyncHTTPClient;
static PyObject *const_tuple_str_plain_SimpleAsyncHTTPClient_tuple;
static PyObject *const_tuple_str_plain_cls_str_plain_attr_name_tuple;
extern PyObject *const_str_plain_BytesIO;
extern PyObject *const_str_digest_6f55736fcd57507504cc7f0643fbf21f;
extern PyObject *const_str_plain_impl;
extern PyObject *const_str_plain_name;
static PyObject *const_str_digest_408e7d16e641d982e20ed3aec8e62210;
static PyObject *const_str_digest_cc31a339455bd0327750e06c46ff8b3d;
extern PyObject *const_tuple_int_0_tuple;
extern PyObject *const_str_plain_proxy_port;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain___new__;
static PyObject *const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple;
extern PyObject *const_str_plain_tornado;
extern PyObject *const_str_plain_pop;
extern PyObject *const_int_0;
extern PyObject *const_str_digest_69b6ecec1ddb0b99fd2288f02d4834e9;
extern PyObject *const_str_plain_decompress_response;
extern PyObject *const_str_plain_code;
extern PyObject *const_str_plain_response;
extern PyObject *const_str_plain_getvalue;
static PyObject *const_str_digest_65b038522313564575aa5b2b15974f09;
extern PyObject *const_str_plain_proxy_auth_mode;
extern PyObject *const_str_plain_allow_ipv6;
static PyObject *const_str_digest_57fde80fb420238e76979a36ff351987;
static PyObject *const_tuple_str_plain_print_body_tuple;
extern PyObject *const_str_plain_HTTPRequest;
extern PyObject *const_float_20_0;
extern PyObject *const_str_plain_defaults;
extern PyObject *const_int_pos_200;
extern PyObject *const_str_plain_type;
static PyObject *const_dict_4fb07ff838d1a4c46118c49ac78379b1;
extern PyObject *const_str_plain___cached__;
static PyObject *const_str_digest_a5eb7baaff4fadb0397fae8dc1f75fcc;
extern PyObject *const_tuple_none_tuple;
extern PyObject *const_str_plain_functools;
static PyObject *const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple;
static PyObject *const_str_plain_prepare_curl_callback;
extern PyObject *const_str_plain_partial;
extern PyObject *const_str_plain_effective_url;
extern PyObject *const_str_plain_user_agent;
static PyObject *const_str_plain_make_client;
extern PyObject *const_str_plain___prepare__;
static PyObject *const_tuple_491035fcbadc3d1f2429fd4f40586059_tuple;
extern PyObject *const_str_plain_url;
static PyObject *const_str_plain__instance_cache;
static PyObject *const_str_plain_request_proxy;
extern PyObject *const_str_plain___repr__;
extern PyObject *const_str_plain_follow_redirects;
extern PyObject *const_str_plain_kwargs;
static PyObject *const_tuple_str_plain_proxy_port_tuple;
extern PyObject *const_str_plain_configure;
extern PyObject *const_str_plain_property;
extern PyObject *const_str_plain_native_str;
static PyObject *const_str_plain_rethrow;
extern PyObject *const_str_plain_validate_cert;
extern PyObject *const_str_plain___file__;
extern PyObject *const_str_plain_setter;
extern PyObject *const_tuple_str_plain_cls_tuple;
static PyObject *const_dict_ffca9ad2e079ce02b380f5ac23e6d883;
static PyObject *const_str_digest_50b140d682c90fe96606564ea5dd571b;
static PyObject *const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple;
static PyObject *const_str_digest_fa43d8078d8197a0b1775f650130ce1f;
extern PyObject *const_str_plain_configurable_default;
static PyObject *const_str_plain__error_is_response_code;
static PyObject *const_str_plain_HTTPClientError;
extern PyObject *const_str_plain_Dict;
extern PyObject *const_tuple_str_plain_Configurable_tuple;
extern PyObject *const_str_plain_attr_name;
extern PyObject *const_str_plain_connect_timeout;
static PyObject *const_str_digest_f4859e8d4563b7aa7db5db4d395ab49d;
extern PyObject *const_str_plain_expect_100_continue;
extern PyObject *const_str_plain_reason;
static PyObject *const_str_digest_779a6df8027b904e318e30a6afe10e8b;
static PyObject *const_tuple_15a3f697abc1e545999d43ae8c0a3dba_tuple;
extern PyObject *const_str_plain___orig_bases__;
extern PyObject *const_str_plain_close;
extern PyObject *const_int_pos_300;
static PyObject *const_str_digest_8b80da22e8fa2262b0edc0715696de6e;
static PyObject *const_tuple_c63519c88023de0285858e9e87a81922_tuple;
extern PyObject *const_str_plain___qualname__;
extern PyObject *const_str_plain_network_interface;
extern PyObject *const_str_plain_auth_password;
extern PyObject *const_str_plain_value;
extern PyObject *const_str_plain_e;
extern PyObject *const_str_plain_message;
extern PyObject *const_str_plain_Type;
static PyObject *const_str_plain_print_body;
extern PyObject *const_str_plain_HTTPHeaders;
extern PyObject *const_tuple_str_plain_self_str_plain_value_tuple;
extern PyObject *const_str_plain_weakref;
extern PyObject *const_str_plain_datetime;
extern PyObject *const_str_digest_3f0a7e7b48e8430fc8c8a4c7e40f957f;
static PyObject *const_tuple_str_plain_gen_str_plain_httputil_tuple;
static PyObject *const_dict_f486466b362c183859d46eaa920d7989;
extern PyObject *const_str_plain_error;
extern PyObject *const_str_digest_d255455dca89984a7a880379cd01bd4d;
static PyObject *const_str_digest_cdc2985a6310c422b25edf8035d6b5da;
static PyObject *const_str_plain_cached_val;
extern PyObject *const_str_plain_buffer;
extern PyObject *const_str_plain___getitem__;
static PyObject *const_tuple_e064a1faf239cdd37f9d7015f03bd688_tuple;
extern PyObject *const_str_plain_define;
static PyObject *const_str_digest_2a54b579adbe29e44c1193df08d0ca54;
extern PyObject *const_str_plain_httputil;
extern PyObject *const_str_digest_e8e68d6f1f9fdc5f77d42501a852b12f;
static PyObject *const_tuple_str_plain_self_str_plain_request_str_plain_callback_tuple;
extern PyObject *const_str_plain_auth_username;
static PyObject *const_str_digest_58f29ba955cde6aab12dfbb0f02fc151;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_digest_75fd71b1edada749c2ef7ac810062295;
extern PyObject *const_str_plain_WeakKeyDictionary;
extern PyObject *const_str_plain_responses;
extern PyObject *const_str_plain_HTTPError;
extern PyObject *const_str_plain_callback;
extern PyObject *const_tuple_str_plain_IOLoop_tuple;
extern PyObject *const_str_plain_configurable_base;
extern PyObject *const_str_plain_io;
static PyObject *const_str_digest_02224986bed76353a6f0be294c953224;
extern PyObject *const_str_plain__RequestProxy;
static PyObject *const_str_digest_04f1eb4be9879dbd1efb1ec5bd490663;
extern PyObject *const_str_plain_Configurable;
extern PyObject *const_str_plain___init__;
extern PyObject *const_str_plain_client;
extern PyObject *const_str_plain__DEFAULTS;
static PyObject *const_str_plain_async_client_class;
static PyObject *const_str_digest_646129925d927a2e0f032ec5fd4d12ed;
extern PyObject *const_str_plain_method;
extern PyObject *const_str_plain_max_redirects;
extern PyObject *const_str_plain_client_key;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_empty;
static PyObject *const_str_digest_3f372af9fa5618895a31218b05ae3f4b;
extern PyObject *const_tuple_none_none_tuple;
extern PyObject *const_str_plain_run_sync;
static PyObject *const_str_digest_4b00cdfdcd00aff27f294475d3d9c936;
extern PyObject *const_str_plain_request;
extern PyObject *const_str_digest_1257ef3e7457bac5b76db6123b3c6cc2;
extern PyObject *const_str_plain_initialize;
static PyObject *const_tuple_str_plain_async_client_class_str_plain_kwargs_tuple;
extern PyObject *const_str_plain_bytes;
extern PyObject *const_str_plain_BaseException;
static PyObject *const_str_digest_61e6d62d20308812ff9c0c9f78bcd75f;
static PyObject *const_tuple_d13c1ccf0f7e46b28fae431fffa73548_tuple;
extern PyObject *const_str_digest_66ef772b0f62991fd20497da33b24b9b;
extern PyObject *const_str_plain_return;
static PyObject *const_str_digest_5180ce7a3e8dd5fcafd1ba4691d5bfb2;
extern PyObject *const_str_plain_classmethod;
extern PyObject *const_str_plain_sleep;
static PyObject *const_dict_b3fa57d622665417c82eaa93a4cf743c;
extern PyObject *const_str_digest_649be217853297dd7bed9a9de82b5db6;
static PyObject *const_str_digest_03a6f790f27598bb7c4371b455bd02c5;
extern PyObject *const_str_plain_body;
extern PyObject *const_tuple_true_tuple;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_str_plain_Future;
extern PyObject *const_str_plain_options;
static PyObject *const_dict_480a75f460aacdb5be24e0bff4669741;
extern PyObject *const_str_plain_ssl_options;
extern PyObject *const_tuple_none_none_none_none_none_none_none_none_tuple;
static PyObject *const_tuple_str_plain_follow_redirects_tuple;
extern PyObject *const_str_plain_SSLContext;
extern PyObject *const_str_digest_b9c4baf879ebd882d40843df3a4dead7;
extern PyObject *const_tuple_str_plain_self_tuple;
extern PyObject *const_str_plain_float;
extern PyObject *const_str_plain_parse_command_line;
static PyObject *const_str_digest_2291376f8ac5f724b6b701a36ee441fd;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain___getattr__;
extern PyObject *const_tuple_c20e76db2359ca9d4cb706a65d727a61_tuple;
static PyObject *const_str_plain_time_info;
static PyObject *const_tuple_8530f5cfdba39176f39925bab6b635c8_tuple;
extern PyObject *const_str_plain__io_loop;
static PyObject *const_str_plain__async_client_dict_;
static PyObject *const_str_digest_3b7473fff9725cc62f429dad68799469;
extern PyObject *const_str_plain_Any;
extern PyObject *const_tuple_str_plain_BytesIO_tuple;
extern PyObject *const_str_plain_body_producer;
static PyObject *const_dict_ae8aa59e73f92c1f97579b3c4dbcc675;
extern PyObject *const_str_plain_ssl;
extern PyObject *const_str_plain_fetch_impl;
extern PyObject *const_str_plain_current;
static PyObject *const_tuple_e03233a4438f70401fd7c39b76e4dbb5_tuple;
extern PyObject *const_str_plain_client_cert;
static PyObject *const_str_plain__async_clients;
static PyObject *const_list_str_plain_HTTPResponse_list;
extern PyObject *const_str_plain_make_current;
extern PyObject *const_str_plain_cast;
static PyObject *const_str_plain_print_headers;
extern PyObject *const_str_plain_Unknown;
extern PyObject *const_str_plain___class__;
extern PyObject *const_str_plain_Union;
extern PyObject *const_tuple_type_Exception_tuple;
static PyObject *const_str_digest_3966c5d48e4aec5eaa9d4d76ce2f44a3;
extern PyObject *const_str_plain___module__;
extern PyObject *const_str_plain___str__;
static PyObject *const_tuple_str_plain_cls_str_plain_SimpleAsyncHTTPClient_tuple;
extern PyObject *const_str_plain_utf8;
static PyObject *const_str_digest_7b5c18e05c25516935228d84f6d6dde5;
extern PyObject *const_str_plain_update;
extern PyObject *const_dict_751ed0ee3c51c52082c6a2bb9cbce741;
extern PyObject *const_str_plain_print;
static PyObject *const_str_digest_c3fd672541fb689880c497d1199b14f3;
static PyObject *const_tuple_str_plain_proxy_host_tuple;
extern PyObject *const_str_plain_future_set_exception_unless_cancelled;
extern PyObject *const_str_plain_get;
static PyObject *const_str_plain_use_gzip;
static PyObject *const_str_digest_afb184d09906a65fcfde6b3eefbb19a6;
extern PyObject *const_str_plain_start_time;
extern PyObject *const_str_plain_future_set_result_unless_cancelled;
static PyObject *const_str_digest_b7d04337ffd07131655e2e21c568a5d6;
extern PyObject *const_str_plain_metaclass;
extern PyObject *const_str_plain_main;
extern PyObject *const_str_plain_raise_error;
extern PyObject *const_tuple_str_plain___class___tuple;
extern PyObject *const_tuple_false_tuple;
extern PyObject *const_str_angle_metaclass;
extern PyObject *const_dict_056a293e2058d56276328e53ff09a8b9;
extern PyObject *const_str_plain_arg;
extern PyObject *const_str_plain_args;
extern PyObject *const_str_plain_format_timestamp;
extern PyObject *const_str_plain_request_timeout;
extern PyObject *const_str_plain_gen;
extern PyObject *const_str_plain_items;
extern PyObject *const_str_plain_Callable;
static PyObject *const_str_digest_e01eabb9dfbe5c0e49d4dcf1fbe88c15;
static PyObject *const_str_digest_c24969492806251a8bc7b7e0886afacf;
static PyObject *const_str_digest_dd5b9f91f366a2dcc7dd5058f4640e5c;
extern PyObject *const_str_plain_instance;
extern PyObject *const_str_plain_auth_mode;
static PyObject *const_tuple_str_plain_utf8_str_plain_native_str_tuple;
extern PyObject *const_str_plain_cls;
static PyObject *const_str_digest_3f34f04b46b728f42053850d2d2f2e7c;
extern PyObject *const_str_digest_84a3f2fedb1ec7918aa30203da68a052;
extern PyObject *const_str_plain_join;
extern PyObject *const_str_plain__headers;
extern PyObject *const_str_digest_b6a6369109180527d448642da15ecbc0;
static PyObject *const_str_digest_c83e5f43b5f0faf49ff813bd94426c79;
extern PyObject *const_str_plain_SimpleAsyncHTTPClient;
extern PyObject *const_str_chr_44;
static PyObject *const_dict_a7b9314e26045b98fb4910dec4a6aa82;
static PyObject *const_tuple_str_plain_self_str_plain_defaults_tuple;
extern PyObject *const_str_plain_int;
extern PyObject *const_str_plain_fetch;
extern PyObject *const_str_plain_streaming_callback;
extern PyObject *const_str_plain_request_time;
static PyObject *const_str_plain_instance_cache;
static PyObject *const_tuple_list_str_plain_HTTPResponse_list_none_tuple;
extern PyObject *const_str_digest_9b98666360eab9bd5e6093808f6c2588;
static PyObject *const_str_digest_0f8e13b4708f8a35e2e860117792b9fe;
extern PyObject *const_str_digest_f18f7ca0806c368196be935d559815ea;
extern PyObject *const_str_plain_proxy_username;
static PyObject *const_tuple_str_plain_self_str_plain_cached_val_tuple;
extern PyObject *const_str_plain_header_callback;
extern PyObject *const_str_plain_HTTPResponse;
static PyObject *const_str_digest_1e8b8191e6ee572c2b9ae8835d41eb7f;
static PyObject *const_tuple_str_plain_response_str_plain_raise_error_str_plain_future_tuple;
static PyObject *const_tuple_str_plain_self_str_plain_request_str_plain_defaults_tuple;
extern PyObject *const_str_digest_a7636f57d1f8f80548278378b7bf84e2;
extern PyObject *const_str_plain_proxy_host;
extern PyObject *const_bytes_empty;
static PyObject *const_str_plain_force_instance;
extern PyObject *const_str_plain_Awaitable;
static PyObject *const_str_digest_868647966cabcea20bb9bdd8af5cd9da;
static PyObject *const_str_plain_if_modified_since;
static PyObject *const_str_plain__async_client;
static PyObject *const_str_digest_5cfef7f95e02358e66acc192938bb47e;
extern PyObject *const_tuple_type_object_tuple;
extern PyObject *const_str_plain_ca_certs;
extern PyObject *const_str_plain_proxy_password;
static PyObject *const_str_digest_a1f1d5c51f21741d345f323ecee64ae1;
extern PyObject *const_str_plain_self;
extern PyObject *const_str_plain_io_loop;
static PyObject *const_tuple_str_plain_self_str_plain_name_str_plain_request_attr_tuple;
extern PyObject *const_str_plain_future;
static PyObject *const_tuple_d419ed77ca3e651e427938c507014e9e_tuple;
static PyObject *const_str_digest_2637eb19e85ee47cbbc364b199fa7f11;
static PyObject *const_str_plain_request_attr;
static PyObject *const_str_digest_2637714cd79b52e756b2393287778142;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_501ac84050535bbb2ffaca075c15b490 = UNSTREAM_STRING_ASCII( &constant_bin[ 5350123 ], 40, 0 );
    const_tuple_ec7d9dd540745381d3553e815d2b49ff_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 5350163 ], 496 );
    const_str_digest_eb7897416f79c1314be69b89f4343646 = UNSTREAM_STRING_ASCII( &constant_bin[ 5350659 ], 56, 0 );
    const_tuple_str_plain_validate_cert_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_validate_cert_tuple, 0, const_str_plain_validate_cert ); Py_INCREF( const_str_plain_validate_cert );
    const_str_digest_0ac86c737ebdecf2bdc0ee7a1913d4c6 = UNSTREAM_STRING_ASCII( &constant_bin[ 5350715 ], 40, 0 );
    const_str_digest_110520dd84acf1f2f0cc4300fbe8f98b = UNSTREAM_STRING_ASCII( &constant_bin[ 5350755 ], 25, 0 );
    const_tuple_str_plain_print_headers_tuple = PyTuple_New( 1 );
    const_str_plain_print_headers = UNSTREAM_STRING_ASCII( &constant_bin[ 5350780 ], 13, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_print_headers_tuple, 0, const_str_plain_print_headers ); Py_INCREF( const_str_plain_print_headers );
    const_str_digest_f09643320473256ce56cdb2dfb67dd6b = UNSTREAM_STRING_ASCII( &constant_bin[ 5350793 ], 494, 0 );
    const_str_plain_HTTPClient = UNSTREAM_STRING_ASCII( &constant_bin[ 5333321 ], 10, 1 );
    const_str_plain_handle_response = UNSTREAM_STRING_ASCII( &constant_bin[ 5351287 ], 15, 1 );
    const_tuple_str_plain_SimpleAsyncHTTPClient_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_SimpleAsyncHTTPClient_tuple, 0, const_str_plain_SimpleAsyncHTTPClient ); Py_INCREF( const_str_plain_SimpleAsyncHTTPClient );
    const_tuple_str_plain_cls_str_plain_attr_name_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_attr_name_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_attr_name_tuple, 1, const_str_plain_attr_name ); Py_INCREF( const_str_plain_attr_name );
    const_str_digest_408e7d16e641d982e20ed3aec8e62210 = UNSTREAM_STRING_ASCII( &constant_bin[ 5351302 ], 26, 0 );
    const_str_digest_cc31a339455bd0327750e06c46ff8b3d = UNSTREAM_STRING_ASCII( &constant_bin[ 5351328 ], 59, 0 );
    const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple, 0, const_str_plain_Type ); Py_INCREF( const_str_plain_Type );
    PyTuple_SET_ITEM( const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple, 1, const_str_plain_Any ); Py_INCREF( const_str_plain_Any );
    PyTuple_SET_ITEM( const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple, 2, const_str_plain_Union ); Py_INCREF( const_str_plain_Union );
    PyTuple_SET_ITEM( const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple, 3, const_str_plain_Dict ); Py_INCREF( const_str_plain_Dict );
    PyTuple_SET_ITEM( const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple, 4, const_str_plain_Callable ); Py_INCREF( const_str_plain_Callable );
    PyTuple_SET_ITEM( const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple, 5, const_str_plain_Optional ); Py_INCREF( const_str_plain_Optional );
    PyTuple_SET_ITEM( const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple, 6, const_str_plain_cast ); Py_INCREF( const_str_plain_cast );
    PyTuple_SET_ITEM( const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple, 7, const_str_plain_Awaitable ); Py_INCREF( const_str_plain_Awaitable );
    const_str_digest_65b038522313564575aa5b2b15974f09 = UNSTREAM_STRING_ASCII( &constant_bin[ 5351387 ], 17, 0 );
    const_str_digest_57fde80fb420238e76979a36ff351987 = UNSTREAM_STRING_ASCII( &constant_bin[ 5351404 ], 18, 0 );
    const_tuple_str_plain_print_body_tuple = PyTuple_New( 1 );
    const_str_plain_print_body = UNSTREAM_STRING_ASCII( &constant_bin[ 5351422 ], 10, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_print_body_tuple, 0, const_str_plain_print_body ); Py_INCREF( const_str_plain_print_body );
    const_dict_4fb07ff838d1a4c46118c49ac78379b1 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_4fb07ff838d1a4c46118c49ac78379b1, const_str_plain_return, const_str_plain_AsyncHTTPClient );
    assert( PyDict_Size( const_dict_4fb07ff838d1a4c46118c49ac78379b1 ) == 1 );
    const_str_digest_a5eb7baaff4fadb0397fae8dc1f75fcc = UNSTREAM_STRING_ASCII( &constant_bin[ 5351432 ], 16, 0 );
    const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple = PyTuple_New( 11 );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 1, const_str_plain_request ); Py_INCREF( const_str_plain_request );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 2, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 3, const_str_plain_headers ); Py_INCREF( const_str_plain_headers );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 4, const_str_plain_buffer ); Py_INCREF( const_str_plain_buffer );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 5, const_str_plain_effective_url ); Py_INCREF( const_str_plain_effective_url );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 6, const_str_plain_error ); Py_INCREF( const_str_plain_error );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 7, const_str_plain_request_time ); Py_INCREF( const_str_plain_request_time );
    const_str_plain_time_info = UNSTREAM_STRING_ASCII( &constant_bin[ 5351448 ], 9, 1 );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 8, const_str_plain_time_info ); Py_INCREF( const_str_plain_time_info );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 9, const_str_plain_reason ); Py_INCREF( const_str_plain_reason );
    PyTuple_SET_ITEM( const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 10, const_str_plain_start_time ); Py_INCREF( const_str_plain_start_time );
    const_str_plain_prepare_curl_callback = UNSTREAM_STRING_ASCII( &constant_bin[ 5350406 ], 21, 1 );
    const_str_plain_make_client = UNSTREAM_STRING_ASCII( &constant_bin[ 5351457 ], 11, 1 );
    const_tuple_491035fcbadc3d1f2429fd4f40586059_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_491035fcbadc3d1f2429fd4f40586059_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_491035fcbadc3d1f2429fd4f40586059_tuple, 1, const_str_plain_code ); Py_INCREF( const_str_plain_code );
    PyTuple_SET_ITEM( const_tuple_491035fcbadc3d1f2429fd4f40586059_tuple, 2, const_str_plain_message ); Py_INCREF( const_str_plain_message );
    PyTuple_SET_ITEM( const_tuple_491035fcbadc3d1f2429fd4f40586059_tuple, 3, const_str_plain_response ); Py_INCREF( const_str_plain_response );
    PyTuple_SET_ITEM( const_tuple_491035fcbadc3d1f2429fd4f40586059_tuple, 4, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_plain__instance_cache = UNSTREAM_STRING_ASCII( &constant_bin[ 5351468 ], 15, 1 );
    const_str_plain_request_proxy = UNSTREAM_STRING_ASCII( &constant_bin[ 5351483 ], 13, 1 );
    const_tuple_str_plain_proxy_port_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_proxy_port_tuple, 0, const_str_plain_proxy_port ); Py_INCREF( const_str_plain_proxy_port );
    const_str_plain_rethrow = UNSTREAM_STRING_ASCII( &constant_bin[ 1023464 ], 7, 1 );
    const_dict_ffca9ad2e079ce02b380f5ac23e6d883 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_ffca9ad2e079ce02b380f5ac23e6d883, const_str_plain_type, (PyObject *)&PyBool_Type );
    PyDict_SetItem( const_dict_ffca9ad2e079ce02b380f5ac23e6d883, const_str_plain_default, Py_True );
    assert( PyDict_Size( const_dict_ffca9ad2e079ce02b380f5ac23e6d883 ) == 2 );
    const_str_digest_50b140d682c90fe96606564ea5dd571b = UNSTREAM_STRING_ASCII( &constant_bin[ 5351496 ], 20, 0 );
    const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple = PyTuple_New( 8 );
    PyTuple_SET_ITEM( const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple, 0, const_str_plain_define ); Py_INCREF( const_str_plain_define );
    PyTuple_SET_ITEM( const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple, 1, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple, 2, const_str_plain_parse_command_line ); Py_INCREF( const_str_plain_parse_command_line );
    PyTuple_SET_ITEM( const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple, 3, const_str_plain_args ); Py_INCREF( const_str_plain_args );
    PyTuple_SET_ITEM( const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple, 4, const_str_plain_client ); Py_INCREF( const_str_plain_client );
    PyTuple_SET_ITEM( const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple, 5, const_str_plain_arg ); Py_INCREF( const_str_plain_arg );
    PyTuple_SET_ITEM( const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple, 6, const_str_plain_response ); Py_INCREF( const_str_plain_response );
    PyTuple_SET_ITEM( const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple, 7, const_str_plain_e ); Py_INCREF( const_str_plain_e );
    const_str_digest_fa43d8078d8197a0b1775f650130ce1f = UNSTREAM_STRING_ASCII( &constant_bin[ 5351516 ], 1872, 0 );
    const_str_plain__error_is_response_code = UNSTREAM_STRING_ASCII( &constant_bin[ 5353388 ], 23, 1 );
    const_str_plain_HTTPClientError = UNSTREAM_STRING_ASCII( &constant_bin[ 5353411 ], 15, 1 );
    const_str_digest_f4859e8d4563b7aa7db5db4d395ab49d = UNSTREAM_STRING_ASCII( &constant_bin[ 5353426 ], 1432, 0 );
    const_str_digest_779a6df8027b904e318e30a6afe10e8b = UNSTREAM_STRING_ASCII( &constant_bin[ 5354858 ], 984, 0 );
    const_tuple_15a3f697abc1e545999d43ae8c0a3dba_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_15a3f697abc1e545999d43ae8c0a3dba_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_15a3f697abc1e545999d43ae8c0a3dba_tuple, 1, const_str_plain_request ); Py_INCREF( const_str_plain_request );
    PyTuple_SET_ITEM( const_tuple_15a3f697abc1e545999d43ae8c0a3dba_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_15a3f697abc1e545999d43ae8c0a3dba_tuple, 3, const_str_plain_response ); Py_INCREF( const_str_plain_response );
    const_str_digest_8b80da22e8fa2262b0edc0715696de6e = UNSTREAM_STRING_ASCII( &constant_bin[ 5355842 ], 26, 0 );
    const_tuple_c63519c88023de0285858e9e87a81922_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_c63519c88023de0285858e9e87a81922_tuple, 0, const_str_plain_define ); Py_INCREF( const_str_plain_define );
    PyTuple_SET_ITEM( const_tuple_c63519c88023de0285858e9e87a81922_tuple, 1, const_str_plain_options ); Py_INCREF( const_str_plain_options );
    PyTuple_SET_ITEM( const_tuple_c63519c88023de0285858e9e87a81922_tuple, 2, const_str_plain_parse_command_line ); Py_INCREF( const_str_plain_parse_command_line );
    const_tuple_str_plain_gen_str_plain_httputil_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_gen_str_plain_httputil_tuple, 0, const_str_plain_gen ); Py_INCREF( const_str_plain_gen );
    PyTuple_SET_ITEM( const_tuple_str_plain_gen_str_plain_httputil_tuple, 1, const_str_plain_httputil ); Py_INCREF( const_str_plain_httputil );
    const_dict_f486466b362c183859d46eaa920d7989 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_f486466b362c183859d46eaa920d7989, const_str_plain_response, const_str_plain_HTTPResponse );
    PyDict_SetItem( const_dict_f486466b362c183859d46eaa920d7989, const_str_plain_return, Py_None );
    assert( PyDict_Size( const_dict_f486466b362c183859d46eaa920d7989 ) == 2 );
    const_str_digest_cdc2985a6310c422b25edf8035d6b5da = UNSTREAM_STRING_ASCII( &constant_bin[ 5355868 ], 1274, 0 );
    const_str_plain_cached_val = UNSTREAM_STRING_ASCII( &constant_bin[ 5357142 ], 10, 1 );
    const_tuple_e064a1faf239cdd37f9d7015f03bd688_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_e064a1faf239cdd37f9d7015f03bd688_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_e064a1faf239cdd37f9d7015f03bd688_tuple, 1, const_str_plain_impl ); Py_INCREF( const_str_plain_impl );
    PyTuple_SET_ITEM( const_tuple_e064a1faf239cdd37f9d7015f03bd688_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_e064a1faf239cdd37f9d7015f03bd688_tuple, 3, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_2a54b579adbe29e44c1193df08d0ca54 = UNSTREAM_STRING_ASCII( &constant_bin[ 5357152 ], 19, 0 );
    const_tuple_str_plain_self_str_plain_request_str_plain_callback_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_request_str_plain_callback_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_request_str_plain_callback_tuple, 1, const_str_plain_request ); Py_INCREF( const_str_plain_request );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_request_str_plain_callback_tuple, 2, const_str_plain_callback ); Py_INCREF( const_str_plain_callback );
    const_str_digest_58f29ba955cde6aab12dfbb0f02fc151 = UNSTREAM_STRING_ASCII( &constant_bin[ 5357171 ], 401, 0 );
    const_str_digest_02224986bed76353a6f0be294c953224 = UNSTREAM_STRING_ASCII( &constant_bin[ 5357572 ], 669, 0 );
    const_str_digest_04f1eb4be9879dbd1efb1ec5bd490663 = UNSTREAM_STRING_ASCII( &constant_bin[ 5358241 ], 27, 0 );
    const_str_plain_async_client_class = UNSTREAM_STRING_ASCII( &constant_bin[ 5358268 ], 18, 1 );
    const_str_digest_646129925d927a2e0f032ec5fd4d12ed = UNSTREAM_STRING_ASCII( &constant_bin[ 5358286 ], 46, 0 );
    const_str_digest_3f372af9fa5618895a31218b05ae3f4b = UNSTREAM_STRING_ASCII( &constant_bin[ 5358332 ], 36, 0 );
    const_str_digest_4b00cdfdcd00aff27f294475d3d9c936 = UNSTREAM_STRING_ASCII( &constant_bin[ 5358368 ], 23, 0 );
    const_tuple_str_plain_async_client_class_str_plain_kwargs_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_async_client_class_str_plain_kwargs_tuple, 0, const_str_plain_async_client_class ); Py_INCREF( const_str_plain_async_client_class );
    PyTuple_SET_ITEM( const_tuple_str_plain_async_client_class_str_plain_kwargs_tuple, 1, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    const_str_digest_61e6d62d20308812ff9c0c9f78bcd75f = UNSTREAM_STRING_ASCII( &constant_bin[ 5358391 ], 19, 0 );
    const_tuple_d13c1ccf0f7e46b28fae431fffa73548_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_d13c1ccf0f7e46b28fae431fffa73548_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    const_str_plain_force_instance = UNSTREAM_STRING_ASCII( &constant_bin[ 5351101 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_d13c1ccf0f7e46b28fae431fffa73548_tuple, 1, const_str_plain_force_instance ); Py_INCREF( const_str_plain_force_instance );
    PyTuple_SET_ITEM( const_tuple_d13c1ccf0f7e46b28fae431fffa73548_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_d13c1ccf0f7e46b28fae431fffa73548_tuple, 3, const_str_plain_io_loop ); Py_INCREF( const_str_plain_io_loop );
    const_str_plain_instance_cache = UNSTREAM_STRING_ASCII( &constant_bin[ 5351469 ], 14, 1 );
    PyTuple_SET_ITEM( const_tuple_d13c1ccf0f7e46b28fae431fffa73548_tuple, 4, const_str_plain_instance_cache ); Py_INCREF( const_str_plain_instance_cache );
    PyTuple_SET_ITEM( const_tuple_d13c1ccf0f7e46b28fae431fffa73548_tuple, 5, const_str_plain_instance ); Py_INCREF( const_str_plain_instance );
    PyTuple_SET_ITEM( const_tuple_d13c1ccf0f7e46b28fae431fffa73548_tuple, 6, const_str_plain___class__ ); Py_INCREF( const_str_plain___class__ );
    const_str_digest_5180ce7a3e8dd5fcafd1ba4691d5bfb2 = UNSTREAM_STRING_ASCII( &constant_bin[ 5358410 ], 111, 0 );
    const_dict_b3fa57d622665417c82eaa93a4cf743c = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_b3fa57d622665417c82eaa93a4cf743c, const_str_plain_type, (PyObject *)&PyUnicode_Type );
    assert( PyDict_Size( const_dict_b3fa57d622665417c82eaa93a4cf743c ) == 1 );
    const_str_digest_03a6f790f27598bb7c4371b455bd02c5 = UNSTREAM_STRING_ASCII( &constant_bin[ 5358521 ], 34, 0 );
    const_dict_480a75f460aacdb5be24e0bff4669741 = _PyDict_NewPresized( 8 );
    PyDict_SetItem( const_dict_480a75f460aacdb5be24e0bff4669741, const_str_plain_connect_timeout, const_float_20_0 );
    PyDict_SetItem( const_dict_480a75f460aacdb5be24e0bff4669741, const_str_plain_request_timeout, const_float_20_0 );
    PyDict_SetItem( const_dict_480a75f460aacdb5be24e0bff4669741, const_str_plain_follow_redirects, Py_True );
    PyDict_SetItem( const_dict_480a75f460aacdb5be24e0bff4669741, const_str_plain_max_redirects, const_int_pos_5 );
    PyDict_SetItem( const_dict_480a75f460aacdb5be24e0bff4669741, const_str_plain_decompress_response, Py_True );
    PyDict_SetItem( const_dict_480a75f460aacdb5be24e0bff4669741, const_str_plain_proxy_password, const_str_empty );
    PyDict_SetItem( const_dict_480a75f460aacdb5be24e0bff4669741, const_str_plain_allow_nonstandard_methods, Py_False );
    PyDict_SetItem( const_dict_480a75f460aacdb5be24e0bff4669741, const_str_plain_validate_cert, Py_True );
    assert( PyDict_Size( const_dict_480a75f460aacdb5be24e0bff4669741 ) == 8 );
    const_tuple_str_plain_follow_redirects_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_follow_redirects_tuple, 0, const_str_plain_follow_redirects ); Py_INCREF( const_str_plain_follow_redirects );
    const_str_digest_2291376f8ac5f724b6b701a36ee441fd = UNSTREAM_STRING_ASCII( &constant_bin[ 5358555 ], 40, 0 );
    const_tuple_8530f5cfdba39176f39925bab6b635c8_tuple = PyTuple_New( 7 );
    PyTuple_SET_ITEM( const_tuple_8530f5cfdba39176f39925bab6b635c8_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_8530f5cfdba39176f39925bab6b635c8_tuple, 1, const_str_plain_request ); Py_INCREF( const_str_plain_request );
    PyTuple_SET_ITEM( const_tuple_8530f5cfdba39176f39925bab6b635c8_tuple, 2, const_str_plain_raise_error ); Py_INCREF( const_str_plain_raise_error );
    PyTuple_SET_ITEM( const_tuple_8530f5cfdba39176f39925bab6b635c8_tuple, 3, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_8530f5cfdba39176f39925bab6b635c8_tuple, 4, const_str_plain_request_proxy ); Py_INCREF( const_str_plain_request_proxy );
    PyTuple_SET_ITEM( const_tuple_8530f5cfdba39176f39925bab6b635c8_tuple, 5, const_str_plain_future ); Py_INCREF( const_str_plain_future );
    PyTuple_SET_ITEM( const_tuple_8530f5cfdba39176f39925bab6b635c8_tuple, 6, const_str_plain_handle_response ); Py_INCREF( const_str_plain_handle_response );
    const_str_plain__async_client_dict_ = UNSTREAM_STRING_ASCII( &constant_bin[ 5358595 ], 19, 1 );
    const_str_digest_3b7473fff9725cc62f429dad68799469 = UNSTREAM_STRING_ASCII( &constant_bin[ 5355842 ], 21, 0 );
    const_dict_ae8aa59e73f92c1f97579b3c4dbcc675 = _PyDict_NewPresized( 2 );
    PyDict_SetItem( const_dict_ae8aa59e73f92c1f97579b3c4dbcc675, const_str_plain_type, (PyObject *)&PyBool_Type );
    PyDict_SetItem( const_dict_ae8aa59e73f92c1f97579b3c4dbcc675, const_str_plain_default, Py_False );
    assert( PyDict_Size( const_dict_ae8aa59e73f92c1f97579b3c4dbcc675 ) == 2 );
    const_tuple_e03233a4438f70401fd7c39b76e4dbb5_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_e03233a4438f70401fd7c39b76e4dbb5_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_e03233a4438f70401fd7c39b76e4dbb5_tuple, 1, const_str_plain_async_client_class ); Py_INCREF( const_str_plain_async_client_class );
    PyTuple_SET_ITEM( const_tuple_e03233a4438f70401fd7c39b76e4dbb5_tuple, 2, const_str_plain_kwargs ); Py_INCREF( const_str_plain_kwargs );
    PyTuple_SET_ITEM( const_tuple_e03233a4438f70401fd7c39b76e4dbb5_tuple, 3, const_str_plain_make_client ); Py_INCREF( const_str_plain_make_client );
    const_str_plain__async_clients = UNSTREAM_STRING_ASCII( &constant_bin[ 5358614 ], 14, 1 );
    const_list_str_plain_HTTPResponse_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_HTTPResponse_list, 0, const_str_plain_HTTPResponse ); Py_INCREF( const_str_plain_HTTPResponse );
    const_str_digest_3966c5d48e4aec5eaa9d4d76ce2f44a3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5358628 ], 16, 0 );
    const_tuple_str_plain_cls_str_plain_SimpleAsyncHTTPClient_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_SimpleAsyncHTTPClient_tuple, 0, const_str_plain_cls ); Py_INCREF( const_str_plain_cls );
    PyTuple_SET_ITEM( const_tuple_str_plain_cls_str_plain_SimpleAsyncHTTPClient_tuple, 1, const_str_plain_SimpleAsyncHTTPClient ); Py_INCREF( const_str_plain_SimpleAsyncHTTPClient );
    const_str_digest_7b5c18e05c25516935228d84f6d6dde5 = UNSTREAM_STRING_ASCII( &constant_bin[ 5358644 ], 22, 0 );
    const_str_digest_c3fd672541fb689880c497d1199b14f3 = UNSTREAM_STRING_ASCII( &constant_bin[ 5350715 ], 21, 0 );
    const_tuple_str_plain_proxy_host_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_proxy_host_tuple, 0, const_str_plain_proxy_host ); Py_INCREF( const_str_plain_proxy_host );
    const_str_plain_use_gzip = UNSTREAM_STRING_ASCII( &constant_bin[ 5350340 ], 8, 1 );
    const_str_digest_afb184d09906a65fcfde6b3eefbb19a6 = UNSTREAM_STRING_ASCII( &constant_bin[ 5358666 ], 21, 0 );
    const_str_digest_b7d04337ffd07131655e2e21c568a5d6 = UNSTREAM_STRING_ASCII( &constant_bin[ 5358687 ], 1724, 0 );
    const_str_digest_e01eabb9dfbe5c0e49d4dcf1fbe88c15 = UNSTREAM_STRING_ASCII( &constant_bin[ 5355847 ], 16, 0 );
    const_str_digest_c24969492806251a8bc7b7e0886afacf = UNSTREAM_STRING_ASCII( &constant_bin[ 5360411 ], 33, 0 );
    const_str_digest_dd5b9f91f366a2dcc7dd5058f4640e5c = UNSTREAM_STRING_ASCII( &constant_bin[ 5360444 ], 24, 0 );
    const_tuple_str_plain_utf8_str_plain_native_str_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_utf8_str_plain_native_str_tuple, 0, const_str_plain_utf8 ); Py_INCREF( const_str_plain_utf8 );
    PyTuple_SET_ITEM( const_tuple_str_plain_utf8_str_plain_native_str_tuple, 1, const_str_plain_native_str ); Py_INCREF( const_str_plain_native_str );
    const_str_digest_3f34f04b46b728f42053850d2d2f2e7c = UNSTREAM_STRING_ASCII( &constant_bin[ 5360468 ], 21, 0 );
    const_str_digest_c83e5f43b5f0faf49ff813bd94426c79 = UNSTREAM_STRING_ASCII( &constant_bin[ 5360489 ], 839, 0 );
    const_dict_a7b9314e26045b98fb4910dec4a6aa82 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_a7b9314e26045b98fb4910dec4a6aa82, const_str_plain_type, (PyObject *)&PyLong_Type );
    assert( PyDict_Size( const_dict_a7b9314e26045b98fb4910dec4a6aa82 ) == 1 );
    const_tuple_str_plain_self_str_plain_defaults_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_defaults_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_defaults_tuple, 1, const_str_plain_defaults ); Py_INCREF( const_str_plain_defaults );
    const_tuple_list_str_plain_HTTPResponse_list_none_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_list_str_plain_HTTPResponse_list_none_tuple, 0, const_list_str_plain_HTTPResponse_list ); Py_INCREF( const_list_str_plain_HTTPResponse_list );
    PyTuple_SET_ITEM( const_tuple_list_str_plain_HTTPResponse_list_none_tuple, 1, Py_None ); Py_INCREF( Py_None );
    const_str_digest_0f8e13b4708f8a35e2e860117792b9fe = UNSTREAM_STRING_ASCII( &constant_bin[ 5361328 ], 30, 0 );
    const_tuple_str_plain_self_str_plain_cached_val_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_cached_val_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_cached_val_tuple, 1, const_str_plain_cached_val ); Py_INCREF( const_str_plain_cached_val );
    const_str_digest_1e8b8191e6ee572c2b9ae8835d41eb7f = UNSTREAM_STRING_ASCII( &constant_bin[ 5361358 ], 50, 0 );
    const_tuple_str_plain_response_str_plain_raise_error_str_plain_future_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_response_str_plain_raise_error_str_plain_future_tuple, 0, const_str_plain_response ); Py_INCREF( const_str_plain_response );
    PyTuple_SET_ITEM( const_tuple_str_plain_response_str_plain_raise_error_str_plain_future_tuple, 1, const_str_plain_raise_error ); Py_INCREF( const_str_plain_raise_error );
    PyTuple_SET_ITEM( const_tuple_str_plain_response_str_plain_raise_error_str_plain_future_tuple, 2, const_str_plain_future ); Py_INCREF( const_str_plain_future );
    const_tuple_str_plain_self_str_plain_request_str_plain_defaults_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_request_str_plain_defaults_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_request_str_plain_defaults_tuple, 1, const_str_plain_request ); Py_INCREF( const_str_plain_request );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_request_str_plain_defaults_tuple, 2, const_str_plain_defaults ); Py_INCREF( const_str_plain_defaults );
    const_str_digest_868647966cabcea20bb9bdd8af5cd9da = UNSTREAM_STRING_ASCII( &constant_bin[ 5361408 ], 27, 0 );
    const_str_plain_if_modified_since = UNSTREAM_STRING_ASCII( &constant_bin[ 5350276 ], 17, 1 );
    const_str_plain__async_client = UNSTREAM_STRING_ASCII( &constant_bin[ 5358595 ], 13, 1 );
    const_str_digest_5cfef7f95e02358e66acc192938bb47e = UNSTREAM_STRING_ASCII( &constant_bin[ 5361435 ], 6322, 0 );
    const_str_digest_a1f1d5c51f21741d345f323ecee64ae1 = UNSTREAM_STRING_ASCII( &constant_bin[ 5367757 ], 25, 0 );
    const_tuple_str_plain_self_str_plain_name_str_plain_request_attr_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_name_str_plain_request_attr_tuple, 0, const_str_plain_self ); Py_INCREF( const_str_plain_self );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_name_str_plain_request_attr_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_str_plain_request_attr = UNSTREAM_STRING_ASCII( &constant_bin[ 5367782 ], 12, 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_self_str_plain_name_str_plain_request_attr_tuple, 2, const_str_plain_request_attr ); Py_INCREF( const_str_plain_request_attr );
    const_tuple_d419ed77ca3e651e427938c507014e9e_tuple = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 5367794 ], 38 );
    const_str_digest_2637eb19e85ee47cbbc364b199fa7f11 = UNSTREAM_STRING_ASCII( &constant_bin[ 5367832 ], 20, 0 );
    const_str_digest_2637714cd79b52e756b2393287778142 = UNSTREAM_STRING_ASCII( &constant_bin[ 5367852 ], 23, 0 );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_tornado$httpclient( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_bd02e7133760dec6a20885e7a008916c;
static PyCodeObject *codeobj_e51e71200c4d41bba590fb0c4f6851d0;
static PyCodeObject *codeobj_5ab4f6dbba8ac4d4a89799531de9c715;
static PyCodeObject *codeobj_63d8f7595cf2a14b5cdc744a245bc71e;
static PyCodeObject *codeobj_8653d73da3de6cf1359d78767acabc70;
static PyCodeObject *codeobj_3c585940a00c16cc3fd3083c84c8ac0a;
static PyCodeObject *codeobj_ed5be14316d8a2e2facc323ac7ebc012;
static PyCodeObject *codeobj_0d8ab451af9278fbd65fc25aa6fc0cb3;
static PyCodeObject *codeobj_b60cd2325a77cff95cc166ab3d03ea88;
static PyCodeObject *codeobj_0e20bad896a4422afeb795351bab726a;
static PyCodeObject *codeobj_cb175ae3e77fd1961b670de199c01c57;
static PyCodeObject *codeobj_51b641b6a50b5ff292086f3f52f64be5;
static PyCodeObject *codeobj_722efbae57c0bf39d22d7c04dffb6d9f;
static PyCodeObject *codeobj_e82fccd137c01e8770baa23fc14bdbff;
static PyCodeObject *codeobj_6e307838b588317efea4ea20564a9b52;
static PyCodeObject *codeobj_79fbdd10bb9e61809342ea24b6a19499;
static PyCodeObject *codeobj_d4736e092cab086b98221122b05139d0;
static PyCodeObject *codeobj_6b3619961e2f6e053d8497a025234733;
static PyCodeObject *codeobj_56fcdc3ec0cc288b70e5c021245c863d;
static PyCodeObject *codeobj_3c36f9c232036606bbbd432d349ddfa8;
static PyCodeObject *codeobj_d7e781f31b6acc7ad3976fc72cd431a0;
static PyCodeObject *codeobj_71afdc46e7a96471c6239fb00dc242e8;
static PyCodeObject *codeobj_d92002cbbc3b75ffa1988a8cdeb54d2d;
static PyCodeObject *codeobj_9a7ec33ce2c27a7376ec35db69f3bda8;
static PyCodeObject *codeobj_c24a659a5d00f17352a387a27bb0d03e;
static PyCodeObject *codeobj_ce5cfbe92344c9f4f7c47b27ed8bbe1a;
static PyCodeObject *codeobj_d911a993b10fba49163895c1c4d188fa;
static PyCodeObject *codeobj_03b8157c632b8cf42f1a152fc9d1a1fb;
static PyCodeObject *codeobj_d24ab2ae782140ba0106d4042d210003;
static PyCodeObject *codeobj_18f23f5a7e6fe4e9cd0bf09e0959019a;
static PyCodeObject *codeobj_b31cc5cce776a7d51eeb572d3309127c;
static PyCodeObject *codeobj_18eaf614fcb3f563067213568d8d3711;
static PyCodeObject *codeobj_fb2562f3267c33570dc44f4893e3a2c4;
static PyCodeObject *codeobj_2f8fa415e313f31cf65e08fde0777d5f;
static PyCodeObject *codeobj_07edb31f42cc29ab8bbf8de92f57e171;
static PyCodeObject *codeobj_4d6b5db01563778c1d19c42ea72f07a8;
static PyCodeObject *codeobj_0345d03b190573e043fea91e5df23663;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_3f34f04b46b728f42053850d2d2f2e7c );
    codeobj_bd02e7133760dec6a20885e7a008916c = MAKE_CODEOBJ( module_filename_obj, const_str_angle_genexpr, 680, const_tuple_str_digest_b9c4baf879ebd882d40843df3a4dead7_str_plain_i_tuple, 1, 0, CO_GENERATOR | CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e51e71200c4d41bba590fb0c4f6851d0 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_868647966cabcea20bb9bdd8af5cd9da, 1, const_tuple_empty, 0, 0, CO_NOFREE );
    codeobj_5ab4f6dbba8ac4d4a89799531de9c715 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_AsyncHTTPClient, 138, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_63d8f7595cf2a14b5cdc744a245bc71e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_HTTPClient, 59, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_8653d73da3de6cf1359d78767acabc70 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_HTTPClientError, 684, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_3c585940a00c16cc3fd3083c84c8ac0a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_HTTPRequest, 337, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_ed5be14316d8a2e2facc323ac7ebc012 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_HTTPResponse, 568, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_0d8ab451af9278fbd65fc25aa6fc0cb3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__RequestProxy, 726, const_tuple_str_plain___class___tuple, 0, 0, CO_NOFREE );
    codeobj_b60cd2325a77cff95cc166ab3d03ea88 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___del__, 110, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_0e20bad896a4422afeb795351bab726a = MAKE_CODEOBJ( module_filename_obj, const_str_plain___getattr__, 738, const_tuple_str_plain_self_str_plain_name_str_plain_request_attr_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_cb175ae3e77fd1961b670de199c01c57 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 89, const_tuple_e03233a4438f70401fd7c39b76e4dbb5_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_51b641b6a50b5ff292086f3f52f64be5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 705, const_tuple_491035fcbadc3d1f2429fd4f40586059_tuple, 4, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_722efbae57c0bf39d22d7c04dffb6d9f = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 623, const_tuple_1726b9270bfea6e6daa6649c411deecf_tuple, 11, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_e82fccd137c01e8770baa23fc14bdbff = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 732, const_tuple_str_plain_self_str_plain_request_str_plain_defaults_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6e307838b588317efea4ea20564a9b52 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___init__, 356, const_tuple_ec7d9dd540745381d3553e815d2b49ff_tuple, 34, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_79fbdd10bb9e61809342ea24b6a19499 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___new__, 196, const_tuple_d13c1ccf0f7e46b28fae431fffa73548_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS );
    codeobj_d4736e092cab086b98221122b05139d0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___repr__, 679, const_tuple_str_plain_self_str_plain_args_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_6b3619961e2f6e053d8497a025234733 = MAKE_CODEOBJ( module_filename_obj, const_str_plain___str__, 713, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_56fcdc3ec0cc288b70e5c021245c863d = MAKE_CODEOBJ( module_filename_obj, const_str_plain__async_clients, 189, const_tuple_str_plain_cls_str_plain_attr_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_3c36f9c232036606bbbd432d349ddfa8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_body, 559, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d7e781f31b6acc7ad3976fc72cd431a0 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_body, 665, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_71afdc46e7a96471c6239fb00dc242e8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_body, 563, const_tuple_str_plain_self_str_plain_value_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d92002cbbc3b75ffa1988a8cdeb54d2d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_close, 113, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_9a7ec33ce2c27a7376ec35db69f3bda8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_close, 221, const_tuple_str_plain_self_str_plain_cached_val_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_c24a659a5d00f17352a387a27bb0d03e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_configurable_base, 179, const_tuple_str_plain_cls_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_ce5cfbe92344c9f4f7c47b27ed8bbe1a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_configurable_default, 183, const_tuple_str_plain_cls_str_plain_SimpleAsyncHTTPClient_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_d911a993b10fba49163895c1c4d188fa = MAKE_CODEOBJ( module_filename_obj, const_str_plain_configure, 312, const_tuple_e064a1faf239cdd37f9d7015f03bd688_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS );
    codeobj_03b8157c632b8cf42f1a152fc9d1a1fb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_fetch, 120, const_tuple_15a3f697abc1e545999d43ae8c0a3dba_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_d24ab2ae782140ba0106d4042d210003 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_fetch, 247, const_tuple_8530f5cfdba39176f39925bab6b635c8_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_VARKEYWORDS | CO_NOFREE );
    codeobj_18f23f5a7e6fe4e9cd0bf09e0959019a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_fetch_impl, 307, const_tuple_str_plain_self_str_plain_request_str_plain_callback_tuple, 3, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_b31cc5cce776a7d51eeb572d3309127c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_handle_response, 297, const_tuple_str_plain_response_str_plain_raise_error_str_plain_future_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_18eaf614fcb3f563067213568d8d3711 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_headers, 545, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_fb2562f3267c33570dc44f4893e3a2c4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_headers, 552, const_tuple_str_plain_self_str_plain_value_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_2f8fa415e313f31cf65e08fde0777d5f = MAKE_CODEOBJ( module_filename_obj, const_str_plain_initialize, 214, const_tuple_str_plain_self_str_plain_defaults_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_07edb31f42cc29ab8bbf8de92f57e171 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_main, 748, const_tuple_ace1a868d90fa685266e16c0a1a992df_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
    codeobj_4d6b5db01563778c1d19c42ea72f07a8 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_make_client, 102, const_tuple_str_plain_async_client_class_str_plain_kwargs_tuple, 0, 0, CO_COROUTINE | CO_OPTIMIZED | CO_NEWLOCALS );
    codeobj_0345d03b190573e043fea91e5df23663 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_rethrow, 674, const_tuple_str_plain_self_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE );
}

// The module function declarations.
static PyObject *tornado$httpclient$$$function_1___init__$$$function_1_make_client$$$coroutine_1_make_client_maker( void );


static PyObject *tornado$httpclient$$$function_22___repr__$$$genexpr_1_genexpr_maker( void );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_1__mro_entries_conversion( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_2_complex_call_helper_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_6_complex_call_helper_pos_star_dict( PyObject **python_pars );


NUITKA_CROSS_MODULE PyObject *impl___internal__$$$function_10_complex_call_helper_keywords_star_dict( PyObject **python_pars );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_10_close( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_11_fetch( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_11_fetch$$$function_1_handle_response( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_12_fetch_impl( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_13_configure( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_14___init__( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_15_headers( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_16_headers( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_17_body( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_18_body( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_19___init__( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_1___init__( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_1___init__$$$function_1_make_client( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_20_body( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_21_rethrow( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_22___repr__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_23___init__( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_24___str__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_25___init__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_26___getattr__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_27_main( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_2___del__( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_3_close( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_4_fetch( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_5_configurable_base( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_6_configurable_default( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_7__async_clients( PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_8___new__( PyObject *defaults, PyObject *annotations );


static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_9_initialize( PyObject *defaults, PyObject *annotations );


// The module function definitions.
static PyObject *impl_tornado$httpclient$$$function_1___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_CellObject *par_async_client_class = PyCell_NEW1( python_pars[ 1 ] );
    struct Nuitka_CellObject *par_kwargs = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *var_make_client = NULL;
    struct Nuitka_FrameObject *frame_cb175ae3e77fd1961b670de199c01c57;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_cb175ae3e77fd1961b670de199c01c57 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_cb175ae3e77fd1961b670de199c01c57, codeobj_cb175ae3e77fd1961b670de199c01c57, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_cb175ae3e77fd1961b670de199c01c57 = cache_frame_cb175ae3e77fd1961b670de199c01c57;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_cb175ae3e77fd1961b670de199c01c57 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_cb175ae3e77fd1961b670de199c01c57 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__closed, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 95;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_assattr_target_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_IOLoop );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOLoop );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOLoop" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 96;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_kw_name_1 = PyDict_Copy( const_dict_751ed0ee3c51c52082c6a2bb9cbce741 );
        frame_cb175ae3e77fd1961b670de199c01c57->m_frame.f_lineno = 96;
        tmp_assattr_name_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__io_loop, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 96;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( PyCell_GET( par_async_client_class ) );
        tmp_compexpr_left_1 = PyCell_GET( par_async_client_class );
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_mvar_value_2;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_AsyncHTTPClient );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AsyncHTTPClient );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AsyncHTTPClient" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 98;
                type_description_1 = "occo";
                goto frame_exception_exit_1;
            }

            tmp_assign_source_1 = tmp_mvar_value_2;
            {
                PyObject *old = PyCell_GET( par_async_client_class );
                PyCell_SET( par_async_client_class, tmp_assign_source_1 );
                Py_INCREF( tmp_assign_source_1 );
                Py_XDECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_annotations_1;
        tmp_annotations_1 = PyDict_Copy( const_dict_4fb07ff838d1a4c46118c49ac78379b1 );
        tmp_assign_source_2 = MAKE_FUNCTION_tornado$httpclient$$$function_1___init__$$$function_1_make_client( tmp_annotations_1 );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] = par_async_client_class;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] = par_kwargs;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_2)->m_closure[1] );


        assert( var_make_client == NULL );
        var_make_client = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__io_loop );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_make_client );
        tmp_args_element_name_1 = var_make_client;
        frame_cb175ae3e77fd1961b670de199c01c57->m_frame.f_lineno = 107;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_run_sync, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assattr_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__async_client, tmp_assattr_name_3 );
        Py_DECREF( tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 107;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        tmp_assattr_name_4 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain__closed, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 108;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb175ae3e77fd1961b670de199c01c57 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_cb175ae3e77fd1961b670de199c01c57 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_cb175ae3e77fd1961b670de199c01c57, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_cb175ae3e77fd1961b670de199c01c57->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_cb175ae3e77fd1961b670de199c01c57, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_cb175ae3e77fd1961b670de199c01c57,
        type_description_1,
        par_self,
        par_async_client_class,
        par_kwargs,
        var_make_client
    );


    // Release cached frame.
    if ( frame_cb175ae3e77fd1961b670de199c01c57 == cache_frame_cb175ae3e77fd1961b670de199c01c57 )
    {
        Py_DECREF( frame_cb175ae3e77fd1961b670de199c01c57 );
    }
    cache_frame_cb175ae3e77fd1961b670de199c01c57 = NULL;

    assertFrameObject( frame_cb175ae3e77fd1961b670de199c01c57 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_1___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_async_client_class );
    Py_DECREF( par_async_client_class );
    par_async_client_class = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_make_client );
    Py_DECREF( var_make_client );
    var_make_client = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_async_client_class );
    Py_DECREF( par_async_client_class );
    par_async_client_class = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_make_client );
    var_make_client = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_1___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_1___init__$$$function_1_make_client( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    tmp_return_value = tornado$httpclient$$$function_1___init__$$$function_1_make_client$$$coroutine_1_make_client_maker();

    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] = self->m_closure[0];
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[0] );
    ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] = self->m_closure[1];
    Py_INCREF( ((struct Nuitka_CoroutineObject *)tmp_return_value)->m_closure[1] );


    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_1___init__$$$function_1_make_client );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct tornado$httpclient$$$function_1___init__$$$function_1_make_client$$$coroutine_1_make_client_locals {
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *tmp_return_value;
};

static PyObject *tornado$httpclient$$$function_1___init__$$$function_1_make_client$$$coroutine_1_make_client_context( struct Nuitka_CoroutineObject *coroutine, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)coroutine );
    assert( Nuitka_Coroutine_Check( (PyObject *)coroutine ) );

    // Heap access if used.
    struct tornado$httpclient$$$function_1___init__$$$function_1_make_client$$$coroutine_1_make_client_locals *coroutine_heap = (struct tornado$httpclient$$$function_1___init__$$$function_1_make_client$$$coroutine_1_make_client_locals *)coroutine->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(coroutine->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    coroutine_heap->type_description_1 = NULL;
    coroutine_heap->exception_type = NULL;
    coroutine_heap->exception_value = NULL;
    coroutine_heap->exception_tb = NULL;
    coroutine_heap->exception_lineno = 0;
    coroutine_heap->tmp_return_value = NULL;

    // Actual coroutine body.
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_4d6b5db01563778c1d19c42ea72f07a8, module_tornado$httpclient, sizeof(void *)+sizeof(void *) );
    coroutine->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( coroutine->m_frame );
    assert( Py_REFCNT( coroutine->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    coroutine->m_frame->m_frame.f_gen = (PyObject *)coroutine;
#endif

    Py_CLEAR( coroutine->m_frame->m_frame.f_back );

    coroutine->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( coroutine->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &coroutine->m_frame->m_frame;
    Py_INCREF( coroutine->m_frame );

    Nuitka_Frame_MarkAsExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        coroutine->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( coroutine->m_frame->m_frame.f_exc_type == Py_None ) coroutine->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_type );
    coroutine->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_value );
    coroutine->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_frame->m_frame.f_exc_traceback );
#else
        coroutine->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( coroutine->m_exc_state.exc_type == Py_None ) coroutine->m_exc_state.exc_type = NULL;
        Py_XINCREF( coroutine->m_exc_state.exc_type );
        coroutine->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_value );
        coroutine->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( coroutine->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_expression_name_2;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_await_result_1;
        coroutine->m_frame->m_frame.f_lineno = 103;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_gen );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_gen );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "gen" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 103;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        coroutine->m_frame->m_frame.f_lineno = 103;
        tmp_expression_name_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_sleep, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        if ( tmp_expression_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 103;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        tmp_expression_name_1 = ASYNC_AWAIT( tmp_expression_name_2, await_normal );
        Py_DECREF( tmp_expression_name_2 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 103;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        Nuitka_PreserveHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), NULL );
        coroutine->m_yield_return_index = 1;
        coroutine->m_yieldfrom = tmp_expression_name_1;
        coroutine->m_awaiting = true;
        return NULL;

        yield_return_1:
        Nuitka_RestoreHeap( coroutine_heap->yield_tmps, &tmp_expression_name_2, sizeof(PyObject *), &tmp_called_instance_1, sizeof(PyObject *), &tmp_mvar_value_1, sizeof(PyObject *), NULL );
        coroutine->m_awaiting = false;

        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 103;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        tmp_await_result_1 = yield_return_value;
        if ( tmp_await_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 103;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_await_result_1 );
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "async_client_class" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 104;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = PyCell_GET( coroutine->m_closure[0] );
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            coroutine_heap->exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            coroutine_heap->exception_lineno = 104;
            RAISE_EXCEPTION_WITH_TYPE( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_dircall_arg2_1;
        if ( PyCell_GET( coroutine->m_closure[0] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "async_client_class" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 105;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg1_1 = PyCell_GET( coroutine->m_closure[0] );
        if ( PyCell_GET( coroutine->m_closure[1] ) == NULL )
        {

            coroutine_heap->exception_type = PyExc_NameError;
            Py_INCREF( coroutine_heap->exception_type );
            coroutine_heap->exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "kwargs" );
            coroutine_heap->exception_tb = NULL;
            NORMALIZE_EXCEPTION( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );
            CHAIN_EXCEPTION( coroutine_heap->exception_value );

            coroutine_heap->exception_lineno = 105;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_dircall_arg2_1 = PyCell_GET( coroutine->m_closure[1] );
        Py_INCREF( tmp_dircall_arg1_1 );
        Py_INCREF( tmp_dircall_arg2_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1};
            coroutine_heap->tmp_return_value = impl___internal__$$$function_2_complex_call_helper_star_dict( dir_call_args );
        }
        if ( coroutine_heap->tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &coroutine_heap->exception_type, &coroutine_heap->exception_value, &coroutine_heap->exception_tb );


            coroutine_heap->exception_lineno = 105;
            coroutine_heap->type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

    Nuitka_Frame_MarkAsNotExecuting( coroutine->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( coroutine->m_frame );
    goto frame_no_exception_1;

    frame_return_exit_1:;

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );
    goto function_return_exit;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( coroutine_heap->exception_type ) )
    {
        if ( coroutine_heap->exception_tb == NULL )
        {
            coroutine_heap->exception_tb = MAKE_TRACEBACK( coroutine->m_frame, coroutine_heap->exception_lineno );
        }
        else if ( coroutine_heap->exception_tb->tb_frame != &coroutine->m_frame->m_frame )
        {
            coroutine_heap->exception_tb = ADD_TRACEBACK( coroutine_heap->exception_tb, coroutine->m_frame, coroutine_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)coroutine->m_frame,
            coroutine_heap->type_description_1,
            coroutine->m_closure[0],
            coroutine->m_closure[1]
        );


        // Release cached frame.
        if ( coroutine->m_frame == cache_m_frame )
        {
            Py_DECREF( coroutine->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( coroutine->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( coroutine->m_exc_state.exc_type );
    Py_CLEAR( coroutine->m_exc_state.exc_value );
    Py_CLEAR( coroutine->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_type );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_value );
    Py_CLEAR( coroutine->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( coroutine->m_frame );

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must be present.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_1___init__$$$function_1_make_client$$$coroutine_1_make_client );

    function_exception_exit:
    assert( coroutine_heap->exception_type );
    RESTORE_ERROR_OCCURRED( coroutine_heap->exception_type, coroutine_heap->exception_value, coroutine_heap->exception_tb );
    return NULL;
    function_return_exit:;

    coroutine->m_returned = coroutine_heap->tmp_return_value;

    return NULL;

}

static PyObject *tornado$httpclient$$$function_1___init__$$$function_1_make_client$$$coroutine_1_make_client_maker( void )
{
    return Nuitka_Coroutine_New(
        tornado$httpclient$$$function_1___init__$$$function_1_make_client$$$coroutine_1_make_client_context,
        module_tornado$httpclient,
        const_str_plain_make_client,
        const_str_digest_2291376f8ac5f724b6b701a36ee441fd,
        codeobj_4d6b5db01563778c1d19c42ea72f07a8,
        2,
        sizeof(struct tornado$httpclient$$$function_1___init__$$$function_1_make_client$$$coroutine_1_make_client_locals)
    );
}


static PyObject *impl_tornado$httpclient$$$function_2___del__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b60cd2325a77cff95cc166ab3d03ea88;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_b60cd2325a77cff95cc166ab3d03ea88 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b60cd2325a77cff95cc166ab3d03ea88, codeobj_b60cd2325a77cff95cc166ab3d03ea88, module_tornado$httpclient, sizeof(void *) );
    frame_b60cd2325a77cff95cc166ab3d03ea88 = cache_frame_b60cd2325a77cff95cc166ab3d03ea88;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b60cd2325a77cff95cc166ab3d03ea88 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b60cd2325a77cff95cc166ab3d03ea88 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_self );
        tmp_called_instance_1 = par_self;
        frame_b60cd2325a77cff95cc166ab3d03ea88->m_frame.f_lineno = 111;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_close );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 111;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b60cd2325a77cff95cc166ab3d03ea88 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b60cd2325a77cff95cc166ab3d03ea88 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b60cd2325a77cff95cc166ab3d03ea88, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b60cd2325a77cff95cc166ab3d03ea88->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b60cd2325a77cff95cc166ab3d03ea88, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b60cd2325a77cff95cc166ab3d03ea88,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_b60cd2325a77cff95cc166ab3d03ea88 == cache_frame_b60cd2325a77cff95cc166ab3d03ea88 )
    {
        Py_DECREF( frame_b60cd2325a77cff95cc166ab3d03ea88 );
    }
    cache_frame_b60cd2325a77cff95cc166ab3d03ea88 = NULL;

    assertFrameObject( frame_b60cd2325a77cff95cc166ab3d03ea88 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_2___del__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_2___del__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_3_close( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d92002cbbc3b75ffa1988a8cdeb54d2d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_d92002cbbc3b75ffa1988a8cdeb54d2d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d92002cbbc3b75ffa1988a8cdeb54d2d, codeobj_d92002cbbc3b75ffa1988a8cdeb54d2d, module_tornado$httpclient, sizeof(void *) );
    frame_d92002cbbc3b75ffa1988a8cdeb54d2d = cache_frame_d92002cbbc3b75ffa1988a8cdeb54d2d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d92002cbbc3b75ffa1988a8cdeb54d2d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d92002cbbc3b75ffa1988a8cdeb54d2d ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__closed );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__async_client );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_d92002cbbc3b75ffa1988a8cdeb54d2d->m_frame.f_lineno = 116;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_close );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 116;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_3 = par_self;
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain__io_loop );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 117;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            frame_d92002cbbc3b75ffa1988a8cdeb54d2d->m_frame.f_lineno = 117;
            tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_close );
            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 117;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            tmp_assattr_name_1 = Py_True;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_1 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__closed, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 118;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d92002cbbc3b75ffa1988a8cdeb54d2d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d92002cbbc3b75ffa1988a8cdeb54d2d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d92002cbbc3b75ffa1988a8cdeb54d2d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d92002cbbc3b75ffa1988a8cdeb54d2d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d92002cbbc3b75ffa1988a8cdeb54d2d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d92002cbbc3b75ffa1988a8cdeb54d2d,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_d92002cbbc3b75ffa1988a8cdeb54d2d == cache_frame_d92002cbbc3b75ffa1988a8cdeb54d2d )
    {
        Py_DECREF( frame_d92002cbbc3b75ffa1988a8cdeb54d2d );
    }
    cache_frame_d92002cbbc3b75ffa1988a8cdeb54d2d = NULL;

    assertFrameObject( frame_d92002cbbc3b75ffa1988a8cdeb54d2d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_3_close );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_3_close );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_4_fetch( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_request = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_response = NULL;
    struct Nuitka_FrameObject *frame_03b8157c632b8cf42f1a152fc9d1a1fb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_03b8157c632b8cf42f1a152fc9d1a1fb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_03b8157c632b8cf42f1a152fc9d1a1fb, codeobj_03b8157c632b8cf42f1a152fc9d1a1fb, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_03b8157c632b8cf42f1a152fc9d1a1fb = cache_frame_03b8157c632b8cf42f1a152fc9d1a1fb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_03b8157c632b8cf42f1a152fc9d1a1fb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_03b8157c632b8cf42f1a152fc9d1a1fb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_dircall_arg3_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__io_loop );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_run_sync );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_functools );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_functools );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "functools" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_1;
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_partial );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 133;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__async_client );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_dircall_arg1_1 );

            exception_lineno = 133;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_fetch );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_dircall_arg1_1 );

            exception_lineno = 133;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg2_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_request );
        tmp_tuple_element_1 = par_request;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 1, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_args_element_name_1 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 133;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        frame_03b8157c632b8cf42f1a152fc9d1a1fb->m_frame.f_lineno = 132;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_response == NULL );
        var_response = tmp_assign_source_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_03b8157c632b8cf42f1a152fc9d1a1fb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_03b8157c632b8cf42f1a152fc9d1a1fb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_03b8157c632b8cf42f1a152fc9d1a1fb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_03b8157c632b8cf42f1a152fc9d1a1fb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_03b8157c632b8cf42f1a152fc9d1a1fb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_03b8157c632b8cf42f1a152fc9d1a1fb,
        type_description_1,
        par_self,
        par_request,
        par_kwargs,
        var_response
    );


    // Release cached frame.
    if ( frame_03b8157c632b8cf42f1a152fc9d1a1fb == cache_frame_03b8157c632b8cf42f1a152fc9d1a1fb )
    {
        Py_DECREF( frame_03b8157c632b8cf42f1a152fc9d1a1fb );
    }
    cache_frame_03b8157c632b8cf42f1a152fc9d1a1fb = NULL;

    assertFrameObject( frame_03b8157c632b8cf42f1a152fc9d1a1fb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_response );
    tmp_return_value = var_response;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_4_fetch );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_response );
    Py_DECREF( var_response );
    var_response = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_4_fetch );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_5_configurable_base( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c24a659a5d00f17352a387a27bb0d03e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c24a659a5d00f17352a387a27bb0d03e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c24a659a5d00f17352a387a27bb0d03e, codeobj_c24a659a5d00f17352a387a27bb0d03e, module_tornado$httpclient, sizeof(void *) );
    frame_c24a659a5d00f17352a387a27bb0d03e = cache_frame_c24a659a5d00f17352a387a27bb0d03e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c24a659a5d00f17352a387a27bb0d03e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c24a659a5d00f17352a387a27bb0d03e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_AsyncHTTPClient );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AsyncHTTPClient );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AsyncHTTPClient" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 181;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_return_value = tmp_mvar_value_1;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c24a659a5d00f17352a387a27bb0d03e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c24a659a5d00f17352a387a27bb0d03e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c24a659a5d00f17352a387a27bb0d03e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c24a659a5d00f17352a387a27bb0d03e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c24a659a5d00f17352a387a27bb0d03e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c24a659a5d00f17352a387a27bb0d03e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c24a659a5d00f17352a387a27bb0d03e,
        type_description_1,
        par_cls
    );


    // Release cached frame.
    if ( frame_c24a659a5d00f17352a387a27bb0d03e == cache_frame_c24a659a5d00f17352a387a27bb0d03e )
    {
        Py_DECREF( frame_c24a659a5d00f17352a387a27bb0d03e );
    }
    cache_frame_c24a659a5d00f17352a387a27bb0d03e = NULL;

    assertFrameObject( frame_c24a659a5d00f17352a387a27bb0d03e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_5_configurable_base );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_5_configurable_base );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_6_configurable_default( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *var_SimpleAsyncHTTPClient = NULL;
    struct Nuitka_FrameObject *frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a, codeobj_ce5cfbe92344c9f4f7c47b27ed8bbe1a, module_tornado$httpclient, sizeof(void *)+sizeof(void *) );
    frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a = cache_frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_b6a6369109180527d448642da15ecbc0;
        tmp_globals_name_1 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_SimpleAsyncHTTPClient_tuple;
        tmp_level_name_1 = const_int_0;
        frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a->m_frame.f_lineno = 185;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_SimpleAsyncHTTPClient );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 185;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_SimpleAsyncHTTPClient == NULL );
        var_SimpleAsyncHTTPClient = tmp_assign_source_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a,
        type_description_1,
        par_cls,
        var_SimpleAsyncHTTPClient
    );


    // Release cached frame.
    if ( frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a == cache_frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a )
    {
        Py_DECREF( frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a );
    }
    cache_frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a = NULL;

    assertFrameObject( frame_ce5cfbe92344c9f4f7c47b27ed8bbe1a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_SimpleAsyncHTTPClient );
    tmp_return_value = var_SimpleAsyncHTTPClient;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_6_configurable_default );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)var_SimpleAsyncHTTPClient );
    Py_DECREF( var_SimpleAsyncHTTPClient );
    var_SimpleAsyncHTTPClient = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_6_configurable_default );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_7__async_clients( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *var_attr_name = NULL;
    struct Nuitka_FrameObject *frame_56fcdc3ec0cc288b70e5c021245c863d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_56fcdc3ec0cc288b70e5c021245c863d = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_56fcdc3ec0cc288b70e5c021245c863d, codeobj_56fcdc3ec0cc288b70e5c021245c863d, module_tornado$httpclient, sizeof(void *)+sizeof(void *) );
    frame_56fcdc3ec0cc288b70e5c021245c863d = cache_frame_56fcdc3ec0cc288b70e5c021245c863d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_56fcdc3ec0cc288b70e5c021245c863d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_56fcdc3ec0cc288b70e5c021245c863d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_1;
        tmp_left_name_1 = const_str_plain__async_client_dict_;
        CHECK_OBJECT( par_cls );
        tmp_source_name_1 = par_cls;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___name__ );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_attr_name == NULL );
        var_attr_name = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_attribute_name_1;
        CHECK_OBJECT( par_cls );
        tmp_source_name_2 = par_cls;
        CHECK_OBJECT( var_attr_name );
        tmp_attribute_name_1 = var_attr_name;
        tmp_res = BUILTIN_HASATTR_BOOL( tmp_source_name_2, tmp_attribute_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_setattr_target_1;
            PyObject *tmp_setattr_attr_1;
            PyObject *tmp_setattr_value_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_capi_result_1;
            CHECK_OBJECT( par_cls );
            tmp_setattr_target_1 = par_cls;
            CHECK_OBJECT( var_attr_name );
            tmp_setattr_attr_1 = var_attr_name;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_weakref );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_weakref );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "weakref" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 193;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            frame_56fcdc3ec0cc288b70e5c021245c863d->m_frame.f_lineno = 193;
            tmp_setattr_value_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_WeakKeyDictionary );
            if ( tmp_setattr_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 193;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_capi_result_1 = BUILTIN_SETATTR( tmp_setattr_target_1, tmp_setattr_attr_1, tmp_setattr_value_1 );
            Py_DECREF( tmp_setattr_value_1 );
            if ( tmp_capi_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 193;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_getattr_attr_1;
        CHECK_OBJECT( par_cls );
        tmp_getattr_target_1 = par_cls;
        CHECK_OBJECT( var_attr_name );
        tmp_getattr_attr_1 = var_attr_name;
        tmp_return_value = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_56fcdc3ec0cc288b70e5c021245c863d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_56fcdc3ec0cc288b70e5c021245c863d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_56fcdc3ec0cc288b70e5c021245c863d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_56fcdc3ec0cc288b70e5c021245c863d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_56fcdc3ec0cc288b70e5c021245c863d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_56fcdc3ec0cc288b70e5c021245c863d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_56fcdc3ec0cc288b70e5c021245c863d,
        type_description_1,
        par_cls,
        var_attr_name
    );


    // Release cached frame.
    if ( frame_56fcdc3ec0cc288b70e5c021245c863d == cache_frame_56fcdc3ec0cc288b70e5c021245c863d )
    {
        Py_DECREF( frame_56fcdc3ec0cc288b70e5c021245c863d );
    }
    cache_frame_56fcdc3ec0cc288b70e5c021245c863d = NULL;

    assertFrameObject( frame_56fcdc3ec0cc288b70e5c021245c863d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_7__async_clients );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)var_attr_name );
    Py_DECREF( var_attr_name );
    var_attr_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    Py_XDECREF( var_attr_name );
    var_attr_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_7__async_clients );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_8___new__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_force_instance = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    PyObject *var_io_loop = NULL;
    PyObject *var_instance_cache = NULL;
    PyObject *var_instance = NULL;
    struct Nuitka_FrameObject *frame_79fbdd10bb9e61809342ea24b6a19499;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_79fbdd10bb9e61809342ea24b6a19499 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_79fbdd10bb9e61809342ea24b6a19499, codeobj_79fbdd10bb9e61809342ea24b6a19499, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_79fbdd10bb9e61809342ea24b6a19499 = cache_frame_79fbdd10bb9e61809342ea24b6a19499;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_79fbdd10bb9e61809342ea24b6a19499 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_79fbdd10bb9e61809342ea24b6a19499 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_IOLoop );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOLoop );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOLoop" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 197;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_79fbdd10bb9e61809342ea24b6a19499->m_frame.f_lineno = 197;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_current );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        assert( var_io_loop == NULL );
        var_io_loop = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_force_instance );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_force_instance );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 198;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            tmp_assign_source_2 = Py_None;
            assert( var_instance_cache == NULL );
            Py_INCREF( tmp_assign_source_2 );
            var_instance_cache = tmp_assign_source_2;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_instance_2;
            CHECK_OBJECT( par_cls );
            tmp_called_instance_2 = par_cls;
            frame_79fbdd10bb9e61809342ea24b6a19499->m_frame.f_lineno = 201;
            tmp_assign_source_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain__async_clients );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 201;
                type_description_1 = "ooooooN";
                goto frame_exception_exit_1;
            }
            assert( var_instance_cache == NULL );
            var_instance_cache = tmp_assign_source_3;
        }
        branch_end_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( var_instance_cache );
        tmp_compexpr_left_1 = var_instance_cache;
        tmp_compexpr_right_1 = Py_None;
        tmp_and_left_value_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_io_loop );
        tmp_compexpr_left_2 = var_io_loop;
        CHECK_OBJECT( var_instance_cache );
        tmp_compexpr_right_2 = var_instance_cache;
        tmp_res = PySequence_Contains( tmp_compexpr_right_2, tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_2 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_2 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( var_instance_cache );
            tmp_subscribed_name_1 = var_instance_cache;
            CHECK_OBJECT( var_io_loop );
            tmp_subscript_name_1 = var_io_loop;
            tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            if ( tmp_return_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 203;
                type_description_1 = "ooooooN";
                goto frame_exception_exit_1;
            }
            goto frame_return_exit_1;
        }
        branch_no_2:;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_object_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_AsyncHTTPClient );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AsyncHTTPClient );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AsyncHTTPClient" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 204;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_cls );
        tmp_object_name_1 = par_cls;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 204;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain___new__ );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 204;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_cls );
        tmp_tuple_element_1 = par_cls;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_assign_source_4 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 204;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
        assert( var_instance == NULL );
        var_instance = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( var_instance_cache );
        tmp_assattr_name_1 = var_instance_cache;
        CHECK_OBJECT( var_instance );
        tmp_assattr_target_1 = var_instance;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__instance_cache, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 209;
            type_description_1 = "ooooooN";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        CHECK_OBJECT( var_instance_cache );
        tmp_compexpr_left_3 = var_instance_cache;
        tmp_compexpr_right_3 = Py_None;
        tmp_condition_result_3 = ( tmp_compexpr_left_3 != tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_ass_subscript_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( var_instance );
            tmp_ass_subvalue_1 = var_instance;
            CHECK_OBJECT( var_instance_cache );
            tmp_ass_subscribed_1 = var_instance_cache;
            CHECK_OBJECT( var_instance );
            tmp_source_name_2 = var_instance;
            tmp_ass_subscript_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_io_loop );
            if ( tmp_ass_subscript_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "ooooooN";
                goto frame_exception_exit_1;
            }
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subscript_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 211;
                type_description_1 = "ooooooN";
                goto frame_exception_exit_1;
            }
        }
        branch_no_3:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_79fbdd10bb9e61809342ea24b6a19499 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_79fbdd10bb9e61809342ea24b6a19499 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_79fbdd10bb9e61809342ea24b6a19499 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_79fbdd10bb9e61809342ea24b6a19499, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_79fbdd10bb9e61809342ea24b6a19499->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_79fbdd10bb9e61809342ea24b6a19499, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_79fbdd10bb9e61809342ea24b6a19499,
        type_description_1,
        par_cls,
        par_force_instance,
        par_kwargs,
        var_io_loop,
        var_instance_cache,
        var_instance,
        NULL
    );


    // Release cached frame.
    if ( frame_79fbdd10bb9e61809342ea24b6a19499 == cache_frame_79fbdd10bb9e61809342ea24b6a19499 )
    {
        Py_DECREF( frame_79fbdd10bb9e61809342ea24b6a19499 );
    }
    cache_frame_79fbdd10bb9e61809342ea24b6a19499 = NULL;

    assertFrameObject( frame_79fbdd10bb9e61809342ea24b6a19499 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_instance );
    tmp_return_value = var_instance;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_8___new__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_force_instance );
    Py_DECREF( par_force_instance );
    par_force_instance = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_io_loop );
    Py_DECREF( var_io_loop );
    var_io_loop = NULL;

    CHECK_OBJECT( (PyObject *)var_instance_cache );
    Py_DECREF( var_instance_cache );
    var_instance_cache = NULL;

    Py_XDECREF( var_instance );
    var_instance = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_force_instance );
    Py_DECREF( par_force_instance );
    par_force_instance = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_io_loop );
    var_io_loop = NULL;

    Py_XDECREF( var_instance_cache );
    var_instance_cache = NULL;

    Py_XDECREF( var_instance );
    var_instance = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_8___new__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_9_initialize( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_defaults = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_2f8fa415e313f31cf65e08fde0777d5f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_2f8fa415e313f31cf65e08fde0777d5f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2f8fa415e313f31cf65e08fde0777d5f, codeobj_2f8fa415e313f31cf65e08fde0777d5f, module_tornado$httpclient, sizeof(void *)+sizeof(void *) );
    frame_2f8fa415e313f31cf65e08fde0777d5f = cache_frame_2f8fa415e313f31cf65e08fde0777d5f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2f8fa415e313f31cf65e08fde0777d5f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2f8fa415e313f31cf65e08fde0777d5f ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_IOLoop );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOLoop );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOLoop" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 215;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_1 = tmp_mvar_value_1;
        frame_2f8fa415e313f31cf65e08fde0777d5f->m_frame.f_lineno = 215;
        tmp_assattr_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_current );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 215;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_io_loop, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 215;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_dict_seq_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_assattr_target_2;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPRequest );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPRequest );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPRequest" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 216;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_2;
        tmp_dict_seq_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__DEFAULTS );
        if ( tmp_dict_seq_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_assattr_name_2 = TO_DICT( tmp_dict_seq_1, NULL );
        Py_DECREF( tmp_dict_seq_1 );
        if ( tmp_assattr_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_defaults, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_defaults );
        tmp_compexpr_left_1 = par_defaults;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_2;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_defaults );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 218;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_defaults );
            tmp_args_element_name_1 = par_defaults;
            frame_2f8fa415e313f31cf65e08fde0777d5f->m_frame.f_lineno = 218;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_update, call_args );
            }

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 218;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        tmp_assattr_name_3 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain__closed, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2f8fa415e313f31cf65e08fde0777d5f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2f8fa415e313f31cf65e08fde0777d5f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2f8fa415e313f31cf65e08fde0777d5f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2f8fa415e313f31cf65e08fde0777d5f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2f8fa415e313f31cf65e08fde0777d5f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2f8fa415e313f31cf65e08fde0777d5f,
        type_description_1,
        par_self,
        par_defaults
    );


    // Release cached frame.
    if ( frame_2f8fa415e313f31cf65e08fde0777d5f == cache_frame_2f8fa415e313f31cf65e08fde0777d5f )
    {
        Py_DECREF( frame_2f8fa415e313f31cf65e08fde0777d5f );
    }
    cache_frame_2f8fa415e313f31cf65e08fde0777d5f = NULL;

    assertFrameObject( frame_2f8fa415e313f31cf65e08fde0777d5f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_9_initialize );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_defaults );
    Py_DECREF( par_defaults );
    par_defaults = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_defaults );
    Py_DECREF( par_defaults );
    par_defaults = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_9_initialize );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_10_close( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_cached_val = NULL;
    struct Nuitka_FrameObject *frame_9a7ec33ce2c27a7376ec35db69f3bda8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_9a7ec33ce2c27a7376ec35db69f3bda8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9a7ec33ce2c27a7376ec35db69f3bda8, codeobj_9a7ec33ce2c27a7376ec35db69f3bda8, module_tornado$httpclient, sizeof(void *)+sizeof(void *) );
    frame_9a7ec33ce2c27a7376ec35db69f3bda8 = cache_frame_9a7ec33ce2c27a7376ec35db69f3bda8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9a7ec33ce2c27a7376ec35db69f3bda8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9a7ec33ce2c27a7376ec35db69f3bda8 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__closed );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 234;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 234;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_assattr_name_1 = Py_True;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__closed, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 236;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__instance_cache );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 237;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_source_name_4;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_5;
            PyObject *tmp_args_element_name_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__instance_cache );
            if ( tmp_source_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 238;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_pop );
            Py_DECREF( tmp_source_name_3 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 238;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_io_loop );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 238;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_args_element_name_2 = Py_None;
            frame_9a7ec33ce2c27a7376ec35db69f3bda8->m_frame.f_lineno = 238;
            {
                PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
            }

            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 238;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( var_cached_val == NULL );
            var_cached_val = tmp_assign_source_1;
        }
        {
            nuitka_bool tmp_condition_result_3;
            int tmp_and_left_truth_1;
            nuitka_bool tmp_and_left_value_1;
            nuitka_bool tmp_and_right_value_1;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( var_cached_val );
            tmp_compexpr_left_2 = var_cached_val;
            tmp_compexpr_right_2 = Py_None;
            tmp_and_left_value_1 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_and_left_truth_1 == 1 )
            {
                goto and_right_1;
            }
            else
            {
                goto and_left_1;
            }
            and_right_1:;
            CHECK_OBJECT( var_cached_val );
            tmp_compexpr_left_3 = var_cached_val;
            CHECK_OBJECT( par_self );
            tmp_compexpr_right_3 = par_self;
            tmp_and_right_value_1 = ( tmp_compexpr_left_3 != tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_3 = tmp_and_right_value_1;
            goto and_end_1;
            and_left_1:;
            tmp_condition_result_3 = tmp_and_left_value_1;
            and_end_1:;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_make_exception_arg_1;
                tmp_make_exception_arg_1 = const_str_digest_03a6f790f27598bb7c4371b455bd02c5;
                frame_9a7ec33ce2c27a7376ec35db69f3bda8->m_frame.f_lineno = 245;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_1 };
                    tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_RuntimeError, call_args );
                }

                assert( !(tmp_raise_type_1 == NULL) );
                exception_type = tmp_raise_type_1;
                exception_lineno = 245;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            branch_no_3:;
        }
        branch_no_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a7ec33ce2c27a7376ec35db69f3bda8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a7ec33ce2c27a7376ec35db69f3bda8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a7ec33ce2c27a7376ec35db69f3bda8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9a7ec33ce2c27a7376ec35db69f3bda8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9a7ec33ce2c27a7376ec35db69f3bda8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9a7ec33ce2c27a7376ec35db69f3bda8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9a7ec33ce2c27a7376ec35db69f3bda8,
        type_description_1,
        par_self,
        var_cached_val
    );


    // Release cached frame.
    if ( frame_9a7ec33ce2c27a7376ec35db69f3bda8 == cache_frame_9a7ec33ce2c27a7376ec35db69f3bda8 )
    {
        Py_DECREF( frame_9a7ec33ce2c27a7376ec35db69f3bda8 );
    }
    cache_frame_9a7ec33ce2c27a7376ec35db69f3bda8 = NULL;

    assertFrameObject( frame_9a7ec33ce2c27a7376ec35db69f3bda8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_10_close );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_cached_val );
    var_cached_val = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_cached_val );
    var_cached_val = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_10_close );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_11_fetch( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_request = python_pars[ 1 ];
    struct Nuitka_CellObject *par_raise_error = PyCell_NEW1( python_pars[ 2 ] );
    PyObject *par_kwargs = python_pars[ 3 ];
    PyObject *var_request_proxy = NULL;
    struct Nuitka_CellObject *var_future = PyCell_EMPTY();
    PyObject *var_handle_response = NULL;
    struct Nuitka_FrameObject *frame_d24ab2ae782140ba0106d4042d210003;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_d24ab2ae782140ba0106d4042d210003 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d24ab2ae782140ba0106d4042d210003, codeobj_d24ab2ae782140ba0106d4042d210003, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d24ab2ae782140ba0106d4042d210003 = cache_frame_d24ab2ae782140ba0106d4042d210003;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d24ab2ae782140ba0106d4042d210003 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d24ab2ae782140ba0106d4042d210003 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__closed );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 281;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 281;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            tmp_make_exception_arg_1 = const_str_digest_501ac84050535bbb2ffaca075c15b490;
            frame_d24ab2ae782140ba0106d4042d210003->m_frame.f_lineno = 282;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_RuntimeError, call_args );
            }

            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 282;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_request );
        tmp_isinstance_inst_1 = par_request;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPRequest );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPRequest );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPRequest" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 283;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 283;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 283;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_dircall_arg1_1;
            PyObject *tmp_mvar_value_2;
            PyObject *tmp_dircall_arg2_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_dircall_arg3_1;
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPRequest );

            if (unlikely( tmp_mvar_value_2 == NULL ))
            {
                tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPRequest );
            }

            if ( tmp_mvar_value_2 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPRequest" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 284;
                type_description_1 = "oocooco";
                goto frame_exception_exit_1;
            }

            tmp_dircall_arg1_1 = tmp_mvar_value_2;
            tmp_dict_key_1 = const_str_plain_url;
            CHECK_OBJECT( par_request );
            tmp_dict_value_1 = par_request;
            tmp_dircall_arg2_1 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_dircall_arg2_1, tmp_dict_key_1, tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            CHECK_OBJECT( par_kwargs );
            tmp_dircall_arg3_1 = par_kwargs;
            Py_INCREF( tmp_dircall_arg1_1 );
            Py_INCREF( tmp_dircall_arg3_1 );

            {
                PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
                tmp_assign_source_1 = impl___internal__$$$function_10_complex_call_helper_keywords_star_dict( dir_call_args );
            }
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 284;
                type_description_1 = "oocooco";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = par_request;
                assert( old != NULL );
                par_request = tmp_assign_source_1;
                Py_DECREF( old );
            }

        }
        goto branch_end_2;
        branch_no_2:;
        {
            nuitka_bool tmp_condition_result_3;
            int tmp_truth_name_2;
            CHECK_OBJECT( par_kwargs );
            tmp_truth_name_2 = CHECK_IF_TRUE( par_kwargs );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 286;
                type_description_1 = "oocooco";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_make_exception_arg_2;
                tmp_make_exception_arg_2 = const_str_digest_eb7897416f79c1314be69b89f4343646;
                frame_d24ab2ae782140ba0106d4042d210003->m_frame.f_lineno = 287;
                {
                    PyObject *call_args[] = { tmp_make_exception_arg_2 };
                    tmp_raise_type_2 = CALL_FUNCTION_WITH_ARGS1( PyExc_ValueError, call_args );
                }

                assert( !(tmp_raise_type_2 == NULL) );
                exception_type = tmp_raise_type_2;
                exception_lineno = 287;
                RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oocooco";
                goto frame_exception_exit_1;
            }
            branch_no_3:;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 293;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_3;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_HTTPHeaders );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 293;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_request );
        tmp_source_name_3 = par_request;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_headers );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 293;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        frame_d24ab2ae782140ba0106d4042d210003->m_frame.f_lineno = 293;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 293;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_request );
        tmp_assattr_target_1 = par_request;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_headers, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 293;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_source_name_4;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain__RequestProxy );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__RequestProxy );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_RequestProxy" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 294;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_4;
        CHECK_OBJECT( par_request );
        tmp_args_element_name_2 = par_request;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_defaults );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        frame_d24ab2ae782140ba0106d4042d210003->m_frame.f_lineno = 294;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_assign_source_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 294;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        assert( var_request_proxy == NULL );
        var_request_proxy = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_name_3;
        PyObject *tmp_mvar_value_5;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Future );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Future );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Future" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 295;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_3 = tmp_mvar_value_5;
        frame_d24ab2ae782140ba0106d4042d210003->m_frame.f_lineno = 295;
        tmp_assign_source_3 = CALL_FUNCTION_NO_ARGS( tmp_called_name_3 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 295;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_future ) == NULL );
        PyCell_SET( var_future, tmp_assign_source_3 );

    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_annotations_1;
        tmp_annotations_1 = PyDict_Copy( const_dict_f486466b362c183859d46eaa920d7989 );
        tmp_assign_source_4 = MAKE_FUNCTION_tornado$httpclient$$$function_11_fetch$$$function_1_handle_response( tmp_annotations_1 );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[0] = var_future;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[1] = par_raise_error;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_4)->m_closure[1] );


        assert( var_handle_response == NULL );
        var_handle_response = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_name_5;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_args_element_name_7;
        CHECK_OBJECT( par_self );
        tmp_source_name_5 = par_self;
        tmp_called_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_fetch_impl );
        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_cast );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_cast );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "cast" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 304;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }

        tmp_called_name_5 = tmp_mvar_value_6;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPRequest );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPRequest );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPRequest" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 304;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_5 = tmp_mvar_value_7;
        CHECK_OBJECT( var_request_proxy );
        tmp_args_element_name_6 = var_request_proxy;
        frame_d24ab2ae782140ba0106d4042d210003->m_frame.f_lineno = 304;
        {
            PyObject *call_args[] = { tmp_args_element_name_5, tmp_args_element_name_6 };
            tmp_args_element_name_4 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_5, call_args );
        }

        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 304;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle_response );
        tmp_args_element_name_7 = var_handle_response;
        frame_d24ab2ae782140ba0106d4042d210003->m_frame.f_lineno = 304;
        {
            PyObject *call_args[] = { tmp_args_element_name_4, tmp_args_element_name_7 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 304;
            type_description_1 = "oocooco";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d24ab2ae782140ba0106d4042d210003 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d24ab2ae782140ba0106d4042d210003 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d24ab2ae782140ba0106d4042d210003, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d24ab2ae782140ba0106d4042d210003->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d24ab2ae782140ba0106d4042d210003, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d24ab2ae782140ba0106d4042d210003,
        type_description_1,
        par_self,
        par_request,
        par_raise_error,
        par_kwargs,
        var_request_proxy,
        var_future,
        var_handle_response
    );


    // Release cached frame.
    if ( frame_d24ab2ae782140ba0106d4042d210003 == cache_frame_d24ab2ae782140ba0106d4042d210003 )
    {
        Py_DECREF( frame_d24ab2ae782140ba0106d4042d210003 );
    }
    cache_frame_d24ab2ae782140ba0106d4042d210003 = NULL;

    assertFrameObject( frame_d24ab2ae782140ba0106d4042d210003 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( PyCell_GET( var_future ) );
    tmp_return_value = PyCell_GET( var_future );
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_11_fetch );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_raise_error );
    Py_DECREF( par_raise_error );
    par_raise_error = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    CHECK_OBJECT( (PyObject *)var_request_proxy );
    Py_DECREF( var_request_proxy );
    var_request_proxy = NULL;

    CHECK_OBJECT( (PyObject *)var_future );
    Py_DECREF( var_future );
    var_future = NULL;

    CHECK_OBJECT( (PyObject *)var_handle_response );
    Py_DECREF( var_handle_response );
    var_handle_response = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_raise_error );
    Py_DECREF( par_raise_error );
    par_raise_error = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    Py_XDECREF( var_request_proxy );
    var_request_proxy = NULL;

    CHECK_OBJECT( (PyObject *)var_future );
    Py_DECREF( var_future );
    var_future = NULL;

    Py_XDECREF( var_handle_response );
    var_handle_response = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_11_fetch );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_11_fetch$$$function_1_handle_response( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_response = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_b31cc5cce776a7d51eeb572d3309127c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_b31cc5cce776a7d51eeb572d3309127c = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_b31cc5cce776a7d51eeb572d3309127c, codeobj_b31cc5cce776a7d51eeb572d3309127c, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_b31cc5cce776a7d51eeb572d3309127c = cache_frame_b31cc5cce776a7d51eeb572d3309127c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_b31cc5cce776a7d51eeb572d3309127c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_b31cc5cce776a7d51eeb572d3309127c ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_response );
        tmp_source_name_1 = par_response;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_error );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 298;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 298;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_or_left_truth_1;
            nuitka_bool tmp_or_left_value_1;
            nuitka_bool tmp_or_right_value_1;
            int tmp_truth_name_2;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_2;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "raise_error" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 299;
                type_description_1 = "occ";
                goto frame_exception_exit_1;
            }

            tmp_truth_name_2 = CHECK_IF_TRUE( PyCell_GET( self->m_closure[1] ) );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 299;
                type_description_1 = "occ";
                goto frame_exception_exit_1;
            }
            tmp_or_left_value_1 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_or_left_truth_1 == 1 )
            {
                goto or_left_1;
            }
            else
            {
                goto or_right_1;
            }
            or_right_1:;
            CHECK_OBJECT( par_response );
            tmp_source_name_2 = par_response;
            tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__error_is_response_code );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 299;
                type_description_1 = "occ";
                goto frame_exception_exit_1;
            }
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 299;
                type_description_1 = "occ";
                goto frame_exception_exit_1;
            }
            tmp_or_right_value_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_or_right_value_1;
            goto or_end_1;
            or_left_1:;
            tmp_condition_result_2 = tmp_or_left_value_1;
            or_end_1:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_1;
                PyObject *tmp_call_result_1;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_args_element_name_2;
                PyObject *tmp_source_name_3;
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_future_set_exception_unless_cancelled );

                if (unlikely( tmp_mvar_value_1 == NULL ))
                {
                    tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_future_set_exception_unless_cancelled );
                }

                if ( tmp_mvar_value_1 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "future_set_exception_unless_cancelled" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 300;
                    type_description_1 = "occ";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_1 = tmp_mvar_value_1;
                if ( PyCell_GET( self->m_closure[0] ) == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "future" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 300;
                    type_description_1 = "occ";
                    goto frame_exception_exit_1;
                }

                tmp_args_element_name_1 = PyCell_GET( self->m_closure[0] );
                CHECK_OBJECT( par_response );
                tmp_source_name_3 = par_response;
                tmp_args_element_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_error );
                if ( tmp_args_element_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 300;
                    type_description_1 = "occ";
                    goto frame_exception_exit_1;
                }
                frame_b31cc5cce776a7d51eeb572d3309127c->m_frame.f_lineno = 300;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                    tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_1, call_args );
                }

                Py_DECREF( tmp_args_element_name_2 );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 300;
                    type_description_1 = "occ";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            tmp_return_value = Py_None;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            branch_no_2:;
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_future_set_result_unless_cancelled );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_future_set_result_unless_cancelled );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "future_set_result_unless_cancelled" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 302;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_2;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "future" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 302;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_3 = PyCell_GET( self->m_closure[0] );
        CHECK_OBJECT( par_response );
        tmp_args_element_name_4 = par_response;
        frame_b31cc5cce776a7d51eeb572d3309127c->m_frame.f_lineno = 302;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 302;
            type_description_1 = "occ";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b31cc5cce776a7d51eeb572d3309127c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_b31cc5cce776a7d51eeb572d3309127c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_b31cc5cce776a7d51eeb572d3309127c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_b31cc5cce776a7d51eeb572d3309127c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_b31cc5cce776a7d51eeb572d3309127c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_b31cc5cce776a7d51eeb572d3309127c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_b31cc5cce776a7d51eeb572d3309127c,
        type_description_1,
        par_response,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_b31cc5cce776a7d51eeb572d3309127c == cache_frame_b31cc5cce776a7d51eeb572d3309127c )
    {
        Py_DECREF( frame_b31cc5cce776a7d51eeb572d3309127c );
    }
    cache_frame_b31cc5cce776a7d51eeb572d3309127c = NULL;

    assertFrameObject( frame_b31cc5cce776a7d51eeb572d3309127c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_11_fetch$$$function_1_handle_response );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_response );
    Py_DECREF( par_response );
    par_response = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_response );
    Py_DECREF( par_response );
    par_response = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_11_fetch$$$function_1_handle_response );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_12_fetch_impl( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_request = python_pars[ 1 ];
    PyObject *par_callback = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_18f23f5a7e6fe4e9cd0bf09e0959019a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_18f23f5a7e6fe4e9cd0bf09e0959019a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_18f23f5a7e6fe4e9cd0bf09e0959019a, codeobj_18f23f5a7e6fe4e9cd0bf09e0959019a, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_18f23f5a7e6fe4e9cd0bf09e0959019a = cache_frame_18f23f5a7e6fe4e9cd0bf09e0959019a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_18f23f5a7e6fe4e9cd0bf09e0959019a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_18f23f5a7e6fe4e9cd0bf09e0959019a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_raise_type_1;
        frame_18f23f5a7e6fe4e9cd0bf09e0959019a->m_frame.f_lineno = 310;
        tmp_raise_type_1 = CALL_FUNCTION_NO_ARGS( PyExc_NotImplementedError );
        assert( !(tmp_raise_type_1 == NULL) );
        exception_type = tmp_raise_type_1;
        exception_lineno = 310;
        RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
        type_description_1 = "ooo";
        goto frame_exception_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18f23f5a7e6fe4e9cd0bf09e0959019a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18f23f5a7e6fe4e9cd0bf09e0959019a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_18f23f5a7e6fe4e9cd0bf09e0959019a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_18f23f5a7e6fe4e9cd0bf09e0959019a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_18f23f5a7e6fe4e9cd0bf09e0959019a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_18f23f5a7e6fe4e9cd0bf09e0959019a,
        type_description_1,
        par_self,
        par_request,
        par_callback
    );


    // Release cached frame.
    if ( frame_18f23f5a7e6fe4e9cd0bf09e0959019a == cache_frame_18f23f5a7e6fe4e9cd0bf09e0959019a )
    {
        Py_DECREF( frame_18f23f5a7e6fe4e9cd0bf09e0959019a );
    }
    cache_frame_18f23f5a7e6fe4e9cd0bf09e0959019a = NULL;

    assertFrameObject( frame_18f23f5a7e6fe4e9cd0bf09e0959019a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_12_fetch_impl );
    return NULL;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_callback );
    Py_DECREF( par_callback );
    par_callback = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_12_fetch_impl );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

}


static PyObject *impl_tornado$httpclient$$$function_13_configure( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_cls = python_pars[ 0 ];
    PyObject *par_impl = python_pars[ 1 ];
    PyObject *par_kwargs = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_d911a993b10fba49163895c1c4d188fa;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d911a993b10fba49163895c1c4d188fa = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d911a993b10fba49163895c1c4d188fa, codeobj_d911a993b10fba49163895c1c4d188fa, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_d911a993b10fba49163895c1c4d188fa = cache_frame_d911a993b10fba49163895c1c4d188fa;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d911a993b10fba49163895c1c4d188fa );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d911a993b10fba49163895c1c4d188fa ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_dircall_arg1_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_object_name_1;
        PyObject *tmp_dircall_arg2_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_dircall_arg3_1;
        PyObject *tmp_call_result_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_AsyncHTTPClient );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_AsyncHTTPClient );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "AsyncHTTPClient" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 334;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_cls );
        tmp_object_name_1 = par_cls;
        tmp_source_name_1 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        tmp_dircall_arg1_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_configure );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_dircall_arg1_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_impl );
        tmp_tuple_element_1 = par_impl;
        tmp_dircall_arg2_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_dircall_arg2_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_kwargs );
        tmp_dircall_arg3_1 = par_kwargs;
        Py_INCREF( tmp_dircall_arg3_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1, tmp_dircall_arg2_1, tmp_dircall_arg3_1};
            tmp_call_result_1 = impl___internal__$$$function_6_complex_call_helper_pos_star_dict( dir_call_args );
        }
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "oooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d911a993b10fba49163895c1c4d188fa );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d911a993b10fba49163895c1c4d188fa );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d911a993b10fba49163895c1c4d188fa, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d911a993b10fba49163895c1c4d188fa->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d911a993b10fba49163895c1c4d188fa, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d911a993b10fba49163895c1c4d188fa,
        type_description_1,
        par_cls,
        par_impl,
        par_kwargs,
        NULL
    );


    // Release cached frame.
    if ( frame_d911a993b10fba49163895c1c4d188fa == cache_frame_d911a993b10fba49163895c1c4d188fa )
    {
        Py_DECREF( frame_d911a993b10fba49163895c1c4d188fa );
    }
    cache_frame_d911a993b10fba49163895c1c4d188fa = NULL;

    assertFrameObject( frame_d911a993b10fba49163895c1c4d188fa );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_13_configure );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_impl );
    Py_DECREF( par_impl );
    par_impl = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_cls );
    Py_DECREF( par_cls );
    par_cls = NULL;

    CHECK_OBJECT( (PyObject *)par_impl );
    Py_DECREF( par_impl );
    par_impl = NULL;

    CHECK_OBJECT( (PyObject *)par_kwargs );
    Py_DECREF( par_kwargs );
    par_kwargs = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_13_configure );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_14___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_url = python_pars[ 1 ];
    PyObject *par_method = python_pars[ 2 ];
    PyObject *par_headers = python_pars[ 3 ];
    PyObject *par_body = python_pars[ 4 ];
    PyObject *par_auth_username = python_pars[ 5 ];
    PyObject *par_auth_password = python_pars[ 6 ];
    PyObject *par_auth_mode = python_pars[ 7 ];
    PyObject *par_connect_timeout = python_pars[ 8 ];
    PyObject *par_request_timeout = python_pars[ 9 ];
    PyObject *par_if_modified_since = python_pars[ 10 ];
    PyObject *par_follow_redirects = python_pars[ 11 ];
    PyObject *par_max_redirects = python_pars[ 12 ];
    PyObject *par_user_agent = python_pars[ 13 ];
    PyObject *par_use_gzip = python_pars[ 14 ];
    PyObject *par_network_interface = python_pars[ 15 ];
    PyObject *par_streaming_callback = python_pars[ 16 ];
    PyObject *par_header_callback = python_pars[ 17 ];
    PyObject *par_prepare_curl_callback = python_pars[ 18 ];
    PyObject *par_proxy_host = python_pars[ 19 ];
    PyObject *par_proxy_port = python_pars[ 20 ];
    PyObject *par_proxy_username = python_pars[ 21 ];
    PyObject *par_proxy_password = python_pars[ 22 ];
    PyObject *par_proxy_auth_mode = python_pars[ 23 ];
    PyObject *par_allow_nonstandard_methods = python_pars[ 24 ];
    PyObject *par_validate_cert = python_pars[ 25 ];
    PyObject *par_ca_certs = python_pars[ 26 ];
    PyObject *par_allow_ipv6 = python_pars[ 27 ];
    PyObject *par_client_key = python_pars[ 28 ];
    PyObject *par_client_cert = python_pars[ 29 ];
    PyObject *par_body_producer = python_pars[ 30 ];
    PyObject *par_expect_100_continue = python_pars[ 31 ];
    PyObject *par_decompress_response = python_pars[ 32 ];
    PyObject *par_ssl_options = python_pars[ 33 ];
    struct Nuitka_FrameObject *frame_6e307838b588317efea4ea20564a9b52;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6e307838b588317efea4ea20564a9b52 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6e307838b588317efea4ea20564a9b52, codeobj_6e307838b588317efea4ea20564a9b52, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_6e307838b588317efea4ea20564a9b52 = cache_frame_6e307838b588317efea4ea20564a9b52;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6e307838b588317efea4ea20564a9b52 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6e307838b588317efea4ea20564a9b52 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_headers );
        tmp_assattr_name_1 = par_headers;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_headers, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 505;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_if_modified_since );
        tmp_truth_name_1 = CHECK_IF_TRUE( par_if_modified_since );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 506;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_ass_subvalue_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_ass_subscribed_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_ass_subscript_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 507;
                type_description_1 = "oooooooooooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            CHECK_OBJECT( par_if_modified_since );
            tmp_args_element_name_1 = par_if_modified_since;
            frame_6e307838b588317efea4ea20564a9b52->m_frame.f_lineno = 507;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_ass_subvalue_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_format_timestamp, call_args );
            }

            if ( tmp_ass_subvalue_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 507;
                type_description_1 = "oooooooooooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_source_name_1 = par_self;
            tmp_ass_subscribed_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_headers );
            if ( tmp_ass_subscribed_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_ass_subvalue_1 );

                exception_lineno = 507;
                type_description_1 = "oooooooooooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_ass_subscript_1 = const_str_digest_3f0a7e7b48e8430fc8c8a4c7e40f957f;
            tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
            Py_DECREF( tmp_ass_subscribed_1 );
            Py_DECREF( tmp_ass_subvalue_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 507;
                type_description_1 = "oooooooooooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_proxy_host );
        tmp_assattr_name_2 = par_proxy_host;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_proxy_host, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 510;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_proxy_port );
        tmp_assattr_name_3 = par_proxy_port;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_proxy_port, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 511;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        PyObject *tmp_assattr_target_4;
        CHECK_OBJECT( par_proxy_username );
        tmp_assattr_name_4 = par_proxy_username;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_proxy_username, tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 512;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_5;
        PyObject *tmp_assattr_target_5;
        CHECK_OBJECT( par_proxy_password );
        tmp_assattr_name_5 = par_proxy_password;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_5 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_proxy_password, tmp_assattr_name_5 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 513;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_6;
        PyObject *tmp_assattr_target_6;
        CHECK_OBJECT( par_proxy_auth_mode );
        tmp_assattr_name_6 = par_proxy_auth_mode;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_6 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain_proxy_auth_mode, tmp_assattr_name_6 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 514;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_assattr_target_7;
        CHECK_OBJECT( par_url );
        tmp_assattr_name_7 = par_url;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_7 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain_url, tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 515;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_8;
        PyObject *tmp_assattr_target_8;
        CHECK_OBJECT( par_method );
        tmp_assattr_name_8 = par_method;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_8 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain_method, tmp_assattr_name_8 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 516;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_9;
        PyObject *tmp_assattr_target_9;
        CHECK_OBJECT( par_body );
        tmp_assattr_name_9 = par_body;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_9 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_9, const_str_plain_body, tmp_assattr_name_9 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 517;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_10;
        PyObject *tmp_assattr_target_10;
        CHECK_OBJECT( par_body_producer );
        tmp_assattr_name_10 = par_body_producer;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_10 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_10, const_str_plain_body_producer, tmp_assattr_name_10 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 518;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_11;
        PyObject *tmp_assattr_target_11;
        CHECK_OBJECT( par_auth_username );
        tmp_assattr_name_11 = par_auth_username;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_11 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_11, const_str_plain_auth_username, tmp_assattr_name_11 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 519;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_12;
        PyObject *tmp_assattr_target_12;
        CHECK_OBJECT( par_auth_password );
        tmp_assattr_name_12 = par_auth_password;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_12 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_12, const_str_plain_auth_password, tmp_assattr_name_12 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 520;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_13;
        PyObject *tmp_assattr_target_13;
        CHECK_OBJECT( par_auth_mode );
        tmp_assattr_name_13 = par_auth_mode;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_13 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_13, const_str_plain_auth_mode, tmp_assattr_name_13 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 521;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_14;
        PyObject *tmp_assattr_target_14;
        CHECK_OBJECT( par_connect_timeout );
        tmp_assattr_name_14 = par_connect_timeout;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_14 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_14, const_str_plain_connect_timeout, tmp_assattr_name_14 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 522;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_15;
        PyObject *tmp_assattr_target_15;
        CHECK_OBJECT( par_request_timeout );
        tmp_assattr_name_15 = par_request_timeout;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_15 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_15, const_str_plain_request_timeout, tmp_assattr_name_15 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 523;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_16;
        PyObject *tmp_assattr_target_16;
        CHECK_OBJECT( par_follow_redirects );
        tmp_assattr_name_16 = par_follow_redirects;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_16 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_16, const_str_plain_follow_redirects, tmp_assattr_name_16 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 524;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_17;
        PyObject *tmp_assattr_target_17;
        CHECK_OBJECT( par_max_redirects );
        tmp_assattr_name_17 = par_max_redirects;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_17 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_17, const_str_plain_max_redirects, tmp_assattr_name_17 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 525;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_18;
        PyObject *tmp_assattr_target_18;
        CHECK_OBJECT( par_user_agent );
        tmp_assattr_name_18 = par_user_agent;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_18 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_18, const_str_plain_user_agent, tmp_assattr_name_18 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 526;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_decompress_response );
        tmp_compexpr_left_1 = par_decompress_response;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assattr_name_19;
            PyObject *tmp_assattr_target_19;
            CHECK_OBJECT( par_decompress_response );
            tmp_assattr_name_19 = par_decompress_response;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_19 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_19, const_str_plain_decompress_response, tmp_assattr_name_19 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 528;
                type_description_1 = "oooooooooooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assattr_name_20;
            PyObject *tmp_assattr_target_20;
            CHECK_OBJECT( par_use_gzip );
            tmp_assattr_name_20 = par_use_gzip;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_20 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_20, const_str_plain_decompress_response, tmp_assattr_name_20 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 530;
                type_description_1 = "oooooooooooooooooooooooooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assattr_name_21;
        PyObject *tmp_assattr_target_21;
        CHECK_OBJECT( par_network_interface );
        tmp_assattr_name_21 = par_network_interface;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_21 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_21, const_str_plain_network_interface, tmp_assattr_name_21 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 531;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_22;
        PyObject *tmp_assattr_target_22;
        CHECK_OBJECT( par_streaming_callback );
        tmp_assattr_name_22 = par_streaming_callback;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_22 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_22, const_str_plain_streaming_callback, tmp_assattr_name_22 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 532;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_23;
        PyObject *tmp_assattr_target_23;
        CHECK_OBJECT( par_header_callback );
        tmp_assattr_name_23 = par_header_callback;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_23 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_23, const_str_plain_header_callback, tmp_assattr_name_23 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 533;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_24;
        PyObject *tmp_assattr_target_24;
        CHECK_OBJECT( par_prepare_curl_callback );
        tmp_assattr_name_24 = par_prepare_curl_callback;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_24 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_24, const_str_plain_prepare_curl_callback, tmp_assattr_name_24 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 534;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_25;
        PyObject *tmp_assattr_target_25;
        CHECK_OBJECT( par_allow_nonstandard_methods );
        tmp_assattr_name_25 = par_allow_nonstandard_methods;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_25 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_25, const_str_plain_allow_nonstandard_methods, tmp_assattr_name_25 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 535;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_26;
        PyObject *tmp_assattr_target_26;
        CHECK_OBJECT( par_validate_cert );
        tmp_assattr_name_26 = par_validate_cert;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_26 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_26, const_str_plain_validate_cert, tmp_assattr_name_26 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 536;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_27;
        PyObject *tmp_assattr_target_27;
        CHECK_OBJECT( par_ca_certs );
        tmp_assattr_name_27 = par_ca_certs;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_27 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_27, const_str_plain_ca_certs, tmp_assattr_name_27 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 537;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_28;
        PyObject *tmp_assattr_target_28;
        CHECK_OBJECT( par_allow_ipv6 );
        tmp_assattr_name_28 = par_allow_ipv6;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_28 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_28, const_str_plain_allow_ipv6, tmp_assattr_name_28 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 538;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_29;
        PyObject *tmp_assattr_target_29;
        CHECK_OBJECT( par_client_key );
        tmp_assattr_name_29 = par_client_key;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_29 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_29, const_str_plain_client_key, tmp_assattr_name_29 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 539;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_30;
        PyObject *tmp_assattr_target_30;
        CHECK_OBJECT( par_client_cert );
        tmp_assattr_name_30 = par_client_cert;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_30 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_30, const_str_plain_client_cert, tmp_assattr_name_30 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 540;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_31;
        PyObject *tmp_assattr_target_31;
        CHECK_OBJECT( par_ssl_options );
        tmp_assattr_name_31 = par_ssl_options;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_31 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_31, const_str_plain_ssl_options, tmp_assattr_name_31 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 541;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_32;
        PyObject *tmp_assattr_target_32;
        CHECK_OBJECT( par_expect_100_continue );
        tmp_assattr_name_32 = par_expect_100_continue;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_32 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_32, const_str_plain_expect_100_continue, tmp_assattr_name_32 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 542;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_33;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_assattr_target_33;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_time );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_time );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "time" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 543;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_instance_2 = tmp_mvar_value_2;
        frame_6e307838b588317efea4ea20564a9b52->m_frame.f_lineno = 543;
        tmp_assattr_name_33 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_time );
        if ( tmp_assattr_name_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 543;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_33 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_33, const_str_plain_start_time, tmp_assattr_name_33 );
        Py_DECREF( tmp_assattr_name_33 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 543;
            type_description_1 = "oooooooooooooooooooooooooooooooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e307838b588317efea4ea20564a9b52 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6e307838b588317efea4ea20564a9b52 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6e307838b588317efea4ea20564a9b52, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6e307838b588317efea4ea20564a9b52->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6e307838b588317efea4ea20564a9b52, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6e307838b588317efea4ea20564a9b52,
        type_description_1,
        par_self,
        par_url,
        par_method,
        par_headers,
        par_body,
        par_auth_username,
        par_auth_password,
        par_auth_mode,
        par_connect_timeout,
        par_request_timeout,
        par_if_modified_since,
        par_follow_redirects,
        par_max_redirects,
        par_user_agent,
        par_use_gzip,
        par_network_interface,
        par_streaming_callback,
        par_header_callback,
        par_prepare_curl_callback,
        par_proxy_host,
        par_proxy_port,
        par_proxy_username,
        par_proxy_password,
        par_proxy_auth_mode,
        par_allow_nonstandard_methods,
        par_validate_cert,
        par_ca_certs,
        par_allow_ipv6,
        par_client_key,
        par_client_cert,
        par_body_producer,
        par_expect_100_continue,
        par_decompress_response,
        par_ssl_options
    );


    // Release cached frame.
    if ( frame_6e307838b588317efea4ea20564a9b52 == cache_frame_6e307838b588317efea4ea20564a9b52 )
    {
        Py_DECREF( frame_6e307838b588317efea4ea20564a9b52 );
    }
    cache_frame_6e307838b588317efea4ea20564a9b52 = NULL;

    assertFrameObject( frame_6e307838b588317efea4ea20564a9b52 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_14___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_url );
    Py_DECREF( par_url );
    par_url = NULL;

    CHECK_OBJECT( (PyObject *)par_method );
    Py_DECREF( par_method );
    par_method = NULL;

    CHECK_OBJECT( (PyObject *)par_headers );
    Py_DECREF( par_headers );
    par_headers = NULL;

    CHECK_OBJECT( (PyObject *)par_body );
    Py_DECREF( par_body );
    par_body = NULL;

    CHECK_OBJECT( (PyObject *)par_auth_username );
    Py_DECREF( par_auth_username );
    par_auth_username = NULL;

    CHECK_OBJECT( (PyObject *)par_auth_password );
    Py_DECREF( par_auth_password );
    par_auth_password = NULL;

    CHECK_OBJECT( (PyObject *)par_auth_mode );
    Py_DECREF( par_auth_mode );
    par_auth_mode = NULL;

    CHECK_OBJECT( (PyObject *)par_connect_timeout );
    Py_DECREF( par_connect_timeout );
    par_connect_timeout = NULL;

    CHECK_OBJECT( (PyObject *)par_request_timeout );
    Py_DECREF( par_request_timeout );
    par_request_timeout = NULL;

    CHECK_OBJECT( (PyObject *)par_if_modified_since );
    Py_DECREF( par_if_modified_since );
    par_if_modified_since = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_redirects );
    Py_DECREF( par_follow_redirects );
    par_follow_redirects = NULL;

    CHECK_OBJECT( (PyObject *)par_max_redirects );
    Py_DECREF( par_max_redirects );
    par_max_redirects = NULL;

    CHECK_OBJECT( (PyObject *)par_user_agent );
    Py_DECREF( par_user_agent );
    par_user_agent = NULL;

    CHECK_OBJECT( (PyObject *)par_use_gzip );
    Py_DECREF( par_use_gzip );
    par_use_gzip = NULL;

    CHECK_OBJECT( (PyObject *)par_network_interface );
    Py_DECREF( par_network_interface );
    par_network_interface = NULL;

    CHECK_OBJECT( (PyObject *)par_streaming_callback );
    Py_DECREF( par_streaming_callback );
    par_streaming_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_header_callback );
    Py_DECREF( par_header_callback );
    par_header_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_prepare_curl_callback );
    Py_DECREF( par_prepare_curl_callback );
    par_prepare_curl_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_proxy_host );
    Py_DECREF( par_proxy_host );
    par_proxy_host = NULL;

    CHECK_OBJECT( (PyObject *)par_proxy_port );
    Py_DECREF( par_proxy_port );
    par_proxy_port = NULL;

    CHECK_OBJECT( (PyObject *)par_proxy_username );
    Py_DECREF( par_proxy_username );
    par_proxy_username = NULL;

    CHECK_OBJECT( (PyObject *)par_proxy_password );
    Py_DECREF( par_proxy_password );
    par_proxy_password = NULL;

    CHECK_OBJECT( (PyObject *)par_proxy_auth_mode );
    Py_DECREF( par_proxy_auth_mode );
    par_proxy_auth_mode = NULL;

    CHECK_OBJECT( (PyObject *)par_allow_nonstandard_methods );
    Py_DECREF( par_allow_nonstandard_methods );
    par_allow_nonstandard_methods = NULL;

    CHECK_OBJECT( (PyObject *)par_validate_cert );
    Py_DECREF( par_validate_cert );
    par_validate_cert = NULL;

    CHECK_OBJECT( (PyObject *)par_ca_certs );
    Py_DECREF( par_ca_certs );
    par_ca_certs = NULL;

    CHECK_OBJECT( (PyObject *)par_allow_ipv6 );
    Py_DECREF( par_allow_ipv6 );
    par_allow_ipv6 = NULL;

    CHECK_OBJECT( (PyObject *)par_client_key );
    Py_DECREF( par_client_key );
    par_client_key = NULL;

    CHECK_OBJECT( (PyObject *)par_client_cert );
    Py_DECREF( par_client_cert );
    par_client_cert = NULL;

    CHECK_OBJECT( (PyObject *)par_body_producer );
    Py_DECREF( par_body_producer );
    par_body_producer = NULL;

    CHECK_OBJECT( (PyObject *)par_expect_100_continue );
    Py_DECREF( par_expect_100_continue );
    par_expect_100_continue = NULL;

    CHECK_OBJECT( (PyObject *)par_decompress_response );
    Py_DECREF( par_decompress_response );
    par_decompress_response = NULL;

    CHECK_OBJECT( (PyObject *)par_ssl_options );
    Py_DECREF( par_ssl_options );
    par_ssl_options = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_url );
    Py_DECREF( par_url );
    par_url = NULL;

    CHECK_OBJECT( (PyObject *)par_method );
    Py_DECREF( par_method );
    par_method = NULL;

    CHECK_OBJECT( (PyObject *)par_headers );
    Py_DECREF( par_headers );
    par_headers = NULL;

    CHECK_OBJECT( (PyObject *)par_body );
    Py_DECREF( par_body );
    par_body = NULL;

    CHECK_OBJECT( (PyObject *)par_auth_username );
    Py_DECREF( par_auth_username );
    par_auth_username = NULL;

    CHECK_OBJECT( (PyObject *)par_auth_password );
    Py_DECREF( par_auth_password );
    par_auth_password = NULL;

    CHECK_OBJECT( (PyObject *)par_auth_mode );
    Py_DECREF( par_auth_mode );
    par_auth_mode = NULL;

    CHECK_OBJECT( (PyObject *)par_connect_timeout );
    Py_DECREF( par_connect_timeout );
    par_connect_timeout = NULL;

    CHECK_OBJECT( (PyObject *)par_request_timeout );
    Py_DECREF( par_request_timeout );
    par_request_timeout = NULL;

    CHECK_OBJECT( (PyObject *)par_if_modified_since );
    Py_DECREF( par_if_modified_since );
    par_if_modified_since = NULL;

    CHECK_OBJECT( (PyObject *)par_follow_redirects );
    Py_DECREF( par_follow_redirects );
    par_follow_redirects = NULL;

    CHECK_OBJECT( (PyObject *)par_max_redirects );
    Py_DECREF( par_max_redirects );
    par_max_redirects = NULL;

    CHECK_OBJECT( (PyObject *)par_user_agent );
    Py_DECREF( par_user_agent );
    par_user_agent = NULL;

    CHECK_OBJECT( (PyObject *)par_use_gzip );
    Py_DECREF( par_use_gzip );
    par_use_gzip = NULL;

    CHECK_OBJECT( (PyObject *)par_network_interface );
    Py_DECREF( par_network_interface );
    par_network_interface = NULL;

    CHECK_OBJECT( (PyObject *)par_streaming_callback );
    Py_DECREF( par_streaming_callback );
    par_streaming_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_header_callback );
    Py_DECREF( par_header_callback );
    par_header_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_prepare_curl_callback );
    Py_DECREF( par_prepare_curl_callback );
    par_prepare_curl_callback = NULL;

    CHECK_OBJECT( (PyObject *)par_proxy_host );
    Py_DECREF( par_proxy_host );
    par_proxy_host = NULL;

    CHECK_OBJECT( (PyObject *)par_proxy_port );
    Py_DECREF( par_proxy_port );
    par_proxy_port = NULL;

    CHECK_OBJECT( (PyObject *)par_proxy_username );
    Py_DECREF( par_proxy_username );
    par_proxy_username = NULL;

    CHECK_OBJECT( (PyObject *)par_proxy_password );
    Py_DECREF( par_proxy_password );
    par_proxy_password = NULL;

    CHECK_OBJECT( (PyObject *)par_proxy_auth_mode );
    Py_DECREF( par_proxy_auth_mode );
    par_proxy_auth_mode = NULL;

    CHECK_OBJECT( (PyObject *)par_allow_nonstandard_methods );
    Py_DECREF( par_allow_nonstandard_methods );
    par_allow_nonstandard_methods = NULL;

    CHECK_OBJECT( (PyObject *)par_validate_cert );
    Py_DECREF( par_validate_cert );
    par_validate_cert = NULL;

    CHECK_OBJECT( (PyObject *)par_ca_certs );
    Py_DECREF( par_ca_certs );
    par_ca_certs = NULL;

    CHECK_OBJECT( (PyObject *)par_allow_ipv6 );
    Py_DECREF( par_allow_ipv6 );
    par_allow_ipv6 = NULL;

    CHECK_OBJECT( (PyObject *)par_client_key );
    Py_DECREF( par_client_key );
    par_client_key = NULL;

    CHECK_OBJECT( (PyObject *)par_client_cert );
    Py_DECREF( par_client_cert );
    par_client_cert = NULL;

    CHECK_OBJECT( (PyObject *)par_body_producer );
    Py_DECREF( par_body_producer );
    par_body_producer = NULL;

    CHECK_OBJECT( (PyObject *)par_expect_100_continue );
    Py_DECREF( par_expect_100_continue );
    par_expect_100_continue = NULL;

    CHECK_OBJECT( (PyObject *)par_decompress_response );
    Py_DECREF( par_decompress_response );
    par_decompress_response = NULL;

    CHECK_OBJECT( (PyObject *)par_ssl_options );
    Py_DECREF( par_ssl_options );
    par_ssl_options = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_14___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_15_headers( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_18eaf614fcb3f563067213568d8d3711;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_18eaf614fcb3f563067213568d8d3711 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_18eaf614fcb3f563067213568d8d3711, codeobj_18eaf614fcb3f563067213568d8d3711, module_tornado$httpclient, sizeof(void *) );
    frame_18eaf614fcb3f563067213568d8d3711 = cache_frame_18eaf614fcb3f563067213568d8d3711;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_18eaf614fcb3f563067213568d8d3711 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_18eaf614fcb3f563067213568d8d3711 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__headers );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 550;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18eaf614fcb3f563067213568d8d3711 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_18eaf614fcb3f563067213568d8d3711 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18eaf614fcb3f563067213568d8d3711 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_18eaf614fcb3f563067213568d8d3711, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_18eaf614fcb3f563067213568d8d3711->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_18eaf614fcb3f563067213568d8d3711, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_18eaf614fcb3f563067213568d8d3711,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_18eaf614fcb3f563067213568d8d3711 == cache_frame_18eaf614fcb3f563067213568d8d3711 )
    {
        Py_DECREF( frame_18eaf614fcb3f563067213568d8d3711 );
    }
    cache_frame_18eaf614fcb3f563067213568d8d3711 = NULL;

    assertFrameObject( frame_18eaf614fcb3f563067213568d8d3711 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_15_headers );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_15_headers );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_16_headers( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_fb2562f3267c33570dc44f4893e3a2c4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_fb2562f3267c33570dc44f4893e3a2c4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_fb2562f3267c33570dc44f4893e3a2c4, codeobj_fb2562f3267c33570dc44f4893e3a2c4, module_tornado$httpclient, sizeof(void *)+sizeof(void *) );
    frame_fb2562f3267c33570dc44f4893e3a2c4 = cache_frame_fb2562f3267c33570dc44f4893e3a2c4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_fb2562f3267c33570dc44f4893e3a2c4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_fb2562f3267c33570dc44f4893e3a2c4 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_value );
        tmp_compexpr_left_1 = par_value;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_assattr_target_1;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 555;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_1 = tmp_mvar_value_1;
            frame_fb2562f3267c33570dc44f4893e3a2c4->m_frame.f_lineno = 555;
            tmp_assattr_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_HTTPHeaders );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 555;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_assattr_target_1 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__headers, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 555;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_assattr_target_2;
            CHECK_OBJECT( par_value );
            tmp_assattr_name_2 = par_value;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_2 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain__headers, tmp_assattr_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 557;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_fb2562f3267c33570dc44f4893e3a2c4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_fb2562f3267c33570dc44f4893e3a2c4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_fb2562f3267c33570dc44f4893e3a2c4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_fb2562f3267c33570dc44f4893e3a2c4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_fb2562f3267c33570dc44f4893e3a2c4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_fb2562f3267c33570dc44f4893e3a2c4,
        type_description_1,
        par_self,
        par_value
    );


    // Release cached frame.
    if ( frame_fb2562f3267c33570dc44f4893e3a2c4 == cache_frame_fb2562f3267c33570dc44f4893e3a2c4 )
    {
        Py_DECREF( frame_fb2562f3267c33570dc44f4893e3a2c4 );
    }
    cache_frame_fb2562f3267c33570dc44f4893e3a2c4 = NULL;

    assertFrameObject( frame_fb2562f3267c33570dc44f4893e3a2c4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_16_headers );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_16_headers );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_17_body( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3c36f9c232036606bbbd432d349ddfa8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_3c36f9c232036606bbbd432d349ddfa8 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3c36f9c232036606bbbd432d349ddfa8, codeobj_3c36f9c232036606bbbd432d349ddfa8, module_tornado$httpclient, sizeof(void *) );
    frame_3c36f9c232036606bbbd432d349ddfa8 = cache_frame_3c36f9c232036606bbbd432d349ddfa8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3c36f9c232036606bbbd432d349ddfa8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3c36f9c232036606bbbd432d349ddfa8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__body );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 561;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3c36f9c232036606bbbd432d349ddfa8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_3c36f9c232036606bbbd432d349ddfa8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3c36f9c232036606bbbd432d349ddfa8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3c36f9c232036606bbbd432d349ddfa8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3c36f9c232036606bbbd432d349ddfa8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3c36f9c232036606bbbd432d349ddfa8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3c36f9c232036606bbbd432d349ddfa8,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_3c36f9c232036606bbbd432d349ddfa8 == cache_frame_3c36f9c232036606bbbd432d349ddfa8 )
    {
        Py_DECREF( frame_3c36f9c232036606bbbd432d349ddfa8 );
    }
    cache_frame_3c36f9c232036606bbbd432d349ddfa8 = NULL;

    assertFrameObject( frame_3c36f9c232036606bbbd432d349ddfa8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_17_body );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_17_body );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_18_body( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_value = python_pars[ 1 ];
    struct Nuitka_FrameObject *frame_71afdc46e7a96471c6239fb00dc242e8;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_71afdc46e7a96471c6239fb00dc242e8 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_71afdc46e7a96471c6239fb00dc242e8, codeobj_71afdc46e7a96471c6239fb00dc242e8, module_tornado$httpclient, sizeof(void *)+sizeof(void *) );
    frame_71afdc46e7a96471c6239fb00dc242e8 = cache_frame_71afdc46e7a96471c6239fb00dc242e8;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_71afdc46e7a96471c6239fb00dc242e8 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_71afdc46e7a96471c6239fb00dc242e8 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_utf8 );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_utf8 );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "utf8" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 565;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_value );
        tmp_args_element_name_1 = par_value;
        frame_71afdc46e7a96471c6239fb00dc242e8->m_frame.f_lineno = 565;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assattr_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 565;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__body, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 565;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_71afdc46e7a96471c6239fb00dc242e8 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_71afdc46e7a96471c6239fb00dc242e8 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_71afdc46e7a96471c6239fb00dc242e8, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_71afdc46e7a96471c6239fb00dc242e8->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_71afdc46e7a96471c6239fb00dc242e8, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_71afdc46e7a96471c6239fb00dc242e8,
        type_description_1,
        par_self,
        par_value
    );


    // Release cached frame.
    if ( frame_71afdc46e7a96471c6239fb00dc242e8 == cache_frame_71afdc46e7a96471c6239fb00dc242e8 )
    {
        Py_DECREF( frame_71afdc46e7a96471c6239fb00dc242e8 );
    }
    cache_frame_71afdc46e7a96471c6239fb00dc242e8 = NULL;

    assertFrameObject( frame_71afdc46e7a96471c6239fb00dc242e8 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_18_body );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_value );
    Py_DECREF( par_value );
    par_value = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_18_body );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_19___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_request = python_pars[ 1 ];
    PyObject *par_code = python_pars[ 2 ];
    PyObject *par_headers = python_pars[ 3 ];
    PyObject *par_buffer = python_pars[ 4 ];
    PyObject *par_effective_url = python_pars[ 5 ];
    PyObject *par_error = python_pars[ 6 ];
    PyObject *par_request_time = python_pars[ 7 ];
    PyObject *par_time_info = python_pars[ 8 ];
    PyObject *par_reason = python_pars[ 9 ];
    PyObject *par_start_time = python_pars[ 10 ];
    struct Nuitka_FrameObject *frame_722efbae57c0bf39d22d7c04dffb6d9f;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_722efbae57c0bf39d22d7c04dffb6d9f = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_722efbae57c0bf39d22d7c04dffb6d9f, codeobj_722efbae57c0bf39d22d7c04dffb6d9f, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_722efbae57c0bf39d22d7c04dffb6d9f = cache_frame_722efbae57c0bf39d22d7c04dffb6d9f;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_722efbae57c0bf39d22d7c04dffb6d9f );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_722efbae57c0bf39d22d7c04dffb6d9f ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_request );
        tmp_isinstance_inst_1 = par_request;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain__RequestProxy );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__RequestProxy );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_RequestProxy" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 636;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_isinstance_cls_1 = tmp_mvar_value_1;
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 636;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_source_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( par_request );
            tmp_source_name_1 = par_request;
            tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_request );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 637;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_assattr_target_1 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_request, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 637;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_assattr_target_2;
            CHECK_OBJECT( par_request );
            tmp_assattr_name_2 = par_request;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_2 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_request, tmp_assattr_name_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 639;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_code );
        tmp_assattr_name_3 = par_code;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_code, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 640;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_4;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_assattr_target_4;
        CHECK_OBJECT( par_reason );
        tmp_or_left_value_1 = par_reason;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 641;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 641;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_2;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_responses );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 641;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_code );
        tmp_args_element_name_1 = par_code;
        tmp_args_element_name_2 = const_str_plain_Unknown;
        frame_722efbae57c0bf39d22d7c04dffb6d9f->m_frame.f_lineno = 641;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_or_right_value_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_or_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 641;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        tmp_assattr_name_4 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_assattr_name_4 = tmp_or_left_value_1;
        or_end_1:;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_4 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_4, const_str_plain_reason, tmp_assattr_name_4 );
        Py_DECREF( tmp_assattr_name_4 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 641;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( par_headers );
        tmp_compexpr_left_1 = par_headers;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_2 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assattr_name_5;
            PyObject *tmp_assattr_target_5;
            CHECK_OBJECT( par_headers );
            tmp_assattr_name_5 = par_headers;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_5 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_5, const_str_plain_headers, tmp_assattr_name_5 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 643;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assattr_name_6;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_assattr_target_6;
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 645;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }

            tmp_called_instance_2 = tmp_mvar_value_3;
            frame_722efbae57c0bf39d22d7c04dffb6d9f->m_frame.f_lineno = 645;
            tmp_assattr_name_6 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_HTTPHeaders );
            if ( tmp_assattr_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 645;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_assattr_target_6 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_6, const_str_plain_headers, tmp_assattr_name_6 );
            Py_DECREF( tmp_assattr_name_6 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 645;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assattr_name_7;
        PyObject *tmp_assattr_target_7;
        CHECK_OBJECT( par_buffer );
        tmp_assattr_name_7 = par_buffer;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_7 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_7, const_str_plain_buffer, tmp_assattr_name_7 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 646;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_8;
        PyObject *tmp_assattr_target_8;
        tmp_assattr_name_8 = Py_None;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_8 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_8, const_str_plain__body, tmp_assattr_name_8 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 647;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        CHECK_OBJECT( par_effective_url );
        tmp_compexpr_left_2 = par_effective_url;
        tmp_compexpr_right_2 = Py_None;
        tmp_condition_result_3 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_assattr_name_9;
            PyObject *tmp_source_name_3;
            PyObject *tmp_assattr_target_9;
            CHECK_OBJECT( par_request );
            tmp_source_name_3 = par_request;
            tmp_assattr_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_url );
            if ( tmp_assattr_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 649;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_self );
            tmp_assattr_target_9 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_9, const_str_plain_effective_url, tmp_assattr_name_9 );
            Py_DECREF( tmp_assattr_name_9 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 649;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        goto branch_end_3;
        branch_no_3:;
        {
            PyObject *tmp_assattr_name_10;
            PyObject *tmp_assattr_target_10;
            CHECK_OBJECT( par_effective_url );
            tmp_assattr_name_10 = par_effective_url;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_10 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_10, const_str_plain_effective_url, tmp_assattr_name_10 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 651;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_end_3:;
    }
    {
        PyObject *tmp_assattr_name_11;
        PyObject *tmp_assattr_target_11;
        tmp_assattr_name_11 = Py_False;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_11 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_11, const_str_plain__error_is_response_code, tmp_assattr_name_11 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 652;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_compexpr_left_3;
        PyObject *tmp_compexpr_right_3;
        CHECK_OBJECT( par_error );
        tmp_compexpr_left_3 = par_error;
        tmp_compexpr_right_3 = Py_None;
        tmp_condition_result_4 = ( tmp_compexpr_left_3 == tmp_compexpr_right_3 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            nuitka_bool tmp_condition_result_5;
            int tmp_or_left_truth_2;
            nuitka_bool tmp_or_left_value_2;
            nuitka_bool tmp_or_right_value_2;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            PyObject *tmp_source_name_4;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( par_self );
            tmp_source_name_4 = par_self;
            tmp_compexpr_left_4 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_code );
            if ( tmp_compexpr_left_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 654;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_4 = const_int_pos_200;
            tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            Py_DECREF( tmp_compexpr_left_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 654;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_or_left_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_or_left_truth_2 = tmp_or_left_value_2 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_or_left_truth_2 == 1 )
            {
                goto or_left_2;
            }
            else
            {
                goto or_right_2;
            }
            or_right_2:;
            CHECK_OBJECT( par_self );
            tmp_source_name_5 = par_self;
            tmp_compexpr_left_5 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_code );
            if ( tmp_compexpr_left_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 654;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_5 = const_int_pos_300;
            tmp_res = RICH_COMPARE_BOOL_GTE_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            Py_DECREF( tmp_compexpr_left_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 654;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
            tmp_or_right_value_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_5 = tmp_or_right_value_2;
            goto or_end_2;
            or_left_2:;
            tmp_condition_result_5 = tmp_or_left_value_2;
            or_end_2:;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_5;
            }
            else
            {
                goto branch_no_5;
            }
            branch_yes_5:;
            {
                PyObject *tmp_assattr_name_12;
                PyObject *tmp_assattr_target_12;
                tmp_assattr_name_12 = Py_True;
                CHECK_OBJECT( par_self );
                tmp_assattr_target_12 = par_self;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_12, const_str_plain__error_is_response_code, tmp_assattr_name_12 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 655;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            {
                PyObject *tmp_assattr_name_13;
                PyObject *tmp_called_name_1;
                PyObject *tmp_mvar_value_4;
                PyObject *tmp_args_name_1;
                PyObject *tmp_tuple_element_1;
                PyObject *tmp_source_name_6;
                PyObject *tmp_kw_name_1;
                PyObject *tmp_dict_key_1;
                PyObject *tmp_dict_value_1;
                PyObject *tmp_source_name_7;
                PyObject *tmp_dict_key_2;
                PyObject *tmp_dict_value_2;
                PyObject *tmp_assattr_target_13;
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPError );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPError );
                }

                if ( tmp_mvar_value_4 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPError" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 656;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }

                tmp_called_name_1 = tmp_mvar_value_4;
                CHECK_OBJECT( par_self );
                tmp_source_name_6 = par_self;
                tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_code );
                if ( tmp_tuple_element_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 656;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_args_name_1 = PyTuple_New( 1 );
                PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
                tmp_dict_key_1 = const_str_plain_message;
                CHECK_OBJECT( par_self );
                tmp_source_name_7 = par_self;
                tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_reason );
                if ( tmp_dict_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_args_name_1 );

                    exception_lineno = 656;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                tmp_kw_name_1 = _PyDict_NewPresized( 2 );
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
                Py_DECREF( tmp_dict_value_1 );
                assert( !(tmp_res != 0) );
                tmp_dict_key_2 = const_str_plain_response;
                CHECK_OBJECT( par_self );
                tmp_dict_value_2 = par_self;
                tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
                assert( !(tmp_res != 0) );
                frame_722efbae57c0bf39d22d7c04dffb6d9f->m_frame.f_lineno = 656;
                tmp_assattr_name_13 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
                Py_DECREF( tmp_args_name_1 );
                Py_DECREF( tmp_kw_name_1 );
                if ( tmp_assattr_name_13 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 656;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_self );
                tmp_assattr_target_13 = par_self;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_13, const_str_plain_error, tmp_assattr_name_13 );
                Py_DECREF( tmp_assattr_name_13 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 656;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            goto branch_end_5;
            branch_no_5:;
            {
                PyObject *tmp_assattr_name_14;
                PyObject *tmp_assattr_target_14;
                tmp_assattr_name_14 = Py_None;
                CHECK_OBJECT( par_self );
                tmp_assattr_target_14 = par_self;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_14, const_str_plain_error, tmp_assattr_name_14 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 658;
                    type_description_1 = "ooooooooooo";
                    goto frame_exception_exit_1;
                }
            }
            branch_end_5:;
        }
        goto branch_end_4;
        branch_no_4:;
        {
            PyObject *tmp_assattr_name_15;
            PyObject *tmp_assattr_target_15;
            CHECK_OBJECT( par_error );
            tmp_assattr_name_15 = par_error;
            CHECK_OBJECT( par_self );
            tmp_assattr_target_15 = par_self;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_15, const_str_plain_error, tmp_assattr_name_15 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 660;
                type_description_1 = "ooooooooooo";
                goto frame_exception_exit_1;
            }
        }
        branch_end_4:;
    }
    {
        PyObject *tmp_assattr_name_16;
        PyObject *tmp_assattr_target_16;
        CHECK_OBJECT( par_start_time );
        tmp_assattr_name_16 = par_start_time;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_16 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_16, const_str_plain_start_time, tmp_assattr_name_16 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 661;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_17;
        PyObject *tmp_assattr_target_17;
        CHECK_OBJECT( par_request_time );
        tmp_assattr_name_17 = par_request_time;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_17 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_17, const_str_plain_request_time, tmp_assattr_name_17 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 662;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_18;
        int tmp_or_left_truth_3;
        PyObject *tmp_or_left_value_3;
        PyObject *tmp_or_right_value_3;
        PyObject *tmp_assattr_target_18;
        CHECK_OBJECT( par_time_info );
        tmp_or_left_value_3 = par_time_info;
        tmp_or_left_truth_3 = CHECK_IF_TRUE( tmp_or_left_value_3 );
        if ( tmp_or_left_truth_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 663;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_3 == 1 )
        {
            goto or_left_3;
        }
        else
        {
            goto or_right_3;
        }
        or_right_3:;
        tmp_or_right_value_3 = PyDict_New();
        tmp_assattr_name_18 = tmp_or_right_value_3;
        goto or_end_3;
        or_left_3:;
        Py_INCREF( tmp_or_left_value_3 );
        tmp_assattr_name_18 = tmp_or_left_value_3;
        or_end_3:;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_18 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_18, const_str_plain_time_info, tmp_assattr_name_18 );
        Py_DECREF( tmp_assattr_name_18 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 663;
            type_description_1 = "ooooooooooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_722efbae57c0bf39d22d7c04dffb6d9f );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_722efbae57c0bf39d22d7c04dffb6d9f );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_722efbae57c0bf39d22d7c04dffb6d9f, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_722efbae57c0bf39d22d7c04dffb6d9f->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_722efbae57c0bf39d22d7c04dffb6d9f, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_722efbae57c0bf39d22d7c04dffb6d9f,
        type_description_1,
        par_self,
        par_request,
        par_code,
        par_headers,
        par_buffer,
        par_effective_url,
        par_error,
        par_request_time,
        par_time_info,
        par_reason,
        par_start_time
    );


    // Release cached frame.
    if ( frame_722efbae57c0bf39d22d7c04dffb6d9f == cache_frame_722efbae57c0bf39d22d7c04dffb6d9f )
    {
        Py_DECREF( frame_722efbae57c0bf39d22d7c04dffb6d9f );
    }
    cache_frame_722efbae57c0bf39d22d7c04dffb6d9f = NULL;

    assertFrameObject( frame_722efbae57c0bf39d22d7c04dffb6d9f );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_19___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    CHECK_OBJECT( (PyObject *)par_headers );
    Py_DECREF( par_headers );
    par_headers = NULL;

    CHECK_OBJECT( (PyObject *)par_buffer );
    Py_DECREF( par_buffer );
    par_buffer = NULL;

    CHECK_OBJECT( (PyObject *)par_effective_url );
    Py_DECREF( par_effective_url );
    par_effective_url = NULL;

    CHECK_OBJECT( (PyObject *)par_error );
    Py_DECREF( par_error );
    par_error = NULL;

    CHECK_OBJECT( (PyObject *)par_request_time );
    Py_DECREF( par_request_time );
    par_request_time = NULL;

    CHECK_OBJECT( (PyObject *)par_time_info );
    Py_DECREF( par_time_info );
    par_time_info = NULL;

    CHECK_OBJECT( (PyObject *)par_reason );
    Py_DECREF( par_reason );
    par_reason = NULL;

    CHECK_OBJECT( (PyObject *)par_start_time );
    Py_DECREF( par_start_time );
    par_start_time = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    CHECK_OBJECT( (PyObject *)par_headers );
    Py_DECREF( par_headers );
    par_headers = NULL;

    CHECK_OBJECT( (PyObject *)par_buffer );
    Py_DECREF( par_buffer );
    par_buffer = NULL;

    CHECK_OBJECT( (PyObject *)par_effective_url );
    Py_DECREF( par_effective_url );
    par_effective_url = NULL;

    CHECK_OBJECT( (PyObject *)par_error );
    Py_DECREF( par_error );
    par_error = NULL;

    CHECK_OBJECT( (PyObject *)par_request_time );
    Py_DECREF( par_request_time );
    par_request_time = NULL;

    CHECK_OBJECT( (PyObject *)par_time_info );
    Py_DECREF( par_time_info );
    par_time_info = NULL;

    CHECK_OBJECT( (PyObject *)par_reason );
    Py_DECREF( par_reason );
    par_reason = NULL;

    CHECK_OBJECT( (PyObject *)par_start_time );
    Py_DECREF( par_start_time );
    par_start_time = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_19___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_20_body( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d7e781f31b6acc7ad3976fc72cd431a0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_d7e781f31b6acc7ad3976fc72cd431a0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d7e781f31b6acc7ad3976fc72cd431a0, codeobj_d7e781f31b6acc7ad3976fc72cd431a0, module_tornado$httpclient, sizeof(void *) );
    frame_d7e781f31b6acc7ad3976fc72cd431a0 = cache_frame_d7e781f31b6acc7ad3976fc72cd431a0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d7e781f31b6acc7ad3976fc72cd431a0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d7e781f31b6acc7ad3976fc72cd431a0 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_buffer );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 667;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = const_bytes_empty;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain__body );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 669;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 == tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assattr_name_1;
                PyObject *tmp_called_instance_1;
                PyObject *tmp_source_name_3;
                PyObject *tmp_assattr_target_1;
                CHECK_OBJECT( par_self );
                tmp_source_name_3 = par_self;
                tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_buffer );
                if ( tmp_called_instance_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 670;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                frame_d7e781f31b6acc7ad3976fc72cd431a0->m_frame.f_lineno = 670;
                tmp_assattr_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_getvalue );
                Py_DECREF( tmp_called_instance_1 );
                if ( tmp_assattr_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 670;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_self );
                tmp_assattr_target_1 = par_self;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain__body, tmp_assattr_name_1 );
                Py_DECREF( tmp_assattr_name_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 670;
                    type_description_1 = "o";
                    goto frame_exception_exit_1;
                }
            }
            branch_no_2:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain__body );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 672;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d7e781f31b6acc7ad3976fc72cd431a0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d7e781f31b6acc7ad3976fc72cd431a0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d7e781f31b6acc7ad3976fc72cd431a0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d7e781f31b6acc7ad3976fc72cd431a0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d7e781f31b6acc7ad3976fc72cd431a0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d7e781f31b6acc7ad3976fc72cd431a0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d7e781f31b6acc7ad3976fc72cd431a0,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_d7e781f31b6acc7ad3976fc72cd431a0 == cache_frame_d7e781f31b6acc7ad3976fc72cd431a0 )
    {
        Py_DECREF( frame_d7e781f31b6acc7ad3976fc72cd431a0 );
    }
    cache_frame_d7e781f31b6acc7ad3976fc72cd431a0 = NULL;

    assertFrameObject( frame_d7e781f31b6acc7ad3976fc72cd431a0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_20_body );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_20_body );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_21_rethrow( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0345d03b190573e043fea91e5df23663;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0345d03b190573e043fea91e5df23663 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0345d03b190573e043fea91e5df23663, codeobj_0345d03b190573e043fea91e5df23663, module_tornado$httpclient, sizeof(void *) );
    frame_0345d03b190573e043fea91e5df23663 = cache_frame_0345d03b190573e043fea91e5df23663;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0345d03b190573e043fea91e5df23663 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0345d03b190573e043fea91e5df23663 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_error );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 676;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 676;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_raise_type_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_error );
            if ( tmp_raise_type_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 677;
                type_description_1 = "o";
                goto frame_exception_exit_1;
            }
            exception_type = tmp_raise_type_1;
            exception_lineno = 677;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0345d03b190573e043fea91e5df23663 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0345d03b190573e043fea91e5df23663 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0345d03b190573e043fea91e5df23663, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0345d03b190573e043fea91e5df23663->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0345d03b190573e043fea91e5df23663, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0345d03b190573e043fea91e5df23663,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_0345d03b190573e043fea91e5df23663 == cache_frame_0345d03b190573e043fea91e5df23663 )
    {
        Py_DECREF( frame_0345d03b190573e043fea91e5df23663 );
    }
    cache_frame_0345d03b190573e043fea91e5df23663 = NULL;

    assertFrameObject( frame_0345d03b190573e043fea91e5df23663 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_21_rethrow );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_21_rethrow );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_22___repr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *var_args = NULL;
    PyObject *tmp_genexpr_1__$0 = NULL;
    struct Nuitka_FrameObject *frame_d4736e092cab086b98221122b05139d0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_d4736e092cab086b98221122b05139d0 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d4736e092cab086b98221122b05139d0, codeobj_d4736e092cab086b98221122b05139d0, module_tornado$httpclient, sizeof(void *)+sizeof(void *) );
    frame_d4736e092cab086b98221122b05139d0 = cache_frame_d4736e092cab086b98221122b05139d0;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d4736e092cab086b98221122b05139d0 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d4736e092cab086b98221122b05139d0 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_element_name_1;
        tmp_source_name_1 = const_str_chr_44;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_join );
        assert( !(tmp_called_name_1 == NULL) );
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_iter_arg_1;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_2;
            tmp_called_name_2 = LOOKUP_BUILTIN( const_str_plain_sorted );
            assert( tmp_called_name_2 != NULL );
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE_DICT_SLOT( tmp_source_name_2 );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 680;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_d4736e092cab086b98221122b05139d0->m_frame.f_lineno = 680;
            tmp_args_element_name_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_items );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 680;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_d4736e092cab086b98221122b05139d0->m_frame.f_lineno = 680;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_iter_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 680;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
            Py_DECREF( tmp_iter_arg_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 680;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( tmp_genexpr_1__$0 == NULL );
            tmp_genexpr_1__$0 = tmp_assign_source_2;
        }
        // Tried code:
        tmp_args_element_name_1 = tornado$httpclient$$$function_22___repr__$$$genexpr_1_genexpr_maker();

        ((struct Nuitka_GeneratorObject *)tmp_args_element_name_1)->m_closure[0] = PyCell_NEW0( tmp_genexpr_1__$0 );


        goto try_return_handler_2;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_22___repr__ );
        return NULL;
        // Return handler code:
        try_return_handler_2:;
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        goto outline_result_1;
        // End of try:
        CHECK_OBJECT( (PyObject *)tmp_genexpr_1__$0 );
        Py_DECREF( tmp_genexpr_1__$0 );
        tmp_genexpr_1__$0 = NULL;

        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_22___repr__ );
        return NULL;
        outline_result_1:;
        frame_d4736e092cab086b98221122b05139d0->m_frame.f_lineno = 680;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 680;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_args == NULL );
        var_args = tmp_assign_source_1;
    }
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        tmp_left_name_1 = const_str_digest_a7636f57d1f8f80548278378b7bf84e2;
        CHECK_OBJECT( par_self );
        tmp_source_name_4 = par_self;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE_CLASS_SLOT( tmp_source_name_4 );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 681;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain___name__ );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 681;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( var_args );
        tmp_tuple_element_1 = var_args;
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 681;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d4736e092cab086b98221122b05139d0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_d4736e092cab086b98221122b05139d0 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d4736e092cab086b98221122b05139d0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d4736e092cab086b98221122b05139d0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d4736e092cab086b98221122b05139d0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d4736e092cab086b98221122b05139d0, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d4736e092cab086b98221122b05139d0,
        type_description_1,
        par_self,
        var_args
    );


    // Release cached frame.
    if ( frame_d4736e092cab086b98221122b05139d0 == cache_frame_d4736e092cab086b98221122b05139d0 )
    {
        Py_DECREF( frame_d4736e092cab086b98221122b05139d0 );
    }
    cache_frame_d4736e092cab086b98221122b05139d0 = NULL;

    assertFrameObject( frame_d4736e092cab086b98221122b05139d0 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_22___repr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)var_args );
    Py_DECREF( var_args );
    var_args = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    Py_XDECREF( var_args );
    var_args = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_22___repr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



struct tornado$httpclient$$$function_22___repr__$$$genexpr_1_genexpr_locals {
    PyObject *var_i;
    PyObject *tmp_iter_value_0;
    char const *type_description_1;
    PyObject *exception_type;
    PyObject *exception_value;
    PyTracebackObject *exception_tb;
    int exception_lineno;
    char yield_tmps[1024];
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    int exception_keeper_lineno_2;
};

static PyObject *tornado$httpclient$$$function_22___repr__$$$genexpr_1_genexpr_context( struct Nuitka_GeneratorObject *generator, PyObject *yield_return_value )
{
    CHECK_OBJECT( (PyObject *)generator );
    assert( Nuitka_Generator_Check( (PyObject *)generator ) );

    // Heap access if used.
    struct tornado$httpclient$$$function_22___repr__$$$genexpr_1_genexpr_locals *generator_heap = (struct tornado$httpclient$$$function_22___repr__$$$genexpr_1_genexpr_locals *)generator->m_heap_storage;

    // Dispatch to yield based on return label index:
    switch(generator->m_yield_return_index) {
    case 1: goto yield_return_1;
    }

    // Local variable initialization
    static struct Nuitka_FrameObject *cache_m_frame = NULL;
    generator_heap->var_i = NULL;
    generator_heap->tmp_iter_value_0 = NULL;
    generator_heap->type_description_1 = NULL;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    // Actual generator function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_m_frame, codeobj_bd02e7133760dec6a20885e7a008916c, module_tornado$httpclient, sizeof(void *)+sizeof(void *) );
    generator->m_frame = cache_m_frame;

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    Py_INCREF( generator->m_frame );
    assert( Py_REFCNT( generator->m_frame ) == 2 ); // Frame stack

#if PYTHON_VERSION >= 340
    generator->m_frame->m_frame.f_gen = (PyObject *)generator;
#endif

    Py_CLEAR( generator->m_frame->m_frame.f_back );

    generator->m_frame->m_frame.f_back = PyThreadState_GET()->frame;
    Py_INCREF( generator->m_frame->m_frame.f_back );

    PyThreadState_GET()->frame = &generator->m_frame->m_frame;
    Py_INCREF( generator->m_frame );

    Nuitka_Frame_MarkAsExecuting( generator->m_frame );

#if PYTHON_VERSION >= 300
    // Accept currently existing exception as the one to publish again when we
    // yield or yield from.
    {
        PyThreadState *thread_state = PyThreadState_GET();

#if PYTHON_VERSION < 370
        generator->m_frame->m_frame.f_exc_type = EXC_TYPE( thread_state );
    if ( generator->m_frame->m_frame.f_exc_type == Py_None ) generator->m_frame->m_frame.f_exc_type = NULL;
        Py_XINCREF( generator->m_frame->m_frame.f_exc_type );
    generator->m_frame->m_frame.f_exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_value );
    generator->m_frame->m_frame.f_exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_frame->m_frame.f_exc_traceback );
#else
        generator->m_exc_state.exc_type = EXC_TYPE( thread_state );
        if ( generator->m_exc_state.exc_type == Py_None ) generator->m_exc_state.exc_type = NULL;
        Py_XINCREF( generator->m_exc_state.exc_type );
        generator->m_exc_state.exc_value = EXC_VALUE( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_value );
        generator->m_exc_state.exc_traceback = EXC_TRACEBACK( thread_state );
        Py_XINCREF( generator->m_exc_state.exc_traceback );
#endif
    }

#endif

    // Framed code:
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_1;
        CHECK_OBJECT( PyCell_GET( generator->m_closure[0] ) );
        tmp_next_source_1 = PyCell_GET( generator->m_closure[0] );
        tmp_assign_source_1 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );
                generator_heap->type_description_1 = "No";
                generator_heap->exception_lineno = 680;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = generator_heap->tmp_iter_value_0;
            generator_heap->tmp_iter_value_0 = tmp_assign_source_1;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( generator_heap->tmp_iter_value_0 );
        tmp_assign_source_2 = generator_heap->tmp_iter_value_0;
        {
            PyObject *old = generator_heap->var_i;
            generator_heap->var_i = tmp_assign_source_2;
            Py_INCREF( generator_heap->var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_expression_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        NUITKA_MAY_BE_UNUSED PyObject *tmp_yield_result_1;
        tmp_left_name_1 = const_str_digest_69b6ecec1ddb0b99fd2288f02d4834e9;
        CHECK_OBJECT( generator_heap->var_i );
        tmp_right_name_1 = generator_heap->var_i;
        tmp_expression_name_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_expression_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 680;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        Nuitka_PreserveHeap( generator_heap->yield_tmps, &tmp_left_name_1, sizeof(PyObject *), &tmp_right_name_1, sizeof(PyObject *), NULL );
        generator->m_yield_return_index = 1;
        return tmp_expression_name_1;
        yield_return_1:
        Nuitka_RestoreHeap( generator_heap->yield_tmps, &tmp_left_name_1, sizeof(PyObject *), &tmp_right_name_1, sizeof(PyObject *), NULL );
        if ( yield_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


            generator_heap->exception_lineno = 680;
            generator_heap->type_description_1 = "No";
            goto try_except_handler_2;
        }
        tmp_yield_result_1 = yield_return_value;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &generator_heap->exception_type, &generator_heap->exception_value, &generator_heap->exception_tb );


        generator_heap->exception_lineno = 680;
        generator_heap->type_description_1 = "No";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    generator_heap->exception_keeper_type_1 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_1 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_1 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_1 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_1;
    generator_heap->exception_value = generator_heap->exception_keeper_value_1;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_1;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

    Nuitka_Frame_MarkAsNotExecuting( generator->m_frame );

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    // Allow re-use of the frame again.
    Py_DECREF( generator->m_frame );
    goto frame_no_exception_1;

    frame_exception_exit_1:;

    // If it's not an exit exception, consider and create a traceback for it.
    if ( !EXCEPTION_MATCH_GENERATOR( generator_heap->exception_type ) )
    {
        if ( generator_heap->exception_tb == NULL )
        {
            generator_heap->exception_tb = MAKE_TRACEBACK( generator->m_frame, generator_heap->exception_lineno );
        }
        else if ( generator_heap->exception_tb->tb_frame != &generator->m_frame->m_frame )
        {
            generator_heap->exception_tb = ADD_TRACEBACK( generator_heap->exception_tb, generator->m_frame, generator_heap->exception_lineno );
        }

        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)generator->m_frame,
            generator_heap->type_description_1,
            NULL,
            generator_heap->var_i
        );


        // Release cached frame.
        if ( generator->m_frame == cache_m_frame )
        {
            Py_DECREF( generator->m_frame );
        }
        cache_m_frame = NULL;

        assertFrameObject( generator->m_frame );
    }

#if PYTHON_VERSION >= 370
    Py_CLEAR( generator->m_exc_state.exc_type );
    Py_CLEAR( generator->m_exc_state.exc_value );
    Py_CLEAR( generator->m_exc_state.exc_traceback );
#elif PYTHON_VERSION >= 300
    Py_CLEAR( generator->m_frame->m_frame.f_exc_type );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_value );
    Py_CLEAR( generator->m_frame->m_frame.f_exc_traceback );
#endif

    Py_DECREF( generator->m_frame );

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_1:;
    generator_heap->exception_keeper_type_2 = generator_heap->exception_type;
    generator_heap->exception_keeper_value_2 = generator_heap->exception_value;
    generator_heap->exception_keeper_tb_2 = generator_heap->exception_tb;
    generator_heap->exception_keeper_lineno_2 = generator_heap->exception_lineno;
    generator_heap->exception_type = NULL;
    generator_heap->exception_value = NULL;
    generator_heap->exception_tb = NULL;
    generator_heap->exception_lineno = 0;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;

    // Re-raise.
    generator_heap->exception_type = generator_heap->exception_keeper_type_2;
    generator_heap->exception_value = generator_heap->exception_keeper_value_2;
    generator_heap->exception_tb = generator_heap->exception_keeper_tb_2;
    generator_heap->exception_lineno = generator_heap->exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:
    try_end_2:;
    Py_XDECREF( generator_heap->tmp_iter_value_0 );
    generator_heap->tmp_iter_value_0 = NULL;

    Py_XDECREF( generator_heap->var_i );
    generator_heap->var_i = NULL;



    return NULL;

    function_exception_exit:
    assert( generator_heap->exception_type );
    RESTORE_ERROR_OCCURRED( generator_heap->exception_type, generator_heap->exception_value, generator_heap->exception_tb );

    return NULL;

}

static PyObject *tornado$httpclient$$$function_22___repr__$$$genexpr_1_genexpr_maker( void )
{
    return Nuitka_Generator_New(
        tornado$httpclient$$$function_22___repr__$$$genexpr_1_genexpr_context,
        module_tornado$httpclient,
        const_str_angle_genexpr,
#if PYTHON_VERSION >= 350
        const_str_digest_0ac86c737ebdecf2bdc0ee7a1913d4c6,
#endif
        codeobj_bd02e7133760dec6a20885e7a008916c,
        1,
        sizeof(struct tornado$httpclient$$$function_22___repr__$$$genexpr_1_genexpr_locals)
    );
}


static PyObject *impl_tornado$httpclient$$$function_23___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_code = python_pars[ 1 ];
    PyObject *par_message = python_pars[ 2 ];
    PyObject *par_response = python_pars[ 3 ];
    struct Nuitka_FrameObject *frame_51b641b6a50b5ff292086f3f52f64be5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_51b641b6a50b5ff292086f3f52f64be5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_51b641b6a50b5ff292086f3f52f64be5, codeobj_51b641b6a50b5ff292086f3f52f64be5, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_51b641b6a50b5ff292086f3f52f64be5 = cache_frame_51b641b6a50b5ff292086f3f52f64be5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_51b641b6a50b5ff292086f3f52f64be5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_51b641b6a50b5ff292086f3f52f64be5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_code );
        tmp_assattr_name_1 = par_code;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_code, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 708;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        int tmp_or_left_truth_1;
        PyObject *tmp_or_left_value_1;
        PyObject *tmp_or_right_value_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_message );
        tmp_or_left_value_1 = par_message;
        tmp_or_left_truth_1 = CHECK_IF_TRUE( tmp_or_left_value_1 );
        if ( tmp_or_left_truth_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 709;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
        if ( tmp_or_left_truth_1 == 1 )
        {
            goto or_left_1;
        }
        else
        {
            goto or_right_1;
        }
        or_right_1:;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 709;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_responses );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 709;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_code );
        tmp_args_element_name_1 = par_code;
        tmp_args_element_name_2 = const_str_plain_Unknown;
        frame_51b641b6a50b5ff292086f3f52f64be5->m_frame.f_lineno = 709;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_or_right_value_1 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_or_right_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 709;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
        tmp_assattr_name_2 = tmp_or_right_value_1;
        goto or_end_1;
        or_left_1:;
        Py_INCREF( tmp_or_left_value_1 );
        tmp_assattr_name_2 = tmp_or_left_value_1;
        or_end_1:;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_message, tmp_assattr_name_2 );
        Py_DECREF( tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 709;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_3;
        PyObject *tmp_assattr_target_3;
        CHECK_OBJECT( par_response );
        tmp_assattr_name_3 = par_response;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_3 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_3, const_str_plain_response, tmp_assattr_name_3 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 710;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_type_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_object_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPClientError );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPClientError );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPClientError" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 711;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }

        tmp_type_name_1 = tmp_mvar_value_2;
        CHECK_OBJECT( par_self );
        tmp_object_name_1 = par_self;
        tmp_called_instance_2 = BUILTIN_SUPER( tmp_type_name_1, tmp_object_name_1 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 711;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_code );
        tmp_args_element_name_3 = par_code;
        CHECK_OBJECT( par_message );
        tmp_args_element_name_4 = par_message;
        CHECK_OBJECT( par_response );
        tmp_args_element_name_5 = par_response;
        frame_51b641b6a50b5ff292086f3f52f64be5->m_frame.f_lineno = 711;
        {
            PyObject *call_args[] = { tmp_args_element_name_3, tmp_args_element_name_4, tmp_args_element_name_5 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS3( tmp_called_instance_2, const_str_plain___init__, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 711;
            type_description_1 = "ooooN";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_51b641b6a50b5ff292086f3f52f64be5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_51b641b6a50b5ff292086f3f52f64be5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_51b641b6a50b5ff292086f3f52f64be5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_51b641b6a50b5ff292086f3f52f64be5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_51b641b6a50b5ff292086f3f52f64be5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_51b641b6a50b5ff292086f3f52f64be5,
        type_description_1,
        par_self,
        par_code,
        par_message,
        par_response,
        NULL
    );


    // Release cached frame.
    if ( frame_51b641b6a50b5ff292086f3f52f64be5 == cache_frame_51b641b6a50b5ff292086f3f52f64be5 )
    {
        Py_DECREF( frame_51b641b6a50b5ff292086f3f52f64be5 );
    }
    cache_frame_51b641b6a50b5ff292086f3f52f64be5 = NULL;

    assertFrameObject( frame_51b641b6a50b5ff292086f3f52f64be5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_23___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_response );
    Py_DECREF( par_response );
    par_response = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_code );
    Py_DECREF( par_code );
    par_code = NULL;

    CHECK_OBJECT( (PyObject *)par_message );
    Py_DECREF( par_message );
    par_message = NULL;

    CHECK_OBJECT( (PyObject *)par_response );
    Py_DECREF( par_response );
    par_response = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_23___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_24___str__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6b3619961e2f6e053d8497a025234733;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6b3619961e2f6e053d8497a025234733 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6b3619961e2f6e053d8497a025234733, codeobj_6b3619961e2f6e053d8497a025234733, module_tornado$httpclient, sizeof(void *) );
    frame_6b3619961e2f6e053d8497a025234733 = cache_frame_6b3619961e2f6e053d8497a025234733;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6b3619961e2f6e053d8497a025234733 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6b3619961e2f6e053d8497a025234733 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        tmp_left_name_1 = const_str_digest_f18f7ca0806c368196be935d559815ea;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_code );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 714;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = PyTuple_New( 2 );
        PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_1 );
        CHECK_OBJECT( par_self );
        tmp_source_name_2 = par_self;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_message );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_right_name_1 );

            exception_lineno = 714;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_1 );
        tmp_return_value = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 714;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b3619961e2f6e053d8497a025234733 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b3619961e2f6e053d8497a025234733 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6b3619961e2f6e053d8497a025234733 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6b3619961e2f6e053d8497a025234733, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6b3619961e2f6e053d8497a025234733->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6b3619961e2f6e053d8497a025234733, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6b3619961e2f6e053d8497a025234733,
        type_description_1,
        par_self
    );


    // Release cached frame.
    if ( frame_6b3619961e2f6e053d8497a025234733 == cache_frame_6b3619961e2f6e053d8497a025234733 )
    {
        Py_DECREF( frame_6b3619961e2f6e053d8497a025234733 );
    }
    cache_frame_6b3619961e2f6e053d8497a025234733 = NULL;

    assertFrameObject( frame_6b3619961e2f6e053d8497a025234733 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_24___str__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_24___str__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_25___init__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_request = python_pars[ 1 ];
    PyObject *par_defaults = python_pars[ 2 ];
    struct Nuitka_FrameObject *frame_e82fccd137c01e8770baa23fc14bdbff;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_e82fccd137c01e8770baa23fc14bdbff = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e82fccd137c01e8770baa23fc14bdbff, codeobj_e82fccd137c01e8770baa23fc14bdbff, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e82fccd137c01e8770baa23fc14bdbff = cache_frame_e82fccd137c01e8770baa23fc14bdbff;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e82fccd137c01e8770baa23fc14bdbff );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e82fccd137c01e8770baa23fc14bdbff ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( par_request );
        tmp_assattr_name_1 = par_request;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_1 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_request, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 735;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        CHECK_OBJECT( par_defaults );
        tmp_assattr_name_2 = par_defaults;
        CHECK_OBJECT( par_self );
        tmp_assattr_target_2 = par_self;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_defaults, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 736;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e82fccd137c01e8770baa23fc14bdbff );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e82fccd137c01e8770baa23fc14bdbff );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e82fccd137c01e8770baa23fc14bdbff, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e82fccd137c01e8770baa23fc14bdbff->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e82fccd137c01e8770baa23fc14bdbff, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e82fccd137c01e8770baa23fc14bdbff,
        type_description_1,
        par_self,
        par_request,
        par_defaults
    );


    // Release cached frame.
    if ( frame_e82fccd137c01e8770baa23fc14bdbff == cache_frame_e82fccd137c01e8770baa23fc14bdbff )
    {
        Py_DECREF( frame_e82fccd137c01e8770baa23fc14bdbff );
    }
    cache_frame_e82fccd137c01e8770baa23fc14bdbff = NULL;

    assertFrameObject( frame_e82fccd137c01e8770baa23fc14bdbff );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_25___init__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_defaults );
    Py_DECREF( par_defaults );
    par_defaults = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_request );
    Py_DECREF( par_request );
    par_request = NULL;

    CHECK_OBJECT( (PyObject *)par_defaults );
    Py_DECREF( par_defaults );
    par_defaults = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_25___init__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_26___getattr__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_self = python_pars[ 0 ];
    PyObject *par_name = python_pars[ 1 ];
    PyObject *var_request_attr = NULL;
    struct Nuitka_FrameObject *frame_0e20bad896a4422afeb795351bab726a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *tmp_return_value = NULL;
    static struct Nuitka_FrameObject *cache_frame_0e20bad896a4422afeb795351bab726a = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0e20bad896a4422afeb795351bab726a, codeobj_0e20bad896a4422afeb795351bab726a, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_0e20bad896a4422afeb795351bab726a = cache_frame_0e20bad896a4422afeb795351bab726a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0e20bad896a4422afeb795351bab726a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0e20bad896a4422afeb795351bab726a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_getattr_target_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_getattr_attr_1;
        CHECK_OBJECT( par_self );
        tmp_source_name_1 = par_self;
        tmp_getattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_request );
        if ( tmp_getattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 739;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_name );
        tmp_getattr_attr_1 = par_name;
        tmp_assign_source_1 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, NULL );
        Py_DECREF( tmp_getattr_target_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 739;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_request_attr == NULL );
        var_request_attr = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_request_attr );
        tmp_compexpr_left_1 = var_request_attr;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( var_request_attr );
        tmp_return_value = var_request_attr;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_2;
            CHECK_OBJECT( par_self );
            tmp_source_name_2 = par_self;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_defaults );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 742;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_instance_1;
                PyObject *tmp_source_name_3;
                PyObject *tmp_args_element_name_1;
                PyObject *tmp_args_element_name_2;
                CHECK_OBJECT( par_self );
                tmp_source_name_3 = par_self;
                tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_defaults );
                if ( tmp_called_instance_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 743;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( par_name );
                tmp_args_element_name_1 = par_name;
                tmp_args_element_name_2 = Py_None;
                frame_0e20bad896a4422afeb795351bab726a->m_frame.f_lineno = 743;
                {
                    PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
                    tmp_return_value = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_get, call_args );
                }

                Py_DECREF( tmp_called_instance_1 );
                if ( tmp_return_value == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 743;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                goto frame_return_exit_1;
            }
            goto branch_end_2;
            branch_no_2:;
            tmp_return_value = Py_None;
            Py_INCREF( tmp_return_value );
            goto frame_return_exit_1;
            branch_end_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0e20bad896a4422afeb795351bab726a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_0e20bad896a4422afeb795351bab726a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0e20bad896a4422afeb795351bab726a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0e20bad896a4422afeb795351bab726a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0e20bad896a4422afeb795351bab726a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0e20bad896a4422afeb795351bab726a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0e20bad896a4422afeb795351bab726a,
        type_description_1,
        par_self,
        par_name,
        var_request_attr
    );


    // Release cached frame.
    if ( frame_0e20bad896a4422afeb795351bab726a == cache_frame_0e20bad896a4422afeb795351bab726a )
    {
        Py_DECREF( frame_0e20bad896a4422afeb795351bab726a );
    }
    cache_frame_0e20bad896a4422afeb795351bab726a = NULL;

    assertFrameObject( frame_0e20bad896a4422afeb795351bab726a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_26___getattr__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)var_request_attr );
    Py_DECREF( var_request_attr );
    var_request_attr = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_self );
    Py_DECREF( par_self );
    par_self = NULL;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    Py_XDECREF( var_request_attr );
    var_request_attr = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_26___getattr__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_tornado$httpclient$$$function_27_main( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_define = NULL;
    PyObject *var_options = NULL;
    PyObject *var_parse_command_line = NULL;
    PyObject *var_args = NULL;
    PyObject *var_client = NULL;
    PyObject *var_arg = NULL;
    PyObject *var_response = NULL;
    PyObject *var_e = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_07edb31f42cc29ab8bbf8de92f57e171;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    int tmp_res;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    bool tmp_result;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    static struct Nuitka_FrameObject *cache_frame_07edb31f42cc29ab8bbf8de92f57e171 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_07edb31f42cc29ab8bbf8de92f57e171, codeobj_07edb31f42cc29ab8bbf8de92f57e171, module_tornado$httpclient, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_07edb31f42cc29ab8bbf8de92f57e171 = cache_frame_07edb31f42cc29ab8bbf8de92f57e171;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_07edb31f42cc29ab8bbf8de92f57e171 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_07edb31f42cc29ab8bbf8de92f57e171 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_e8e68d6f1f9fdc5f77d42501a852b12f;
        tmp_globals_name_1 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_c63519c88023de0285858e9e87a81922_tuple;
        tmp_level_name_1 = const_int_0;
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 749;
        tmp_assign_source_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 749;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_1;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_import_name_from_1;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_1 = tmp_import_from_1__module;
        tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_define );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 749;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        assert( var_define == NULL );
        var_define = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_3 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_options );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 749;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        assert( var_options == NULL );
        var_options = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_parse_command_line );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 749;
            type_description_1 = "oooooooo";
            goto try_except_handler_2;
        }
        assert( var_parse_command_line == NULL );
        var_parse_command_line = tmp_assign_source_4;
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( var_define );
        tmp_called_name_1 = var_define;
        tmp_args_name_1 = const_tuple_str_plain_print_headers_tuple;
        tmp_kw_name_1 = PyDict_Copy( const_dict_ae8aa59e73f92c1f97579b3c4dbcc675 );
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 751;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 751;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_name_2;
        PyObject *tmp_kw_name_2;
        CHECK_OBJECT( var_define );
        tmp_called_name_2 = var_define;
        tmp_args_name_2 = const_tuple_str_plain_print_body_tuple;
        tmp_kw_name_2 = PyDict_Copy( const_dict_ffca9ad2e079ce02b380f5ac23e6d883 );
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 752;
        tmp_call_result_2 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 752;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_name_3;
        PyObject *tmp_kw_name_3;
        CHECK_OBJECT( var_define );
        tmp_called_name_3 = var_define;
        tmp_args_name_3 = const_tuple_str_plain_follow_redirects_tuple;
        tmp_kw_name_3 = PyDict_Copy( const_dict_ffca9ad2e079ce02b380f5ac23e6d883 );
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 753;
        tmp_call_result_3 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 753;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_name_4;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_name_4;
        PyObject *tmp_kw_name_4;
        CHECK_OBJECT( var_define );
        tmp_called_name_4 = var_define;
        tmp_args_name_4 = const_tuple_str_plain_validate_cert_tuple;
        tmp_kw_name_4 = PyDict_Copy( const_dict_ffca9ad2e079ce02b380f5ac23e6d883 );
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 754;
        tmp_call_result_4 = CALL_FUNCTION( tmp_called_name_4, tmp_args_name_4, tmp_kw_name_4 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 754;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_called_name_5;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_name_5;
        PyObject *tmp_kw_name_5;
        CHECK_OBJECT( var_define );
        tmp_called_name_5 = var_define;
        tmp_args_name_5 = const_tuple_str_plain_proxy_host_tuple;
        tmp_kw_name_5 = PyDict_Copy( const_dict_b3fa57d622665417c82eaa93a4cf743c );
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 755;
        tmp_call_result_5 = CALL_FUNCTION( tmp_called_name_5, tmp_args_name_5, tmp_kw_name_5 );
        Py_DECREF( tmp_kw_name_5 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 755;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        PyObject *tmp_called_name_6;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_name_6;
        PyObject *tmp_kw_name_6;
        CHECK_OBJECT( var_define );
        tmp_called_name_6 = var_define;
        tmp_args_name_6 = const_tuple_str_plain_proxy_port_tuple;
        tmp_kw_name_6 = PyDict_Copy( const_dict_a7b9314e26045b98fb4910dec4a6aa82 );
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 756;
        tmp_call_result_6 = CALL_FUNCTION( tmp_called_name_6, tmp_args_name_6, tmp_kw_name_6 );
        Py_DECREF( tmp_kw_name_6 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 756;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_7;
        CHECK_OBJECT( var_parse_command_line );
        tmp_called_name_7 = var_parse_command_line;
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 757;
        tmp_assign_source_5 = CALL_FUNCTION_NO_ARGS( tmp_called_name_7 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 757;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_args == NULL );
        var_args = tmp_assign_source_5;
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPClient );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPClient );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPClient" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 758;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_1;
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 758;
        tmp_assign_source_6 = CALL_FUNCTION_NO_ARGS( tmp_called_name_8 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 758;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( var_client == NULL );
        var_client = tmp_assign_source_6;
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_iter_arg_1;
        CHECK_OBJECT( var_args );
        tmp_iter_arg_1 = var_args;
        tmp_assign_source_7 = MAKE_ITERATOR( tmp_iter_arg_1 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 759;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_7;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_8;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_8 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_8 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oooooooo";
                exception_lineno = 759;
                goto try_except_handler_3;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_8;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_9 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_arg;
            var_arg = tmp_assign_source_9;
            Py_INCREF( var_arg );
            Py_XDECREF( old );
        }

    }
    // Tried code:
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_name_9;
        PyObject *tmp_source_name_1;
        PyObject *tmp_args_name_7;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_7;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( var_client );
        tmp_source_name_1 = var_client;
        tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_fetch );
        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 761;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        CHECK_OBJECT( var_arg );
        tmp_tuple_element_1 = var_arg;
        tmp_args_name_7 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_follow_redirects;
        CHECK_OBJECT( var_options );
        tmp_source_name_2 = var_options;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_follow_redirects );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_name_7 );

            exception_lineno = 763;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        tmp_kw_name_7 = _PyDict_NewPresized( 4 );
        tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_validate_cert;
        CHECK_OBJECT( var_options );
        tmp_source_name_3 = var_options;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_validate_cert );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_name_7 );
            Py_DECREF( tmp_kw_name_7 );

            exception_lineno = 764;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_3 = const_str_plain_proxy_host;
        CHECK_OBJECT( var_options );
        tmp_source_name_4 = var_options;
        tmp_dict_value_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_proxy_host );
        if ( tmp_dict_value_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_name_7 );
            Py_DECREF( tmp_kw_name_7 );

            exception_lineno = 765;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_3, tmp_dict_value_3 );
        Py_DECREF( tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_proxy_port;
        CHECK_OBJECT( var_options );
        tmp_source_name_5 = var_options;
        tmp_dict_value_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_proxy_port );
        if ( tmp_dict_value_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_name_7 );
            Py_DECREF( tmp_kw_name_7 );

            exception_lineno = 766;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_4, tmp_dict_value_4 );
        Py_DECREF( tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 761;
        tmp_assign_source_10 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_7, tmp_kw_name_7 );
        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_name_7 );
        Py_DECREF( tmp_kw_name_7 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 761;
            type_description_1 = "oooooooo";
            goto try_except_handler_4;
        }
        {
            PyObject *old = var_response;
            var_response = tmp_assign_source_10;
            Py_XDECREF( old );
        }

    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_2 == NULL )
    {
        exception_keeper_tb_2 = MAKE_TRACEBACK( frame_07edb31f42cc29ab8bbf8de92f57e171, exception_keeper_lineno_2 );
    }
    else if ( exception_keeper_lineno_2 != 0 )
    {
        exception_keeper_tb_2 = ADD_TRACEBACK( exception_keeper_tb_2, frame_07edb31f42cc29ab8bbf8de92f57e171, exception_keeper_lineno_2 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    PyException_SetTraceback( exception_keeper_value_2, (PyObject *)exception_keeper_tb_2 );
    PUBLISH_EXCEPTION( &exception_keeper_type_2, &exception_keeper_value_2, &exception_keeper_tb_2 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_mvar_value_2;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPError );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPError );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPError" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 768;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }

        tmp_compexpr_right_1 = tmp_mvar_value_2;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 768;
            type_description_1 = "oooooooo";
            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_11;
            tmp_assign_source_11 = EXC_VALUE(PyThreadState_GET());
            {
                PyObject *old = var_e;
                var_e = tmp_assign_source_11;
                Py_INCREF( var_e );
                Py_XDECREF( old );
            }

        }
        // Tried code:
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( var_e );
            tmp_source_name_6 = var_e;
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_response );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 769;
                type_description_1 = "oooooooo";
                goto try_except_handler_6;
            }
            tmp_compexpr_right_2 = Py_None;
            tmp_condition_result_2 = ( tmp_compexpr_left_2 != tmp_compexpr_right_2 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_12;
                PyObject *tmp_source_name_7;
                CHECK_OBJECT( var_e );
                tmp_source_name_7 = var_e;
                tmp_assign_source_12 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_response );
                if ( tmp_assign_source_12 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 770;
                    type_description_1 = "oooooooo";
                    goto try_except_handler_6;
                }
                {
                    PyObject *old = var_response;
                    var_response = tmp_assign_source_12;
                    Py_XDECREF( old );
                }

            }
            goto branch_end_2;
            branch_no_2:;
            tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            if (unlikely( tmp_result == false ))
            {
                exception_lineno = 772;
            }

            if (exception_tb && exception_tb->tb_frame == &frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame) frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = exception_tb->tb_lineno;
            type_description_1 = "oooooooo";
            goto try_except_handler_6;
            branch_end_2:;
        }
        goto try_end_3;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_3 = exception_type;
        exception_keeper_value_3 = exception_value;
        exception_keeper_tb_3 = exception_tb;
        exception_keeper_lineno_3 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_XDECREF( var_e );
        var_e = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_3;
        exception_value = exception_keeper_value_3;
        exception_tb = exception_keeper_tb_3;
        exception_lineno = exception_keeper_lineno_3;

        goto try_except_handler_5;
        // End of try:
        try_end_3:;
        Py_XDECREF( var_e );
        var_e = NULL;

        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 760;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame) frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "oooooooo";
        goto try_except_handler_5;
        branch_end_1:;
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto try_except_handler_3;
    // End of try:
    try_end_4:;
    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    goto try_end_2;
    // exception handler codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_27_main );
    return NULL;
    // End of try:
    try_end_2:;
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_source_name_8;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_options );
        tmp_source_name_8 = var_options;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_print_headers );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 773;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 773;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_3 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_3;
        }
        else
        {
            goto branch_no_3;
        }
        branch_yes_3:;
        {
            PyObject *tmp_called_name_10;
            PyObject *tmp_call_result_7;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_source_name_9;
            tmp_called_name_10 = LOOKUP_BUILTIN( const_str_plain_print );
            assert( tmp_called_name_10 != NULL );
            if ( var_response == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "response" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 774;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }

            tmp_source_name_9 = var_response;
            tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_headers );
            if ( tmp_args_element_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 774;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 774;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
            }

            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_call_result_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 774;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_7 );
        }
        branch_no_3:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_10;
        PyObject *tmp_attribute_value_2;
        int tmp_truth_name_2;
        CHECK_OBJECT( var_options );
        tmp_source_name_10 = var_options;
        tmp_attribute_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_print_body );
        if ( tmp_attribute_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 775;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_2 );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_2 );

            exception_lineno = 775;
            type_description_1 = "oooooooo";
            goto try_except_handler_3;
        }
        tmp_condition_result_4 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_2 );
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_4;
        }
        else
        {
            goto branch_no_4;
        }
        branch_yes_4:;
        {
            PyObject *tmp_called_name_11;
            PyObject *tmp_call_result_8;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_called_name_12;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_source_name_11;
            tmp_called_name_11 = LOOKUP_BUILTIN( const_str_plain_print );
            assert( tmp_called_name_11 != NULL );
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_native_str );

            if (unlikely( tmp_mvar_value_3 == NULL ))
            {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_native_str );
            }

            if ( tmp_mvar_value_3 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "native_str" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 776;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }

            tmp_called_name_12 = tmp_mvar_value_3;
            if ( var_response == NULL )
            {

                exception_type = PyExc_UnboundLocalError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "response" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 776;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }

            tmp_source_name_11 = var_response;
            tmp_args_element_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_body );
            if ( tmp_args_element_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 776;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 776;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
            }

            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_args_element_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 776;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 776;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
            }

            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_call_result_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 776;
                type_description_1 = "oooooooo";
                goto try_except_handler_3;
            }
            Py_DECREF( tmp_call_result_8 );
        }
        branch_no_4:;
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 759;
        type_description_1 = "oooooooo";
        goto try_except_handler_3;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_5;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_5 = exception_type;
    exception_keeper_value_5 = exception_value;
    exception_keeper_tb_5 = exception_tb;
    exception_keeper_lineno_5 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_5;
    exception_value = exception_keeper_value_5;
    exception_tb = exception_keeper_tb_5;
    exception_lineno = exception_keeper_lineno_5;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_9;
        CHECK_OBJECT( var_client );
        tmp_called_instance_1 = var_client;
        frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame.f_lineno = 777;
        tmp_call_result_9 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_close );
        if ( tmp_call_result_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 777;
            type_description_1 = "oooooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_9 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_07edb31f42cc29ab8bbf8de92f57e171 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_07edb31f42cc29ab8bbf8de92f57e171 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_07edb31f42cc29ab8bbf8de92f57e171, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_07edb31f42cc29ab8bbf8de92f57e171->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_07edb31f42cc29ab8bbf8de92f57e171, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_07edb31f42cc29ab8bbf8de92f57e171,
        type_description_1,
        var_define,
        var_options,
        var_parse_command_line,
        var_args,
        var_client,
        var_arg,
        var_response,
        var_e
    );


    // Release cached frame.
    if ( frame_07edb31f42cc29ab8bbf8de92f57e171 == cache_frame_07edb31f42cc29ab8bbf8de92f57e171 )
    {
        Py_DECREF( frame_07edb31f42cc29ab8bbf8de92f57e171 );
    }
    cache_frame_07edb31f42cc29ab8bbf8de92f57e171 = NULL;

    assertFrameObject( frame_07edb31f42cc29ab8bbf8de92f57e171 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_27_main );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_define );
    Py_DECREF( var_define );
    var_define = NULL;

    CHECK_OBJECT( (PyObject *)var_options );
    Py_DECREF( var_options );
    var_options = NULL;

    CHECK_OBJECT( (PyObject *)var_parse_command_line );
    Py_DECREF( var_parse_command_line );
    var_parse_command_line = NULL;

    CHECK_OBJECT( (PyObject *)var_args );
    Py_DECREF( var_args );
    var_args = NULL;

    CHECK_OBJECT( (PyObject *)var_client );
    Py_DECREF( var_client );
    var_client = NULL;

    Py_XDECREF( var_arg );
    var_arg = NULL;

    Py_XDECREF( var_response );
    var_response = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_6 = exception_type;
    exception_keeper_value_6 = exception_value;
    exception_keeper_tb_6 = exception_tb;
    exception_keeper_lineno_6 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_define );
    var_define = NULL;

    Py_XDECREF( var_options );
    var_options = NULL;

    Py_XDECREF( var_parse_command_line );
    var_parse_command_line = NULL;

    Py_XDECREF( var_args );
    var_args = NULL;

    Py_XDECREF( var_client );
    var_client = NULL;

    Py_XDECREF( var_arg );
    var_arg = NULL;

    Py_XDECREF( var_response );
    var_response = NULL;

    Py_XDECREF( var_e );
    var_e = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_6;
    exception_value = exception_keeper_value_6;
    exception_tb = exception_keeper_tb_6;
    exception_lineno = exception_keeper_lineno_6;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( tornado$httpclient$$$function_27_main );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_10_close( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_10_close,
        const_str_plain_close,
#if PYTHON_VERSION >= 300
        const_str_digest_afb184d09906a65fcfde6b3eefbb19a6,
#endif
        codeobj_9a7ec33ce2c27a7376ec35db69f3bda8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        const_str_digest_f09643320473256ce56cdb2dfb67dd6b,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_11_fetch( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_11_fetch,
        const_str_plain_fetch,
#if PYTHON_VERSION >= 300
        const_str_digest_3b7473fff9725cc62f429dad68799469,
#endif
        codeobj_d24ab2ae782140ba0106d4042d210003,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        const_str_digest_cdc2985a6310c422b25edf8035d6b5da,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_11_fetch$$$function_1_handle_response( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_11_fetch$$$function_1_handle_response,
        const_str_plain_handle_response,
#if PYTHON_VERSION >= 300
        const_str_digest_646129925d927a2e0f032ec5fd4d12ed,
#endif
        codeobj_b31cc5cce776a7d51eeb572d3309127c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_12_fetch_impl( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_12_fetch_impl,
        const_str_plain_fetch_impl,
#if PYTHON_VERSION >= 300
        const_str_digest_8b80da22e8fa2262b0edc0715696de6e,
#endif
        codeobj_18f23f5a7e6fe4e9cd0bf09e0959019a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_13_configure( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_13_configure,
        const_str_plain_configure,
#if PYTHON_VERSION >= 300
        const_str_digest_110520dd84acf1f2f0cc4300fbe8f98b,
#endif
        codeobj_d911a993b10fba49163895c1c4d188fa,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        const_str_digest_c83e5f43b5f0faf49ff813bd94426c79,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_14___init__( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_14___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_2637eb19e85ee47cbbc364b199fa7f11,
#endif
        codeobj_6e307838b588317efea4ea20564a9b52,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        const_str_digest_5cfef7f95e02358e66acc192938bb47e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_15_headers( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_15_headers,
        const_str_plain_headers,
#if PYTHON_VERSION >= 300
        const_str_digest_2a54b579adbe29e44c1193df08d0ca54,
#endif
        codeobj_18eaf614fcb3f563067213568d8d3711,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_16_headers( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_16_headers,
        const_str_plain_headers,
#if PYTHON_VERSION >= 300
        const_str_digest_2a54b579adbe29e44c1193df08d0ca54,
#endif
        codeobj_fb2562f3267c33570dc44f4893e3a2c4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_17_body( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_17_body,
        const_str_plain_body,
#if PYTHON_VERSION >= 300
        const_str_digest_3966c5d48e4aec5eaa9d4d76ce2f44a3,
#endif
        codeobj_3c36f9c232036606bbbd432d349ddfa8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_18_body( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_18_body,
        const_str_plain_body,
#if PYTHON_VERSION >= 300
        const_str_digest_3966c5d48e4aec5eaa9d4d76ce2f44a3,
#endif
        codeobj_71afdc46e7a96471c6239fb00dc242e8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_19___init__( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_19___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_d255455dca89984a7a880379cd01bd4d,
#endif
        codeobj_722efbae57c0bf39d22d7c04dffb6d9f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_1___init__( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_1___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_61e6d62d20308812ff9c0c9f78bcd75f,
#endif
        codeobj_cb175ae3e77fd1961b670de199c01c57,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_1___init__$$$function_1_make_client( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_1___init__$$$function_1_make_client,
        const_str_plain_make_client,
#if PYTHON_VERSION >= 300
        const_str_digest_2291376f8ac5f724b6b701a36ee441fd,
#endif
        codeobj_4d6b5db01563778c1d19c42ea72f07a8,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_20_body( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_20_body,
        const_str_plain_body,
#if PYTHON_VERSION >= 300
        const_str_digest_65b038522313564575aa5b2b15974f09,
#endif
        codeobj_d7e781f31b6acc7ad3976fc72cd431a0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_21_rethrow( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_21_rethrow,
        const_str_plain_rethrow,
#if PYTHON_VERSION >= 300
        const_str_digest_50b140d682c90fe96606564ea5dd571b,
#endif
        codeobj_0345d03b190573e043fea91e5df23663,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        const_str_digest_cc31a339455bd0327750e06c46ff8b3d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_22___repr__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_22___repr__,
        const_str_plain___repr__,
#if PYTHON_VERSION >= 300
        const_str_digest_c3fd672541fb689880c497d1199b14f3,
#endif
        codeobj_d4736e092cab086b98221122b05139d0,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_23___init__( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_23___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_dd5b9f91f366a2dcc7dd5058f4640e5c,
#endif
        codeobj_51b641b6a50b5ff292086f3f52f64be5,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_24___str__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_24___str__,
        const_str_plain___str__,
#if PYTHON_VERSION >= 300
        const_str_digest_4b00cdfdcd00aff27f294475d3d9c936,
#endif
        codeobj_6b3619961e2f6e053d8497a025234733,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_25___init__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_25___init__,
        const_str_plain___init__,
#if PYTHON_VERSION >= 300
        const_str_digest_7b5c18e05c25516935228d84f6d6dde5,
#endif
        codeobj_e82fccd137c01e8770baa23fc14bdbff,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_26___getattr__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_26___getattr__,
        const_str_plain___getattr__,
#if PYTHON_VERSION >= 300
        const_str_digest_a1f1d5c51f21741d345f323ecee64ae1,
#endif
        codeobj_0e20bad896a4422afeb795351bab726a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_27_main( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_27_main,
        const_str_plain_main,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_07edb31f42cc29ab8bbf8de92f57e171,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_2___del__( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_2___del__,
        const_str_plain___del__,
#if PYTHON_VERSION >= 300
        const_str_digest_57fde80fb420238e76979a36ff351987,
#endif
        codeobj_b60cd2325a77cff95cc166ab3d03ea88,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_3_close( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_3_close,
        const_str_plain_close,
#if PYTHON_VERSION >= 300
        const_str_digest_a5eb7baaff4fadb0397fae8dc1f75fcc,
#endif
        codeobj_d92002cbbc3b75ffa1988a8cdeb54d2d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        const_str_digest_1e8b8191e6ee572c2b9ae8835d41eb7f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_4_fetch( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_4_fetch,
        const_str_plain_fetch,
#if PYTHON_VERSION >= 300
        const_str_digest_e01eabb9dfbe5c0e49d4dcf1fbe88c15,
#endif
        codeobj_03b8157c632b8cf42f1a152fc9d1a1fb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        const_str_digest_58f29ba955cde6aab12dfbb0f02fc151,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_5_configurable_base( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_5_configurable_base,
        const_str_plain_configurable_base,
#if PYTHON_VERSION >= 300
        const_str_digest_c24969492806251a8bc7b7e0886afacf,
#endif
        codeobj_c24a659a5d00f17352a387a27bb0d03e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_6_configurable_default( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_6_configurable_default,
        const_str_plain_configurable_default,
#if PYTHON_VERSION >= 300
        const_str_digest_3f372af9fa5618895a31218b05ae3f4b,
#endif
        codeobj_ce5cfbe92344c9f4f7c47b27ed8bbe1a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_7__async_clients( PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_7__async_clients,
        const_str_plain__async_clients,
#if PYTHON_VERSION >= 300
        const_str_digest_0f8e13b4708f8a35e2e860117792b9fe,
#endif
        codeobj_56fcdc3ec0cc288b70e5c021245c863d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_8___new__( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_8___new__,
        const_str_plain___new__,
#if PYTHON_VERSION >= 300
        const_str_digest_2637714cd79b52e756b2393287778142,
#endif
        codeobj_79fbdd10bb9e61809342ea24b6a19499,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_tornado$httpclient$$$function_9_initialize( PyObject *defaults, PyObject *annotations )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_tornado$httpclient$$$function_9_initialize,
        const_str_plain_initialize,
#if PYTHON_VERSION >= 300
        const_str_digest_408e7d16e641d982e20ed3aec8e62210,
#endif
        codeobj_2f8fa415e313f31cf65e08fde0777d5f,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        annotations,
#endif
        module_tornado$httpclient,
        NULL,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_tornado$httpclient =
{
    PyModuleDef_HEAD_INIT,
    "tornado.httpclient",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(tornado$httpclient)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(tornado$httpclient)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_tornado$httpclient );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("tornado.httpclient: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("tornado.httpclient: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("tornado.httpclient: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in inittornado$httpclient" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_tornado$httpclient = Py_InitModule4(
        "tornado.httpclient",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_tornado$httpclient = PyModule_Create( &mdef_tornado$httpclient );
#endif

    moduledict_tornado$httpclient = MODULE_DICT( module_tornado$httpclient );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_tornado$httpclient,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_tornado$httpclient,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_tornado$httpclient,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_tornado$httpclient,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_tornado$httpclient );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_6f55736fcd57507504cc7f0643fbf21f, module_tornado$httpclient );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *outline_0_var___class__ = NULL;
    PyObject *outline_1_var___class__ = NULL;
    PyObject *outline_2_var___class__ = NULL;
    PyObject *outline_3_var___class__ = NULL;
    PyObject *outline_4_var___class__ = NULL;
    PyObject *outline_5_var___class__ = NULL;
    PyObject *tmp_class_creation_1__bases = NULL;
    PyObject *tmp_class_creation_1__class_decl_dict = NULL;
    PyObject *tmp_class_creation_1__metaclass = NULL;
    PyObject *tmp_class_creation_1__prepared = NULL;
    PyObject *tmp_class_creation_2__bases = NULL;
    PyObject *tmp_class_creation_2__bases_orig = NULL;
    PyObject *tmp_class_creation_2__class_decl_dict = NULL;
    PyObject *tmp_class_creation_2__metaclass = NULL;
    PyObject *tmp_class_creation_2__prepared = NULL;
    PyObject *tmp_class_creation_3__bases = NULL;
    PyObject *tmp_class_creation_3__class_decl_dict = NULL;
    PyObject *tmp_class_creation_3__metaclass = NULL;
    PyObject *tmp_class_creation_3__prepared = NULL;
    PyObject *tmp_class_creation_4__bases = NULL;
    PyObject *tmp_class_creation_4__class_decl_dict = NULL;
    PyObject *tmp_class_creation_4__metaclass = NULL;
    PyObject *tmp_class_creation_4__prepared = NULL;
    PyObject *tmp_class_creation_5__bases = NULL;
    PyObject *tmp_class_creation_5__class_decl_dict = NULL;
    PyObject *tmp_class_creation_5__metaclass = NULL;
    PyObject *tmp_class_creation_5__prepared = NULL;
    PyObject *tmp_class_creation_6__bases = NULL;
    PyObject *tmp_class_creation_6__class_decl_dict = NULL;
    PyObject *tmp_class_creation_6__metaclass = NULL;
    PyObject *tmp_class_creation_6__prepared = NULL;
    PyObject *tmp_import_from_1__module = NULL;
    PyObject *tmp_import_from_2__module = NULL;
    PyObject *tmp_import_from_3__module = NULL;
    PyObject *tmp_import_from_4__module = NULL;
    struct Nuitka_FrameObject *frame_e51e71200c4d41bba590fb0c4f6851d0;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;
    PyObject *exception_keeper_type_4;
    PyObject *exception_keeper_value_4;
    PyTracebackObject *exception_keeper_tb_4;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_4;
    int tmp_res;
    PyObject *tmp_dictdel_dict;
    PyObject *tmp_dictdel_key;
    PyObject *locals_tornado$httpclient_59 = NULL;
    PyObject *tmp_dictset_value;
    struct Nuitka_FrameObject *frame_63d8f7595cf2a14b5cdc744a245bc71e_2;
    NUITKA_MAY_BE_UNUSED char const *type_description_2 = NULL;
    static struct Nuitka_FrameObject *cache_frame_63d8f7595cf2a14b5cdc744a245bc71e_2 = NULL;
    PyObject *exception_keeper_type_5;
    PyObject *exception_keeper_value_5;
    PyTracebackObject *exception_keeper_tb_5;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_5;
    PyObject *exception_keeper_type_6;
    PyObject *exception_keeper_value_6;
    PyTracebackObject *exception_keeper_tb_6;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_6;
    PyObject *exception_keeper_type_7;
    PyObject *exception_keeper_value_7;
    PyTracebackObject *exception_keeper_tb_7;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_7;
    PyObject *locals_tornado$httpclient_138 = NULL;
    struct Nuitka_FrameObject *frame_5ab4f6dbba8ac4d4a89799531de9c715_3;
    NUITKA_MAY_BE_UNUSED char const *type_description_3 = NULL;
    static struct Nuitka_FrameObject *cache_frame_5ab4f6dbba8ac4d4a89799531de9c715_3 = NULL;
    PyObject *exception_keeper_type_8;
    PyObject *exception_keeper_value_8;
    PyTracebackObject *exception_keeper_tb_8;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_8;
    PyObject *exception_keeper_type_9;
    PyObject *exception_keeper_value_9;
    PyTracebackObject *exception_keeper_tb_9;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_9;
    PyObject *exception_keeper_type_10;
    PyObject *exception_keeper_value_10;
    PyTracebackObject *exception_keeper_tb_10;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_10;
    PyObject *locals_tornado$httpclient_337 = NULL;
    struct Nuitka_FrameObject *frame_3c585940a00c16cc3fd3083c84c8ac0a_4;
    NUITKA_MAY_BE_UNUSED char const *type_description_4 = NULL;
    static struct Nuitka_FrameObject *cache_frame_3c585940a00c16cc3fd3083c84c8ac0a_4 = NULL;
    PyObject *exception_keeper_type_11;
    PyObject *exception_keeper_value_11;
    PyTracebackObject *exception_keeper_tb_11;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_11;
    PyObject *exception_keeper_type_12;
    PyObject *exception_keeper_value_12;
    PyTracebackObject *exception_keeper_tb_12;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_12;
    PyObject *exception_keeper_type_13;
    PyObject *exception_keeper_value_13;
    PyTracebackObject *exception_keeper_tb_13;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_13;
    PyObject *locals_tornado$httpclient_568 = NULL;
    struct Nuitka_FrameObject *frame_ed5be14316d8a2e2facc323ac7ebc012_5;
    NUITKA_MAY_BE_UNUSED char const *type_description_5 = NULL;
    static struct Nuitka_FrameObject *cache_frame_ed5be14316d8a2e2facc323ac7ebc012_5 = NULL;
    PyObject *exception_keeper_type_14;
    PyObject *exception_keeper_value_14;
    PyTracebackObject *exception_keeper_tb_14;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_14;
    PyObject *exception_keeper_type_15;
    PyObject *exception_keeper_value_15;
    PyTracebackObject *exception_keeper_tb_15;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_15;
    PyObject *exception_keeper_type_16;
    PyObject *exception_keeper_value_16;
    PyTracebackObject *exception_keeper_tb_16;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_16;
    PyObject *locals_tornado$httpclient_684 = NULL;
    struct Nuitka_FrameObject *frame_8653d73da3de6cf1359d78767acabc70_6;
    NUITKA_MAY_BE_UNUSED char const *type_description_6 = NULL;
    static struct Nuitka_FrameObject *cache_frame_8653d73da3de6cf1359d78767acabc70_6 = NULL;
    PyObject *exception_keeper_type_17;
    PyObject *exception_keeper_value_17;
    PyTracebackObject *exception_keeper_tb_17;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_17;
    PyObject *exception_keeper_type_18;
    PyObject *exception_keeper_value_18;
    PyTracebackObject *exception_keeper_tb_18;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_18;
    PyObject *exception_keeper_type_19;
    PyObject *exception_keeper_value_19;
    PyTracebackObject *exception_keeper_tb_19;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_19;
    PyObject *locals_tornado$httpclient_726 = NULL;
    struct Nuitka_FrameObject *frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7;
    NUITKA_MAY_BE_UNUSED char const *type_description_7 = NULL;
    static struct Nuitka_FrameObject *cache_frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 = NULL;
    PyObject *exception_keeper_type_20;
    PyObject *exception_keeper_value_20;
    PyTracebackObject *exception_keeper_tb_20;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_20;
    PyObject *exception_keeper_type_21;
    PyObject *exception_keeper_value_21;
    PyTracebackObject *exception_keeper_tb_21;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_21;
    PyObject *exception_keeper_type_22;
    PyObject *exception_keeper_value_22;
    PyTracebackObject *exception_keeper_tb_22;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_22;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_b7d04337ffd07131655e2e21c568a5d6;
        UPDATE_STRING_DICT0( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_e51e71200c4d41bba590fb0c4f6851d0 = MAKE_MODULE_FRAME( codeobj_e51e71200c4d41bba590fb0c4f6851d0, module_tornado$httpclient );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_e51e71200c4d41bba590fb0c4f6851d0 );
    assert( Py_REFCNT( frame_e51e71200c4d41bba590fb0c4f6851d0 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_plain_datetime;
        tmp_globals_name_1 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = Py_None;
        tmp_level_name_1 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 39;
        tmp_assign_source_4 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 39;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_datetime, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_functools;
        tmp_globals_name_2 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 40;
        tmp_assign_source_5 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 40;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_functools, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_io;
        tmp_globals_name_3 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_BytesIO_tuple;
        tmp_level_name_3 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 41;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_6 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_BytesIO );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 41;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_BytesIO, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_plain_ssl;
        tmp_globals_name_4 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = Py_None;
        tmp_level_name_4 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 42;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 42;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_ssl, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_time;
        tmp_globals_name_5 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = Py_None;
        tmp_level_name_5 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 43;
        tmp_assign_source_8 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        assert( !(tmp_assign_source_8 == NULL) );
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_time, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_weakref;
        tmp_globals_name_6 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = Py_None;
        tmp_level_name_6 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 44;
        tmp_assign_source_9 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 44;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_weakref, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_9b98666360eab9bd5e6093808f6c2588;
        tmp_globals_name_7 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_c20e76db2359ca9d4cb706a65d727a61_tuple;
        tmp_level_name_7 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 46;
        tmp_assign_source_10 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_10;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_2;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_2 = tmp_import_from_1__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_Future );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Future, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_future_set_result_unless_cancelled );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_future_set_result_unless_cancelled, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_future_set_exception_unless_cancelled );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_future_set_exception_unless_cancelled, tmp_assign_source_13 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_84a3f2fedb1ec7918aa30203da68a052;
        tmp_globals_name_8 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_utf8_str_plain_native_str_tuple;
        tmp_level_name_8 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 51;
        tmp_assign_source_14 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_2__module == NULL );
        tmp_import_from_2__module = tmp_assign_source_14;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_5 = tmp_import_from_2__module;
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_utf8 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_utf8, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_2__module );
        tmp_import_name_from_6 = tmp_import_from_2__module;
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_native_str );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 51;

            goto try_except_handler_2;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_native_str, tmp_assign_source_16 );
    }
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_2__module );
    Py_DECREF( tmp_import_from_2__module );
    tmp_import_from_2__module = NULL;

    {
        PyObject *tmp_assign_source_17;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_plain_tornado;
        tmp_globals_name_9 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_gen_str_plain_httputil_tuple;
        tmp_level_name_9 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 52;
        tmp_assign_source_17 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_assign_source_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_3__module == NULL );
        tmp_import_from_3__module = tmp_assign_source_17;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_18;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_7 = tmp_import_from_3__module;
        tmp_assign_source_18 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_gen );
        if ( tmp_assign_source_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_gen, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_3__module );
        tmp_import_name_from_8 = tmp_import_from_3__module;
        tmp_assign_source_19 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_httputil );
        if ( tmp_assign_source_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 52;

            goto try_except_handler_3;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil, tmp_assign_source_19 );
    }
    goto try_end_3;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto frame_exception_exit_1;
    // End of try:
    try_end_3:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_3__module );
    Py_DECREF( tmp_import_from_3__module );
    tmp_import_from_3__module = NULL;

    {
        PyObject *tmp_assign_source_20;
        PyObject *tmp_import_name_from_9;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_digest_66ef772b0f62991fd20497da33b24b9b;
        tmp_globals_name_10 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_IOLoop_tuple;
        tmp_level_name_10 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 53;
        tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_import_name_from_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_20 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_IOLoop );
        Py_DECREF( tmp_import_name_from_9 );
        if ( tmp_assign_source_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 53;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_IOLoop, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_import_name_from_10;
        PyObject *tmp_name_name_11;
        PyObject *tmp_globals_name_11;
        PyObject *tmp_locals_name_11;
        PyObject *tmp_fromlist_name_11;
        PyObject *tmp_level_name_11;
        tmp_name_name_11 = const_str_digest_e077944e15accbec54ecb40fd81dafde;
        tmp_globals_name_11 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_11 = Py_None;
        tmp_fromlist_name_11 = const_tuple_str_plain_Configurable_tuple;
        tmp_level_name_11 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 54;
        tmp_import_name_from_10 = IMPORT_MODULE5( tmp_name_name_11, tmp_globals_name_11, tmp_locals_name_11, tmp_fromlist_name_11, tmp_level_name_11 );
        if ( tmp_import_name_from_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_21 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_Configurable );
        Py_DECREF( tmp_import_name_from_10 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 54;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Configurable, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_name_name_12;
        PyObject *tmp_globals_name_12;
        PyObject *tmp_locals_name_12;
        PyObject *tmp_fromlist_name_12;
        PyObject *tmp_level_name_12;
        tmp_name_name_12 = const_str_plain_typing;
        tmp_globals_name_12 = (PyObject *)moduledict_tornado$httpclient;
        tmp_locals_name_12 = Py_None;
        tmp_fromlist_name_12 = const_tuple_2f2328bd4d26dc97ba02335c881302e1_tuple;
        tmp_level_name_12 = const_int_0;
        frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 56;
        tmp_assign_source_22 = IMPORT_MODULE5( tmp_name_name_12, tmp_globals_name_12, tmp_locals_name_12, tmp_fromlist_name_12, tmp_level_name_12 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_4__module == NULL );
        tmp_import_from_4__module = tmp_assign_source_22;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_import_name_from_11;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_11 = tmp_import_from_4__module;
        tmp_assign_source_23 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_Type );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Type, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_import_name_from_12;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_12 = tmp_import_from_4__module;
        tmp_assign_source_24 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_Any );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_import_name_from_13;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_13 = tmp_import_from_4__module;
        tmp_assign_source_25 = IMPORT_NAME( tmp_import_name_from_13, const_str_plain_Union );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Union, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_import_name_from_14;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_14 = tmp_import_from_4__module;
        tmp_assign_source_26 = IMPORT_NAME( tmp_import_name_from_14, const_str_plain_Dict );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Dict, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_import_name_from_15;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_15 = tmp_import_from_4__module;
        tmp_assign_source_27 = IMPORT_NAME( tmp_import_name_from_15, const_str_plain_Callable );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Callable, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_import_name_from_16;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_16 = tmp_import_from_4__module;
        tmp_assign_source_28 = IMPORT_NAME( tmp_import_name_from_16, const_str_plain_Optional );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Optional, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_import_name_from_17;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_17 = tmp_import_from_4__module;
        tmp_assign_source_29 = IMPORT_NAME( tmp_import_name_from_17, const_str_plain_cast );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_cast, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_import_name_from_18;
        CHECK_OBJECT( tmp_import_from_4__module );
        tmp_import_name_from_18 = tmp_import_from_4__module;
        tmp_assign_source_30 = IMPORT_NAME( tmp_import_name_from_18, const_str_plain_Awaitable );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto try_except_handler_4;
        }
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Awaitable, tmp_assign_source_30 );
    }
    goto try_end_4;
    // Exception handler code:
    try_except_handler_4:;
    exception_keeper_type_4 = exception_type;
    exception_keeper_value_4 = exception_value;
    exception_keeper_tb_4 = exception_tb;
    exception_keeper_lineno_4 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_4;
    exception_value = exception_keeper_value_4;
    exception_tb = exception_keeper_tb_4;
    exception_lineno = exception_keeper_lineno_4;

    goto frame_exception_exit_1;
    // End of try:
    try_end_4:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_4__module );
    Py_DECREF( tmp_import_from_4__module );
    tmp_import_from_4__module = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_dircall_arg1_1;
        tmp_dircall_arg1_1 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_1 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_1};
            tmp_assign_source_31 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_1__bases == NULL );
        tmp_class_creation_1__bases = tmp_assign_source_31;
    }
    {
        PyObject *tmp_assign_source_32;
        tmp_assign_source_32 = PyDict_New();
        assert( tmp_class_creation_1__class_decl_dict == NULL );
        tmp_class_creation_1__class_decl_dict = tmp_assign_source_32;
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_metaclass_name_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_key_name_1;
        PyObject *tmp_dict_name_1;
        PyObject *tmp_dict_name_2;
        PyObject *tmp_key_name_2;
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        PyObject *tmp_type_arg_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_bases_name_1;
        tmp_key_name_1 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_1 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_1, tmp_key_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_5;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_2 = tmp_class_creation_1__class_decl_dict;
        tmp_key_name_2 = const_str_plain_metaclass;
        tmp_metaclass_name_1 = DICT_GET_ITEM( tmp_dict_name_2, tmp_key_name_2 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_5;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_class_creation_1__bases );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_5;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_2;
        }
        else
        {
            goto condexpr_false_2;
        }
        condexpr_true_2:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_subscribed_name_1 = tmp_class_creation_1__bases;
        tmp_subscript_name_1 = const_int_0;
        tmp_type_arg_1 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_1, tmp_subscript_name_1, 0 );
        if ( tmp_type_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_5;
        }
        tmp_metaclass_name_1 = BUILTIN_TYPE1( tmp_type_arg_1 );
        Py_DECREF( tmp_type_arg_1 );
        if ( tmp_metaclass_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_5;
        }
        goto condexpr_end_2;
        condexpr_false_2:;
        tmp_metaclass_name_1 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_1 );
        condexpr_end_2:;
        condexpr_end_1:;
        CHECK_OBJECT( tmp_class_creation_1__bases );
        tmp_bases_name_1 = tmp_class_creation_1__bases;
        tmp_assign_source_33 = SELECT_METACLASS( tmp_metaclass_name_1, tmp_bases_name_1 );
        Py_DECREF( tmp_metaclass_name_1 );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_5;
        }
        assert( tmp_class_creation_1__metaclass == NULL );
        tmp_class_creation_1__metaclass = tmp_assign_source_33;
    }
    {
        nuitka_bool tmp_condition_result_3;
        PyObject *tmp_key_name_3;
        PyObject *tmp_dict_name_3;
        tmp_key_name_3 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dict_name_3 = tmp_class_creation_1__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_3, tmp_key_name_3 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_5;
        }
        tmp_condition_result_3 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_1__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_5;
        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_4;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( tmp_class_creation_1__metaclass );
        tmp_source_name_1 = tmp_class_creation_1__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_1, const_str_plain___prepare__ );
        tmp_condition_result_4 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_4 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_34;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_source_name_2 = tmp_class_creation_1__metaclass;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain___prepare__ );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;

                goto try_except_handler_5;
            }
            tmp_tuple_element_1 = const_str_plain_HTTPClient;
            tmp_args_name_1 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_1 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 1, tmp_tuple_element_1 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_1 = tmp_class_creation_1__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 59;
            tmp_assign_source_34 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            if ( tmp_assign_source_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;

                goto try_except_handler_5;
            }
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_34;
        }
        {
            nuitka_bool tmp_condition_result_5;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_3;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_source_name_3 = tmp_class_creation_1__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_3, const_str_plain___getitem__ );
            tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;

                goto try_except_handler_5;
            }
            tmp_condition_result_5 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_5 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_raise_type_1;
                PyObject *tmp_raise_value_1;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_tuple_element_2;
                PyObject *tmp_getattr_target_1;
                PyObject *tmp_getattr_attr_1;
                PyObject *tmp_getattr_default_1;
                PyObject *tmp_source_name_4;
                PyObject *tmp_type_arg_2;
                tmp_raise_type_1 = PyExc_TypeError;
                tmp_left_name_1 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_1__metaclass );
                tmp_getattr_target_1 = tmp_class_creation_1__metaclass;
                tmp_getattr_attr_1 = const_str_plain___name__;
                tmp_getattr_default_1 = const_str_angle_metaclass;
                tmp_tuple_element_2 = BUILTIN_GETATTR( tmp_getattr_target_1, tmp_getattr_attr_1, tmp_getattr_default_1 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 59;

                    goto try_except_handler_5;
                }
                tmp_right_name_1 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_1, 0, tmp_tuple_element_2 );
                CHECK_OBJECT( tmp_class_creation_1__prepared );
                tmp_type_arg_2 = tmp_class_creation_1__prepared;
                tmp_source_name_4 = BUILTIN_TYPE1( tmp_type_arg_2 );
                assert( !(tmp_source_name_4 == NULL) );
                tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_4 );
                if ( tmp_tuple_element_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_1 );

                    exception_lineno = 59;

                    goto try_except_handler_5;
                }
                PyTuple_SET_ITEM( tmp_right_name_1, 1, tmp_tuple_element_2 );
                tmp_raise_value_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_raise_value_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 59;

                    goto try_except_handler_5;
                }
                exception_type = tmp_raise_type_1;
                Py_INCREF( tmp_raise_type_1 );
                exception_value = tmp_raise_value_1;
                exception_lineno = 59;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_5;
            }
            branch_no_3:;
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_assign_source_35;
            tmp_assign_source_35 = PyDict_New();
            assert( tmp_class_creation_1__prepared == NULL );
            tmp_class_creation_1__prepared = tmp_assign_source_35;
        }
        branch_end_2:;
    }
    {
        PyObject *tmp_assign_source_36;
        {
            PyObject *tmp_set_locals_1;
            CHECK_OBJECT( tmp_class_creation_1__prepared );
            tmp_set_locals_1 = tmp_class_creation_1__prepared;
            locals_tornado$httpclient_59 = tmp_set_locals_1;
            Py_INCREF( tmp_set_locals_1 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_6f55736fcd57507504cc7f0643fbf21f;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_59, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_digest_779a6df8027b904e318e30a6afe10e8b;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_59, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_7;
        }
        tmp_dictset_value = const_str_plain_HTTPClient;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_59, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;

            goto try_except_handler_7;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_63d8f7595cf2a14b5cdc744a245bc71e_2, codeobj_63d8f7595cf2a14b5cdc744a245bc71e, module_tornado$httpclient, sizeof(void *) );
        frame_63d8f7595cf2a14b5cdc744a245bc71e_2 = cache_frame_63d8f7595cf2a14b5cdc744a245bc71e_2;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_63d8f7595cf2a14b5cdc744a245bc71e_2 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_63d8f7595cf2a14b5cdc744a245bc71e_2 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_defaults_1;
            PyObject *tmp_annotations_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_subscribed_name_2;
            PyObject *tmp_mvar_value_3;
            PyObject *tmp_subscript_name_2;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_mvar_value_4;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            tmp_defaults_1 = const_tuple_none_tuple;
            tmp_dict_key_1 = const_str_plain_async_client_class;
            tmp_subscribed_name_2 = PyObject_GetItem( locals_tornado$httpclient_59, const_str_plain_Type );

            if ( tmp_subscribed_name_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Type );

                if (unlikely( tmp_mvar_value_3 == NULL ))
                {
                    tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Type );
                }

                if ( tmp_mvar_value_3 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Type" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 90;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_2 = tmp_mvar_value_3;
                Py_INCREF( tmp_subscribed_name_2 );
                }
            }

            tmp_subscript_name_2 = const_str_plain_AsyncHTTPClient;
            tmp_dict_value_1 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_2, tmp_subscript_name_2 );
            Py_DECREF( tmp_subscribed_name_2 );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 90;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_annotations_1 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_2 = const_str_plain_kwargs;
            tmp_dict_value_2 = PyObject_GetItem( locals_tornado$httpclient_59, const_str_plain_Any );

            if ( tmp_dict_value_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_4 == NULL ))
                {
                    tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_4 == NULL )
                {
                    Py_DECREF( tmp_annotations_1 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 90;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_dict_value_2 = tmp_mvar_value_4;
                Py_INCREF( tmp_dict_value_2 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_3 = const_str_plain_return;
            tmp_dict_value_3 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_1, tmp_dict_key_3, tmp_dict_value_3 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_1 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_1___init__( tmp_defaults_1, tmp_annotations_1 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_59, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 89;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_2;
            tmp_annotations_2 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_2___del__( tmp_annotations_2 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_59, const_str_plain___del__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 110;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_3;
            tmp_annotations_3 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_3_close( tmp_annotations_3 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_59, const_str_plain_close, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 113;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }
        {
            PyObject *tmp_annotations_4;
            PyObject *tmp_dict_key_4;
            PyObject *tmp_dict_value_4;
            PyObject *tmp_subscribed_name_3;
            PyObject *tmp_mvar_value_5;
            PyObject *tmp_subscript_name_3;
            PyObject *tmp_tuple_element_3;
            PyObject *tmp_dict_key_5;
            PyObject *tmp_dict_value_5;
            PyObject *tmp_mvar_value_6;
            PyObject *tmp_dict_key_6;
            PyObject *tmp_dict_value_6;
            tmp_dict_key_4 = const_str_plain_request;
            tmp_subscribed_name_3 = PyObject_GetItem( locals_tornado$httpclient_59, const_str_plain_Union );

            if ( tmp_subscribed_name_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_5 == NULL ))
                {
                    tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_5 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 121;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_subscribed_name_3 = tmp_mvar_value_5;
                Py_INCREF( tmp_subscribed_name_3 );
                }
            }

            tmp_tuple_element_3 = const_str_plain_HTTPRequest;
            tmp_subscript_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_3 );
            PyTuple_SET_ITEM( tmp_subscript_name_3, 0, tmp_tuple_element_3 );
            tmp_tuple_element_3 = PyObject_GetItem( locals_tornado$httpclient_59, const_str_plain_str );

            if ( tmp_tuple_element_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_3 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_3 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_3, 1, tmp_tuple_element_3 );
            tmp_dict_value_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_3, tmp_subscript_name_3 );
            Py_DECREF( tmp_subscribed_name_3 );
            Py_DECREF( tmp_subscript_name_3 );
            if ( tmp_dict_value_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 121;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
            tmp_annotations_4 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_4, tmp_dict_value_4 );
            Py_DECREF( tmp_dict_value_4 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_5 = const_str_plain_kwargs;
            tmp_dict_value_5 = PyObject_GetItem( locals_tornado$httpclient_59, const_str_plain_Any );

            if ( tmp_dict_value_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_6 == NULL ))
                {
                    tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_6 == NULL )
                {
                    Py_DECREF( tmp_annotations_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 121;
                    type_description_2 = "o";
                    goto frame_exception_exit_2;
                }

                tmp_dict_value_5 = tmp_mvar_value_6;
                Py_INCREF( tmp_dict_value_5 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_5, tmp_dict_value_5 );
            Py_DECREF( tmp_dict_value_5 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_6 = const_str_plain_return;
            tmp_dict_value_6 = const_str_plain_HTTPResponse;
            tmp_res = PyDict_SetItem( tmp_annotations_4, tmp_dict_key_6, tmp_dict_value_6 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_4_fetch( tmp_annotations_4 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_59, const_str_plain_fetch, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 120;
                type_description_2 = "o";
                goto frame_exception_exit_2;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_63d8f7595cf2a14b5cdc744a245bc71e_2 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_1;

        frame_exception_exit_2:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_63d8f7595cf2a14b5cdc744a245bc71e_2 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_63d8f7595cf2a14b5cdc744a245bc71e_2, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_63d8f7595cf2a14b5cdc744a245bc71e_2->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_63d8f7595cf2a14b5cdc744a245bc71e_2, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_63d8f7595cf2a14b5cdc744a245bc71e_2,
            type_description_2,
            outline_0_var___class__
        );


        // Release cached frame.
        if ( frame_63d8f7595cf2a14b5cdc744a245bc71e_2 == cache_frame_63d8f7595cf2a14b5cdc744a245bc71e_2 )
        {
            Py_DECREF( frame_63d8f7595cf2a14b5cdc744a245bc71e_2 );
        }
        cache_frame_63d8f7595cf2a14b5cdc744a245bc71e_2 = NULL;

        assertFrameObject( frame_63d8f7595cf2a14b5cdc744a245bc71e_2 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_1;

        frame_no_exception_1:;
        goto skip_nested_handling_1;
        nested_frame_exit_1:;

        goto try_except_handler_7;
        skip_nested_handling_1:;
        {
            nuitka_bool tmp_condition_result_6;
            PyObject *tmp_compexpr_left_1;
            PyObject *tmp_compexpr_right_1;
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_compexpr_left_1 = tmp_class_creation_1__bases;
            tmp_compexpr_right_1 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;

                goto try_except_handler_7;
            }
            tmp_condition_result_6 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_6 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_4;
            }
            else
            {
                goto branch_no_4;
            }
            branch_yes_4:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_59, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;

                goto try_except_handler_7;
            }
            branch_no_4:;
        }
        {
            PyObject *tmp_assign_source_37;
            PyObject *tmp_called_name_2;
            PyObject *tmp_args_name_2;
            PyObject *tmp_tuple_element_4;
            PyObject *tmp_kw_name_2;
            CHECK_OBJECT( tmp_class_creation_1__metaclass );
            tmp_called_name_2 = tmp_class_creation_1__metaclass;
            tmp_tuple_element_4 = const_str_plain_HTTPClient;
            tmp_args_name_2 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 0, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__bases );
            tmp_tuple_element_4 = tmp_class_creation_1__bases;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 1, tmp_tuple_element_4 );
            tmp_tuple_element_4 = locals_tornado$httpclient_59;
            Py_INCREF( tmp_tuple_element_4 );
            PyTuple_SET_ITEM( tmp_args_name_2, 2, tmp_tuple_element_4 );
            CHECK_OBJECT( tmp_class_creation_1__class_decl_dict );
            tmp_kw_name_2 = tmp_class_creation_1__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 59;
            tmp_assign_source_37 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_args_name_2 );
            if ( tmp_assign_source_37 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 59;

                goto try_except_handler_7;
            }
            assert( outline_0_var___class__ == NULL );
            outline_0_var___class__ = tmp_assign_source_37;
        }
        CHECK_OBJECT( outline_0_var___class__ );
        tmp_assign_source_36 = outline_0_var___class__;
        Py_INCREF( tmp_assign_source_36 );
        goto try_return_handler_7;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_7:;
        Py_DECREF( locals_tornado$httpclient_59 );
        locals_tornado$httpclient_59 = NULL;
        goto try_return_handler_6;
        // Exception handler code:
        try_except_handler_7:;
        exception_keeper_type_5 = exception_type;
        exception_keeper_value_5 = exception_value;
        exception_keeper_tb_5 = exception_tb;
        exception_keeper_lineno_5 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$httpclient_59 );
        locals_tornado$httpclient_59 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_5;
        exception_value = exception_keeper_value_5;
        exception_tb = exception_keeper_tb_5;
        exception_lineno = exception_keeper_lineno_5;

        goto try_except_handler_6;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_6:;
        CHECK_OBJECT( (PyObject *)outline_0_var___class__ );
        Py_DECREF( outline_0_var___class__ );
        outline_0_var___class__ = NULL;

        goto outline_result_1;
        // Exception handler code:
        try_except_handler_6:;
        exception_keeper_type_6 = exception_type;
        exception_keeper_value_6 = exception_value;
        exception_keeper_tb_6 = exception_tb;
        exception_keeper_lineno_6 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_6;
        exception_value = exception_keeper_value_6;
        exception_tb = exception_keeper_tb_6;
        exception_lineno = exception_keeper_lineno_6;

        goto outline_exception_1;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_1:;
        exception_lineno = 59;
        goto try_except_handler_5;
        outline_result_1:;
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPClient, tmp_assign_source_36 );
    }
    goto try_end_5;
    // Exception handler code:
    try_except_handler_5:;
    exception_keeper_type_7 = exception_type;
    exception_keeper_value_7 = exception_value;
    exception_keeper_tb_7 = exception_tb;
    exception_keeper_lineno_7 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    Py_XDECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_7;
    exception_value = exception_keeper_value_7;
    exception_tb = exception_keeper_tb_7;
    exception_lineno = exception_keeper_lineno_7;

    goto frame_exception_exit_1;
    // End of try:
    try_end_5:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__bases );
    Py_DECREF( tmp_class_creation_1__bases );
    tmp_class_creation_1__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__class_decl_dict );
    Py_DECREF( tmp_class_creation_1__class_decl_dict );
    tmp_class_creation_1__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__metaclass );
    Py_DECREF( tmp_class_creation_1__metaclass );
    tmp_class_creation_1__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_1__prepared );
    Py_DECREF( tmp_class_creation_1__prepared );
    tmp_class_creation_1__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_tuple_element_5;
        PyObject *tmp_mvar_value_7;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Configurable );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Configurable );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Configurable" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;

            goto try_except_handler_8;
        }

        tmp_tuple_element_5 = tmp_mvar_value_7;
        tmp_assign_source_38 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_5 );
        PyTuple_SET_ITEM( tmp_assign_source_38, 0, tmp_tuple_element_5 );
        assert( tmp_class_creation_2__bases_orig == NULL );
        tmp_class_creation_2__bases_orig = tmp_assign_source_38;
    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_dircall_arg1_2;
        CHECK_OBJECT( tmp_class_creation_2__bases_orig );
        tmp_dircall_arg1_2 = tmp_class_creation_2__bases_orig;
        Py_INCREF( tmp_dircall_arg1_2 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_2};
            tmp_assign_source_39 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_8;
        }
        assert( tmp_class_creation_2__bases == NULL );
        tmp_class_creation_2__bases = tmp_assign_source_39;
    }
    {
        PyObject *tmp_assign_source_40;
        tmp_assign_source_40 = PyDict_New();
        assert( tmp_class_creation_2__class_decl_dict == NULL );
        tmp_class_creation_2__class_decl_dict = tmp_assign_source_40;
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_metaclass_name_2;
        nuitka_bool tmp_condition_result_7;
        PyObject *tmp_key_name_4;
        PyObject *tmp_dict_name_4;
        PyObject *tmp_dict_name_5;
        PyObject *tmp_key_name_5;
        nuitka_bool tmp_condition_result_8;
        int tmp_truth_name_2;
        PyObject *tmp_type_arg_3;
        PyObject *tmp_subscribed_name_4;
        PyObject *tmp_subscript_name_4;
        PyObject *tmp_bases_name_2;
        tmp_key_name_4 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_4 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_4, tmp_key_name_4 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_8;
        }
        tmp_condition_result_7 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_7 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_3;
        }
        else
        {
            goto condexpr_false_3;
        }
        condexpr_true_3:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_5 = tmp_class_creation_2__class_decl_dict;
        tmp_key_name_5 = const_str_plain_metaclass;
        tmp_metaclass_name_2 = DICT_GET_ITEM( tmp_dict_name_5, tmp_key_name_5 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_8;
        }
        goto condexpr_end_3;
        condexpr_false_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_truth_name_2 = CHECK_IF_TRUE( tmp_class_creation_2__bases );
        if ( tmp_truth_name_2 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_8;
        }
        tmp_condition_result_8 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_8 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_4;
        }
        else
        {
            goto condexpr_false_4;
        }
        condexpr_true_4:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_subscribed_name_4 = tmp_class_creation_2__bases;
        tmp_subscript_name_4 = const_int_0;
        tmp_type_arg_3 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_4, tmp_subscript_name_4, 0 );
        if ( tmp_type_arg_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_8;
        }
        tmp_metaclass_name_2 = BUILTIN_TYPE1( tmp_type_arg_3 );
        Py_DECREF( tmp_type_arg_3 );
        if ( tmp_metaclass_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_8;
        }
        goto condexpr_end_4;
        condexpr_false_4:;
        tmp_metaclass_name_2 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_2 );
        condexpr_end_4:;
        condexpr_end_3:;
        CHECK_OBJECT( tmp_class_creation_2__bases );
        tmp_bases_name_2 = tmp_class_creation_2__bases;
        tmp_assign_source_41 = SELECT_METACLASS( tmp_metaclass_name_2, tmp_bases_name_2 );
        Py_DECREF( tmp_metaclass_name_2 );
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_8;
        }
        assert( tmp_class_creation_2__metaclass == NULL );
        tmp_class_creation_2__metaclass = tmp_assign_source_41;
    }
    {
        nuitka_bool tmp_condition_result_9;
        PyObject *tmp_key_name_6;
        PyObject *tmp_dict_name_6;
        tmp_key_name_6 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dict_name_6 = tmp_class_creation_2__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_6, tmp_key_name_6 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_8;
        }
        tmp_condition_result_9 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_9 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_5;
        }
        else
        {
            goto branch_no_5;
        }
        branch_yes_5:;
        CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_2__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_8;
        }
        branch_no_5:;
    }
    {
        nuitka_bool tmp_condition_result_10;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_class_creation_2__metaclass );
        tmp_source_name_5 = tmp_class_creation_2__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_5, const_str_plain___prepare__ );
        tmp_condition_result_10 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_10 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_6;
        }
        else
        {
            goto branch_no_6;
        }
        branch_yes_6:;
        {
            PyObject *tmp_assign_source_42;
            PyObject *tmp_called_name_3;
            PyObject *tmp_source_name_6;
            PyObject *tmp_args_name_3;
            PyObject *tmp_tuple_element_6;
            PyObject *tmp_kw_name_3;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_source_name_6 = tmp_class_creation_2__metaclass;
            tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain___prepare__ );
            if ( tmp_called_name_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_8;
            }
            tmp_tuple_element_6 = const_str_plain_AsyncHTTPClient;
            tmp_args_name_3 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 0, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_6 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_6 );
            PyTuple_SET_ITEM( tmp_args_name_3, 1, tmp_tuple_element_6 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_3 = tmp_class_creation_2__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 138;
            tmp_assign_source_42 = CALL_FUNCTION( tmp_called_name_3, tmp_args_name_3, tmp_kw_name_3 );
            Py_DECREF( tmp_called_name_3 );
            Py_DECREF( tmp_args_name_3 );
            if ( tmp_assign_source_42 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_8;
            }
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_42;
        }
        {
            nuitka_bool tmp_condition_result_11;
            PyObject *tmp_operand_name_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_source_name_7 = tmp_class_creation_2__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_7, const_str_plain___getitem__ );
            tmp_operand_name_2 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_8;
            }
            tmp_condition_result_11 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_11 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_7;
            }
            else
            {
                goto branch_no_7;
            }
            branch_yes_7:;
            {
                PyObject *tmp_raise_type_2;
                PyObject *tmp_raise_value_2;
                PyObject *tmp_left_name_2;
                PyObject *tmp_right_name_2;
                PyObject *tmp_tuple_element_7;
                PyObject *tmp_getattr_target_2;
                PyObject *tmp_getattr_attr_2;
                PyObject *tmp_getattr_default_2;
                PyObject *tmp_source_name_8;
                PyObject *tmp_type_arg_4;
                tmp_raise_type_2 = PyExc_TypeError;
                tmp_left_name_2 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_2__metaclass );
                tmp_getattr_target_2 = tmp_class_creation_2__metaclass;
                tmp_getattr_attr_2 = const_str_plain___name__;
                tmp_getattr_default_2 = const_str_angle_metaclass;
                tmp_tuple_element_7 = BUILTIN_GETATTR( tmp_getattr_target_2, tmp_getattr_attr_2, tmp_getattr_default_2 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 138;

                    goto try_except_handler_8;
                }
                tmp_right_name_2 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_2, 0, tmp_tuple_element_7 );
                CHECK_OBJECT( tmp_class_creation_2__prepared );
                tmp_type_arg_4 = tmp_class_creation_2__prepared;
                tmp_source_name_8 = BUILTIN_TYPE1( tmp_type_arg_4 );
                assert( !(tmp_source_name_8 == NULL) );
                tmp_tuple_element_7 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_tuple_element_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_2 );

                    exception_lineno = 138;

                    goto try_except_handler_8;
                }
                PyTuple_SET_ITEM( tmp_right_name_2, 1, tmp_tuple_element_7 );
                tmp_raise_value_2 = BINARY_OPERATION_REMAINDER( tmp_left_name_2, tmp_right_name_2 );
                Py_DECREF( tmp_right_name_2 );
                if ( tmp_raise_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 138;

                    goto try_except_handler_8;
                }
                exception_type = tmp_raise_type_2;
                Py_INCREF( tmp_raise_type_2 );
                exception_value = tmp_raise_value_2;
                exception_lineno = 138;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_8;
            }
            branch_no_7:;
        }
        goto branch_end_6;
        branch_no_6:;
        {
            PyObject *tmp_assign_source_43;
            tmp_assign_source_43 = PyDict_New();
            assert( tmp_class_creation_2__prepared == NULL );
            tmp_class_creation_2__prepared = tmp_assign_source_43;
        }
        branch_end_6:;
    }
    {
        PyObject *tmp_assign_source_44;
        {
            PyObject *tmp_set_locals_2;
            CHECK_OBJECT( tmp_class_creation_2__prepared );
            tmp_set_locals_2 = tmp_class_creation_2__prepared;
            locals_tornado$httpclient_138 = tmp_set_locals_2;
            Py_INCREF( tmp_set_locals_2 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_6f55736fcd57507504cc7f0643fbf21f;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_10;
        }
        tmp_dictset_value = const_str_digest_f4859e8d4563b7aa7db5db4d395ab49d;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_10;
        }
        tmp_dictset_value = const_str_plain_AsyncHTTPClient;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto try_except_handler_10;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_5ab4f6dbba8ac4d4a89799531de9c715_3, codeobj_5ab4f6dbba8ac4d4a89799531de9c715, module_tornado$httpclient, sizeof(void *) );
        frame_5ab4f6dbba8ac4d4a89799531de9c715_3 = cache_frame_5ab4f6dbba8ac4d4a89799531de9c715_3;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_5ab4f6dbba8ac4d4a89799531de9c715_3 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_5ab4f6dbba8ac4d4a89799531de9c715_3 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain__instance_cache, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_2 = "o";
            goto frame_exception_exit_3;
        }
        {
            nuitka_bool tmp_condition_result_12;
            PyObject *tmp_called_name_4;
            PyObject *tmp_args_element_name_1;
            PyObject *tmp_annotations_5;
            PyObject *tmp_dict_key_7;
            PyObject *tmp_dict_value_7;
            PyObject *tmp_subscribed_name_5;
            PyObject *tmp_mvar_value_8;
            PyObject *tmp_subscript_name_5;
            PyObject *tmp_mvar_value_9;
            PyObject *tmp_classmethod_arg_1;
            PyObject *tmp_annotations_6;
            PyObject *tmp_dict_key_8;
            PyObject *tmp_dict_value_8;
            PyObject *tmp_subscribed_name_6;
            PyObject *tmp_mvar_value_10;
            PyObject *tmp_subscript_name_6;
            PyObject *tmp_mvar_value_11;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$httpclient_138, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 179;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_condition_result_12 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_12 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_5;
            }
            else
            {
                goto condexpr_false_5;
            }
            condexpr_true_5:;
            tmp_called_name_4 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_classmethod );

            if ( tmp_called_name_4 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 179;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

            if ( tmp_called_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 179;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_dict_key_7 = const_str_plain_return;
            tmp_subscribed_name_5 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Type );

            if ( tmp_subscribed_name_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Type );

                if (unlikely( tmp_mvar_value_8 == NULL ))
                {
                    tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Type );
                }

                if ( tmp_mvar_value_8 == NULL )
                {
                    Py_DECREF( tmp_called_name_4 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Type" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 180;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_5 = tmp_mvar_value_8;
                Py_INCREF( tmp_subscribed_name_5 );
                }
            }

            tmp_subscript_name_5 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Configurable );

            if ( tmp_subscript_name_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Configurable );

                if (unlikely( tmp_mvar_value_9 == NULL ))
                {
                    tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Configurable );
                }

                if ( tmp_mvar_value_9 == NULL )
                {
                    Py_DECREF( tmp_called_name_4 );
                    Py_DECREF( tmp_subscribed_name_5 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Configurable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 180;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscript_name_5 = tmp_mvar_value_9;
                Py_INCREF( tmp_subscript_name_5 );
                }
            }

            tmp_dict_value_7 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_5, tmp_subscript_name_5 );
            Py_DECREF( tmp_subscribed_name_5 );
            Py_DECREF( tmp_subscript_name_5 );
            if ( tmp_dict_value_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_4 );

                exception_lineno = 180;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_annotations_5 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_5, tmp_dict_key_7, tmp_dict_value_7 );
            Py_DECREF( tmp_dict_value_7 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_1 = MAKE_FUNCTION_tornado$httpclient$$$function_5_configurable_base( tmp_annotations_5 );



            frame_5ab4f6dbba8ac4d4a89799531de9c715_3->m_frame.f_lineno = 179;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
            }

            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_args_element_name_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 179;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            goto condexpr_end_5;
            condexpr_false_5:;
            tmp_dict_key_8 = const_str_plain_return;
            tmp_subscribed_name_6 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Type );

            if ( tmp_subscribed_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Type );

                if (unlikely( tmp_mvar_value_10 == NULL ))
                {
                    tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Type );
                }

                if ( tmp_mvar_value_10 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Type" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 180;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_6 = tmp_mvar_value_10;
                Py_INCREF( tmp_subscribed_name_6 );
                }
            }

            tmp_subscript_name_6 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Configurable );

            if ( tmp_subscript_name_6 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Configurable );

                if (unlikely( tmp_mvar_value_11 == NULL ))
                {
                    tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Configurable );
                }

                if ( tmp_mvar_value_11 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_6 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Configurable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 180;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscript_name_6 = tmp_mvar_value_11;
                Py_INCREF( tmp_subscript_name_6 );
                }
            }

            tmp_dict_value_8 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_6, tmp_subscript_name_6 );
            Py_DECREF( tmp_subscribed_name_6 );
            Py_DECREF( tmp_subscript_name_6 );
            if ( tmp_dict_value_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 180;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_annotations_6 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_6, tmp_dict_key_8, tmp_dict_value_8 );
            Py_DECREF( tmp_dict_value_8 );
            assert( !(tmp_res != 0) );
            tmp_classmethod_arg_1 = MAKE_FUNCTION_tornado$httpclient$$$function_5_configurable_base( tmp_annotations_6 );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_1 );
            Py_DECREF( tmp_classmethod_arg_1 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 179;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            condexpr_end_5:;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain_configurable_base, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 179;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            nuitka_bool tmp_condition_result_13;
            PyObject *tmp_called_name_5;
            PyObject *tmp_args_element_name_2;
            PyObject *tmp_annotations_7;
            PyObject *tmp_dict_key_9;
            PyObject *tmp_dict_value_9;
            PyObject *tmp_subscribed_name_7;
            PyObject *tmp_mvar_value_12;
            PyObject *tmp_subscript_name_7;
            PyObject *tmp_mvar_value_13;
            PyObject *tmp_classmethod_arg_2;
            PyObject *tmp_annotations_8;
            PyObject *tmp_dict_key_10;
            PyObject *tmp_dict_value_10;
            PyObject *tmp_subscribed_name_8;
            PyObject *tmp_mvar_value_14;
            PyObject *tmp_subscript_name_8;
            PyObject *tmp_mvar_value_15;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$httpclient_138, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 183;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_condition_result_13 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_13 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_6;
            }
            else
            {
                goto condexpr_false_6;
            }
            condexpr_true_6:;
            tmp_called_name_5 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_classmethod );

            if ( tmp_called_name_5 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 183;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

            if ( tmp_called_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 183;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_dict_key_9 = const_str_plain_return;
            tmp_subscribed_name_7 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Type );

            if ( tmp_subscribed_name_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Type );

                if (unlikely( tmp_mvar_value_12 == NULL ))
                {
                    tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Type );
                }

                if ( tmp_mvar_value_12 == NULL )
                {
                    Py_DECREF( tmp_called_name_5 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Type" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 184;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_7 = tmp_mvar_value_12;
                Py_INCREF( tmp_subscribed_name_7 );
                }
            }

            tmp_subscript_name_7 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Configurable );

            if ( tmp_subscript_name_7 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Configurable );

                if (unlikely( tmp_mvar_value_13 == NULL ))
                {
                    tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Configurable );
                }

                if ( tmp_mvar_value_13 == NULL )
                {
                    Py_DECREF( tmp_called_name_5 );
                    Py_DECREF( tmp_subscribed_name_7 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Configurable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 184;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscript_name_7 = tmp_mvar_value_13;
                Py_INCREF( tmp_subscript_name_7 );
                }
            }

            tmp_dict_value_9 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_7, tmp_subscript_name_7 );
            Py_DECREF( tmp_subscribed_name_7 );
            Py_DECREF( tmp_subscript_name_7 );
            if ( tmp_dict_value_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_5 );

                exception_lineno = 184;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_annotations_7 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_7, tmp_dict_key_9, tmp_dict_value_9 );
            Py_DECREF( tmp_dict_value_9 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_2 = MAKE_FUNCTION_tornado$httpclient$$$function_6_configurable_default( tmp_annotations_7 );



            frame_5ab4f6dbba8ac4d4a89799531de9c715_3->m_frame.f_lineno = 183;
            {
                PyObject *call_args[] = { tmp_args_element_name_2 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
            }

            Py_DECREF( tmp_called_name_5 );
            Py_DECREF( tmp_args_element_name_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 183;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            goto condexpr_end_6;
            condexpr_false_6:;
            tmp_dict_key_10 = const_str_plain_return;
            tmp_subscribed_name_8 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Type );

            if ( tmp_subscribed_name_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Type );

                if (unlikely( tmp_mvar_value_14 == NULL ))
                {
                    tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Type );
                }

                if ( tmp_mvar_value_14 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Type" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 184;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_8 = tmp_mvar_value_14;
                Py_INCREF( tmp_subscribed_name_8 );
                }
            }

            tmp_subscript_name_8 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Configurable );

            if ( tmp_subscript_name_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Configurable );

                if (unlikely( tmp_mvar_value_15 == NULL ))
                {
                    tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Configurable );
                }

                if ( tmp_mvar_value_15 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_8 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Configurable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 184;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscript_name_8 = tmp_mvar_value_15;
                Py_INCREF( tmp_subscript_name_8 );
                }
            }

            tmp_dict_value_10 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_8, tmp_subscript_name_8 );
            Py_DECREF( tmp_subscribed_name_8 );
            Py_DECREF( tmp_subscript_name_8 );
            if ( tmp_dict_value_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 184;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_annotations_8 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_8, tmp_dict_key_10, tmp_dict_value_10 );
            Py_DECREF( tmp_dict_value_10 );
            assert( !(tmp_res != 0) );
            tmp_classmethod_arg_2 = MAKE_FUNCTION_tornado$httpclient$$$function_6_configurable_default( tmp_annotations_8 );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_2 );
            Py_DECREF( tmp_classmethod_arg_2 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 183;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            condexpr_end_6:;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain_configurable_default, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 183;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            nuitka_bool tmp_condition_result_14;
            PyObject *tmp_called_name_6;
            PyObject *tmp_args_element_name_3;
            PyObject *tmp_annotations_9;
            PyObject *tmp_dict_key_11;
            PyObject *tmp_dict_value_11;
            PyObject *tmp_subscribed_name_9;
            PyObject *tmp_mvar_value_16;
            PyObject *tmp_subscript_name_9;
            PyObject *tmp_tuple_element_8;
            PyObject *tmp_mvar_value_17;
            PyObject *tmp_classmethod_arg_3;
            PyObject *tmp_annotations_10;
            PyObject *tmp_dict_key_12;
            PyObject *tmp_dict_value_12;
            PyObject *tmp_subscribed_name_10;
            PyObject *tmp_mvar_value_18;
            PyObject *tmp_subscript_name_10;
            PyObject *tmp_tuple_element_9;
            PyObject *tmp_mvar_value_19;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$httpclient_138, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_condition_result_14 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_14 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_7;
            }
            else
            {
                goto condexpr_false_7;
            }
            condexpr_true_7:;
            tmp_called_name_6 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_classmethod );

            if ( tmp_called_name_6 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

            if ( tmp_called_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_dict_key_11 = const_str_plain_return;
            tmp_subscribed_name_9 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Dict );

            if ( tmp_subscribed_name_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_16 == NULL ))
                {
                    tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_16 == NULL )
                {
                    Py_DECREF( tmp_called_name_6 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 190;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_9 = tmp_mvar_value_16;
                Py_INCREF( tmp_subscribed_name_9 );
                }
            }

            tmp_tuple_element_8 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_IOLoop );

            if ( tmp_tuple_element_8 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_IOLoop );

                if (unlikely( tmp_mvar_value_17 == NULL ))
                {
                    tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOLoop );
                }

                if ( tmp_mvar_value_17 == NULL )
                {
                    Py_DECREF( tmp_called_name_6 );
                    Py_DECREF( tmp_subscribed_name_9 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOLoop" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 190;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_tuple_element_8 = tmp_mvar_value_17;
                Py_INCREF( tmp_tuple_element_8 );
                }
            }

            tmp_subscript_name_9 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_9, 0, tmp_tuple_element_8 );
            tmp_tuple_element_8 = const_str_plain_AsyncHTTPClient;
            Py_INCREF( tmp_tuple_element_8 );
            PyTuple_SET_ITEM( tmp_subscript_name_9, 1, tmp_tuple_element_8 );
            tmp_dict_value_11 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_9, tmp_subscript_name_9 );
            Py_DECREF( tmp_subscribed_name_9 );
            Py_DECREF( tmp_subscript_name_9 );
            if ( tmp_dict_value_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_6 );

                exception_lineno = 190;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_annotations_9 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_9, tmp_dict_key_11, tmp_dict_value_11 );
            Py_DECREF( tmp_dict_value_11 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_3 = MAKE_FUNCTION_tornado$httpclient$$$function_7__async_clients( tmp_annotations_9 );



            frame_5ab4f6dbba8ac4d4a89799531de9c715_3->m_frame.f_lineno = 189;
            {
                PyObject *call_args[] = { tmp_args_element_name_3 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
            }

            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_args_element_name_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            goto condexpr_end_7;
            condexpr_false_7:;
            tmp_dict_key_12 = const_str_plain_return;
            tmp_subscribed_name_10 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Dict );

            if ( tmp_subscribed_name_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_18 == NULL ))
                {
                    tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_18 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 190;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_10 = tmp_mvar_value_18;
                Py_INCREF( tmp_subscribed_name_10 );
                }
            }

            tmp_tuple_element_9 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_IOLoop );

            if ( tmp_tuple_element_9 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_IOLoop );

                if (unlikely( tmp_mvar_value_19 == NULL ))
                {
                    tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_IOLoop );
                }

                if ( tmp_mvar_value_19 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_10 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "IOLoop" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 190;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_tuple_element_9 = tmp_mvar_value_19;
                Py_INCREF( tmp_tuple_element_9 );
                }
            }

            tmp_subscript_name_10 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_10, 0, tmp_tuple_element_9 );
            tmp_tuple_element_9 = const_str_plain_AsyncHTTPClient;
            Py_INCREF( tmp_tuple_element_9 );
            PyTuple_SET_ITEM( tmp_subscript_name_10, 1, tmp_tuple_element_9 );
            tmp_dict_value_12 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_10, tmp_subscript_name_10 );
            Py_DECREF( tmp_subscribed_name_10 );
            Py_DECREF( tmp_subscript_name_10 );
            if ( tmp_dict_value_12 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 190;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_annotations_10 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_10, tmp_dict_key_12, tmp_dict_value_12 );
            Py_DECREF( tmp_dict_value_12 );
            assert( !(tmp_res != 0) );
            tmp_classmethod_arg_3 = MAKE_FUNCTION_tornado$httpclient$$$function_7__async_clients( tmp_annotations_10 );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_3 );
            Py_DECREF( tmp_classmethod_arg_3 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            condexpr_end_7:;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain__async_clients, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 189;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_2;
            PyObject *tmp_annotations_11;
            PyObject *tmp_dict_key_13;
            PyObject *tmp_dict_value_13;
            PyObject *tmp_dict_key_14;
            PyObject *tmp_dict_value_14;
            PyObject *tmp_mvar_value_20;
            PyObject *tmp_dict_key_15;
            PyObject *tmp_dict_value_15;
            tmp_defaults_2 = const_tuple_false_tuple;
            tmp_dict_key_13 = const_str_plain_force_instance;
            tmp_dict_value_13 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_bool );

            if ( tmp_dict_value_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_13 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_13 );
                }
            }

            tmp_annotations_11 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_11, tmp_dict_key_13, tmp_dict_value_13 );
            Py_DECREF( tmp_dict_value_13 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_14 = const_str_plain_kwargs;
            tmp_dict_value_14 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Any );

            if ( tmp_dict_value_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_20 == NULL ))
                {
                    tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_20 == NULL )
                {
                    Py_DECREF( tmp_annotations_11 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 196;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_dict_value_14 = tmp_mvar_value_20;
                Py_INCREF( tmp_dict_value_14 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_11, tmp_dict_key_14, tmp_dict_value_14 );
            Py_DECREF( tmp_dict_value_14 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_15 = const_str_plain_return;
            tmp_dict_value_15 = const_str_plain_AsyncHTTPClient;
            tmp_res = PyDict_SetItem( tmp_annotations_11, tmp_dict_key_15, tmp_dict_value_15 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_2 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_8___new__( tmp_defaults_2, tmp_annotations_11 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain___new__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_3;
            PyObject *tmp_annotations_12;
            PyObject *tmp_dict_key_16;
            PyObject *tmp_dict_value_16;
            PyObject *tmp_subscribed_name_11;
            PyObject *tmp_mvar_value_21;
            PyObject *tmp_subscript_name_11;
            PyObject *tmp_tuple_element_10;
            PyObject *tmp_mvar_value_22;
            PyObject *tmp_dict_key_17;
            PyObject *tmp_dict_value_17;
            tmp_defaults_3 = const_tuple_none_tuple;
            tmp_dict_key_16 = const_str_plain_defaults;
            tmp_subscribed_name_11 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Dict );

            if ( tmp_subscribed_name_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_21 == NULL ))
                {
                    tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_21 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 214;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_11 = tmp_mvar_value_21;
                Py_INCREF( tmp_subscribed_name_11 );
                }
            }

            tmp_tuple_element_10 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_str );

            if ( tmp_tuple_element_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_10 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_10 );
                }
            }

            tmp_subscript_name_11 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_11, 0, tmp_tuple_element_10 );
            tmp_tuple_element_10 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Any );

            if ( tmp_tuple_element_10 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_22 == NULL ))
                {
                    tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_22 == NULL )
                {
                    Py_DECREF( tmp_subscribed_name_11 );
                    Py_DECREF( tmp_subscript_name_11 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 214;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_tuple_element_10 = tmp_mvar_value_22;
                Py_INCREF( tmp_tuple_element_10 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_11, 1, tmp_tuple_element_10 );
            tmp_dict_value_16 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_11, tmp_subscript_name_11 );
            Py_DECREF( tmp_subscribed_name_11 );
            Py_DECREF( tmp_subscript_name_11 );
            if ( tmp_dict_value_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_annotations_12 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_12, tmp_dict_key_16, tmp_dict_value_16 );
            Py_DECREF( tmp_dict_value_16 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_17 = const_str_plain_return;
            tmp_dict_value_17 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_12, tmp_dict_key_17, tmp_dict_value_17 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_3 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_9_initialize( tmp_defaults_3, tmp_annotations_12 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain_initialize, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 214;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_annotations_13;
            tmp_annotations_13 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_10_close( tmp_annotations_13 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain_close, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 221;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_defaults_4;
            PyObject *tmp_annotations_14;
            PyObject *tmp_dict_key_18;
            PyObject *tmp_dict_value_18;
            PyObject *tmp_subscribed_name_12;
            PyObject *tmp_mvar_value_23;
            PyObject *tmp_subscript_name_12;
            PyObject *tmp_tuple_element_11;
            PyObject *tmp_dict_key_19;
            PyObject *tmp_dict_value_19;
            PyObject *tmp_dict_key_20;
            PyObject *tmp_dict_value_20;
            PyObject *tmp_mvar_value_24;
            PyObject *tmp_dict_key_21;
            PyObject *tmp_dict_value_21;
            PyObject *tmp_subscribed_name_13;
            PyObject *tmp_mvar_value_25;
            PyObject *tmp_subscript_name_13;
            tmp_defaults_4 = const_tuple_true_tuple;
            tmp_dict_key_18 = const_str_plain_request;
            tmp_subscribed_name_12 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Union );

            if ( tmp_subscribed_name_12 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_23 == NULL ))
                {
                    tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_23 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 249;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_12 = tmp_mvar_value_23;
                Py_INCREF( tmp_subscribed_name_12 );
                }
            }

            tmp_tuple_element_11 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_str );

            if ( tmp_tuple_element_11 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_11 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_11 );
                }
            }

            tmp_subscript_name_12 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_12, 0, tmp_tuple_element_11 );
            tmp_tuple_element_11 = const_str_plain_HTTPRequest;
            Py_INCREF( tmp_tuple_element_11 );
            PyTuple_SET_ITEM( tmp_subscript_name_12, 1, tmp_tuple_element_11 );
            tmp_dict_value_18 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_12, tmp_subscript_name_12 );
            Py_DECREF( tmp_subscribed_name_12 );
            Py_DECREF( tmp_subscript_name_12 );
            if ( tmp_dict_value_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 249;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_annotations_14 = _PyDict_NewPresized( 4 );
            tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_18, tmp_dict_value_18 );
            Py_DECREF( tmp_dict_value_18 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_19 = const_str_plain_raise_error;
            tmp_dict_value_19 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_bool );

            if ( tmp_dict_value_19 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_19 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_19 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_19, tmp_dict_value_19 );
            Py_DECREF( tmp_dict_value_19 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_20 = const_str_plain_kwargs;
            tmp_dict_value_20 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Any );

            if ( tmp_dict_value_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_24 == NULL ))
                {
                    tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_24 == NULL )
                {
                    Py_DECREF( tmp_annotations_14 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 251;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_dict_value_20 = tmp_mvar_value_24;
                Py_INCREF( tmp_dict_value_20 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_20, tmp_dict_value_20 );
            Py_DECREF( tmp_dict_value_20 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_21 = const_str_plain_return;
            tmp_subscribed_name_13 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Awaitable );

            if ( tmp_subscribed_name_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Awaitable );

                if (unlikely( tmp_mvar_value_25 == NULL ))
                {
                    tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Awaitable );
                }

                if ( tmp_mvar_value_25 == NULL )
                {
                    Py_DECREF( tmp_annotations_14 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Awaitable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 252;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_13 = tmp_mvar_value_25;
                Py_INCREF( tmp_subscribed_name_13 );
                }
            }

            tmp_subscript_name_13 = const_str_plain_HTTPResponse;
            tmp_dict_value_21 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_13, tmp_subscript_name_13 );
            Py_DECREF( tmp_subscribed_name_13 );
            if ( tmp_dict_value_21 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_14 );

                exception_lineno = 252;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_14, tmp_dict_key_21, tmp_dict_value_21 );
            Py_DECREF( tmp_dict_value_21 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_4 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_11_fetch( tmp_defaults_4, tmp_annotations_14 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain_fetch, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 247;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            PyObject *tmp_annotations_15;
            PyObject *tmp_dict_key_22;
            PyObject *tmp_dict_value_22;
            PyObject *tmp_dict_key_23;
            PyObject *tmp_dict_value_23;
            PyObject *tmp_subscribed_name_14;
            PyObject *tmp_mvar_value_26;
            PyObject *tmp_subscript_name_14;
            PyObject *tmp_dict_key_24;
            PyObject *tmp_dict_value_24;
            tmp_dict_key_22 = const_str_plain_request;
            tmp_dict_value_22 = const_str_plain_HTTPRequest;
            tmp_annotations_15 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_15, tmp_dict_key_22, tmp_dict_value_22 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_23 = const_str_plain_callback;
            tmp_subscribed_name_14 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Callable );

            if ( tmp_subscribed_name_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Callable );

                if (unlikely( tmp_mvar_value_26 == NULL ))
                {
                    tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Callable );
                }

                if ( tmp_mvar_value_26 == NULL )
                {
                    Py_DECREF( tmp_annotations_15 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Callable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 308;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_subscribed_name_14 = tmp_mvar_value_26;
                Py_INCREF( tmp_subscribed_name_14 );
                }
            }

            tmp_subscript_name_14 = DEEP_COPY( const_tuple_list_str_plain_HTTPResponse_list_none_tuple );
            tmp_dict_value_23 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_14, tmp_subscript_name_14 );
            Py_DECREF( tmp_subscribed_name_14 );
            Py_DECREF( tmp_subscript_name_14 );
            if ( tmp_dict_value_23 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_15 );

                exception_lineno = 308;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_15, tmp_dict_key_23, tmp_dict_value_23 );
            Py_DECREF( tmp_dict_value_23 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_24 = const_str_plain_return;
            tmp_dict_value_24 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_15, tmp_dict_key_24, tmp_dict_value_24 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_12_fetch_impl( tmp_annotations_15 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain_fetch_impl, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 307;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }
        {
            nuitka_bool tmp_condition_result_15;
            PyObject *tmp_called_name_7;
            PyObject *tmp_args_element_name_4;
            PyObject *tmp_annotations_16;
            PyObject *tmp_dict_key_25;
            PyObject *tmp_dict_value_25;
            PyObject *tmp_dict_key_26;
            PyObject *tmp_dict_value_26;
            PyObject *tmp_mvar_value_27;
            PyObject *tmp_dict_key_27;
            PyObject *tmp_dict_value_27;
            PyObject *tmp_classmethod_arg_4;
            PyObject *tmp_annotations_17;
            PyObject *tmp_dict_key_28;
            PyObject *tmp_dict_value_28;
            PyObject *tmp_dict_key_29;
            PyObject *tmp_dict_value_29;
            PyObject *tmp_mvar_value_28;
            PyObject *tmp_dict_key_30;
            PyObject *tmp_dict_value_30;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$httpclient_138, const_str_plain_classmethod );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 312;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_condition_result_15 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_15 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_8;
            }
            else
            {
                goto condexpr_false_8;
            }
            condexpr_true_8:;
            tmp_called_name_7 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_classmethod );

            if ( tmp_called_name_7 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "classmethod" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 312;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }

            if ( tmp_called_name_7 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 312;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            tmp_dict_key_25 = const_str_plain_impl;
            tmp_dict_value_25 = const_str_digest_649be217853297dd7bed9a9de82b5db6;
            tmp_annotations_16 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_25, tmp_dict_value_25 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_26 = const_str_plain_kwargs;
            tmp_dict_value_26 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Any );

            if ( tmp_dict_value_26 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_27 == NULL ))
                {
                    tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_27 == NULL )
                {
                    Py_DECREF( tmp_called_name_7 );
                    Py_DECREF( tmp_annotations_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 314;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_dict_value_26 = tmp_mvar_value_27;
                Py_INCREF( tmp_dict_value_26 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_26, tmp_dict_value_26 );
            Py_DECREF( tmp_dict_value_26 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_27 = const_str_plain_return;
            tmp_dict_value_27 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_16, tmp_dict_key_27, tmp_dict_value_27 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_4 = MAKE_FUNCTION_tornado$httpclient$$$function_13_configure( tmp_annotations_16 );



            frame_5ab4f6dbba8ac4d4a89799531de9c715_3->m_frame.f_lineno = 312;
            {
                PyObject *call_args[] = { tmp_args_element_name_4 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
            }

            Py_DECREF( tmp_called_name_7 );
            Py_DECREF( tmp_args_element_name_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 312;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            goto condexpr_end_8;
            condexpr_false_8:;
            tmp_dict_key_28 = const_str_plain_impl;
            tmp_dict_value_28 = const_str_digest_649be217853297dd7bed9a9de82b5db6;
            tmp_annotations_17 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_28, tmp_dict_value_28 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_29 = const_str_plain_kwargs;
            tmp_dict_value_29 = PyObject_GetItem( locals_tornado$httpclient_138, const_str_plain_Any );

            if ( tmp_dict_value_29 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_28 == NULL ))
                {
                    tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_28 == NULL )
                {
                    Py_DECREF( tmp_annotations_17 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 314;
                    type_description_2 = "o";
                    goto frame_exception_exit_3;
                }

                tmp_dict_value_29 = tmp_mvar_value_28;
                Py_INCREF( tmp_dict_value_29 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_29, tmp_dict_value_29 );
            Py_DECREF( tmp_dict_value_29 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_30 = const_str_plain_return;
            tmp_dict_value_30 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_17, tmp_dict_key_30, tmp_dict_value_30 );
            assert( !(tmp_res != 0) );
            tmp_classmethod_arg_4 = MAKE_FUNCTION_tornado$httpclient$$$function_13_configure( tmp_annotations_17 );



            tmp_dictset_value = BUILTIN_CLASSMETHOD( tmp_classmethod_arg_4 );
            Py_DECREF( tmp_classmethod_arg_4 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 312;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
            condexpr_end_8:;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain_configure, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 312;
                type_description_2 = "o";
                goto frame_exception_exit_3;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_5ab4f6dbba8ac4d4a89799531de9c715_3 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_2;

        frame_exception_exit_3:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_5ab4f6dbba8ac4d4a89799531de9c715_3 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_5ab4f6dbba8ac4d4a89799531de9c715_3, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_5ab4f6dbba8ac4d4a89799531de9c715_3->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_5ab4f6dbba8ac4d4a89799531de9c715_3, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_5ab4f6dbba8ac4d4a89799531de9c715_3,
            type_description_2,
            outline_1_var___class__
        );


        // Release cached frame.
        if ( frame_5ab4f6dbba8ac4d4a89799531de9c715_3 == cache_frame_5ab4f6dbba8ac4d4a89799531de9c715_3 )
        {
            Py_DECREF( frame_5ab4f6dbba8ac4d4a89799531de9c715_3 );
        }
        cache_frame_5ab4f6dbba8ac4d4a89799531de9c715_3 = NULL;

        assertFrameObject( frame_5ab4f6dbba8ac4d4a89799531de9c715_3 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_2;

        frame_no_exception_2:;
        goto skip_nested_handling_2;
        nested_frame_exit_2:;

        goto try_except_handler_10;
        skip_nested_handling_2:;
        {
            nuitka_bool tmp_condition_result_16;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_compexpr_left_2 = tmp_class_creation_2__bases;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_compexpr_right_2 = tmp_class_creation_2__bases_orig;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_10;
            }
            tmp_condition_result_16 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_16 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_8;
            }
            else
            {
                goto branch_no_8;
            }
            branch_yes_8:;
            CHECK_OBJECT( tmp_class_creation_2__bases_orig );
            tmp_dictset_value = tmp_class_creation_2__bases_orig;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_138, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_10;
            }
            branch_no_8:;
        }
        {
            PyObject *tmp_assign_source_45;
            PyObject *tmp_called_name_8;
            PyObject *tmp_args_name_4;
            PyObject *tmp_tuple_element_12;
            PyObject *tmp_kw_name_4;
            CHECK_OBJECT( tmp_class_creation_2__metaclass );
            tmp_called_name_8 = tmp_class_creation_2__metaclass;
            tmp_tuple_element_12 = const_str_plain_AsyncHTTPClient;
            tmp_args_name_4 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_4, 0, tmp_tuple_element_12 );
            CHECK_OBJECT( tmp_class_creation_2__bases );
            tmp_tuple_element_12 = tmp_class_creation_2__bases;
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_4, 1, tmp_tuple_element_12 );
            tmp_tuple_element_12 = locals_tornado$httpclient_138;
            Py_INCREF( tmp_tuple_element_12 );
            PyTuple_SET_ITEM( tmp_args_name_4, 2, tmp_tuple_element_12 );
            CHECK_OBJECT( tmp_class_creation_2__class_decl_dict );
            tmp_kw_name_4 = tmp_class_creation_2__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 138;
            tmp_assign_source_45 = CALL_FUNCTION( tmp_called_name_8, tmp_args_name_4, tmp_kw_name_4 );
            Py_DECREF( tmp_args_name_4 );
            if ( tmp_assign_source_45 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 138;

                goto try_except_handler_10;
            }
            assert( outline_1_var___class__ == NULL );
            outline_1_var___class__ = tmp_assign_source_45;
        }
        CHECK_OBJECT( outline_1_var___class__ );
        tmp_assign_source_44 = outline_1_var___class__;
        Py_INCREF( tmp_assign_source_44 );
        goto try_return_handler_10;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_10:;
        Py_DECREF( locals_tornado$httpclient_138 );
        locals_tornado$httpclient_138 = NULL;
        goto try_return_handler_9;
        // Exception handler code:
        try_except_handler_10:;
        exception_keeper_type_8 = exception_type;
        exception_keeper_value_8 = exception_value;
        exception_keeper_tb_8 = exception_tb;
        exception_keeper_lineno_8 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$httpclient_138 );
        locals_tornado$httpclient_138 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_8;
        exception_value = exception_keeper_value_8;
        exception_tb = exception_keeper_tb_8;
        exception_lineno = exception_keeper_lineno_8;

        goto try_except_handler_9;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_9:;
        CHECK_OBJECT( (PyObject *)outline_1_var___class__ );
        Py_DECREF( outline_1_var___class__ );
        outline_1_var___class__ = NULL;

        goto outline_result_2;
        // Exception handler code:
        try_except_handler_9:;
        exception_keeper_type_9 = exception_type;
        exception_keeper_value_9 = exception_value;
        exception_keeper_tb_9 = exception_tb;
        exception_keeper_lineno_9 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_9;
        exception_value = exception_keeper_value_9;
        exception_tb = exception_keeper_tb_9;
        exception_lineno = exception_keeper_lineno_9;

        goto outline_exception_2;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_2:;
        exception_lineno = 138;
        goto try_except_handler_8;
        outline_result_2:;
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_AsyncHTTPClient, tmp_assign_source_44 );
    }
    goto try_end_6;
    // Exception handler code:
    try_except_handler_8:;
    exception_keeper_type_10 = exception_type;
    exception_keeper_value_10 = exception_value;
    exception_keeper_tb_10 = exception_tb;
    exception_keeper_lineno_10 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    Py_XDECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    Py_XDECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_10;
    exception_value = exception_keeper_value_10;
    exception_tb = exception_keeper_tb_10;
    exception_lineno = exception_keeper_lineno_10;

    goto frame_exception_exit_1;
    // End of try:
    try_end_6:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases_orig );
    Py_DECREF( tmp_class_creation_2__bases_orig );
    tmp_class_creation_2__bases_orig = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__bases );
    Py_DECREF( tmp_class_creation_2__bases );
    tmp_class_creation_2__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__class_decl_dict );
    Py_DECREF( tmp_class_creation_2__class_decl_dict );
    tmp_class_creation_2__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__metaclass );
    Py_DECREF( tmp_class_creation_2__metaclass );
    tmp_class_creation_2__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_2__prepared );
    Py_DECREF( tmp_class_creation_2__prepared );
    tmp_class_creation_2__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_dircall_arg1_3;
        tmp_dircall_arg1_3 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_3 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_3};
            tmp_assign_source_46 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_11;
        }
        assert( tmp_class_creation_3__bases == NULL );
        tmp_class_creation_3__bases = tmp_assign_source_46;
    }
    {
        PyObject *tmp_assign_source_47;
        tmp_assign_source_47 = PyDict_New();
        assert( tmp_class_creation_3__class_decl_dict == NULL );
        tmp_class_creation_3__class_decl_dict = tmp_assign_source_47;
    }
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_metaclass_name_3;
        nuitka_bool tmp_condition_result_17;
        PyObject *tmp_key_name_7;
        PyObject *tmp_dict_name_7;
        PyObject *tmp_dict_name_8;
        PyObject *tmp_key_name_8;
        nuitka_bool tmp_condition_result_18;
        int tmp_truth_name_3;
        PyObject *tmp_type_arg_5;
        PyObject *tmp_subscribed_name_15;
        PyObject *tmp_subscript_name_15;
        PyObject *tmp_bases_name_3;
        tmp_key_name_7 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_7 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_7, tmp_key_name_7 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_11;
        }
        tmp_condition_result_17 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_17 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_9;
        }
        else
        {
            goto condexpr_false_9;
        }
        condexpr_true_9:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_8 = tmp_class_creation_3__class_decl_dict;
        tmp_key_name_8 = const_str_plain_metaclass;
        tmp_metaclass_name_3 = DICT_GET_ITEM( tmp_dict_name_8, tmp_key_name_8 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_11;
        }
        goto condexpr_end_9;
        condexpr_false_9:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_truth_name_3 = CHECK_IF_TRUE( tmp_class_creation_3__bases );
        if ( tmp_truth_name_3 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_11;
        }
        tmp_condition_result_18 = tmp_truth_name_3 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_18 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_10;
        }
        else
        {
            goto condexpr_false_10;
        }
        condexpr_true_10:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_subscribed_name_15 = tmp_class_creation_3__bases;
        tmp_subscript_name_15 = const_int_0;
        tmp_type_arg_5 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_15, tmp_subscript_name_15, 0 );
        if ( tmp_type_arg_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_11;
        }
        tmp_metaclass_name_3 = BUILTIN_TYPE1( tmp_type_arg_5 );
        Py_DECREF( tmp_type_arg_5 );
        if ( tmp_metaclass_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_11;
        }
        goto condexpr_end_10;
        condexpr_false_10:;
        tmp_metaclass_name_3 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_3 );
        condexpr_end_10:;
        condexpr_end_9:;
        CHECK_OBJECT( tmp_class_creation_3__bases );
        tmp_bases_name_3 = tmp_class_creation_3__bases;
        tmp_assign_source_48 = SELECT_METACLASS( tmp_metaclass_name_3, tmp_bases_name_3 );
        Py_DECREF( tmp_metaclass_name_3 );
        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_11;
        }
        assert( tmp_class_creation_3__metaclass == NULL );
        tmp_class_creation_3__metaclass = tmp_assign_source_48;
    }
    {
        nuitka_bool tmp_condition_result_19;
        PyObject *tmp_key_name_9;
        PyObject *tmp_dict_name_9;
        tmp_key_name_9 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dict_name_9 = tmp_class_creation_3__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_9, tmp_key_name_9 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_11;
        }
        tmp_condition_result_19 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_19 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_9;
        }
        else
        {
            goto branch_no_9;
        }
        branch_yes_9:;
        CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_3__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_11;
        }
        branch_no_9:;
    }
    {
        nuitka_bool tmp_condition_result_20;
        PyObject *tmp_source_name_9;
        CHECK_OBJECT( tmp_class_creation_3__metaclass );
        tmp_source_name_9 = tmp_class_creation_3__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_9, const_str_plain___prepare__ );
        tmp_condition_result_20 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_20 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_10;
        }
        else
        {
            goto branch_no_10;
        }
        branch_yes_10:;
        {
            PyObject *tmp_assign_source_49;
            PyObject *tmp_called_name_9;
            PyObject *tmp_source_name_10;
            PyObject *tmp_args_name_5;
            PyObject *tmp_tuple_element_13;
            PyObject *tmp_kw_name_5;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_source_name_10 = tmp_class_creation_3__metaclass;
            tmp_called_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain___prepare__ );
            if ( tmp_called_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 337;

                goto try_except_handler_11;
            }
            tmp_tuple_element_13 = const_str_plain_HTTPRequest;
            tmp_args_name_5 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_13 );
            PyTuple_SET_ITEM( tmp_args_name_5, 0, tmp_tuple_element_13 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_13 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_13 );
            PyTuple_SET_ITEM( tmp_args_name_5, 1, tmp_tuple_element_13 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_5 = tmp_class_creation_3__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 337;
            tmp_assign_source_49 = CALL_FUNCTION( tmp_called_name_9, tmp_args_name_5, tmp_kw_name_5 );
            Py_DECREF( tmp_called_name_9 );
            Py_DECREF( tmp_args_name_5 );
            if ( tmp_assign_source_49 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 337;

                goto try_except_handler_11;
            }
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_49;
        }
        {
            nuitka_bool tmp_condition_result_21;
            PyObject *tmp_operand_name_3;
            PyObject *tmp_source_name_11;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_source_name_11 = tmp_class_creation_3__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_11, const_str_plain___getitem__ );
            tmp_operand_name_3 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 337;

                goto try_except_handler_11;
            }
            tmp_condition_result_21 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_21 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_11;
            }
            else
            {
                goto branch_no_11;
            }
            branch_yes_11:;
            {
                PyObject *tmp_raise_type_3;
                PyObject *tmp_raise_value_3;
                PyObject *tmp_left_name_3;
                PyObject *tmp_right_name_3;
                PyObject *tmp_tuple_element_14;
                PyObject *tmp_getattr_target_3;
                PyObject *tmp_getattr_attr_3;
                PyObject *tmp_getattr_default_3;
                PyObject *tmp_source_name_12;
                PyObject *tmp_type_arg_6;
                tmp_raise_type_3 = PyExc_TypeError;
                tmp_left_name_3 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_3__metaclass );
                tmp_getattr_target_3 = tmp_class_creation_3__metaclass;
                tmp_getattr_attr_3 = const_str_plain___name__;
                tmp_getattr_default_3 = const_str_angle_metaclass;
                tmp_tuple_element_14 = BUILTIN_GETATTR( tmp_getattr_target_3, tmp_getattr_attr_3, tmp_getattr_default_3 );
                if ( tmp_tuple_element_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 337;

                    goto try_except_handler_11;
                }
                tmp_right_name_3 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_3, 0, tmp_tuple_element_14 );
                CHECK_OBJECT( tmp_class_creation_3__prepared );
                tmp_type_arg_6 = tmp_class_creation_3__prepared;
                tmp_source_name_12 = BUILTIN_TYPE1( tmp_type_arg_6 );
                assert( !(tmp_source_name_12 == NULL) );
                tmp_tuple_element_14 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_12 );
                if ( tmp_tuple_element_14 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_3 );

                    exception_lineno = 337;

                    goto try_except_handler_11;
                }
                PyTuple_SET_ITEM( tmp_right_name_3, 1, tmp_tuple_element_14 );
                tmp_raise_value_3 = BINARY_OPERATION_REMAINDER( tmp_left_name_3, tmp_right_name_3 );
                Py_DECREF( tmp_right_name_3 );
                if ( tmp_raise_value_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 337;

                    goto try_except_handler_11;
                }
                exception_type = tmp_raise_type_3;
                Py_INCREF( tmp_raise_type_3 );
                exception_value = tmp_raise_value_3;
                exception_lineno = 337;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_11;
            }
            branch_no_11:;
        }
        goto branch_end_10;
        branch_no_10:;
        {
            PyObject *tmp_assign_source_50;
            tmp_assign_source_50 = PyDict_New();
            assert( tmp_class_creation_3__prepared == NULL );
            tmp_class_creation_3__prepared = tmp_assign_source_50;
        }
        branch_end_10:;
    }
    {
        PyObject *tmp_assign_source_51;
        {
            PyObject *tmp_set_locals_3;
            CHECK_OBJECT( tmp_class_creation_3__prepared );
            tmp_set_locals_3 = tmp_class_creation_3__prepared;
            locals_tornado$httpclient_337 = tmp_set_locals_3;
            Py_INCREF( tmp_set_locals_3 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_6f55736fcd57507504cc7f0643fbf21f;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_13;
        }
        tmp_dictset_value = const_str_digest_04f1eb4be9879dbd1efb1ec5bd490663;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_13;
        }
        tmp_dictset_value = const_str_plain_HTTPRequest;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 337;

            goto try_except_handler_13;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_3c585940a00c16cc3fd3083c84c8ac0a_4, codeobj_3c585940a00c16cc3fd3083c84c8ac0a, module_tornado$httpclient, sizeof(void *) );
        frame_3c585940a00c16cc3fd3083c84c8ac0a_4 = cache_frame_3c585940a00c16cc3fd3083c84c8ac0a_4;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_3c585940a00c16cc3fd3083c84c8ac0a_4 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_3c585940a00c16cc3fd3083c84c8ac0a_4 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain__headers, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 340;
            type_description_2 = "o";
            goto frame_exception_exit_4;
        }
        {
            nuitka_bool tmp_condition_result_22;
            PyObject *tmp_called_name_10;
            PyObject *tmp_kw_name_6;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$httpclient_337, const_str_plain_dict );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 345;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_22 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_22 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_11;
            }
            else
            {
                goto condexpr_false_11;
            }
            condexpr_true_11:;
            tmp_called_name_10 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_dict );

            if ( tmp_called_name_10 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "dict" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 345;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 345;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_kw_name_6 = PyDict_Copy( const_dict_480a75f460aacdb5be24e0bff4669741 );
            frame_3c585940a00c16cc3fd3083c84c8ac0a_4->m_frame.f_lineno = 345;
            tmp_dictset_value = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_10, tmp_kw_name_6 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_kw_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 345;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_11;
            condexpr_false_11:;
            tmp_dictset_value = PyDict_Copy( const_dict_480a75f460aacdb5be24e0bff4669741 );
            condexpr_end_11:;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain__DEFAULTS, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 345;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            PyObject *tmp_defaults_5;
            PyObject *tmp_annotations_18;
            PyObject *tmp_dict_key_31;
            PyObject *tmp_dict_value_31;
            PyObject *tmp_dict_key_32;
            PyObject *tmp_dict_value_32;
            PyObject *tmp_dict_key_33;
            PyObject *tmp_dict_value_33;
            PyObject *tmp_subscribed_name_16;
            PyObject *tmp_mvar_value_29;
            PyObject *tmp_subscript_name_16;
            PyObject *tmp_tuple_element_15;
            PyObject *tmp_subscribed_name_17;
            PyObject *tmp_mvar_value_30;
            PyObject *tmp_subscript_name_17;
            PyObject *tmp_tuple_element_16;
            PyObject *tmp_source_name_13;
            PyObject *tmp_mvar_value_31;
            PyObject *tmp_dict_key_34;
            PyObject *tmp_dict_value_34;
            PyObject *tmp_subscribed_name_18;
            PyObject *tmp_mvar_value_32;
            PyObject *tmp_subscript_name_18;
            PyObject *tmp_tuple_element_17;
            PyObject *tmp_dict_key_35;
            PyObject *tmp_dict_value_35;
            PyObject *tmp_dict_key_36;
            PyObject *tmp_dict_value_36;
            PyObject *tmp_dict_key_37;
            PyObject *tmp_dict_value_37;
            PyObject *tmp_dict_key_38;
            PyObject *tmp_dict_value_38;
            PyObject *tmp_dict_key_39;
            PyObject *tmp_dict_value_39;
            PyObject *tmp_dict_key_40;
            PyObject *tmp_dict_value_40;
            PyObject *tmp_subscribed_name_19;
            PyObject *tmp_mvar_value_33;
            PyObject *tmp_subscript_name_19;
            PyObject *tmp_tuple_element_18;
            PyObject *tmp_source_name_14;
            PyObject *tmp_mvar_value_34;
            PyObject *tmp_dict_key_41;
            PyObject *tmp_dict_value_41;
            PyObject *tmp_dict_key_42;
            PyObject *tmp_dict_value_42;
            PyObject *tmp_dict_key_43;
            PyObject *tmp_dict_value_43;
            PyObject *tmp_dict_key_44;
            PyObject *tmp_dict_value_44;
            PyObject *tmp_dict_key_45;
            PyObject *tmp_dict_value_45;
            PyObject *tmp_dict_key_46;
            PyObject *tmp_dict_value_46;
            PyObject *tmp_subscribed_name_20;
            PyObject *tmp_mvar_value_35;
            PyObject *tmp_subscript_name_20;
            PyObject *tmp_tuple_element_19;
            PyObject *tmp_list_element_1;
            PyObject *tmp_dict_key_47;
            PyObject *tmp_dict_value_47;
            PyObject *tmp_subscribed_name_21;
            PyObject *tmp_mvar_value_36;
            PyObject *tmp_subscript_name_21;
            PyObject *tmp_tuple_element_20;
            PyObject *tmp_list_element_2;
            PyObject *tmp_dict_key_48;
            PyObject *tmp_dict_value_48;
            PyObject *tmp_subscribed_name_22;
            PyObject *tmp_mvar_value_37;
            PyObject *tmp_subscript_name_22;
            PyObject *tmp_tuple_element_21;
            PyObject *tmp_list_element_3;
            PyObject *tmp_mvar_value_38;
            PyObject *tmp_dict_key_49;
            PyObject *tmp_dict_value_49;
            PyObject *tmp_dict_key_50;
            PyObject *tmp_dict_value_50;
            PyObject *tmp_dict_key_51;
            PyObject *tmp_dict_value_51;
            PyObject *tmp_dict_key_52;
            PyObject *tmp_dict_value_52;
            PyObject *tmp_dict_key_53;
            PyObject *tmp_dict_value_53;
            PyObject *tmp_dict_key_54;
            PyObject *tmp_dict_value_54;
            PyObject *tmp_dict_key_55;
            PyObject *tmp_dict_value_55;
            PyObject *tmp_dict_key_56;
            PyObject *tmp_dict_value_56;
            PyObject *tmp_dict_key_57;
            PyObject *tmp_dict_value_57;
            PyObject *tmp_dict_key_58;
            PyObject *tmp_dict_value_58;
            PyObject *tmp_dict_key_59;
            PyObject *tmp_dict_value_59;
            PyObject *tmp_dict_key_60;
            PyObject *tmp_dict_value_60;
            PyObject *tmp_subscribed_name_23;
            PyObject *tmp_mvar_value_39;
            PyObject *tmp_subscript_name_23;
            PyObject *tmp_tuple_element_22;
            PyObject *tmp_list_element_4;
            PyObject *tmp_subscribed_name_24;
            PyObject *tmp_mvar_value_40;
            PyObject *tmp_subscript_name_24;
            PyObject *tmp_tuple_element_23;
            PyObject *tmp_list_element_5;
            PyObject *tmp_dict_key_61;
            PyObject *tmp_dict_value_61;
            PyObject *tmp_dict_key_62;
            PyObject *tmp_dict_value_62;
            PyObject *tmp_dict_key_63;
            PyObject *tmp_dict_value_63;
            PyObject *tmp_subscribed_name_25;
            PyObject *tmp_mvar_value_41;
            PyObject *tmp_subscript_name_25;
            PyObject *tmp_tuple_element_24;
            PyObject *tmp_subscribed_name_26;
            PyObject *tmp_mvar_value_42;
            PyObject *tmp_subscript_name_26;
            PyObject *tmp_tuple_element_25;
            PyObject *tmp_mvar_value_43;
            PyObject *tmp_source_name_15;
            PyObject *tmp_mvar_value_44;
            PyObject *tmp_dict_key_64;
            PyObject *tmp_dict_value_64;
            tmp_defaults_5 = const_tuple_d419ed77ca3e651e427938c507014e9e_tuple;
            tmp_dict_key_31 = const_str_plain_url;
            tmp_dict_value_31 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_31 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_31 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_31 );
                }
            }

            tmp_annotations_18 = _PyDict_NewPresized( 34 );
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_31, tmp_dict_value_31 );
            Py_DECREF( tmp_dict_value_31 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_32 = const_str_plain_method;
            tmp_dict_value_32 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_32 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_32 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_32 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_32, tmp_dict_value_32 );
            Py_DECREF( tmp_dict_value_32 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_33 = const_str_plain_headers;
            tmp_subscribed_name_16 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Union );

            if ( tmp_subscribed_name_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_29 == NULL ))
                {
                    tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_29 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 360;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_16 = tmp_mvar_value_29;
                Py_INCREF( tmp_subscribed_name_16 );
                }
            }

            tmp_subscribed_name_17 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Dict );

            if ( tmp_subscribed_name_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_30 == NULL ))
                {
                    tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_30 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 360;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_17 = tmp_mvar_value_30;
                Py_INCREF( tmp_subscribed_name_17 );
                }
            }

            tmp_tuple_element_16 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_tuple_element_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_16 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_16 );
                }
            }

            tmp_subscript_name_17 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_17, 0, tmp_tuple_element_16 );
            tmp_tuple_element_16 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_tuple_element_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_16 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_16 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_17, 1, tmp_tuple_element_16 );
            tmp_tuple_element_15 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_17, tmp_subscript_name_17 );
            Py_DECREF( tmp_subscribed_name_17 );
            Py_DECREF( tmp_subscript_name_17 );
            if ( tmp_tuple_element_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );
                Py_DECREF( tmp_subscribed_name_16 );

                exception_lineno = 360;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_subscript_name_16 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_16, 0, tmp_tuple_element_15 );
            tmp_source_name_13 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_httputil );

            if ( tmp_source_name_13 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

                if (unlikely( tmp_mvar_value_31 == NULL ))
                {
                    tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
                }

                if ( tmp_mvar_value_31 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_16 );
                    Py_DECREF( tmp_subscript_name_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 360;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_13 = tmp_mvar_value_31;
                Py_INCREF( tmp_source_name_13 );
                }
            }

            tmp_tuple_element_15 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_HTTPHeaders );
            Py_DECREF( tmp_source_name_13 );
            if ( tmp_tuple_element_15 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );
                Py_DECREF( tmp_subscribed_name_16 );
                Py_DECREF( tmp_subscript_name_16 );

                exception_lineno = 360;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_16, 1, tmp_tuple_element_15 );
            tmp_dict_value_33 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_16, tmp_subscript_name_16 );
            Py_DECREF( tmp_subscribed_name_16 );
            Py_DECREF( tmp_subscript_name_16 );
            if ( tmp_dict_value_33 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 360;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_33, tmp_dict_value_33 );
            Py_DECREF( tmp_dict_value_33 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_34 = const_str_plain_body;
            tmp_subscribed_name_18 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Union );

            if ( tmp_subscribed_name_18 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_32 == NULL ))
                {
                    tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_32 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 361;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_18 = tmp_mvar_value_32;
                Py_INCREF( tmp_subscribed_name_18 );
                }
            }

            tmp_tuple_element_17 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bytes );

            if ( tmp_tuple_element_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_17 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_tuple_element_17 );
                }
            }

            tmp_subscript_name_18 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_18, 0, tmp_tuple_element_17 );
            tmp_tuple_element_17 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_tuple_element_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_17 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_17 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_18, 1, tmp_tuple_element_17 );
            tmp_dict_value_34 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_18, tmp_subscript_name_18 );
            Py_DECREF( tmp_subscribed_name_18 );
            Py_DECREF( tmp_subscript_name_18 );
            if ( tmp_dict_value_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 361;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_34, tmp_dict_value_34 );
            Py_DECREF( tmp_dict_value_34 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_35 = const_str_plain_auth_username;
            tmp_dict_value_35 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_35 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_35 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_35 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_35, tmp_dict_value_35 );
            Py_DECREF( tmp_dict_value_35 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_36 = const_str_plain_auth_password;
            tmp_dict_value_36 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_36 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_36 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_36 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_36, tmp_dict_value_36 );
            Py_DECREF( tmp_dict_value_36 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_37 = const_str_plain_auth_mode;
            tmp_dict_value_37 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_37 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_37 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_37 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_37, tmp_dict_value_37 );
            Py_DECREF( tmp_dict_value_37 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_38 = const_str_plain_connect_timeout;
            tmp_dict_value_38 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_float );

            if ( tmp_dict_value_38 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_38 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_dict_value_38 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_38, tmp_dict_value_38 );
            Py_DECREF( tmp_dict_value_38 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_39 = const_str_plain_request_timeout;
            tmp_dict_value_39 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_float );

            if ( tmp_dict_value_39 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_39 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_dict_value_39 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_39, tmp_dict_value_39 );
            Py_DECREF( tmp_dict_value_39 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_40 = const_str_plain_if_modified_since;
            tmp_subscribed_name_19 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Union );

            if ( tmp_subscribed_name_19 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_33 == NULL ))
                {
                    tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_33 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 367;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_19 = tmp_mvar_value_33;
                Py_INCREF( tmp_subscribed_name_19 );
                }
            }

            tmp_tuple_element_18 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_float );

            if ( tmp_tuple_element_18 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_18 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_tuple_element_18 );
                }
            }

            tmp_subscript_name_19 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_19, 0, tmp_tuple_element_18 );
            tmp_source_name_14 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_datetime );

            if ( tmp_source_name_14 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_datetime );

                if (unlikely( tmp_mvar_value_34 == NULL ))
                {
                    tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_datetime );
                }

                if ( tmp_mvar_value_34 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_19 );
                    Py_DECREF( tmp_subscript_name_19 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "datetime" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 367;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_14 = tmp_mvar_value_34;
                Py_INCREF( tmp_source_name_14 );
                }
            }

            tmp_tuple_element_18 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_datetime );
            Py_DECREF( tmp_source_name_14 );
            if ( tmp_tuple_element_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );
                Py_DECREF( tmp_subscribed_name_19 );
                Py_DECREF( tmp_subscript_name_19 );

                exception_lineno = 367;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_19, 1, tmp_tuple_element_18 );
            tmp_dict_value_40 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_19, tmp_subscript_name_19 );
            Py_DECREF( tmp_subscribed_name_19 );
            Py_DECREF( tmp_subscript_name_19 );
            if ( tmp_dict_value_40 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 367;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_40, tmp_dict_value_40 );
            Py_DECREF( tmp_dict_value_40 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_41 = const_str_plain_follow_redirects;
            tmp_dict_value_41 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bool );

            if ( tmp_dict_value_41 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_41 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_41 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_41, tmp_dict_value_41 );
            Py_DECREF( tmp_dict_value_41 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_42 = const_str_plain_max_redirects;
            tmp_dict_value_42 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_int );

            if ( tmp_dict_value_42 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_42 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_42 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_42, tmp_dict_value_42 );
            Py_DECREF( tmp_dict_value_42 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_43 = const_str_plain_user_agent;
            tmp_dict_value_43 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_43 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_43 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_43 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_43, tmp_dict_value_43 );
            Py_DECREF( tmp_dict_value_43 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_44 = const_str_plain_use_gzip;
            tmp_dict_value_44 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bool );

            if ( tmp_dict_value_44 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_44 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_44 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_44, tmp_dict_value_44 );
            Py_DECREF( tmp_dict_value_44 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_45 = const_str_plain_network_interface;
            tmp_dict_value_45 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_45 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_45 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_45 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_45, tmp_dict_value_45 );
            Py_DECREF( tmp_dict_value_45 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_46 = const_str_plain_streaming_callback;
            tmp_subscribed_name_20 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Callable );

            if ( tmp_subscribed_name_20 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Callable );

                if (unlikely( tmp_mvar_value_35 == NULL ))
                {
                    tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Callable );
                }

                if ( tmp_mvar_value_35 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Callable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 373;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_20 = tmp_mvar_value_35;
                Py_INCREF( tmp_subscribed_name_20 );
                }
            }

            tmp_list_element_1 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bytes );

            if ( tmp_list_element_1 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_list_element_1 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_list_element_1 );
                }
            }

            tmp_tuple_element_19 = PyList_New( 1 );
            PyList_SET_ITEM( tmp_tuple_element_19, 0, tmp_list_element_1 );
            tmp_subscript_name_20 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_20, 0, tmp_tuple_element_19 );
            tmp_tuple_element_19 = Py_None;
            Py_INCREF( tmp_tuple_element_19 );
            PyTuple_SET_ITEM( tmp_subscript_name_20, 1, tmp_tuple_element_19 );
            tmp_dict_value_46 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_20, tmp_subscript_name_20 );
            Py_DECREF( tmp_subscribed_name_20 );
            Py_DECREF( tmp_subscript_name_20 );
            if ( tmp_dict_value_46 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 373;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_46, tmp_dict_value_46 );
            Py_DECREF( tmp_dict_value_46 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_47 = const_str_plain_header_callback;
            tmp_subscribed_name_21 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Callable );

            if ( tmp_subscribed_name_21 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Callable );

                if (unlikely( tmp_mvar_value_36 == NULL ))
                {
                    tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Callable );
                }

                if ( tmp_mvar_value_36 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Callable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 374;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_21 = tmp_mvar_value_36;
                Py_INCREF( tmp_subscribed_name_21 );
                }
            }

            tmp_list_element_2 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_list_element_2 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_list_element_2 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_list_element_2 );
                }
            }

            tmp_tuple_element_20 = PyList_New( 1 );
            PyList_SET_ITEM( tmp_tuple_element_20, 0, tmp_list_element_2 );
            tmp_subscript_name_21 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_21, 0, tmp_tuple_element_20 );
            tmp_tuple_element_20 = Py_None;
            Py_INCREF( tmp_tuple_element_20 );
            PyTuple_SET_ITEM( tmp_subscript_name_21, 1, tmp_tuple_element_20 );
            tmp_dict_value_47 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_21, tmp_subscript_name_21 );
            Py_DECREF( tmp_subscribed_name_21 );
            Py_DECREF( tmp_subscript_name_21 );
            if ( tmp_dict_value_47 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 374;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_47, tmp_dict_value_47 );
            Py_DECREF( tmp_dict_value_47 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_48 = const_str_plain_prepare_curl_callback;
            tmp_subscribed_name_22 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Callable );

            if ( tmp_subscribed_name_22 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Callable );

                if (unlikely( tmp_mvar_value_37 == NULL ))
                {
                    tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Callable );
                }

                if ( tmp_mvar_value_37 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Callable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 375;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_22 = tmp_mvar_value_37;
                Py_INCREF( tmp_subscribed_name_22 );
                }
            }

            tmp_list_element_3 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Any );

            if ( tmp_list_element_3 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_38 == NULL ))
                {
                    tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_38 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_22 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 375;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_list_element_3 = tmp_mvar_value_38;
                Py_INCREF( tmp_list_element_3 );
                }
            }

            tmp_tuple_element_21 = PyList_New( 1 );
            PyList_SET_ITEM( tmp_tuple_element_21, 0, tmp_list_element_3 );
            tmp_subscript_name_22 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_22, 0, tmp_tuple_element_21 );
            tmp_tuple_element_21 = Py_None;
            Py_INCREF( tmp_tuple_element_21 );
            PyTuple_SET_ITEM( tmp_subscript_name_22, 1, tmp_tuple_element_21 );
            tmp_dict_value_48 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_22, tmp_subscript_name_22 );
            Py_DECREF( tmp_subscribed_name_22 );
            Py_DECREF( tmp_subscript_name_22 );
            if ( tmp_dict_value_48 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 375;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_48, tmp_dict_value_48 );
            Py_DECREF( tmp_dict_value_48 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_49 = const_str_plain_proxy_host;
            tmp_dict_value_49 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_49 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_49 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_49 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_49, tmp_dict_value_49 );
            Py_DECREF( tmp_dict_value_49 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_50 = const_str_plain_proxy_port;
            tmp_dict_value_50 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_int );

            if ( tmp_dict_value_50 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_50 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_50 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_50, tmp_dict_value_50 );
            Py_DECREF( tmp_dict_value_50 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_51 = const_str_plain_proxy_username;
            tmp_dict_value_51 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_51 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_51 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_51 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_51, tmp_dict_value_51 );
            Py_DECREF( tmp_dict_value_51 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_52 = const_str_plain_proxy_password;
            tmp_dict_value_52 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_52 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_52 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_52 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_52, tmp_dict_value_52 );
            Py_DECREF( tmp_dict_value_52 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_53 = const_str_plain_proxy_auth_mode;
            tmp_dict_value_53 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_53 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_53 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_53 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_53, tmp_dict_value_53 );
            Py_DECREF( tmp_dict_value_53 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_54 = const_str_plain_allow_nonstandard_methods;
            tmp_dict_value_54 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bool );

            if ( tmp_dict_value_54 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_54 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_54 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_54, tmp_dict_value_54 );
            Py_DECREF( tmp_dict_value_54 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_55 = const_str_plain_validate_cert;
            tmp_dict_value_55 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bool );

            if ( tmp_dict_value_55 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_55 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_55 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_55, tmp_dict_value_55 );
            Py_DECREF( tmp_dict_value_55 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_56 = const_str_plain_ca_certs;
            tmp_dict_value_56 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_56 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_56 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_56 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_56, tmp_dict_value_56 );
            Py_DECREF( tmp_dict_value_56 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_57 = const_str_plain_allow_ipv6;
            tmp_dict_value_57 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bool );

            if ( tmp_dict_value_57 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_57 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_57 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_57, tmp_dict_value_57 );
            Py_DECREF( tmp_dict_value_57 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_58 = const_str_plain_client_key;
            tmp_dict_value_58 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_58 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_58 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_58 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_58, tmp_dict_value_58 );
            Py_DECREF( tmp_dict_value_58 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_59 = const_str_plain_client_cert;
            tmp_dict_value_59 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_dict_value_59 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_59 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_59 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_59, tmp_dict_value_59 );
            Py_DECREF( tmp_dict_value_59 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_60 = const_str_plain_body_producer;
            tmp_subscribed_name_23 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Callable );

            if ( tmp_subscribed_name_23 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Callable );

                if (unlikely( tmp_mvar_value_39 == NULL ))
                {
                    tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Callable );
                }

                if ( tmp_mvar_value_39 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Callable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 387;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_23 = tmp_mvar_value_39;
                Py_INCREF( tmp_subscribed_name_23 );
                }
            }

            tmp_subscribed_name_24 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Callable );

            if ( tmp_subscribed_name_24 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Callable );

                if (unlikely( tmp_mvar_value_40 == NULL ))
                {
                    tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Callable );
                }

                if ( tmp_mvar_value_40 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_23 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Callable" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 387;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_24 = tmp_mvar_value_40;
                Py_INCREF( tmp_subscribed_name_24 );
                }
            }

            tmp_list_element_5 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bytes );

            if ( tmp_list_element_5 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_list_element_5 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_list_element_5 );
                }
            }

            tmp_tuple_element_23 = PyList_New( 1 );
            PyList_SET_ITEM( tmp_tuple_element_23, 0, tmp_list_element_5 );
            tmp_subscript_name_24 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_24, 0, tmp_tuple_element_23 );
            tmp_tuple_element_23 = Py_None;
            Py_INCREF( tmp_tuple_element_23 );
            PyTuple_SET_ITEM( tmp_subscript_name_24, 1, tmp_tuple_element_23 );
            tmp_list_element_4 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_24, tmp_subscript_name_24 );
            Py_DECREF( tmp_subscribed_name_24 );
            Py_DECREF( tmp_subscript_name_24 );
            if ( tmp_list_element_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );
                Py_DECREF( tmp_subscribed_name_23 );

                exception_lineno = 387;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_tuple_element_22 = PyList_New( 1 );
            PyList_SET_ITEM( tmp_tuple_element_22, 0, tmp_list_element_4 );
            tmp_subscript_name_23 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_23, 0, tmp_tuple_element_22 );
            tmp_tuple_element_22 = const_str_digest_1257ef3e7457bac5b76db6123b3c6cc2;
            Py_INCREF( tmp_tuple_element_22 );
            PyTuple_SET_ITEM( tmp_subscript_name_23, 1, tmp_tuple_element_22 );
            tmp_dict_value_60 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_23, tmp_subscript_name_23 );
            Py_DECREF( tmp_subscribed_name_23 );
            Py_DECREF( tmp_subscript_name_23 );
            if ( tmp_dict_value_60 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 387;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_60, tmp_dict_value_60 );
            Py_DECREF( tmp_dict_value_60 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_61 = const_str_plain_expect_100_continue;
            tmp_dict_value_61 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bool );

            if ( tmp_dict_value_61 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_61 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_61 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_61, tmp_dict_value_61 );
            Py_DECREF( tmp_dict_value_61 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_62 = const_str_plain_decompress_response;
            tmp_dict_value_62 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bool );

            if ( tmp_dict_value_62 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_62 = (PyObject *)&PyBool_Type;
                Py_INCREF( tmp_dict_value_62 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_62, tmp_dict_value_62 );
            Py_DECREF( tmp_dict_value_62 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_63 = const_str_plain_ssl_options;
            tmp_subscribed_name_25 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Union );

            if ( tmp_subscribed_name_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_41 == NULL ))
                {
                    tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_41 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 390;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_25 = tmp_mvar_value_41;
                Py_INCREF( tmp_subscribed_name_25 );
                }
            }

            tmp_subscribed_name_26 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Dict );

            if ( tmp_subscribed_name_26 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_42 == NULL ))
                {
                    tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_42 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_25 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 390;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_26 = tmp_mvar_value_42;
                Py_INCREF( tmp_subscribed_name_26 );
                }
            }

            tmp_tuple_element_25 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_tuple_element_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_25 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_25 );
                }
            }

            tmp_subscript_name_26 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_26, 0, tmp_tuple_element_25 );
            tmp_tuple_element_25 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Any );

            if ( tmp_tuple_element_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_43 == NULL ))
                {
                    tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_43 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_25 );
                    Py_DECREF( tmp_subscribed_name_26 );
                    Py_DECREF( tmp_subscript_name_26 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 390;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_tuple_element_25 = tmp_mvar_value_43;
                Py_INCREF( tmp_tuple_element_25 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_26, 1, tmp_tuple_element_25 );
            tmp_tuple_element_24 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_26, tmp_subscript_name_26 );
            Py_DECREF( tmp_subscribed_name_26 );
            Py_DECREF( tmp_subscript_name_26 );
            if ( tmp_tuple_element_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );
                Py_DECREF( tmp_subscribed_name_25 );

                exception_lineno = 390;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_subscript_name_25 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_25, 0, tmp_tuple_element_24 );
            tmp_source_name_15 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_ssl );

            if ( tmp_source_name_15 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_ssl );

                if (unlikely( tmp_mvar_value_44 == NULL ))
                {
                    tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_ssl );
                }

                if ( tmp_mvar_value_44 == NULL )
                {
                    Py_DECREF( tmp_annotations_18 );
                    Py_DECREF( tmp_subscribed_name_25 );
                    Py_DECREF( tmp_subscript_name_25 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "ssl" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 390;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_15 = tmp_mvar_value_44;
                Py_INCREF( tmp_source_name_15 );
                }
            }

            tmp_tuple_element_24 = LOOKUP_ATTRIBUTE( tmp_source_name_15, const_str_plain_SSLContext );
            Py_DECREF( tmp_source_name_15 );
            if ( tmp_tuple_element_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );
                Py_DECREF( tmp_subscribed_name_25 );
                Py_DECREF( tmp_subscript_name_25 );

                exception_lineno = 390;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_25, 1, tmp_tuple_element_24 );
            tmp_dict_value_63 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_25, tmp_subscript_name_25 );
            Py_DECREF( tmp_subscribed_name_25 );
            Py_DECREF( tmp_subscript_name_25 );
            if ( tmp_dict_value_63 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_18 );

                exception_lineno = 390;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_63, tmp_dict_value_63 );
            Py_DECREF( tmp_dict_value_63 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_64 = const_str_plain_return;
            tmp_dict_value_64 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_18, tmp_dict_key_64, tmp_dict_value_64 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_5 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_14___init__( tmp_defaults_5, tmp_annotations_18 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 356;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            nuitka_bool tmp_condition_result_23;
            PyObject *tmp_called_name_11;
            PyObject *tmp_args_element_name_5;
            PyObject *tmp_annotations_19;
            PyObject *tmp_dict_key_65;
            PyObject *tmp_dict_value_65;
            PyObject *tmp_source_name_16;
            PyObject *tmp_mvar_value_45;
            PyObject *tmp_called_name_12;
            PyObject *tmp_args_element_name_6;
            PyObject *tmp_annotations_20;
            PyObject *tmp_dict_key_66;
            PyObject *tmp_dict_value_66;
            PyObject *tmp_source_name_17;
            PyObject *tmp_mvar_value_46;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$httpclient_337, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 545;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_23 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_23 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_12;
            }
            else
            {
                goto condexpr_false_12;
            }
            condexpr_true_12:;
            tmp_called_name_11 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_property );

            if ( tmp_called_name_11 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 545;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 545;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_dict_key_65 = const_str_plain_return;
            tmp_source_name_16 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_httputil );

            if ( tmp_source_name_16 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

                if (unlikely( tmp_mvar_value_45 == NULL ))
                {
                    tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
                }

                if ( tmp_mvar_value_45 == NULL )
                {
                    Py_DECREF( tmp_called_name_11 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 546;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_16 = tmp_mvar_value_45;
                Py_INCREF( tmp_source_name_16 );
                }
            }

            tmp_dict_value_65 = LOOKUP_ATTRIBUTE( tmp_source_name_16, const_str_plain_HTTPHeaders );
            Py_DECREF( tmp_source_name_16 );
            if ( tmp_dict_value_65 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_11 );

                exception_lineno = 546;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_annotations_19 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_19, tmp_dict_key_65, tmp_dict_value_65 );
            Py_DECREF( tmp_dict_value_65 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_5 = MAKE_FUNCTION_tornado$httpclient$$$function_15_headers( tmp_annotations_19 );



            frame_3c585940a00c16cc3fd3083c84c8ac0a_4->m_frame.f_lineno = 545;
            {
                PyObject *call_args[] = { tmp_args_element_name_5 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
            }

            Py_DECREF( tmp_called_name_11 );
            Py_DECREF( tmp_args_element_name_5 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 545;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_12;
            condexpr_false_12:;
            tmp_called_name_12 = (PyObject *)&PyProperty_Type;
            tmp_dict_key_66 = const_str_plain_return;
            tmp_source_name_17 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_httputil );

            if ( tmp_source_name_17 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

                if (unlikely( tmp_mvar_value_46 == NULL ))
                {
                    tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
                }

                if ( tmp_mvar_value_46 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 546;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_17 = tmp_mvar_value_46;
                Py_INCREF( tmp_source_name_17 );
                }
            }

            tmp_dict_value_66 = LOOKUP_ATTRIBUTE( tmp_source_name_17, const_str_plain_HTTPHeaders );
            Py_DECREF( tmp_source_name_17 );
            if ( tmp_dict_value_66 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 546;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_annotations_20 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_20, tmp_dict_key_66, tmp_dict_value_66 );
            Py_DECREF( tmp_dict_value_66 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_6 = MAKE_FUNCTION_tornado$httpclient$$$function_15_headers( tmp_annotations_20 );



            frame_3c585940a00c16cc3fd3083c84c8ac0a_4->m_frame.f_lineno = 545;
            {
                PyObject *call_args[] = { tmp_args_element_name_6 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
            }

            Py_DECREF( tmp_args_element_name_6 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 545;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_12:;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain_headers, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 545;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            PyObject *tmp_called_name_13;
            PyObject *tmp_source_name_18;
            PyObject *tmp_args_element_name_7;
            PyObject *tmp_annotations_21;
            PyObject *tmp_dict_key_67;
            PyObject *tmp_dict_value_67;
            PyObject *tmp_subscribed_name_27;
            PyObject *tmp_mvar_value_47;
            PyObject *tmp_subscript_name_27;
            PyObject *tmp_tuple_element_26;
            PyObject *tmp_subscribed_name_28;
            PyObject *tmp_mvar_value_48;
            PyObject *tmp_subscript_name_28;
            PyObject *tmp_tuple_element_27;
            PyObject *tmp_source_name_19;
            PyObject *tmp_mvar_value_49;
            PyObject *tmp_dict_key_68;
            PyObject *tmp_dict_value_68;
            tmp_source_name_18 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_headers );

            if ( tmp_source_name_18 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "headers" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 552;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_source_name_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 552;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_called_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_18, const_str_plain_setter );
            Py_DECREF( tmp_source_name_18 );
            if ( tmp_called_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 552;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_dict_key_67 = const_str_plain_value;
            tmp_subscribed_name_27 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Union );

            if ( tmp_subscribed_name_27 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_47 == NULL ))
                {
                    tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_47 == NULL )
                {
                    Py_DECREF( tmp_called_name_13 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 553;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_27 = tmp_mvar_value_47;
                Py_INCREF( tmp_subscribed_name_27 );
                }
            }

            tmp_subscribed_name_28 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Dict );

            if ( tmp_subscribed_name_28 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_48 == NULL ))
                {
                    tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_48 == NULL )
                {
                    Py_DECREF( tmp_called_name_13 );
                    Py_DECREF( tmp_subscribed_name_27 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 553;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_28 = tmp_mvar_value_48;
                Py_INCREF( tmp_subscribed_name_28 );
                }
            }

            tmp_tuple_element_27 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_tuple_element_27 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_27 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_27 );
                }
            }

            tmp_subscript_name_28 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_28, 0, tmp_tuple_element_27 );
            tmp_tuple_element_27 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_tuple_element_27 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_27 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_27 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_28, 1, tmp_tuple_element_27 );
            tmp_tuple_element_26 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_28, tmp_subscript_name_28 );
            Py_DECREF( tmp_subscribed_name_28 );
            Py_DECREF( tmp_subscript_name_28 );
            if ( tmp_tuple_element_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_13 );
                Py_DECREF( tmp_subscribed_name_27 );

                exception_lineno = 553;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_subscript_name_27 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_27, 0, tmp_tuple_element_26 );
            tmp_source_name_19 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_httputil );

            if ( tmp_source_name_19 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

                if (unlikely( tmp_mvar_value_49 == NULL ))
                {
                    tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
                }

                if ( tmp_mvar_value_49 == NULL )
                {
                    Py_DECREF( tmp_called_name_13 );
                    Py_DECREF( tmp_subscribed_name_27 );
                    Py_DECREF( tmp_subscript_name_27 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 553;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_source_name_19 = tmp_mvar_value_49;
                Py_INCREF( tmp_source_name_19 );
                }
            }

            tmp_tuple_element_26 = LOOKUP_ATTRIBUTE( tmp_source_name_19, const_str_plain_HTTPHeaders );
            Py_DECREF( tmp_source_name_19 );
            if ( tmp_tuple_element_26 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_13 );
                Py_DECREF( tmp_subscribed_name_27 );
                Py_DECREF( tmp_subscript_name_27 );

                exception_lineno = 553;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            PyTuple_SET_ITEM( tmp_subscript_name_27, 1, tmp_tuple_element_26 );
            tmp_dict_value_67 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_27, tmp_subscript_name_27 );
            Py_DECREF( tmp_subscribed_name_27 );
            Py_DECREF( tmp_subscript_name_27 );
            if ( tmp_dict_value_67 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_13 );

                exception_lineno = 553;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_annotations_21 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_21, tmp_dict_key_67, tmp_dict_value_67 );
            Py_DECREF( tmp_dict_value_67 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_68 = const_str_plain_return;
            tmp_dict_value_68 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_21, tmp_dict_key_68, tmp_dict_value_68 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_7 = MAKE_FUNCTION_tornado$httpclient$$$function_16_headers( tmp_annotations_21 );



            frame_3c585940a00c16cc3fd3083c84c8ac0a_4->m_frame.f_lineno = 552;
            {
                PyObject *call_args[] = { tmp_args_element_name_7 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
            }

            Py_DECREF( tmp_called_name_13 );
            Py_DECREF( tmp_args_element_name_7 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 552;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain_headers, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 552;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            nuitka_bool tmp_condition_result_24;
            PyObject *tmp_called_name_14;
            PyObject *tmp_args_element_name_8;
            PyObject *tmp_annotations_22;
            PyObject *tmp_dict_key_69;
            PyObject *tmp_dict_value_69;
            PyObject *tmp_called_name_15;
            PyObject *tmp_args_element_name_9;
            PyObject *tmp_annotations_23;
            PyObject *tmp_dict_key_70;
            PyObject *tmp_dict_value_70;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$httpclient_337, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 559;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_condition_result_24 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_24 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_13;
            }
            else
            {
                goto condexpr_false_13;
            }
            condexpr_true_13:;
            tmp_called_name_14 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_property );

            if ( tmp_called_name_14 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 559;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_called_name_14 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 559;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_dict_key_69 = const_str_plain_return;
            tmp_dict_value_69 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bytes );

            if ( tmp_dict_value_69 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_69 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_dict_value_69 );
                }
            }

            tmp_annotations_22 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_22, tmp_dict_key_69, tmp_dict_value_69 );
            Py_DECREF( tmp_dict_value_69 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_8 = MAKE_FUNCTION_tornado$httpclient$$$function_17_body( tmp_annotations_22 );



            frame_3c585940a00c16cc3fd3083c84c8ac0a_4->m_frame.f_lineno = 559;
            {
                PyObject *call_args[] = { tmp_args_element_name_8 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
            }

            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_args_element_name_8 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 559;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            goto condexpr_end_13;
            condexpr_false_13:;
            tmp_called_name_15 = (PyObject *)&PyProperty_Type;
            tmp_dict_key_70 = const_str_plain_return;
            tmp_dict_value_70 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bytes );

            if ( tmp_dict_value_70 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_70 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_dict_value_70 );
                }
            }

            tmp_annotations_23 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_23, tmp_dict_key_70, tmp_dict_value_70 );
            Py_DECREF( tmp_dict_value_70 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_9 = MAKE_FUNCTION_tornado$httpclient$$$function_17_body( tmp_annotations_23 );



            frame_3c585940a00c16cc3fd3083c84c8ac0a_4->m_frame.f_lineno = 559;
            {
                PyObject *call_args[] = { tmp_args_element_name_9 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
            }

            Py_DECREF( tmp_args_element_name_9 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 559;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            condexpr_end_13:;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain_body, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 559;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }
        {
            PyObject *tmp_called_name_16;
            PyObject *tmp_source_name_20;
            PyObject *tmp_args_element_name_10;
            PyObject *tmp_annotations_24;
            PyObject *tmp_dict_key_71;
            PyObject *tmp_dict_value_71;
            PyObject *tmp_subscribed_name_29;
            PyObject *tmp_mvar_value_50;
            PyObject *tmp_subscript_name_29;
            PyObject *tmp_tuple_element_28;
            PyObject *tmp_dict_key_72;
            PyObject *tmp_dict_value_72;
            tmp_source_name_20 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_body );

            if ( tmp_source_name_20 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "body" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 563;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }

            if ( tmp_source_name_20 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 563;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_called_name_16 = LOOKUP_ATTRIBUTE( tmp_source_name_20, const_str_plain_setter );
            Py_DECREF( tmp_source_name_20 );
            if ( tmp_called_name_16 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 563;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_dict_key_71 = const_str_plain_value;
            tmp_subscribed_name_29 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_Union );

            if ( tmp_subscribed_name_29 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Union );

                if (unlikely( tmp_mvar_value_50 == NULL ))
                {
                    tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Union );
                }

                if ( tmp_mvar_value_50 == NULL )
                {
                    Py_DECREF( tmp_called_name_16 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Union" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 564;
                    type_description_2 = "o";
                    goto frame_exception_exit_4;
                }

                tmp_subscribed_name_29 = tmp_mvar_value_50;
                Py_INCREF( tmp_subscribed_name_29 );
                }
            }

            tmp_tuple_element_28 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_bytes );

            if ( tmp_tuple_element_28 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_28 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_tuple_element_28 );
                }
            }

            tmp_subscript_name_29 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_29, 0, tmp_tuple_element_28 );
            tmp_tuple_element_28 = PyObject_GetItem( locals_tornado$httpclient_337, const_str_plain_str );

            if ( tmp_tuple_element_28 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_28 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_28 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_29, 1, tmp_tuple_element_28 );
            tmp_dict_value_71 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_29, tmp_subscript_name_29 );
            Py_DECREF( tmp_subscribed_name_29 );
            Py_DECREF( tmp_subscript_name_29 );
            if ( tmp_dict_value_71 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_16 );

                exception_lineno = 564;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_annotations_24 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_24, tmp_dict_key_71, tmp_dict_value_71 );
            Py_DECREF( tmp_dict_value_71 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_72 = const_str_plain_return;
            tmp_dict_value_72 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_24, tmp_dict_key_72, tmp_dict_value_72 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_10 = MAKE_FUNCTION_tornado$httpclient$$$function_18_body( tmp_annotations_24 );



            frame_3c585940a00c16cc3fd3083c84c8ac0a_4->m_frame.f_lineno = 563;
            {
                PyObject *call_args[] = { tmp_args_element_name_10 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
            }

            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_args_element_name_10 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 563;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain_body, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 563;
                type_description_2 = "o";
                goto frame_exception_exit_4;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3c585940a00c16cc3fd3083c84c8ac0a_4 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_3;

        frame_exception_exit_4:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_3c585940a00c16cc3fd3083c84c8ac0a_4 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_3c585940a00c16cc3fd3083c84c8ac0a_4, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_3c585940a00c16cc3fd3083c84c8ac0a_4->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_3c585940a00c16cc3fd3083c84c8ac0a_4, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_3c585940a00c16cc3fd3083c84c8ac0a_4,
            type_description_2,
            outline_2_var___class__
        );


        // Release cached frame.
        if ( frame_3c585940a00c16cc3fd3083c84c8ac0a_4 == cache_frame_3c585940a00c16cc3fd3083c84c8ac0a_4 )
        {
            Py_DECREF( frame_3c585940a00c16cc3fd3083c84c8ac0a_4 );
        }
        cache_frame_3c585940a00c16cc3fd3083c84c8ac0a_4 = NULL;

        assertFrameObject( frame_3c585940a00c16cc3fd3083c84c8ac0a_4 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_3;

        frame_no_exception_3:;
        goto skip_nested_handling_3;
        nested_frame_exit_3:;

        goto try_except_handler_13;
        skip_nested_handling_3:;
        {
            nuitka_bool tmp_condition_result_25;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_compexpr_left_3 = tmp_class_creation_3__bases;
            tmp_compexpr_right_3 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 337;

                goto try_except_handler_13;
            }
            tmp_condition_result_25 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_25 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_12;
            }
            else
            {
                goto branch_no_12;
            }
            branch_yes_12:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_337, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 337;

                goto try_except_handler_13;
            }
            branch_no_12:;
        }
        {
            PyObject *tmp_assign_source_52;
            PyObject *tmp_called_name_17;
            PyObject *tmp_args_name_6;
            PyObject *tmp_tuple_element_29;
            PyObject *tmp_kw_name_7;
            CHECK_OBJECT( tmp_class_creation_3__metaclass );
            tmp_called_name_17 = tmp_class_creation_3__metaclass;
            tmp_tuple_element_29 = const_str_plain_HTTPRequest;
            tmp_args_name_6 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_29 );
            PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_29 );
            CHECK_OBJECT( tmp_class_creation_3__bases );
            tmp_tuple_element_29 = tmp_class_creation_3__bases;
            Py_INCREF( tmp_tuple_element_29 );
            PyTuple_SET_ITEM( tmp_args_name_6, 1, tmp_tuple_element_29 );
            tmp_tuple_element_29 = locals_tornado$httpclient_337;
            Py_INCREF( tmp_tuple_element_29 );
            PyTuple_SET_ITEM( tmp_args_name_6, 2, tmp_tuple_element_29 );
            CHECK_OBJECT( tmp_class_creation_3__class_decl_dict );
            tmp_kw_name_7 = tmp_class_creation_3__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 337;
            tmp_assign_source_52 = CALL_FUNCTION( tmp_called_name_17, tmp_args_name_6, tmp_kw_name_7 );
            Py_DECREF( tmp_args_name_6 );
            if ( tmp_assign_source_52 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 337;

                goto try_except_handler_13;
            }
            assert( outline_2_var___class__ == NULL );
            outline_2_var___class__ = tmp_assign_source_52;
        }
        CHECK_OBJECT( outline_2_var___class__ );
        tmp_assign_source_51 = outline_2_var___class__;
        Py_INCREF( tmp_assign_source_51 );
        goto try_return_handler_13;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_13:;
        Py_DECREF( locals_tornado$httpclient_337 );
        locals_tornado$httpclient_337 = NULL;
        goto try_return_handler_12;
        // Exception handler code:
        try_except_handler_13:;
        exception_keeper_type_11 = exception_type;
        exception_keeper_value_11 = exception_value;
        exception_keeper_tb_11 = exception_tb;
        exception_keeper_lineno_11 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$httpclient_337 );
        locals_tornado$httpclient_337 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_11;
        exception_value = exception_keeper_value_11;
        exception_tb = exception_keeper_tb_11;
        exception_lineno = exception_keeper_lineno_11;

        goto try_except_handler_12;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_12:;
        CHECK_OBJECT( (PyObject *)outline_2_var___class__ );
        Py_DECREF( outline_2_var___class__ );
        outline_2_var___class__ = NULL;

        goto outline_result_3;
        // Exception handler code:
        try_except_handler_12:;
        exception_keeper_type_12 = exception_type;
        exception_keeper_value_12 = exception_value;
        exception_keeper_tb_12 = exception_tb;
        exception_keeper_lineno_12 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_12;
        exception_value = exception_keeper_value_12;
        exception_tb = exception_keeper_tb_12;
        exception_lineno = exception_keeper_lineno_12;

        goto outline_exception_3;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_3:;
        exception_lineno = 337;
        goto try_except_handler_11;
        outline_result_3:;
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPRequest, tmp_assign_source_51 );
    }
    goto try_end_7;
    // Exception handler code:
    try_except_handler_11:;
    exception_keeper_type_13 = exception_type;
    exception_keeper_value_13 = exception_value;
    exception_keeper_tb_13 = exception_tb;
    exception_keeper_lineno_13 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    Py_XDECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_13;
    exception_value = exception_keeper_value_13;
    exception_tb = exception_keeper_tb_13;
    exception_lineno = exception_keeper_lineno_13;

    goto frame_exception_exit_1;
    // End of try:
    try_end_7:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__bases );
    Py_DECREF( tmp_class_creation_3__bases );
    tmp_class_creation_3__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__class_decl_dict );
    Py_DECREF( tmp_class_creation_3__class_decl_dict );
    tmp_class_creation_3__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__metaclass );
    Py_DECREF( tmp_class_creation_3__metaclass );
    tmp_class_creation_3__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_3__prepared );
    Py_DECREF( tmp_class_creation_3__prepared );
    tmp_class_creation_3__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_53;
        PyObject *tmp_dircall_arg1_4;
        tmp_dircall_arg1_4 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_4 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_4};
            tmp_assign_source_53 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_14;
        }
        assert( tmp_class_creation_4__bases == NULL );
        tmp_class_creation_4__bases = tmp_assign_source_53;
    }
    {
        PyObject *tmp_assign_source_54;
        tmp_assign_source_54 = PyDict_New();
        assert( tmp_class_creation_4__class_decl_dict == NULL );
        tmp_class_creation_4__class_decl_dict = tmp_assign_source_54;
    }
    {
        PyObject *tmp_assign_source_55;
        PyObject *tmp_metaclass_name_4;
        nuitka_bool tmp_condition_result_26;
        PyObject *tmp_key_name_10;
        PyObject *tmp_dict_name_10;
        PyObject *tmp_dict_name_11;
        PyObject *tmp_key_name_11;
        nuitka_bool tmp_condition_result_27;
        int tmp_truth_name_4;
        PyObject *tmp_type_arg_7;
        PyObject *tmp_subscribed_name_30;
        PyObject *tmp_subscript_name_30;
        PyObject *tmp_bases_name_4;
        tmp_key_name_10 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_10 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_10, tmp_key_name_10 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_14;
        }
        tmp_condition_result_26 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_26 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_14;
        }
        else
        {
            goto condexpr_false_14;
        }
        condexpr_true_14:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_11 = tmp_class_creation_4__class_decl_dict;
        tmp_key_name_11 = const_str_plain_metaclass;
        tmp_metaclass_name_4 = DICT_GET_ITEM( tmp_dict_name_11, tmp_key_name_11 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_14;
        }
        goto condexpr_end_14;
        condexpr_false_14:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_truth_name_4 = CHECK_IF_TRUE( tmp_class_creation_4__bases );
        if ( tmp_truth_name_4 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_14;
        }
        tmp_condition_result_27 = tmp_truth_name_4 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_27 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_15;
        }
        else
        {
            goto condexpr_false_15;
        }
        condexpr_true_15:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_subscribed_name_30 = tmp_class_creation_4__bases;
        tmp_subscript_name_30 = const_int_0;
        tmp_type_arg_7 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_30, tmp_subscript_name_30, 0 );
        if ( tmp_type_arg_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_14;
        }
        tmp_metaclass_name_4 = BUILTIN_TYPE1( tmp_type_arg_7 );
        Py_DECREF( tmp_type_arg_7 );
        if ( tmp_metaclass_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_14;
        }
        goto condexpr_end_15;
        condexpr_false_15:;
        tmp_metaclass_name_4 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_4 );
        condexpr_end_15:;
        condexpr_end_14:;
        CHECK_OBJECT( tmp_class_creation_4__bases );
        tmp_bases_name_4 = tmp_class_creation_4__bases;
        tmp_assign_source_55 = SELECT_METACLASS( tmp_metaclass_name_4, tmp_bases_name_4 );
        Py_DECREF( tmp_metaclass_name_4 );
        if ( tmp_assign_source_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_14;
        }
        assert( tmp_class_creation_4__metaclass == NULL );
        tmp_class_creation_4__metaclass = tmp_assign_source_55;
    }
    {
        nuitka_bool tmp_condition_result_28;
        PyObject *tmp_key_name_12;
        PyObject *tmp_dict_name_12;
        tmp_key_name_12 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dict_name_12 = tmp_class_creation_4__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_12, tmp_key_name_12 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_14;
        }
        tmp_condition_result_28 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_28 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_13;
        }
        else
        {
            goto branch_no_13;
        }
        branch_yes_13:;
        CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_4__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_14;
        }
        branch_no_13:;
    }
    {
        nuitka_bool tmp_condition_result_29;
        PyObject *tmp_source_name_21;
        CHECK_OBJECT( tmp_class_creation_4__metaclass );
        tmp_source_name_21 = tmp_class_creation_4__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_21, const_str_plain___prepare__ );
        tmp_condition_result_29 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_29 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_14;
        }
        else
        {
            goto branch_no_14;
        }
        branch_yes_14:;
        {
            PyObject *tmp_assign_source_56;
            PyObject *tmp_called_name_18;
            PyObject *tmp_source_name_22;
            PyObject *tmp_args_name_7;
            PyObject *tmp_tuple_element_30;
            PyObject *tmp_kw_name_8;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_source_name_22 = tmp_class_creation_4__metaclass;
            tmp_called_name_18 = LOOKUP_ATTRIBUTE( tmp_source_name_22, const_str_plain___prepare__ );
            if ( tmp_called_name_18 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;

                goto try_except_handler_14;
            }
            tmp_tuple_element_30 = const_str_plain_HTTPResponse;
            tmp_args_name_7 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_30 );
            PyTuple_SET_ITEM( tmp_args_name_7, 0, tmp_tuple_element_30 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_30 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_30 );
            PyTuple_SET_ITEM( tmp_args_name_7, 1, tmp_tuple_element_30 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_8 = tmp_class_creation_4__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 568;
            tmp_assign_source_56 = CALL_FUNCTION( tmp_called_name_18, tmp_args_name_7, tmp_kw_name_8 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_args_name_7 );
            if ( tmp_assign_source_56 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;

                goto try_except_handler_14;
            }
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_56;
        }
        {
            nuitka_bool tmp_condition_result_30;
            PyObject *tmp_operand_name_4;
            PyObject *tmp_source_name_23;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_source_name_23 = tmp_class_creation_4__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_23, const_str_plain___getitem__ );
            tmp_operand_name_4 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;

                goto try_except_handler_14;
            }
            tmp_condition_result_30 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_30 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_15;
            }
            else
            {
                goto branch_no_15;
            }
            branch_yes_15:;
            {
                PyObject *tmp_raise_type_4;
                PyObject *tmp_raise_value_4;
                PyObject *tmp_left_name_4;
                PyObject *tmp_right_name_4;
                PyObject *tmp_tuple_element_31;
                PyObject *tmp_getattr_target_4;
                PyObject *tmp_getattr_attr_4;
                PyObject *tmp_getattr_default_4;
                PyObject *tmp_source_name_24;
                PyObject *tmp_type_arg_8;
                tmp_raise_type_4 = PyExc_TypeError;
                tmp_left_name_4 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_4__metaclass );
                tmp_getattr_target_4 = tmp_class_creation_4__metaclass;
                tmp_getattr_attr_4 = const_str_plain___name__;
                tmp_getattr_default_4 = const_str_angle_metaclass;
                tmp_tuple_element_31 = BUILTIN_GETATTR( tmp_getattr_target_4, tmp_getattr_attr_4, tmp_getattr_default_4 );
                if ( tmp_tuple_element_31 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 568;

                    goto try_except_handler_14;
                }
                tmp_right_name_4 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_4, 0, tmp_tuple_element_31 );
                CHECK_OBJECT( tmp_class_creation_4__prepared );
                tmp_type_arg_8 = tmp_class_creation_4__prepared;
                tmp_source_name_24 = BUILTIN_TYPE1( tmp_type_arg_8 );
                assert( !(tmp_source_name_24 == NULL) );
                tmp_tuple_element_31 = LOOKUP_ATTRIBUTE( tmp_source_name_24, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_24 );
                if ( tmp_tuple_element_31 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_4 );

                    exception_lineno = 568;

                    goto try_except_handler_14;
                }
                PyTuple_SET_ITEM( tmp_right_name_4, 1, tmp_tuple_element_31 );
                tmp_raise_value_4 = BINARY_OPERATION_REMAINDER( tmp_left_name_4, tmp_right_name_4 );
                Py_DECREF( tmp_right_name_4 );
                if ( tmp_raise_value_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 568;

                    goto try_except_handler_14;
                }
                exception_type = tmp_raise_type_4;
                Py_INCREF( tmp_raise_type_4 );
                exception_value = tmp_raise_value_4;
                exception_lineno = 568;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_14;
            }
            branch_no_15:;
        }
        goto branch_end_14;
        branch_no_14:;
        {
            PyObject *tmp_assign_source_57;
            tmp_assign_source_57 = PyDict_New();
            assert( tmp_class_creation_4__prepared == NULL );
            tmp_class_creation_4__prepared = tmp_assign_source_57;
        }
        branch_end_14:;
    }
    {
        PyObject *tmp_assign_source_58;
        {
            PyObject *tmp_set_locals_4;
            CHECK_OBJECT( tmp_class_creation_4__prepared );
            tmp_set_locals_4 = tmp_class_creation_4__prepared;
            locals_tornado$httpclient_568 = tmp_set_locals_4;
            Py_INCREF( tmp_set_locals_4 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_6f55736fcd57507504cc7f0643fbf21f;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_16;
        }
        tmp_dictset_value = const_str_digest_fa43d8078d8197a0b1775f650130ce1f;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_16;
        }
        tmp_dictset_value = const_str_plain_HTTPResponse;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 568;

            goto try_except_handler_16;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_ed5be14316d8a2e2facc323ac7ebc012_5, codeobj_ed5be14316d8a2e2facc323ac7ebc012, module_tornado$httpclient, sizeof(void *) );
        frame_ed5be14316d8a2e2facc323ac7ebc012_5 = cache_frame_ed5be14316d8a2e2facc323ac7ebc012_5;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_ed5be14316d8a2e2facc323ac7ebc012_5 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_ed5be14316d8a2e2facc323ac7ebc012_5 ) == 2 ); // Frame stack

        // Framed code:
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain_error, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 619;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }
        tmp_dictset_value = Py_False;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain__error_is_response_code, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 620;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }
        tmp_dictset_value = Py_None;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain_request, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 621;
            type_description_2 = "o";
            goto frame_exception_exit_5;
        }
        {
            PyObject *tmp_defaults_6;
            PyObject *tmp_annotations_25;
            PyObject *tmp_dict_key_73;
            PyObject *tmp_dict_value_73;
            PyObject *tmp_mvar_value_51;
            PyObject *tmp_dict_key_74;
            PyObject *tmp_dict_value_74;
            PyObject *tmp_dict_key_75;
            PyObject *tmp_dict_value_75;
            PyObject *tmp_source_name_25;
            PyObject *tmp_mvar_value_52;
            PyObject *tmp_dict_key_76;
            PyObject *tmp_dict_value_76;
            PyObject *tmp_mvar_value_53;
            PyObject *tmp_dict_key_77;
            PyObject *tmp_dict_value_77;
            PyObject *tmp_dict_key_78;
            PyObject *tmp_dict_value_78;
            PyObject *tmp_dict_key_79;
            PyObject *tmp_dict_value_79;
            PyObject *tmp_dict_key_80;
            PyObject *tmp_dict_value_80;
            PyObject *tmp_subscribed_name_31;
            PyObject *tmp_mvar_value_54;
            PyObject *tmp_subscript_name_31;
            PyObject *tmp_tuple_element_32;
            PyObject *tmp_dict_key_81;
            PyObject *tmp_dict_value_81;
            PyObject *tmp_dict_key_82;
            PyObject *tmp_dict_value_82;
            PyObject *tmp_dict_key_83;
            PyObject *tmp_dict_value_83;
            tmp_defaults_6 = const_tuple_none_none_none_none_none_none_none_none_tuple;
            tmp_dict_key_73 = const_str_plain_request;
            tmp_dict_value_73 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_HTTPRequest );

            if ( tmp_dict_value_73 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_51 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPRequest );

                if (unlikely( tmp_mvar_value_51 == NULL ))
                {
                    tmp_mvar_value_51 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPRequest );
                }

                if ( tmp_mvar_value_51 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPRequest" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 625;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_dict_value_73 = tmp_mvar_value_51;
                Py_INCREF( tmp_dict_value_73 );
                }
            }

            tmp_annotations_25 = _PyDict_NewPresized( 11 );
            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_73, tmp_dict_value_73 );
            Py_DECREF( tmp_dict_value_73 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_74 = const_str_plain_code;
            tmp_dict_value_74 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_int );

            if ( tmp_dict_value_74 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_74 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_74 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_74, tmp_dict_value_74 );
            Py_DECREF( tmp_dict_value_74 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_75 = const_str_plain_headers;
            tmp_source_name_25 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_httputil );

            if ( tmp_source_name_25 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_52 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_httputil );

                if (unlikely( tmp_mvar_value_52 == NULL ))
                {
                    tmp_mvar_value_52 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_httputil );
                }

                if ( tmp_mvar_value_52 == NULL )
                {
                    Py_DECREF( tmp_annotations_25 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "httputil" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 627;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_source_name_25 = tmp_mvar_value_52;
                Py_INCREF( tmp_source_name_25 );
                }
            }

            tmp_dict_value_75 = LOOKUP_ATTRIBUTE( tmp_source_name_25, const_str_plain_HTTPHeaders );
            Py_DECREF( tmp_source_name_25 );
            if ( tmp_dict_value_75 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_25 );

                exception_lineno = 627;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_75, tmp_dict_value_75 );
            Py_DECREF( tmp_dict_value_75 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_76 = const_str_plain_buffer;
            tmp_dict_value_76 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_BytesIO );

            if ( tmp_dict_value_76 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_53 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_BytesIO );

                if (unlikely( tmp_mvar_value_53 == NULL ))
                {
                    tmp_mvar_value_53 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_BytesIO );
                }

                if ( tmp_mvar_value_53 == NULL )
                {
                    Py_DECREF( tmp_annotations_25 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "BytesIO" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 628;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_dict_value_76 = tmp_mvar_value_53;
                Py_INCREF( tmp_dict_value_76 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_76, tmp_dict_value_76 );
            Py_DECREF( tmp_dict_value_76 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_77 = const_str_plain_effective_url;
            tmp_dict_value_77 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_str );

            if ( tmp_dict_value_77 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_77 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_77 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_77, tmp_dict_value_77 );
            Py_DECREF( tmp_dict_value_77 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_78 = const_str_plain_error;
            tmp_dict_value_78 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_BaseException );

            if ( tmp_dict_value_78 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_78 = PyExc_BaseException;
                Py_INCREF( tmp_dict_value_78 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_78, tmp_dict_value_78 );
            Py_DECREF( tmp_dict_value_78 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_79 = const_str_plain_request_time;
            tmp_dict_value_79 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_float );

            if ( tmp_dict_value_79 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_79 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_dict_value_79 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_79, tmp_dict_value_79 );
            Py_DECREF( tmp_dict_value_79 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_80 = const_str_plain_time_info;
            tmp_subscribed_name_31 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_Dict );

            if ( tmp_subscribed_name_31 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_54 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_54 == NULL ))
                {
                    tmp_mvar_value_54 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_54 == NULL )
                {
                    Py_DECREF( tmp_annotations_25 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 632;
                    type_description_2 = "o";
                    goto frame_exception_exit_5;
                }

                tmp_subscribed_name_31 = tmp_mvar_value_54;
                Py_INCREF( tmp_subscribed_name_31 );
                }
            }

            tmp_tuple_element_32 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_str );

            if ( tmp_tuple_element_32 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_32 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_32 );
                }
            }

            tmp_subscript_name_31 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_31, 0, tmp_tuple_element_32 );
            tmp_tuple_element_32 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_float );

            if ( tmp_tuple_element_32 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_32 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_tuple_element_32 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_31, 1, tmp_tuple_element_32 );
            tmp_dict_value_80 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_31, tmp_subscript_name_31 );
            Py_DECREF( tmp_subscribed_name_31 );
            Py_DECREF( tmp_subscript_name_31 );
            if ( tmp_dict_value_80 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_25 );

                exception_lineno = 632;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_80, tmp_dict_value_80 );
            Py_DECREF( tmp_dict_value_80 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_81 = const_str_plain_reason;
            tmp_dict_value_81 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_str );

            if ( tmp_dict_value_81 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_81 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_81 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_81, tmp_dict_value_81 );
            Py_DECREF( tmp_dict_value_81 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_82 = const_str_plain_start_time;
            tmp_dict_value_82 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_float );

            if ( tmp_dict_value_82 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_82 = (PyObject *)&PyFloat_Type;
                Py_INCREF( tmp_dict_value_82 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_82, tmp_dict_value_82 );
            Py_DECREF( tmp_dict_value_82 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_83 = const_str_plain_return;
            tmp_dict_value_83 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_25, tmp_dict_key_83, tmp_dict_value_83 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_6 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_19___init__( tmp_defaults_6, tmp_annotations_25 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 623;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }
        {
            nuitka_bool tmp_condition_result_31;
            PyObject *tmp_called_name_19;
            PyObject *tmp_args_element_name_11;
            PyObject *tmp_annotations_26;
            PyObject *tmp_dict_key_84;
            PyObject *tmp_dict_value_84;
            PyObject *tmp_called_name_20;
            PyObject *tmp_args_element_name_12;
            PyObject *tmp_annotations_27;
            PyObject *tmp_dict_key_85;
            PyObject *tmp_dict_value_85;
            tmp_res = MAPPING_HAS_ITEM( locals_tornado$httpclient_568, const_str_plain_property );

            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 665;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_condition_result_31 = ( tmp_res == 1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_31 == NUITKA_BOOL_TRUE )
            {
                goto condexpr_true_16;
            }
            else
            {
                goto condexpr_false_16;
            }
            condexpr_true_16:;
            tmp_called_name_19 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_property );

            if ( tmp_called_name_19 == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "property" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 665;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }

            if ( tmp_called_name_19 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 665;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            tmp_dict_key_84 = const_str_plain_return;
            tmp_dict_value_84 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_bytes );

            if ( tmp_dict_value_84 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_84 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_dict_value_84 );
                }
            }

            tmp_annotations_26 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_26, tmp_dict_key_84, tmp_dict_value_84 );
            Py_DECREF( tmp_dict_value_84 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_11 = MAKE_FUNCTION_tornado$httpclient$$$function_20_body( tmp_annotations_26 );



            frame_ed5be14316d8a2e2facc323ac7ebc012_5->m_frame.f_lineno = 665;
            {
                PyObject *call_args[] = { tmp_args_element_name_11 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
            }

            Py_DECREF( tmp_called_name_19 );
            Py_DECREF( tmp_args_element_name_11 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 665;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            goto condexpr_end_16;
            condexpr_false_16:;
            tmp_called_name_20 = (PyObject *)&PyProperty_Type;
            tmp_dict_key_85 = const_str_plain_return;
            tmp_dict_value_85 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_bytes );

            if ( tmp_dict_value_85 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_85 = (PyObject *)&PyBytes_Type;
                Py_INCREF( tmp_dict_value_85 );
                }
            }

            tmp_annotations_27 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_27, tmp_dict_key_85, tmp_dict_value_85 );
            Py_DECREF( tmp_dict_value_85 );
            assert( !(tmp_res != 0) );
            tmp_args_element_name_12 = MAKE_FUNCTION_tornado$httpclient$$$function_20_body( tmp_annotations_27 );



            frame_ed5be14316d8a2e2facc323ac7ebc012_5->m_frame.f_lineno = 665;
            {
                PyObject *call_args[] = { tmp_args_element_name_12 };
                tmp_dictset_value = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
            }

            Py_DECREF( tmp_args_element_name_12 );
            if ( tmp_dictset_value == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 665;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
            condexpr_end_16:;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain_body, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 665;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }
        {
            PyObject *tmp_annotations_28;
            tmp_annotations_28 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_21_rethrow( tmp_annotations_28 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain_rethrow, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 674;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }
        {
            PyObject *tmp_annotations_29;
            PyObject *tmp_dict_key_86;
            PyObject *tmp_dict_value_86;
            tmp_dict_key_86 = const_str_plain_return;
            tmp_dict_value_86 = PyObject_GetItem( locals_tornado$httpclient_568, const_str_plain_str );

            if ( tmp_dict_value_86 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_86 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_86 );
                }
            }

            tmp_annotations_29 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_29, tmp_dict_key_86, tmp_dict_value_86 );
            Py_DECREF( tmp_dict_value_86 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_22___repr__( tmp_annotations_29 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain___repr__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 679;
                type_description_2 = "o";
                goto frame_exception_exit_5;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_ed5be14316d8a2e2facc323ac7ebc012_5 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_4;

        frame_exception_exit_5:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_ed5be14316d8a2e2facc323ac7ebc012_5 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_ed5be14316d8a2e2facc323ac7ebc012_5, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_ed5be14316d8a2e2facc323ac7ebc012_5->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_ed5be14316d8a2e2facc323ac7ebc012_5, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_ed5be14316d8a2e2facc323ac7ebc012_5,
            type_description_2,
            outline_3_var___class__
        );


        // Release cached frame.
        if ( frame_ed5be14316d8a2e2facc323ac7ebc012_5 == cache_frame_ed5be14316d8a2e2facc323ac7ebc012_5 )
        {
            Py_DECREF( frame_ed5be14316d8a2e2facc323ac7ebc012_5 );
        }
        cache_frame_ed5be14316d8a2e2facc323ac7ebc012_5 = NULL;

        assertFrameObject( frame_ed5be14316d8a2e2facc323ac7ebc012_5 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_4;

        frame_no_exception_4:;
        goto skip_nested_handling_4;
        nested_frame_exit_4:;

        goto try_except_handler_16;
        skip_nested_handling_4:;
        {
            nuitka_bool tmp_condition_result_32;
            PyObject *tmp_compexpr_left_4;
            PyObject *tmp_compexpr_right_4;
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_compexpr_left_4 = tmp_class_creation_4__bases;
            tmp_compexpr_right_4 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_4, tmp_compexpr_right_4 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;

                goto try_except_handler_16;
            }
            tmp_condition_result_32 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_32 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_16;
            }
            else
            {
                goto branch_no_16;
            }
            branch_yes_16:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_568, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;

                goto try_except_handler_16;
            }
            branch_no_16:;
        }
        {
            PyObject *tmp_assign_source_59;
            PyObject *tmp_called_name_21;
            PyObject *tmp_args_name_8;
            PyObject *tmp_tuple_element_33;
            PyObject *tmp_kw_name_9;
            CHECK_OBJECT( tmp_class_creation_4__metaclass );
            tmp_called_name_21 = tmp_class_creation_4__metaclass;
            tmp_tuple_element_33 = const_str_plain_HTTPResponse;
            tmp_args_name_8 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_33 );
            PyTuple_SET_ITEM( tmp_args_name_8, 0, tmp_tuple_element_33 );
            CHECK_OBJECT( tmp_class_creation_4__bases );
            tmp_tuple_element_33 = tmp_class_creation_4__bases;
            Py_INCREF( tmp_tuple_element_33 );
            PyTuple_SET_ITEM( tmp_args_name_8, 1, tmp_tuple_element_33 );
            tmp_tuple_element_33 = locals_tornado$httpclient_568;
            Py_INCREF( tmp_tuple_element_33 );
            PyTuple_SET_ITEM( tmp_args_name_8, 2, tmp_tuple_element_33 );
            CHECK_OBJECT( tmp_class_creation_4__class_decl_dict );
            tmp_kw_name_9 = tmp_class_creation_4__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 568;
            tmp_assign_source_59 = CALL_FUNCTION( tmp_called_name_21, tmp_args_name_8, tmp_kw_name_9 );
            Py_DECREF( tmp_args_name_8 );
            if ( tmp_assign_source_59 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 568;

                goto try_except_handler_16;
            }
            assert( outline_3_var___class__ == NULL );
            outline_3_var___class__ = tmp_assign_source_59;
        }
        CHECK_OBJECT( outline_3_var___class__ );
        tmp_assign_source_58 = outline_3_var___class__;
        Py_INCREF( tmp_assign_source_58 );
        goto try_return_handler_16;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_16:;
        Py_DECREF( locals_tornado$httpclient_568 );
        locals_tornado$httpclient_568 = NULL;
        goto try_return_handler_15;
        // Exception handler code:
        try_except_handler_16:;
        exception_keeper_type_14 = exception_type;
        exception_keeper_value_14 = exception_value;
        exception_keeper_tb_14 = exception_tb;
        exception_keeper_lineno_14 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$httpclient_568 );
        locals_tornado$httpclient_568 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_14;
        exception_value = exception_keeper_value_14;
        exception_tb = exception_keeper_tb_14;
        exception_lineno = exception_keeper_lineno_14;

        goto try_except_handler_15;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_15:;
        CHECK_OBJECT( (PyObject *)outline_3_var___class__ );
        Py_DECREF( outline_3_var___class__ );
        outline_3_var___class__ = NULL;

        goto outline_result_4;
        // Exception handler code:
        try_except_handler_15:;
        exception_keeper_type_15 = exception_type;
        exception_keeper_value_15 = exception_value;
        exception_keeper_tb_15 = exception_tb;
        exception_keeper_lineno_15 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_15;
        exception_value = exception_keeper_value_15;
        exception_tb = exception_keeper_tb_15;
        exception_lineno = exception_keeper_lineno_15;

        goto outline_exception_4;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_4:;
        exception_lineno = 568;
        goto try_except_handler_14;
        outline_result_4:;
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPResponse, tmp_assign_source_58 );
    }
    goto try_end_8;
    // Exception handler code:
    try_except_handler_14:;
    exception_keeper_type_16 = exception_type;
    exception_keeper_value_16 = exception_value;
    exception_keeper_tb_16 = exception_tb;
    exception_keeper_lineno_16 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    Py_XDECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_16;
    exception_value = exception_keeper_value_16;
    exception_tb = exception_keeper_tb_16;
    exception_lineno = exception_keeper_lineno_16;

    goto frame_exception_exit_1;
    // End of try:
    try_end_8:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__bases );
    Py_DECREF( tmp_class_creation_4__bases );
    tmp_class_creation_4__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__class_decl_dict );
    Py_DECREF( tmp_class_creation_4__class_decl_dict );
    tmp_class_creation_4__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__metaclass );
    Py_DECREF( tmp_class_creation_4__metaclass );
    tmp_class_creation_4__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_4__prepared );
    Py_DECREF( tmp_class_creation_4__prepared );
    tmp_class_creation_4__prepared = NULL;

    // Tried code:
    {
        PyObject *tmp_assign_source_60;
        PyObject *tmp_dircall_arg1_5;
        tmp_dircall_arg1_5 = const_tuple_type_Exception_tuple;
        Py_INCREF( tmp_dircall_arg1_5 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_5};
            tmp_assign_source_60 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_17;
        }
        assert( tmp_class_creation_5__bases == NULL );
        tmp_class_creation_5__bases = tmp_assign_source_60;
    }
    {
        PyObject *tmp_assign_source_61;
        tmp_assign_source_61 = PyDict_New();
        assert( tmp_class_creation_5__class_decl_dict == NULL );
        tmp_class_creation_5__class_decl_dict = tmp_assign_source_61;
    }
    {
        PyObject *tmp_assign_source_62;
        PyObject *tmp_metaclass_name_5;
        nuitka_bool tmp_condition_result_33;
        PyObject *tmp_key_name_13;
        PyObject *tmp_dict_name_13;
        PyObject *tmp_dict_name_14;
        PyObject *tmp_key_name_14;
        nuitka_bool tmp_condition_result_34;
        int tmp_truth_name_5;
        PyObject *tmp_type_arg_9;
        PyObject *tmp_subscribed_name_32;
        PyObject *tmp_subscript_name_32;
        PyObject *tmp_bases_name_5;
        tmp_key_name_13 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_13 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_13, tmp_key_name_13 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_17;
        }
        tmp_condition_result_33 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_33 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_17;
        }
        else
        {
            goto condexpr_false_17;
        }
        condexpr_true_17:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_14 = tmp_class_creation_5__class_decl_dict;
        tmp_key_name_14 = const_str_plain_metaclass;
        tmp_metaclass_name_5 = DICT_GET_ITEM( tmp_dict_name_14, tmp_key_name_14 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_17;
        }
        goto condexpr_end_17;
        condexpr_false_17:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_truth_name_5 = CHECK_IF_TRUE( tmp_class_creation_5__bases );
        if ( tmp_truth_name_5 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_17;
        }
        tmp_condition_result_34 = tmp_truth_name_5 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_34 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_18;
        }
        else
        {
            goto condexpr_false_18;
        }
        condexpr_true_18:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_subscribed_name_32 = tmp_class_creation_5__bases;
        tmp_subscript_name_32 = const_int_0;
        tmp_type_arg_9 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_32, tmp_subscript_name_32, 0 );
        if ( tmp_type_arg_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_17;
        }
        tmp_metaclass_name_5 = BUILTIN_TYPE1( tmp_type_arg_9 );
        Py_DECREF( tmp_type_arg_9 );
        if ( tmp_metaclass_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_17;
        }
        goto condexpr_end_18;
        condexpr_false_18:;
        tmp_metaclass_name_5 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_5 );
        condexpr_end_18:;
        condexpr_end_17:;
        CHECK_OBJECT( tmp_class_creation_5__bases );
        tmp_bases_name_5 = tmp_class_creation_5__bases;
        tmp_assign_source_62 = SELECT_METACLASS( tmp_metaclass_name_5, tmp_bases_name_5 );
        Py_DECREF( tmp_metaclass_name_5 );
        if ( tmp_assign_source_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_17;
        }
        assert( tmp_class_creation_5__metaclass == NULL );
        tmp_class_creation_5__metaclass = tmp_assign_source_62;
    }
    {
        nuitka_bool tmp_condition_result_35;
        PyObject *tmp_key_name_15;
        PyObject *tmp_dict_name_15;
        tmp_key_name_15 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dict_name_15 = tmp_class_creation_5__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_15, tmp_key_name_15 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_17;
        }
        tmp_condition_result_35 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_35 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_17;
        }
        else
        {
            goto branch_no_17;
        }
        branch_yes_17:;
        CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_5__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_17;
        }
        branch_no_17:;
    }
    {
        nuitka_bool tmp_condition_result_36;
        PyObject *tmp_source_name_26;
        CHECK_OBJECT( tmp_class_creation_5__metaclass );
        tmp_source_name_26 = tmp_class_creation_5__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_26, const_str_plain___prepare__ );
        tmp_condition_result_36 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_36 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_18;
        }
        else
        {
            goto branch_no_18;
        }
        branch_yes_18:;
        {
            PyObject *tmp_assign_source_63;
            PyObject *tmp_called_name_22;
            PyObject *tmp_source_name_27;
            PyObject *tmp_args_name_9;
            PyObject *tmp_tuple_element_34;
            PyObject *tmp_kw_name_10;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_source_name_27 = tmp_class_creation_5__metaclass;
            tmp_called_name_22 = LOOKUP_ATTRIBUTE( tmp_source_name_27, const_str_plain___prepare__ );
            if ( tmp_called_name_22 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 684;

                goto try_except_handler_17;
            }
            tmp_tuple_element_34 = const_str_plain_HTTPClientError;
            tmp_args_name_9 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_34 );
            PyTuple_SET_ITEM( tmp_args_name_9, 0, tmp_tuple_element_34 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_34 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_34 );
            PyTuple_SET_ITEM( tmp_args_name_9, 1, tmp_tuple_element_34 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_10 = tmp_class_creation_5__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 684;
            tmp_assign_source_63 = CALL_FUNCTION( tmp_called_name_22, tmp_args_name_9, tmp_kw_name_10 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_args_name_9 );
            if ( tmp_assign_source_63 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 684;

                goto try_except_handler_17;
            }
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_63;
        }
        {
            nuitka_bool tmp_condition_result_37;
            PyObject *tmp_operand_name_5;
            PyObject *tmp_source_name_28;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_source_name_28 = tmp_class_creation_5__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_28, const_str_plain___getitem__ );
            tmp_operand_name_5 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 684;

                goto try_except_handler_17;
            }
            tmp_condition_result_37 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_37 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_19;
            }
            else
            {
                goto branch_no_19;
            }
            branch_yes_19:;
            {
                PyObject *tmp_raise_type_5;
                PyObject *tmp_raise_value_5;
                PyObject *tmp_left_name_5;
                PyObject *tmp_right_name_5;
                PyObject *tmp_tuple_element_35;
                PyObject *tmp_getattr_target_5;
                PyObject *tmp_getattr_attr_5;
                PyObject *tmp_getattr_default_5;
                PyObject *tmp_source_name_29;
                PyObject *tmp_type_arg_10;
                tmp_raise_type_5 = PyExc_TypeError;
                tmp_left_name_5 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_5__metaclass );
                tmp_getattr_target_5 = tmp_class_creation_5__metaclass;
                tmp_getattr_attr_5 = const_str_plain___name__;
                tmp_getattr_default_5 = const_str_angle_metaclass;
                tmp_tuple_element_35 = BUILTIN_GETATTR( tmp_getattr_target_5, tmp_getattr_attr_5, tmp_getattr_default_5 );
                if ( tmp_tuple_element_35 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 684;

                    goto try_except_handler_17;
                }
                tmp_right_name_5 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_5, 0, tmp_tuple_element_35 );
                CHECK_OBJECT( tmp_class_creation_5__prepared );
                tmp_type_arg_10 = tmp_class_creation_5__prepared;
                tmp_source_name_29 = BUILTIN_TYPE1( tmp_type_arg_10 );
                assert( !(tmp_source_name_29 == NULL) );
                tmp_tuple_element_35 = LOOKUP_ATTRIBUTE( tmp_source_name_29, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_29 );
                if ( tmp_tuple_element_35 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_5 );

                    exception_lineno = 684;

                    goto try_except_handler_17;
                }
                PyTuple_SET_ITEM( tmp_right_name_5, 1, tmp_tuple_element_35 );
                tmp_raise_value_5 = BINARY_OPERATION_REMAINDER( tmp_left_name_5, tmp_right_name_5 );
                Py_DECREF( tmp_right_name_5 );
                if ( tmp_raise_value_5 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 684;

                    goto try_except_handler_17;
                }
                exception_type = tmp_raise_type_5;
                Py_INCREF( tmp_raise_type_5 );
                exception_value = tmp_raise_value_5;
                exception_lineno = 684;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_17;
            }
            branch_no_19:;
        }
        goto branch_end_18;
        branch_no_18:;
        {
            PyObject *tmp_assign_source_64;
            tmp_assign_source_64 = PyDict_New();
            assert( tmp_class_creation_5__prepared == NULL );
            tmp_class_creation_5__prepared = tmp_assign_source_64;
        }
        branch_end_18:;
    }
    {
        PyObject *tmp_assign_source_65;
        {
            PyObject *tmp_set_locals_5;
            CHECK_OBJECT( tmp_class_creation_5__prepared );
            tmp_set_locals_5 = tmp_class_creation_5__prepared;
            locals_tornado$httpclient_684 = tmp_set_locals_5;
            Py_INCREF( tmp_set_locals_5 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_6f55736fcd57507504cc7f0643fbf21f;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_684, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_19;
        }
        tmp_dictset_value = const_str_digest_02224986bed76353a6f0be294c953224;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_684, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_19;
        }
        tmp_dictset_value = const_str_plain_HTTPClientError;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_684, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 684;

            goto try_except_handler_19;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_8653d73da3de6cf1359d78767acabc70_6, codeobj_8653d73da3de6cf1359d78767acabc70, module_tornado$httpclient, sizeof(void *) );
        frame_8653d73da3de6cf1359d78767acabc70_6 = cache_frame_8653d73da3de6cf1359d78767acabc70_6;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_8653d73da3de6cf1359d78767acabc70_6 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_8653d73da3de6cf1359d78767acabc70_6 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_defaults_7;
            PyObject *tmp_annotations_30;
            PyObject *tmp_dict_key_87;
            PyObject *tmp_dict_value_87;
            PyObject *tmp_dict_key_88;
            PyObject *tmp_dict_value_88;
            PyObject *tmp_dict_key_89;
            PyObject *tmp_dict_value_89;
            PyObject *tmp_mvar_value_55;
            PyObject *tmp_dict_key_90;
            PyObject *tmp_dict_value_90;
            tmp_defaults_7 = const_tuple_none_none_tuple;
            tmp_dict_key_87 = const_str_plain_code;
            tmp_dict_value_87 = PyObject_GetItem( locals_tornado$httpclient_684, const_str_plain_int );

            if ( tmp_dict_value_87 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_87 = (PyObject *)&PyLong_Type;
                Py_INCREF( tmp_dict_value_87 );
                }
            }

            tmp_annotations_30 = _PyDict_NewPresized( 4 );
            tmp_res = PyDict_SetItem( tmp_annotations_30, tmp_dict_key_87, tmp_dict_value_87 );
            Py_DECREF( tmp_dict_value_87 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_88 = const_str_plain_message;
            tmp_dict_value_88 = PyObject_GetItem( locals_tornado$httpclient_684, const_str_plain_str );

            if ( tmp_dict_value_88 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_88 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_88 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_30, tmp_dict_key_88, tmp_dict_value_88 );
            Py_DECREF( tmp_dict_value_88 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_89 = const_str_plain_response;
            tmp_dict_value_89 = PyObject_GetItem( locals_tornado$httpclient_684, const_str_plain_HTTPResponse );

            if ( tmp_dict_value_89 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_55 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPResponse );

                if (unlikely( tmp_mvar_value_55 == NULL ))
                {
                    tmp_mvar_value_55 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPResponse );
                }

                if ( tmp_mvar_value_55 == NULL )
                {
                    Py_DECREF( tmp_annotations_30 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPResponse" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 706;
                    type_description_2 = "o";
                    goto frame_exception_exit_6;
                }

                tmp_dict_value_89 = tmp_mvar_value_55;
                Py_INCREF( tmp_dict_value_89 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_30, tmp_dict_key_89, tmp_dict_value_89 );
            Py_DECREF( tmp_dict_value_89 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_90 = const_str_plain_return;
            tmp_dict_value_90 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_30, tmp_dict_key_90, tmp_dict_value_90 );
            assert( !(tmp_res != 0) );
            Py_INCREF( tmp_defaults_7 );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_23___init__( tmp_defaults_7, tmp_annotations_30 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_684, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 705;
                type_description_2 = "o";
                goto frame_exception_exit_6;
            }
        }
        {
            PyObject *tmp_annotations_31;
            PyObject *tmp_dict_key_91;
            PyObject *tmp_dict_value_91;
            tmp_dict_key_91 = const_str_plain_return;
            tmp_dict_value_91 = PyObject_GetItem( locals_tornado$httpclient_684, const_str_plain_str );

            if ( tmp_dict_value_91 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_91 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_91 );
                }
            }

            tmp_annotations_31 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_annotations_31, tmp_dict_key_91, tmp_dict_value_91 );
            Py_DECREF( tmp_dict_value_91 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_24___str__( tmp_annotations_31 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_684, const_str_plain___str__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 713;
                type_description_2 = "o";
                goto frame_exception_exit_6;
            }
        }
        tmp_dictset_value = PyObject_GetItem( locals_tornado$httpclient_684, const_str_plain___str__ );

        if ( tmp_dictset_value == NULL && CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "__str__" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 720;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }

        if ( tmp_dictset_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 720;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_684, const_str_plain___repr__, tmp_dictset_value );
        Py_DECREF( tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 720;
            type_description_2 = "o";
            goto frame_exception_exit_6;
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8653d73da3de6cf1359d78767acabc70_6 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_5;

        frame_exception_exit_6:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_8653d73da3de6cf1359d78767acabc70_6 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_8653d73da3de6cf1359d78767acabc70_6, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_8653d73da3de6cf1359d78767acabc70_6->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_8653d73da3de6cf1359d78767acabc70_6, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_8653d73da3de6cf1359d78767acabc70_6,
            type_description_2,
            outline_4_var___class__
        );


        // Release cached frame.
        if ( frame_8653d73da3de6cf1359d78767acabc70_6 == cache_frame_8653d73da3de6cf1359d78767acabc70_6 )
        {
            Py_DECREF( frame_8653d73da3de6cf1359d78767acabc70_6 );
        }
        cache_frame_8653d73da3de6cf1359d78767acabc70_6 = NULL;

        assertFrameObject( frame_8653d73da3de6cf1359d78767acabc70_6 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_5;

        frame_no_exception_5:;
        goto skip_nested_handling_5;
        nested_frame_exit_5:;

        goto try_except_handler_19;
        skip_nested_handling_5:;
        {
            nuitka_bool tmp_condition_result_38;
            PyObject *tmp_compexpr_left_5;
            PyObject *tmp_compexpr_right_5;
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_compexpr_left_5 = tmp_class_creation_5__bases;
            tmp_compexpr_right_5 = const_tuple_type_Exception_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_5, tmp_compexpr_right_5 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 684;

                goto try_except_handler_19;
            }
            tmp_condition_result_38 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_38 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_20;
            }
            else
            {
                goto branch_no_20;
            }
            branch_yes_20:;
            tmp_dictset_value = const_tuple_type_Exception_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_684, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 684;

                goto try_except_handler_19;
            }
            branch_no_20:;
        }
        {
            PyObject *tmp_assign_source_66;
            PyObject *tmp_called_name_23;
            PyObject *tmp_args_name_10;
            PyObject *tmp_tuple_element_36;
            PyObject *tmp_kw_name_11;
            CHECK_OBJECT( tmp_class_creation_5__metaclass );
            tmp_called_name_23 = tmp_class_creation_5__metaclass;
            tmp_tuple_element_36 = const_str_plain_HTTPClientError;
            tmp_args_name_10 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_36 );
            PyTuple_SET_ITEM( tmp_args_name_10, 0, tmp_tuple_element_36 );
            CHECK_OBJECT( tmp_class_creation_5__bases );
            tmp_tuple_element_36 = tmp_class_creation_5__bases;
            Py_INCREF( tmp_tuple_element_36 );
            PyTuple_SET_ITEM( tmp_args_name_10, 1, tmp_tuple_element_36 );
            tmp_tuple_element_36 = locals_tornado$httpclient_684;
            Py_INCREF( tmp_tuple_element_36 );
            PyTuple_SET_ITEM( tmp_args_name_10, 2, tmp_tuple_element_36 );
            CHECK_OBJECT( tmp_class_creation_5__class_decl_dict );
            tmp_kw_name_11 = tmp_class_creation_5__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 684;
            tmp_assign_source_66 = CALL_FUNCTION( tmp_called_name_23, tmp_args_name_10, tmp_kw_name_11 );
            Py_DECREF( tmp_args_name_10 );
            if ( tmp_assign_source_66 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 684;

                goto try_except_handler_19;
            }
            assert( outline_4_var___class__ == NULL );
            outline_4_var___class__ = tmp_assign_source_66;
        }
        CHECK_OBJECT( outline_4_var___class__ );
        tmp_assign_source_65 = outline_4_var___class__;
        Py_INCREF( tmp_assign_source_65 );
        goto try_return_handler_19;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_19:;
        Py_DECREF( locals_tornado$httpclient_684 );
        locals_tornado$httpclient_684 = NULL;
        goto try_return_handler_18;
        // Exception handler code:
        try_except_handler_19:;
        exception_keeper_type_17 = exception_type;
        exception_keeper_value_17 = exception_value;
        exception_keeper_tb_17 = exception_tb;
        exception_keeper_lineno_17 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$httpclient_684 );
        locals_tornado$httpclient_684 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_17;
        exception_value = exception_keeper_value_17;
        exception_tb = exception_keeper_tb_17;
        exception_lineno = exception_keeper_lineno_17;

        goto try_except_handler_18;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_18:;
        CHECK_OBJECT( (PyObject *)outline_4_var___class__ );
        Py_DECREF( outline_4_var___class__ );
        outline_4_var___class__ = NULL;

        goto outline_result_5;
        // Exception handler code:
        try_except_handler_18:;
        exception_keeper_type_18 = exception_type;
        exception_keeper_value_18 = exception_value;
        exception_keeper_tb_18 = exception_tb;
        exception_keeper_lineno_18 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_18;
        exception_value = exception_keeper_value_18;
        exception_tb = exception_keeper_tb_18;
        exception_lineno = exception_keeper_lineno_18;

        goto outline_exception_5;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_5:;
        exception_lineno = 684;
        goto try_except_handler_17;
        outline_result_5:;
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPClientError, tmp_assign_source_65 );
    }
    goto try_end_9;
    // Exception handler code:
    try_except_handler_17:;
    exception_keeper_type_19 = exception_type;
    exception_keeper_value_19 = exception_value;
    exception_keeper_tb_19 = exception_tb;
    exception_keeper_lineno_19 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    Py_XDECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_19;
    exception_value = exception_keeper_value_19;
    exception_tb = exception_keeper_tb_19;
    exception_lineno = exception_keeper_lineno_19;

    goto frame_exception_exit_1;
    // End of try:
    try_end_9:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__bases );
    Py_DECREF( tmp_class_creation_5__bases );
    tmp_class_creation_5__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__class_decl_dict );
    Py_DECREF( tmp_class_creation_5__class_decl_dict );
    tmp_class_creation_5__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__metaclass );
    Py_DECREF( tmp_class_creation_5__metaclass );
    tmp_class_creation_5__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_5__prepared );
    Py_DECREF( tmp_class_creation_5__prepared );
    tmp_class_creation_5__prepared = NULL;

    {
        PyObject *tmp_assign_source_67;
        PyObject *tmp_mvar_value_56;
        tmp_mvar_value_56 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPClientError );

        if (unlikely( tmp_mvar_value_56 == NULL ))
        {
            tmp_mvar_value_56 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPClientError );
        }

        if ( tmp_mvar_value_56 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPClientError" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 723;

            goto frame_exception_exit_1;
        }

        tmp_assign_source_67 = tmp_mvar_value_56;
        UPDATE_STRING_DICT0( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPError, tmp_assign_source_67 );
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_68;
        PyObject *tmp_dircall_arg1_6;
        tmp_dircall_arg1_6 = const_tuple_type_object_tuple;
        Py_INCREF( tmp_dircall_arg1_6 );

        {
            PyObject *dir_call_args[] = {tmp_dircall_arg1_6};
            tmp_assign_source_68 = impl___internal__$$$function_1__mro_entries_conversion( dir_call_args );
        }
        if ( tmp_assign_source_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_20;
        }
        assert( tmp_class_creation_6__bases == NULL );
        tmp_class_creation_6__bases = tmp_assign_source_68;
    }
    {
        PyObject *tmp_assign_source_69;
        tmp_assign_source_69 = PyDict_New();
        assert( tmp_class_creation_6__class_decl_dict == NULL );
        tmp_class_creation_6__class_decl_dict = tmp_assign_source_69;
    }
    {
        PyObject *tmp_assign_source_70;
        PyObject *tmp_metaclass_name_6;
        nuitka_bool tmp_condition_result_39;
        PyObject *tmp_key_name_16;
        PyObject *tmp_dict_name_16;
        PyObject *tmp_dict_name_17;
        PyObject *tmp_key_name_17;
        nuitka_bool tmp_condition_result_40;
        int tmp_truth_name_6;
        PyObject *tmp_type_arg_11;
        PyObject *tmp_subscribed_name_33;
        PyObject *tmp_subscript_name_33;
        PyObject *tmp_bases_name_6;
        tmp_key_name_16 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_16 = tmp_class_creation_6__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_16, tmp_key_name_16 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_20;
        }
        tmp_condition_result_39 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_39 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_19;
        }
        else
        {
            goto condexpr_false_19;
        }
        condexpr_true_19:;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_17 = tmp_class_creation_6__class_decl_dict;
        tmp_key_name_17 = const_str_plain_metaclass;
        tmp_metaclass_name_6 = DICT_GET_ITEM( tmp_dict_name_17, tmp_key_name_17 );
        if ( tmp_metaclass_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_20;
        }
        goto condexpr_end_19;
        condexpr_false_19:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_truth_name_6 = CHECK_IF_TRUE( tmp_class_creation_6__bases );
        if ( tmp_truth_name_6 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_20;
        }
        tmp_condition_result_40 = tmp_truth_name_6 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_40 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_20;
        }
        else
        {
            goto condexpr_false_20;
        }
        condexpr_true_20:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_subscribed_name_33 = tmp_class_creation_6__bases;
        tmp_subscript_name_33 = const_int_0;
        tmp_type_arg_11 = LOOKUP_SUBSCRIPT_CONST( tmp_subscribed_name_33, tmp_subscript_name_33, 0 );
        if ( tmp_type_arg_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_20;
        }
        tmp_metaclass_name_6 = BUILTIN_TYPE1( tmp_type_arg_11 );
        Py_DECREF( tmp_type_arg_11 );
        if ( tmp_metaclass_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_20;
        }
        goto condexpr_end_20;
        condexpr_false_20:;
        tmp_metaclass_name_6 = (PyObject *)&PyType_Type;
        Py_INCREF( tmp_metaclass_name_6 );
        condexpr_end_20:;
        condexpr_end_19:;
        CHECK_OBJECT( tmp_class_creation_6__bases );
        tmp_bases_name_6 = tmp_class_creation_6__bases;
        tmp_assign_source_70 = SELECT_METACLASS( tmp_metaclass_name_6, tmp_bases_name_6 );
        Py_DECREF( tmp_metaclass_name_6 );
        if ( tmp_assign_source_70 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_20;
        }
        assert( tmp_class_creation_6__metaclass == NULL );
        tmp_class_creation_6__metaclass = tmp_assign_source_70;
    }
    {
        nuitka_bool tmp_condition_result_41;
        PyObject *tmp_key_name_18;
        PyObject *tmp_dict_name_18;
        tmp_key_name_18 = const_str_plain_metaclass;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dict_name_18 = tmp_class_creation_6__class_decl_dict;
        tmp_res = PyDict_Contains( tmp_dict_name_18, tmp_key_name_18 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_20;
        }
        tmp_condition_result_41 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_41 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_21;
        }
        else
        {
            goto branch_no_21;
        }
        branch_yes_21:;
        CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
        tmp_dictdel_dict = tmp_class_creation_6__class_decl_dict;
        tmp_dictdel_key = const_str_plain_metaclass;
        tmp_result = DICT_REMOVE_ITEM( tmp_dictdel_dict, tmp_dictdel_key );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_20;
        }
        branch_no_21:;
    }
    {
        nuitka_bool tmp_condition_result_42;
        PyObject *tmp_source_name_30;
        CHECK_OBJECT( tmp_class_creation_6__metaclass );
        tmp_source_name_30 = tmp_class_creation_6__metaclass;
        tmp_res = PyObject_HasAttr( tmp_source_name_30, const_str_plain___prepare__ );
        tmp_condition_result_42 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_42 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_22;
        }
        else
        {
            goto branch_no_22;
        }
        branch_yes_22:;
        {
            PyObject *tmp_assign_source_71;
            PyObject *tmp_called_name_24;
            PyObject *tmp_source_name_31;
            PyObject *tmp_args_name_11;
            PyObject *tmp_tuple_element_37;
            PyObject *tmp_kw_name_12;
            CHECK_OBJECT( tmp_class_creation_6__metaclass );
            tmp_source_name_31 = tmp_class_creation_6__metaclass;
            tmp_called_name_24 = LOOKUP_ATTRIBUTE( tmp_source_name_31, const_str_plain___prepare__ );
            if ( tmp_called_name_24 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 726;

                goto try_except_handler_20;
            }
            tmp_tuple_element_37 = const_str_plain__RequestProxy;
            tmp_args_name_11 = PyTuple_New( 2 );
            Py_INCREF( tmp_tuple_element_37 );
            PyTuple_SET_ITEM( tmp_args_name_11, 0, tmp_tuple_element_37 );
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_tuple_element_37 = tmp_class_creation_6__bases;
            Py_INCREF( tmp_tuple_element_37 );
            PyTuple_SET_ITEM( tmp_args_name_11, 1, tmp_tuple_element_37 );
            CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
            tmp_kw_name_12 = tmp_class_creation_6__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 726;
            tmp_assign_source_71 = CALL_FUNCTION( tmp_called_name_24, tmp_args_name_11, tmp_kw_name_12 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_args_name_11 );
            if ( tmp_assign_source_71 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 726;

                goto try_except_handler_20;
            }
            assert( tmp_class_creation_6__prepared == NULL );
            tmp_class_creation_6__prepared = tmp_assign_source_71;
        }
        {
            nuitka_bool tmp_condition_result_43;
            PyObject *tmp_operand_name_6;
            PyObject *tmp_source_name_32;
            CHECK_OBJECT( tmp_class_creation_6__prepared );
            tmp_source_name_32 = tmp_class_creation_6__prepared;
            tmp_res = PyObject_HasAttr( tmp_source_name_32, const_str_plain___getitem__ );
            tmp_operand_name_6 = ( tmp_res != 0 ) ? Py_True : Py_False;
            tmp_res = CHECK_IF_TRUE( tmp_operand_name_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 726;

                goto try_except_handler_20;
            }
            tmp_condition_result_43 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_43 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_23;
            }
            else
            {
                goto branch_no_23;
            }
            branch_yes_23:;
            {
                PyObject *tmp_raise_type_6;
                PyObject *tmp_raise_value_6;
                PyObject *tmp_left_name_6;
                PyObject *tmp_right_name_6;
                PyObject *tmp_tuple_element_38;
                PyObject *tmp_getattr_target_6;
                PyObject *tmp_getattr_attr_6;
                PyObject *tmp_getattr_default_6;
                PyObject *tmp_source_name_33;
                PyObject *tmp_type_arg_12;
                tmp_raise_type_6 = PyExc_TypeError;
                tmp_left_name_6 = const_str_digest_75fd71b1edada749c2ef7ac810062295;
                CHECK_OBJECT( tmp_class_creation_6__metaclass );
                tmp_getattr_target_6 = tmp_class_creation_6__metaclass;
                tmp_getattr_attr_6 = const_str_plain___name__;
                tmp_getattr_default_6 = const_str_angle_metaclass;
                tmp_tuple_element_38 = BUILTIN_GETATTR( tmp_getattr_target_6, tmp_getattr_attr_6, tmp_getattr_default_6 );
                if ( tmp_tuple_element_38 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 726;

                    goto try_except_handler_20;
                }
                tmp_right_name_6 = PyTuple_New( 2 );
                PyTuple_SET_ITEM( tmp_right_name_6, 0, tmp_tuple_element_38 );
                CHECK_OBJECT( tmp_class_creation_6__prepared );
                tmp_type_arg_12 = tmp_class_creation_6__prepared;
                tmp_source_name_33 = BUILTIN_TYPE1( tmp_type_arg_12 );
                assert( !(tmp_source_name_33 == NULL) );
                tmp_tuple_element_38 = LOOKUP_ATTRIBUTE( tmp_source_name_33, const_str_plain___name__ );
                Py_DECREF( tmp_source_name_33 );
                if ( tmp_tuple_element_38 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_right_name_6 );

                    exception_lineno = 726;

                    goto try_except_handler_20;
                }
                PyTuple_SET_ITEM( tmp_right_name_6, 1, tmp_tuple_element_38 );
                tmp_raise_value_6 = BINARY_OPERATION_REMAINDER( tmp_left_name_6, tmp_right_name_6 );
                Py_DECREF( tmp_right_name_6 );
                if ( tmp_raise_value_6 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 726;

                    goto try_except_handler_20;
                }
                exception_type = tmp_raise_type_6;
                Py_INCREF( tmp_raise_type_6 );
                exception_value = tmp_raise_value_6;
                exception_lineno = 726;
                RAISE_EXCEPTION_IMPLICIT( &exception_type, &exception_value, &exception_tb );

                goto try_except_handler_20;
            }
            branch_no_23:;
        }
        goto branch_end_22;
        branch_no_22:;
        {
            PyObject *tmp_assign_source_72;
            tmp_assign_source_72 = PyDict_New();
            assert( tmp_class_creation_6__prepared == NULL );
            tmp_class_creation_6__prepared = tmp_assign_source_72;
        }
        branch_end_22:;
    }
    {
        PyObject *tmp_assign_source_73;
        {
            PyObject *tmp_set_locals_6;
            CHECK_OBJECT( tmp_class_creation_6__prepared );
            tmp_set_locals_6 = tmp_class_creation_6__prepared;
            locals_tornado$httpclient_726 = tmp_set_locals_6;
            Py_INCREF( tmp_set_locals_6 );
        }
        // Tried code:
        // Tried code:
        tmp_dictset_value = const_str_digest_6f55736fcd57507504cc7f0643fbf21f;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_726, const_str_plain___module__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_22;
        }
        tmp_dictset_value = const_str_digest_5180ce7a3e8dd5fcafd1ba4691d5bfb2;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_726, const_str_plain___doc__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_22;
        }
        tmp_dictset_value = const_str_plain__RequestProxy;
        tmp_res = PyObject_SetItem( locals_tornado$httpclient_726, const_str_plain___qualname__, tmp_dictset_value );
        if ( tmp_res != 0 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 726;

            goto try_except_handler_22;
        }
        MAKE_OR_REUSE_FRAME( cache_frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7, codeobj_0d8ab451af9278fbd65fc25aa6fc0cb3, module_tornado$httpclient, sizeof(void *) );
        frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 = cache_frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7;

        // Push the new frame as the currently active one.
        pushFrameStack( frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 );

        // Mark the frame object as in use, ref count 1 will be up for reuse.
        assert( Py_REFCNT( frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 ) == 2 ); // Frame stack

        // Framed code:
        {
            PyObject *tmp_annotations_32;
            PyObject *tmp_dict_key_92;
            PyObject *tmp_dict_value_92;
            PyObject *tmp_mvar_value_57;
            PyObject *tmp_dict_key_93;
            PyObject *tmp_dict_value_93;
            PyObject *tmp_subscribed_name_34;
            PyObject *tmp_mvar_value_58;
            PyObject *tmp_subscript_name_34;
            PyObject *tmp_subscribed_name_35;
            PyObject *tmp_mvar_value_59;
            PyObject *tmp_subscript_name_35;
            PyObject *tmp_tuple_element_39;
            PyObject *tmp_mvar_value_60;
            PyObject *tmp_dict_key_94;
            PyObject *tmp_dict_value_94;
            tmp_dict_key_92 = const_str_plain_request;
            tmp_dict_value_92 = PyObject_GetItem( locals_tornado$httpclient_726, const_str_plain_HTTPRequest );

            if ( tmp_dict_value_92 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_57 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_HTTPRequest );

                if (unlikely( tmp_mvar_value_57 == NULL ))
                {
                    tmp_mvar_value_57 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_HTTPRequest );
                }

                if ( tmp_mvar_value_57 == NULL )
                {

                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "HTTPRequest" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 733;
                    type_description_2 = "o";
                    goto frame_exception_exit_7;
                }

                tmp_dict_value_92 = tmp_mvar_value_57;
                Py_INCREF( tmp_dict_value_92 );
                }
            }

            tmp_annotations_32 = _PyDict_NewPresized( 3 );
            tmp_res = PyDict_SetItem( tmp_annotations_32, tmp_dict_key_92, tmp_dict_value_92 );
            Py_DECREF( tmp_dict_value_92 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_93 = const_str_plain_defaults;
            tmp_subscribed_name_34 = PyObject_GetItem( locals_tornado$httpclient_726, const_str_plain_Optional );

            if ( tmp_subscribed_name_34 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_58 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Optional );

                if (unlikely( tmp_mvar_value_58 == NULL ))
                {
                    tmp_mvar_value_58 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Optional );
                }

                if ( tmp_mvar_value_58 == NULL )
                {
                    Py_DECREF( tmp_annotations_32 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Optional" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 733;
                    type_description_2 = "o";
                    goto frame_exception_exit_7;
                }

                tmp_subscribed_name_34 = tmp_mvar_value_58;
                Py_INCREF( tmp_subscribed_name_34 );
                }
            }

            tmp_subscribed_name_35 = PyObject_GetItem( locals_tornado$httpclient_726, const_str_plain_Dict );

            if ( tmp_subscribed_name_35 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_59 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Dict );

                if (unlikely( tmp_mvar_value_59 == NULL ))
                {
                    tmp_mvar_value_59 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Dict );
                }

                if ( tmp_mvar_value_59 == NULL )
                {
                    Py_DECREF( tmp_annotations_32 );
                    Py_DECREF( tmp_subscribed_name_34 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Dict" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 733;
                    type_description_2 = "o";
                    goto frame_exception_exit_7;
                }

                tmp_subscribed_name_35 = tmp_mvar_value_59;
                Py_INCREF( tmp_subscribed_name_35 );
                }
            }

            tmp_tuple_element_39 = PyObject_GetItem( locals_tornado$httpclient_726, const_str_plain_str );

            if ( tmp_tuple_element_39 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_tuple_element_39 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_tuple_element_39 );
                }
            }

            tmp_subscript_name_35 = PyTuple_New( 2 );
            PyTuple_SET_ITEM( tmp_subscript_name_35, 0, tmp_tuple_element_39 );
            tmp_tuple_element_39 = PyObject_GetItem( locals_tornado$httpclient_726, const_str_plain_Any );

            if ( tmp_tuple_element_39 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_60 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_60 == NULL ))
                {
                    tmp_mvar_value_60 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_60 == NULL )
                {
                    Py_DECREF( tmp_annotations_32 );
                    Py_DECREF( tmp_subscribed_name_34 );
                    Py_DECREF( tmp_subscribed_name_35 );
                    Py_DECREF( tmp_subscript_name_35 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 733;
                    type_description_2 = "o";
                    goto frame_exception_exit_7;
                }

                tmp_tuple_element_39 = tmp_mvar_value_60;
                Py_INCREF( tmp_tuple_element_39 );
                }
            }

            PyTuple_SET_ITEM( tmp_subscript_name_35, 1, tmp_tuple_element_39 );
            tmp_subscript_name_34 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_35, tmp_subscript_name_35 );
            Py_DECREF( tmp_subscribed_name_35 );
            Py_DECREF( tmp_subscript_name_35 );
            if ( tmp_subscript_name_34 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_32 );
                Py_DECREF( tmp_subscribed_name_34 );

                exception_lineno = 733;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }
            tmp_dict_value_93 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_34, tmp_subscript_name_34 );
            Py_DECREF( tmp_subscribed_name_34 );
            Py_DECREF( tmp_subscript_name_34 );
            if ( tmp_dict_value_93 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_annotations_32 );

                exception_lineno = 733;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }
            tmp_res = PyDict_SetItem( tmp_annotations_32, tmp_dict_key_93, tmp_dict_value_93 );
            Py_DECREF( tmp_dict_value_93 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_94 = const_str_plain_return;
            tmp_dict_value_94 = Py_None;
            tmp_res = PyDict_SetItem( tmp_annotations_32, tmp_dict_key_94, tmp_dict_value_94 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_25___init__( tmp_annotations_32 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_726, const_str_plain___init__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 732;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }
        }
        {
            PyObject *tmp_annotations_33;
            PyObject *tmp_dict_key_95;
            PyObject *tmp_dict_value_95;
            PyObject *tmp_dict_key_96;
            PyObject *tmp_dict_value_96;
            PyObject *tmp_mvar_value_61;
            tmp_dict_key_95 = const_str_plain_name;
            tmp_dict_value_95 = PyObject_GetItem( locals_tornado$httpclient_726, const_str_plain_str );

            if ( tmp_dict_value_95 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_dict_value_95 = (PyObject *)&PyUnicode_Type;
                Py_INCREF( tmp_dict_value_95 );
                }
            }

            tmp_annotations_33 = _PyDict_NewPresized( 2 );
            tmp_res = PyDict_SetItem( tmp_annotations_33, tmp_dict_key_95, tmp_dict_value_95 );
            Py_DECREF( tmp_dict_value_95 );
            assert( !(tmp_res != 0) );
            tmp_dict_key_96 = const_str_plain_return;
            tmp_dict_value_96 = PyObject_GetItem( locals_tornado$httpclient_726, const_str_plain_Any );

            if ( tmp_dict_value_96 == NULL )
            {
                if ( CHECK_AND_CLEAR_KEY_ERROR_OCCURRED() )
                {
                tmp_mvar_value_61 = GET_STRING_DICT_VALUE( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_Any );

                if (unlikely( tmp_mvar_value_61 == NULL ))
                {
                    tmp_mvar_value_61 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Any );
                }

                if ( tmp_mvar_value_61 == NULL )
                {
                    Py_DECREF( tmp_annotations_33 );
                    exception_type = PyExc_NameError;
                    Py_INCREF( exception_type );
                    exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Any" );
                    exception_tb = NULL;
                    NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                    CHAIN_EXCEPTION( exception_value );

                    exception_lineno = 738;
                    type_description_2 = "o";
                    goto frame_exception_exit_7;
                }

                tmp_dict_value_96 = tmp_mvar_value_61;
                Py_INCREF( tmp_dict_value_96 );
                }
            }

            tmp_res = PyDict_SetItem( tmp_annotations_33, tmp_dict_key_96, tmp_dict_value_96 );
            Py_DECREF( tmp_dict_value_96 );
            assert( !(tmp_res != 0) );
            tmp_dictset_value = MAKE_FUNCTION_tornado$httpclient$$$function_26___getattr__( tmp_annotations_33 );



            tmp_res = PyObject_SetItem( locals_tornado$httpclient_726, const_str_plain___getattr__, tmp_dictset_value );
            Py_DECREF( tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 738;
                type_description_2 = "o";
                goto frame_exception_exit_7;
            }
        }

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 );
#endif

        // Put the previous frame back on top.
        popFrameStack();

        goto frame_no_exception_6;

        frame_exception_exit_7:;

#if 0
        RESTORE_FRAME_EXCEPTION( frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 );
#endif

        if ( exception_tb == NULL )
        {
            exception_tb = MAKE_TRACEBACK( frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7, exception_lineno );
        }
        else if ( exception_tb->tb_frame != &frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7->m_frame )
        {
            exception_tb = ADD_TRACEBACK( exception_tb, frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7, exception_lineno );
        }

        // Attachs locals to frame if any.
        Nuitka_Frame_AttachLocals(
            (struct Nuitka_FrameObject *)frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7,
            type_description_2,
            outline_5_var___class__
        );


        // Release cached frame.
        if ( frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 == cache_frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 )
        {
            Py_DECREF( frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 );
        }
        cache_frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 = NULL;

        assertFrameObject( frame_0d8ab451af9278fbd65fc25aa6fc0cb3_7 );

        // Put the previous frame back on top.
        popFrameStack();

        // Return the error.
        goto nested_frame_exit_6;

        frame_no_exception_6:;
        goto skip_nested_handling_6;
        nested_frame_exit_6:;

        goto try_except_handler_22;
        skip_nested_handling_6:;
        {
            nuitka_bool tmp_condition_result_44;
            PyObject *tmp_compexpr_left_6;
            PyObject *tmp_compexpr_right_6;
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_compexpr_left_6 = tmp_class_creation_6__bases;
            tmp_compexpr_right_6 = const_tuple_type_object_tuple;
            tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_6, tmp_compexpr_right_6 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 726;

                goto try_except_handler_22;
            }
            tmp_condition_result_44 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_44 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_24;
            }
            else
            {
                goto branch_no_24;
            }
            branch_yes_24:;
            tmp_dictset_value = const_tuple_type_object_tuple;
            tmp_res = PyObject_SetItem( locals_tornado$httpclient_726, const_str_plain___orig_bases__, tmp_dictset_value );
            if ( tmp_res != 0 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 726;

                goto try_except_handler_22;
            }
            branch_no_24:;
        }
        {
            PyObject *tmp_assign_source_74;
            PyObject *tmp_called_name_25;
            PyObject *tmp_args_name_12;
            PyObject *tmp_tuple_element_40;
            PyObject *tmp_kw_name_13;
            CHECK_OBJECT( tmp_class_creation_6__metaclass );
            tmp_called_name_25 = tmp_class_creation_6__metaclass;
            tmp_tuple_element_40 = const_str_plain__RequestProxy;
            tmp_args_name_12 = PyTuple_New( 3 );
            Py_INCREF( tmp_tuple_element_40 );
            PyTuple_SET_ITEM( tmp_args_name_12, 0, tmp_tuple_element_40 );
            CHECK_OBJECT( tmp_class_creation_6__bases );
            tmp_tuple_element_40 = tmp_class_creation_6__bases;
            Py_INCREF( tmp_tuple_element_40 );
            PyTuple_SET_ITEM( tmp_args_name_12, 1, tmp_tuple_element_40 );
            tmp_tuple_element_40 = locals_tornado$httpclient_726;
            Py_INCREF( tmp_tuple_element_40 );
            PyTuple_SET_ITEM( tmp_args_name_12, 2, tmp_tuple_element_40 );
            CHECK_OBJECT( tmp_class_creation_6__class_decl_dict );
            tmp_kw_name_13 = tmp_class_creation_6__class_decl_dict;
            frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame.f_lineno = 726;
            tmp_assign_source_74 = CALL_FUNCTION( tmp_called_name_25, tmp_args_name_12, tmp_kw_name_13 );
            Py_DECREF( tmp_args_name_12 );
            if ( tmp_assign_source_74 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 726;

                goto try_except_handler_22;
            }
            assert( outline_5_var___class__ == NULL );
            outline_5_var___class__ = tmp_assign_source_74;
        }
        CHECK_OBJECT( outline_5_var___class__ );
        tmp_assign_source_73 = outline_5_var___class__;
        Py_INCREF( tmp_assign_source_73 );
        goto try_return_handler_22;
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_22:;
        Py_DECREF( locals_tornado$httpclient_726 );
        locals_tornado$httpclient_726 = NULL;
        goto try_return_handler_21;
        // Exception handler code:
        try_except_handler_22:;
        exception_keeper_type_20 = exception_type;
        exception_keeper_value_20 = exception_value;
        exception_keeper_tb_20 = exception_tb;
        exception_keeper_lineno_20 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        Py_DECREF( locals_tornado$httpclient_726 );
        locals_tornado$httpclient_726 = NULL;
        // Re-raise.
        exception_type = exception_keeper_type_20;
        exception_value = exception_keeper_value_20;
        exception_tb = exception_keeper_tb_20;
        exception_lineno = exception_keeper_lineno_20;

        goto try_except_handler_21;
        // End of try:
        // tried codes exits in all cases
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        // Return handler code:
        try_return_handler_21:;
        CHECK_OBJECT( (PyObject *)outline_5_var___class__ );
        Py_DECREF( outline_5_var___class__ );
        outline_5_var___class__ = NULL;

        goto outline_result_6;
        // Exception handler code:
        try_except_handler_21:;
        exception_keeper_type_21 = exception_type;
        exception_keeper_value_21 = exception_value;
        exception_keeper_tb_21 = exception_tb;
        exception_keeper_lineno_21 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        // Re-raise.
        exception_type = exception_keeper_type_21;
        exception_value = exception_keeper_value_21;
        exception_tb = exception_keeper_tb_21;
        exception_lineno = exception_keeper_lineno_21;

        goto outline_exception_6;
        // End of try:
        // Return statement must have exited already.
        NUITKA_CANNOT_GET_HERE( tornado$httpclient );
        return MOD_RETURN_VALUE( NULL );
        outline_exception_6:;
        exception_lineno = 726;
        goto try_except_handler_20;
        outline_result_6:;
        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain__RequestProxy, tmp_assign_source_73 );
    }
    goto try_end_10;
    // Exception handler code:
    try_except_handler_20:;
    exception_keeper_type_22 = exception_type;
    exception_keeper_value_22 = exception_value;
    exception_keeper_tb_22 = exception_tb;
    exception_keeper_lineno_22 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_class_creation_6__bases );
    tmp_class_creation_6__bases = NULL;

    Py_XDECREF( tmp_class_creation_6__class_decl_dict );
    tmp_class_creation_6__class_decl_dict = NULL;

    Py_XDECREF( tmp_class_creation_6__metaclass );
    tmp_class_creation_6__metaclass = NULL;

    Py_XDECREF( tmp_class_creation_6__prepared );
    tmp_class_creation_6__prepared = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_22;
    exception_value = exception_keeper_value_22;
    exception_tb = exception_keeper_tb_22;
    exception_lineno = exception_keeper_lineno_22;

    goto frame_exception_exit_1;
    // End of try:
    try_end_10:;

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e51e71200c4d41bba590fb0c4f6851d0 );
#endif
    popFrameStack();

    assertFrameObject( frame_e51e71200c4d41bba590fb0c4f6851d0 );

    goto frame_no_exception_7;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_e51e71200c4d41bba590fb0c4f6851d0 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e51e71200c4d41bba590fb0c4f6851d0, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e51e71200c4d41bba590fb0c4f6851d0->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e51e71200c4d41bba590fb0c4f6851d0, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_7:;
    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__bases );
    Py_DECREF( tmp_class_creation_6__bases );
    tmp_class_creation_6__bases = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__class_decl_dict );
    Py_DECREF( tmp_class_creation_6__class_decl_dict );
    tmp_class_creation_6__class_decl_dict = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__metaclass );
    Py_DECREF( tmp_class_creation_6__metaclass );
    tmp_class_creation_6__metaclass = NULL;

    CHECK_OBJECT( (PyObject *)tmp_class_creation_6__prepared );
    Py_DECREF( tmp_class_creation_6__prepared );
    tmp_class_creation_6__prepared = NULL;

    {
        PyObject *tmp_assign_source_75;
        PyObject *tmp_annotations_34;
        tmp_annotations_34 = PyDict_Copy( const_dict_056a293e2058d56276328e53ff09a8b9 );
        tmp_assign_source_75 = MAKE_FUNCTION_tornado$httpclient$$$function_27_main( tmp_annotations_34 );



        UPDATE_STRING_DICT1( moduledict_tornado$httpclient, (Nuitka_StringObject *)const_str_plain_main, tmp_assign_source_75 );
    }

    return MOD_RETURN_VALUE( module_tornado$httpclient );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
