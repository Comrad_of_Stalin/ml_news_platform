/* Generated code for Python module 'prompt_toolkit.key_binding.bindings.named_commands'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$key_binding$bindings$named_commands" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$key_binding$bindings$named_commands;
PyDictObject *moduledict_prompt_toolkit$key_binding$bindings$named_commands;

/* The declarations of module constants used, if any. */
static PyObject *const_str_digest_072123a03afbbc4a30fb698f3e63aac0;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain_macro;
extern PyObject *const_str_plain_i;
static PyObject *const_str_plain_next_history;
static PyObject *const_str_plain_redraw_current_line;
extern PyObject *const_str_digest_2341ccadcc0716db92a6611c3a0c5709;
static PyObject *const_str_digest_b957e174ba0fdf03669f960ad9df6dd4;
extern PyObject *const_str_plain_delete_before_cursor;
static PyObject *const_str_digest_48235e17fb80f346bb9d79f90caba172;
static PyObject *const_str_digest_d170e87999e8451393080db2ed4cfc68;
static PyObject *const_tuple_str_plain_event_str_plain_b_str_plain_p_tuple;
extern PyObject *const_tuple_str_plain_key_binding_tuple;
extern PyObject *const_tuple_str_digest_fb5c11df9835340c3dd4a642c111b2eb_tuple;
extern PyObject *const_str_plain_None;
static PyObject *const_str_digest_31c4b630f4ffd96ff20e45c83bc6d08e;
static PyObject *const_tuple_str_plain_k_str_plain_event_tuple;
static PyObject *const_str_plain_reverse_search_history;
static PyObject *const_str_digest_5b1ffed3e4be8aa5f4cb3887c232c8e1;
extern PyObject *const_str_digest_ffa4782c58075c6dd84b572edd3109a7;
static PyObject *const_str_digest_8050ff83554e3b5eef4c5274df33464c;
extern PyObject *const_str_plain___debug__;
static PyObject *const_str_digest_6435b37fd837c6013a16282357bd256d;
static PyObject *const_str_digest_968f9b527a999c6f0eb6156bb8866021;
extern PyObject *const_str_plain_arg_present;
extern PyObject *const_str_plain_change;
extern PyObject *const_str_digest_402d95161f3800c90c59735f4d8c4c59;
static PyObject *const_str_digest_85dadd9de2ac2ff25d2fd136dc90bc72;
extern PyObject *const_str_plain_six;
extern PyObject *const_str_plain_clipboard;
static PyObject *const_str_plain_downcase_word;
static PyObject *const_str_plain_previous_history;
extern PyObject *const_str_digest_70508f93fca682b73b6276fb31ac0cad;
extern PyObject *const_str_plain_Escape;
static PyObject *const_str_plain_forward_char;
extern PyObject *const_str_plain_feed_multiple;
extern PyObject *const_str_plain_event;
static PyObject *const_str_digest_34910ada2a9571c81d0198bdebe83810;
extern PyObject *const_str_plain_emacs_state;
extern PyObject *const_tuple_str_digest_4e5a5ed7c51e9863d1e36202b87fe43e_tuple;
extern PyObject *const_str_plain_Keys;
extern PyObject *const_str_plain_complete_previous;
extern PyObject *const_str_plain_name;
static PyObject *const_str_digest_ce90e5fd750ebe5fbf3c059d5d422d22;
extern PyObject *const_str_plain_quoted_insert;
extern PyObject *const_str_digest_908c8ae8a1ea88b9446eaa47eb000fad;
extern PyObject *const_str_plain_cursor_position_col;
extern PyObject *const_str_plain_complete;
extern PyObject *const_tuple_int_0_tuple;
static PyObject *const_str_plain_edit_and_execute;
extern PyObject *const_str_plain_map;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_digest_bb2edfc35430ba0550dc023a5379a967;
extern PyObject *const_tuple_str_digest_402d95161f3800c90c59735f4d8c4c59_tuple;
extern PyObject *const_str_plain_run_in_terminal;
extern PyObject *const_str_digest_bec9411c13b58be3a688ffe0c42ba2da;
extern PyObject *const_str_plain_open_in_editor;
static PyObject *const_str_plain_end_kbd_macro;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_bell;
extern PyObject *const_str_plain_rotate;
extern PyObject *const_str_plain_cursor_position;
extern PyObject *const_str_plain_register;
static PyObject *const_str_digest_d2154be122eab39bb0280421ac483f4e;
static PyObject *const_str_digest_9d4e7794e591dce4cbbd54460a027e3f;
static PyObject *const_str_plain_delete_before;
static PyObject *const_str_plain_beginning_of_line;
static PyObject *const_str_plain_call_last_kbd_macro;
extern PyObject *const_str_plain_buff;
extern PyObject *const_str_plain_get_data;
extern PyObject *const_str_digest_8ac25a148822dcf205845c372c142dc5;
static PyObject *const_str_digest_3f4d599e74335620cbcb6d738c0fe3fc;
extern PyObject *const_str_digest_4d94ac19d21ce83f39e74e91d067ed52;
static PyObject *const_str_plain_menu_complete;
extern PyObject *const_str_plain_Document;
static PyObject *const_tuple_str_plain_name_str_plain_decorator_tuple;
extern PyObject *const_str_digest_ae692cb46ab2cf52c0be9c11d31b5318;
extern PyObject *const_str_plain_EditingMode;
static PyObject *const_str_plain_delete_after;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_exit;
static PyObject *const_str_digest_74f41a8411a3e103f4e1c3a33b0e5662;
extern PyObject *const_str_plain_VI;
static PyObject *const_tuple_str_digest_5b1ffed3e4be8aa5f4cb3887c232c8e1_tuple;
static PyObject *const_tuple_2585165d60931c7b7dc1b2c728b4f332_tuple;
extern PyObject *const_str_plain_generate_completions;
extern PyObject *const_str_digest_d2a5158fd7bb2018f91aee17fcfed439;
extern PyObject *const_tuple_str_digest_c8c3694ea6fe9becb903618b30b45e7b_tuple;
extern PyObject *const_int_pos_1;
static PyObject *const_str_digest_66d9c352af7ed5132c71e2f398933b45;
static PyObject *const_str_plain_beginning_of_history;
static PyObject *const_str_digest_274b6eb2a65f09ccc339d29eea36ebd9;
static PyObject *const_str_plain_delete_horizontal_space;
extern PyObject *const_str_digest_256dc4d88752a2b798517347adc6bf9a;
extern PyObject *const_tuple_str_digest_4e2aeb083e5702a53bc119c9bb3ab9d7_tuple;
static PyObject *const_tuple_str_digest_e00c6f9ddf82e0ad26674bf1f89e79b3_tuple;
extern PyObject *const_str_plain_get_by_name;
static PyObject *const_str_digest_8ab7b8386ed49775e91498682cd2986c;
extern PyObject *const_str_plain_document_before_paste;
static PyObject *const_str_digest_78bb55f89abb5b3ad76eb473479dd6a3;
extern PyObject *const_tuple_str_digest_204c920806e032799a87379e1294bcfc_tuple;
extern PyObject *const_str_digest_820ffb9c299a4fe53ab8241c74e4288a;
extern PyObject *const_str_plain_get_start_of_line_position;
static PyObject *const_str_plain_vi_editing_mode;
extern PyObject *const_str_plain_startswith;
extern PyObject *const_str_plain_find_previous_word_beginning;
extern PyObject *const_str_plain_feed;
static PyObject *const_str_plain_backward_kill_word;
extern PyObject *const_str_plain_history_backward;
static PyObject *const_str_digest_0917fbd04b33410fa0c2d713b22ffff2;
extern PyObject *const_str_plain_decorator;
extern PyObject *const_tuple_str_digest_bec9411c13b58be3a688ffe0c42ba2da_tuple;
extern PyObject *const_str_digest_d4d66fa4b26f35cc7cfe3fc57dce3f65;
static PyObject *const_tuple_855c21556e24f864827dd0797c057760_tuple;
static PyObject *const_str_digest_1bad627ed19ed28ad2101a569d58d565;
extern PyObject *const_tuple_str_plain_line_tuple;
static PyObject *const_str_digest_02a5690946f1cc1ce0757fc48394bbff;
extern PyObject *const_slice_int_pos_1_none_none;
extern PyObject *const_str_plain___file__;
static PyObject *const_str_plain_set_working_index;
extern PyObject *const_str_plain_lstrip;
static PyObject *const_str_digest_bac376679fee18ab8e8f621bfb04b25e;
static PyObject *const_str_digest_17a581addb5f14b5622a3275e5609824;
static PyObject *const_str_plain_start_kbd_macro;
extern PyObject *const_str_digest_7e022856dd1b5bd6663571c7df813303;
extern PyObject *const_tuple_str_digest_d306eb7c71f9186853263120b43be12e_tuple;
static PyObject *const_str_digest_bff9c642660585f4a91209646bf69a4a;
extern PyObject *const_tuple_str_plain_EditingMode_tuple;
static PyObject *const_str_digest_a519a736eca93ebf6d46cc412c173add;
extern PyObject *const_tuple_str_digest_c014cbade4743da3cd8e8b1fadea68a6_tuple;
extern PyObject *const_tuple_int_pos_1_tuple;
extern PyObject *const_str_plain_find_start_of_previous_word;
extern PyObject *const_str_plain_lower;
extern PyObject *const_tuple_str_digest_bb2edfc35430ba0550dc023a5379a967_tuple;
extern PyObject *const_str_plain_KeyPress;
extern PyObject *const_str_plain_paste_clipboard_data;
extern PyObject *const_tuple_str_plain_KeyPress_tuple;
static PyObject *const_str_digest_b6c306f5553e75c8ab5c659e7b1bb0c0;
extern PyObject *const_str_digest_0adea339c095a653a19ec3c66bfd3784;
extern PyObject *const_str_plain_yank;
extern PyObject *const_str_plain_n;
static PyObject *const_str_digest_7f3dc17e1f79ad0755aafac73dd0d273;
extern PyObject *const_tuple_str_digest_af1adbb7bec7eaa2f6c08a25f5ab2b5d_tuple;
static PyObject *const_tuple_str_plain_event_str_plain_n_tuple;
extern PyObject *const_str_plain_p;
static PyObject *const_str_digest_eac4292b628774117c5284605831d6d0;
extern PyObject *const_str_plain_current_char;
extern PyObject *const_str_plain_editing_mode;
extern PyObject *const_str_digest_0a70b85b2fe4fc5624d81d2e6686c384;
static PyObject *const_str_digest_67055978b31b0cd8ee70ae4eac029a8f;
extern PyObject *const_str_digest_d306eb7c71f9186853263120b43be12e;
extern PyObject *const_str_digest_fe8d6ea5daee2fa16a6d6e2bae6159bb;
static PyObject *const_str_plain_print_last_kbd_macro;
extern PyObject *const_str_plain_after_whitespace;
static PyObject *const_str_digest_4650e344edbd2542c4421b2b140d04da;
extern PyObject *const_str_digest_a04f324144c20908fd5b501ea5e5f924;
static PyObject *const_str_plain_uppercase_word;
extern PyObject *const_str_plain_k;
static PyObject *const_tuple_str_digest_400d834c6f1d5d5d7b9185420395cb8b_tuple;
extern PyObject *const_str_plain___all__;
static PyObject *const_str_digest_793ec132e5b39c0badda11bae0171c7a;
static PyObject *const_str_plain_capitalize_word;
static PyObject *const_list_str_plain_get_by_name_list;
extern PyObject *const_str_digest_c014cbade4743da3cd8e8b1fadea68a6;
static PyObject *const_tuple_str_plain_event_str_plain_deleted_tuple;
extern PyObject *const_str_plain_upper;
static PyObject *const_str_digest_b7ccea59c184956e0e6390de033e4e13;
extern PyObject *const_str_digest_23b6565e2fad8b5df75562a1b722c5fa;
extern PyObject *const_str_plain_text;
extern PyObject *const_str_plain_origin;
static PyObject *const_tuple_str_plain_handler_str_plain_name_tuple;
extern PyObject *const_tuple_str_plain_run_in_terminal_tuple;
extern PyObject *const_tuple_str_plain_event_str_plain_buff_tuple;
extern PyObject *const_tuple_str_digest_0adea339c095a653a19ec3c66bfd3784_tuple;
extern PyObject *const_str_plain_current_buffer;
static PyObject *const_tuple_str_digest_94831ee4810e73b56026b83acc214eaf_tuple;
extern PyObject *const_str_plain_new_index;
static PyObject *const_tuple_e4126eaf674adc64e483dadb1d72998f_tuple;
extern PyObject *const_str_plain_words;
extern PyObject *const_str_plain_range;
extern PyObject *const_str_digest_e6c13694f23bea8b3c2d2e215abd5980;
static PyObject *const_str_plain_doc_before_paste;
static PyObject *const_tuple_fd2995ae22a9b8ea312ca0255b92783a_tuple;
extern PyObject *const_str_digest_e13b285e26a38d1a90b94ccc0668450c;
extern PyObject *const_str_plain_record_in_macro;
static PyObject *const_str_plain_yank_pop;
extern PyObject *const_dict_d96fc9db79024853de9fbab06f7c41d9;
extern PyObject *const_tuple_str_digest_fe8d6ea5daee2fa16a6d6e2bae6159bb_tuple;
static PyObject *const_tuple_str_plain_event_str_plain_buff_str_plain_change_tuple;
extern PyObject *const_tuple_str_digest_23b6565e2fad8b5df75562a1b722c5fa_tuple;
extern PyObject *const_tuple_str_plain_undo_tuple;
extern PyObject *const_str_plain_get_cursor_left_position;
extern PyObject *const_str_plain_yank_last_arg;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_plain_delete;
extern PyObject *const_str_plain_text_before_cursor;
extern PyObject *const_tuple_str_digest_e19d832c39da60eea3d5c4896bf12fa2_tuple;
extern PyObject *const_str_plain_insert_text;
static PyObject *const_str_plain_backward_delete_char;
extern PyObject *const_str_plain_yank_nth_arg;
extern PyObject *const_str_plain_get_end_of_line_position;
extern PyObject *const_str_plain_current_search_state;
extern PyObject *const_str_plain_SearchDirection;
static PyObject *const_tuple_2502f7b4cbd494f46f966272e92176d2_tuple;
extern PyObject *const_str_digest_e19d832c39da60eea3d5c4896bf12fa2;
extern PyObject *const_str_plain__working_lines;
static PyObject *const_str_digest_bd43b33ede5b6fb74b5a7494f79f0067;
static PyObject *const_str_plain_forward_word;
extern PyObject *const_tuple_str_digest_647f11624bc526afd8ce034dc78066a1_tuple;
extern PyObject *const_tuple_str_plain_event_tuple;
extern PyObject *const_str_digest_204c920806e032799a87379e1294bcfc;
extern PyObject *const_str_plain_validate_and_handle;
static PyObject *const_tuple_str_plain_event_str_plain_buff_str_plain_pos_tuple;
static PyObject *const_tuple_04b5f32bde1726e2e6564bface78b67f_tuple;
extern PyObject *const_tuple_str_chr_35_tuple;
static PyObject *const_str_digest_58304b5642f2a8741eb67b7c2ad22ce5;
static PyObject *const_str_digest_60701c7754d852c86a2f19de3267c311;
static PyObject *const_str_digest_3ef4f4689543f14060661203b3f01cf9;
extern PyObject *const_str_digest_f6826d177b892c516eb853c8574210e2;
extern PyObject *const_tuple_true_tuple;
extern PyObject *const_tuple_str_plain_yank_tuple;
extern PyObject *const_str_plain_paste_mode;
extern PyObject *const_str_plain_pos;
extern PyObject *const_str_digest_4f8c7230e3d1a51c6fa37895ee70f054;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_tuple_str_digest_4f8c7230e3d1a51c6fa37895ee70f054_tuple;
extern PyObject *const_str_digest_6695b0a823c8cd6b75b68a2528261696;
static PyObject *const_str_plain_prefix_meta;
extern PyObject *const_str_plain_data;
extern PyObject *const_tuple_str_digest_3e8aa0ff62897226c7230752efac5de8_tuple;
extern PyObject *const_tuple_str_digest_d2a5158fd7bb2018f91aee17fcfed439_tuple;
extern PyObject *const_str_plain_is_repeat;
extern PyObject *const_tuple_str_digest_d4d66fa4b26f35cc7cfe3fc57dce3f65_tuple;
extern PyObject *const_str_plain_control;
static PyObject *const_str_digest_e6f30477bcc260ba4ea547b3b2911818;
static PyObject *const_str_digest_6770944cf34737f0fd1c0ae57ea84e00;
extern PyObject *const_tuple_str_digest_21e45c1f878d59fe84fced59f3e10b63_tuple;
static PyObject *const_tuple_str_plain_event_str_plain_buff_str_plain_deleted_tuple;
extern PyObject *const_str_digest_647f11624bc526afd8ce034dc78066a1;
extern PyObject *const_dict_e2d16c98e7cb03ba7fd208a07fd5d185;
static PyObject *const_str_digest_2a8fcad8b385b0c88d0194c208d97103;
extern PyObject *const_str_plain_direction;
static PyObject *const_str_digest_9cc1ee391748caad62e066f6aaa843a5;
extern PyObject *const_str_chr_35;
extern PyObject *const_str_digest_b7cd7d3061b29ef6badd990d7c83ddb1;
extern PyObject *const_str_digest_21e45c1f878d59fe84fced59f3e10b63;
extern PyObject *const_tuple_str_digest_2341ccadcc0716db92a6611c3a0c5709_tuple;
static PyObject *const_str_plain_kill_line;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_plain_start_macro;
extern PyObject *const_str_plain_append;
static PyObject *const_str_digest_7d1c0c5af5d1875a6a400642e2bc6d31;
static PyObject *const_str_plain_print_macro;
extern PyObject *const_str_plain_key_binding;
extern PyObject *const_tuple_str_digest_ffa4782c58075c6dd84b572edd3109a7_tuple;
static PyObject *const_str_digest_ade7a4439193c90488d0fdc4fa908f0e;
extern PyObject *const_tuple_str_digest_10c978016ae019e97a4da8b4078edf46_tuple;
static PyObject *const_str_plain_emacs_editing_mode;
static PyObject *const_str_digest_ff960a63a7cd023625d10d8afb7b75af;
extern PyObject *const_str_plain_b;
static PyObject *const_str_digest_f3142aabc8984e9b5ee4284ee94fc9e1;
static PyObject *const_dict_47be3150a6234742830e43bdf5d5778f;
static PyObject *const_tuple_str_digest_4650e344edbd2542c4421b2b140d04da_tuple;
static PyObject *const_dict_2de9ada199d63ba5bdc7c9b31335d708;
extern PyObject *const_str_plain_completion;
extern PyObject *const_tuple_str_digest_f8c276bab5a9ae14be8475d6e0e75e09_tuple;
extern PyObject *const_str_plain_renderer;
extern PyObject *const_str_plain_first;
extern PyObject *const_str_plain_current_control;
extern PyObject *const_str_digest_4dcf85ba674584b7335275c899c0ab24;
static PyObject *const_str_plain_insert_comment;
extern PyObject *const_str_plain_set_text;
extern PyObject *const_str_digest_f8c276bab5a9ae14be8475d6e0e75e09;
static PyObject *const_str_plain_operate_and_get_next;
static PyObject *const_str_plain_transpose_chars;
static PyObject *const_tuple_str_plain_event_str_plain_control_tuple;
static PyObject *const_tuple_str_plain_complete_tuple;
extern PyObject *const_str_plain_get_cursor_right_position;
extern PyObject *const_str_plain_app;
extern PyObject *const_tuple_str_digest_820ffb9c299a4fe53ab8241c74e4288a_tuple;
static PyObject *const_str_digest_2ea096c33d6acdbf8f658453d87483a6;
extern PyObject *const_str_digest_637ae2db1c15757d3f68020910fc7af5;
static PyObject *const_str_plain_unix_line_discard;
static PyObject *const_str_digest_9836aff840b931e5ea309144651873e0;
extern PyObject *const_str_plain_text_type;
extern PyObject *const_str_plain_go_to_history;
extern PyObject *const_str_plain_print;
static PyObject *const_str_digest_86fc33ac35792a7b74893a235817abff;
extern PyObject *const_str_plain_clear_screen;
static PyObject *const_str_digest_feff521061f9718c2c4575ee30698cef;
static PyObject *const_str_digest_1beb6b9754702adc084aa39d18c50ba8;
extern PyObject *const_str_plain_swap_characters_before_cursor;
static PyObject *const_tuple_str_plain_new_index_str_plain_buff_tuple;
extern PyObject *const_str_digest_c1dec28d38befd226a053f4c881a493b;
static PyObject *const_str_digest_3dbaa58e29edd8b0fa66cb30e90e21b6;
static PyObject *const_str_plain_end_of_line;
extern PyObject *const_str_plain_arg;
static PyObject *const_str_plain_menu_complete_backward;
extern PyObject *const_str_plain_search_buffer_control;
extern PyObject *const_str_digest_ebac526994d665b2afb9731ed9b7a23c;
extern PyObject *const_str_digest_3e8aa0ff62897226c7230752efac5de8;
extern PyObject *const_tuple_str_plain_Keys_tuple;
extern PyObject *const_str_digest_af1adbb7bec7eaa2f6c08a25f5ab2b5d;
static PyObject *const_str_plain_kill_word;
extern PyObject *const_str_plain_layout;
static PyObject *const_str_digest_a382cc8c0cded4a250815b426d58ad03;
extern PyObject *const_tuple_str_digest_bc52841ed7d0c0e991090611ebb55c62_tuple;
extern PyObject *const_str_plain_splitlines;
static PyObject *const_str_digest_56ddd1cd56382cd88f10c4ba7452feda;
extern PyObject *const_str_plain_join;
static PyObject *const_str_plain_unix_word_rubout;
extern PyObject *const_tuple_str_digest_4dcf85ba674584b7335275c899c0ab24_tuple;
extern PyObject *const_tuple_str_plain_Document_tuple;
extern PyObject *const_str_plain_end_macro;
extern PyObject *const_str_digest_bc52841ed7d0c0e991090611ebb55c62;
static PyObject *const_str_digest_6436ae48f56ffce91fa387300abe37dd;
extern PyObject *const_str_plain_WORD;
static PyObject *const_str_plain_end_of_file;
extern PyObject *const_str_plain_pre_run_callables;
static PyObject *const_str_digest_e00c6f9ddf82e0ad26674bf1f89e79b3;
static PyObject *const_str_digest_94831ee4810e73b56026b83acc214eaf;
extern PyObject *const_tuple_str_plain_range_tuple;
extern PyObject *const_str_plain_line;
static PyObject *const_d0478556cd6657c16dd6c8c5a9fff9b0;
static PyObject *const_str_digest_705488b055618885c52de69cddaccd3f;
extern PyObject *const_tuple_str_digest_637ae2db1c15757d3f68020910fc7af5_tuple;
extern PyObject *const_tuple_str_digest_7e022856dd1b5bd6663571c7df813303_tuple;
extern PyObject *const_str_plain_history_forward;
static PyObject *const_tuple_str_plain_PasteMode_tuple;
extern PyObject *const_str_plain_title;
static PyObject *const_str_plain_end_of_history;
extern PyObject *const_tuple_str_plain_SearchDirection_tuple;
extern PyObject *const_dict_3c1e954997caa37c7b6a50568aed10db;
static PyObject *const_str_digest_d55dd59af31cb709bc87237ed76a42a2;
extern PyObject *const_str_plain_document;
extern PyObject *const_str_plain_PasteMode;
static PyObject *const_str_plain_accept_line;
extern PyObject *const_str_digest_4e2aeb083e5702a53bc119c9bb3ab9d7;
static PyObject *const_dict_c6fe41077d8176687979ef8499963adc;
extern PyObject *const_tuple_str_digest_4d94ac19d21ce83f39e74e91d067ed52_tuple;
static PyObject *const_str_digest_0759fa4e9adc187ca65f264c0278e040;
static PyObject *const_str_plain_self_insert;
extern PyObject *const_tuple_str_digest_f6826d177b892c516eb853c8574210e2_tuple;
static PyObject *const_str_digest_45658ead8dc363a1802864ace51fce07;
static PyObject *const_str_plain__readline_commands;
extern PyObject *const_str_plain_output;
static PyObject *const_str_digest_400d834c6f1d5d5d7b9185420395cb8b;
static PyObject *const_tuple_str_digest_1bad627ed19ed28ad2101a569d58d565_tuple;
static PyObject *const_tuple_str_digest_bff9c642660585f4a91209646bf69a4a_tuple;
extern PyObject *const_tuple_str_digest_1e115ca000973c4222f04cf175132c7e_tuple;
extern PyObject *const_str_plain_working_index;
extern PyObject *const_str_digest_c8c3694ea6fe9becb903618b30b45e7b;
extern PyObject *const_str_plain_count;
static PyObject *const_str_digest_550d97e354a8bb55dff671e969fae43d;
extern PyObject *const_str_digest_4e5a5ed7c51e9863d1e36202b87fe43e;
static PyObject *const_str_plain_delete_char;
extern PyObject *const_str_plain_overwrite;
extern PyObject *const_str_plain_key_processor;
extern PyObject *const_str_digest_10c978016ae019e97a4da8b4078edf46;
static PyObject *const_str_digest_6205b7c65ea809e47b9f08db79bf8bf3;
extern PyObject *const_str_plain_text_after_cursor;
static PyObject *const_str_plain_backward_char;
extern PyObject *const_str_digest_fb5c11df9835340c3dd4a642c111b2eb;
extern PyObject *const_str_plain_BACKWARD;
extern PyObject *const_str_plain_deleted;
static PyObject *const_str_plain_backward_word;
extern PyObject *const_tuple_str_digest_ae692cb46ab2cf52c0be9c11d31b5318_tuple;
extern PyObject *const_tuple_str_digest_c1dec28d38befd226a053f4c881a493b_tuple;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_str_plain_EMACS;
static PyObject *const_tuple_1d906eddd9a25222b8901b143039d608_tuple;
extern PyObject *const_tuple_str_digest_70508f93fca682b73b6276fb31ac0cad_tuple;
extern PyObject *const_str_plain_find_next_word_ending;
static PyObject *const_str_digest_f49d647b7859fb1b29a225aba45b7cc4;
static PyObject *const_str_digest_e7a7c19e756842d84e9bf8616e02117b;
extern PyObject *const_str_plain_display_completions_like_readline;
extern PyObject *const_str_newline;
static PyObject *const_dict_23a38ac95c6ae48450b8623c5e2b573e;
extern PyObject *const_str_plain_undo;
extern PyObject *const_str_plain_clear;
extern PyObject *const_str_plain_rstrip;
extern PyObject *const_tuple_str_plain_name_tuple;
extern PyObject *const_tuple_str_digest_e13b285e26a38d1a90b94ccc0668450c_tuple;
extern PyObject *const_str_digest_d8d4f5e440c7f01129293c18f6555af5;
static PyObject *const_tuple_ab4ed65530a0c83fa1ab050a3702b859_tuple;
extern PyObject *const_tuple_str_digest_8ac25a148822dcf205845c372c142dc5_tuple;
extern PyObject *const_dict_0bf8402271879b64e927f26f79aacd8b;
extern PyObject *const_str_digest_1e115ca000973c4222f04cf175132c7e;
extern PyObject *const_str_plain_handler;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_str_digest_072123a03afbbc4a30fb698f3e63aac0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4682976 ], 71, 0 );
    const_str_plain_next_history = UNSTREAM_STRING_ASCII( &constant_bin[ 4683047 ], 12, 1 );
    const_str_plain_redraw_current_line = UNSTREAM_STRING_ASCII( &constant_bin[ 4683059 ], 19, 1 );
    const_str_digest_b957e174ba0fdf03669f960ad9df6dd4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4683078 ], 139, 0 );
    const_str_digest_48235e17fb80f346bb9d79f90caba172 = UNSTREAM_STRING_ASCII( &constant_bin[ 4683217 ], 41, 0 );
    const_str_digest_d170e87999e8451393080db2ed4cfc68 = UNSTREAM_STRING_ASCII( &constant_bin[ 4683258 ], 236, 0 );
    const_tuple_str_plain_event_str_plain_b_str_plain_p_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_b_str_plain_p_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_b_str_plain_p_tuple, 1, const_str_plain_b ); Py_INCREF( const_str_plain_b );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_b_str_plain_p_tuple, 2, const_str_plain_p ); Py_INCREF( const_str_plain_p );
    const_str_digest_31c4b630f4ffd96ff20e45c83bc6d08e = UNSTREAM_STRING_ASCII( &constant_bin[ 4683494 ], 30, 0 );
    const_tuple_str_plain_k_str_plain_event_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_k_str_plain_event_tuple, 0, const_str_plain_k ); Py_INCREF( const_str_plain_k );
    PyTuple_SET_ITEM( const_tuple_str_plain_k_str_plain_event_tuple, 1, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    const_str_plain_reverse_search_history = UNSTREAM_STRING_ASCII( &constant_bin[ 4683524 ], 22, 1 );
    const_str_digest_5b1ffed3e4be8aa5f4cb3887c232c8e1 = UNSTREAM_STRING_ASCII( &constant_bin[ 4683546 ], 20, 0 );
    const_str_digest_8050ff83554e3b5eef4c5274df33464c = UNSTREAM_STRING_ASCII( &constant_bin[ 4683566 ], 24, 0 );
    const_str_digest_6435b37fd837c6013a16282357bd256d = UNSTREAM_STRING_ASCII( &constant_bin[ 4683590 ], 76, 0 );
    const_str_digest_968f9b527a999c6f0eb6156bb8866021 = UNSTREAM_STRING_ASCII( &constant_bin[ 4683666 ], 52, 0 );
    const_str_digest_85dadd9de2ac2ff25d2fd136dc90bc72 = UNSTREAM_STRING_ASCII( &constant_bin[ 4683718 ], 32, 0 );
    const_str_plain_downcase_word = UNSTREAM_STRING_ASCII( &constant_bin[ 4683750 ], 13, 1 );
    const_str_plain_previous_history = UNSTREAM_STRING_ASCII( &constant_bin[ 4683763 ], 16, 1 );
    const_str_plain_forward_char = UNSTREAM_STRING_ASCII( &constant_bin[ 4683779 ], 12, 1 );
    const_str_digest_34910ada2a9571c81d0198bdebe83810 = UNSTREAM_STRING_ASCII( &constant_bin[ 4683791 ], 41, 0 );
    const_str_digest_ce90e5fd750ebe5fbf3c059d5d422d22 = UNSTREAM_STRING_ASCII( &constant_bin[ 4683832 ], 30, 0 );
    const_str_plain_edit_and_execute = UNSTREAM_STRING_ASCII( &constant_bin[ 4683862 ], 16, 1 );
    const_str_plain_end_kbd_macro = UNSTREAM_STRING_ASCII( &constant_bin[ 4683878 ], 13, 1 );
    const_str_digest_d2154be122eab39bb0280421ac483f4e = UNSTREAM_STRING_ASCII( &constant_bin[ 4683891 ], 27, 0 );
    const_str_digest_9d4e7794e591dce4cbbd54460a027e3f = UNSTREAM_STRING_ASCII( &constant_bin[ 4683918 ], 108, 0 );
    const_str_plain_delete_before = UNSTREAM_STRING_ASCII( &constant_bin[ 4597781 ], 13, 1 );
    const_str_plain_beginning_of_line = UNSTREAM_STRING_ASCII( &constant_bin[ 4684026 ], 17, 1 );
    const_str_plain_call_last_kbd_macro = UNSTREAM_STRING_ASCII( &constant_bin[ 4684043 ], 19, 1 );
    const_str_digest_3f4d599e74335620cbcb6d738c0fe3fc = UNSTREAM_STRING_ASCII( &constant_bin[ 4684062 ], 76, 0 );
    const_str_plain_menu_complete = UNSTREAM_STRING_ASCII( &constant_bin[ 4684138 ], 13, 1 );
    const_tuple_str_plain_name_str_plain_decorator_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_decorator_tuple, 0, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    PyTuple_SET_ITEM( const_tuple_str_plain_name_str_plain_decorator_tuple, 1, const_str_plain_decorator ); Py_INCREF( const_str_plain_decorator );
    const_str_plain_delete_after = UNSTREAM_STRING_ASCII( &constant_bin[ 4684151 ], 12, 1 );
    const_str_digest_74f41a8411a3e103f4e1c3a33b0e5662 = UNSTREAM_STRING_ASCII( &constant_bin[ 4684163 ], 63, 0 );
    const_tuple_str_digest_5b1ffed3e4be8aa5f4cb3887c232c8e1_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_5b1ffed3e4be8aa5f4cb3887c232c8e1_tuple, 0, const_str_digest_5b1ffed3e4be8aa5f4cb3887c232c8e1 ); Py_INCREF( const_str_digest_5b1ffed3e4be8aa5f4cb3887c232c8e1 );
    const_tuple_2585165d60931c7b7dc1b2c728b4f332_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_2585165d60931c7b7dc1b2c728b4f332_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    const_str_plain_print_macro = UNSTREAM_STRING_ASCII( &constant_bin[ 4683821 ], 11, 1 );
    PyTuple_SET_ITEM( const_tuple_2585165d60931c7b7dc1b2c728b4f332_tuple, 1, const_str_plain_print_macro ); Py_INCREF( const_str_plain_print_macro );
    PyTuple_SET_ITEM( const_tuple_2585165d60931c7b7dc1b2c728b4f332_tuple, 2, const_str_plain_run_in_terminal ); Py_INCREF( const_str_plain_run_in_terminal );
    const_str_digest_66d9c352af7ed5132c71e2f398933b45 = UNSTREAM_STRING_ASCII( &constant_bin[ 4684226 ], 42, 0 );
    const_str_plain_beginning_of_history = UNSTREAM_STRING_ASCII( &constant_bin[ 4684268 ], 20, 1 );
    const_str_digest_274b6eb2a65f09ccc339d29eea36ebd9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4684288 ], 50, 0 );
    const_str_plain_delete_horizontal_space = UNSTREAM_STRING_ASCII( &constant_bin[ 4684338 ], 23, 1 );
    const_tuple_str_digest_e00c6f9ddf82e0ad26674bf1f89e79b3_tuple = PyTuple_New( 1 );
    const_str_digest_e00c6f9ddf82e0ad26674bf1f89e79b3 = UNSTREAM_STRING_ASCII( &constant_bin[ 4684361 ], 15, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_e00c6f9ddf82e0ad26674bf1f89e79b3_tuple, 0, const_str_digest_e00c6f9ddf82e0ad26674bf1f89e79b3 ); Py_INCREF( const_str_digest_e00c6f9ddf82e0ad26674bf1f89e79b3 );
    const_str_digest_8ab7b8386ed49775e91498682cd2986c = UNSTREAM_STRING_ASCII( &constant_bin[ 4684376 ], 53, 0 );
    const_str_digest_78bb55f89abb5b3ad76eb473479dd6a3 = UNSTREAM_STRING_ASCII( &constant_bin[ 4684429 ], 40, 0 );
    const_str_plain_vi_editing_mode = UNSTREAM_STRING_ASCII( &constant_bin[ 4684469 ], 15, 1 );
    const_str_plain_backward_kill_word = UNSTREAM_STRING_ASCII( &constant_bin[ 4684484 ], 18, 1 );
    const_str_digest_0917fbd04b33410fa0c2d713b22ffff2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4684502 ], 103, 0 );
    const_tuple_855c21556e24f864827dd0797c057760_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_855c21556e24f864827dd0797c057760_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_855c21556e24f864827dd0797c057760_tuple, 1, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_855c21556e24f864827dd0797c057760_tuple, 2, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_855c21556e24f864827dd0797c057760_tuple, 3, const_str_plain_deleted ); Py_INCREF( const_str_plain_deleted );
    const_str_digest_1bad627ed19ed28ad2101a569d58d565 = UNSTREAM_STRING_ASCII( &constant_bin[ 4684605 ], 18, 0 );
    const_str_digest_02a5690946f1cc1ce0757fc48394bbff = UNSTREAM_STRING_ASCII( &constant_bin[ 4684623 ], 123, 0 );
    const_str_plain_set_working_index = UNSTREAM_STRING_ASCII( &constant_bin[ 4684746 ], 17, 1 );
    const_str_digest_bac376679fee18ab8e8f621bfb04b25e = UNSTREAM_STRING_ASCII( &constant_bin[ 4684763 ], 105, 0 );
    const_str_digest_17a581addb5f14b5622a3275e5609824 = UNSTREAM_STRING_ASCII( &constant_bin[ 4684868 ], 78, 0 );
    const_str_plain_start_kbd_macro = UNSTREAM_STRING_ASCII( &constant_bin[ 4684946 ], 15, 1 );
    const_str_digest_bff9c642660585f4a91209646bf69a4a = UNSTREAM_STRING_ASCII( &constant_bin[ 4684961 ], 11, 0 );
    const_str_digest_a519a736eca93ebf6d46cc412c173add = UNSTREAM_STRING_ASCII( &constant_bin[ 4684972 ], 157, 0 );
    const_str_digest_b6c306f5553e75c8ab5c659e7b1bb0c0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4685129 ], 27, 0 );
    const_str_digest_7f3dc17e1f79ad0755aafac73dd0d273 = UNSTREAM_STRING_ASCII( &constant_bin[ 4685156 ], 240, 0 );
    const_tuple_str_plain_event_str_plain_n_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_n_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_n_tuple, 1, const_str_plain_n ); Py_INCREF( const_str_plain_n );
    const_str_digest_eac4292b628774117c5284605831d6d0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4685396 ], 57, 0 );
    const_str_digest_67055978b31b0cd8ee70ae4eac029a8f = UNSTREAM_STRING_ASCII( &constant_bin[ 4685453 ], 47, 0 );
    const_str_plain_print_last_kbd_macro = UNSTREAM_STRING_ASCII( &constant_bin[ 4683791 ], 20, 1 );
    const_str_digest_4650e344edbd2542c4421b2b140d04da = UNSTREAM_STRING_ASCII( &constant_bin[ 4685500 ], 22, 0 );
    const_str_plain_uppercase_word = UNSTREAM_STRING_ASCII( &constant_bin[ 4685522 ], 14, 1 );
    const_tuple_str_digest_400d834c6f1d5d5d7b9185420395cb8b_tuple = PyTuple_New( 1 );
    const_str_digest_400d834c6f1d5d5d7b9185420395cb8b = UNSTREAM_STRING_ASCII( &constant_bin[ 4685536 ], 19, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_400d834c6f1d5d5d7b9185420395cb8b_tuple, 0, const_str_digest_400d834c6f1d5d5d7b9185420395cb8b ); Py_INCREF( const_str_digest_400d834c6f1d5d5d7b9185420395cb8b );
    const_str_digest_793ec132e5b39c0badda11bae0171c7a = UNSTREAM_STRING_ASCII( &constant_bin[ 4685555 ], 111, 0 );
    const_str_plain_capitalize_word = UNSTREAM_STRING_ASCII( &constant_bin[ 4685666 ], 15, 1 );
    const_list_str_plain_get_by_name_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_get_by_name_list, 0, const_str_plain_get_by_name ); Py_INCREF( const_str_plain_get_by_name );
    const_tuple_str_plain_event_str_plain_deleted_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_deleted_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_deleted_tuple, 1, const_str_plain_deleted ); Py_INCREF( const_str_plain_deleted );
    const_str_digest_b7ccea59c184956e0e6390de033e4e13 = UNSTREAM_STRING_ASCII( &constant_bin[ 4685681 ], 130, 0 );
    const_tuple_str_plain_handler_str_plain_name_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_handler_str_plain_name_tuple, 0, const_str_plain_handler ); Py_INCREF( const_str_plain_handler );
    PyTuple_SET_ITEM( const_tuple_str_plain_handler_str_plain_name_tuple, 1, const_str_plain_name ); Py_INCREF( const_str_plain_name );
    const_tuple_str_digest_94831ee4810e73b56026b83acc214eaf_tuple = PyTuple_New( 1 );
    const_str_digest_94831ee4810e73b56026b83acc214eaf = UNSTREAM_STRING_ASCII( &constant_bin[ 4685811 ], 11, 0 );
    PyTuple_SET_ITEM( const_tuple_str_digest_94831ee4810e73b56026b83acc214eaf_tuple, 0, const_str_digest_94831ee4810e73b56026b83acc214eaf ); Py_INCREF( const_str_digest_94831ee4810e73b56026b83acc214eaf );
    const_tuple_e4126eaf674adc64e483dadb1d72998f_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_e4126eaf674adc64e483dadb1d72998f_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_e4126eaf674adc64e483dadb1d72998f_tuple, 1, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_e4126eaf674adc64e483dadb1d72998f_tuple, 2, const_str_plain_i ); Py_INCREF( const_str_plain_i );
    PyTuple_SET_ITEM( const_tuple_e4126eaf674adc64e483dadb1d72998f_tuple, 3, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_e4126eaf674adc64e483dadb1d72998f_tuple, 4, const_str_plain_words ); Py_INCREF( const_str_plain_words );
    const_str_plain_doc_before_paste = UNSTREAM_STRING_ASCII( &constant_bin[ 4685822 ], 16, 1 );
    const_tuple_fd2995ae22a9b8ea312ca0255b92783a_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_fd2995ae22a9b8ea312ca0255b92783a_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_fd2995ae22a9b8ea312ca0255b92783a_tuple, 1, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_fd2995ae22a9b8ea312ca0255b92783a_tuple, 2, const_str_plain_text_before_cursor ); Py_INCREF( const_str_plain_text_before_cursor );
    PyTuple_SET_ITEM( const_tuple_fd2995ae22a9b8ea312ca0255b92783a_tuple, 3, const_str_plain_text_after_cursor ); Py_INCREF( const_str_plain_text_after_cursor );
    PyTuple_SET_ITEM( const_tuple_fd2995ae22a9b8ea312ca0255b92783a_tuple, 4, const_str_plain_delete_before ); Py_INCREF( const_str_plain_delete_before );
    PyTuple_SET_ITEM( const_tuple_fd2995ae22a9b8ea312ca0255b92783a_tuple, 5, const_str_plain_delete_after ); Py_INCREF( const_str_plain_delete_after );
    const_str_plain_yank_pop = UNSTREAM_STRING_ASCII( &constant_bin[ 4685838 ], 8, 1 );
    const_tuple_str_plain_event_str_plain_buff_str_plain_change_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_buff_str_plain_change_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_buff_str_plain_change_tuple, 1, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_buff_str_plain_change_tuple, 2, const_str_plain_change ); Py_INCREF( const_str_plain_change );
    const_str_plain_backward_delete_char = UNSTREAM_STRING_ASCII( &constant_bin[ 4685846 ], 20, 1 );
    const_tuple_2502f7b4cbd494f46f966272e92176d2_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_2502f7b4cbd494f46f966272e92176d2_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_2502f7b4cbd494f46f966272e92176d2_tuple, 1, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_2502f7b4cbd494f46f966272e92176d2_tuple, 2, const_str_plain_new_index ); Py_INCREF( const_str_plain_new_index );
    PyTuple_SET_ITEM( const_tuple_2502f7b4cbd494f46f966272e92176d2_tuple, 3, const_str_plain_set_working_index ); Py_INCREF( const_str_plain_set_working_index );
    const_str_digest_bd43b33ede5b6fb74b5a7494f79f0067 = UNSTREAM_STRING_ASCII( &constant_bin[ 4685866 ], 32, 0 );
    const_str_plain_forward_word = UNSTREAM_STRING_ASCII( &constant_bin[ 4685898 ], 12, 1 );
    const_tuple_str_plain_event_str_plain_buff_str_plain_pos_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_buff_str_plain_pos_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_buff_str_plain_pos_tuple, 1, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_buff_str_plain_pos_tuple, 2, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    const_tuple_04b5f32bde1726e2e6564bface78b67f_tuple = PyTuple_New( 4 );
    PyTuple_SET_ITEM( const_tuple_04b5f32bde1726e2e6564bface78b67f_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_04b5f32bde1726e2e6564bface78b67f_tuple, 1, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_04b5f32bde1726e2e6564bface78b67f_tuple, 2, const_str_plain_doc_before_paste ); Py_INCREF( const_str_plain_doc_before_paste );
    PyTuple_SET_ITEM( const_tuple_04b5f32bde1726e2e6564bface78b67f_tuple, 3, const_str_plain_clipboard ); Py_INCREF( const_str_plain_clipboard );
    const_str_digest_58304b5642f2a8741eb67b7c2ad22ce5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4685910 ], 28, 0 );
    const_str_digest_60701c7754d852c86a2f19de3267c311 = UNSTREAM_STRING_ASCII( &constant_bin[ 4685938 ], 132, 0 );
    const_str_digest_3ef4f4689543f14060661203b3f01cf9 = UNSTREAM_STRING_ASCII( &constant_bin[ 4686070 ], 59, 0 );
    const_str_plain_prefix_meta = UNSTREAM_STRING_ASCII( &constant_bin[ 4685379 ], 11, 1 );
    const_str_digest_e6f30477bcc260ba4ea547b3b2911818 = UNSTREAM_STRING_ASCII( &constant_bin[ 4686129 ], 53, 0 );
    const_str_digest_6770944cf34737f0fd1c0ae57ea84e00 = UNSTREAM_STRING_ASCII( &constant_bin[ 4686182 ], 136, 0 );
    const_tuple_str_plain_event_str_plain_buff_str_plain_deleted_tuple = PyTuple_New( 3 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_buff_str_plain_deleted_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_buff_str_plain_deleted_tuple, 1, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_buff_str_plain_deleted_tuple, 2, const_str_plain_deleted ); Py_INCREF( const_str_plain_deleted );
    const_str_digest_2a8fcad8b385b0c88d0194c208d97103 = UNSTREAM_STRING_ASCII( &constant_bin[ 4686318 ], 69, 0 );
    const_str_digest_9cc1ee391748caad62e066f6aaa843a5 = UNSTREAM_STRING_ASCII( &constant_bin[ 4686387 ], 15, 0 );
    const_str_plain_kill_line = UNSTREAM_STRING_ASCII( &constant_bin[ 4686402 ], 9, 1 );
    const_str_digest_7d1c0c5af5d1875a6a400642e2bc6d31 = UNSTREAM_STRING_ASCII( &constant_bin[ 4686411 ], 18, 0 );
    const_str_digest_ade7a4439193c90488d0fdc4fa908f0e = UNSTREAM_STRING_ASCII( &constant_bin[ 4686429 ], 37, 0 );
    const_str_plain_emacs_editing_mode = UNSTREAM_STRING_ASCII( &constant_bin[ 4686466 ], 18, 1 );
    const_str_digest_ff960a63a7cd023625d10d8afb7b75af = UNSTREAM_STRING_ASCII( &constant_bin[ 4686484 ], 52, 0 );
    const_str_digest_f3142aabc8984e9b5ee4284ee94fc9e1 = UNSTREAM_STRING_ASCII( &constant_bin[ 4686536 ], 28, 0 );
    const_dict_47be3150a6234742830e43bdf5d5778f = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_47be3150a6234742830e43bdf5d5778f, const_str_plain_validate_and_handle, Py_True );
    assert( PyDict_Size( const_dict_47be3150a6234742830e43bdf5d5778f ) == 1 );
    const_tuple_str_digest_4650e344edbd2542c4421b2b140d04da_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_4650e344edbd2542c4421b2b140d04da_tuple, 0, const_str_digest_4650e344edbd2542c4421b2b140d04da ); Py_INCREF( const_str_digest_4650e344edbd2542c4421b2b140d04da );
    const_dict_2de9ada199d63ba5bdc7c9b31335d708 = _PyDict_NewPresized( 1 );
    const_d0478556cd6657c16dd6c8c5a9fff9b0 = PyMarshal_ReadObjectFromString( (char *)&constant_bin[ 4686564 ], 51 );
    PyDict_SetItem( const_dict_2de9ada199d63ba5bdc7c9b31335d708, const_str_plain_count, const_d0478556cd6657c16dd6c8c5a9fff9b0 );
    assert( PyDict_Size( const_dict_2de9ada199d63ba5bdc7c9b31335d708 ) == 1 );
    const_str_plain_insert_comment = UNSTREAM_STRING_ASCII( &constant_bin[ 4683832 ], 14, 1 );
    const_str_plain_operate_and_get_next = UNSTREAM_STRING_ASCII( &constant_bin[ 4685453 ], 20, 1 );
    const_str_plain_transpose_chars = UNSTREAM_STRING_ASCII( &constant_bin[ 4686615 ], 15, 1 );
    const_tuple_str_plain_event_str_plain_control_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_control_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_str_plain_event_str_plain_control_tuple, 1, const_str_plain_control ); Py_INCREF( const_str_plain_control );
    const_tuple_str_plain_complete_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_complete_tuple, 0, const_str_plain_complete ); Py_INCREF( const_str_plain_complete );
    const_str_digest_2ea096c33d6acdbf8f658453d87483a6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4686630 ], 87, 0 );
    const_str_plain_unix_line_discard = UNSTREAM_STRING_ASCII( &constant_bin[ 4686717 ], 17, 1 );
    const_str_digest_9836aff840b931e5ea309144651873e0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4686734 ], 30, 0 );
    const_str_digest_86fc33ac35792a7b74893a235817abff = UNSTREAM_STRING_ASCII( &constant_bin[ 4686764 ], 259, 0 );
    const_str_digest_feff521061f9718c2c4575ee30698cef = UNSTREAM_STRING_ASCII( &constant_bin[ 4687023 ], 96, 0 );
    const_str_digest_1beb6b9754702adc084aa39d18c50ba8 = UNSTREAM_STRING_ASCII( &constant_bin[ 4687119 ], 151, 0 );
    const_tuple_str_plain_new_index_str_plain_buff_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_str_plain_new_index_str_plain_buff_tuple, 0, const_str_plain_new_index ); Py_INCREF( const_str_plain_new_index );
    PyTuple_SET_ITEM( const_tuple_str_plain_new_index_str_plain_buff_tuple, 1, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    const_str_digest_3dbaa58e29edd8b0fa66cb30e90e21b6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4687270 ], 31, 0 );
    const_str_plain_end_of_line = UNSTREAM_STRING_ASCII( &constant_bin[ 4625404 ], 11, 1 );
    const_str_plain_menu_complete_backward = UNSTREAM_STRING_ASCII( &constant_bin[ 4687301 ], 22, 1 );
    const_str_plain_kill_word = UNSTREAM_STRING_ASCII( &constant_bin[ 4684493 ], 9, 1 );
    const_str_digest_a382cc8c0cded4a250815b426d58ad03 = UNSTREAM_STRING_ASCII( &constant_bin[ 4687323 ], 38, 0 );
    const_str_digest_56ddd1cd56382cd88f10c4ba7452feda = UNSTREAM_STRING_ASCII( &constant_bin[ 4687361 ], 97, 0 );
    const_str_plain_unix_word_rubout = UNSTREAM_STRING_ASCII( &constant_bin[ 4687458 ], 16, 1 );
    const_str_digest_6436ae48f56ffce91fa387300abe37dd = UNSTREAM_STRING_ASCII( &constant_bin[ 4687474 ], 452, 0 );
    const_str_plain_end_of_file = UNSTREAM_STRING_ASCII( &constant_bin[ 4687926 ], 11, 1 );
    const_str_digest_705488b055618885c52de69cddaccd3f = UNSTREAM_STRING_ASCII( &constant_bin[ 4687937 ], 102, 0 );
    const_tuple_str_plain_PasteMode_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_PasteMode_tuple, 0, const_str_plain_PasteMode ); Py_INCREF( const_str_plain_PasteMode );
    const_str_plain_end_of_history = UNSTREAM_STRING_ASCII( &constant_bin[ 4688039 ], 14, 1 );
    const_str_digest_d55dd59af31cb709bc87237ed76a42a2 = UNSTREAM_STRING_ASCII( &constant_bin[ 4688053 ], 134, 0 );
    const_str_plain_accept_line = UNSTREAM_STRING_ASCII( &constant_bin[ 4688187 ], 11, 1 );
    const_dict_c6fe41077d8176687979ef8499963adc = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_c6fe41077d8176687979ef8499963adc, const_str_plain_WORD, Py_False );
    assert( PyDict_Size( const_dict_c6fe41077d8176687979ef8499963adc ) == 1 );
    const_str_digest_0759fa4e9adc187ca65f264c0278e040 = UNSTREAM_STRING_ASCII( &constant_bin[ 4688198 ], 77, 0 );
    const_str_plain_self_insert = UNSTREAM_STRING_ASCII( &constant_bin[ 4688275 ], 11, 1 );
    const_str_digest_45658ead8dc363a1802864ace51fce07 = UNSTREAM_STRING_ASCII( &constant_bin[ 4688286 ], 52, 0 );
    const_str_plain__readline_commands = UNSTREAM_STRING_ASCII( &constant_bin[ 4684190 ], 18, 1 );
    const_tuple_str_digest_1bad627ed19ed28ad2101a569d58d565_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_1bad627ed19ed28ad2101a569d58d565_tuple, 0, const_str_digest_1bad627ed19ed28ad2101a569d58d565 ); Py_INCREF( const_str_digest_1bad627ed19ed28ad2101a569d58d565 );
    const_tuple_str_digest_bff9c642660585f4a91209646bf69a4a_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_bff9c642660585f4a91209646bf69a4a_tuple, 0, const_str_digest_bff9c642660585f4a91209646bf69a4a ); Py_INCREF( const_str_digest_bff9c642660585f4a91209646bf69a4a );
    const_str_digest_550d97e354a8bb55dff671e969fae43d = UNSTREAM_STRING_ASCII( &constant_bin[ 4688338 ], 40, 0 );
    const_str_plain_delete_char = UNSTREAM_STRING_ASCII( &constant_bin[ 4685855 ], 11, 1 );
    const_str_digest_6205b7c65ea809e47b9f08db79bf8bf3 = UNSTREAM_STRING_ASCII( &constant_bin[ 4688378 ], 134, 0 );
    const_str_plain_backward_char = UNSTREAM_STRING_ASCII( &constant_bin[ 4688512 ], 13, 1 );
    const_str_plain_backward_word = UNSTREAM_STRING_ASCII( &constant_bin[ 4688525 ], 13, 1 );
    const_tuple_1d906eddd9a25222b8901b143039d608_tuple = PyTuple_New( 2 );
    PyTuple_SET_ITEM( const_tuple_1d906eddd9a25222b8901b143039d608_tuple, 0, const_str_plain_generate_completions ); Py_INCREF( const_str_plain_generate_completions );
    PyTuple_SET_ITEM( const_tuple_1d906eddd9a25222b8901b143039d608_tuple, 1, const_str_plain_display_completions_like_readline ); Py_INCREF( const_str_plain_display_completions_like_readline );
    const_str_digest_f49d647b7859fb1b29a225aba45b7cc4 = UNSTREAM_STRING_ASCII( &constant_bin[ 4688538 ], 19, 0 );
    const_str_digest_e7a7c19e756842d84e9bf8616e02117b = UNSTREAM_STRING_ASCII( &constant_bin[ 4688557 ], 74, 0 );
    const_dict_23a38ac95c6ae48450b8623c5e2b573e = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_23a38ac95c6ae48450b8623c5e2b573e, const_str_plain_record_in_macro, Py_False );
    assert( PyDict_Size( const_dict_23a38ac95c6ae48450b8623c5e2b573e ) == 1 );
    const_tuple_ab4ed65530a0c83fa1ab050a3702b859_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_ab4ed65530a0c83fa1ab050a3702b859_tuple, 0, const_str_plain_event ); Py_INCREF( const_str_plain_event );
    PyTuple_SET_ITEM( const_tuple_ab4ed65530a0c83fa1ab050a3702b859_tuple, 1, const_str_plain_WORD ); Py_INCREF( const_str_plain_WORD );
    PyTuple_SET_ITEM( const_tuple_ab4ed65530a0c83fa1ab050a3702b859_tuple, 2, const_str_plain_buff ); Py_INCREF( const_str_plain_buff );
    PyTuple_SET_ITEM( const_tuple_ab4ed65530a0c83fa1ab050a3702b859_tuple, 3, const_str_plain_pos ); Py_INCREF( const_str_plain_pos );
    PyTuple_SET_ITEM( const_tuple_ab4ed65530a0c83fa1ab050a3702b859_tuple, 4, const_str_plain_deleted ); Py_INCREF( const_str_plain_deleted );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$key_binding$bindings$named_commands( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_8c5fb6e65dc9f8f648efd26b8fac0a57;
static PyCodeObject *codeobj_c6053e4667cff011640e3abbd139e7da;
static PyCodeObject *codeobj_24a983b79c05a66d729037082626d405;
static PyCodeObject *codeobj_0518d9e28a85d130dc7dd7830cf50ece;
static PyCodeObject *codeobj_5cef15a627204a6a34d9977b1d03f826;
static PyCodeObject *codeobj_767bcb9ae21587b5979dce639ee2d6a3;
static PyCodeObject *codeobj_12cd250f02ebc71ccdb2ff48752774c5;
static PyCodeObject *codeobj_749f2ff2a66d38422ecbc1eff69df74d;
static PyCodeObject *codeobj_efa8e942d162049ae5d2899df9e1088c;
static PyCodeObject *codeobj_a4151aa1583707a1af87bc7dca844f41;
static PyCodeObject *codeobj_823324200e60cf6166d8f7ff6d294e48;
static PyCodeObject *codeobj_dcde661036877db73bd081d3ad20601e;
static PyCodeObject *codeobj_8b24d74de375253360b7bb31d8618a2b;
static PyCodeObject *codeobj_d1ae5db46ddec2f0d84cc2f7753005f4;
static PyCodeObject *codeobj_588b68462b0b864f34de8314f9ada309;
static PyCodeObject *codeobj_34ff4fc0dd5f52aacc6ec26d8e93f2df;
static PyCodeObject *codeobj_63a9d9d241c96d8c5750972866445eb4;
static PyCodeObject *codeobj_690bd84dbafdd852898db42272c38b83;
static PyCodeObject *codeobj_060e0f20cbcf031ce2850e90f9802fc1;
static PyCodeObject *codeobj_0a5b5be3846430f22fb03f1d970c448c;
static PyCodeObject *codeobj_5bdc03f3d4c97002f3d3b93d45e2810a;
static PyCodeObject *codeobj_50753afd5df05e0514e22929ed73d40a;
static PyCodeObject *codeobj_48e352436fa387cc54d3a74128217bbf;
static PyCodeObject *codeobj_c3b4b5c70d8cd76855f0bfd713e153ba;
static PyCodeObject *codeobj_3ce7bc74879e0ac013b3102a55e00fba;
static PyCodeObject *codeobj_1d9ea5d5080207ab5bda5f057edfb276;
static PyCodeObject *codeobj_4411806d0c478c17e0ae7a40d162bc9d;
static PyCodeObject *codeobj_9ac0ea985d42c67f708b6ac774ef0d6d;
static PyCodeObject *codeobj_29f3c5ee949bbc61155cc369d86c4fff;
static PyCodeObject *codeobj_8c14b78b16a71186c05c11186cae7ea7;
static PyCodeObject *codeobj_9b1fe3eab7b189a9f30b3fed0cdb3b00;
static PyCodeObject *codeobj_c5f30d095ec1aff287f6e262473fd2c1;
static PyCodeObject *codeobj_338ce756c26e6afd0cb3658efccaadb6;
static PyCodeObject *codeobj_aa3d720d464998daed17c750d1ea6be2;
static PyCodeObject *codeobj_5ae4de48ba54b51ddeafe37a66e9d813;
static PyCodeObject *codeobj_90864adb7408db80ae55fc791e7071e7;
static PyCodeObject *codeobj_2e66240a37b34bb60a8a6a94c8cf2c17;
static PyCodeObject *codeobj_e0e1f04177862bc0b53992054feda9eb;
static PyCodeObject *codeobj_47c4ee38692d0bdf1d33575b039983a4;
static PyCodeObject *codeobj_8a40f6cb630df8a665be9a28d5c40a17;
static PyCodeObject *codeobj_2e176e57c9bb68b214ddce5bff4992db;
static PyCodeObject *codeobj_d5c1944153306d7fb7a9366ba732546a;
static PyCodeObject *codeobj_8468e9931c6c1f378d1338f0f777eb21;
static PyCodeObject *codeobj_e0c7a156ecdfbd87849e32041464e850;
static PyCodeObject *codeobj_6750b758ae89dba6953c9afd97461adb;
static PyCodeObject *codeobj_5a13762e2fb0e0384b5215a3360ed584;
static PyCodeObject *codeobj_a5c5fa4e6cebfbe870b60beeb035c74c;
static PyCodeObject *codeobj_acba45f633543b0c0ce4809e8825e34d;
static PyCodeObject *codeobj_c80d506d3405d850aceda812c41be29d;
static PyCodeObject *codeobj_e121294c05d8b3cba10d7b2078f3a52a;
static PyCodeObject *codeobj_826b738cdd1cbdebe17b65f5c0d4938d;
static PyCodeObject *codeobj_3419c02f5e676ebb31c78b9f702e00ea;
static PyCodeObject *codeobj_c8eead45aa3dc99f95a05d8ccc2a5ff5;
static PyCodeObject *codeobj_51f16b1961475f8370f81cc4016a2a59;
static PyCodeObject *codeobj_9a5d0dfb29ab447ff46ddd48a0d06117;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_e6f30477bcc260ba4ea547b3b2911818 );
    codeobj_8c5fb6e65dc9f8f648efd26b8fac0a57 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_3ef4f4689543f14060661203b3f01cf9, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c6053e4667cff011640e3abbd139e7da = MAKE_CODEOBJ( module_filename_obj, const_str_plain_accept_line, 132, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_24a983b79c05a66d729037082626d405 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_backward_char, 77, const_tuple_str_plain_event_str_plain_buff_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0518d9e28a85d130dc7dd7830cf50ece = MAKE_CODEOBJ( module_filename_obj, const_str_plain_backward_delete_char, 199, const_tuple_str_plain_event_str_plain_deleted_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5cef15a627204a6a34d9977b1d03f826 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_backward_kill_word, 357, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_767bcb9ae21587b5979dce639ee2d6a3 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_backward_word, 97, const_tuple_str_plain_event_str_plain_buff_str_plain_pos_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_12cd250f02ebc71ccdb2ff48752774c5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_beginning_of_history, 150, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_749f2ff2a66d38422ecbc1eff69df74d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_beginning_of_line, 56, const_tuple_str_plain_event_str_plain_buff_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_efa8e942d162049ae5d2899df9e1088c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_call_last_kbd_macro, 486, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a4151aa1583707a1af87bc7dca844f41 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_capitalize_word, 264, const_tuple_e4126eaf674adc64e483dadb1d72998f_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_823324200e60cf6166d8f7ff6d294e48 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_change, 536, const_tuple_str_plain_line_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_dcde661036877db73bd081d3ad20601e = MAKE_CODEOBJ( module_filename_obj, const_str_plain_change, 539, const_tuple_str_plain_line_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_8b24d74de375253360b7bb31d8618a2b = MAKE_CODEOBJ( module_filename_obj, const_str_plain_clear_screen, 110, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d1ae5db46ddec2f0d84cc2f7753005f4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_complete, 444, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_588b68462b0b864f34de8314f9ada309 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_decorator, 34, const_tuple_str_plain_handler_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_34ff4fc0dd5f52aacc6ec26d8e93f2df = MAKE_CODEOBJ( module_filename_obj, const_str_plain_delete_char, 191, const_tuple_str_plain_event_str_plain_deleted_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_63a9d9d241c96d8c5750972866445eb4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_delete_horizontal_space, 366, const_tuple_fd2995ae22a9b8ea312ca0255b92783a_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_690bd84dbafdd852898db42272c38b83 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_downcase_word, 251, const_tuple_e4126eaf674adc64e483dadb1d72998f_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_060e0f20cbcf031ce2850e90f9802fc1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_edit_and_execute, 597, const_tuple_str_plain_event_str_plain_buff_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0a5b5be3846430f22fb03f1d970c448c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_emacs_editing_mode, 556, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5bdc03f3d4c97002f3d3b93d45e2810a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_end_kbd_macro, 477, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_50753afd5df05e0514e22929ed73d40a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_end_of_file, 183, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_48e352436fa387cc54d3a74128217bbf = MAKE_CODEOBJ( module_filename_obj, const_str_plain_end_of_history, 156, const_tuple_str_plain_event_str_plain_buff_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c3b4b5c70d8cd76855f0bfd713e153ba = MAKE_CODEOBJ( module_filename_obj, const_str_plain_end_of_line, 63, const_tuple_str_plain_event_str_plain_buff_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3ce7bc74879e0ac013b3102a55e00fba = MAKE_CODEOBJ( module_filename_obj, const_str_plain_forward_char, 70, const_tuple_str_plain_event_str_plain_buff_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1d9ea5d5080207ab5bda5f057edfb276 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_forward_word, 84, const_tuple_str_plain_event_str_plain_buff_str_plain_pos_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_4411806d0c478c17e0ae7a40d162bc9d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_get_by_name, 41, const_tuple_str_plain_name_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9ac0ea985d42c67f708b6ac774ef0d6d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_insert_comment, 525, const_tuple_str_plain_event_str_plain_buff_str_plain_change_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_29f3c5ee949bbc61155cc369d86c4fff = MAKE_CODEOBJ( module_filename_obj, const_str_plain_kill_line, 290, const_tuple_str_plain_event_str_plain_buff_str_plain_deleted_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_8c14b78b16a71186c05c11186cae7ea7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_kill_word, 310, const_tuple_855c21556e24f864827dd0797c057760_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9b1fe3eab7b189a9f30b3fed0cdb3b00 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_menu_complete, 450, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c5f30d095ec1aff287f6e262473fd2c1 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_menu_complete_backward, 459, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_338ce756c26e6afd0cb3658efccaadb6 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_next_history, 144, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_aa3d720d464998daed17c750d1ea6be2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_operate_and_get_next, 576, const_tuple_2502f7b4cbd494f46f966272e92176d2_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5ae4de48ba54b51ddeafe37a66e9d813 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_prefix_meta, 562, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_90864adb7408db80ae55fc791e7071e7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_previous_history, 138, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_2e66240a37b34bb60a8a6a94c8cf2c17 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_print_last_kbd_macro, 503, const_tuple_2585165d60931c7b7dc1b2c728b4f332_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e0e1f04177862bc0b53992054feda9eb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_print_macro, 507, const_tuple_str_plain_k_str_plain_event_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_47c4ee38692d0bdf1d33575b039983a4 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_quoted_insert, 277, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_8a40f6cb630df8a665be9a28d5c40a17 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_redraw_current_line, 118, const_tuple_str_plain_event_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_2e176e57c9bb68b214ddce5bff4992db = MAKE_CODEOBJ( module_filename_obj, const_str_plain_register, 28, const_tuple_str_plain_name_str_plain_decorator_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_d5c1944153306d7fb7a9366ba732546a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_reverse_search_history, 166, const_tuple_str_plain_event_str_plain_control_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_8468e9931c6c1f378d1338f0f777eb21 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_self_insert, 213, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e0c7a156ecdfbd87849e32041464e850 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_set_working_index, 590, const_tuple_str_plain_new_index_str_plain_buff_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_FUTURE_UNICODE_LITERALS );
    codeobj_6750b758ae89dba6953c9afd97461adb = MAKE_CODEOBJ( module_filename_obj, const_str_plain_start_kbd_macro, 469, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_5a13762e2fb0e0384b5215a3360ed584 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_transpose_chars, 219, const_tuple_str_plain_event_str_plain_b_str_plain_p_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_a5c5fa4e6cebfbe870b60beeb035c74c = MAKE_CODEOBJ( module_filename_obj, const_str_plain_undo, 519, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_acba45f633543b0c0ce4809e8825e34d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_unix_line_discard, 380, const_tuple_str_plain_event_str_plain_buff_str_plain_deleted_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c80d506d3405d850aceda812c41be29d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_unix_word_rubout, 328, const_tuple_ab4ed65530a0c83fa1ab050a3702b859_tuple, 2, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_e121294c05d8b3cba10d7b2078f3a52a = MAKE_CODEOBJ( module_filename_obj, const_str_plain_uppercase_word, 238, const_tuple_e4126eaf674adc64e483dadb1d72998f_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_826b738cdd1cbdebe17b65f5c0d4938d = MAKE_CODEOBJ( module_filename_obj, const_str_plain_vi_editing_mode, 550, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_3419c02f5e676ebb31c78b9f702e00ea = MAKE_CODEOBJ( module_filename_obj, const_str_plain_yank, 394, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_c8eead45aa3dc99f95a05d8ccc2a5ff5 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_yank_last_arg, 413, const_tuple_str_plain_event_str_plain_n_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_51f16b1961475f8370f81cc4016a2a59 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_yank_nth_arg, 403, const_tuple_str_plain_event_str_plain_n_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_9a5d0dfb29ab447ff46ddd48a0d06117 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_yank_pop, 423, const_tuple_04b5f32bde1726e2e6564bface78b67f_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_10_redraw_current_line(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_11_accept_line(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_12_previous_history(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_13_next_history(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_14_beginning_of_history(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_15_end_of_history(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_16_reverse_search_history(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_17_end_of_file(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_18_delete_char(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_19_backward_delete_char(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register$$$function_1_decorator(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_20_self_insert(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_21_transpose_chars(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_22_uppercase_word(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_23_downcase_word(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_24_capitalize_word(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_25_quoted_insert(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_26_kill_line(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_27_kill_word(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_28_unix_word_rubout( PyObject *defaults );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_29_backward_kill_word(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_2_get_by_name(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_30_delete_horizontal_space(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_31_unix_line_discard(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_32_yank(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_33_yank_nth_arg(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_34_yank_last_arg(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_35_yank_pop(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_36_complete(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_37_menu_complete(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_38_menu_complete_backward(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_39_start_kbd_macro(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_3_beginning_of_line(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_40_end_kbd_macro(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_41_call_last_kbd_macro(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro$$$function_1_print_macro(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_43_undo(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_1_change(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_2_change(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_45_vi_editing_mode(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_46_emacs_editing_mode(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_47_prefix_meta(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next$$$function_1_set_working_index(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_49_edit_and_execute(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_4_end_of_line(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_5_forward_char(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_6_backward_char(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_7_forward_word(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_8_backward_word(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_9_clear_screen(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_name = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_decorator = NULL;
    struct Nuitka_FrameObject *frame_2e176e57c9bb68b214ddce5bff4992db;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_2e176e57c9bb68b214ddce5bff4992db = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2e176e57c9bb68b214ddce5bff4992db, codeobj_2e176e57c9bb68b214ddce5bff4992db, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_2e176e57c9bb68b214ddce5bff4992db = cache_frame_2e176e57c9bb68b214ddce5bff4992db;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2e176e57c9bb68b214ddce5bff4992db );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2e176e57c9bb68b214ddce5bff4992db ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_isinstance_inst_1;
        PyObject *tmp_isinstance_cls_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( PyCell_GET( par_name ) );
        tmp_isinstance_inst_1 = PyCell_GET( par_name );
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_six );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_six );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "six" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 32;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_isinstance_cls_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_text_type );
        if ( tmp_isinstance_cls_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_res = Nuitka_IsInstance( tmp_isinstance_inst_1, tmp_isinstance_cls_1 );
        Py_DECREF( tmp_isinstance_cls_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_operand_name_1 = ( tmp_res != 0 ) ? Py_True : Py_False;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 32;
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            tmp_raise_type_1 = PyExc_AssertionError;
            exception_type = tmp_raise_type_1;
            Py_INCREF( tmp_raise_type_1 );
            exception_lineno = 32;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "co";
            goto frame_exception_exit_1;
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e176e57c9bb68b214ddce5bff4992db );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e176e57c9bb68b214ddce5bff4992db );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2e176e57c9bb68b214ddce5bff4992db, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2e176e57c9bb68b214ddce5bff4992db->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2e176e57c9bb68b214ddce5bff4992db, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2e176e57c9bb68b214ddce5bff4992db,
        type_description_1,
        par_name,
        var_decorator
    );


    // Release cached frame.
    if ( frame_2e176e57c9bb68b214ddce5bff4992db == cache_frame_2e176e57c9bb68b214ddce5bff4992db )
    {
        Py_DECREF( frame_2e176e57c9bb68b214ddce5bff4992db );
    }
    cache_frame_2e176e57c9bb68b214ddce5bff4992db = NULL;

    assertFrameObject( frame_2e176e57c9bb68b214ddce5bff4992db );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register$$$function_1_decorator(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_name;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var_decorator == NULL );
        var_decorator = tmp_assign_source_1;
    }
    CHECK_OBJECT( var_decorator );
    tmp_return_value = var_decorator;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    CHECK_OBJECT( (PyObject *)var_decorator );
    Py_DECREF( var_decorator );
    var_decorator = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register$$$function_1_decorator( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_handler = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_588b68462b0b864f34de8314f9ada309;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_588b68462b0b864f34de8314f9ada309 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_588b68462b0b864f34de8314f9ada309, codeobj_588b68462b0b864f34de8314f9ada309, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_588b68462b0b864f34de8314f9ada309 = cache_frame_588b68462b0b864f34de8314f9ada309;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_588b68462b0b864f34de8314f9ada309 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_588b68462b0b864f34de8314f9ada309 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_ass_subvalue_1;
        PyObject *tmp_ass_subscribed_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_ass_subscript_1;
        CHECK_OBJECT( par_handler );
        tmp_ass_subvalue_1 = par_handler;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain__readline_commands );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__readline_commands );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_readline_commands" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_ass_subscribed_1 = tmp_mvar_value_1;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 36;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_ass_subscript_1 = PyCell_GET( self->m_closure[0] );
        tmp_result = SET_SUBSCRIPT( tmp_ass_subscribed_1, tmp_ass_subscript_1, tmp_ass_subvalue_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 36;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_588b68462b0b864f34de8314f9ada309 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_588b68462b0b864f34de8314f9ada309 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_588b68462b0b864f34de8314f9ada309, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_588b68462b0b864f34de8314f9ada309->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_588b68462b0b864f34de8314f9ada309, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_588b68462b0b864f34de8314f9ada309,
        type_description_1,
        par_handler,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_588b68462b0b864f34de8314f9ada309 == cache_frame_588b68462b0b864f34de8314f9ada309 )
    {
        Py_DECREF( frame_588b68462b0b864f34de8314f9ada309 );
    }
    cache_frame_588b68462b0b864f34de8314f9ada309 = NULL;

    assertFrameObject( frame_588b68462b0b864f34de8314f9ada309 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( par_handler );
    tmp_return_value = par_handler;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register$$$function_1_decorator );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_handler );
    Py_DECREF( par_handler );
    par_handler = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_handler );
    Py_DECREF( par_handler );
    par_handler = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register$$$function_1_decorator );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_2_get_by_name( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_name = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_4411806d0c478c17e0ae7a40d162bc9d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_preserved_type_1;
    PyObject *exception_preserved_value_1;
    PyTracebackObject *exception_preserved_tb_1;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_4411806d0c478c17e0ae7a40d162bc9d = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4411806d0c478c17e0ae7a40d162bc9d, codeobj_4411806d0c478c17e0ae7a40d162bc9d, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_4411806d0c478c17e0ae7a40d162bc9d = cache_frame_4411806d0c478c17e0ae7a40d162bc9d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4411806d0c478c17e0ae7a40d162bc9d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4411806d0c478c17e0ae7a40d162bc9d ) == 2 ); // Frame stack

    // Framed code:
    // Tried code:
    {
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_subscript_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain__readline_commands );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain__readline_commands );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "_readline_commands" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 46;
            type_description_1 = "o";
            goto try_except_handler_2;
        }

        tmp_subscribed_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_name );
        tmp_subscript_name_1 = par_name;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 46;
            type_description_1 = "o";
            goto try_except_handler_2;
        }
        goto frame_return_exit_1;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_2_get_by_name );
    return NULL;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Preserve existing published exception.
    exception_preserved_type_1 = EXC_TYPE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_type_1 );
    exception_preserved_value_1 = EXC_VALUE(PyThreadState_GET());
    Py_XINCREF( exception_preserved_value_1 );
    exception_preserved_tb_1 = (PyTracebackObject *)EXC_TRACEBACK(PyThreadState_GET());
    Py_XINCREF( exception_preserved_tb_1 );

    if ( exception_keeper_tb_1 == NULL )
    {
        exception_keeper_tb_1 = MAKE_TRACEBACK( frame_4411806d0c478c17e0ae7a40d162bc9d, exception_keeper_lineno_1 );
    }
    else if ( exception_keeper_lineno_1 != 0 )
    {
        exception_keeper_tb_1 = ADD_TRACEBACK( exception_keeper_tb_1, frame_4411806d0c478c17e0ae7a40d162bc9d, exception_keeper_lineno_1 );
    }

    NORMALIZE_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    PyException_SetTraceback( exception_keeper_value_1, (PyObject *)exception_keeper_tb_1 );
    PUBLISH_EXCEPTION( &exception_keeper_type_1, &exception_keeper_value_1, &exception_keeper_tb_1 );
    // Tried code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        tmp_compexpr_left_1 = EXC_TYPE(PyThreadState_GET());
        tmp_compexpr_right_1 = PyExc_KeyError;
        tmp_res = EXCEPTION_MATCH_BOOL( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 47;
            type_description_1 = "o";
            goto try_except_handler_3;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_raise_type_1;
            PyObject *tmp_make_exception_arg_1;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            tmp_left_name_1 = const_str_digest_58304b5642f2a8741eb67b7c2ad22ce5;
            CHECK_OBJECT( par_name );
            tmp_right_name_1 = par_name;
            tmp_make_exception_arg_1 = BINARY_OPERATION_REMAINDER( tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_make_exception_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 48;
                type_description_1 = "o";
                goto try_except_handler_3;
            }
            frame_4411806d0c478c17e0ae7a40d162bc9d->m_frame.f_lineno = 48;
            {
                PyObject *call_args[] = { tmp_make_exception_arg_1 };
                tmp_raise_type_1 = CALL_FUNCTION_WITH_ARGS1( PyExc_KeyError, call_args );
            }

            Py_DECREF( tmp_make_exception_arg_1 );
            assert( !(tmp_raise_type_1 == NULL) );
            exception_type = tmp_raise_type_1;
            exception_lineno = 48;
            RAISE_EXCEPTION_WITH_TYPE( &exception_type, &exception_value, &exception_tb );
            type_description_1 = "o";
            goto try_except_handler_3;
        }
        goto branch_end_1;
        branch_no_1:;
        tmp_result = RERAISE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
        if (unlikely( tmp_result == false ))
        {
            exception_lineno = 45;
        }

        if (exception_tb && exception_tb->tb_frame == &frame_4411806d0c478c17e0ae7a40d162bc9d->m_frame) frame_4411806d0c478c17e0ae7a40d162bc9d->m_frame.f_lineno = exception_tb->tb_lineno;
        type_description_1 = "o";
        goto try_except_handler_3;
        branch_end_1:;
    }
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_2_get_by_name );
    return NULL;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    // Restore previous exception.
    SET_CURRENT_EXCEPTION( exception_preserved_type_1, exception_preserved_value_1, exception_preserved_tb_1 );
    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    // End of try:

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4411806d0c478c17e0ae7a40d162bc9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_4411806d0c478c17e0ae7a40d162bc9d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4411806d0c478c17e0ae7a40d162bc9d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4411806d0c478c17e0ae7a40d162bc9d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4411806d0c478c17e0ae7a40d162bc9d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4411806d0c478c17e0ae7a40d162bc9d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4411806d0c478c17e0ae7a40d162bc9d,
        type_description_1,
        par_name
    );


    // Release cached frame.
    if ( frame_4411806d0c478c17e0ae7a40d162bc9d == cache_frame_4411806d0c478c17e0ae7a40d162bc9d )
    {
        Py_DECREF( frame_4411806d0c478c17e0ae7a40d162bc9d );
    }
    cache_frame_4411806d0c478c17e0ae7a40d162bc9d = NULL;

    assertFrameObject( frame_4411806d0c478c17e0ae7a40d162bc9d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_2_get_by_name );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_name );
    Py_DECREF( par_name );
    par_name = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_2_get_by_name );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_3_beginning_of_line( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_749f2ff2a66d38422ecbc1eff69df74d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_749f2ff2a66d38422ecbc1eff69df74d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_749f2ff2a66d38422ecbc1eff69df74d, codeobj_749f2ff2a66d38422ecbc1eff69df74d, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_749f2ff2a66d38422ecbc1eff69df74d = cache_frame_749f2ff2a66d38422ecbc1eff69df74d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_749f2ff2a66d38422ecbc1eff69df74d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_749f2ff2a66d38422ecbc1eff69df74d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 59;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_buff );
        tmp_source_name_2 = var_buff;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cursor_position );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( tmp_inplace_assign_attr_1__start == NULL );
        tmp_inplace_assign_attr_1__start = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
        tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
        CHECK_OBJECT( var_buff );
        tmp_source_name_4 = var_buff;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_document );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_get_start_of_line_position );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_0bf8402271879b64e927f26f79aacd8b );
        frame_749f2ff2a66d38422ecbc1eff69df74d->m_frame.f_lineno = 60;
        tmp_right_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_assign_source_3 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_inplace_assign_attr_1__end == NULL );
        tmp_inplace_assign_attr_1__end = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
        tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
        CHECK_OBJECT( var_buff );
        tmp_assattr_target_1 = var_buff;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 60;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_749f2ff2a66d38422ecbc1eff69df74d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_749f2ff2a66d38422ecbc1eff69df74d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_749f2ff2a66d38422ecbc1eff69df74d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_749f2ff2a66d38422ecbc1eff69df74d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_749f2ff2a66d38422ecbc1eff69df74d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_749f2ff2a66d38422ecbc1eff69df74d,
        type_description_1,
        par_event,
        var_buff
    );


    // Release cached frame.
    if ( frame_749f2ff2a66d38422ecbc1eff69df74d == cache_frame_749f2ff2a66d38422ecbc1eff69df74d )
    {
        Py_DECREF( frame_749f2ff2a66d38422ecbc1eff69df74d );
    }
    cache_frame_749f2ff2a66d38422ecbc1eff69df74d = NULL;

    assertFrameObject( frame_749f2ff2a66d38422ecbc1eff69df74d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_3_beginning_of_line );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_3_beginning_of_line );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_4_end_of_line( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_c3b4b5c70d8cd76855f0bfd713e153ba;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_c3b4b5c70d8cd76855f0bfd713e153ba = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c3b4b5c70d8cd76855f0bfd713e153ba, codeobj_c3b4b5c70d8cd76855f0bfd713e153ba, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_c3b4b5c70d8cd76855f0bfd713e153ba = cache_frame_c3b4b5c70d8cd76855f0bfd713e153ba;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c3b4b5c70d8cd76855f0bfd713e153ba );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c3b4b5c70d8cd76855f0bfd713e153ba ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 66;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_buff );
        tmp_source_name_2 = var_buff;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cursor_position );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( tmp_inplace_assign_attr_1__start == NULL );
        tmp_inplace_assign_attr_1__start = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
        tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        frame_c3b4b5c70d8cd76855f0bfd713e153ba->m_frame.f_lineno = 67;
        tmp_right_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_end_of_line_position );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_assign_source_3 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_inplace_assign_attr_1__end == NULL );
        tmp_inplace_assign_attr_1__end = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
        tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
        CHECK_OBJECT( var_buff );
        tmp_assattr_target_1 = var_buff;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 67;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c3b4b5c70d8cd76855f0bfd713e153ba );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c3b4b5c70d8cd76855f0bfd713e153ba );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c3b4b5c70d8cd76855f0bfd713e153ba, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c3b4b5c70d8cd76855f0bfd713e153ba->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c3b4b5c70d8cd76855f0bfd713e153ba, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c3b4b5c70d8cd76855f0bfd713e153ba,
        type_description_1,
        par_event,
        var_buff
    );


    // Release cached frame.
    if ( frame_c3b4b5c70d8cd76855f0bfd713e153ba == cache_frame_c3b4b5c70d8cd76855f0bfd713e153ba )
    {
        Py_DECREF( frame_c3b4b5c70d8cd76855f0bfd713e153ba );
    }
    cache_frame_c3b4b5c70d8cd76855f0bfd713e153ba = NULL;

    assertFrameObject( frame_c3b4b5c70d8cd76855f0bfd713e153ba );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_4_end_of_line );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_4_end_of_line );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_5_forward_char( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_3ce7bc74879e0ac013b3102a55e00fba;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_3ce7bc74879e0ac013b3102a55e00fba = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3ce7bc74879e0ac013b3102a55e00fba, codeobj_3ce7bc74879e0ac013b3102a55e00fba, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_3ce7bc74879e0ac013b3102a55e00fba = cache_frame_3ce7bc74879e0ac013b3102a55e00fba;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3ce7bc74879e0ac013b3102a55e00fba );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3ce7bc74879e0ac013b3102a55e00fba ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 73;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_buff );
        tmp_source_name_2 = var_buff;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cursor_position );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( tmp_inplace_assign_attr_1__start == NULL );
        tmp_inplace_assign_attr_1__start = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
        tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
        CHECK_OBJECT( var_buff );
        tmp_source_name_4 = var_buff;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_document );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_get_cursor_right_position );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 74;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_3ce7bc74879e0ac013b3102a55e00fba->m_frame.f_lineno = 74;
        tmp_right_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_assign_source_3 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_inplace_assign_attr_1__end == NULL );
        tmp_inplace_assign_attr_1__end = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
        tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
        CHECK_OBJECT( var_buff );
        tmp_assattr_target_1 = var_buff;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 74;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3ce7bc74879e0ac013b3102a55e00fba );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3ce7bc74879e0ac013b3102a55e00fba );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3ce7bc74879e0ac013b3102a55e00fba, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3ce7bc74879e0ac013b3102a55e00fba->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3ce7bc74879e0ac013b3102a55e00fba, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3ce7bc74879e0ac013b3102a55e00fba,
        type_description_1,
        par_event,
        var_buff
    );


    // Release cached frame.
    if ( frame_3ce7bc74879e0ac013b3102a55e00fba == cache_frame_3ce7bc74879e0ac013b3102a55e00fba )
    {
        Py_DECREF( frame_3ce7bc74879e0ac013b3102a55e00fba );
    }
    cache_frame_3ce7bc74879e0ac013b3102a55e00fba = NULL;

    assertFrameObject( frame_3ce7bc74879e0ac013b3102a55e00fba );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_5_forward_char );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_5_forward_char );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_6_backward_char( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_24a983b79c05a66d729037082626d405;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_24a983b79c05a66d729037082626d405 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_24a983b79c05a66d729037082626d405, codeobj_24a983b79c05a66d729037082626d405, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_24a983b79c05a66d729037082626d405 = cache_frame_24a983b79c05a66d729037082626d405;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_24a983b79c05a66d729037082626d405 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_24a983b79c05a66d729037082626d405 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 80;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_buff );
        tmp_source_name_2 = var_buff;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cursor_position );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( tmp_inplace_assign_attr_1__start == NULL );
        tmp_inplace_assign_attr_1__start = tmp_assign_source_2;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
        tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
        CHECK_OBJECT( var_buff );
        tmp_source_name_4 = var_buff;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_document );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_get_cursor_left_position );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 81;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_24a983b79c05a66d729037082626d405->m_frame.f_lineno = 81;
        tmp_right_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        tmp_assign_source_3 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oo";
            goto try_except_handler_2;
        }
        assert( tmp_inplace_assign_attr_1__end == NULL );
        tmp_inplace_assign_attr_1__end = tmp_assign_source_3;
    }
    // Tried code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
        tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
        CHECK_OBJECT( var_buff );
        tmp_assattr_target_1 = var_buff;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 81;
            type_description_1 = "oo";
            goto try_except_handler_3;
        }
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_3:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto try_except_handler_2;
    // End of try:
    try_end_1:;
    goto try_end_2;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto frame_exception_exit_1;
    // End of try:
    try_end_2:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_24a983b79c05a66d729037082626d405 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_24a983b79c05a66d729037082626d405 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_24a983b79c05a66d729037082626d405, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_24a983b79c05a66d729037082626d405->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_24a983b79c05a66d729037082626d405, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_24a983b79c05a66d729037082626d405,
        type_description_1,
        par_event,
        var_buff
    );


    // Release cached frame.
    if ( frame_24a983b79c05a66d729037082626d405 == cache_frame_24a983b79c05a66d729037082626d405 )
    {
        Py_DECREF( frame_24a983b79c05a66d729037082626d405 );
    }
    cache_frame_24a983b79c05a66d729037082626d405 = NULL;

    assertFrameObject( frame_24a983b79c05a66d729037082626d405 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
    Py_DECREF( tmp_inplace_assign_attr_1__end );
    tmp_inplace_assign_attr_1__end = NULL;

    CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
    Py_DECREF( tmp_inplace_assign_attr_1__start );
    tmp_inplace_assign_attr_1__start = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_6_backward_char );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_6_backward_char );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_7_forward_word( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_pos = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_1d9ea5d5080207ab5bda5f057edfb276;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_1d9ea5d5080207ab5bda5f057edfb276 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1d9ea5d5080207ab5bda5f057edfb276, codeobj_1d9ea5d5080207ab5bda5f057edfb276, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_1d9ea5d5080207ab5bda5f057edfb276 = cache_frame_1d9ea5d5080207ab5bda5f057edfb276;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1d9ea5d5080207ab5bda5f057edfb276 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1d9ea5d5080207ab5bda5f057edfb276 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 90;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_find_next_word_ending );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 91;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_1d9ea5d5080207ab5bda5f057edfb276->m_frame.f_lineno = 91;
        tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 91;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_pos == NULL );
        var_pos = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_pos );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_pos );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 93;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( var_buff );
            tmp_source_name_5 = var_buff;
            tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_cursor_position );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_inplace_assign_attr_1__start == NULL );
            tmp_inplace_assign_attr_1__start = tmp_assign_source_3;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
            tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
            CHECK_OBJECT( var_pos );
            tmp_right_name_1 = var_pos;
            tmp_assign_source_4 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            assert( tmp_inplace_assign_attr_1__end == NULL );
            tmp_inplace_assign_attr_1__end = tmp_assign_source_4;
        }
        // Tried code:
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
            tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
            CHECK_OBJECT( var_buff );
            tmp_assattr_target_1 = var_buff;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 94;
                type_description_1 = "ooo";
                goto try_except_handler_3;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d9ea5d5080207ab5bda5f057edfb276 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1d9ea5d5080207ab5bda5f057edfb276 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1d9ea5d5080207ab5bda5f057edfb276, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1d9ea5d5080207ab5bda5f057edfb276->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1d9ea5d5080207ab5bda5f057edfb276, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1d9ea5d5080207ab5bda5f057edfb276,
        type_description_1,
        par_event,
        var_buff,
        var_pos
    );


    // Release cached frame.
    if ( frame_1d9ea5d5080207ab5bda5f057edfb276 == cache_frame_1d9ea5d5080207ab5bda5f057edfb276 )
    {
        Py_DECREF( frame_1d9ea5d5080207ab5bda5f057edfb276 );
    }
    cache_frame_1d9ea5d5080207ab5bda5f057edfb276 = NULL;

    assertFrameObject( frame_1d9ea5d5080207ab5bda5f057edfb276 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_7_forward_word );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    CHECK_OBJECT( (PyObject *)var_pos );
    Py_DECREF( var_pos );
    var_pos = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_7_forward_word );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_8_backward_word( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_pos = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_767bcb9ae21587b5979dce639ee2d6a3;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_767bcb9ae21587b5979dce639ee2d6a3 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_767bcb9ae21587b5979dce639ee2d6a3, codeobj_767bcb9ae21587b5979dce639ee2d6a3, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_767bcb9ae21587b5979dce639ee2d6a3 = cache_frame_767bcb9ae21587b5979dce639ee2d6a3;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_767bcb9ae21587b5979dce639ee2d6a3 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_767bcb9ae21587b5979dce639ee2d6a3 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 103;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_find_previous_word_beginning );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 104;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_767bcb9ae21587b5979dce639ee2d6a3->m_frame.f_lineno = 104;
        tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 104;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_pos == NULL );
        var_pos = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_pos );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_pos );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 106;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( var_buff );
            tmp_source_name_5 = var_buff;
            tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_cursor_position );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 107;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( tmp_inplace_assign_attr_1__start == NULL );
            tmp_inplace_assign_attr_1__start = tmp_assign_source_3;
        }
        // Tried code:
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_left_name_1;
            PyObject *tmp_right_name_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
            tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
            CHECK_OBJECT( var_pos );
            tmp_right_name_1 = var_pos;
            tmp_assign_source_4 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 107;
                type_description_1 = "ooo";
                goto try_except_handler_2;
            }
            assert( tmp_inplace_assign_attr_1__end == NULL );
            tmp_inplace_assign_attr_1__end = tmp_assign_source_4;
        }
        // Tried code:
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
            tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
            CHECK_OBJECT( var_buff );
            tmp_assattr_target_1 = var_buff;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 107;
                type_description_1 = "ooo";
                goto try_except_handler_3;
            }
        }
        goto try_end_1;
        // Exception handler code:
        try_except_handler_3:;
        exception_keeper_type_1 = exception_type;
        exception_keeper_value_1 = exception_value;
        exception_keeper_tb_1 = exception_tb;
        exception_keeper_lineno_1 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_1;
        exception_value = exception_keeper_value_1;
        exception_tb = exception_keeper_tb_1;
        exception_lineno = exception_keeper_lineno_1;

        goto try_except_handler_2;
        // End of try:
        try_end_1:;
        goto try_end_2;
        // Exception handler code:
        try_except_handler_2:;
        exception_keeper_type_2 = exception_type;
        exception_keeper_value_2 = exception_value;
        exception_keeper_tb_2 = exception_tb;
        exception_keeper_lineno_2 = exception_lineno;
        exception_type = NULL;
        exception_value = NULL;
        exception_tb = NULL;
        exception_lineno = 0;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        // Re-raise.
        exception_type = exception_keeper_type_2;
        exception_value = exception_keeper_value_2;
        exception_tb = exception_keeper_tb_2;
        exception_lineno = exception_keeper_lineno_2;

        goto frame_exception_exit_1;
        // End of try:
        try_end_2:;
        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
        Py_DECREF( tmp_inplace_assign_attr_1__end );
        tmp_inplace_assign_attr_1__end = NULL;

        CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
        Py_DECREF( tmp_inplace_assign_attr_1__start );
        tmp_inplace_assign_attr_1__start = NULL;

        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_767bcb9ae21587b5979dce639ee2d6a3 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_767bcb9ae21587b5979dce639ee2d6a3 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_767bcb9ae21587b5979dce639ee2d6a3, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_767bcb9ae21587b5979dce639ee2d6a3->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_767bcb9ae21587b5979dce639ee2d6a3, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_767bcb9ae21587b5979dce639ee2d6a3,
        type_description_1,
        par_event,
        var_buff,
        var_pos
    );


    // Release cached frame.
    if ( frame_767bcb9ae21587b5979dce639ee2d6a3 == cache_frame_767bcb9ae21587b5979dce639ee2d6a3 )
    {
        Py_DECREF( frame_767bcb9ae21587b5979dce639ee2d6a3 );
    }
    cache_frame_767bcb9ae21587b5979dce639ee2d6a3 = NULL;

    assertFrameObject( frame_767bcb9ae21587b5979dce639ee2d6a3 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_8_backward_word );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    CHECK_OBJECT( (PyObject *)var_pos );
    Py_DECREF( var_pos );
    var_pos = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_8_backward_word );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_9_clear_screen( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_8b24d74de375253360b7bb31d8618a2b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8b24d74de375253360b7bb31d8618a2b = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8b24d74de375253360b7bb31d8618a2b, codeobj_8b24d74de375253360b7bb31d8618a2b, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_8b24d74de375253360b7bb31d8618a2b = cache_frame_8b24d74de375253360b7bb31d8618a2b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8b24d74de375253360b7bb31d8618a2b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8b24d74de375253360b7bb31d8618a2b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_app );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_renderer );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_8b24d74de375253360b7bb31d8618a2b->m_frame.f_lineno = 115;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_clear );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8b24d74de375253360b7bb31d8618a2b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8b24d74de375253360b7bb31d8618a2b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8b24d74de375253360b7bb31d8618a2b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8b24d74de375253360b7bb31d8618a2b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8b24d74de375253360b7bb31d8618a2b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8b24d74de375253360b7bb31d8618a2b,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_8b24d74de375253360b7bb31d8618a2b == cache_frame_8b24d74de375253360b7bb31d8618a2b )
    {
        Py_DECREF( frame_8b24d74de375253360b7bb31d8618a2b );
    }
    cache_frame_8b24d74de375253360b7bb31d8618a2b = NULL;

    assertFrameObject( frame_8b24d74de375253360b7bb31d8618a2b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_9_clear_screen );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_9_clear_screen );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_10_redraw_current_line( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_10_redraw_current_line );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_10_redraw_current_line );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_11_accept_line( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c6053e4667cff011640e3abbd139e7da;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c6053e4667cff011640e3abbd139e7da = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c6053e4667cff011640e3abbd139e7da, codeobj_c6053e4667cff011640e3abbd139e7da, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_c6053e4667cff011640e3abbd139e7da = cache_frame_c6053e4667cff011640e3abbd139e7da;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c6053e4667cff011640e3abbd139e7da );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c6053e4667cff011640e3abbd139e7da ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_c6053e4667cff011640e3abbd139e7da->m_frame.f_lineno = 135;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_validate_and_handle );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c6053e4667cff011640e3abbd139e7da );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c6053e4667cff011640e3abbd139e7da );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c6053e4667cff011640e3abbd139e7da, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c6053e4667cff011640e3abbd139e7da->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c6053e4667cff011640e3abbd139e7da, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c6053e4667cff011640e3abbd139e7da,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_c6053e4667cff011640e3abbd139e7da == cache_frame_c6053e4667cff011640e3abbd139e7da )
    {
        Py_DECREF( frame_c6053e4667cff011640e3abbd139e7da );
    }
    cache_frame_c6053e4667cff011640e3abbd139e7da = NULL;

    assertFrameObject( frame_c6053e4667cff011640e3abbd139e7da );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_11_accept_line );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_11_accept_line );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_12_previous_history( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_90864adb7408db80ae55fc791e7071e7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_90864adb7408db80ae55fc791e7071e7 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_90864adb7408db80ae55fc791e7071e7, codeobj_90864adb7408db80ae55fc791e7071e7, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_90864adb7408db80ae55fc791e7071e7 = cache_frame_90864adb7408db80ae55fc791e7071e7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_90864adb7408db80ae55fc791e7071e7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_90864adb7408db80ae55fc791e7071e7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_history_backward );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 141;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_90864adb7408db80ae55fc791e7071e7->m_frame.f_lineno = 141;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_90864adb7408db80ae55fc791e7071e7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_90864adb7408db80ae55fc791e7071e7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_90864adb7408db80ae55fc791e7071e7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_90864adb7408db80ae55fc791e7071e7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_90864adb7408db80ae55fc791e7071e7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_90864adb7408db80ae55fc791e7071e7,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_90864adb7408db80ae55fc791e7071e7 == cache_frame_90864adb7408db80ae55fc791e7071e7 )
    {
        Py_DECREF( frame_90864adb7408db80ae55fc791e7071e7 );
    }
    cache_frame_90864adb7408db80ae55fc791e7071e7 = NULL;

    assertFrameObject( frame_90864adb7408db80ae55fc791e7071e7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_12_previous_history );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_12_previous_history );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_13_next_history( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_338ce756c26e6afd0cb3658efccaadb6;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_338ce756c26e6afd0cb3658efccaadb6 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_338ce756c26e6afd0cb3658efccaadb6, codeobj_338ce756c26e6afd0cb3658efccaadb6, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_338ce756c26e6afd0cb3658efccaadb6 = cache_frame_338ce756c26e6afd0cb3658efccaadb6;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_338ce756c26e6afd0cb3658efccaadb6 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_338ce756c26e6afd0cb3658efccaadb6 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_history_forward );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 147;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_338ce756c26e6afd0cb3658efccaadb6->m_frame.f_lineno = 147;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_338ce756c26e6afd0cb3658efccaadb6 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_338ce756c26e6afd0cb3658efccaadb6 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_338ce756c26e6afd0cb3658efccaadb6, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_338ce756c26e6afd0cb3658efccaadb6->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_338ce756c26e6afd0cb3658efccaadb6, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_338ce756c26e6afd0cb3658efccaadb6,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_338ce756c26e6afd0cb3658efccaadb6 == cache_frame_338ce756c26e6afd0cb3658efccaadb6 )
    {
        Py_DECREF( frame_338ce756c26e6afd0cb3658efccaadb6 );
    }
    cache_frame_338ce756c26e6afd0cb3658efccaadb6 = NULL;

    assertFrameObject( frame_338ce756c26e6afd0cb3658efccaadb6 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_13_next_history );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_13_next_history );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_14_beginning_of_history( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_12cd250f02ebc71ccdb2ff48752774c5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_12cd250f02ebc71ccdb2ff48752774c5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_12cd250f02ebc71ccdb2ff48752774c5, codeobj_12cd250f02ebc71ccdb2ff48752774c5, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_12cd250f02ebc71ccdb2ff48752774c5 = cache_frame_12cd250f02ebc71ccdb2ff48752774c5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_12cd250f02ebc71ccdb2ff48752774c5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_12cd250f02ebc71ccdb2ff48752774c5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_12cd250f02ebc71ccdb2ff48752774c5->m_frame.f_lineno = 153;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_go_to_history, &PyTuple_GET_ITEM( const_tuple_int_0_tuple, 0 ) );

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 153;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_12cd250f02ebc71ccdb2ff48752774c5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_12cd250f02ebc71ccdb2ff48752774c5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_12cd250f02ebc71ccdb2ff48752774c5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_12cd250f02ebc71ccdb2ff48752774c5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_12cd250f02ebc71ccdb2ff48752774c5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_12cd250f02ebc71ccdb2ff48752774c5,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_12cd250f02ebc71ccdb2ff48752774c5 == cache_frame_12cd250f02ebc71ccdb2ff48752774c5 )
    {
        Py_DECREF( frame_12cd250f02ebc71ccdb2ff48752774c5 );
    }
    cache_frame_12cd250f02ebc71ccdb2ff48752774c5 = NULL;

    assertFrameObject( frame_12cd250f02ebc71ccdb2ff48752774c5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_14_beginning_of_history );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_14_beginning_of_history );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_15_end_of_history( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    struct Nuitka_FrameObject *frame_48e352436fa387cc54d3a74128217bbf;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_48e352436fa387cc54d3a74128217bbf = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_48e352436fa387cc54d3a74128217bbf, codeobj_48e352436fa387cc54d3a74128217bbf, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_48e352436fa387cc54d3a74128217bbf = cache_frame_48e352436fa387cc54d3a74128217bbf;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_48e352436fa387cc54d3a74128217bbf );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_48e352436fa387cc54d3a74128217bbf ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_history_forward );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_2de9ada199d63ba5bdc7c9b31335d708 );
        frame_48e352436fa387cc54d3a74128217bbf->m_frame.f_lineno = 161;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 162;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( var_buff );
        tmp_source_name_4 = var_buff;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_go_to_history );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_buff );
        tmp_source_name_5 = var_buff;
        tmp_len_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain__working_lines );
        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 163;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_left_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 163;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = const_int_pos_1;
        tmp_args_element_name_1 = BINARY_OPERATION_SUB_LONG_LONG( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        assert( !(tmp_args_element_name_1 == NULL) );
        frame_48e352436fa387cc54d3a74128217bbf->m_frame.f_lineno = 163;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 163;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_48e352436fa387cc54d3a74128217bbf );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_48e352436fa387cc54d3a74128217bbf );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_48e352436fa387cc54d3a74128217bbf, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_48e352436fa387cc54d3a74128217bbf->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_48e352436fa387cc54d3a74128217bbf, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_48e352436fa387cc54d3a74128217bbf,
        type_description_1,
        par_event,
        var_buff
    );


    // Release cached frame.
    if ( frame_48e352436fa387cc54d3a74128217bbf == cache_frame_48e352436fa387cc54d3a74128217bbf )
    {
        Py_DECREF( frame_48e352436fa387cc54d3a74128217bbf );
    }
    cache_frame_48e352436fa387cc54d3a74128217bbf = NULL;

    assertFrameObject( frame_48e352436fa387cc54d3a74128217bbf );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_15_end_of_history );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_15_end_of_history );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_16_reverse_search_history( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_control = NULL;
    struct Nuitka_FrameObject *frame_d5c1944153306d7fb7a9366ba732546a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_d5c1944153306d7fb7a9366ba732546a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d5c1944153306d7fb7a9366ba732546a, codeobj_d5c1944153306d7fb7a9366ba732546a, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_d5c1944153306d7fb7a9366ba732546a = cache_frame_d5c1944153306d7fb7a9366ba732546a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d5c1944153306d7fb7a9366ba732546a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d5c1944153306d7fb7a9366ba732546a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_layout );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_control );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_control == NULL );
        var_control = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_control );
        tmp_source_name_4 = var_control;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_search_buffer_control );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 174;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_source_name_5;
            PyObject *tmp_mvar_value_1;
            PyObject *tmp_assattr_target_1;
            PyObject *tmp_source_name_6;
            PyObject *tmp_source_name_7;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_SearchDirection );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_SearchDirection );
            }

            if ( tmp_mvar_value_1 == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "SearchDirection" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 175;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_5 = tmp_mvar_value_1;
            tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_BACKWARD );
            if ( tmp_assattr_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_event );
            tmp_source_name_7 = par_event;
            tmp_source_name_6 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_app );
            if ( tmp_source_name_6 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_1 );

                exception_lineno = 175;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_assattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_current_search_state );
            Py_DECREF( tmp_source_name_6 );
            if ( tmp_assattr_target_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_1 );

                exception_lineno = 175;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_direction, tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_name_1 );
            Py_DECREF( tmp_assattr_target_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 175;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
        }
        {
            PyObject *tmp_assattr_name_2;
            PyObject *tmp_source_name_8;
            PyObject *tmp_assattr_target_2;
            PyObject *tmp_source_name_9;
            PyObject *tmp_source_name_10;
            CHECK_OBJECT( var_control );
            tmp_source_name_8 = var_control;
            tmp_assattr_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_search_buffer_control );
            if ( tmp_assattr_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( par_event );
            tmp_source_name_10 = par_event;
            tmp_source_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_app );
            if ( tmp_source_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_2 );

                exception_lineno = 176;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_assattr_target_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_layout );
            Py_DECREF( tmp_source_name_9 );
            if ( tmp_assattr_target_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_assattr_name_2 );

                exception_lineno = 176;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_current_control, tmp_assattr_name_2 );
            Py_DECREF( tmp_assattr_name_2 );
            Py_DECREF( tmp_assattr_target_2 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 176;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d5c1944153306d7fb7a9366ba732546a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d5c1944153306d7fb7a9366ba732546a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d5c1944153306d7fb7a9366ba732546a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d5c1944153306d7fb7a9366ba732546a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d5c1944153306d7fb7a9366ba732546a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d5c1944153306d7fb7a9366ba732546a,
        type_description_1,
        par_event,
        var_control
    );


    // Release cached frame.
    if ( frame_d5c1944153306d7fb7a9366ba732546a == cache_frame_d5c1944153306d7fb7a9366ba732546a )
    {
        Py_DECREF( frame_d5c1944153306d7fb7a9366ba732546a );
    }
    cache_frame_d5c1944153306d7fb7a9366ba732546a = NULL;

    assertFrameObject( frame_d5c1944153306d7fb7a9366ba732546a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_16_reverse_search_history );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_control );
    Py_DECREF( var_control );
    var_control = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_control );
    var_control = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_16_reverse_search_history );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_17_end_of_file( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_50753afd5df05e0514e22929ed73d40a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_50753afd5df05e0514e22929ed73d40a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_50753afd5df05e0514e22929ed73d40a, codeobj_50753afd5df05e0514e22929ed73d40a, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_50753afd5df05e0514e22929ed73d40a = cache_frame_50753afd5df05e0514e22929ed73d40a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_50753afd5df05e0514e22929ed73d40a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_50753afd5df05e0514e22929ed73d40a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_app );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_50753afd5df05e0514e22929ed73d40a->m_frame.f_lineno = 188;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_exit );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 188;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_50753afd5df05e0514e22929ed73d40a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_50753afd5df05e0514e22929ed73d40a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_50753afd5df05e0514e22929ed73d40a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_50753afd5df05e0514e22929ed73d40a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_50753afd5df05e0514e22929ed73d40a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_50753afd5df05e0514e22929ed73d40a,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_50753afd5df05e0514e22929ed73d40a == cache_frame_50753afd5df05e0514e22929ed73d40a )
    {
        Py_DECREF( frame_50753afd5df05e0514e22929ed73d40a );
    }
    cache_frame_50753afd5df05e0514e22929ed73d40a = NULL;

    assertFrameObject( frame_50753afd5df05e0514e22929ed73d40a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_17_end_of_file );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_17_end_of_file );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_18_delete_char( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_deleted = NULL;
    struct Nuitka_FrameObject *frame_34ff4fc0dd5f52aacc6ec26d8e93f2df;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_34ff4fc0dd5f52aacc6ec26d8e93f2df = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_34ff4fc0dd5f52aacc6ec26d8e93f2df, codeobj_34ff4fc0dd5f52aacc6ec26d8e93f2df, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_34ff4fc0dd5f52aacc6ec26d8e93f2df = cache_frame_34ff4fc0dd5f52aacc6ec26d8e93f2df;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_34ff4fc0dd5f52aacc6ec26d8e93f2df );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_34ff4fc0dd5f52aacc6ec26d8e93f2df ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_delete );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 194;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_34ff4fc0dd5f52aacc6ec26d8e93f2df->m_frame.f_lineno = 194;
        tmp_assign_source_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_deleted == NULL );
        var_deleted = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_operand_name_1;
        CHECK_OBJECT( var_deleted );
        tmp_operand_name_1 = var_deleted;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 195;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_4;
            PyObject *tmp_source_name_5;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( par_event );
            tmp_source_name_5 = par_event;
            tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_app );
            if ( tmp_source_name_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_output );
            Py_DECREF( tmp_source_name_4 );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_34ff4fc0dd5f52aacc6ec26d8e93f2df->m_frame.f_lineno = 196;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_bell );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 196;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_34ff4fc0dd5f52aacc6ec26d8e93f2df );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_34ff4fc0dd5f52aacc6ec26d8e93f2df );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_34ff4fc0dd5f52aacc6ec26d8e93f2df, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_34ff4fc0dd5f52aacc6ec26d8e93f2df->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_34ff4fc0dd5f52aacc6ec26d8e93f2df, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_34ff4fc0dd5f52aacc6ec26d8e93f2df,
        type_description_1,
        par_event,
        var_deleted
    );


    // Release cached frame.
    if ( frame_34ff4fc0dd5f52aacc6ec26d8e93f2df == cache_frame_34ff4fc0dd5f52aacc6ec26d8e93f2df )
    {
        Py_DECREF( frame_34ff4fc0dd5f52aacc6ec26d8e93f2df );
    }
    cache_frame_34ff4fc0dd5f52aacc6ec26d8e93f2df = NULL;

    assertFrameObject( frame_34ff4fc0dd5f52aacc6ec26d8e93f2df );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_18_delete_char );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_deleted );
    Py_DECREF( var_deleted );
    var_deleted = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_deleted );
    var_deleted = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_18_delete_char );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_19_backward_delete_char( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_deleted = NULL;
    struct Nuitka_FrameObject *frame_0518d9e28a85d130dc7dd7830cf50ece;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_0518d9e28a85d130dc7dd7830cf50ece = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0518d9e28a85d130dc7dd7830cf50ece, codeobj_0518d9e28a85d130dc7dd7830cf50ece, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_0518d9e28a85d130dc7dd7830cf50ece = cache_frame_0518d9e28a85d130dc7dd7830cf50ece;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0518d9e28a85d130dc7dd7830cf50ece );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0518d9e28a85d130dc7dd7830cf50ece ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_arg );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_1;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_2;
            PyObject *tmp_source_name_3;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( par_event );
            tmp_source_name_3 = par_event;
            tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_current_buffer );
            if ( tmp_source_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_delete );
            Py_DECREF( tmp_source_name_2 );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_dict_key_1 = const_str_plain_count;
            CHECK_OBJECT( par_event );
            tmp_source_name_4 = par_event;
            tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_arg );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 205;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_1 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 205;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_0518d9e28a85d130dc7dd7830cf50ece->m_frame.f_lineno = 205;
            tmp_assign_source_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_assign_source_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 205;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( var_deleted == NULL );
            var_deleted = tmp_assign_source_1;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_source_name_6;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            PyObject *tmp_source_name_7;
            CHECK_OBJECT( par_event );
            tmp_source_name_6 = par_event;
            tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_current_buffer );
            if ( tmp_source_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 207;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_delete_before_cursor );
            Py_DECREF( tmp_source_name_5 );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 207;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_dict_key_2 = const_str_plain_count;
            CHECK_OBJECT( par_event );
            tmp_source_name_7 = par_event;
            tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_arg );
            if ( tmp_dict_value_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 207;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_2 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
            Py_DECREF( tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            frame_0518d9e28a85d130dc7dd7830cf50ece->m_frame.f_lineno = 207;
            tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 207;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            assert( var_deleted == NULL );
            var_deleted = tmp_assign_source_2;
        }
        branch_end_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        PyObject *tmp_operand_name_2;
        CHECK_OBJECT( var_deleted );
        tmp_operand_name_2 = var_deleted;
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 209;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = ( tmp_res == 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_8;
            PyObject *tmp_source_name_9;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( par_event );
            tmp_source_name_9 = par_event;
            tmp_source_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_app );
            if ( tmp_source_name_8 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_output );
            Py_DECREF( tmp_source_name_8 );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            frame_0518d9e28a85d130dc7dd7830cf50ece->m_frame.f_lineno = 210;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_bell );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 210;
                type_description_1 = "oo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0518d9e28a85d130dc7dd7830cf50ece );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0518d9e28a85d130dc7dd7830cf50ece );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0518d9e28a85d130dc7dd7830cf50ece, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0518d9e28a85d130dc7dd7830cf50ece->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0518d9e28a85d130dc7dd7830cf50ece, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0518d9e28a85d130dc7dd7830cf50ece,
        type_description_1,
        par_event,
        var_deleted
    );


    // Release cached frame.
    if ( frame_0518d9e28a85d130dc7dd7830cf50ece == cache_frame_0518d9e28a85d130dc7dd7830cf50ece )
    {
        Py_DECREF( frame_0518d9e28a85d130dc7dd7830cf50ece );
    }
    cache_frame_0518d9e28a85d130dc7dd7830cf50ece = NULL;

    assertFrameObject( frame_0518d9e28a85d130dc7dd7830cf50ece );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_19_backward_delete_char );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_deleted );
    Py_DECREF( var_deleted );
    var_deleted = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_deleted );
    var_deleted = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_19_backward_delete_char );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_20_self_insert( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_8468e9931c6c1f378d1338f0f777eb21;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8468e9931c6c1f378d1338f0f777eb21 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8468e9931c6c1f378d1338f0f777eb21, codeobj_8468e9931c6c1f378d1338f0f777eb21, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_8468e9931c6c1f378d1338f0f777eb21 = cache_frame_8468e9931c6c1f378d1338f0f777eb21;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8468e9931c6c1f378d1338f0f777eb21 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8468e9931c6c1f378d1338f0f777eb21 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_left_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_right_name_1;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_insert_text );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_data );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 216;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_arg );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 216;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = BINARY_OPERATION_MUL_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 216;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_8468e9931c6c1f378d1338f0f777eb21->m_frame.f_lineno = 216;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 216;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8468e9931c6c1f378d1338f0f777eb21 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8468e9931c6c1f378d1338f0f777eb21 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8468e9931c6c1f378d1338f0f777eb21, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8468e9931c6c1f378d1338f0f777eb21->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8468e9931c6c1f378d1338f0f777eb21, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8468e9931c6c1f378d1338f0f777eb21,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_8468e9931c6c1f378d1338f0f777eb21 == cache_frame_8468e9931c6c1f378d1338f0f777eb21 )
    {
        Py_DECREF( frame_8468e9931c6c1f378d1338f0f777eb21 );
    }
    cache_frame_8468e9931c6c1f378d1338f0f777eb21 = NULL;

    assertFrameObject( frame_8468e9931c6c1f378d1338f0f777eb21 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_20_self_insert );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_20_self_insert );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_21_transpose_chars( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_b = NULL;
    PyObject *var_p = NULL;
    PyObject *tmp_inplace_assign_attr_1__end = NULL;
    PyObject *tmp_inplace_assign_attr_1__start = NULL;
    struct Nuitka_FrameObject *frame_5a13762e2fb0e0384b5215a3360ed584;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    PyObject *tmp_return_value = NULL;
    bool tmp_result;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;
    static struct Nuitka_FrameObject *cache_frame_5a13762e2fb0e0384b5215a3360ed584 = NULL;
    PyObject *exception_keeper_type_3;
    PyObject *exception_keeper_value_3;
    PyTracebackObject *exception_keeper_tb_3;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_3;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5a13762e2fb0e0384b5215a3360ed584, codeobj_5a13762e2fb0e0384b5215a3360ed584, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_5a13762e2fb0e0384b5215a3360ed584 = cache_frame_5a13762e2fb0e0384b5215a3360ed584;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5a13762e2fb0e0384b5215a3360ed584 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5a13762e2fb0e0384b5215a3360ed584 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 227;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_b == NULL );
        var_b = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_b );
        tmp_source_name_2 = var_b;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cursor_position );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 228;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_p == NULL );
        var_p = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_p );
        tmp_compexpr_left_1 = var_p;
        tmp_compexpr_right_1 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 229;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        tmp_return_value = Py_None;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            int tmp_or_left_truth_1;
            nuitka_bool tmp_or_left_value_1;
            nuitka_bool tmp_or_right_value_1;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_len_arg_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_compexpr_left_3;
            PyObject *tmp_compexpr_right_3;
            PyObject *tmp_subscribed_name_1;
            PyObject *tmp_source_name_4;
            PyObject *tmp_subscript_name_1;
            CHECK_OBJECT( var_p );
            tmp_compexpr_left_2 = var_p;
            CHECK_OBJECT( var_b );
            tmp_source_name_3 = var_b;
            tmp_len_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_text );
            if ( tmp_len_arg_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = BUILTIN_LEN( tmp_len_arg_1 );
            Py_DECREF( tmp_len_arg_1 );
            if ( tmp_compexpr_right_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_right_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_or_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_or_left_truth_1 = tmp_or_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
            if ( tmp_or_left_truth_1 == 1 )
            {
                goto or_left_1;
            }
            else
            {
                goto or_right_1;
            }
            or_right_1:;
            CHECK_OBJECT( var_b );
            tmp_source_name_4 = var_b;
            tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_text );
            if ( tmp_subscribed_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_p );
            tmp_subscript_name_1 = var_p;
            tmp_compexpr_left_3 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
            Py_DECREF( tmp_subscribed_name_1 );
            if ( tmp_compexpr_left_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_3 = const_str_newline;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_3, tmp_compexpr_right_3 );
            Py_DECREF( tmp_compexpr_left_3 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 231;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_or_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            tmp_condition_result_2 = tmp_or_right_value_1;
            goto or_end_1;
            or_left_1:;
            tmp_condition_result_2 = tmp_or_left_value_1;
            or_end_1:;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_called_instance_1;
                PyObject *tmp_call_result_1;
                CHECK_OBJECT( var_b );
                tmp_called_instance_1 = var_b;
                frame_5a13762e2fb0e0384b5215a3360ed584->m_frame.f_lineno = 232;
                tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_swap_characters_before_cursor );
                if ( tmp_call_result_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 232;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_1 );
            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_source_name_5;
                CHECK_OBJECT( var_b );
                tmp_source_name_5 = var_b;
                tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_cursor_position );
                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                assert( tmp_inplace_assign_attr_1__start == NULL );
                tmp_inplace_assign_attr_1__start = tmp_assign_source_3;
            }
            // Tried code:
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_called_instance_2;
                PyObject *tmp_source_name_6;
                CHECK_OBJECT( tmp_inplace_assign_attr_1__start );
                tmp_left_name_1 = tmp_inplace_assign_attr_1__start;
                CHECK_OBJECT( var_b );
                tmp_source_name_6 = var_b;
                tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_document );
                if ( tmp_called_instance_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "ooo";
                    goto try_except_handler_2;
                }
                frame_5a13762e2fb0e0384b5215a3360ed584->m_frame.f_lineno = 234;
                tmp_right_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_cursor_right_position );
                Py_DECREF( tmp_called_instance_2 );
                if ( tmp_right_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "ooo";
                    goto try_except_handler_2;
                }
                tmp_assign_source_4 = BINARY_OPERATION( PyNumber_InPlaceAdd, tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "ooo";
                    goto try_except_handler_2;
                }
                assert( tmp_inplace_assign_attr_1__end == NULL );
                tmp_inplace_assign_attr_1__end = tmp_assign_source_4;
            }
            // Tried code:
            {
                PyObject *tmp_assattr_name_1;
                PyObject *tmp_assattr_target_1;
                CHECK_OBJECT( tmp_inplace_assign_attr_1__end );
                tmp_assattr_name_1 = tmp_inplace_assign_attr_1__end;
                CHECK_OBJECT( var_b );
                tmp_assattr_target_1 = var_b;
                tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_cursor_position, tmp_assattr_name_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 234;
                    type_description_1 = "ooo";
                    goto try_except_handler_3;
                }
            }
            goto try_end_1;
            // Exception handler code:
            try_except_handler_3:;
            exception_keeper_type_1 = exception_type;
            exception_keeper_value_1 = exception_value;
            exception_keeper_tb_1 = exception_tb;
            exception_keeper_lineno_1 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
            Py_DECREF( tmp_inplace_assign_attr_1__end );
            tmp_inplace_assign_attr_1__end = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_1;
            exception_value = exception_keeper_value_1;
            exception_tb = exception_keeper_tb_1;
            exception_lineno = exception_keeper_lineno_1;

            goto try_except_handler_2;
            // End of try:
            try_end_1:;
            goto try_end_2;
            // Exception handler code:
            try_except_handler_2:;
            exception_keeper_type_2 = exception_type;
            exception_keeper_value_2 = exception_value;
            exception_keeper_tb_2 = exception_tb;
            exception_keeper_lineno_2 = exception_lineno;
            exception_type = NULL;
            exception_value = NULL;
            exception_tb = NULL;
            exception_lineno = 0;

            CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
            Py_DECREF( tmp_inplace_assign_attr_1__start );
            tmp_inplace_assign_attr_1__start = NULL;

            // Re-raise.
            exception_type = exception_keeper_type_2;
            exception_value = exception_keeper_value_2;
            exception_tb = exception_keeper_tb_2;
            exception_lineno = exception_keeper_lineno_2;

            goto frame_exception_exit_1;
            // End of try:
            try_end_2:;
            CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__end );
            Py_DECREF( tmp_inplace_assign_attr_1__end );
            tmp_inplace_assign_attr_1__end = NULL;

            CHECK_OBJECT( (PyObject *)tmp_inplace_assign_attr_1__start );
            Py_DECREF( tmp_inplace_assign_attr_1__start );
            tmp_inplace_assign_attr_1__start = NULL;

            {
                PyObject *tmp_called_instance_3;
                PyObject *tmp_call_result_2;
                CHECK_OBJECT( var_b );
                tmp_called_instance_3 = var_b;
                frame_5a13762e2fb0e0384b5215a3360ed584->m_frame.f_lineno = 235;
                tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_swap_characters_before_cursor );
                if ( tmp_call_result_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 235;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                Py_DECREF( tmp_call_result_2 );
            }
            branch_end_2:;
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a13762e2fb0e0384b5215a3360ed584 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a13762e2fb0e0384b5215a3360ed584 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5a13762e2fb0e0384b5215a3360ed584 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5a13762e2fb0e0384b5215a3360ed584, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5a13762e2fb0e0384b5215a3360ed584->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5a13762e2fb0e0384b5215a3360ed584, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5a13762e2fb0e0384b5215a3360ed584,
        type_description_1,
        par_event,
        var_b,
        var_p
    );


    // Release cached frame.
    if ( frame_5a13762e2fb0e0384b5215a3360ed584 == cache_frame_5a13762e2fb0e0384b5215a3360ed584 )
    {
        Py_DECREF( frame_5a13762e2fb0e0384b5215a3360ed584 );
    }
    cache_frame_5a13762e2fb0e0384b5215a3360ed584 = NULL;

    assertFrameObject( frame_5a13762e2fb0e0384b5215a3360ed584 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_21_transpose_chars );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_b );
    Py_DECREF( var_b );
    var_b = NULL;

    CHECK_OBJECT( (PyObject *)var_p );
    Py_DECREF( var_p );
    var_p = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_3 = exception_type;
    exception_keeper_value_3 = exception_value;
    exception_keeper_tb_3 = exception_tb;
    exception_keeper_lineno_3 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_b );
    var_b = NULL;

    Py_XDECREF( var_p );
    var_p = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_3;
    exception_value = exception_keeper_value_3;
    exception_tb = exception_keeper_tb_3;
    exception_lineno = exception_keeper_lineno_3;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_21_transpose_chars );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_22_uppercase_word( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_i = NULL;
    PyObject *var_pos = NULL;
    PyObject *var_words = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_e121294c05d8b3cba10d7b2078f3a52a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_e121294c05d8b3cba10d7b2078f3a52a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e121294c05d8b3cba10d7b2078f3a52a, codeobj_e121294c05d8b3cba10d7b2078f3a52a, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_e121294c05d8b3cba10d7b2078f3a52a = cache_frame_e121294c05d8b3cba10d7b2078f3a52a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e121294c05d8b3cba10d7b2078f3a52a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e121294c05d8b3cba10d7b2078f3a52a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 243;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_range );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_range );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "range" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 245;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 245;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_e121294c05d8b3cba10d7b2078f3a52a->m_frame.f_lineno = 245;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 245;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 245;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                exception_lineno = 245;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_4;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        frame_e121294c05d8b3cba10d7b2078f3a52a->m_frame.f_lineno = 246;
        tmp_assign_source_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_find_next_word_ending );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 246;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_pos;
            var_pos = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_step_name_1;
        CHECK_OBJECT( var_buff );
        tmp_source_name_5 = var_buff;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_document );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_text_after_cursor );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_start_name_1 = Py_None;
        CHECK_OBJECT( var_pos );
        tmp_stop_name_1 = var_pos;
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_assign_source_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 247;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_words;
            var_words = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( var_buff );
        tmp_source_name_6 = var_buff;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_insert_text );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_words );
        tmp_called_instance_2 = var_words;
        frame_e121294c05d8b3cba10d7b2078f3a52a->m_frame.f_lineno = 248;
        tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_upper );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 248;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_3c1e954997caa37c7b6a50568aed10db );
        frame_e121294c05d8b3cba10d7b2078f3a52a->m_frame.f_lineno = 248;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 248;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 245;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e121294c05d8b3cba10d7b2078f3a52a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e121294c05d8b3cba10d7b2078f3a52a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e121294c05d8b3cba10d7b2078f3a52a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e121294c05d8b3cba10d7b2078f3a52a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e121294c05d8b3cba10d7b2078f3a52a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e121294c05d8b3cba10d7b2078f3a52a,
        type_description_1,
        par_event,
        var_buff,
        var_i,
        var_pos,
        var_words
    );


    // Release cached frame.
    if ( frame_e121294c05d8b3cba10d7b2078f3a52a == cache_frame_e121294c05d8b3cba10d7b2078f3a52a )
    {
        Py_DECREF( frame_e121294c05d8b3cba10d7b2078f3a52a );
    }
    cache_frame_e121294c05d8b3cba10d7b2078f3a52a = NULL;

    assertFrameObject( frame_e121294c05d8b3cba10d7b2078f3a52a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_22_uppercase_word );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_words );
    var_words = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_words );
    var_words = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_22_uppercase_word );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_23_downcase_word( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_i = NULL;
    PyObject *var_pos = NULL;
    PyObject *var_words = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_690bd84dbafdd852898db42272c38b83;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_690bd84dbafdd852898db42272c38b83 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_690bd84dbafdd852898db42272c38b83, codeobj_690bd84dbafdd852898db42272c38b83, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_690bd84dbafdd852898db42272c38b83 = cache_frame_690bd84dbafdd852898db42272c38b83;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_690bd84dbafdd852898db42272c38b83 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_690bd84dbafdd852898db42272c38b83 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 256;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_range );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_range );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "range" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 258;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 258;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_690bd84dbafdd852898db42272c38b83->m_frame.f_lineno = 258;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 258;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 258;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                exception_lineno = 258;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_4;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        frame_690bd84dbafdd852898db42272c38b83->m_frame.f_lineno = 259;
        tmp_assign_source_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_find_next_word_ending );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 259;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_pos;
            var_pos = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_step_name_1;
        CHECK_OBJECT( var_buff );
        tmp_source_name_5 = var_buff;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_document );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 260;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_text_after_cursor );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 260;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_start_name_1 = Py_None;
        CHECK_OBJECT( var_pos );
        tmp_stop_name_1 = var_pos;
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_assign_source_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 260;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_words;
            var_words = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( var_buff );
        tmp_source_name_6 = var_buff;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_insert_text );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 261;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_words );
        tmp_called_instance_2 = var_words;
        frame_690bd84dbafdd852898db42272c38b83->m_frame.f_lineno = 261;
        tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_lower );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 261;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_3c1e954997caa37c7b6a50568aed10db );
        frame_690bd84dbafdd852898db42272c38b83->m_frame.f_lineno = 261;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 261;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 258;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_690bd84dbafdd852898db42272c38b83 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_690bd84dbafdd852898db42272c38b83 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_690bd84dbafdd852898db42272c38b83, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_690bd84dbafdd852898db42272c38b83->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_690bd84dbafdd852898db42272c38b83, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_690bd84dbafdd852898db42272c38b83,
        type_description_1,
        par_event,
        var_buff,
        var_i,
        var_pos,
        var_words
    );


    // Release cached frame.
    if ( frame_690bd84dbafdd852898db42272c38b83 == cache_frame_690bd84dbafdd852898db42272c38b83 )
    {
        Py_DECREF( frame_690bd84dbafdd852898db42272c38b83 );
    }
    cache_frame_690bd84dbafdd852898db42272c38b83 = NULL;

    assertFrameObject( frame_690bd84dbafdd852898db42272c38b83 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_23_downcase_word );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_words );
    var_words = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_words );
    var_words = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_23_downcase_word );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_24_capitalize_word( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_i = NULL;
    PyObject *var_pos = NULL;
    PyObject *var_words = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_a4151aa1583707a1af87bc7dca844f41;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_a4151aa1583707a1af87bc7dca844f41 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a4151aa1583707a1af87bc7dca844f41, codeobj_a4151aa1583707a1af87bc7dca844f41, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_a4151aa1583707a1af87bc7dca844f41 = cache_frame_a4151aa1583707a1af87bc7dca844f41;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a4151aa1583707a1af87bc7dca844f41 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a4151aa1583707a1af87bc7dca844f41 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 269;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_range );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_range );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "range" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 271;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_a4151aa1583707a1af87bc7dca844f41->m_frame.f_lineno = 271;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_iter_arg_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 271;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_2;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_3 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_3 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "ooooo";
                exception_lineno = 271;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_3;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_4;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_4 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_i;
            var_i = tmp_assign_source_4;
            Py_INCREF( var_i );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 272;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        frame_a4151aa1583707a1af87bc7dca844f41->m_frame.f_lineno = 272;
        tmp_assign_source_5 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_find_next_word_ending );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 272;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_pos;
            var_pos = tmp_assign_source_5;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_subscript_name_1;
        PyObject *tmp_start_name_1;
        PyObject *tmp_stop_name_1;
        PyObject *tmp_step_name_1;
        CHECK_OBJECT( var_buff );
        tmp_source_name_5 = var_buff;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_document );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 273;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_subscribed_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_text_after_cursor );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_subscribed_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 273;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_start_name_1 = Py_None;
        CHECK_OBJECT( var_pos );
        tmp_stop_name_1 = var_pos;
        tmp_step_name_1 = Py_None;
        tmp_subscript_name_1 = MAKE_SLICEOBJ3( tmp_start_name_1, tmp_stop_name_1, tmp_step_name_1 );
        assert( !(tmp_subscript_name_1 == NULL) );
        tmp_assign_source_6 = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        Py_DECREF( tmp_subscribed_name_1 );
        Py_DECREF( tmp_subscript_name_1 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 273;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        {
            PyObject *old = var_words;
            var_words = tmp_assign_source_6;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_instance_2;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( var_buff );
        tmp_source_name_6 = var_buff;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_insert_text );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 274;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        CHECK_OBJECT( var_words );
        tmp_called_instance_2 = var_words;
        frame_a4151aa1583707a1af87bc7dca844f41->m_frame.f_lineno = 274;
        tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_title );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 274;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_3c1e954997caa37c7b6a50568aed10db );
        frame_a4151aa1583707a1af87bc7dca844f41->m_frame.f_lineno = 274;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_2, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 274;
            type_description_1 = "ooooo";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 271;
        type_description_1 = "ooooo";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a4151aa1583707a1af87bc7dca844f41 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a4151aa1583707a1af87bc7dca844f41 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a4151aa1583707a1af87bc7dca844f41, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a4151aa1583707a1af87bc7dca844f41->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a4151aa1583707a1af87bc7dca844f41, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a4151aa1583707a1af87bc7dca844f41,
        type_description_1,
        par_event,
        var_buff,
        var_i,
        var_pos,
        var_words
    );


    // Release cached frame.
    if ( frame_a4151aa1583707a1af87bc7dca844f41 == cache_frame_a4151aa1583707a1af87bc7dca844f41 )
    {
        Py_DECREF( frame_a4151aa1583707a1af87bc7dca844f41 );
    }
    cache_frame_a4151aa1583707a1af87bc7dca844f41 = NULL;

    assertFrameObject( frame_a4151aa1583707a1af87bc7dca844f41 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_24_capitalize_word );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_words );
    var_words = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_i );
    var_i = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_words );
    var_words = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_24_capitalize_word );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_25_quoted_insert( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_47c4ee38692d0bdf1d33575b039983a4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_47c4ee38692d0bdf1d33575b039983a4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_47c4ee38692d0bdf1d33575b039983a4, codeobj_47c4ee38692d0bdf1d33575b039983a4, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_47c4ee38692d0bdf1d33575b039983a4 = cache_frame_47c4ee38692d0bdf1d33575b039983a4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_47c4ee38692d0bdf1d33575b039983a4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_47c4ee38692d0bdf1d33575b039983a4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_source_name_1;
        tmp_assattr_name_1 = Py_True;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_app );
        if ( tmp_assattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 283;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_quoted_insert, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_target_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 283;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_47c4ee38692d0bdf1d33575b039983a4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_47c4ee38692d0bdf1d33575b039983a4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_47c4ee38692d0bdf1d33575b039983a4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_47c4ee38692d0bdf1d33575b039983a4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_47c4ee38692d0bdf1d33575b039983a4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_47c4ee38692d0bdf1d33575b039983a4,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_47c4ee38692d0bdf1d33575b039983a4 == cache_frame_47c4ee38692d0bdf1d33575b039983a4 )
    {
        Py_DECREF( frame_47c4ee38692d0bdf1d33575b039983a4 );
    }
    cache_frame_47c4ee38692d0bdf1d33575b039983a4 = NULL;

    assertFrameObject( frame_47c4ee38692d0bdf1d33575b039983a4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_25_quoted_insert );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_25_quoted_insert );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_26_kill_line( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_deleted = NULL;
    struct Nuitka_FrameObject *frame_29f3c5ee949bbc61155cc369d86c4fff;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_29f3c5ee949bbc61155cc369d86c4fff = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_29f3c5ee949bbc61155cc369d86c4fff, codeobj_29f3c5ee949bbc61155cc369d86c4fff, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_29f3c5ee949bbc61155cc369d86c4fff = cache_frame_29f3c5ee949bbc61155cc369d86c4fff;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_29f3c5ee949bbc61155cc369d86c4fff );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_29f3c5ee949bbc61155cc369d86c4fff ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 299;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 300;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 300;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_3;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_4;
            CHECK_OBJECT( var_buff );
            tmp_source_name_3 = var_buff;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_delete_before_cursor );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 301;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_key_1 = const_str_plain_count;
            CHECK_OBJECT( var_buff );
            tmp_source_name_4 = var_buff;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_document );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 301;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_29f3c5ee949bbc61155cc369d86c4fff->m_frame.f_lineno = 301;
            tmp_operand_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_start_of_line_position );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 301;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_1 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 301;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_29f3c5ee949bbc61155cc369d86c4fff->m_frame.f_lineno = 301;
            tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 301;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( var_deleted == NULL );
            var_deleted = tmp_assign_source_2;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_compexpr_left_2;
            PyObject *tmp_compexpr_right_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_source_name_6;
            CHECK_OBJECT( var_buff );
            tmp_source_name_6 = var_buff;
            tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_document );
            if ( tmp_source_name_5 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 303;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_current_char );
            Py_DECREF( tmp_source_name_5 );
            if ( tmp_compexpr_left_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 303;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_compexpr_right_2 = const_str_newline;
            tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
            Py_DECREF( tmp_compexpr_left_2 );
            if ( tmp_res == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 303;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_3;
                PyObject *tmp_called_instance_2;
                CHECK_OBJECT( var_buff );
                tmp_called_instance_2 = var_buff;
                frame_29f3c5ee949bbc61155cc369d86c4fff->m_frame.f_lineno = 304;
                tmp_assign_source_3 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_delete, &PyTuple_GET_ITEM( const_tuple_int_pos_1_tuple, 0 ) );

                if ( tmp_assign_source_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 304;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                assert( var_deleted == NULL );
                var_deleted = tmp_assign_source_3;
            }
            goto branch_end_2;
            branch_no_2:;
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_called_name_2;
                PyObject *tmp_source_name_7;
                PyObject *tmp_kw_name_2;
                PyObject *tmp_dict_key_2;
                PyObject *tmp_dict_value_2;
                PyObject *tmp_called_instance_3;
                PyObject *tmp_source_name_8;
                CHECK_OBJECT( var_buff );
                tmp_source_name_7 = var_buff;
                tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_delete );
                if ( tmp_called_name_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 306;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                tmp_dict_key_2 = const_str_plain_count;
                CHECK_OBJECT( var_buff );
                tmp_source_name_8 = var_buff;
                tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_document );
                if ( tmp_called_instance_3 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_2 );

                    exception_lineno = 306;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                frame_29f3c5ee949bbc61155cc369d86c4fff->m_frame.f_lineno = 306;
                tmp_dict_value_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_get_end_of_line_position );
                Py_DECREF( tmp_called_instance_3 );
                if ( tmp_dict_value_2 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                    Py_DECREF( tmp_called_name_2 );

                    exception_lineno = 306;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                tmp_kw_name_2 = _PyDict_NewPresized( 1 );
                tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
                Py_DECREF( tmp_dict_value_2 );
                assert( !(tmp_res != 0) );
                frame_29f3c5ee949bbc61155cc369d86c4fff->m_frame.f_lineno = 306;
                tmp_assign_source_4 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
                Py_DECREF( tmp_called_name_2 );
                Py_DECREF( tmp_kw_name_2 );
                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 306;
                    type_description_1 = "ooo";
                    goto frame_exception_exit_1;
                }
                assert( var_deleted == NULL );
                var_deleted = tmp_assign_source_4;
            }
            branch_end_2:;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_called_name_3;
        PyObject *tmp_source_name_9;
        PyObject *tmp_source_name_10;
        PyObject *tmp_source_name_11;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_11 = par_event;
        tmp_source_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_app );
        if ( tmp_source_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 307;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_source_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_clipboard );
        Py_DECREF( tmp_source_name_10 );
        if ( tmp_source_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 307;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_set_text );
        Py_DECREF( tmp_source_name_9 );
        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 307;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        if ( var_deleted == NULL )
        {
            Py_DECREF( tmp_called_name_3 );
            exception_type = PyExc_UnboundLocalError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "local variable '%s' referenced before assignment", "deleted" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 307;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_args_element_name_1 = var_deleted;
        frame_29f3c5ee949bbc61155cc369d86c4fff->m_frame.f_lineno = 307;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 307;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_29f3c5ee949bbc61155cc369d86c4fff );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_29f3c5ee949bbc61155cc369d86c4fff );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_29f3c5ee949bbc61155cc369d86c4fff, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_29f3c5ee949bbc61155cc369d86c4fff->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_29f3c5ee949bbc61155cc369d86c4fff, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_29f3c5ee949bbc61155cc369d86c4fff,
        type_description_1,
        par_event,
        var_buff,
        var_deleted
    );


    // Release cached frame.
    if ( frame_29f3c5ee949bbc61155cc369d86c4fff == cache_frame_29f3c5ee949bbc61155cc369d86c4fff )
    {
        Py_DECREF( frame_29f3c5ee949bbc61155cc369d86c4fff );
    }
    cache_frame_29f3c5ee949bbc61155cc369d86c4fff = NULL;

    assertFrameObject( frame_29f3c5ee949bbc61155cc369d86c4fff );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_26_kill_line );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_deleted );
    var_deleted = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_deleted );
    var_deleted = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_26_kill_line );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_27_kill_word( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_pos = NULL;
    PyObject *var_deleted = NULL;
    struct Nuitka_FrameObject *frame_8c14b78b16a71186c05c11186cae7ea7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_8c14b78b16a71186c05c11186cae7ea7 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8c14b78b16a71186c05c11186cae7ea7, codeobj_8c14b78b16a71186c05c11186cae7ea7, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_8c14b78b16a71186c05c11186cae7ea7 = cache_frame_8c14b78b16a71186c05c11186cae7ea7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8c14b78b16a71186c05c11186cae7ea7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8c14b78b16a71186c05c11186cae7ea7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 316;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_find_next_word_ending );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 317;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_8c14b78b16a71186c05c11186cae7ea7->m_frame.f_lineno = 317;
        tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 317;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_pos == NULL );
        var_pos = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_pos );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_pos );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 319;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_5;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_2;
            PyObject *tmp_dict_value_2;
            CHECK_OBJECT( var_buff );
            tmp_source_name_5 = var_buff;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_delete );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_key_2 = const_str_plain_count;
            CHECK_OBJECT( var_pos );
            tmp_dict_value_2 = var_pos;
            tmp_kw_name_2 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
            assert( !(tmp_res != 0) );
            frame_8c14b78b16a71186c05c11186cae7ea7->m_frame.f_lineno = 320;
            tmp_assign_source_3 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 320;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            assert( var_deleted == NULL );
            var_deleted = tmp_assign_source_3;
        }
        {
            nuitka_bool tmp_condition_result_2;
            PyObject *tmp_source_name_6;
            PyObject *tmp_attribute_value_1;
            int tmp_truth_name_2;
            CHECK_OBJECT( par_event );
            tmp_source_name_6 = par_event;
            tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_is_repeat );
            if ( tmp_attribute_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 322;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_1 );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_attribute_value_1 );

                exception_lineno = 322;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_2 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_attribute_value_1 );
            if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_2;
            }
            else
            {
                goto branch_no_2;
            }
            branch_yes_2:;
            {
                PyObject *tmp_assign_source_4;
                PyObject *tmp_left_name_1;
                PyObject *tmp_source_name_7;
                PyObject *tmp_called_instance_1;
                PyObject *tmp_source_name_8;
                PyObject *tmp_source_name_9;
                PyObject *tmp_right_name_1;
                CHECK_OBJECT( par_event );
                tmp_source_name_9 = par_event;
                tmp_source_name_8 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_app );
                if ( tmp_source_name_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 323;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_clipboard );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_called_instance_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 323;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                frame_8c14b78b16a71186c05c11186cae7ea7->m_frame.f_lineno = 323;
                tmp_source_name_7 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_data );
                Py_DECREF( tmp_called_instance_1 );
                if ( tmp_source_name_7 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 323;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_text );
                Py_DECREF( tmp_source_name_7 );
                if ( tmp_left_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 323;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                CHECK_OBJECT( var_deleted );
                tmp_right_name_1 = var_deleted;
                tmp_assign_source_4 = BINARY_OPERATION_ADD_OBJECT_OBJECT( tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_left_name_1 );
                if ( tmp_assign_source_4 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 323;
                    type_description_1 = "oooo";
                    goto frame_exception_exit_1;
                }
                {
                    PyObject *old = var_deleted;
                    assert( old != NULL );
                    var_deleted = tmp_assign_source_4;
                    Py_DECREF( old );
                }

            }
            branch_no_2:;
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_10;
            PyObject *tmp_source_name_11;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_event );
            tmp_source_name_11 = par_event;
            tmp_source_name_10 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_app );
            if ( tmp_source_name_10 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 325;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_clipboard );
            Py_DECREF( tmp_source_name_10 );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 325;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_deleted );
            tmp_args_element_name_1 = var_deleted;
            frame_8c14b78b16a71186c05c11186cae7ea7->m_frame.f_lineno = 325;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_text, call_args );
            }

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 325;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c14b78b16a71186c05c11186cae7ea7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c14b78b16a71186c05c11186cae7ea7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8c14b78b16a71186c05c11186cae7ea7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8c14b78b16a71186c05c11186cae7ea7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8c14b78b16a71186c05c11186cae7ea7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8c14b78b16a71186c05c11186cae7ea7,
        type_description_1,
        par_event,
        var_buff,
        var_pos,
        var_deleted
    );


    // Release cached frame.
    if ( frame_8c14b78b16a71186c05c11186cae7ea7 == cache_frame_8c14b78b16a71186c05c11186cae7ea7 )
    {
        Py_DECREF( frame_8c14b78b16a71186c05c11186cae7ea7 );
    }
    cache_frame_8c14b78b16a71186c05c11186cae7ea7 = NULL;

    assertFrameObject( frame_8c14b78b16a71186c05c11186cae7ea7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_27_kill_word );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    CHECK_OBJECT( (PyObject *)var_pos );
    Py_DECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_deleted );
    var_deleted = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_deleted );
    var_deleted = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_27_kill_word );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_28_unix_word_rubout( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *par_WORD = python_pars[ 1 ];
    PyObject *var_buff = NULL;
    PyObject *var_pos = NULL;
    PyObject *var_deleted = NULL;
    struct Nuitka_FrameObject *frame_c80d506d3405d850aceda812c41be29d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_c80d506d3405d850aceda812c41be29d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c80d506d3405d850aceda812c41be29d, codeobj_c80d506d3405d850aceda812c41be29d, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_c80d506d3405d850aceda812c41be29d = cache_frame_c80d506d3405d850aceda812c41be29d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c80d506d3405d850aceda812c41be29d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c80d506d3405d850aceda812c41be29d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 334;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 335;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_find_start_of_previous_word );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 335;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 335;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_WORD;
        CHECK_OBJECT( par_WORD );
        tmp_dict_value_2 = par_WORD;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_c80d506d3405d850aceda812c41be29d->m_frame.f_lineno = 335;
        tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 335;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_pos == NULL );
        var_pos = tmp_assign_source_2;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_pos );
        tmp_compexpr_left_1 = var_pos;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 == tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_3;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_source_name_5;
            CHECK_OBJECT( var_buff );
            tmp_source_name_5 = var_buff;
            tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_cursor_position );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 341;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_assign_source_3 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_assign_source_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 341;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            {
                PyObject *old = var_pos;
                assert( old != NULL );
                var_pos = tmp_assign_source_3;
                Py_DECREF( old );
            }

        }
        branch_no_1:;
    }
    {
        nuitka_bool tmp_condition_result_2;
        int tmp_truth_name_1;
        CHECK_OBJECT( var_pos );
        tmp_truth_name_1 = CHECK_IF_TRUE( var_pos );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 343;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_2 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_2 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_2;
        }
        else
        {
            goto branch_no_2;
        }
        branch_yes_2:;
        {
            PyObject *tmp_assign_source_4;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_6;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_3;
            PyObject *tmp_dict_value_3;
            PyObject *tmp_operand_name_2;
            CHECK_OBJECT( var_buff );
            tmp_source_name_6 = var_buff;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_delete_before_cursor );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 344;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_key_3 = const_str_plain_count;
            CHECK_OBJECT( var_pos );
            tmp_operand_name_2 = var_pos;
            tmp_dict_value_3 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_2 );
            if ( tmp_dict_value_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 344;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_2 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_3, tmp_dict_value_3 );
            Py_DECREF( tmp_dict_value_3 );
            assert( !(tmp_res != 0) );
            frame_c80d506d3405d850aceda812c41be29d->m_frame.f_lineno = 344;
            tmp_assign_source_4 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_assign_source_4 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 344;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            assert( var_deleted == NULL );
            var_deleted = tmp_assign_source_4;
        }
        {
            nuitka_bool tmp_condition_result_3;
            PyObject *tmp_source_name_7;
            PyObject *tmp_attribute_value_1;
            int tmp_truth_name_2;
            CHECK_OBJECT( par_event );
            tmp_source_name_7 = par_event;
            tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_is_repeat );
            if ( tmp_attribute_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 348;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_truth_name_2 = CHECK_IF_TRUE( tmp_attribute_value_1 );
            if ( tmp_truth_name_2 == -1 )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_attribute_value_1 );

                exception_lineno = 348;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_condition_result_3 = tmp_truth_name_2 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
            Py_DECREF( tmp_attribute_value_1 );
            if ( tmp_condition_result_3 == NUITKA_BOOL_TRUE )
            {
                goto branch_yes_3;
            }
            else
            {
                goto branch_no_3;
            }
            branch_yes_3:;
            {
                PyObject *tmp_assign_source_5;
                PyObject *tmp_left_name_1;
                PyObject *tmp_right_name_1;
                PyObject *tmp_source_name_8;
                PyObject *tmp_called_instance_1;
                PyObject *tmp_source_name_9;
                PyObject *tmp_source_name_10;
                CHECK_OBJECT( var_deleted );
                tmp_left_name_1 = var_deleted;
                CHECK_OBJECT( par_event );
                tmp_source_name_10 = par_event;
                tmp_source_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_app );
                if ( tmp_source_name_9 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 349;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_clipboard );
                Py_DECREF( tmp_source_name_9 );
                if ( tmp_called_instance_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 349;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                frame_c80d506d3405d850aceda812c41be29d->m_frame.f_lineno = 349;
                tmp_source_name_8 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_data );
                Py_DECREF( tmp_called_instance_1 );
                if ( tmp_source_name_8 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 349;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                tmp_right_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_text );
                Py_DECREF( tmp_source_name_8 );
                if ( tmp_right_name_1 == NULL )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 349;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                tmp_result = BINARY_OPERATION_ADD_OBJECT_OBJECT_INPLACE( &tmp_left_name_1, tmp_right_name_1 );
                Py_DECREF( tmp_right_name_1 );
                if ( tmp_result == false )
                {
                    assert( ERROR_OCCURRED() );

                    FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                    exception_lineno = 349;
                    type_description_1 = "ooooo";
                    goto frame_exception_exit_1;
                }
                tmp_assign_source_5 = tmp_left_name_1;
                var_deleted = tmp_assign_source_5;

            }
            branch_no_3:;
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_11;
            PyObject *tmp_source_name_12;
            PyObject *tmp_call_result_1;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_event );
            tmp_source_name_12 = par_event;
            tmp_source_name_11 = LOOKUP_ATTRIBUTE( tmp_source_name_12, const_str_plain_app );
            if ( tmp_source_name_11 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 351;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_11, const_str_plain_clipboard );
            Py_DECREF( tmp_source_name_11 );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 351;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_deleted );
            tmp_args_element_name_1 = var_deleted;
            frame_c80d506d3405d850aceda812c41be29d->m_frame.f_lineno = 351;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_text, call_args );
            }

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 351;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_2;
        branch_no_2:;
        {
            PyObject *tmp_called_instance_3;
            PyObject *tmp_source_name_13;
            PyObject *tmp_source_name_14;
            PyObject *tmp_call_result_2;
            CHECK_OBJECT( par_event );
            tmp_source_name_14 = par_event;
            tmp_source_name_13 = LOOKUP_ATTRIBUTE( tmp_source_name_14, const_str_plain_app );
            if ( tmp_source_name_13 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_13, const_str_plain_output );
            Py_DECREF( tmp_source_name_13 );
            if ( tmp_called_instance_3 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            frame_c80d506d3405d850aceda812c41be29d->m_frame.f_lineno = 354;
            tmp_call_result_2 = CALL_METHOD_NO_ARGS( tmp_called_instance_3, const_str_plain_bell );
            Py_DECREF( tmp_called_instance_3 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 354;
                type_description_1 = "ooooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_end_2:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c80d506d3405d850aceda812c41be29d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c80d506d3405d850aceda812c41be29d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c80d506d3405d850aceda812c41be29d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c80d506d3405d850aceda812c41be29d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c80d506d3405d850aceda812c41be29d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c80d506d3405d850aceda812c41be29d,
        type_description_1,
        par_event,
        par_WORD,
        var_buff,
        var_pos,
        var_deleted
    );


    // Release cached frame.
    if ( frame_c80d506d3405d850aceda812c41be29d == cache_frame_c80d506d3405d850aceda812c41be29d )
    {
        Py_DECREF( frame_c80d506d3405d850aceda812c41be29d );
    }
    cache_frame_c80d506d3405d850aceda812c41be29d = NULL;

    assertFrameObject( frame_c80d506d3405d850aceda812c41be29d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_28_unix_word_rubout );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)par_WORD );
    Py_DECREF( par_WORD );
    par_WORD = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    CHECK_OBJECT( (PyObject *)var_pos );
    Py_DECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_deleted );
    var_deleted = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)par_WORD );
    Py_DECREF( par_WORD );
    par_WORD = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_pos );
    var_pos = NULL;

    Py_XDECREF( var_deleted );
    var_deleted = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_28_unix_word_rubout );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_29_backward_kill_word( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5cef15a627204a6a34d9977b1d03f826;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5cef15a627204a6a34d9977b1d03f826 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5cef15a627204a6a34d9977b1d03f826, codeobj_5cef15a627204a6a34d9977b1d03f826, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_5cef15a627204a6a34d9977b1d03f826 = cache_frame_5cef15a627204a6a34d9977b1d03f826;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5cef15a627204a6a34d9977b1d03f826 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5cef15a627204a6a34d9977b1d03f826 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_kw_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_unix_word_rubout );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_unix_word_rubout );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "unix_word_rubout" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 363;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_event );
        tmp_tuple_element_1 = par_event;
        tmp_args_name_1 = PyTuple_New( 1 );
        Py_INCREF( tmp_tuple_element_1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_c6fe41077d8176687979ef8499963adc );
        frame_5cef15a627204a6a34d9977b1d03f826->m_frame.f_lineno = 363;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 363;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5cef15a627204a6a34d9977b1d03f826 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5cef15a627204a6a34d9977b1d03f826 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5cef15a627204a6a34d9977b1d03f826, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5cef15a627204a6a34d9977b1d03f826->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5cef15a627204a6a34d9977b1d03f826, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5cef15a627204a6a34d9977b1d03f826,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_5cef15a627204a6a34d9977b1d03f826 == cache_frame_5cef15a627204a6a34d9977b1d03f826 )
    {
        Py_DECREF( frame_5cef15a627204a6a34d9977b1d03f826 );
    }
    cache_frame_5cef15a627204a6a34d9977b1d03f826 = NULL;

    assertFrameObject( frame_5cef15a627204a6a34d9977b1d03f826 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_29_backward_kill_word );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_29_backward_kill_word );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_30_delete_horizontal_space( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_text_before_cursor = NULL;
    PyObject *var_text_after_cursor = NULL;
    PyObject *var_delete_before = NULL;
    PyObject *var_delete_after = NULL;
    struct Nuitka_FrameObject *frame_63a9d9d241c96d8c5750972866445eb4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_63a9d9d241c96d8c5750972866445eb4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_63a9d9d241c96d8c5750972866445eb4, codeobj_63a9d9d241c96d8c5750972866445eb4, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_63a9d9d241c96d8c5750972866445eb4 = cache_frame_63a9d9d241c96d8c5750972866445eb4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_63a9d9d241c96d8c5750972866445eb4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_63a9d9d241c96d8c5750972866445eb4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 369;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 370;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_text_before_cursor );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 370;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_text_before_cursor == NULL );
        var_text_before_cursor = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( var_buff );
        tmp_source_name_5 = var_buff;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_document );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 371;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_text_after_cursor );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 371;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        assert( var_text_after_cursor == NULL );
        var_text_after_cursor = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_left_name_1;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_right_name_1;
        PyObject *tmp_len_arg_2;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_text_before_cursor );
        tmp_len_arg_1 = var_text_before_cursor;
        tmp_left_name_1 = BUILTIN_LEN( tmp_len_arg_1 );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 373;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_text_before_cursor );
        tmp_called_instance_1 = var_text_before_cursor;
        frame_63a9d9d241c96d8c5750972866445eb4->m_frame.f_lineno = 373;
        tmp_len_arg_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_rstrip, &PyTuple_GET_ITEM( const_tuple_str_digest_bb2edfc35430ba0550dc023a5379a967_tuple, 0 ) );

        if ( tmp_len_arg_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 373;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = BUILTIN_LEN( tmp_len_arg_2 );
        Py_DECREF( tmp_len_arg_2 );
        if ( tmp_right_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_1 );

            exception_lineno = 373;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_4 = BINARY_OPERATION_SUB_LONG_LONG( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        Py_DECREF( tmp_right_name_1 );
        assert( !(tmp_assign_source_4 == NULL) );
        assert( var_delete_before == NULL );
        var_delete_before = tmp_assign_source_4;
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_left_name_2;
        PyObject *tmp_len_arg_3;
        PyObject *tmp_right_name_2;
        PyObject *tmp_len_arg_4;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_text_after_cursor );
        tmp_len_arg_3 = var_text_after_cursor;
        tmp_left_name_2 = BUILTIN_LEN( tmp_len_arg_3 );
        if ( tmp_left_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 374;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_text_after_cursor );
        tmp_called_instance_2 = var_text_after_cursor;
        frame_63a9d9d241c96d8c5750972866445eb4->m_frame.f_lineno = 374;
        tmp_len_arg_4 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_lstrip, &PyTuple_GET_ITEM( const_tuple_str_digest_bb2edfc35430ba0550dc023a5379a967_tuple, 0 ) );

        if ( tmp_len_arg_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_2 );

            exception_lineno = 374;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_2 = BUILTIN_LEN( tmp_len_arg_4 );
        Py_DECREF( tmp_len_arg_4 );
        if ( tmp_right_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_left_name_2 );

            exception_lineno = 374;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = BINARY_OPERATION_SUB_LONG_LONG( tmp_left_name_2, tmp_right_name_2 );
        Py_DECREF( tmp_left_name_2 );
        Py_DECREF( tmp_right_name_2 );
        assert( !(tmp_assign_source_5 == NULL) );
        assert( var_delete_after == NULL );
        var_delete_after = tmp_assign_source_5;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_6;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        CHECK_OBJECT( var_buff );
        tmp_source_name_6 = var_buff;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_delete_before_cursor );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 376;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( var_delete_before );
        tmp_dict_value_1 = var_delete_before;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_63a9d9d241c96d8c5750972866445eb4->m_frame.f_lineno = 376;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 376;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_7;
        PyObject *tmp_call_result_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        CHECK_OBJECT( var_buff );
        tmp_source_name_7 = var_buff;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_delete );
        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 377;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_2 = const_str_plain_count;
        CHECK_OBJECT( var_delete_after );
        tmp_dict_value_2 = var_delete_after;
        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_63a9d9d241c96d8c5750972866445eb4->m_frame.f_lineno = 377;
        tmp_call_result_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 377;
            type_description_1 = "oooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_63a9d9d241c96d8c5750972866445eb4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_63a9d9d241c96d8c5750972866445eb4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_63a9d9d241c96d8c5750972866445eb4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_63a9d9d241c96d8c5750972866445eb4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_63a9d9d241c96d8c5750972866445eb4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_63a9d9d241c96d8c5750972866445eb4,
        type_description_1,
        par_event,
        var_buff,
        var_text_before_cursor,
        var_text_after_cursor,
        var_delete_before,
        var_delete_after
    );


    // Release cached frame.
    if ( frame_63a9d9d241c96d8c5750972866445eb4 == cache_frame_63a9d9d241c96d8c5750972866445eb4 )
    {
        Py_DECREF( frame_63a9d9d241c96d8c5750972866445eb4 );
    }
    cache_frame_63a9d9d241c96d8c5750972866445eb4 = NULL;

    assertFrameObject( frame_63a9d9d241c96d8c5750972866445eb4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_30_delete_horizontal_space );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    CHECK_OBJECT( (PyObject *)var_text_before_cursor );
    Py_DECREF( var_text_before_cursor );
    var_text_before_cursor = NULL;

    CHECK_OBJECT( (PyObject *)var_text_after_cursor );
    Py_DECREF( var_text_after_cursor );
    var_text_after_cursor = NULL;

    CHECK_OBJECT( (PyObject *)var_delete_before );
    Py_DECREF( var_delete_before );
    var_delete_before = NULL;

    CHECK_OBJECT( (PyObject *)var_delete_after );
    Py_DECREF( var_delete_after );
    var_delete_after = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_text_before_cursor );
    var_text_before_cursor = NULL;

    Py_XDECREF( var_text_after_cursor );
    var_text_after_cursor = NULL;

    Py_XDECREF( var_delete_before );
    var_delete_before = NULL;

    Py_XDECREF( var_delete_after );
    var_delete_after = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_30_delete_horizontal_space );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_31_unix_line_discard( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_deleted = NULL;
    struct Nuitka_FrameObject *frame_acba45f633543b0c0ce4809e8825e34d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_acba45f633543b0c0ce4809e8825e34d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_acba45f633543b0c0ce4809e8825e34d, codeobj_acba45f633543b0c0ce4809e8825e34d, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_acba45f633543b0c0ce4809e8825e34d = cache_frame_acba45f633543b0c0ce4809e8825e34d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_acba45f633543b0c0ce4809e8825e34d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_acba45f633543b0c0ce4809e8825e34d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 385;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        int tmp_and_left_truth_1;
        nuitka_bool tmp_and_left_value_1;
        nuitka_bool tmp_and_right_value_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_compexpr_left_2;
        PyObject *tmp_compexpr_right_2;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        CHECK_OBJECT( var_buff );
        tmp_source_name_3 = var_buff;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_document );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_cursor_position_col );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_EQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_and_left_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_and_left_truth_1 = tmp_and_left_value_1 == NUITKA_BOOL_TRUE ? 1 : 0;
        if ( tmp_and_left_truth_1 == 1 )
        {
            goto and_right_1;
        }
        else
        {
            goto and_left_1;
        }
        and_right_1:;
        CHECK_OBJECT( var_buff );
        tmp_source_name_5 = var_buff;
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_document );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_left_2 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_cursor_position );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_compexpr_left_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_2 = const_int_0;
        tmp_res = RICH_COMPARE_BOOL_GT_OBJECT_OBJECT( tmp_compexpr_left_2, tmp_compexpr_right_2 );
        Py_DECREF( tmp_compexpr_left_2 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 387;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_and_right_value_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        tmp_condition_result_1 = tmp_and_right_value_1;
        goto and_end_1;
        and_left_1:;
        tmp_condition_result_1 = tmp_and_left_value_1;
        and_end_1:;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_6;
            PyObject *tmp_call_result_1;
            PyObject *tmp_kw_name_1;
            CHECK_OBJECT( var_buff );
            tmp_source_name_6 = var_buff;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_delete_before_cursor );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 388;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = PyDict_Copy( const_dict_d96fc9db79024853de9fbab06f7c41d9 );
            frame_acba45f633543b0c0ce4809e8825e34d->m_frame.f_lineno = 388;
            tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 388;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_2;
            PyObject *tmp_called_name_2;
            PyObject *tmp_source_name_7;
            PyObject *tmp_kw_name_2;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_operand_name_1;
            PyObject *tmp_called_instance_1;
            PyObject *tmp_source_name_8;
            CHECK_OBJECT( var_buff );
            tmp_source_name_7 = var_buff;
            tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_7, const_str_plain_delete_before_cursor );
            if ( tmp_called_name_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 390;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_key_1 = const_str_plain_count;
            CHECK_OBJECT( var_buff );
            tmp_source_name_8 = var_buff;
            tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_8, const_str_plain_document );
            if ( tmp_called_instance_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 390;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            frame_acba45f633543b0c0ce4809e8825e34d->m_frame.f_lineno = 390;
            tmp_operand_name_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_start_of_line_position );
            Py_DECREF( tmp_called_instance_1 );
            if ( tmp_operand_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 390;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_dict_value_1 = UNARY_OPERATION( PyNumber_Negative, tmp_operand_name_1 );
            Py_DECREF( tmp_operand_name_1 );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_2 );

                exception_lineno = 390;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_2 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_acba45f633543b0c0ce4809e8825e34d->m_frame.f_lineno = 390;
            tmp_assign_source_2 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_2, tmp_kw_name_2 );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_kw_name_2 );
            if ( tmp_assign_source_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 390;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            assert( var_deleted == NULL );
            var_deleted = tmp_assign_source_2;
        }
        {
            PyObject *tmp_called_instance_2;
            PyObject *tmp_source_name_9;
            PyObject *tmp_source_name_10;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_element_name_1;
            CHECK_OBJECT( par_event );
            tmp_source_name_10 = par_event;
            tmp_source_name_9 = LOOKUP_ATTRIBUTE( tmp_source_name_10, const_str_plain_app );
            if ( tmp_source_name_9 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 391;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_9, const_str_plain_clipboard );
            Py_DECREF( tmp_source_name_9 );
            if ( tmp_called_instance_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 391;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_deleted );
            tmp_args_element_name_1 = var_deleted;
            frame_acba45f633543b0c0ce4809e8825e34d->m_frame.f_lineno = 391;
            {
                PyObject *call_args[] = { tmp_args_element_name_1 };
                tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_text, call_args );
            }

            Py_DECREF( tmp_called_instance_2 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 391;
                type_description_1 = "ooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_end_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_acba45f633543b0c0ce4809e8825e34d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_acba45f633543b0c0ce4809e8825e34d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_acba45f633543b0c0ce4809e8825e34d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_acba45f633543b0c0ce4809e8825e34d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_acba45f633543b0c0ce4809e8825e34d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_acba45f633543b0c0ce4809e8825e34d,
        type_description_1,
        par_event,
        var_buff,
        var_deleted
    );


    // Release cached frame.
    if ( frame_acba45f633543b0c0ce4809e8825e34d == cache_frame_acba45f633543b0c0ce4809e8825e34d )
    {
        Py_DECREF( frame_acba45f633543b0c0ce4809e8825e34d );
    }
    cache_frame_acba45f633543b0c0ce4809e8825e34d = NULL;

    assertFrameObject( frame_acba45f633543b0c0ce4809e8825e34d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_31_unix_line_discard );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_deleted );
    var_deleted = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_deleted );
    var_deleted = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_31_unix_line_discard );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_32_yank( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_3419c02f5e676ebb31c78b9f702e00ea;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_3419c02f5e676ebb31c78b9f702e00ea = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_3419c02f5e676ebb31c78b9f702e00ea, codeobj_3419c02f5e676ebb31c78b9f702e00ea, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_3419c02f5e676ebb31c78b9f702e00ea = cache_frame_3419c02f5e676ebb31c78b9f702e00ea;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_3419c02f5e676ebb31c78b9f702e00ea );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_3419c02f5e676ebb31c78b9f702e00ea ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_5;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_source_name_6;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 399;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_paste_clipboard_data );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 399;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_app );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 400;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_clipboard );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 400;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_3419c02f5e676ebb31c78b9f702e00ea->m_frame.f_lineno = 400;
        tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_get_data );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 400;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_5 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );

            exception_lineno = 400;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_paste_mode;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_PasteMode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PasteMode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PasteMode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 400;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_6 = tmp_mvar_value_1;
        tmp_dict_value_2 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_EMACS );
        if ( tmp_dict_value_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );

            exception_lineno = 400;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        Py_DECREF( tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_3419c02f5e676ebb31c78b9f702e00ea->m_frame.f_lineno = 399;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 399;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3419c02f5e676ebb31c78b9f702e00ea );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_3419c02f5e676ebb31c78b9f702e00ea );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_3419c02f5e676ebb31c78b9f702e00ea, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_3419c02f5e676ebb31c78b9f702e00ea->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_3419c02f5e676ebb31c78b9f702e00ea, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_3419c02f5e676ebb31c78b9f702e00ea,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_3419c02f5e676ebb31c78b9f702e00ea == cache_frame_3419c02f5e676ebb31c78b9f702e00ea )
    {
        Py_DECREF( frame_3419c02f5e676ebb31c78b9f702e00ea );
    }
    cache_frame_3419c02f5e676ebb31c78b9f702e00ea = NULL;

    assertFrameObject( frame_3419c02f5e676ebb31c78b9f702e00ea );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_32_yank );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_32_yank );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_33_yank_nth_arg( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_n = NULL;
    struct Nuitka_FrameObject *frame_51f16b1961475f8370f81cc4016a2a59;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_51f16b1961475f8370f81cc4016a2a59 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_51f16b1961475f8370f81cc4016a2a59, codeobj_51f16b1961475f8370f81cc4016a2a59, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_51f16b1961475f8370f81cc4016a2a59 = cache_frame_51f16b1961475f8370f81cc4016a2a59;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_51f16b1961475f8370f81cc4016a2a59 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_51f16b1961475f8370f81cc4016a2a59 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_arg_present );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 409;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 409;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 409;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_assign_source_1 = Py_None;
        Py_INCREF( tmp_assign_source_1 );
        condexpr_end_1:;
        assert( var_n == NULL );
        var_n = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 410;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_n );
        tmp_args_element_name_1 = var_n;
        frame_51f16b1961475f8370f81cc4016a2a59->m_frame.f_lineno = 410;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_yank_nth_arg, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 410;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_51f16b1961475f8370f81cc4016a2a59 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_51f16b1961475f8370f81cc4016a2a59 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_51f16b1961475f8370f81cc4016a2a59, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_51f16b1961475f8370f81cc4016a2a59->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_51f16b1961475f8370f81cc4016a2a59, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_51f16b1961475f8370f81cc4016a2a59,
        type_description_1,
        par_event,
        var_n
    );


    // Release cached frame.
    if ( frame_51f16b1961475f8370f81cc4016a2a59 == cache_frame_51f16b1961475f8370f81cc4016a2a59 )
    {
        Py_DECREF( frame_51f16b1961475f8370f81cc4016a2a59 );
    }
    cache_frame_51f16b1961475f8370f81cc4016a2a59 = NULL;

    assertFrameObject( frame_51f16b1961475f8370f81cc4016a2a59 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_33_yank_nth_arg );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_n );
    Py_DECREF( var_n );
    var_n = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_33_yank_nth_arg );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_34_yank_last_arg( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_n = NULL;
    struct Nuitka_FrameObject *frame_c8eead45aa3dc99f95a05d8ccc2a5ff5;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c8eead45aa3dc99f95a05d8ccc2a5ff5, codeobj_c8eead45aa3dc99f95a05d8ccc2a5ff5, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 = cache_frame_c8eead45aa3dc99f95a05d8ccc2a5ff5;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_attribute_value_1;
        int tmp_truth_name_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_attribute_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_arg_present );
        if ( tmp_attribute_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 419;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_attribute_value_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_attribute_value_1 );

            exception_lineno = 419;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_attribute_value_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 419;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        tmp_assign_source_1 = Py_None;
        Py_INCREF( tmp_assign_source_1 );
        condexpr_end_1:;
        assert( var_n == NULL );
        var_n = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 420;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_n );
        tmp_args_element_name_1 = var_n;
        frame_c8eead45aa3dc99f95a05d8ccc2a5ff5->m_frame.f_lineno = 420;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_yank_last_arg, call_args );
        }

        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 420;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c8eead45aa3dc99f95a05d8ccc2a5ff5, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c8eead45aa3dc99f95a05d8ccc2a5ff5->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c8eead45aa3dc99f95a05d8ccc2a5ff5, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c8eead45aa3dc99f95a05d8ccc2a5ff5,
        type_description_1,
        par_event,
        var_n
    );


    // Release cached frame.
    if ( frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 == cache_frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 )
    {
        Py_DECREF( frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 );
    }
    cache_frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 = NULL;

    assertFrameObject( frame_c8eead45aa3dc99f95a05d8ccc2a5ff5 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_34_yank_last_arg );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_n );
    Py_DECREF( var_n );
    var_n = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_n );
    var_n = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_34_yank_last_arg );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_35_yank_pop( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_doc_before_paste = NULL;
    PyObject *var_clipboard = NULL;
    struct Nuitka_FrameObject *frame_9a5d0dfb29ab447ff46ddd48a0d06117;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_9a5d0dfb29ab447ff46ddd48a0d06117 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9a5d0dfb29ab447ff46ddd48a0d06117, codeobj_9a5d0dfb29ab447ff46ddd48a0d06117, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9a5d0dfb29ab447ff46ddd48a0d06117 = cache_frame_9a5d0dfb29ab447ff46ddd48a0d06117;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9a5d0dfb29ab447ff46ddd48a0d06117 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9a5d0dfb29ab447ff46ddd48a0d06117 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 429;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( var_buff );
        tmp_source_name_2 = var_buff;
        tmp_assign_source_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_document_before_paste );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 430;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_doc_before_paste == NULL );
        var_doc_before_paste = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_app );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 431;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_clipboard );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 431;
            type_description_1 = "oooo";
            goto frame_exception_exit_1;
        }
        assert( var_clipboard == NULL );
        var_clipboard = tmp_assign_source_3;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        CHECK_OBJECT( var_doc_before_paste );
        tmp_compexpr_left_1 = var_doc_before_paste;
        tmp_compexpr_right_1 = Py_None;
        tmp_condition_result_1 = ( tmp_compexpr_left_1 != tmp_compexpr_right_1 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            CHECK_OBJECT( var_doc_before_paste );
            tmp_assattr_name_1 = var_doc_before_paste;
            CHECK_OBJECT( var_buff );
            tmp_assattr_target_1 = var_buff;
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_document, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 434;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
        }
        {
            PyObject *tmp_called_instance_1;
            PyObject *tmp_call_result_1;
            CHECK_OBJECT( var_clipboard );
            tmp_called_instance_1 = var_clipboard;
            frame_9a5d0dfb29ab447ff46ddd48a0d06117->m_frame.f_lineno = 435;
            tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_rotate );
            if ( tmp_call_result_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 435;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_1 );
        }
        {
            PyObject *tmp_called_name_1;
            PyObject *tmp_source_name_5;
            PyObject *tmp_call_result_2;
            PyObject *tmp_args_name_1;
            PyObject *tmp_tuple_element_1;
            PyObject *tmp_called_instance_2;
            PyObject *tmp_kw_name_1;
            PyObject *tmp_dict_key_1;
            PyObject *tmp_dict_value_1;
            PyObject *tmp_source_name_6;
            PyObject *tmp_mvar_value_1;
            CHECK_OBJECT( var_buff );
            tmp_source_name_5 = var_buff;
            tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_paste_clipboard_data );
            if ( tmp_called_name_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 436;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            CHECK_OBJECT( var_clipboard );
            tmp_called_instance_2 = var_clipboard;
            frame_9a5d0dfb29ab447ff46ddd48a0d06117->m_frame.f_lineno = 437;
            tmp_tuple_element_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_get_data );
            if ( tmp_tuple_element_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );

                exception_lineno = 437;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_args_name_1 = PyTuple_New( 1 );
            PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
            tmp_dict_key_1 = const_str_plain_paste_mode;
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_PasteMode );

            if (unlikely( tmp_mvar_value_1 == NULL ))
            {
                tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_PasteMode );
            }

            if ( tmp_mvar_value_1 == NULL )
            {
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_name_1 );
                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "PasteMode" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 437;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }

            tmp_source_name_6 = tmp_mvar_value_1;
            tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_EMACS );
            if ( tmp_dict_value_1 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                Py_DECREF( tmp_called_name_1 );
                Py_DECREF( tmp_args_name_1 );

                exception_lineno = 437;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            tmp_kw_name_1 = _PyDict_NewPresized( 1 );
            tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
            Py_DECREF( tmp_dict_value_1 );
            assert( !(tmp_res != 0) );
            frame_9a5d0dfb29ab447ff46ddd48a0d06117->m_frame.f_lineno = 436;
            tmp_call_result_2 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
            Py_DECREF( tmp_called_name_1 );
            Py_DECREF( tmp_args_name_1 );
            Py_DECREF( tmp_kw_name_1 );
            if ( tmp_call_result_2 == NULL )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 436;
                type_description_1 = "oooo";
                goto frame_exception_exit_1;
            }
            Py_DECREF( tmp_call_result_2 );
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a5d0dfb29ab447ff46ddd48a0d06117 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9a5d0dfb29ab447ff46ddd48a0d06117 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9a5d0dfb29ab447ff46ddd48a0d06117, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9a5d0dfb29ab447ff46ddd48a0d06117->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9a5d0dfb29ab447ff46ddd48a0d06117, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9a5d0dfb29ab447ff46ddd48a0d06117,
        type_description_1,
        par_event,
        var_buff,
        var_doc_before_paste,
        var_clipboard
    );


    // Release cached frame.
    if ( frame_9a5d0dfb29ab447ff46ddd48a0d06117 == cache_frame_9a5d0dfb29ab447ff46ddd48a0d06117 )
    {
        Py_DECREF( frame_9a5d0dfb29ab447ff46ddd48a0d06117 );
    }
    cache_frame_9a5d0dfb29ab447ff46ddd48a0d06117 = NULL;

    assertFrameObject( frame_9a5d0dfb29ab447ff46ddd48a0d06117 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_35_yank_pop );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    CHECK_OBJECT( (PyObject *)var_doc_before_paste );
    Py_DECREF( var_doc_before_paste );
    var_doc_before_paste = NULL;

    CHECK_OBJECT( (PyObject *)var_clipboard );
    Py_DECREF( var_clipboard );
    var_clipboard = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_doc_before_paste );
    var_doc_before_paste = NULL;

    Py_XDECREF( var_clipboard );
    var_clipboard = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_35_yank_pop );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_36_complete( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_d1ae5db46ddec2f0d84cc2f7753005f4;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_d1ae5db46ddec2f0d84cc2f7753005f4 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_d1ae5db46ddec2f0d84cc2f7753005f4, codeobj_d1ae5db46ddec2f0d84cc2f7753005f4, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_d1ae5db46ddec2f0d84cc2f7753005f4 = cache_frame_d1ae5db46ddec2f0d84cc2f7753005f4;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_d1ae5db46ddec2f0d84cc2f7753005f4 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_d1ae5db46ddec2f0d84cc2f7753005f4 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_display_completions_like_readline );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_display_completions_like_readline );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "display_completions_like_readline" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 447;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_event );
        tmp_args_element_name_1 = par_event;
        frame_d1ae5db46ddec2f0d84cc2f7753005f4->m_frame.f_lineno = 447;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 447;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d1ae5db46ddec2f0d84cc2f7753005f4 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_d1ae5db46ddec2f0d84cc2f7753005f4 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_d1ae5db46ddec2f0d84cc2f7753005f4, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_d1ae5db46ddec2f0d84cc2f7753005f4->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_d1ae5db46ddec2f0d84cc2f7753005f4, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_d1ae5db46ddec2f0d84cc2f7753005f4,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_d1ae5db46ddec2f0d84cc2f7753005f4 == cache_frame_d1ae5db46ddec2f0d84cc2f7753005f4 )
    {
        Py_DECREF( frame_d1ae5db46ddec2f0d84cc2f7753005f4 );
    }
    cache_frame_d1ae5db46ddec2f0d84cc2f7753005f4 = NULL;

    assertFrameObject( frame_d1ae5db46ddec2f0d84cc2f7753005f4 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_36_complete );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_36_complete );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_37_menu_complete( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_9b1fe3eab7b189a9f30b3fed0cdb3b00;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9b1fe3eab7b189a9f30b3fed0cdb3b00, codeobj_9b1fe3eab7b189a9f30b3fed0cdb3b00, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 = cache_frame_9b1fe3eab7b189a9f30b3fed0cdb3b00;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_generate_completions );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_generate_completions );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "generate_completions" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 456;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        CHECK_OBJECT( par_event );
        tmp_args_element_name_1 = par_event;
        frame_9b1fe3eab7b189a9f30b3fed0cdb3b00->m_frame.f_lineno = 456;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 456;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9b1fe3eab7b189a9f30b3fed0cdb3b00, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9b1fe3eab7b189a9f30b3fed0cdb3b00->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9b1fe3eab7b189a9f30b3fed0cdb3b00, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9b1fe3eab7b189a9f30b3fed0cdb3b00,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 == cache_frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 )
    {
        Py_DECREF( frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 );
    }
    cache_frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 = NULL;

    assertFrameObject( frame_9b1fe3eab7b189a9f30b3fed0cdb3b00 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_37_menu_complete );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_37_menu_complete );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_38_menu_complete_backward( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_c5f30d095ec1aff287f6e262473fd2c1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c5f30d095ec1aff287f6e262473fd2c1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_c5f30d095ec1aff287f6e262473fd2c1, codeobj_c5f30d095ec1aff287f6e262473fd2c1, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_c5f30d095ec1aff287f6e262473fd2c1 = cache_frame_c5f30d095ec1aff287f6e262473fd2c1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c5f30d095ec1aff287f6e262473fd2c1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c5f30d095ec1aff287f6e262473fd2c1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 462;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_c5f30d095ec1aff287f6e262473fd2c1->m_frame.f_lineno = 462;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_complete_previous );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 462;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c5f30d095ec1aff287f6e262473fd2c1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c5f30d095ec1aff287f6e262473fd2c1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c5f30d095ec1aff287f6e262473fd2c1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c5f30d095ec1aff287f6e262473fd2c1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c5f30d095ec1aff287f6e262473fd2c1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c5f30d095ec1aff287f6e262473fd2c1,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_c5f30d095ec1aff287f6e262473fd2c1 == cache_frame_c5f30d095ec1aff287f6e262473fd2c1 )
    {
        Py_DECREF( frame_c5f30d095ec1aff287f6e262473fd2c1 );
    }
    cache_frame_c5f30d095ec1aff287f6e262473fd2c1 = NULL;

    assertFrameObject( frame_c5f30d095ec1aff287f6e262473fd2c1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_38_menu_complete_backward );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_38_menu_complete_backward );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_39_start_kbd_macro( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_6750b758ae89dba6953c9afd97461adb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_6750b758ae89dba6953c9afd97461adb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_6750b758ae89dba6953c9afd97461adb, codeobj_6750b758ae89dba6953c9afd97461adb, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_6750b758ae89dba6953c9afd97461adb = cache_frame_6750b758ae89dba6953c9afd97461adb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_6750b758ae89dba6953c9afd97461adb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_6750b758ae89dba6953c9afd97461adb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_app );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 474;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_emacs_state );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 474;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_6750b758ae89dba6953c9afd97461adb->m_frame.f_lineno = 474;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_start_macro );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 474;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6750b758ae89dba6953c9afd97461adb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_6750b758ae89dba6953c9afd97461adb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_6750b758ae89dba6953c9afd97461adb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_6750b758ae89dba6953c9afd97461adb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_6750b758ae89dba6953c9afd97461adb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_6750b758ae89dba6953c9afd97461adb,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_6750b758ae89dba6953c9afd97461adb == cache_frame_6750b758ae89dba6953c9afd97461adb )
    {
        Py_DECREF( frame_6750b758ae89dba6953c9afd97461adb );
    }
    cache_frame_6750b758ae89dba6953c9afd97461adb = NULL;

    assertFrameObject( frame_6750b758ae89dba6953c9afd97461adb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_39_start_kbd_macro );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_39_start_kbd_macro );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_40_end_kbd_macro( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5bdc03f3d4c97002f3d3b93d45e2810a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5bdc03f3d4c97002f3d3b93d45e2810a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5bdc03f3d4c97002f3d3b93d45e2810a, codeobj_5bdc03f3d4c97002f3d3b93d45e2810a, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_5bdc03f3d4c97002f3d3b93d45e2810a = cache_frame_5bdc03f3d4c97002f3d3b93d45e2810a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5bdc03f3d4c97002f3d3b93d45e2810a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5bdc03f3d4c97002f3d3b93d45e2810a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_app );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 483;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_emacs_state );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 483;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_5bdc03f3d4c97002f3d3b93d45e2810a->m_frame.f_lineno = 483;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_end_macro );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 483;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5bdc03f3d4c97002f3d3b93d45e2810a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5bdc03f3d4c97002f3d3b93d45e2810a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5bdc03f3d4c97002f3d3b93d45e2810a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5bdc03f3d4c97002f3d3b93d45e2810a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5bdc03f3d4c97002f3d3b93d45e2810a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5bdc03f3d4c97002f3d3b93d45e2810a,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_5bdc03f3d4c97002f3d3b93d45e2810a == cache_frame_5bdc03f3d4c97002f3d3b93d45e2810a )
    {
        Py_DECREF( frame_5bdc03f3d4c97002f3d3b93d45e2810a );
    }
    cache_frame_5bdc03f3d4c97002f3d3b93d45e2810a = NULL;

    assertFrameObject( frame_5bdc03f3d4c97002f3d3b93d45e2810a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_40_end_kbd_macro );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_40_end_kbd_macro );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_41_call_last_kbd_macro( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_efa8e942d162049ae5d2899df9e1088c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_efa8e942d162049ae5d2899df9e1088c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_efa8e942d162049ae5d2899df9e1088c, codeobj_efa8e942d162049ae5d2899df9e1088c, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_efa8e942d162049ae5d2899df9e1088c = cache_frame_efa8e942d162049ae5d2899df9e1088c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_efa8e942d162049ae5d2899df9e1088c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_efa8e942d162049ae5d2899df9e1088c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_source_name_5;
        PyObject *tmp_source_name_6;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 499;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_key_processor );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 499;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_feed_multiple );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 499;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_6 = par_event;
        tmp_source_name_5 = LOOKUP_ATTRIBUTE( tmp_source_name_6, const_str_plain_app );
        if ( tmp_source_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 500;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_source_name_4 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_emacs_state );
        Py_DECREF( tmp_source_name_5 );
        if ( tmp_source_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 500;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_macro );
        Py_DECREF( tmp_source_name_4 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 500;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_e2d16c98e7cb03ba7fd208a07fd5d185 );
        frame_efa8e942d162049ae5d2899df9e1088c->m_frame.f_lineno = 499;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 499;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_efa8e942d162049ae5d2899df9e1088c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_efa8e942d162049ae5d2899df9e1088c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_efa8e942d162049ae5d2899df9e1088c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_efa8e942d162049ae5d2899df9e1088c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_efa8e942d162049ae5d2899df9e1088c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_efa8e942d162049ae5d2899df9e1088c,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_efa8e942d162049ae5d2899df9e1088c == cache_frame_efa8e942d162049ae5d2899df9e1088c )
    {
        Py_DECREF( frame_efa8e942d162049ae5d2899df9e1088c );
    }
    cache_frame_efa8e942d162049ae5d2899df9e1088c = NULL;

    assertFrameObject( frame_efa8e942d162049ae5d2899df9e1088c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_41_call_last_kbd_macro );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_41_call_last_kbd_macro );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_CellObject *par_event = PyCell_NEW1( python_pars[ 0 ] );
    PyObject *var_print_macro = NULL;
    PyObject *var_run_in_terminal = NULL;
    struct Nuitka_FrameObject *frame_2e66240a37b34bb60a8a6a94c8cf2c17;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_2e66240a37b34bb60a8a6a94c8cf2c17 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro$$$function_1_print_macro(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] = par_event;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_1)->m_closure[0] );


        assert( var_print_macro == NULL );
        var_print_macro = tmp_assign_source_1;
    }
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_2e66240a37b34bb60a8a6a94c8cf2c17, codeobj_2e66240a37b34bb60a8a6a94c8cf2c17, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_2e66240a37b34bb60a8a6a94c8cf2c17 = cache_frame_2e66240a37b34bb60a8a6a94c8cf2c17;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_2e66240a37b34bb60a8a6a94c8cf2c17 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_2e66240a37b34bb60a8a6a94c8cf2c17 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_import_name_from_1;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_ebac526994d665b2afb9731ed9b7a23c;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_run_in_terminal_tuple;
        tmp_level_name_1 = const_int_0;
        frame_2e66240a37b34bb60a8a6a94c8cf2c17->m_frame.f_lineno = 511;
        tmp_import_name_from_1 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 511;
            type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_2 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_run_in_terminal );
        Py_DECREF( tmp_import_name_from_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 511;
            type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        assert( var_run_in_terminal == NULL );
        var_run_in_terminal = tmp_assign_source_2;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( var_run_in_terminal );
        tmp_called_name_1 = var_run_in_terminal;
        CHECK_OBJECT( var_print_macro );
        tmp_args_element_name_1 = var_print_macro;
        frame_2e66240a37b34bb60a8a6a94c8cf2c17->m_frame.f_lineno = 512;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 512;
            type_description_1 = "coo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e66240a37b34bb60a8a6a94c8cf2c17 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_2e66240a37b34bb60a8a6a94c8cf2c17 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_2e66240a37b34bb60a8a6a94c8cf2c17, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_2e66240a37b34bb60a8a6a94c8cf2c17->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_2e66240a37b34bb60a8a6a94c8cf2c17, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_2e66240a37b34bb60a8a6a94c8cf2c17,
        type_description_1,
        par_event,
        var_print_macro,
        var_run_in_terminal
    );


    // Release cached frame.
    if ( frame_2e66240a37b34bb60a8a6a94c8cf2c17 == cache_frame_2e66240a37b34bb60a8a6a94c8cf2c17 )
    {
        Py_DECREF( frame_2e66240a37b34bb60a8a6a94c8cf2c17 );
    }
    cache_frame_2e66240a37b34bb60a8a6a94c8cf2c17 = NULL;

    assertFrameObject( frame_2e66240a37b34bb60a8a6a94c8cf2c17 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_print_macro );
    Py_DECREF( var_print_macro );
    var_print_macro = NULL;

    CHECK_OBJECT( (PyObject *)var_run_in_terminal );
    Py_DECREF( var_run_in_terminal );
    var_run_in_terminal = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_print_macro );
    Py_DECREF( var_print_macro );
    var_print_macro = NULL;

    Py_XDECREF( var_run_in_terminal );
    var_run_in_terminal = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro$$$function_1_print_macro( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_k = NULL;
    PyObject *tmp_for_loop_1__for_iterator = NULL;
    PyObject *tmp_for_loop_1__iter_value = NULL;
    struct Nuitka_FrameObject *frame_e0e1f04177862bc0b53992054feda9eb;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;
    static struct Nuitka_FrameObject *cache_frame_e0e1f04177862bc0b53992054feda9eb = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_2;
    PyObject *exception_keeper_value_2;
    PyTracebackObject *exception_keeper_tb_2;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_2;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_e0e1f04177862bc0b53992054feda9eb, codeobj_e0e1f04177862bc0b53992054feda9eb, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_e0e1f04177862bc0b53992054feda9eb = cache_frame_e0e1f04177862bc0b53992054feda9eb;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e0e1f04177862bc0b53992054feda9eb );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e0e1f04177862bc0b53992054feda9eb ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_iter_arg_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "event" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 508;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = PyCell_GET( self->m_closure[0] );
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 508;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_key_processor );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 508;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_iter_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_macro );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_iter_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 508;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        tmp_assign_source_1 = MAKE_ITERATOR( tmp_iter_arg_1 );
        Py_DECREF( tmp_iter_arg_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 508;
            type_description_1 = "oc";
            goto frame_exception_exit_1;
        }
        assert( tmp_for_loop_1__for_iterator == NULL );
        tmp_for_loop_1__for_iterator = tmp_assign_source_1;
    }
    // Tried code:
    loop_start_1:;
    {
        PyObject *tmp_next_source_1;
        PyObject *tmp_assign_source_2;
        CHECK_OBJECT( tmp_for_loop_1__for_iterator );
        tmp_next_source_1 = tmp_for_loop_1__for_iterator;
        tmp_assign_source_2 = ITERATOR_NEXT( tmp_next_source_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            if ( CHECK_AND_CLEAR_STOP_ITERATION_OCCURRED() )
            {

                goto loop_end_1;
            }
            else
            {

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
                type_description_1 = "oc";
                exception_lineno = 508;
                goto try_except_handler_2;
            }
        }

        {
            PyObject *old = tmp_for_loop_1__iter_value;
            tmp_for_loop_1__iter_value = tmp_assign_source_2;
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        CHECK_OBJECT( tmp_for_loop_1__iter_value );
        tmp_assign_source_3 = tmp_for_loop_1__iter_value;
        {
            PyObject *old = var_k;
            var_k = tmp_assign_source_3;
            Py_INCREF( var_k );
            Py_XDECREF( old );
        }

    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        tmp_called_name_1 = LOOKUP_BUILTIN( const_str_plain_print );
        assert( tmp_called_name_1 != NULL );
        CHECK_OBJECT( var_k );
        tmp_args_element_name_1 = var_k;
        frame_e0e1f04177862bc0b53992054feda9eb->m_frame.f_lineno = 509;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 509;
            type_description_1 = "oc";
            goto try_except_handler_2;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    if ( CONSIDER_THREADING() == false )
    {
        assert( ERROR_OCCURRED() );

        FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


        exception_lineno = 508;
        type_description_1 = "oc";
        goto try_except_handler_2;
    }
    goto loop_start_1;
    loop_end_1:;
    goto try_end_1;
    // Exception handler code:
    try_except_handler_2:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e0e1f04177862bc0b53992054feda9eb );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e0e1f04177862bc0b53992054feda9eb );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e0e1f04177862bc0b53992054feda9eb, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e0e1f04177862bc0b53992054feda9eb->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e0e1f04177862bc0b53992054feda9eb, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e0e1f04177862bc0b53992054feda9eb,
        type_description_1,
        var_k,
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_e0e1f04177862bc0b53992054feda9eb == cache_frame_e0e1f04177862bc0b53992054feda9eb )
    {
        Py_DECREF( frame_e0e1f04177862bc0b53992054feda9eb );
    }
    cache_frame_e0e1f04177862bc0b53992054feda9eb = NULL;

    assertFrameObject( frame_e0e1f04177862bc0b53992054feda9eb );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    Py_XDECREF( tmp_for_loop_1__iter_value );
    tmp_for_loop_1__iter_value = NULL;

    CHECK_OBJECT( (PyObject *)tmp_for_loop_1__for_iterator );
    Py_DECREF( tmp_for_loop_1__for_iterator );
    tmp_for_loop_1__for_iterator = NULL;

    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro$$$function_1_print_macro );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    Py_XDECREF( var_k );
    var_k = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_2 = exception_type;
    exception_keeper_value_2 = exception_value;
    exception_keeper_tb_2 = exception_tb;
    exception_keeper_lineno_2 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_k );
    var_k = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_2;
    exception_value = exception_keeper_value_2;
    exception_tb = exception_keeper_tb_2;
    exception_lineno = exception_keeper_lineno_2;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro$$$function_1_print_macro );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_43_undo( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_a5c5fa4e6cebfbe870b60beeb035c74c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_a5c5fa4e6cebfbe870b60beeb035c74c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_a5c5fa4e6cebfbe870b60beeb035c74c, codeobj_a5c5fa4e6cebfbe870b60beeb035c74c, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_a5c5fa4e6cebfbe870b60beeb035c74c = cache_frame_a5c5fa4e6cebfbe870b60beeb035c74c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_a5c5fa4e6cebfbe870b60beeb035c74c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_a5c5fa4e6cebfbe870b60beeb035c74c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 522;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_a5c5fa4e6cebfbe870b60beeb035c74c->m_frame.f_lineno = 522;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_undo );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 522;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a5c5fa4e6cebfbe870b60beeb035c74c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_a5c5fa4e6cebfbe870b60beeb035c74c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_a5c5fa4e6cebfbe870b60beeb035c74c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_a5c5fa4e6cebfbe870b60beeb035c74c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_a5c5fa4e6cebfbe870b60beeb035c74c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_a5c5fa4e6cebfbe870b60beeb035c74c,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_a5c5fa4e6cebfbe870b60beeb035c74c == cache_frame_a5c5fa4e6cebfbe870b60beeb035c74c )
    {
        Py_DECREF( frame_a5c5fa4e6cebfbe870b60beeb035c74c );
    }
    cache_frame_a5c5fa4e6cebfbe870b60beeb035c74c = NULL;

    assertFrameObject( frame_a5c5fa4e6cebfbe870b60beeb035c74c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_43_undo );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_43_undo );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    PyObject *var_change = NULL;
    struct Nuitka_FrameObject *frame_9ac0ea985d42c67f708b6ac774ef0d6d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_9ac0ea985d42c67f708b6ac774ef0d6d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_9ac0ea985d42c67f708b6ac774ef0d6d, codeobj_9ac0ea985d42c67f708b6ac774ef0d6d, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_9ac0ea985d42c67f708b6ac774ef0d6d = cache_frame_9ac0ea985d42c67f708b6ac774ef0d6d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_9ac0ea985d42c67f708b6ac774ef0d6d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_9ac0ea985d42c67f708b6ac774ef0d6d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 532;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_source_name_2;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_compexpr_left_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_arg );
        if ( tmp_compexpr_left_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 535;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = const_int_pos_1;
        tmp_res = RICH_COMPARE_BOOL_NOTEQ_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_left_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 535;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assign_source_2;
            tmp_assign_source_2 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_1_change(  );



            assert( var_change == NULL );
            var_change = tmp_assign_source_2;
        }
        goto branch_end_1;
        branch_no_1:;
        {
            PyObject *tmp_assign_source_3;
            tmp_assign_source_3 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_2_change(  );



            assert( var_change == NULL );
            var_change = tmp_assign_source_3;
        }
        branch_end_1:;
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_assattr_target_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_Document );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Document );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Document" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 542;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        tmp_dict_key_1 = const_str_plain_text;
        tmp_source_name_3 = const_str_newline;
        tmp_called_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_join );
        assert( !(tmp_called_name_2 == NULL) );
        tmp_called_name_3 = (PyObject *)&PyMap_Type;
        CHECK_OBJECT( var_change );
        tmp_args_element_name_2 = var_change;
        CHECK_OBJECT( var_buff );
        tmp_source_name_4 = var_buff;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_text );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 543;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_9ac0ea985d42c67f708b6ac774ef0d6d->m_frame.f_lineno = 543;
        tmp_args_element_name_3 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_splitlines );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 543;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_9ac0ea985d42c67f708b6ac774ef0d6d->m_frame.f_lineno = 543;
        {
            PyObject *call_args[] = { tmp_args_element_name_2, tmp_args_element_name_3 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 543;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        frame_9ac0ea985d42c67f708b6ac774ef0d6d->m_frame.f_lineno = 543;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_dict_value_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 543;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_2 = const_str_plain_cursor_position;
        tmp_dict_value_2 = const_int_0;
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_9ac0ea985d42c67f708b6ac774ef0d6d->m_frame.f_lineno = 542;
        tmp_assattr_name_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 542;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_buff );
        tmp_assattr_target_1 = var_buff;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_document, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 542;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( var_buff );
        tmp_called_instance_2 = var_buff;
        frame_9ac0ea985d42c67f708b6ac774ef0d6d->m_frame.f_lineno = 547;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_2, const_str_plain_validate_and_handle );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 547;
            type_description_1 = "ooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9ac0ea985d42c67f708b6ac774ef0d6d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_9ac0ea985d42c67f708b6ac774ef0d6d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_9ac0ea985d42c67f708b6ac774ef0d6d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_9ac0ea985d42c67f708b6ac774ef0d6d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_9ac0ea985d42c67f708b6ac774ef0d6d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_9ac0ea985d42c67f708b6ac774ef0d6d,
        type_description_1,
        par_event,
        var_buff,
        var_change
    );


    // Release cached frame.
    if ( frame_9ac0ea985d42c67f708b6ac774ef0d6d == cache_frame_9ac0ea985d42c67f708b6ac774ef0d6d )
    {
        Py_DECREF( frame_9ac0ea985d42c67f708b6ac774ef0d6d );
    }
    cache_frame_9ac0ea985d42c67f708b6ac774ef0d6d = NULL;

    assertFrameObject( frame_9ac0ea985d42c67f708b6ac774ef0d6d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    CHECK_OBJECT( (PyObject *)var_change );
    Py_DECREF( var_change );
    var_change = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    Py_XDECREF( var_change );
    var_change = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_1_change( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_line = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_823324200e60cf6166d8f7ff6d294e48;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_823324200e60cf6166d8f7ff6d294e48 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_823324200e60cf6166d8f7ff6d294e48, codeobj_823324200e60cf6166d8f7ff6d294e48, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_823324200e60cf6166d8f7ff6d294e48 = cache_frame_823324200e60cf6166d8f7ff6d294e48;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_823324200e60cf6166d8f7ff6d294e48 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_823324200e60cf6166d8f7ff6d294e48 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        int tmp_truth_name_1;
        PyObject *tmp_subscribed_name_1;
        PyObject *tmp_subscript_name_1;
        CHECK_OBJECT( par_line );
        tmp_called_instance_1 = par_line;
        frame_823324200e60cf6166d8f7ff6d294e48->m_frame.f_lineno = 537;
        tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_1, const_str_plain_startswith, &PyTuple_GET_ITEM( const_tuple_str_chr_35_tuple, 0 ) );

        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 537;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_truth_name_1 = CHECK_IF_TRUE( tmp_call_result_1 );
        if ( tmp_truth_name_1 == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_call_result_1 );

            exception_lineno = 537;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = tmp_truth_name_1 == 1 ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        Py_DECREF( tmp_call_result_1 );
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto condexpr_true_1;
        }
        else
        {
            goto condexpr_false_1;
        }
        condexpr_true_1:;
        CHECK_OBJECT( par_line );
        tmp_subscribed_name_1 = par_line;
        tmp_subscript_name_1 = const_slice_int_pos_1_none_none;
        tmp_return_value = LOOKUP_SUBSCRIPT( tmp_subscribed_name_1, tmp_subscript_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 537;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto condexpr_end_1;
        condexpr_false_1:;
        CHECK_OBJECT( par_line );
        tmp_return_value = par_line;
        Py_INCREF( tmp_return_value );
        condexpr_end_1:;
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_823324200e60cf6166d8f7ff6d294e48 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_823324200e60cf6166d8f7ff6d294e48 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_823324200e60cf6166d8f7ff6d294e48 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_823324200e60cf6166d8f7ff6d294e48, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_823324200e60cf6166d8f7ff6d294e48->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_823324200e60cf6166d8f7ff6d294e48, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_823324200e60cf6166d8f7ff6d294e48,
        type_description_1,
        par_line
    );


    // Release cached frame.
    if ( frame_823324200e60cf6166d8f7ff6d294e48 == cache_frame_823324200e60cf6166d8f7ff6d294e48 )
    {
        Py_DECREF( frame_823324200e60cf6166d8f7ff6d294e48 );
    }
    cache_frame_823324200e60cf6166d8f7ff6d294e48 = NULL;

    assertFrameObject( frame_823324200e60cf6166d8f7ff6d294e48 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_1_change );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_line );
    Py_DECREF( par_line );
    par_line = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_line );
    Py_DECREF( par_line );
    par_line = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_1_change );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_2_change( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_line = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_dcde661036877db73bd081d3ad20601e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_dcde661036877db73bd081d3ad20601e = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_dcde661036877db73bd081d3ad20601e, codeobj_dcde661036877db73bd081d3ad20601e, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_dcde661036877db73bd081d3ad20601e = cache_frame_dcde661036877db73bd081d3ad20601e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dcde661036877db73bd081d3ad20601e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dcde661036877db73bd081d3ad20601e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_left_name_1;
        PyObject *tmp_right_name_1;
        tmp_left_name_1 = const_str_chr_35;
        CHECK_OBJECT( par_line );
        tmp_right_name_1 = par_line;
        tmp_return_value = BINARY_OPERATION_ADD_UNICODE_OBJECT( tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 540;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dcde661036877db73bd081d3ad20601e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dcde661036877db73bd081d3ad20601e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dcde661036877db73bd081d3ad20601e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dcde661036877db73bd081d3ad20601e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dcde661036877db73bd081d3ad20601e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dcde661036877db73bd081d3ad20601e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dcde661036877db73bd081d3ad20601e,
        type_description_1,
        par_line
    );


    // Release cached frame.
    if ( frame_dcde661036877db73bd081d3ad20601e == cache_frame_dcde661036877db73bd081d3ad20601e )
    {
        Py_DECREF( frame_dcde661036877db73bd081d3ad20601e );
    }
    cache_frame_dcde661036877db73bd081d3ad20601e = NULL;

    assertFrameObject( frame_dcde661036877db73bd081d3ad20601e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_2_change );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_line );
    Py_DECREF( par_line );
    par_line = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_line );
    Py_DECREF( par_line );
    par_line = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_2_change );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_45_vi_editing_mode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_826b738cdd1cbdebe17b65f5c0d4938d;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_826b738cdd1cbdebe17b65f5c0d4938d = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_826b738cdd1cbdebe17b65f5c0d4938d, codeobj_826b738cdd1cbdebe17b65f5c0d4938d, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_826b738cdd1cbdebe17b65f5c0d4938d = cache_frame_826b738cdd1cbdebe17b65f5c0d4938d;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_826b738cdd1cbdebe17b65f5c0d4938d );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_826b738cdd1cbdebe17b65f5c0d4938d ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_EditingMode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_EditingMode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "EditingMode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 553;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_VI );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 553;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_assattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_app );
        if ( tmp_assattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_1 );

            exception_lineno = 553;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_editing_mode, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_target_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 553;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_826b738cdd1cbdebe17b65f5c0d4938d );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_826b738cdd1cbdebe17b65f5c0d4938d );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_826b738cdd1cbdebe17b65f5c0d4938d, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_826b738cdd1cbdebe17b65f5c0d4938d->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_826b738cdd1cbdebe17b65f5c0d4938d, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_826b738cdd1cbdebe17b65f5c0d4938d,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_826b738cdd1cbdebe17b65f5c0d4938d == cache_frame_826b738cdd1cbdebe17b65f5c0d4938d )
    {
        Py_DECREF( frame_826b738cdd1cbdebe17b65f5c0d4938d );
    }
    cache_frame_826b738cdd1cbdebe17b65f5c0d4938d = NULL;

    assertFrameObject( frame_826b738cdd1cbdebe17b65f5c0d4938d );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_45_vi_editing_mode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_45_vi_editing_mode );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_46_emacs_editing_mode( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0a5b5be3846430f22fb03f1d970c448c;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_0a5b5be3846430f22fb03f1d970c448c = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0a5b5be3846430f22fb03f1d970c448c, codeobj_0a5b5be3846430f22fb03f1d970c448c, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_0a5b5be3846430f22fb03f1d970c448c = cache_frame_0a5b5be3846430f22fb03f1d970c448c;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0a5b5be3846430f22fb03f1d970c448c );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0a5b5be3846430f22fb03f1d970c448c ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_source_name_2;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_EditingMode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_EditingMode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "EditingMode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 559;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = tmp_mvar_value_1;
        tmp_assattr_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_EMACS );
        if ( tmp_assattr_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 559;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_assattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_app );
        if ( tmp_assattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_assattr_name_1 );

            exception_lineno = 559;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_editing_mode, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_target_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 559;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0a5b5be3846430f22fb03f1d970c448c );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0a5b5be3846430f22fb03f1d970c448c );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0a5b5be3846430f22fb03f1d970c448c, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0a5b5be3846430f22fb03f1d970c448c->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0a5b5be3846430f22fb03f1d970c448c, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0a5b5be3846430f22fb03f1d970c448c,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_0a5b5be3846430f22fb03f1d970c448c == cache_frame_0a5b5be3846430f22fb03f1d970c448c )
    {
        Py_DECREF( frame_0a5b5be3846430f22fb03f1d970c448c );
    }
    cache_frame_0a5b5be3846430f22fb03f1d970c448c = NULL;

    assertFrameObject( frame_0a5b5be3846430f22fb03f1d970c448c );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_46_emacs_editing_mode );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_46_emacs_editing_mode );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_47_prefix_meta( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_5ae4de48ba54b51ddeafe37a66e9d813;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_5ae4de48ba54b51ddeafe37a66e9d813 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_5ae4de48ba54b51ddeafe37a66e9d813, codeobj_5ae4de48ba54b51ddeafe37a66e9d813, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *) );
    frame_5ae4de48ba54b51ddeafe37a66e9d813 = cache_frame_5ae4de48ba54b51ddeafe37a66e9d813;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_5ae4de48ba54b51ddeafe37a66e9d813 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_5ae4de48ba54b51ddeafe37a66e9d813 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 573;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_key_processor );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 573;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_feed );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 573;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_KeyPress );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KeyPress );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "KeyPress" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 573;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 573;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_2;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_Escape );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 573;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_5ae4de48ba54b51ddeafe37a66e9d813->m_frame.f_lineno = 573;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 573;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_e2d16c98e7cb03ba7fd208a07fd5d185 );
        frame_5ae4de48ba54b51ddeafe37a66e9d813->m_frame.f_lineno = 573;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 573;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5ae4de48ba54b51ddeafe37a66e9d813 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_5ae4de48ba54b51ddeafe37a66e9d813 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_5ae4de48ba54b51ddeafe37a66e9d813, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_5ae4de48ba54b51ddeafe37a66e9d813->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_5ae4de48ba54b51ddeafe37a66e9d813, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_5ae4de48ba54b51ddeafe37a66e9d813,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_5ae4de48ba54b51ddeafe37a66e9d813 == cache_frame_5ae4de48ba54b51ddeafe37a66e9d813 )
    {
        Py_DECREF( frame_5ae4de48ba54b51ddeafe37a66e9d813 );
    }
    cache_frame_5ae4de48ba54b51ddeafe37a66e9d813 = NULL;

    assertFrameObject( frame_5ae4de48ba54b51ddeafe37a66e9d813 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_47_prefix_meta );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_47_prefix_meta );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_CellObject *var_buff = PyCell_EMPTY();
    struct Nuitka_CellObject *var_new_index = PyCell_EMPTY();
    PyObject *var_set_working_index = NULL;
    struct Nuitka_FrameObject *frame_aa3d720d464998daed17c750d1ea6be2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_aa3d720d464998daed17c750d1ea6be2 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_aa3d720d464998daed17c750d1ea6be2, codeobj_aa3d720d464998daed17c750d1ea6be2, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_aa3d720d464998daed17c750d1ea6be2 = cache_frame_aa3d720d464998daed17c750d1ea6be2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_aa3d720d464998daed17c750d1ea6be2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_aa3d720d464998daed17c750d1ea6be2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 582;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_buff ) == NULL );
        PyCell_SET( var_buff, tmp_assign_source_1 );

    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_right_name_1;
        CHECK_OBJECT( PyCell_GET( var_buff ) );
        tmp_source_name_2 = PyCell_GET( var_buff );
        tmp_left_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_working_index );
        if ( tmp_left_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 583;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        tmp_right_name_1 = const_int_pos_1;
        tmp_assign_source_2 = BINARY_OPERATION_ADD_OBJECT_LONG( tmp_left_name_1, tmp_right_name_1 );
        Py_DECREF( tmp_left_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 583;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        assert( PyCell_GET( var_new_index ) == NULL );
        PyCell_SET( var_new_index, tmp_assign_source_2 );

    }
    {
        PyObject *tmp_called_instance_1;
        PyObject *tmp_call_result_1;
        CHECK_OBJECT( PyCell_GET( var_buff ) );
        tmp_called_instance_1 = PyCell_GET( var_buff );
        frame_aa3d720d464998daed17c750d1ea6be2->m_frame.f_lineno = 587;
        tmp_call_result_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_validate_and_handle );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 587;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next$$$function_1_set_working_index(  );

        ((struct Nuitka_FunctionObject *)tmp_assign_source_3)->m_closure[0] = var_buff;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_3)->m_closure[0] );
        ((struct Nuitka_FunctionObject *)tmp_assign_source_3)->m_closure[1] = var_new_index;
        Py_INCREF( ((struct Nuitka_FunctionObject *)tmp_assign_source_3)->m_closure[1] );


        assert( var_set_working_index == NULL );
        var_set_working_index = tmp_assign_source_3;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_source_name_4;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_source_name_3 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_app );
        if ( tmp_source_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 594;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_pre_run_callables );
        Py_DECREF( tmp_source_name_3 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 594;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_set_working_index );
        tmp_args_element_name_1 = var_set_working_index;
        frame_aa3d720d464998daed17c750d1ea6be2->m_frame.f_lineno = 594;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_2 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_append, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 594;
            type_description_1 = "occo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa3d720d464998daed17c750d1ea6be2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_aa3d720d464998daed17c750d1ea6be2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_aa3d720d464998daed17c750d1ea6be2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_aa3d720d464998daed17c750d1ea6be2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_aa3d720d464998daed17c750d1ea6be2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_aa3d720d464998daed17c750d1ea6be2,
        type_description_1,
        par_event,
        var_buff,
        var_new_index,
        var_set_working_index
    );


    // Release cached frame.
    if ( frame_aa3d720d464998daed17c750d1ea6be2 == cache_frame_aa3d720d464998daed17c750d1ea6be2 )
    {
        Py_DECREF( frame_aa3d720d464998daed17c750d1ea6be2 );
    }
    cache_frame_aa3d720d464998daed17c750d1ea6be2 = NULL;

    assertFrameObject( frame_aa3d720d464998daed17c750d1ea6be2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    CHECK_OBJECT( (PyObject *)var_new_index );
    Py_DECREF( var_new_index );
    var_new_index = NULL;

    CHECK_OBJECT( (PyObject *)var_set_working_index );
    Py_DECREF( var_set_working_index );
    var_set_working_index = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    CHECK_OBJECT( (PyObject *)var_new_index );
    Py_DECREF( var_new_index );
    var_new_index = NULL;

    Py_XDECREF( var_set_working_index );
    var_set_working_index = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next$$$function_1_set_working_index( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_e0c7a156ecdfbd87849e32041464e850;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_e0c7a156ecdfbd87849e32041464e850 = NULL;
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_e0c7a156ecdfbd87849e32041464e850, codeobj_e0c7a156ecdfbd87849e32041464e850, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_e0c7a156ecdfbd87849e32041464e850 = cache_frame_e0c7a156ecdfbd87849e32041464e850;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_e0c7a156ecdfbd87849e32041464e850 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_e0c7a156ecdfbd87849e32041464e850 ) == 2 ); // Frame stack

    // Framed code:
    {
        nuitka_bool tmp_condition_result_1;
        PyObject *tmp_compexpr_left_1;
        PyObject *tmp_compexpr_right_1;
        PyObject *tmp_len_arg_1;
        PyObject *tmp_source_name_1;
        if ( PyCell_GET( self->m_closure[1] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "new_index" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 591;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_compexpr_left_1 = PyCell_GET( self->m_closure[1] );
        if ( PyCell_GET( self->m_closure[0] ) == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "buff" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 591;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }

        tmp_source_name_1 = PyCell_GET( self->m_closure[0] );
        tmp_len_arg_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain__working_lines );
        if ( tmp_len_arg_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 591;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        tmp_compexpr_right_1 = BUILTIN_LEN( tmp_len_arg_1 );
        Py_DECREF( tmp_len_arg_1 );
        if ( tmp_compexpr_right_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 591;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        tmp_res = RICH_COMPARE_BOOL_LT_OBJECT_OBJECT( tmp_compexpr_left_1, tmp_compexpr_right_1 );
        Py_DECREF( tmp_compexpr_right_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 591;
            type_description_1 = "cc";
            goto frame_exception_exit_1;
        }
        tmp_condition_result_1 = ( tmp_res != 0 ) ? NUITKA_BOOL_TRUE : NUITKA_BOOL_FALSE;
        if ( tmp_condition_result_1 == NUITKA_BOOL_TRUE )
        {
            goto branch_yes_1;
        }
        else
        {
            goto branch_no_1;
        }
        branch_yes_1:;
        {
            PyObject *tmp_assattr_name_1;
            PyObject *tmp_assattr_target_1;
            if ( PyCell_GET( self->m_closure[1] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "new_index" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 592;
                type_description_1 = "cc";
                goto frame_exception_exit_1;
            }

            tmp_assattr_name_1 = PyCell_GET( self->m_closure[1] );
            if ( PyCell_GET( self->m_closure[0] ) == NULL )
            {

                exception_type = PyExc_NameError;
                Py_INCREF( exception_type );
                exception_value = PyUnicode_FromFormat( "free variable '%s' referenced before assignment in enclosing scope", "buff" );
                exception_tb = NULL;
                NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
                CHAIN_EXCEPTION( exception_value );

                exception_lineno = 592;
                type_description_1 = "cc";
                goto frame_exception_exit_1;
            }

            tmp_assattr_target_1 = PyCell_GET( self->m_closure[0] );
            tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_working_index, tmp_assattr_name_1 );
            if ( tmp_result == false )
            {
                assert( ERROR_OCCURRED() );

                FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


                exception_lineno = 592;
                type_description_1 = "cc";
                goto frame_exception_exit_1;
            }
        }
        branch_no_1:;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e0c7a156ecdfbd87849e32041464e850 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_e0c7a156ecdfbd87849e32041464e850 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_e0c7a156ecdfbd87849e32041464e850, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_e0c7a156ecdfbd87849e32041464e850->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_e0c7a156ecdfbd87849e32041464e850, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_e0c7a156ecdfbd87849e32041464e850,
        type_description_1,
        self->m_closure[1],
        self->m_closure[0]
    );


    // Release cached frame.
    if ( frame_e0c7a156ecdfbd87849e32041464e850 == cache_frame_e0c7a156ecdfbd87849e32041464e850 )
    {
        Py_DECREF( frame_e0c7a156ecdfbd87849e32041464e850 );
    }
    cache_frame_e0c7a156ecdfbd87849e32041464e850 = NULL;

    assertFrameObject( frame_e0c7a156ecdfbd87849e32041464e850 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto function_return_exit;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next$$$function_1_set_working_index );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_49_edit_and_execute( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_buff = NULL;
    struct Nuitka_FrameObject *frame_060e0f20cbcf031ce2850e90f9802fc1;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_060e0f20cbcf031ce2850e90f9802fc1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_060e0f20cbcf031ce2850e90f9802fc1, codeobj_060e0f20cbcf031ce2850e90f9802fc1, module_prompt_toolkit$key_binding$bindings$named_commands, sizeof(void *)+sizeof(void *) );
    frame_060e0f20cbcf031ce2850e90f9802fc1 = cache_frame_060e0f20cbcf031ce2850e90f9802fc1;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_060e0f20cbcf031ce2850e90f9802fc1 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_060e0f20cbcf031ce2850e90f9802fc1 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 602;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_buff == NULL );
        var_buff = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( var_buff );
        tmp_source_name_2 = var_buff;
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_open_in_editor );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 603;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = PyDict_Copy( const_dict_47be3150a6234742830e43bdf5d5778f );
        frame_060e0f20cbcf031ce2850e90f9802fc1->m_frame.f_lineno = 603;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 603;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_060e0f20cbcf031ce2850e90f9802fc1 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_060e0f20cbcf031ce2850e90f9802fc1 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_060e0f20cbcf031ce2850e90f9802fc1, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_060e0f20cbcf031ce2850e90f9802fc1->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_060e0f20cbcf031ce2850e90f9802fc1, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_060e0f20cbcf031ce2850e90f9802fc1,
        type_description_1,
        par_event,
        var_buff
    );


    // Release cached frame.
    if ( frame_060e0f20cbcf031ce2850e90f9802fc1 == cache_frame_060e0f20cbcf031ce2850e90f9802fc1 )
    {
        Py_DECREF( frame_060e0f20cbcf031ce2850e90f9802fc1 );
    }
    cache_frame_060e0f20cbcf031ce2850e90f9802fc1 = NULL;

    assertFrameObject( frame_060e0f20cbcf031ce2850e90f9802fc1 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_49_edit_and_execute );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_buff );
    Py_DECREF( var_buff );
    var_buff = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_buff );
    var_buff = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$named_commands$$$function_49_edit_and_execute );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_10_redraw_current_line(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_10_redraw_current_line,
        const_str_plain_redraw_current_line,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8a40f6cb630df8a665be9a28d5c40a17,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_9d4e7794e591dce4cbbd54460a027e3f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_11_accept_line(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_11_accept_line,
        const_str_plain_accept_line,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c6053e4667cff011640e3abbd139e7da,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_ff960a63a7cd023625d10d8afb7b75af,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_12_previous_history(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_12_previous_history,
        const_str_plain_previous_history,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_90864adb7408db80ae55fc791e7071e7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_072123a03afbbc4a30fb698f3e63aac0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_13_next_history(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_13_next_history,
        const_str_plain_next_history,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_338ce756c26e6afd0cb3658efccaadb6,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_2a8fcad8b385b0c88d0194c208d97103,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_14_beginning_of_history(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_14_beginning_of_history,
        const_str_plain_beginning_of_history,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_12cd250f02ebc71ccdb2ff48752774c5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_550d97e354a8bb55dff671e969fae43d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_15_end_of_history(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_15_end_of_history,
        const_str_plain_end_of_history,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_48e352436fa387cc54d3a74128217bbf,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_2ea096c33d6acdbf8f658453d87483a6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_16_reverse_search_history(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_16_reverse_search_history,
        const_str_plain_reverse_search_history,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d5c1944153306d7fb7a9366ba732546a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_b957e174ba0fdf03669f960ad9df6dd4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_17_end_of_file(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_17_end_of_file,
        const_str_plain_end_of_file,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_50753afd5df05e0514e22929ed73d40a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_9cc1ee391748caad62e066f6aaa843a5,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_18_delete_char(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_18_delete_char,
        const_str_plain_delete_char,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_34ff4fc0dd5f52aacc6ec26d8e93f2df,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_ade7a4439193c90488d0fdc4fa908f0e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_19_backward_delete_char(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_19_backward_delete_char,
        const_str_plain_backward_delete_char,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0518d9e28a85d130dc7dd7830cf50ece,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_48235e17fb80f346bb9d79f90caba172,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register,
        const_str_plain_register,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2e176e57c9bb68b214ddce5bff4992db,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_74f41a8411a3e103f4e1c3a33b0e5662,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register$$$function_1_decorator(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register$$$function_1_decorator,
        const_str_plain_decorator,
#if PYTHON_VERSION >= 300
        const_str_digest_d2154be122eab39bb0280421ac483f4e,
#endif
        codeobj_588b68462b0b864f34de8314f9ada309,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_a382cc8c0cded4a250815b426d58ad03,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_20_self_insert(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_20_self_insert,
        const_str_plain_self_insert,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8468e9931c6c1f378d1338f0f777eb21,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_7d1c0c5af5d1875a6a400642e2bc6d31,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_21_transpose_chars(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_21_transpose_chars,
        const_str_plain_transpose_chars,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5a13762e2fb0e0384b5215a3360ed584,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_86fc33ac35792a7b74893a235817abff,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_22_uppercase_word(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_22_uppercase_word,
        const_str_plain_uppercase_word,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_e121294c05d8b3cba10d7b2078f3a52a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_968f9b527a999c6f0eb6156bb8866021,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_23_downcase_word(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_23_downcase_word,
        const_str_plain_downcase_word,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_690bd84dbafdd852898db42272c38b83,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_45658ead8dc363a1802864ace51fce07,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_24_capitalize_word(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_24_capitalize_word,
        const_str_plain_capitalize_word,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a4151aa1583707a1af87bc7dca844f41,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_8ab7b8386ed49775e91498682cd2986c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_25_quoted_insert(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_25_quoted_insert,
        const_str_plain_quoted_insert,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_47c4ee38692d0bdf1d33575b039983a4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_02a5690946f1cc1ce0757fc48394bbff,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_26_kill_line(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_26_kill_line,
        const_str_plain_kill_line,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_29f3c5ee949bbc61155cc369d86c4fff,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_d170e87999e8451393080db2ed4cfc68,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_27_kill_word(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_27_kill_word,
        const_str_plain_kill_word,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8c14b78b16a71186c05c11186cae7ea7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_a519a736eca93ebf6d46cc412c173add,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_28_unix_word_rubout( PyObject *defaults )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_28_unix_word_rubout,
        const_str_plain_unix_word_rubout,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c80d506d3405d850aceda812c41be29d,
        defaults,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_bac376679fee18ab8e8f621bfb04b25e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_29_backward_kill_word(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_29_backward_kill_word,
        const_str_plain_backward_kill_word,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5cef15a627204a6a34d9977b1d03f826,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_6205b7c65ea809e47b9f08db79bf8bf3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_2_get_by_name(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_2_get_by_name,
        const_str_plain_get_by_name,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_4411806d0c478c17e0ae7a40d162bc9d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_6435b37fd837c6013a16282357bd256d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_30_delete_horizontal_space(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_30_delete_horizontal_space,
        const_str_plain_delete_horizontal_space,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_63a9d9d241c96d8c5750972866445eb4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_66d9c352af7ed5132c71e2f398933b45,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_31_unix_line_discard(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_31_unix_line_discard,
        const_str_plain_unix_line_discard,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_acba45f633543b0c0ce4809e8825e34d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_0759fa4e9adc187ca65f264c0278e040,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_32_yank(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_32_yank,
        const_str_plain_yank,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3419c02f5e676ebb31c78b9f702e00ea,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_31c4b630f4ffd96ff20e45c83bc6d08e,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_33_yank_nth_arg(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_33_yank_nth_arg,
        const_str_plain_yank_nth_arg,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_51f16b1961475f8370f81cc4016a2a59,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_1beb6b9754702adc084aa39d18c50ba8,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_34_yank_last_arg(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_34_yank_last_arg,
        const_str_plain_yank_last_arg,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c8eead45aa3dc99f95a05d8ccc2a5ff5,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_705488b055618885c52de69cddaccd3f,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_35_yank_pop(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_35_yank_pop,
        const_str_plain_yank_pop,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9a5d0dfb29ab447ff46ddd48a0d06117,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_feff521061f9718c2c4575ee30698cef,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_36_complete(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_36_complete,
        const_str_plain_complete,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_d1ae5db46ddec2f0d84cc2f7753005f4,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_85dadd9de2ac2ff25d2fd136dc90bc72,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_37_menu_complete(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_37_menu_complete,
        const_str_plain_menu_complete,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9b1fe3eab7b189a9f30b3fed0cdb3b00,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_b7ccea59c184956e0e6390de033e4e13,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_38_menu_complete_backward(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_38_menu_complete_backward,
        const_str_plain_menu_complete_backward,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c5f30d095ec1aff287f6e262473fd2c1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_eac4292b628774117c5284605831d6d0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_39_start_kbd_macro(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_39_start_kbd_macro,
        const_str_plain_start_kbd_macro,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_6750b758ae89dba6953c9afd97461adb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_3f4d599e74335620cbcb6d738c0fe3fc,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_3_beginning_of_line(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_3_beginning_of_line,
        const_str_plain_beginning_of_line,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_749f2ff2a66d38422ecbc1eff69df74d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_78bb55f89abb5b3ad76eb473479dd6a3,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_40_end_kbd_macro(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_40_end_kbd_macro,
        const_str_plain_end_kbd_macro,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5bdc03f3d4c97002f3d3b93d45e2810a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_0917fbd04b33410fa0c2d713b22ffff2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_41_call_last_kbd_macro(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_41_call_last_kbd_macro,
        const_str_plain_call_last_kbd_macro,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_efa8e942d162049ae5d2899df9e1088c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_6436ae48f56ffce91fa387300abe37dd,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro,
        const_str_plain_print_last_kbd_macro,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_2e66240a37b34bb60a8a6a94c8cf2c17,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_bd43b33ede5b6fb74b5a7494f79f0067,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro$$$function_1_print_macro(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro$$$function_1_print_macro,
        const_str_plain_print_macro,
#if PYTHON_VERSION >= 300
        const_str_digest_34910ada2a9571c81d0198bdebe83810,
#endif
        codeobj_e0e1f04177862bc0b53992054feda9eb,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        NULL,
        1
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_43_undo(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_43_undo,
        const_str_plain_undo,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_a5c5fa4e6cebfbe870b60beeb035c74c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_f49d647b7859fb1b29a225aba45b7cc4,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment,
        const_str_plain_insert_comment,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_9ac0ea985d42c67f708b6ac774ef0d6d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_6770944cf34737f0fd1c0ae57ea84e00,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_1_change(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_1_change,
        const_str_plain_change,
#if PYTHON_VERSION >= 300
        const_str_digest_ce90e5fd750ebe5fbf3c059d5d422d22,
#endif
        codeobj_823324200e60cf6166d8f7ff6d294e48,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_2_change(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment$$$function_2_change,
        const_str_plain_change,
#if PYTHON_VERSION >= 300
        const_str_digest_ce90e5fd750ebe5fbf3c059d5d422d22,
#endif
        codeobj_dcde661036877db73bd081d3ad20601e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_45_vi_editing_mode(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_45_vi_editing_mode,
        const_str_plain_vi_editing_mode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_826b738cdd1cbdebe17b65f5c0d4938d,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_f3142aabc8984e9b5ee4284ee94fc9e1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_46_emacs_editing_mode(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_46_emacs_editing_mode,
        const_str_plain_emacs_editing_mode,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_0a5b5be3846430f22fb03f1d970c448c,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_3dbaa58e29edd8b0fa66cb30e90e21b6,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_47_prefix_meta(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_47_prefix_meta,
        const_str_plain_prefix_meta,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_5ae4de48ba54b51ddeafe37a66e9d813,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_7f3dc17e1f79ad0755aafac73dd0d273,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next,
        const_str_plain_operate_and_get_next,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_aa3d720d464998daed17c750d1ea6be2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_d55dd59af31cb709bc87237ed76a42a2,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next$$$function_1_set_working_index(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next$$$function_1_set_working_index,
        const_str_plain_set_working_index,
#if PYTHON_VERSION >= 300
        const_str_digest_67055978b31b0cd8ee70ae4eac029a8f,
#endif
        codeobj_e0c7a156ecdfbd87849e32041464e850,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        NULL,
        2
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_49_edit_and_execute(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_49_edit_and_execute,
        const_str_plain_edit_and_execute,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_060e0f20cbcf031ce2850e90f9802fc1,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_17a581addb5f14b5622a3275e5609824,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_4_end_of_line(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_4_end_of_line,
        const_str_plain_end_of_line,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_c3b4b5c70d8cd76855f0bfd713e153ba,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_9836aff840b931e5ea309144651873e0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_5_forward_char(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_5_forward_char,
        const_str_plain_forward_char,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_3ce7bc74879e0ac013b3102a55e00fba,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_b6c306f5553e75c8ab5c659e7b1bb0c0,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_6_backward_char(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_6_backward_char,
        const_str_plain_backward_char,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_24a983b79c05a66d729037082626d405,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_8050ff83554e3b5eef4c5274df33464c,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_7_forward_word(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_7_forward_word,
        const_str_plain_forward_word,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1d9ea5d5080207ab5bda5f057edfb276,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_56ddd1cd56382cd88f10c4ba7452feda,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_8_backward_word(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_8_backward_word,
        const_str_plain_backward_word,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_767bcb9ae21587b5979dce639ee2d6a3,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_793ec132e5b39c0badda11bae0171c7a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_9_clear_screen(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$named_commands$$$function_9_clear_screen,
        const_str_plain_clear_screen,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_8b24d74de375253360b7bb31d8618a2b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$named_commands,
        const_str_digest_e7a7c19e756842d84e9bf8616e02117b,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$key_binding$bindings$named_commands =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.key_binding.bindings.named_commands",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$named_commands)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$key_binding$bindings$named_commands)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$key_binding$bindings$named_commands );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.named_commands: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.named_commands: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.named_commands: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$key_binding$bindings$named_commands" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$key_binding$bindings$named_commands = Py_InitModule4(
        "prompt_toolkit.key_binding.bindings.named_commands",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$key_binding$bindings$named_commands = PyModule_Create( &mdef_prompt_toolkit$key_binding$bindings$named_commands );
#endif

    moduledict_prompt_toolkit$key_binding$bindings$named_commands = MODULE_DICT( module_prompt_toolkit$key_binding$bindings$named_commands );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$key_binding$bindings$named_commands,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$key_binding$bindings$named_commands,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$key_binding$bindings$named_commands,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$key_binding$bindings$named_commands,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$key_binding$bindings$named_commands );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_274b6eb2a65f09ccc339d29eea36ebd9, module_prompt_toolkit$key_binding$bindings$named_commands );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_8c5fb6e65dc9f8f648efd26b8fac0a57;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = const_str_digest_60701c7754d852c86a2f19de3267c311;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_8c5fb6e65dc9f8f648efd26b8fac0a57 = MAKE_MODULE_FRAME( codeobj_8c5fb6e65dc9f8f648efd26b8fac0a57, module_prompt_toolkit$key_binding$bindings$named_commands );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_8c5fb6e65dc9f8f648efd26b8fac0a57 );
    assert( Py_REFCNT( frame_8c5fb6e65dc9f8f648efd26b8fac0a57 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 6;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_d8d4f5e440c7f01129293c18f6555af5;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_range_tuple;
        tmp_level_name_1 = const_int_0;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 7;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_range );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_range, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_plain_six;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = Py_None;
        tmp_level_name_2 = const_int_0;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 8;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 8;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_six, tmp_assign_source_6 );
    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_plain_completion;
        tmp_globals_name_3 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_1d906eddd9a25222b8901b143039d608_tuple;
        tmp_level_name_3 = const_int_pos_1;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 10;
        tmp_assign_source_7 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_7;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_3 ) )
        {
           tmp_assign_source_8 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_3,
                (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands,
                const_str_plain_generate_completions,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_generate_completions );
        }

        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_generate_completions, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        if ( PyModule_Check( tmp_import_name_from_4 ) )
        {
           tmp_assign_source_9 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_4,
                (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands,
                const_str_plain_display_completions_like_readline,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_display_completions_like_readline );
        }

        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_display_completions_like_readline, tmp_assign_source_9 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_5;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_e6c13694f23bea8b3c2d2e215abd5980;
        tmp_globals_name_4 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_Document_tuple;
        tmp_level_name_4 = const_int_0;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 11;
        tmp_import_name_from_5 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_Document );
        Py_DECREF( tmp_import_name_from_5 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 11;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_Document, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_6;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_digest_256dc4d88752a2b798517347adc6bf9a;
        tmp_globals_name_5 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_EditingMode_tuple;
        tmp_level_name_5 = const_int_0;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 12;
        tmp_import_name_from_6 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_EditingMode );
        Py_DECREF( tmp_import_name_from_6 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 12;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_EditingMode, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_7;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_digest_908c8ae8a1ea88b9446eaa47eb000fad;
        tmp_globals_name_6 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_KeyPress_tuple;
        tmp_level_name_6 = const_int_0;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 13;
        tmp_import_name_from_7 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_KeyPress );
        Py_DECREF( tmp_import_name_from_7 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 13;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_KeyPress, tmp_assign_source_12 );
    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_8;
        PyObject *tmp_name_name_7;
        PyObject *tmp_globals_name_7;
        PyObject *tmp_locals_name_7;
        PyObject *tmp_fromlist_name_7;
        PyObject *tmp_level_name_7;
        tmp_name_name_7 = const_str_digest_6695b0a823c8cd6b75b68a2528261696;
        tmp_globals_name_7 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_7 = Py_None;
        tmp_fromlist_name_7 = const_tuple_str_plain_key_binding_tuple;
        tmp_level_name_7 = const_int_0;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 14;
        tmp_import_name_from_8 = IMPORT_MODULE5( tmp_name_name_7, tmp_globals_name_7, tmp_locals_name_7, tmp_fromlist_name_7, tmp_level_name_7 );
        if ( tmp_import_name_from_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_key_binding );
        Py_DECREF( tmp_import_name_from_8 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 14;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_key_binding, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_9;
        PyObject *tmp_name_name_8;
        PyObject *tmp_globals_name_8;
        PyObject *tmp_locals_name_8;
        PyObject *tmp_fromlist_name_8;
        PyObject *tmp_level_name_8;
        tmp_name_name_8 = const_str_digest_b7cd7d3061b29ef6badd990d7c83ddb1;
        tmp_globals_name_8 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_8 = Py_None;
        tmp_fromlist_name_8 = const_tuple_str_plain_Keys_tuple;
        tmp_level_name_8 = const_int_0;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 15;
        tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_8, tmp_globals_name_8, tmp_locals_name_8, tmp_fromlist_name_8, tmp_level_name_8 );
        if ( tmp_import_name_from_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_Keys );
        Py_DECREF( tmp_import_name_from_9 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 15;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_Keys, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_10;
        PyObject *tmp_name_name_9;
        PyObject *tmp_globals_name_9;
        PyObject *tmp_locals_name_9;
        PyObject *tmp_fromlist_name_9;
        PyObject *tmp_level_name_9;
        tmp_name_name_9 = const_str_digest_0a70b85b2fe4fc5624d81d2e6686c384;
        tmp_globals_name_9 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_9 = Py_None;
        tmp_fromlist_name_9 = const_tuple_str_plain_SearchDirection_tuple;
        tmp_level_name_9 = const_int_0;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 16;
        tmp_import_name_from_10 = IMPORT_MODULE5( tmp_name_name_9, tmp_globals_name_9, tmp_locals_name_9, tmp_fromlist_name_9, tmp_level_name_9 );
        if ( tmp_import_name_from_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_SearchDirection );
        Py_DECREF( tmp_import_name_from_10 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 16;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_SearchDirection, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_11;
        PyObject *tmp_name_name_10;
        PyObject *tmp_globals_name_10;
        PyObject *tmp_locals_name_10;
        PyObject *tmp_fromlist_name_10;
        PyObject *tmp_level_name_10;
        tmp_name_name_10 = const_str_digest_a04f324144c20908fd5b501ea5e5f924;
        tmp_globals_name_10 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$named_commands;
        tmp_locals_name_10 = Py_None;
        tmp_fromlist_name_10 = const_tuple_str_plain_PasteMode_tuple;
        tmp_level_name_10 = const_int_0;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 17;
        tmp_import_name_from_11 = IMPORT_MODULE5( tmp_name_name_10, tmp_globals_name_10, tmp_locals_name_10, tmp_fromlist_name_10, tmp_level_name_10 );
        if ( tmp_import_name_from_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_PasteMode );
        Py_DECREF( tmp_import_name_from_11 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 17;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_PasteMode, tmp_assign_source_16 );
    }
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = LIST_COPY( const_list_str_plain_get_by_name_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = PyDict_New();
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain__readline_commands, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_1_register(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register, tmp_assign_source_19 );
    }
    {
        PyObject *tmp_assign_source_20;
        tmp_assign_source_20 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_2_get_by_name(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_get_by_name, tmp_assign_source_20 );
    }
    {
        PyObject *tmp_assign_source_21;
        PyObject *tmp_called_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_3;
        PyObject *tmp_args_element_name_1;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        CHECK_OBJECT( tmp_mvar_value_3 );
        tmp_called_name_2 = tmp_mvar_value_3;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 56;
        tmp_called_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, &PyTuple_GET_ITEM( const_tuple_str_digest_637ae2db1c15757d3f68020910fc7af5_tuple, 0 ) );

        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_1 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_3_beginning_of_line(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 56;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 56;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_beginning_of_line, tmp_assign_source_21 );
    }
    {
        PyObject *tmp_assign_source_22;
        PyObject *tmp_called_name_3;
        PyObject *tmp_called_name_4;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_2;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_4 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 63;

            goto frame_exception_exit_1;
        }

        tmp_called_name_4 = tmp_mvar_value_4;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 63;
        tmp_called_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, &PyTuple_GET_ITEM( const_tuple_str_digest_4dcf85ba674584b7335275c899c0ab24_tuple, 0 ) );

        if ( tmp_called_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_4_end_of_line(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 63;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_assign_source_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, call_args );
        }

        Py_DECREF( tmp_called_name_3 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_assign_source_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 63;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_end_of_line, tmp_assign_source_22 );
    }
    {
        PyObject *tmp_assign_source_23;
        PyObject *tmp_called_name_5;
        PyObject *tmp_called_name_6;
        PyObject *tmp_mvar_value_5;
        PyObject *tmp_args_element_name_3;
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_5 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 70;

            goto frame_exception_exit_1;
        }

        tmp_called_name_6 = tmp_mvar_value_5;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 70;
        tmp_called_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, &PyTuple_GET_ITEM( const_tuple_str_digest_820ffb9c299a4fe53ab8241c74e4288a_tuple, 0 ) );

        if ( tmp_called_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_3 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_5_forward_char(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 70;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_assign_source_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, call_args );
        }

        Py_DECREF( tmp_called_name_5 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_assign_source_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 70;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_forward_char, tmp_assign_source_23 );
    }
    {
        PyObject *tmp_assign_source_24;
        PyObject *tmp_called_name_7;
        PyObject *tmp_called_name_8;
        PyObject *tmp_mvar_value_6;
        PyObject *tmp_args_element_name_4;
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_6 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 77;

            goto frame_exception_exit_1;
        }

        tmp_called_name_8 = tmp_mvar_value_6;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 77;
        tmp_called_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, &PyTuple_GET_ITEM( const_tuple_str_digest_21e45c1f878d59fe84fced59f3e10b63_tuple, 0 ) );

        if ( tmp_called_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_4 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_6_backward_char(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 77;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_assign_source_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, call_args );
        }

        Py_DECREF( tmp_called_name_7 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_assign_source_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 77;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_backward_char, tmp_assign_source_24 );
    }
    {
        PyObject *tmp_assign_source_25;
        PyObject *tmp_called_name_9;
        PyObject *tmp_called_name_10;
        PyObject *tmp_mvar_value_7;
        PyObject *tmp_args_element_name_5;
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_7 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 84;

            goto frame_exception_exit_1;
        }

        tmp_called_name_10 = tmp_mvar_value_7;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 84;
        tmp_called_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, &PyTuple_GET_ITEM( const_tuple_str_digest_1e115ca000973c4222f04cf175132c7e_tuple, 0 ) );

        if ( tmp_called_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_5 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_7_forward_word(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 84;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_assign_source_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, call_args );
        }

        Py_DECREF( tmp_called_name_9 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_assign_source_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 84;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_forward_word, tmp_assign_source_25 );
    }
    {
        PyObject *tmp_assign_source_26;
        PyObject *tmp_called_name_11;
        PyObject *tmp_called_name_12;
        PyObject *tmp_mvar_value_8;
        PyObject *tmp_args_element_name_6;
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_8 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 97;

            goto frame_exception_exit_1;
        }

        tmp_called_name_12 = tmp_mvar_value_8;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 97;
        tmp_called_name_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, &PyTuple_GET_ITEM( const_tuple_str_digest_23b6565e2fad8b5df75562a1b722c5fa_tuple, 0 ) );

        if ( tmp_called_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_6 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_8_backward_word(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_assign_source_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, call_args );
        }

        Py_DECREF( tmp_called_name_11 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_assign_source_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 97;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_backward_word, tmp_assign_source_26 );
    }
    {
        PyObject *tmp_assign_source_27;
        PyObject *tmp_called_name_13;
        PyObject *tmp_called_name_14;
        PyObject *tmp_mvar_value_9;
        PyObject *tmp_args_element_name_7;
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_9 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 110;

            goto frame_exception_exit_1;
        }

        tmp_called_name_14 = tmp_mvar_value_9;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 110;
        tmp_called_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, &PyTuple_GET_ITEM( const_tuple_str_digest_4f8c7230e3d1a51c6fa37895ee70f054_tuple, 0 ) );

        if ( tmp_called_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_7 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_9_clear_screen(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 110;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_assign_source_27 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, call_args );
        }

        Py_DECREF( tmp_called_name_13 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_assign_source_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 110;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_clear_screen, tmp_assign_source_27 );
    }
    {
        PyObject *tmp_assign_source_28;
        PyObject *tmp_called_name_15;
        PyObject *tmp_called_name_16;
        PyObject *tmp_mvar_value_10;
        PyObject *tmp_args_element_name_8;
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_10 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;

            goto frame_exception_exit_1;
        }

        tmp_called_name_16 = tmp_mvar_value_10;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 118;
        tmp_called_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, &PyTuple_GET_ITEM( const_tuple_str_digest_400d834c6f1d5d5d7b9185420395cb8b_tuple, 0 ) );

        if ( tmp_called_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_8 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_10_redraw_current_line(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_assign_source_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, call_args );
        }

        Py_DECREF( tmp_called_name_15 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_assign_source_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_redraw_current_line, tmp_assign_source_28 );
    }
    {
        PyObject *tmp_assign_source_29;
        PyObject *tmp_called_name_17;
        PyObject *tmp_called_name_18;
        PyObject *tmp_mvar_value_11;
        PyObject *tmp_args_element_name_9;
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_11 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;

            goto frame_exception_exit_1;
        }

        tmp_called_name_18 = tmp_mvar_value_11;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 132;
        tmp_called_name_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, &PyTuple_GET_ITEM( const_tuple_str_digest_bc52841ed7d0c0e991090611ebb55c62_tuple, 0 ) );

        if ( tmp_called_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_9 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_11_accept_line(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 132;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_assign_source_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, call_args );
        }

        Py_DECREF( tmp_called_name_17 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_assign_source_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 132;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_accept_line, tmp_assign_source_29 );
    }
    {
        PyObject *tmp_assign_source_30;
        PyObject *tmp_called_name_19;
        PyObject *tmp_called_name_20;
        PyObject *tmp_mvar_value_12;
        PyObject *tmp_args_element_name_10;
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_12 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 138;

            goto frame_exception_exit_1;
        }

        tmp_called_name_20 = tmp_mvar_value_12;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 138;
        tmp_called_name_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, &PyTuple_GET_ITEM( const_tuple_str_digest_af1adbb7bec7eaa2f6c08a25f5ab2b5d_tuple, 0 ) );

        if ( tmp_called_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_10 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_12_previous_history(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 138;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_assign_source_30 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, call_args );
        }

        Py_DECREF( tmp_called_name_19 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_assign_source_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 138;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_previous_history, tmp_assign_source_30 );
    }
    {
        PyObject *tmp_assign_source_31;
        PyObject *tmp_called_name_21;
        PyObject *tmp_called_name_22;
        PyObject *tmp_mvar_value_13;
        PyObject *tmp_args_element_name_11;
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_13 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 144;

            goto frame_exception_exit_1;
        }

        tmp_called_name_22 = tmp_mvar_value_13;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 144;
        tmp_called_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, &PyTuple_GET_ITEM( const_tuple_str_digest_ffa4782c58075c6dd84b572edd3109a7_tuple, 0 ) );

        if ( tmp_called_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_11 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_13_next_history(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 144;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_assign_source_31 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, call_args );
        }

        Py_DECREF( tmp_called_name_21 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_assign_source_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 144;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_next_history, tmp_assign_source_31 );
    }
    {
        PyObject *tmp_assign_source_32;
        PyObject *tmp_called_name_23;
        PyObject *tmp_called_name_24;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_args_element_name_12;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_14 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 150;

            goto frame_exception_exit_1;
        }

        tmp_called_name_24 = tmp_mvar_value_14;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 150;
        tmp_called_name_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, &PyTuple_GET_ITEM( const_tuple_str_digest_4e5a5ed7c51e9863d1e36202b87fe43e_tuple, 0 ) );

        if ( tmp_called_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_12 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_14_beginning_of_history(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 150;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_assign_source_32 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, call_args );
        }

        Py_DECREF( tmp_called_name_23 );
        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_assign_source_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 150;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_beginning_of_history, tmp_assign_source_32 );
    }
    {
        PyObject *tmp_assign_source_33;
        PyObject *tmp_called_name_25;
        PyObject *tmp_called_name_26;
        PyObject *tmp_mvar_value_15;
        PyObject *tmp_args_element_name_13;
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_15 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 156;

            goto frame_exception_exit_1;
        }

        tmp_called_name_26 = tmp_mvar_value_15;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 156;
        tmp_called_name_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, &PyTuple_GET_ITEM( const_tuple_str_digest_70508f93fca682b73b6276fb31ac0cad_tuple, 0 ) );

        if ( tmp_called_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_13 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_15_end_of_history(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 156;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_assign_source_33 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, call_args );
        }

        Py_DECREF( tmp_called_name_25 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_assign_source_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 156;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_end_of_history, tmp_assign_source_33 );
    }
    {
        PyObject *tmp_assign_source_34;
        PyObject *tmp_called_name_27;
        PyObject *tmp_called_name_28;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_args_element_name_14;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_16 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 166;

            goto frame_exception_exit_1;
        }

        tmp_called_name_28 = tmp_mvar_value_16;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 166;
        tmp_called_name_27 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, &PyTuple_GET_ITEM( const_tuple_str_digest_4650e344edbd2542c4421b2b140d04da_tuple, 0 ) );

        if ( tmp_called_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_14 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_16_reverse_search_history(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 166;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_assign_source_34 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, call_args );
        }

        Py_DECREF( tmp_called_name_27 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_assign_source_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_reverse_search_history, tmp_assign_source_34 );
    }
    {
        PyObject *tmp_assign_source_35;
        PyObject *tmp_called_name_29;
        PyObject *tmp_called_name_30;
        PyObject *tmp_mvar_value_17;
        PyObject *tmp_args_element_name_15;
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_17 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 183;

            goto frame_exception_exit_1;
        }

        tmp_called_name_30 = tmp_mvar_value_17;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 183;
        tmp_called_name_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_30, &PyTuple_GET_ITEM( const_tuple_str_digest_94831ee4810e73b56026b83acc214eaf_tuple, 0 ) );

        if ( tmp_called_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_15 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_17_end_of_file(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 183;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_assign_source_35 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_29, call_args );
        }

        Py_DECREF( tmp_called_name_29 );
        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_assign_source_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 183;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_end_of_file, tmp_assign_source_35 );
    }
    {
        PyObject *tmp_assign_source_36;
        PyObject *tmp_called_name_31;
        PyObject *tmp_called_name_32;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_args_element_name_16;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_18 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 191;

            goto frame_exception_exit_1;
        }

        tmp_called_name_32 = tmp_mvar_value_18;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 191;
        tmp_called_name_31 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_32, &PyTuple_GET_ITEM( const_tuple_str_digest_c1dec28d38befd226a053f4c881a493b_tuple, 0 ) );

        if ( tmp_called_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_16 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_18_delete_char(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 191;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_assign_source_36 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_31, call_args );
        }

        Py_DECREF( tmp_called_name_31 );
        Py_DECREF( tmp_args_element_name_16 );
        if ( tmp_assign_source_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 191;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_delete_char, tmp_assign_source_36 );
    }
    {
        PyObject *tmp_assign_source_37;
        PyObject *tmp_called_name_33;
        PyObject *tmp_called_name_34;
        PyObject *tmp_mvar_value_19;
        PyObject *tmp_args_element_name_17;
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_19 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 199;

            goto frame_exception_exit_1;
        }

        tmp_called_name_34 = tmp_mvar_value_19;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 199;
        tmp_called_name_33 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_34, &PyTuple_GET_ITEM( const_tuple_str_digest_647f11624bc526afd8ce034dc78066a1_tuple, 0 ) );

        if ( tmp_called_name_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_17 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_19_backward_delete_char(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 199;
        {
            PyObject *call_args[] = { tmp_args_element_name_17 };
            tmp_assign_source_37 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_33, call_args );
        }

        Py_DECREF( tmp_called_name_33 );
        Py_DECREF( tmp_args_element_name_17 );
        if ( tmp_assign_source_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 199;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_backward_delete_char, tmp_assign_source_37 );
    }
    {
        PyObject *tmp_assign_source_38;
        PyObject *tmp_called_name_35;
        PyObject *tmp_called_name_36;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_args_element_name_18;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 213;

            goto frame_exception_exit_1;
        }

        tmp_called_name_36 = tmp_mvar_value_20;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 213;
        tmp_called_name_35 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_36, &PyTuple_GET_ITEM( const_tuple_str_digest_204c920806e032799a87379e1294bcfc_tuple, 0 ) );

        if ( tmp_called_name_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_18 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_20_self_insert(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 213;
        {
            PyObject *call_args[] = { tmp_args_element_name_18 };
            tmp_assign_source_38 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_35, call_args );
        }

        Py_DECREF( tmp_called_name_35 );
        Py_DECREF( tmp_args_element_name_18 );
        if ( tmp_assign_source_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_self_insert, tmp_assign_source_38 );
    }
    {
        PyObject *tmp_assign_source_39;
        PyObject *tmp_called_name_37;
        PyObject *tmp_called_name_38;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_args_element_name_19;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_21 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 219;

            goto frame_exception_exit_1;
        }

        tmp_called_name_38 = tmp_mvar_value_21;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 219;
        tmp_called_name_37 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_38, &PyTuple_GET_ITEM( const_tuple_str_digest_d2a5158fd7bb2018f91aee17fcfed439_tuple, 0 ) );

        if ( tmp_called_name_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_19 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_21_transpose_chars(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 219;
        {
            PyObject *call_args[] = { tmp_args_element_name_19 };
            tmp_assign_source_39 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_37, call_args );
        }

        Py_DECREF( tmp_called_name_37 );
        Py_DECREF( tmp_args_element_name_19 );
        if ( tmp_assign_source_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 219;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_transpose_chars, tmp_assign_source_39 );
    }
    {
        PyObject *tmp_assign_source_40;
        PyObject *tmp_called_name_39;
        PyObject *tmp_called_name_40;
        PyObject *tmp_mvar_value_22;
        PyObject *tmp_args_element_name_20;
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_22 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 238;

            goto frame_exception_exit_1;
        }

        tmp_called_name_40 = tmp_mvar_value_22;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 238;
        tmp_called_name_39 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_40, &PyTuple_GET_ITEM( const_tuple_str_digest_e13b285e26a38d1a90b94ccc0668450c_tuple, 0 ) );

        if ( tmp_called_name_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_20 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_22_uppercase_word(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 238;
        {
            PyObject *call_args[] = { tmp_args_element_name_20 };
            tmp_assign_source_40 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_39, call_args );
        }

        Py_DECREF( tmp_called_name_39 );
        Py_DECREF( tmp_args_element_name_20 );
        if ( tmp_assign_source_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 238;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_uppercase_word, tmp_assign_source_40 );
    }
    {
        PyObject *tmp_assign_source_41;
        PyObject *tmp_called_name_41;
        PyObject *tmp_called_name_42;
        PyObject *tmp_mvar_value_23;
        PyObject *tmp_args_element_name_21;
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_23 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 251;

            goto frame_exception_exit_1;
        }

        tmp_called_name_42 = tmp_mvar_value_23;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 251;
        tmp_called_name_41 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_42, &PyTuple_GET_ITEM( const_tuple_str_digest_3e8aa0ff62897226c7230752efac5de8_tuple, 0 ) );

        if ( tmp_called_name_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_21 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_23_downcase_word(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 251;
        {
            PyObject *call_args[] = { tmp_args_element_name_21 };
            tmp_assign_source_41 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_41, call_args );
        }

        Py_DECREF( tmp_called_name_41 );
        Py_DECREF( tmp_args_element_name_21 );
        if ( tmp_assign_source_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 251;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_downcase_word, tmp_assign_source_41 );
    }
    {
        PyObject *tmp_assign_source_42;
        PyObject *tmp_called_name_43;
        PyObject *tmp_called_name_44;
        PyObject *tmp_mvar_value_24;
        PyObject *tmp_args_element_name_22;
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_24 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 264;

            goto frame_exception_exit_1;
        }

        tmp_called_name_44 = tmp_mvar_value_24;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 264;
        tmp_called_name_43 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_44, &PyTuple_GET_ITEM( const_tuple_str_digest_4e2aeb083e5702a53bc119c9bb3ab9d7_tuple, 0 ) );

        if ( tmp_called_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 264;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_22 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_24_capitalize_word(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 264;
        {
            PyObject *call_args[] = { tmp_args_element_name_22 };
            tmp_assign_source_42 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_43, call_args );
        }

        Py_DECREF( tmp_called_name_43 );
        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_assign_source_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 264;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_capitalize_word, tmp_assign_source_42 );
    }
    {
        PyObject *tmp_assign_source_43;
        PyObject *tmp_called_name_45;
        PyObject *tmp_called_name_46;
        PyObject *tmp_mvar_value_25;
        PyObject *tmp_args_element_name_23;
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_25 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 277;

            goto frame_exception_exit_1;
        }

        tmp_called_name_46 = tmp_mvar_value_25;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 277;
        tmp_called_name_45 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_46, &PyTuple_GET_ITEM( const_tuple_str_digest_f8c276bab5a9ae14be8475d6e0e75e09_tuple, 0 ) );

        if ( tmp_called_name_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_23 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_25_quoted_insert(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 277;
        {
            PyObject *call_args[] = { tmp_args_element_name_23 };
            tmp_assign_source_43 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_45, call_args );
        }

        Py_DECREF( tmp_called_name_45 );
        Py_DECREF( tmp_args_element_name_23 );
        if ( tmp_assign_source_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 277;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_quoted_insert, tmp_assign_source_43 );
    }
    {
        PyObject *tmp_assign_source_44;
        PyObject *tmp_called_name_47;
        PyObject *tmp_called_name_48;
        PyObject *tmp_mvar_value_26;
        PyObject *tmp_args_element_name_24;
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_26 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 290;

            goto frame_exception_exit_1;
        }

        tmp_called_name_48 = tmp_mvar_value_26;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 290;
        tmp_called_name_47 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_48, &PyTuple_GET_ITEM( const_tuple_str_digest_f6826d177b892c516eb853c8574210e2_tuple, 0 ) );

        if ( tmp_called_name_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 290;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_24 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_26_kill_line(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 290;
        {
            PyObject *call_args[] = { tmp_args_element_name_24 };
            tmp_assign_source_44 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_47, call_args );
        }

        Py_DECREF( tmp_called_name_47 );
        Py_DECREF( tmp_args_element_name_24 );
        if ( tmp_assign_source_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 290;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_kill_line, tmp_assign_source_44 );
    }
    {
        PyObject *tmp_assign_source_45;
        PyObject *tmp_called_name_49;
        PyObject *tmp_called_name_50;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_args_element_name_25;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_27 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 310;

            goto frame_exception_exit_1;
        }

        tmp_called_name_50 = tmp_mvar_value_27;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 310;
        tmp_called_name_49 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_50, &PyTuple_GET_ITEM( const_tuple_str_digest_d306eb7c71f9186853263120b43be12e_tuple, 0 ) );

        if ( tmp_called_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_25 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_27_kill_word(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 310;
        {
            PyObject *call_args[] = { tmp_args_element_name_25 };
            tmp_assign_source_45 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_49, call_args );
        }

        Py_DECREF( tmp_called_name_49 );
        Py_DECREF( tmp_args_element_name_25 );
        if ( tmp_assign_source_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 310;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_kill_word, tmp_assign_source_45 );
    }
    {
        PyObject *tmp_assign_source_46;
        PyObject *tmp_called_name_51;
        PyObject *tmp_called_name_52;
        PyObject *tmp_mvar_value_28;
        PyObject *tmp_args_element_name_26;
        PyObject *tmp_defaults_1;
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_28 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 328;

            goto frame_exception_exit_1;
        }

        tmp_called_name_52 = tmp_mvar_value_28;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 328;
        tmp_called_name_51 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_52, &PyTuple_GET_ITEM( const_tuple_str_digest_8ac25a148822dcf205845c372c142dc5_tuple, 0 ) );

        if ( tmp_called_name_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 328;

            goto frame_exception_exit_1;
        }
        tmp_defaults_1 = const_tuple_true_tuple;
        Py_INCREF( tmp_defaults_1 );
        tmp_args_element_name_26 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_28_unix_word_rubout( tmp_defaults_1 );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 328;
        {
            PyObject *call_args[] = { tmp_args_element_name_26 };
            tmp_assign_source_46 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_51, call_args );
        }

        Py_DECREF( tmp_called_name_51 );
        Py_DECREF( tmp_args_element_name_26 );
        if ( tmp_assign_source_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 328;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_unix_word_rubout, tmp_assign_source_46 );
    }
    {
        PyObject *tmp_assign_source_47;
        PyObject *tmp_called_name_53;
        PyObject *tmp_called_name_54;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_args_element_name_27;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_29 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 357;

            goto frame_exception_exit_1;
        }

        tmp_called_name_54 = tmp_mvar_value_29;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 357;
        tmp_called_name_53 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_54, &PyTuple_GET_ITEM( const_tuple_str_digest_10c978016ae019e97a4da8b4078edf46_tuple, 0 ) );

        if ( tmp_called_name_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_27 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_29_backward_kill_word(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 357;
        {
            PyObject *call_args[] = { tmp_args_element_name_27 };
            tmp_assign_source_47 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_53, call_args );
        }

        Py_DECREF( tmp_called_name_53 );
        Py_DECREF( tmp_args_element_name_27 );
        if ( tmp_assign_source_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 357;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_backward_kill_word, tmp_assign_source_47 );
    }
    {
        PyObject *tmp_assign_source_48;
        PyObject *tmp_called_name_55;
        PyObject *tmp_called_name_56;
        PyObject *tmp_mvar_value_30;
        PyObject *tmp_args_element_name_28;
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_30 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 366;

            goto frame_exception_exit_1;
        }

        tmp_called_name_56 = tmp_mvar_value_30;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 366;
        tmp_called_name_55 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_56, &PyTuple_GET_ITEM( const_tuple_str_digest_4d94ac19d21ce83f39e74e91d067ed52_tuple, 0 ) );

        if ( tmp_called_name_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_28 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_30_delete_horizontal_space(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 366;
        {
            PyObject *call_args[] = { tmp_args_element_name_28 };
            tmp_assign_source_48 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_55, call_args );
        }

        Py_DECREF( tmp_called_name_55 );
        Py_DECREF( tmp_args_element_name_28 );
        if ( tmp_assign_source_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 366;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_delete_horizontal_space, tmp_assign_source_48 );
    }
    {
        PyObject *tmp_assign_source_49;
        PyObject *tmp_called_name_57;
        PyObject *tmp_called_name_58;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_args_element_name_29;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_31 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 380;

            goto frame_exception_exit_1;
        }

        tmp_called_name_58 = tmp_mvar_value_31;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 380;
        tmp_called_name_57 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_58, &PyTuple_GET_ITEM( const_tuple_str_digest_fe8d6ea5daee2fa16a6d6e2bae6159bb_tuple, 0 ) );

        if ( tmp_called_name_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 380;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_29 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_31_unix_line_discard(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 380;
        {
            PyObject *call_args[] = { tmp_args_element_name_29 };
            tmp_assign_source_49 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_57, call_args );
        }

        Py_DECREF( tmp_called_name_57 );
        Py_DECREF( tmp_args_element_name_29 );
        if ( tmp_assign_source_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 380;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_unix_line_discard, tmp_assign_source_49 );
    }
    {
        PyObject *tmp_assign_source_50;
        PyObject *tmp_called_name_59;
        PyObject *tmp_called_name_60;
        PyObject *tmp_mvar_value_32;
        PyObject *tmp_args_element_name_30;
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_32 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 394;

            goto frame_exception_exit_1;
        }

        tmp_called_name_60 = tmp_mvar_value_32;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 394;
        tmp_called_name_59 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_60, &PyTuple_GET_ITEM( const_tuple_str_plain_yank_tuple, 0 ) );

        if ( tmp_called_name_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 394;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_30 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_32_yank(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 394;
        {
            PyObject *call_args[] = { tmp_args_element_name_30 };
            tmp_assign_source_50 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_59, call_args );
        }

        Py_DECREF( tmp_called_name_59 );
        Py_DECREF( tmp_args_element_name_30 );
        if ( tmp_assign_source_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 394;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_yank, tmp_assign_source_50 );
    }
    {
        PyObject *tmp_assign_source_51;
        PyObject *tmp_called_name_61;
        PyObject *tmp_called_name_62;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_args_element_name_31;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_33 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 403;

            goto frame_exception_exit_1;
        }

        tmp_called_name_62 = tmp_mvar_value_33;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 403;
        tmp_called_name_61 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_62, &PyTuple_GET_ITEM( const_tuple_str_digest_bec9411c13b58be3a688ffe0c42ba2da_tuple, 0 ) );

        if ( tmp_called_name_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 403;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_31 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_33_yank_nth_arg(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 403;
        {
            PyObject *call_args[] = { tmp_args_element_name_31 };
            tmp_assign_source_51 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_61, call_args );
        }

        Py_DECREF( tmp_called_name_61 );
        Py_DECREF( tmp_args_element_name_31 );
        if ( tmp_assign_source_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 403;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_yank_nth_arg, tmp_assign_source_51 );
    }
    {
        PyObject *tmp_assign_source_52;
        PyObject *tmp_called_name_63;
        PyObject *tmp_called_name_64;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_args_element_name_32;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_34 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 413;

            goto frame_exception_exit_1;
        }

        tmp_called_name_64 = tmp_mvar_value_34;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 413;
        tmp_called_name_63 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_64, &PyTuple_GET_ITEM( const_tuple_str_digest_c8c3694ea6fe9becb903618b30b45e7b_tuple, 0 ) );

        if ( tmp_called_name_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 413;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_32 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_34_yank_last_arg(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 413;
        {
            PyObject *call_args[] = { tmp_args_element_name_32 };
            tmp_assign_source_52 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_63, call_args );
        }

        Py_DECREF( tmp_called_name_63 );
        Py_DECREF( tmp_args_element_name_32 );
        if ( tmp_assign_source_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 413;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_yank_last_arg, tmp_assign_source_52 );
    }
    {
        PyObject *tmp_assign_source_53;
        PyObject *tmp_called_name_65;
        PyObject *tmp_called_name_66;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_args_element_name_33;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_35 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 423;

            goto frame_exception_exit_1;
        }

        tmp_called_name_66 = tmp_mvar_value_35;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 423;
        tmp_called_name_65 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_66, &PyTuple_GET_ITEM( const_tuple_str_digest_0adea339c095a653a19ec3c66bfd3784_tuple, 0 ) );

        if ( tmp_called_name_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 423;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_33 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_35_yank_pop(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 423;
        {
            PyObject *call_args[] = { tmp_args_element_name_33 };
            tmp_assign_source_53 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_65, call_args );
        }

        Py_DECREF( tmp_called_name_65 );
        Py_DECREF( tmp_args_element_name_33 );
        if ( tmp_assign_source_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 423;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_yank_pop, tmp_assign_source_53 );
    }
    {
        PyObject *tmp_assign_source_54;
        PyObject *tmp_called_name_67;
        PyObject *tmp_called_name_68;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_args_element_name_34;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_36 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 444;

            goto frame_exception_exit_1;
        }

        tmp_called_name_68 = tmp_mvar_value_36;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 444;
        tmp_called_name_67 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_68, &PyTuple_GET_ITEM( const_tuple_str_plain_complete_tuple, 0 ) );

        if ( tmp_called_name_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 444;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_34 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_36_complete(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 444;
        {
            PyObject *call_args[] = { tmp_args_element_name_34 };
            tmp_assign_source_54 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_67, call_args );
        }

        Py_DECREF( tmp_called_name_67 );
        Py_DECREF( tmp_args_element_name_34 );
        if ( tmp_assign_source_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 444;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_complete, tmp_assign_source_54 );
    }
    {
        PyObject *tmp_assign_source_55;
        PyObject *tmp_called_name_69;
        PyObject *tmp_called_name_70;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_args_element_name_35;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_37 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 450;

            goto frame_exception_exit_1;
        }

        tmp_called_name_70 = tmp_mvar_value_37;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 450;
        tmp_called_name_69 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_70, &PyTuple_GET_ITEM( const_tuple_str_digest_2341ccadcc0716db92a6611c3a0c5709_tuple, 0 ) );

        if ( tmp_called_name_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 450;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_35 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_37_menu_complete(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 450;
        {
            PyObject *call_args[] = { tmp_args_element_name_35 };
            tmp_assign_source_55 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_69, call_args );
        }

        Py_DECREF( tmp_called_name_69 );
        Py_DECREF( tmp_args_element_name_35 );
        if ( tmp_assign_source_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 450;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_menu_complete, tmp_assign_source_55 );
    }
    {
        PyObject *tmp_assign_source_56;
        PyObject *tmp_called_name_71;
        PyObject *tmp_called_name_72;
        PyObject *tmp_mvar_value_38;
        PyObject *tmp_args_element_name_36;
        tmp_mvar_value_38 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_38 == NULL ))
        {
            tmp_mvar_value_38 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_38 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 459;

            goto frame_exception_exit_1;
        }

        tmp_called_name_72 = tmp_mvar_value_38;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 459;
        tmp_called_name_71 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_72, &PyTuple_GET_ITEM( const_tuple_str_digest_7e022856dd1b5bd6663571c7df813303_tuple, 0 ) );

        if ( tmp_called_name_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 459;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_36 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_38_menu_complete_backward(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 459;
        {
            PyObject *call_args[] = { tmp_args_element_name_36 };
            tmp_assign_source_56 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_71, call_args );
        }

        Py_DECREF( tmp_called_name_71 );
        Py_DECREF( tmp_args_element_name_36 );
        if ( tmp_assign_source_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 459;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_menu_complete_backward, tmp_assign_source_56 );
    }
    {
        PyObject *tmp_assign_source_57;
        PyObject *tmp_called_name_73;
        PyObject *tmp_called_name_74;
        PyObject *tmp_mvar_value_39;
        PyObject *tmp_args_element_name_37;
        tmp_mvar_value_39 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_39 == NULL ))
        {
            tmp_mvar_value_39 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_39 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 469;

            goto frame_exception_exit_1;
        }

        tmp_called_name_74 = tmp_mvar_value_39;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 469;
        tmp_called_name_73 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_74, &PyTuple_GET_ITEM( const_tuple_str_digest_402d95161f3800c90c59735f4d8c4c59_tuple, 0 ) );

        if ( tmp_called_name_73 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 469;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_37 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_39_start_kbd_macro(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 469;
        {
            PyObject *call_args[] = { tmp_args_element_name_37 };
            tmp_assign_source_57 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_73, call_args );
        }

        Py_DECREF( tmp_called_name_73 );
        Py_DECREF( tmp_args_element_name_37 );
        if ( tmp_assign_source_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 469;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_start_kbd_macro, tmp_assign_source_57 );
    }
    {
        PyObject *tmp_assign_source_58;
        PyObject *tmp_called_name_75;
        PyObject *tmp_called_name_76;
        PyObject *tmp_mvar_value_40;
        PyObject *tmp_args_element_name_38;
        tmp_mvar_value_40 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_40 == NULL ))
        {
            tmp_mvar_value_40 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_40 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 477;

            goto frame_exception_exit_1;
        }

        tmp_called_name_76 = tmp_mvar_value_40;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 477;
        tmp_called_name_75 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_76, &PyTuple_GET_ITEM( const_tuple_str_digest_c014cbade4743da3cd8e8b1fadea68a6_tuple, 0 ) );

        if ( tmp_called_name_75 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 477;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_38 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_40_end_kbd_macro(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 477;
        {
            PyObject *call_args[] = { tmp_args_element_name_38 };
            tmp_assign_source_58 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_75, call_args );
        }

        Py_DECREF( tmp_called_name_75 );
        Py_DECREF( tmp_args_element_name_38 );
        if ( tmp_assign_source_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 477;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_end_kbd_macro, tmp_assign_source_58 );
    }
    {
        PyObject *tmp_assign_source_59;
        PyObject *tmp_called_name_77;
        PyObject *tmp_called_name_78;
        PyObject *tmp_mvar_value_41;
        PyObject *tmp_args_element_name_39;
        PyObject *tmp_called_name_79;
        PyObject *tmp_called_name_80;
        PyObject *tmp_mvar_value_42;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_args_element_name_40;
        tmp_mvar_value_41 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_41 == NULL ))
        {
            tmp_mvar_value_41 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_41 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 486;

            goto frame_exception_exit_1;
        }

        tmp_called_name_78 = tmp_mvar_value_41;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 486;
        tmp_called_name_77 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_78, &PyTuple_GET_ITEM( const_tuple_str_digest_ae692cb46ab2cf52c0be9c11d31b5318_tuple, 0 ) );

        if ( tmp_called_name_77 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 486;

            goto frame_exception_exit_1;
        }
        tmp_mvar_value_42 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_key_binding );

        if (unlikely( tmp_mvar_value_42 == NULL ))
        {
            tmp_mvar_value_42 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_key_binding );
        }

        if ( tmp_mvar_value_42 == NULL )
        {
            Py_DECREF( tmp_called_name_77 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "key_binding" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 487;

            goto frame_exception_exit_1;
        }

        tmp_called_name_80 = tmp_mvar_value_42;
        tmp_kw_name_1 = PyDict_Copy( const_dict_23a38ac95c6ae48450b8623c5e2b573e );
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 487;
        tmp_called_name_79 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_80, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_called_name_79 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_77 );

            exception_lineno = 487;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_40 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_41_call_last_kbd_macro(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 487;
        {
            PyObject *call_args[] = { tmp_args_element_name_40 };
            tmp_args_element_name_39 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_79, call_args );
        }

        Py_DECREF( tmp_called_name_79 );
        Py_DECREF( tmp_args_element_name_40 );
        if ( tmp_args_element_name_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_77 );

            exception_lineno = 487;

            goto frame_exception_exit_1;
        }
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 486;
        {
            PyObject *call_args[] = { tmp_args_element_name_39 };
            tmp_assign_source_59 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_77, call_args );
        }

        Py_DECREF( tmp_called_name_77 );
        Py_DECREF( tmp_args_element_name_39 );
        if ( tmp_assign_source_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 486;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_call_last_kbd_macro, tmp_assign_source_59 );
    }
    {
        PyObject *tmp_assign_source_60;
        PyObject *tmp_called_name_81;
        PyObject *tmp_called_name_82;
        PyObject *tmp_mvar_value_43;
        PyObject *tmp_args_element_name_41;
        tmp_mvar_value_43 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_43 == NULL ))
        {
            tmp_mvar_value_43 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_43 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 503;

            goto frame_exception_exit_1;
        }

        tmp_called_name_82 = tmp_mvar_value_43;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 503;
        tmp_called_name_81 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_82, &PyTuple_GET_ITEM( const_tuple_str_digest_5b1ffed3e4be8aa5f4cb3887c232c8e1_tuple, 0 ) );

        if ( tmp_called_name_81 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 503;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_41 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_42_print_last_kbd_macro(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 503;
        {
            PyObject *call_args[] = { tmp_args_element_name_41 };
            tmp_assign_source_60 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_81, call_args );
        }

        Py_DECREF( tmp_called_name_81 );
        Py_DECREF( tmp_args_element_name_41 );
        if ( tmp_assign_source_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 503;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_print_last_kbd_macro, tmp_assign_source_60 );
    }
    {
        PyObject *tmp_assign_source_61;
        PyObject *tmp_called_name_83;
        PyObject *tmp_called_name_84;
        PyObject *tmp_mvar_value_44;
        PyObject *tmp_args_element_name_42;
        tmp_mvar_value_44 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_44 == NULL ))
        {
            tmp_mvar_value_44 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_44 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 519;

            goto frame_exception_exit_1;
        }

        tmp_called_name_84 = tmp_mvar_value_44;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 519;
        tmp_called_name_83 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_84, &PyTuple_GET_ITEM( const_tuple_str_plain_undo_tuple, 0 ) );

        if ( tmp_called_name_83 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 519;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_42 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_43_undo(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 519;
        {
            PyObject *call_args[] = { tmp_args_element_name_42 };
            tmp_assign_source_61 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_83, call_args );
        }

        Py_DECREF( tmp_called_name_83 );
        Py_DECREF( tmp_args_element_name_42 );
        if ( tmp_assign_source_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 519;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_undo, tmp_assign_source_61 );
    }
    {
        PyObject *tmp_assign_source_62;
        PyObject *tmp_called_name_85;
        PyObject *tmp_called_name_86;
        PyObject *tmp_mvar_value_45;
        PyObject *tmp_args_element_name_43;
        tmp_mvar_value_45 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_45 == NULL ))
        {
            tmp_mvar_value_45 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_45 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 525;

            goto frame_exception_exit_1;
        }

        tmp_called_name_86 = tmp_mvar_value_45;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 525;
        tmp_called_name_85 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_86, &PyTuple_GET_ITEM( const_tuple_str_digest_fb5c11df9835340c3dd4a642c111b2eb_tuple, 0 ) );

        if ( tmp_called_name_85 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 525;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_43 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_44_insert_comment(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 525;
        {
            PyObject *call_args[] = { tmp_args_element_name_43 };
            tmp_assign_source_62 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_85, call_args );
        }

        Py_DECREF( tmp_called_name_85 );
        Py_DECREF( tmp_args_element_name_43 );
        if ( tmp_assign_source_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 525;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_insert_comment, tmp_assign_source_62 );
    }
    {
        PyObject *tmp_assign_source_63;
        PyObject *tmp_called_name_87;
        PyObject *tmp_called_name_88;
        PyObject *tmp_mvar_value_46;
        PyObject *tmp_args_element_name_44;
        tmp_mvar_value_46 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_46 == NULL ))
        {
            tmp_mvar_value_46 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_46 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 550;

            goto frame_exception_exit_1;
        }

        tmp_called_name_88 = tmp_mvar_value_46;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 550;
        tmp_called_name_87 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_88, &PyTuple_GET_ITEM( const_tuple_str_digest_e00c6f9ddf82e0ad26674bf1f89e79b3_tuple, 0 ) );

        if ( tmp_called_name_87 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 550;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_44 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_45_vi_editing_mode(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 550;
        {
            PyObject *call_args[] = { tmp_args_element_name_44 };
            tmp_assign_source_63 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_87, call_args );
        }

        Py_DECREF( tmp_called_name_87 );
        Py_DECREF( tmp_args_element_name_44 );
        if ( tmp_assign_source_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 550;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_vi_editing_mode, tmp_assign_source_63 );
    }
    {
        PyObject *tmp_assign_source_64;
        PyObject *tmp_called_name_89;
        PyObject *tmp_called_name_90;
        PyObject *tmp_mvar_value_47;
        PyObject *tmp_args_element_name_45;
        tmp_mvar_value_47 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_47 == NULL ))
        {
            tmp_mvar_value_47 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_47 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 556;

            goto frame_exception_exit_1;
        }

        tmp_called_name_90 = tmp_mvar_value_47;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 556;
        tmp_called_name_89 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_90, &PyTuple_GET_ITEM( const_tuple_str_digest_1bad627ed19ed28ad2101a569d58d565_tuple, 0 ) );

        if ( tmp_called_name_89 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 556;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_45 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_46_emacs_editing_mode(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 556;
        {
            PyObject *call_args[] = { tmp_args_element_name_45 };
            tmp_assign_source_64 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_89, call_args );
        }

        Py_DECREF( tmp_called_name_89 );
        Py_DECREF( tmp_args_element_name_45 );
        if ( tmp_assign_source_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 556;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_emacs_editing_mode, tmp_assign_source_64 );
    }
    {
        PyObject *tmp_assign_source_65;
        PyObject *tmp_called_name_91;
        PyObject *tmp_called_name_92;
        PyObject *tmp_mvar_value_48;
        PyObject *tmp_args_element_name_46;
        tmp_mvar_value_48 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_48 == NULL ))
        {
            tmp_mvar_value_48 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_48 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 562;

            goto frame_exception_exit_1;
        }

        tmp_called_name_92 = tmp_mvar_value_48;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 562;
        tmp_called_name_91 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_92, &PyTuple_GET_ITEM( const_tuple_str_digest_bff9c642660585f4a91209646bf69a4a_tuple, 0 ) );

        if ( tmp_called_name_91 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 562;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_46 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_47_prefix_meta(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 562;
        {
            PyObject *call_args[] = { tmp_args_element_name_46 };
            tmp_assign_source_65 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_91, call_args );
        }

        Py_DECREF( tmp_called_name_91 );
        Py_DECREF( tmp_args_element_name_46 );
        if ( tmp_assign_source_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 562;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_prefix_meta, tmp_assign_source_65 );
    }
    {
        PyObject *tmp_assign_source_66;
        PyObject *tmp_called_name_93;
        PyObject *tmp_called_name_94;
        PyObject *tmp_mvar_value_49;
        PyObject *tmp_args_element_name_47;
        tmp_mvar_value_49 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_49 == NULL ))
        {
            tmp_mvar_value_49 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_49 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 576;

            goto frame_exception_exit_1;
        }

        tmp_called_name_94 = tmp_mvar_value_49;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 576;
        tmp_called_name_93 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_94, &PyTuple_GET_ITEM( const_tuple_str_digest_d4d66fa4b26f35cc7cfe3fc57dce3f65_tuple, 0 ) );

        if ( tmp_called_name_93 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 576;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_47 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_48_operate_and_get_next(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 576;
        {
            PyObject *call_args[] = { tmp_args_element_name_47 };
            tmp_assign_source_66 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_93, call_args );
        }

        Py_DECREF( tmp_called_name_93 );
        Py_DECREF( tmp_args_element_name_47 );
        if ( tmp_assign_source_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 576;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_operate_and_get_next, tmp_assign_source_66 );
    }
    {
        PyObject *tmp_assign_source_67;
        PyObject *tmp_called_name_95;
        PyObject *tmp_called_name_96;
        PyObject *tmp_mvar_value_50;
        PyObject *tmp_args_element_name_48;
        tmp_mvar_value_50 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_register );

        if (unlikely( tmp_mvar_value_50 == NULL ))
        {
            tmp_mvar_value_50 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_register );
        }

        if ( tmp_mvar_value_50 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "register" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 597;

            goto frame_exception_exit_1;
        }

        tmp_called_name_96 = tmp_mvar_value_50;
        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 597;
        tmp_called_name_95 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_96, &PyTuple_GET_ITEM( const_tuple_str_digest_e19d832c39da60eea3d5c4896bf12fa2_tuple, 0 ) );

        if ( tmp_called_name_95 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 597;

            goto frame_exception_exit_1;
        }
        tmp_args_element_name_48 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$named_commands$$$function_49_edit_and_execute(  );



        frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame.f_lineno = 597;
        {
            PyObject *call_args[] = { tmp_args_element_name_48 };
            tmp_assign_source_67 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_95, call_args );
        }

        Py_DECREF( tmp_called_name_95 );
        Py_DECREF( tmp_args_element_name_48 );
        if ( tmp_assign_source_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 597;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$named_commands, (Nuitka_StringObject *)const_str_plain_edit_and_execute, tmp_assign_source_67 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c5fb6e65dc9f8f648efd26b8fac0a57 );
#endif
    popFrameStack();

    assertFrameObject( frame_8c5fb6e65dc9f8f648efd26b8fac0a57 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_8c5fb6e65dc9f8f648efd26b8fac0a57 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8c5fb6e65dc9f8f648efd26b8fac0a57, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8c5fb6e65dc9f8f648efd26b8fac0a57->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8c5fb6e65dc9f8f648efd26b8fac0a57, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;

    return MOD_RETURN_VALUE( module_prompt_toolkit$key_binding$bindings$named_commands );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
