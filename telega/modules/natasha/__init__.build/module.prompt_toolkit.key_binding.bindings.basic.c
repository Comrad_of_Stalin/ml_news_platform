/* Generated code for Python module 'prompt_toolkit.key_binding.bindings.basic'
 * created by Nuitka version 0.6.5
 *
 * This code is in part copyright 2019 Kay Hayen.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "nuitka/prelude.h"

#include "__helpers.h"

/* The "_module_prompt_toolkit$key_binding$bindings$basic" is a Python object pointer of module type.
 *
 * Note: For full compatibility with CPython, every module variable access
 * needs to go through it except for cases where the module cannot possibly
 * have changed in the mean time.
 */

PyObject *module_prompt_toolkit$key_binding$bindings$basic;
PyDictObject *moduledict_prompt_toolkit$key_binding$bindings$basic;

/* The declarations of module constants used, if any. */
extern PyObject *const_tuple_str_digest_d8156c92c3ac39743ec9345e59bfd2ed_tuple;
extern PyObject *const_str_plain_f5;
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_digest_c1dec28d38befd226a053f4c881a493b;
extern PyObject *const_tuple_str_digest_a71cd96de41b1d83d8e604317aa9a7eb_tuple;
extern PyObject *const_tuple_str_digest_2da2a9c575783ef454fa30e9f4e246ee_tuple;
static PyObject *const_tuple_str_digest_6dfa089e1785bfb099fb5067adfbc10c_tuple;
extern PyObject *const_str_digest_993bf0215ec53a0afc0447dbbdf085c9;
extern PyObject *const_tuple_str_digest_a969c3e8d69c0076a5175c71af8240d8_tuple;
extern PyObject *const_str_plain_enter;
extern PyObject *const_tuple_str_plain_end_tuple;
extern PyObject *const_str_plain_home;
extern PyObject *const_tuple_str_plain_KeyBindings_tuple;
static PyObject *const_tuple_str_digest_cccb566811e08cc4fd2e3ae406952507_tuple;
extern PyObject *const_tuple_str_digest_647f11624bc526afd8ce034dc78066a1_tuple;
extern PyObject *const_tuple_str_digest_3e8226f37423ae997480ac891b15fb8d_tuple;
extern PyObject *const_str_plain_arg;
extern PyObject *const_str_plain___file__;
extern PyObject *const_tuple_str_plain_event_tuple;
extern PyObject *const_tuple_str_chr_13_str_newline_tuple;
extern PyObject *const_str_digest_a71cd96de41b1d83d8e604317aa9a7eb;
static PyObject *const_str_digest_f0d13f3bfe27153ec821d14c89b14f7f;
extern PyObject *const_str_digest_6dfa089e1785bfb099fb5067adfbc10c;
extern PyObject *const_str_digest_4f98b413c851efc6e35345123bc23439;
extern PyObject *const_tuple_str_digest_594981de1deffed7357a6afed608966a_tuple;
extern PyObject *const_str_digest_a36417e3feea8b19ddc0146dc9edb326;
extern PyObject *const_str_plain_has_selection;
extern PyObject *const_str_digest_2341ccadcc0716db92a6611c3a0c5709;
extern PyObject *const_str_plain_load_basic_bindings;
extern PyObject *const_str_digest_204c920806e032799a87379e1294bcfc;
extern PyObject *const_tuple_str_plain_f8_tuple;
extern PyObject *const_str_digest_a969c3e8d69c0076a5175c71af8240d8;
extern PyObject *const_str_plain_insert_mode;
extern PyObject *const_tuple_str_plain_Keys_tuple;
extern PyObject *const_str_digest_af1adbb7bec7eaa2f6c08a25f5ab2b5d;
static PyObject *const_str_digest_9cccc676da4746edb9fb23677cfeea9d;
extern PyObject *const_tuple_str_digest_89633640ede9c2fabde46085a768738e_tuple;
extern PyObject *const_str_plain_f3;
static PyObject *const_str_digest_f7fef436b96330c714feb9f16a46dfa1;
static PyObject *const_tuple_str_plain_f20_tuple;
extern PyObject *const_str_plain_end;
extern PyObject *const_str_digest_7e022856dd1b5bd6663571c7df813303;
extern PyObject *const_str_plain_named_commands;
static PyObject *const_tuple_str_plain_f12_tuple;
static PyObject *const_tuple_str_digest_d759df6b8c71154cebd6df0ba1bf9b3f_tuple;
extern PyObject *const_str_plain_filter;
extern PyObject *const_tuple_str_digest_f2a75caf8ec802255b8dafde1721755d_tuple;
extern PyObject *const_str_plain_f18;
extern PyObject *const_str_digest_594981de1deffed7357a6afed608966a;
static PyObject *const_tuple_str_digest_e9a7688ceb262f6815aa65f8ee8703ab_tuple;
static PyObject *const_str_digest_96aecf7b4adb5c3dbb234466987ace12;
extern PyObject *const_str_digest_5bbf98712488fb50e0bf411a1625446e;
extern PyObject *const_str_digest_f6826d177b892c516eb853c8574210e2;
extern PyObject *const_str_digest_be077fe6fbb1013afb24e5934140597e;
extern PyObject *const_tuple_str_digest_4dcf85ba674584b7335275c899c0ab24_tuple;
extern PyObject *const_str_digest_4f8c7230e3d1a51c6fa37895ee70f054;
extern PyObject *const_str_plain___doc__;
extern PyObject *const_tuple_str_digest_4f8c7230e3d1a51c6fa37895ee70f054_tuple;
extern PyObject *const_tuple_str_plain_get_by_name_tuple;
static PyObject *const_tuple_str_digest_4f98b413c851efc6e35345123bc23439_tuple;
extern PyObject *const_tuple_str_digest_3724a8ee88291331504b26b811efeb5a_tuple;
extern PyObject *const_str_digest_d759df6b8c71154cebd6df0ba1bf9b3f;
extern PyObject *const_str_plain___debug__;
extern PyObject *const_str_digest_73eb09babb290e590b9a69835b5223c4;
extern PyObject *const_str_digest_ffa4782c58075c6dd84b572edd3109a7;
extern PyObject *const_tuple_str_plain_up_tuple;
extern PyObject *const_str_digest_1a6ebe85acbb908025352a60a51a93a4;
static PyObject *const_str_digest_218b33d507522522a424d95c99dc4fc8;
extern PyObject *const_str_plain_KeyPress;
extern PyObject *const_str_plain_data;
extern PyObject *const_tuple_str_digest_d2a5158fd7bb2018f91aee17fcfed439_tuple;
extern PyObject *const_str_plain_is_repeat;
static PyObject *const_tuple_str_digest_993bf0215ec53a0afc0447dbbdf085c9_tuple;
extern PyObject *const_str_plain_add;
extern PyObject *const_tuple_str_digest_daa690fd2d0c2002d9c7aad7dc2bacf6_tuple;
extern PyObject *const_tuple_str_plain_KeyPress_tuple;
extern PyObject *const_str_digest_018e4648ae82f20bdde528d806765fb8;
extern PyObject *const_tuple_str_digest_21e45c1f878d59fe84fced59f3e10b63_tuple;
extern PyObject *const_str_digest_2da2a9c575783ef454fa30e9f4e246ee;
extern PyObject *const_str_plain_down;
static PyObject *const_str_digest_c2d3a8abc72fb9a2a0c25a3f41726cd6;
extern PyObject *const_str_digest_b1b2ece871cd7fad7169ffac4c13c155;
extern PyObject *const_str_plain_f7;
extern PyObject *const_str_digest_5f96fb64d78f0245eec2c9973f838dfa;
static PyObject *const_tuple_str_plain_f18_tuple;
static PyObject *const_list_str_plain_load_basic_bindings_list;
extern PyObject *const_tuple_str_digest_db82dd148fa872efe07c2eb351c2c33f_tuple;
extern PyObject *const_str_plain_Condition;
extern PyObject *const_tuple_str_digest_a36417e3feea8b19ddc0146dc9edb326_tuple;
extern PyObject *const_tuple_str_digest_9102ee6eb9329cb31f5264512e00165d_tuple;
extern PyObject *const_tuple_str_digest_af1adbb7bec7eaa2f6c08a25f5ab2b5d_tuple;
static PyObject *const_tuple_c96c9c34187205cbc4a189c4b751ac8f_tuple;
extern PyObject *const_str_digest_647f11624bc526afd8ce034dc78066a1;
extern PyObject *const_dict_e2d16c98e7cb03ba7fd208a07fd5d185;
extern PyObject *const_tuple_str_digest_90c174e3cde8d76e0cf5b8e86689f4a5_tuple;
extern PyObject *const_str_plain_f16;
extern PyObject *const_str_digest_94c7a0cb80c5a4f2e9a8ddd3d95d0c88;
extern PyObject *const_str_plain_clipboard;
extern PyObject *const_str_plain_insert;
static PyObject *const_tuple_str_digest_94c7a0cb80c5a4f2e9a8ddd3d95d0c88_tuple;
extern PyObject *const_str_digest_f643864f71e00e7bebfd9e255daa97dc;
extern PyObject *const_tuple_str_plain_right_tuple;
extern PyObject *const_str_chr_13;
extern PyObject *const_str_digest_b7cd7d3061b29ef6badd990d7c83ddb1;
extern PyObject *const_str_digest_3724a8ee88291331504b26b811efeb5a;
extern PyObject *const_tuple_str_plain_f4_tuple;
extern PyObject *const_str_digest_21e45c1f878d59fe84fced59f3e10b63;
extern PyObject *const_tuple_str_digest_faa2df820fbcc927d6a1cbff6da2d7e2_tuple;
static PyObject *const_tuple_str_plain_f15_tuple;
extern PyObject *const_tuple_str_plain_event_str_plain_data_tuple;
extern PyObject *const_tuple_str_digest_2341ccadcc0716db92a6611c3a0c5709_tuple;
extern PyObject *const_tuple_str_digest_73eb09babb290e590b9a69835b5223c4_tuple;
static PyObject *const_tuple_str_digest_9d28ad88315d87cb7857dca59e1b839e_tuple;
extern PyObject *const_str_plain_vi_insert_mode;
static PyObject *const_tuple_str_plain_f16_tuple;
extern PyObject *const_tuple_str_digest_637ae2db1c15757d3f68020910fc7af5_tuple;
extern PyObject *const_tuple_str_digest_ee5885bc3d3f26ad23a483060cd09f03_tuple;
extern PyObject *const_tuple_empty;
extern PyObject *const_str_digest_f2a75caf8ec802255b8dafde1721755d;
extern PyObject *const_tuple_str_digest_7e022856dd1b5bd6663571c7df813303_tuple;
extern PyObject *const_str_digest_99c20ff6137166a3040444a10775418c;
extern PyObject *const_str_plain_event;
extern PyObject *const_str_digest_e5fd2b9ef2333d150f68273f43406bf0;
static PyObject *const_str_digest_d55056bbb9e1482440d2f381afe6fba0;
static PyObject *const_tuple_str_plain_f10_tuple;
extern PyObject *const_str_digest_1ceaeb7fd0ea5d7c27a08d77a50fc9bf;
extern PyObject *const_tuple_str_digest_018e4648ae82f20bdde528d806765fb8_tuple;
extern PyObject *const_str_plain_Keys;
extern PyObject *const_str_plain_auto_down;
extern PyObject *const_str_digest_fe8d6ea5daee2fa16a6d6e2bae6159bb;
extern PyObject *const_str_plain_f12;
static PyObject *const_tuple_str_plain_f6_tuple;
extern PyObject *const_str_plain_handle;
extern PyObject *const_tuple_str_plain_pagedown_tuple;
extern PyObject *const_str_plain_quoted_insert;
extern PyObject *const_tuple_str_digest_ffa4782c58075c6dd84b572edd3109a7_tuple;
static PyObject *const_tuple_str_plain_f19_tuple;
extern PyObject *const_str_plain_Any;
extern PyObject *const_str_digest_908c8ae8a1ea88b9446eaa47eb000fad;
static PyObject *const_tuple_str_plain_f17_tuple;
static PyObject *const_tuple_str_plain_f9_tuple;
static PyObject *const_dict_e4181dea19ade43f9b9633b3e1fd3350;
extern PyObject *const_str_plain_backspace;
extern PyObject *const_str_plain_KeyBindings;
extern PyObject *const_tuple_str_digest_f6826d177b892c516eb853c8574210e2_tuple;
extern PyObject *const_str_plain_f1;
extern PyObject *const_str_plain_False;
extern PyObject *const_str_plain_up;
extern PyObject *const_str_digest_ee5885bc3d3f26ad23a483060cd09f03;
extern PyObject *const_str_plain_in_paste_mode;
extern PyObject *const_str_plain_f9;
extern PyObject *const_str_plain___all__;
extern PyObject *const_str_digest_db82dd148fa872efe07c2eb351c2c33f;
static PyObject *const_str_plain_if_no_repeat;
extern PyObject *const_str_digest_daa690fd2d0c2002d9c7aad7dc2bacf6;
extern PyObject *const_str_digest_faa2df820fbcc927d6a1cbff6da2d7e2;
extern PyObject *const_str_digest_7230e58f12e9188dd6e7dddd9636a803;
extern PyObject *const_str_digest_3e8226f37423ae997480ac891b15fb8d;
static PyObject *const_tuple_str_plain_f2_tuple;
extern PyObject *const_str_plain_count;
extern PyObject *const_str_plain_first;
extern PyObject *const_str_digest_4dcf85ba674584b7335275c899c0ab24;
extern PyObject *const_tuple_str_digest_99c20ff6137166a3040444a10775418c_tuple;
extern PyObject *const_int_0;
extern PyObject *const_str_plain_f19;
extern PyObject *const_tuple_str_plain_enter_tuple;
static PyObject *const_tuple_str_plain_f13_tuple;
extern PyObject *const_str_plain_get_app;
extern PyObject *const_str_digest_444f48b1092beda0606928a9606f012b;
extern PyObject *const_str_digest_3a16fa49bea795e638aac8247b0f7f8d;
extern PyObject *const_tuple_str_plain_insert_tuple;
extern PyObject *const_str_digest_89633640ede9c2fabde46085a768738e;
extern PyObject *const_str_plain_text;
static PyObject *const_tuple_str_plain_f1_tuple;
extern PyObject *const_str_plain_f11;
extern PyObject *const_str_digest_b4cd29bb1a0bd6b6362c18c3e7a98845;
static PyObject *const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_tuple;
static PyObject *const_tuple_str_digest_7e1a6d304a63e0ac153bf5f43c885749_tuple;
extern PyObject *const_str_plain_origin;
extern PyObject *const_str_plain_overwrite;
extern PyObject *const_tuple_str_plain_get_app_tuple;
extern PyObject *const_str_plain_f6;
extern PyObject *const_str_plain_newline;
extern PyObject *const_tuple_str_plain_delete_tuple;
static PyObject *const_str_digest_883cd3eeb3aa1b4846f1c9b83f62281a;
extern PyObject *const_str_plain_key_processor;
extern PyObject *const_str_plain_right;
extern PyObject *const_str_plain_f2;
extern PyObject *const_str_digest_8ac25a148822dcf205845c372c142dc5;
extern PyObject *const_tuple_str_plain_pageup_tuple;
extern PyObject *const_tuple_str_digest_f643864f71e00e7bebfd9e255daa97dc_tuple;
extern PyObject *const_tuple_str_plain_down_tuple;
static PyObject *const_str_digest_67c9f6e346eebf99869d712d74a96db7;
extern PyObject *const_str_angle_lambda;
extern PyObject *const_tuple_str_plain_tab_tuple;
extern PyObject *const_str_digest_bca2dca6bc566bcc5093959d9941cbc9;
extern PyObject *const_str_digest_d2b85068d86fe4969537b7e2ea0dd346;
extern PyObject *const_tuple_str_digest_03dce90a9bd25ea1ea957edc60d7b71e_tuple;
extern PyObject *const_str_plain_ControlM;
extern PyObject *const_tuple_str_digest_820ffb9c299a4fe53ab8241c74e4288a_tuple;
extern PyObject *const_str_plain_app;
static PyObject *const_tuple_str_digest_b1b2ece871cd7fad7169ffac4c13c155_tuple;
static PyObject *const_tuple_str_plain_f14_tuple;
static PyObject *const_tuple_str_plain_f3_tuple;
extern PyObject *const_str_plain_current_buffer;
extern PyObject *const_tuple_str_digest_c1dec28d38befd226a053f4c881a493b_tuple;
extern PyObject *const_str_plain___cached__;
extern PyObject *const_str_plain_key_bindings;
extern PyObject *const_str_digest_90c174e3cde8d76e0cf5b8e86689f4a5;
extern PyObject *const_str_digest_9d28ad88315d87cb7857dca59e1b839e;
extern PyObject *const_tuple_str_digest_7230e58f12e9188dd6e7dddd9636a803_tuple;
extern PyObject *const_str_plain__;
extern PyObject *const_str_plain_f10;
extern PyObject *const_str_digest_03dce90a9bd25ea1ea957edc60d7b71e;
extern PyObject *const_str_plain_f14;
extern PyObject *const_str_digest_637ae2db1c15757d3f68020910fc7af5;
static PyObject *const_tuple_str_digest_334b87c0568648956e6ef5f0f9d62e81_tuple;
extern PyObject *const_str_digest_722e5e8c7f53b34663d9669ca7ddc056;
extern PyObject *const_str_digest_9102ee6eb9329cb31f5264512e00165d;
extern PyObject *const_tuple_str_plain_left_tuple;
extern PyObject *const_str_digest_d2a5158fd7bb2018f91aee17fcfed439;
extern PyObject *const_str_plain_unicode_literals;
extern PyObject *const_tuple_str_digest_7ca129d2d421fe965ad48cbb290b579b_str_newline_tuple;
extern PyObject *const_str_plain_eager;
extern PyObject *const_str_digest_334b87c0568648956e6ef5f0f9d62e81;
extern PyObject *const_str_plain_is_multiline;
extern PyObject *const_str_plain_save_before;
extern PyObject *const_str_plain_tab;
extern PyObject *const_int_pos_1;
extern PyObject *const_str_plain_replace;
extern PyObject *const_str_digest_cccb566811e08cc4fd2e3ae406952507;
static PyObject *const_tuple_str_plain_f7_tuple;
static PyObject *const_str_digest_240b478312ccc51a929b8f08d19fd830;
static PyObject *const_tuple_str_digest_d2b85068d86fe4969537b7e2ea0dd346_tuple;
static PyObject *const_tuple_str_plain_f11_tuple;
extern PyObject *const_str_plain_Ignore;
extern PyObject *const_str_digest_7ca129d2d421fe965ad48cbb290b579b;
extern PyObject *const_str_newline;
static PyObject *const_tuple_str_digest_5f96fb64d78f0245eec2c9973f838dfa_tuple;
extern PyObject *const_str_plain_left;
extern PyObject *const_tuple_str_plain_backspace_tuple;
extern PyObject *const_str_plain_copy_margin;
extern PyObject *const_str_digest_e9a7688ceb262f6815aa65f8ee8703ab;
extern PyObject *const_tuple_str_digest_be077fe6fbb1013afb24e5934140597e_tuple;
extern PyObject *const_str_plain_f17;
extern PyObject *const_tuple_str_digest_1a6ebe85acbb908025352a60a51a93a4_tuple;
extern PyObject *const_str_plain_get_by_name;
extern PyObject *const_tuple_str_digest_fe8d6ea5daee2fa16a6d6e2bae6159bb_tuple;
extern PyObject *const_str_plain_cut_selection;
extern PyObject *const_str_plain_f20;
extern PyObject *const_str_digest_d8156c92c3ac39743ec9345e59bfd2ed;
static PyObject *const_tuple_e2bfdb23e9b6dc1265ecaa251ea16394_tuple;
static PyObject *const_tuple_str_plain_f5_tuple;
extern PyObject *const_tuple_str_digest_8ac25a148822dcf205845c372c142dc5_tuple;
static PyObject *const_str_digest_fa8868e1b83a9d3b797546af06905b98;
extern PyObject *const_str_plain_auto_up;
extern PyObject *const_str_plain_f13;
extern PyObject *const_str_plain_pageup;
extern PyObject *const_tuple_str_digest_204c920806e032799a87379e1294bcfc_tuple;
extern PyObject *const_str_digest_820ffb9c299a4fe53ab8241c74e4288a;
static PyObject *const_tuple_str_digest_1ceaeb7fd0ea5d7c27a08d77a50fc9bf_tuple;
extern PyObject *const_str_plain_BracketedPaste;
extern PyObject *const_str_plain_has_location;
extern PyObject *const_str_digest_7e1a6d304a63e0ac153bf5f43c885749;
extern PyObject *const_str_plain_f4;
extern PyObject *const_int_pos_2;
extern PyObject *const_str_plain_f15;
extern PyObject *const_str_plain_set_data;
extern PyObject *const_str_plain_delete;
extern PyObject *const_str_plain_text_before_cursor;
extern PyObject *const_str_plain_pagedown;
extern PyObject *const_str_plain_emacs_insert_mode;
extern PyObject *const_str_plain_feed;
extern PyObject *const_tuple_str_digest_b4cd29bb1a0bd6b6362c18c3e7a98845_tuple;
static PyObject *const_tuple_str_plain_home_tuple;
static PyObject *const_tuple_str_digest_444f48b1092beda0606928a9606f012b_tuple;
extern PyObject *const_str_plain_insert_text;
extern PyObject *const_str_plain_f8;
extern PyObject *const_tuple_str_digest_e5fd2b9ef2333d150f68273f43406bf0_tuple;
static PyObject *module_filename_obj;

/* Indicator if this modules private constants were created yet. */
static bool constants_created = false;

/* Function to create module private constants. */
static void createModuleConstants( void )
{
    const_tuple_str_digest_6dfa089e1785bfb099fb5067adfbc10c_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_6dfa089e1785bfb099fb5067adfbc10c_tuple, 0, const_str_digest_6dfa089e1785bfb099fb5067adfbc10c ); Py_INCREF( const_str_digest_6dfa089e1785bfb099fb5067adfbc10c );
    const_tuple_str_digest_cccb566811e08cc4fd2e3ae406952507_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_cccb566811e08cc4fd2e3ae406952507_tuple, 0, const_str_digest_cccb566811e08cc4fd2e3ae406952507 ); Py_INCREF( const_str_digest_cccb566811e08cc4fd2e3ae406952507 );
    const_str_digest_f0d13f3bfe27153ec821d14c89b14f7f = UNSTREAM_STRING_ASCII( &constant_bin[ 4677379 ], 50, 0 );
    const_str_digest_9cccc676da4746edb9fb23677cfeea9d = UNSTREAM_STRING_ASCII( &constant_bin[ 4677429 ], 90, 0 );
    const_str_digest_f7fef436b96330c714feb9f16a46dfa1 = UNSTREAM_STRING_ASCII( &constant_bin[ 4677519 ], 333, 0 );
    const_tuple_str_plain_f20_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f20_tuple, 0, const_str_plain_f20 ); Py_INCREF( const_str_plain_f20 );
    const_tuple_str_plain_f12_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f12_tuple, 0, const_str_plain_f12 ); Py_INCREF( const_str_plain_f12 );
    const_tuple_str_digest_d759df6b8c71154cebd6df0ba1bf9b3f_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_d759df6b8c71154cebd6df0ba1bf9b3f_tuple, 0, const_str_digest_d759df6b8c71154cebd6df0ba1bf9b3f ); Py_INCREF( const_str_digest_d759df6b8c71154cebd6df0ba1bf9b3f );
    const_tuple_str_digest_e9a7688ceb262f6815aa65f8ee8703ab_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_e9a7688ceb262f6815aa65f8ee8703ab_tuple, 0, const_str_digest_e9a7688ceb262f6815aa65f8ee8703ab ); Py_INCREF( const_str_digest_e9a7688ceb262f6815aa65f8ee8703ab );
    const_str_digest_96aecf7b4adb5c3dbb234466987ace12 = UNSTREAM_STRING_ASCII( &constant_bin[ 4677852 ], 37, 0 );
    const_tuple_str_digest_4f98b413c851efc6e35345123bc23439_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_4f98b413c851efc6e35345123bc23439_tuple, 0, const_str_digest_4f98b413c851efc6e35345123bc23439 ); Py_INCREF( const_str_digest_4f98b413c851efc6e35345123bc23439 );
    const_str_digest_218b33d507522522a424d95c99dc4fc8 = UNSTREAM_STRING_ASCII( &constant_bin[ 4677889 ], 203, 0 );
    const_tuple_str_digest_993bf0215ec53a0afc0447dbbdf085c9_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_993bf0215ec53a0afc0447dbbdf085c9_tuple, 0, const_str_digest_993bf0215ec53a0afc0447dbbdf085c9 ); Py_INCREF( const_str_digest_993bf0215ec53a0afc0447dbbdf085c9 );
    const_str_digest_c2d3a8abc72fb9a2a0c25a3f41726cd6 = UNSTREAM_STRING_ASCII( &constant_bin[ 4678092 ], 25, 0 );
    const_tuple_str_plain_f18_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f18_tuple, 0, const_str_plain_f18 ); Py_INCREF( const_str_plain_f18 );
    const_list_str_plain_load_basic_bindings_list = PyList_New( 1 );
    PyList_SET_ITEM( const_list_str_plain_load_basic_bindings_list, 0, const_str_plain_load_basic_bindings ); Py_INCREF( const_str_plain_load_basic_bindings );
    const_tuple_c96c9c34187205cbc4a189c4b751ac8f_tuple = PyTuple_New( 6 );
    PyTuple_SET_ITEM( const_tuple_c96c9c34187205cbc4a189c4b751ac8f_tuple, 0, const_str_plain_has_selection ); Py_INCREF( const_str_plain_has_selection );
    PyTuple_SET_ITEM( const_tuple_c96c9c34187205cbc4a189c4b751ac8f_tuple, 1, const_str_plain_Condition ); Py_INCREF( const_str_plain_Condition );
    PyTuple_SET_ITEM( const_tuple_c96c9c34187205cbc4a189c4b751ac8f_tuple, 2, const_str_plain_emacs_insert_mode ); Py_INCREF( const_str_plain_emacs_insert_mode );
    PyTuple_SET_ITEM( const_tuple_c96c9c34187205cbc4a189c4b751ac8f_tuple, 3, const_str_plain_vi_insert_mode ); Py_INCREF( const_str_plain_vi_insert_mode );
    PyTuple_SET_ITEM( const_tuple_c96c9c34187205cbc4a189c4b751ac8f_tuple, 4, const_str_plain_in_paste_mode ); Py_INCREF( const_str_plain_in_paste_mode );
    PyTuple_SET_ITEM( const_tuple_c96c9c34187205cbc4a189c4b751ac8f_tuple, 5, const_str_plain_is_multiline ); Py_INCREF( const_str_plain_is_multiline );
    const_tuple_str_digest_94c7a0cb80c5a4f2e9a8ddd3d95d0c88_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_94c7a0cb80c5a4f2e9a8ddd3d95d0c88_tuple, 0, const_str_digest_94c7a0cb80c5a4f2e9a8ddd3d95d0c88 ); Py_INCREF( const_str_digest_94c7a0cb80c5a4f2e9a8ddd3d95d0c88 );
    const_tuple_str_plain_f15_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f15_tuple, 0, const_str_plain_f15 ); Py_INCREF( const_str_plain_f15 );
    const_tuple_str_digest_9d28ad88315d87cb7857dca59e1b839e_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_9d28ad88315d87cb7857dca59e1b839e_tuple, 0, const_str_digest_9d28ad88315d87cb7857dca59e1b839e ); Py_INCREF( const_str_digest_9d28ad88315d87cb7857dca59e1b839e );
    const_tuple_str_plain_f16_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f16_tuple, 0, const_str_plain_f16 ); Py_INCREF( const_str_plain_f16 );
    const_str_digest_d55056bbb9e1482440d2f381afe6fba0 = UNSTREAM_STRING_ASCII( &constant_bin[ 4678117 ], 44, 0 );
    const_tuple_str_plain_f10_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f10_tuple, 0, const_str_plain_f10 ); Py_INCREF( const_str_plain_f10 );
    const_tuple_str_plain_f6_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f6_tuple, 0, const_str_plain_f6 ); Py_INCREF( const_str_plain_f6 );
    const_tuple_str_plain_f19_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f19_tuple, 0, const_str_plain_f19 ); Py_INCREF( const_str_plain_f19 );
    const_tuple_str_plain_f17_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f17_tuple, 0, const_str_plain_f17 ); Py_INCREF( const_str_plain_f17 );
    const_tuple_str_plain_f9_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f9_tuple, 0, const_str_plain_f9 ); Py_INCREF( const_str_plain_f9 );
    const_dict_e4181dea19ade43f9b9633b3e1fd3350 = _PyDict_NewPresized( 1 );
    PyDict_SetItem( const_dict_e4181dea19ade43f9b9633b3e1fd3350, const_str_plain_overwrite, Py_False );
    assert( PyDict_Size( const_dict_e4181dea19ade43f9b9633b3e1fd3350 ) == 1 );
    const_str_plain_if_no_repeat = UNSTREAM_STRING_ASCII( &constant_bin[ 4678161 ], 12, 1 );
    const_tuple_str_plain_f2_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f2_tuple, 0, const_str_plain_f2 ); Py_INCREF( const_str_plain_f2 );
    const_tuple_str_plain_f13_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f13_tuple, 0, const_str_plain_f13 ); Py_INCREF( const_str_plain_f13 );
    const_tuple_str_plain_f1_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f1_tuple, 0, const_str_plain_f1 ); Py_INCREF( const_str_plain_f1 );
    const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_tuple, 0, const_str_digest_5bbf98712488fb50e0bf411a1625446e ); Py_INCREF( const_str_digest_5bbf98712488fb50e0bf411a1625446e );
    const_tuple_str_digest_7e1a6d304a63e0ac153bf5f43c885749_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_7e1a6d304a63e0ac153bf5f43c885749_tuple, 0, const_str_digest_7e1a6d304a63e0ac153bf5f43c885749 ); Py_INCREF( const_str_digest_7e1a6d304a63e0ac153bf5f43c885749 );
    const_str_digest_883cd3eeb3aa1b4846f1c9b83f62281a = UNSTREAM_STRING_ASCII( &constant_bin[ 4678173 ], 38, 0 );
    const_str_digest_67c9f6e346eebf99869d712d74a96db7 = UNSTREAM_STRING_ASCII( &constant_bin[ 4678211 ], 30, 0 );
    const_tuple_str_digest_b1b2ece871cd7fad7169ffac4c13c155_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_b1b2ece871cd7fad7169ffac4c13c155_tuple, 0, const_str_digest_b1b2ece871cd7fad7169ffac4c13c155 ); Py_INCREF( const_str_digest_b1b2ece871cd7fad7169ffac4c13c155 );
    const_tuple_str_plain_f14_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f14_tuple, 0, const_str_plain_f14 ); Py_INCREF( const_str_plain_f14 );
    const_tuple_str_plain_f3_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f3_tuple, 0, const_str_plain_f3 ); Py_INCREF( const_str_plain_f3 );
    const_tuple_str_digest_334b87c0568648956e6ef5f0f9d62e81_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_334b87c0568648956e6ef5f0f9d62e81_tuple, 0, const_str_digest_334b87c0568648956e6ef5f0f9d62e81 ); Py_INCREF( const_str_digest_334b87c0568648956e6ef5f0f9d62e81 );
    const_tuple_str_plain_f7_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f7_tuple, 0, const_str_plain_f7 ); Py_INCREF( const_str_plain_f7 );
    const_str_digest_240b478312ccc51a929b8f08d19fd830 = UNSTREAM_STRING_ASCII( &constant_bin[ 4678241 ], 362, 0 );
    const_tuple_str_digest_d2b85068d86fe4969537b7e2ea0dd346_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_d2b85068d86fe4969537b7e2ea0dd346_tuple, 0, const_str_digest_d2b85068d86fe4969537b7e2ea0dd346 ); Py_INCREF( const_str_digest_d2b85068d86fe4969537b7e2ea0dd346 );
    const_tuple_str_plain_f11_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f11_tuple, 0, const_str_plain_f11 ); Py_INCREF( const_str_plain_f11 );
    const_tuple_str_digest_5f96fb64d78f0245eec2c9973f838dfa_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_5f96fb64d78f0245eec2c9973f838dfa_tuple, 0, const_str_digest_5f96fb64d78f0245eec2c9973f838dfa ); Py_INCREF( const_str_digest_5f96fb64d78f0245eec2c9973f838dfa );
    const_tuple_e2bfdb23e9b6dc1265ecaa251ea16394_tuple = PyTuple_New( 5 );
    PyTuple_SET_ITEM( const_tuple_e2bfdb23e9b6dc1265ecaa251ea16394_tuple, 0, const_str_plain_key_bindings ); Py_INCREF( const_str_plain_key_bindings );
    PyTuple_SET_ITEM( const_tuple_e2bfdb23e9b6dc1265ecaa251ea16394_tuple, 1, const_str_plain_insert_mode ); Py_INCREF( const_str_plain_insert_mode );
    PyTuple_SET_ITEM( const_tuple_e2bfdb23e9b6dc1265ecaa251ea16394_tuple, 2, const_str_plain_handle ); Py_INCREF( const_str_plain_handle );
    PyTuple_SET_ITEM( const_tuple_e2bfdb23e9b6dc1265ecaa251ea16394_tuple, 3, const_str_plain__ ); Py_INCREF( const_str_plain__ );
    PyTuple_SET_ITEM( const_tuple_e2bfdb23e9b6dc1265ecaa251ea16394_tuple, 4, const_str_plain_text_before_cursor ); Py_INCREF( const_str_plain_text_before_cursor );
    const_tuple_str_plain_f5_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_f5_tuple, 0, const_str_plain_f5 ); Py_INCREF( const_str_plain_f5 );
    const_str_digest_fa8868e1b83a9d3b797546af06905b98 = UNSTREAM_STRING_ASCII( &constant_bin[ 4678603 ], 39, 0 );
    const_tuple_str_digest_1ceaeb7fd0ea5d7c27a08d77a50fc9bf_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_1ceaeb7fd0ea5d7c27a08d77a50fc9bf_tuple, 0, const_str_digest_1ceaeb7fd0ea5d7c27a08d77a50fc9bf ); Py_INCREF( const_str_digest_1ceaeb7fd0ea5d7c27a08d77a50fc9bf );
    const_tuple_str_plain_home_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_plain_home_tuple, 0, const_str_plain_home ); Py_INCREF( const_str_plain_home );
    const_tuple_str_digest_444f48b1092beda0606928a9606f012b_tuple = PyTuple_New( 1 );
    PyTuple_SET_ITEM( const_tuple_str_digest_444f48b1092beda0606928a9606f012b_tuple, 0, const_str_digest_444f48b1092beda0606928a9606f012b ); Py_INCREF( const_str_digest_444f48b1092beda0606928a9606f012b );

    constants_created = true;
}

/* Function to verify module private constants for non-corruption. */
#ifndef __NUITKA_NO_ASSERT__
void checkModuleConstants_prompt_toolkit$key_binding$bindings$basic( void )
{
    // The module may not have been used at all, then ignore this.
    if (constants_created == false) return;


}
#endif

// The module code objects.
static PyCodeObject *codeobj_c938375228a721c91a9db1d8d4517f6e;
static PyCodeObject *codeobj_dea0d25ef92a25226ef8ac77a4bcf66b;
static PyCodeObject *codeobj_39e21f71248a54d4e4b95230c4628dc2;
static PyCodeObject *codeobj_fc993341e9a0c21993f19d371d0bd0f7;
static PyCodeObject *codeobj_4e9b1d787fd3d410289f5155384148b9;
static PyCodeObject *codeobj_7e2a60d14f94af675b3e06051a6f3587;
static PyCodeObject *codeobj_45d48e22ad81947896cb4fd75504e88a;
static PyCodeObject *codeobj_7d523f7f31ee26acdccc49520af196e2;
static PyCodeObject *codeobj_0989717c2546f21f166c14362457ab06;
static PyCodeObject *codeobj_18156eef96c515595973ffefa4cad84a;
static PyCodeObject *codeobj_46b2eb01f86e4d19277b86b02001e840;
static PyCodeObject *codeobj_8e6a86834f3d58c22814a3cb40a4bcc7;
static PyCodeObject *codeobj_1fa52785aaac79e9088c70526f118a18;
static PyCodeObject *codeobj_078d10575ac2c0fb043b17bda86af556;

static void createModuleCodeObjects(void)
{
    module_filename_obj = MAKE_RELATIVE_PATH( const_str_digest_d55056bbb9e1482440d2f381afe6fba0 );
    codeobj_c938375228a721c91a9db1d8d4517f6e = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 146, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_dea0d25ef92a25226ef8ac77a4bcf66b = MAKE_CODEOBJ( module_filename_obj, const_str_angle_lambda, 207, const_tuple_empty, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_39e21f71248a54d4e4b95230c4628dc2 = MAKE_CODEOBJ( module_filename_obj, const_str_digest_f0d13f3bfe27153ec821d14c89b14f7f, 1, const_tuple_empty, 0, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_fc993341e9a0c21993f19d371d0bd0f7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 28, const_tuple_str_plain_event_tuple, 1, 0, CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_4e9b1d787fd3d410289f5155384148b9 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 149, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7e2a60d14f94af675b3e06051a6f3587 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 154, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_45d48e22ad81947896cb4fd75504e88a = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 166, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_7d523f7f31ee26acdccc49520af196e2 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 170, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_0989717c2546f21f166c14362457ab06 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 181, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_18156eef96c515595973ffefa4cad84a = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 207, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_46b2eb01f86e4d19277b86b02001e840 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 174, const_tuple_str_plain_event_str_plain_data_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_8e6a86834f3d58c22814a3cb40a4bcc7 = MAKE_CODEOBJ( module_filename_obj, const_str_plain__, 194, const_tuple_str_plain_event_str_plain_data_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_1fa52785aaac79e9088c70526f118a18 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_if_no_repeat, 17, const_tuple_str_plain_event_tuple, 1, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
    codeobj_078d10575ac2c0fb043b17bda86af556 = MAKE_CODEOBJ( module_filename_obj, const_str_plain_load_basic_bindings, 23, const_tuple_e2bfdb23e9b6dc1265ecaa251ea16394_tuple, 0, 0, CO_OPTIMIZED | CO_NEWLOCALS | CO_NOFREE | CO_FUTURE_UNICODE_LITERALS );
}

// The module function declarations.
static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_1_if_no_repeat(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_10__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_11_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_1__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_2_lambda(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_3__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_4__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_5__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_6__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_7__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_8__(  );


static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_9__(  );


// The module function definitions.
static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_1_if_no_repeat( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_1fa52785aaac79e9088c70526f118a18;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_1fa52785aaac79e9088c70526f118a18 = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_1fa52785aaac79e9088c70526f118a18, codeobj_1fa52785aaac79e9088c70526f118a18, module_prompt_toolkit$key_binding$bindings$basic, sizeof(void *) );
    frame_1fa52785aaac79e9088c70526f118a18 = cache_frame_1fa52785aaac79e9088c70526f118a18;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_1fa52785aaac79e9088c70526f118a18 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_1fa52785aaac79e9088c70526f118a18 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_operand_name_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_operand_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_is_repeat );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 20;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_return_value = ( tmp_res == 0 ) ? Py_True : Py_False;
        Py_INCREF( tmp_return_value );
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1fa52785aaac79e9088c70526f118a18 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_1fa52785aaac79e9088c70526f118a18 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto try_return_handler_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_1fa52785aaac79e9088c70526f118a18 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_1fa52785aaac79e9088c70526f118a18, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_1fa52785aaac79e9088c70526f118a18->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_1fa52785aaac79e9088c70526f118a18, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_1fa52785aaac79e9088c70526f118a18,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_1fa52785aaac79e9088c70526f118a18 == cache_frame_1fa52785aaac79e9088c70526f118a18 )
    {
        Py_DECREF( frame_1fa52785aaac79e9088c70526f118a18 );
    }
    cache_frame_1fa52785aaac79e9088c70526f118a18 = NULL;

    assertFrameObject( frame_1fa52785aaac79e9088c70526f118a18 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_1_if_no_repeat );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_1_if_no_repeat );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *var_key_bindings = NULL;
    PyObject *var_insert_mode = NULL;
    PyObject *var_handle = NULL;
    PyObject *var__ = NULL;
    PyObject *var_text_before_cursor = NULL;
    struct Nuitka_FrameObject *frame_078d10575ac2c0fb043b17bda86af556;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_078d10575ac2c0fb043b17bda86af556 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_078d10575ac2c0fb043b17bda86af556, codeobj_078d10575ac2c0fb043b17bda86af556, module_prompt_toolkit$key_binding$bindings$basic, sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *)+sizeof(void *) );
    frame_078d10575ac2c0fb043b17bda86af556 = cache_frame_078d10575ac2c0fb043b17bda86af556;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_078d10575ac2c0fb043b17bda86af556 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_078d10575ac2c0fb043b17bda86af556 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_KeyBindings );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KeyBindings );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "KeyBindings" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 24;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 24;
        tmp_assign_source_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 24;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_key_bindings == NULL );
        var_key_bindings = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_left_name_1;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_right_name_1;
        PyObject *tmp_mvar_value_3;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_vi_insert_mode );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_vi_insert_mode );
        }

        if ( tmp_mvar_value_2 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "vi_insert_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 25;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_left_name_1 = tmp_mvar_value_2;
        tmp_mvar_value_3 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_emacs_insert_mode );

        if (unlikely( tmp_mvar_value_3 == NULL ))
        {
            tmp_mvar_value_3 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_emacs_insert_mode );
        }

        if ( tmp_mvar_value_3 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "emacs_insert_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 25;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_1 = tmp_mvar_value_3;
        tmp_assign_source_2 = BINARY_OPERATION( PyNumber_Or, tmp_left_name_1, tmp_right_name_1 );
        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 25;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_insert_mode == NULL );
        var_insert_mode = tmp_assign_source_2;
    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( var_key_bindings );
        tmp_source_name_1 = var_key_bindings;
        tmp_assign_source_3 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_add );
        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 26;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_handle == NULL );
        var_handle = tmp_assign_source_3;
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_called_name_2;
        PyObject *tmp_called_name_3;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_called_name_4;
        PyObject *tmp_called_name_5;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_called_name_6;
        PyObject *tmp_called_name_7;
        PyObject *tmp_args_element_name_3;
        PyObject *tmp_called_name_8;
        PyObject *tmp_called_name_9;
        PyObject *tmp_args_element_name_4;
        PyObject *tmp_called_name_10;
        PyObject *tmp_called_name_11;
        PyObject *tmp_args_element_name_5;
        PyObject *tmp_called_name_12;
        PyObject *tmp_called_name_13;
        PyObject *tmp_args_element_name_6;
        PyObject *tmp_called_name_14;
        PyObject *tmp_called_name_15;
        PyObject *tmp_args_element_name_7;
        PyObject *tmp_called_name_16;
        PyObject *tmp_called_name_17;
        PyObject *tmp_args_element_name_8;
        PyObject *tmp_called_name_18;
        PyObject *tmp_called_name_19;
        PyObject *tmp_args_element_name_9;
        PyObject *tmp_called_name_20;
        PyObject *tmp_called_name_21;
        PyObject *tmp_args_element_name_10;
        PyObject *tmp_called_name_22;
        PyObject *tmp_called_name_23;
        PyObject *tmp_args_element_name_11;
        PyObject *tmp_called_name_24;
        PyObject *tmp_called_name_25;
        PyObject *tmp_args_element_name_12;
        PyObject *tmp_called_name_26;
        PyObject *tmp_called_name_27;
        PyObject *tmp_args_element_name_13;
        PyObject *tmp_called_name_28;
        PyObject *tmp_called_name_29;
        PyObject *tmp_args_element_name_14;
        PyObject *tmp_called_name_30;
        PyObject *tmp_called_name_31;
        PyObject *tmp_args_element_name_15;
        PyObject *tmp_called_name_32;
        PyObject *tmp_called_name_33;
        PyObject *tmp_args_element_name_16;
        PyObject *tmp_called_name_34;
        PyObject *tmp_called_name_35;
        PyObject *tmp_args_element_name_17;
        PyObject *tmp_called_name_36;
        PyObject *tmp_called_name_37;
        PyObject *tmp_args_element_name_18;
        PyObject *tmp_called_name_38;
        PyObject *tmp_called_name_39;
        PyObject *tmp_args_element_name_19;
        PyObject *tmp_called_name_40;
        PyObject *tmp_called_name_41;
        PyObject *tmp_args_element_name_20;
        PyObject *tmp_called_name_42;
        PyObject *tmp_called_name_43;
        PyObject *tmp_args_element_name_21;
        PyObject *tmp_called_name_44;
        PyObject *tmp_called_name_45;
        PyObject *tmp_args_element_name_22;
        PyObject *tmp_called_name_46;
        PyObject *tmp_called_name_47;
        PyObject *tmp_args_element_name_23;
        PyObject *tmp_called_name_48;
        PyObject *tmp_called_name_49;
        PyObject *tmp_args_element_name_24;
        PyObject *tmp_called_name_50;
        PyObject *tmp_called_name_51;
        PyObject *tmp_args_element_name_25;
        PyObject *tmp_called_name_52;
        PyObject *tmp_called_name_53;
        PyObject *tmp_args_element_name_26;
        PyObject *tmp_called_name_54;
        PyObject *tmp_called_name_55;
        PyObject *tmp_args_element_name_27;
        PyObject *tmp_called_name_56;
        PyObject *tmp_called_name_57;
        PyObject *tmp_args_element_name_28;
        PyObject *tmp_called_name_58;
        PyObject *tmp_called_name_59;
        PyObject *tmp_args_element_name_29;
        PyObject *tmp_called_name_60;
        PyObject *tmp_called_name_61;
        PyObject *tmp_args_element_name_30;
        PyObject *tmp_called_name_62;
        PyObject *tmp_called_name_63;
        PyObject *tmp_args_element_name_31;
        PyObject *tmp_called_name_64;
        PyObject *tmp_called_name_65;
        PyObject *tmp_args_element_name_32;
        PyObject *tmp_called_name_66;
        PyObject *tmp_called_name_67;
        PyObject *tmp_args_element_name_33;
        PyObject *tmp_called_name_68;
        PyObject *tmp_called_name_69;
        PyObject *tmp_args_element_name_34;
        PyObject *tmp_called_name_70;
        PyObject *tmp_called_name_71;
        PyObject *tmp_args_element_name_35;
        PyObject *tmp_called_name_72;
        PyObject *tmp_called_name_73;
        PyObject *tmp_args_element_name_36;
        PyObject *tmp_called_name_74;
        PyObject *tmp_called_name_75;
        PyObject *tmp_args_element_name_37;
        PyObject *tmp_called_name_76;
        PyObject *tmp_called_name_77;
        PyObject *tmp_args_element_name_38;
        PyObject *tmp_called_name_78;
        PyObject *tmp_called_name_79;
        PyObject *tmp_args_element_name_39;
        PyObject *tmp_called_name_80;
        PyObject *tmp_called_name_81;
        PyObject *tmp_args_element_name_40;
        PyObject *tmp_called_name_82;
        PyObject *tmp_called_name_83;
        PyObject *tmp_args_element_name_41;
        PyObject *tmp_called_name_84;
        PyObject *tmp_called_name_85;
        PyObject *tmp_args_element_name_42;
        PyObject *tmp_called_name_86;
        PyObject *tmp_called_name_87;
        PyObject *tmp_args_element_name_43;
        PyObject *tmp_called_name_88;
        PyObject *tmp_called_name_89;
        PyObject *tmp_args_element_name_44;
        PyObject *tmp_called_name_90;
        PyObject *tmp_called_name_91;
        PyObject *tmp_args_element_name_45;
        PyObject *tmp_called_name_92;
        PyObject *tmp_called_name_93;
        PyObject *tmp_args_element_name_46;
        PyObject *tmp_called_name_94;
        PyObject *tmp_called_name_95;
        PyObject *tmp_args_element_name_47;
        PyObject *tmp_called_name_96;
        PyObject *tmp_called_name_97;
        PyObject *tmp_args_element_name_48;
        PyObject *tmp_called_name_98;
        PyObject *tmp_called_name_99;
        PyObject *tmp_args_element_name_49;
        PyObject *tmp_called_name_100;
        PyObject *tmp_called_name_101;
        PyObject *tmp_args_element_name_50;
        PyObject *tmp_called_name_102;
        PyObject *tmp_called_name_103;
        PyObject *tmp_args_element_name_51;
        PyObject *tmp_called_name_104;
        PyObject *tmp_called_name_105;
        PyObject *tmp_args_element_name_52;
        PyObject *tmp_called_name_106;
        PyObject *tmp_called_name_107;
        PyObject *tmp_args_element_name_53;
        PyObject *tmp_called_name_108;
        PyObject *tmp_called_name_109;
        PyObject *tmp_args_element_name_54;
        PyObject *tmp_called_name_110;
        PyObject *tmp_called_name_111;
        PyObject *tmp_args_element_name_55;
        PyObject *tmp_called_name_112;
        PyObject *tmp_called_name_113;
        PyObject *tmp_args_element_name_56;
        PyObject *tmp_called_name_114;
        PyObject *tmp_called_name_115;
        PyObject *tmp_args_element_name_57;
        PyObject *tmp_called_name_116;
        PyObject *tmp_called_name_117;
        PyObject *tmp_args_element_name_58;
        PyObject *tmp_called_name_118;
        PyObject *tmp_called_name_119;
        PyObject *tmp_args_element_name_59;
        PyObject *tmp_called_name_120;
        PyObject *tmp_called_name_121;
        PyObject *tmp_args_element_name_60;
        PyObject *tmp_called_name_122;
        PyObject *tmp_called_name_123;
        PyObject *tmp_args_element_name_61;
        PyObject *tmp_called_name_124;
        PyObject *tmp_called_name_125;
        PyObject *tmp_args_element_name_62;
        PyObject *tmp_called_name_126;
        PyObject *tmp_called_name_127;
        PyObject *tmp_args_element_name_63;
        PyObject *tmp_called_name_128;
        PyObject *tmp_called_name_129;
        PyObject *tmp_args_element_name_64;
        PyObject *tmp_called_name_130;
        PyObject *tmp_called_name_131;
        PyObject *tmp_args_element_name_65;
        PyObject *tmp_called_name_132;
        PyObject *tmp_called_name_133;
        PyObject *tmp_args_element_name_66;
        PyObject *tmp_called_name_134;
        PyObject *tmp_called_name_135;
        PyObject *tmp_args_element_name_67;
        PyObject *tmp_called_name_136;
        PyObject *tmp_called_name_137;
        PyObject *tmp_args_element_name_68;
        PyObject *tmp_called_name_138;
        PyObject *tmp_called_name_139;
        PyObject *tmp_args_element_name_69;
        PyObject *tmp_called_name_140;
        PyObject *tmp_called_name_141;
        PyObject *tmp_args_element_name_70;
        PyObject *tmp_called_name_142;
        PyObject *tmp_called_name_143;
        PyObject *tmp_args_element_name_71;
        PyObject *tmp_called_name_144;
        PyObject *tmp_called_name_145;
        PyObject *tmp_args_element_name_72;
        PyObject *tmp_called_name_146;
        PyObject *tmp_called_name_147;
        PyObject *tmp_args_element_name_73;
        PyObject *tmp_called_name_148;
        PyObject *tmp_called_name_149;
        PyObject *tmp_args_element_name_74;
        PyObject *tmp_called_name_150;
        PyObject *tmp_called_name_151;
        PyObject *tmp_args_element_name_75;
        PyObject *tmp_source_name_2;
        PyObject *tmp_mvar_value_4;
        PyObject *tmp_args_element_name_76;
        CHECK_OBJECT( var_handle );
        tmp_called_name_3 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 28;
        tmp_called_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_3, &PyTuple_GET_ITEM( const_tuple_str_digest_03dce90a9bd25ea1ea957edc60d7b71e_tuple, 0 ) );

        if ( tmp_called_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_5 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 29;
        tmp_called_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_5, &PyTuple_GET_ITEM( const_tuple_str_digest_1a6ebe85acbb908025352a60a51a93a4_tuple, 0 ) );

        if ( tmp_called_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 29;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_7 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 30;
        tmp_called_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_7, &PyTuple_GET_ITEM( const_tuple_str_digest_99c20ff6137166a3040444a10775418c_tuple, 0 ) );

        if ( tmp_called_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 30;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_9 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 31;
        tmp_called_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_9, &PyTuple_GET_ITEM( const_tuple_str_digest_b4cd29bb1a0bd6b6362c18c3e7a98845_tuple, 0 ) );

        if ( tmp_called_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 31;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_11 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 32;
        tmp_called_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_11, &PyTuple_GET_ITEM( const_tuple_str_digest_a36417e3feea8b19ddc0146dc9edb326_tuple, 0 ) );

        if ( tmp_called_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );

            exception_lineno = 32;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_13 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 33;
        tmp_called_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_13, &PyTuple_GET_ITEM( const_tuple_str_digest_89633640ede9c2fabde46085a768738e_tuple, 0 ) );

        if ( tmp_called_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 33;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_15 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 34;
        tmp_called_name_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_15, &PyTuple_GET_ITEM( const_tuple_str_digest_faa2df820fbcc927d6a1cbff6da2d7e2_tuple, 0 ) );

        if ( tmp_called_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );

            exception_lineno = 34;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_17 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 35;
        tmp_called_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_17, &PyTuple_GET_ITEM( const_tuple_str_digest_5f96fb64d78f0245eec2c9973f838dfa_tuple, 0 ) );

        if ( tmp_called_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );

            exception_lineno = 35;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_19 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 36;
        tmp_called_name_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_19, &PyTuple_GET_ITEM( const_tuple_str_digest_d2b85068d86fe4969537b7e2ea0dd346_tuple, 0 ) );

        if ( tmp_called_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );

            exception_lineno = 36;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_21 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 37;
        tmp_called_name_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_21, &PyTuple_GET_ITEM( const_tuple_str_digest_d759df6b8c71154cebd6df0ba1bf9b3f_tuple, 0 ) );

        if ( tmp_called_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );

            exception_lineno = 37;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_23 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 38;
        tmp_called_name_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_23, &PyTuple_GET_ITEM( const_tuple_str_digest_018e4648ae82f20bdde528d806765fb8_tuple, 0 ) );

        if ( tmp_called_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );

            exception_lineno = 38;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_25 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 39;
        tmp_called_name_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_25, &PyTuple_GET_ITEM( const_tuple_str_digest_f643864f71e00e7bebfd9e255daa97dc_tuple, 0 ) );

        if ( tmp_called_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );

            exception_lineno = 39;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_27 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 40;
        tmp_called_name_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_27, &PyTuple_GET_ITEM( const_tuple_str_digest_4f98b413c851efc6e35345123bc23439_tuple, 0 ) );

        if ( tmp_called_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );

            exception_lineno = 40;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_29 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 41;
        tmp_called_name_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_29, &PyTuple_GET_ITEM( const_tuple_str_digest_daa690fd2d0c2002d9c7aad7dc2bacf6_tuple, 0 ) );

        if ( tmp_called_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );

            exception_lineno = 41;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_31 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 42;
        tmp_called_name_30 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_31, &PyTuple_GET_ITEM( const_tuple_str_digest_90c174e3cde8d76e0cf5b8e86689f4a5_tuple, 0 ) );

        if ( tmp_called_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 42;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_33 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 43;
        tmp_called_name_32 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_33, &PyTuple_GET_ITEM( const_tuple_str_digest_db82dd148fa872efe07c2eb351c2c33f_tuple, 0 ) );

        if ( tmp_called_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );

            exception_lineno = 43;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_35 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 44;
        tmp_called_name_34 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_35, &PyTuple_GET_ITEM( const_tuple_str_digest_ee5885bc3d3f26ad23a483060cd09f03_tuple, 0 ) );

        if ( tmp_called_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );

            exception_lineno = 44;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_37 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 45;
        tmp_called_name_36 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_37, &PyTuple_GET_ITEM( const_tuple_str_digest_a71cd96de41b1d83d8e604317aa9a7eb_tuple, 0 ) );

        if ( tmp_called_name_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );

            exception_lineno = 45;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_39 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 46;
        tmp_called_name_38 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_39, &PyTuple_GET_ITEM( const_tuple_str_digest_f2a75caf8ec802255b8dafde1721755d_tuple, 0 ) );

        if ( tmp_called_name_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );

            exception_lineno = 46;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_41 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 47;
        tmp_called_name_40 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_41, &PyTuple_GET_ITEM( const_tuple_str_digest_9d28ad88315d87cb7857dca59e1b839e_tuple, 0 ) );

        if ( tmp_called_name_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );

            exception_lineno = 47;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_43 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 48;
        tmp_called_name_42 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_43, &PyTuple_GET_ITEM( const_tuple_str_digest_2da2a9c575783ef454fa30e9f4e246ee_tuple, 0 ) );

        if ( tmp_called_name_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );

            exception_lineno = 48;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_45 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 49;
        tmp_called_name_44 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_45, &PyTuple_GET_ITEM( const_tuple_str_digest_3724a8ee88291331504b26b811efeb5a_tuple, 0 ) );

        if ( tmp_called_name_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );

            exception_lineno = 49;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_47 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 50;
        tmp_called_name_46 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_47, &PyTuple_GET_ITEM( const_tuple_str_digest_d8156c92c3ac39743ec9345e59bfd2ed_tuple, 0 ) );

        if ( tmp_called_name_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );

            exception_lineno = 50;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_49 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 51;
        tmp_called_name_48 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_49, &PyTuple_GET_ITEM( const_tuple_str_digest_5bbf98712488fb50e0bf411a1625446e_tuple, 0 ) );

        if ( tmp_called_name_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );

            exception_lineno = 51;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_51 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 52;
        tmp_called_name_50 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_51, &PyTuple_GET_ITEM( const_tuple_str_digest_73eb09babb290e590b9a69835b5223c4_tuple, 0 ) );

        if ( tmp_called_name_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );

            exception_lineno = 52;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_53 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 53;
        tmp_called_name_52 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_53, &PyTuple_GET_ITEM( const_tuple_str_digest_e5fd2b9ef2333d150f68273f43406bf0_tuple, 0 ) );

        if ( tmp_called_name_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );

            exception_lineno = 53;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_55 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 54;
        tmp_called_name_54 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_55, &PyTuple_GET_ITEM( const_tuple_str_plain_f1_tuple, 0 ) );

        if ( tmp_called_name_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );

            exception_lineno = 54;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_57 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 55;
        tmp_called_name_56 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_57, &PyTuple_GET_ITEM( const_tuple_str_plain_f2_tuple, 0 ) );

        if ( tmp_called_name_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );

            exception_lineno = 55;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_59 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 56;
        tmp_called_name_58 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_59, &PyTuple_GET_ITEM( const_tuple_str_plain_f3_tuple, 0 ) );

        if ( tmp_called_name_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );

            exception_lineno = 56;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_61 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 57;
        tmp_called_name_60 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_61, &PyTuple_GET_ITEM( const_tuple_str_plain_f4_tuple, 0 ) );

        if ( tmp_called_name_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );

            exception_lineno = 57;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_63 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 58;
        tmp_called_name_62 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_63, &PyTuple_GET_ITEM( const_tuple_str_plain_f5_tuple, 0 ) );

        if ( tmp_called_name_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );

            exception_lineno = 58;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_65 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 59;
        tmp_called_name_64 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_65, &PyTuple_GET_ITEM( const_tuple_str_plain_f6_tuple, 0 ) );

        if ( tmp_called_name_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );

            exception_lineno = 59;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_67 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 60;
        tmp_called_name_66 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_67, &PyTuple_GET_ITEM( const_tuple_str_plain_f7_tuple, 0 ) );

        if ( tmp_called_name_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );

            exception_lineno = 60;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_69 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 61;
        tmp_called_name_68 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_69, &PyTuple_GET_ITEM( const_tuple_str_plain_f8_tuple, 0 ) );

        if ( tmp_called_name_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );

            exception_lineno = 61;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_71 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 62;
        tmp_called_name_70 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_71, &PyTuple_GET_ITEM( const_tuple_str_plain_f9_tuple, 0 ) );

        if ( tmp_called_name_70 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );

            exception_lineno = 62;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_73 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 63;
        tmp_called_name_72 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_73, &PyTuple_GET_ITEM( const_tuple_str_plain_f10_tuple, 0 ) );

        if ( tmp_called_name_72 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );

            exception_lineno = 63;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_75 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 64;
        tmp_called_name_74 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_75, &PyTuple_GET_ITEM( const_tuple_str_plain_f11_tuple, 0 ) );

        if ( tmp_called_name_74 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );

            exception_lineno = 64;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_77 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 65;
        tmp_called_name_76 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_77, &PyTuple_GET_ITEM( const_tuple_str_plain_f12_tuple, 0 ) );

        if ( tmp_called_name_76 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );

            exception_lineno = 65;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_79 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 66;
        tmp_called_name_78 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_79, &PyTuple_GET_ITEM( const_tuple_str_plain_f13_tuple, 0 ) );

        if ( tmp_called_name_78 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );

            exception_lineno = 66;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_81 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 67;
        tmp_called_name_80 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_81, &PyTuple_GET_ITEM( const_tuple_str_plain_f14_tuple, 0 ) );

        if ( tmp_called_name_80 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );

            exception_lineno = 67;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_83 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 68;
        tmp_called_name_82 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_83, &PyTuple_GET_ITEM( const_tuple_str_plain_f15_tuple, 0 ) );

        if ( tmp_called_name_82 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );

            exception_lineno = 68;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_85 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 69;
        tmp_called_name_84 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_85, &PyTuple_GET_ITEM( const_tuple_str_plain_f16_tuple, 0 ) );

        if ( tmp_called_name_84 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );

            exception_lineno = 69;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_87 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 70;
        tmp_called_name_86 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_87, &PyTuple_GET_ITEM( const_tuple_str_plain_f17_tuple, 0 ) );

        if ( tmp_called_name_86 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );

            exception_lineno = 70;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_89 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 71;
        tmp_called_name_88 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_89, &PyTuple_GET_ITEM( const_tuple_str_plain_f18_tuple, 0 ) );

        if ( tmp_called_name_88 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );

            exception_lineno = 71;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_91 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 72;
        tmp_called_name_90 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_91, &PyTuple_GET_ITEM( const_tuple_str_plain_f19_tuple, 0 ) );

        if ( tmp_called_name_90 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );

            exception_lineno = 72;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_93 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 73;
        tmp_called_name_92 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_93, &PyTuple_GET_ITEM( const_tuple_str_plain_f20_tuple, 0 ) );

        if ( tmp_called_name_92 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );

            exception_lineno = 73;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_95 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 74;
        tmp_called_name_94 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_95, &PyTuple_GET_ITEM( const_tuple_str_digest_594981de1deffed7357a6afed608966a_tuple, 0 ) );

        if ( tmp_called_name_94 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );

            exception_lineno = 74;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_97 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 75;
        tmp_called_name_96 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_97, &PyTuple_GET_ITEM( const_tuple_str_digest_1ceaeb7fd0ea5d7c27a08d77a50fc9bf_tuple, 0 ) );

        if ( tmp_called_name_96 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );

            exception_lineno = 75;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_99 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 76;
        tmp_called_name_98 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_99, &PyTuple_GET_ITEM( const_tuple_str_digest_cccb566811e08cc4fd2e3ae406952507_tuple, 0 ) );

        if ( tmp_called_name_98 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );

            exception_lineno = 76;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_101 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 77;
        tmp_called_name_100 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_101, &PyTuple_GET_ITEM( const_tuple_str_digest_7e1a6d304a63e0ac153bf5f43c885749_tuple, 0 ) );

        if ( tmp_called_name_100 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );

            exception_lineno = 77;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_103 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 78;
        tmp_called_name_102 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_103, &PyTuple_GET_ITEM( const_tuple_str_digest_7230e58f12e9188dd6e7dddd9636a803_tuple, 0 ) );

        if ( tmp_called_name_102 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );

            exception_lineno = 78;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_105 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 79;
        tmp_called_name_104 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_105, &PyTuple_GET_ITEM( const_tuple_str_plain_backspace_tuple, 0 ) );

        if ( tmp_called_name_104 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );

            exception_lineno = 79;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_107 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 80;
        tmp_called_name_106 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_107, &PyTuple_GET_ITEM( const_tuple_str_plain_up_tuple, 0 ) );

        if ( tmp_called_name_106 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );

            exception_lineno = 80;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_109 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 81;
        tmp_called_name_108 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_109, &PyTuple_GET_ITEM( const_tuple_str_plain_down_tuple, 0 ) );

        if ( tmp_called_name_108 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );

            exception_lineno = 81;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_111 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 82;
        tmp_called_name_110 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_111, &PyTuple_GET_ITEM( const_tuple_str_plain_right_tuple, 0 ) );

        if ( tmp_called_name_110 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );

            exception_lineno = 82;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_113 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 83;
        tmp_called_name_112 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_113, &PyTuple_GET_ITEM( const_tuple_str_plain_left_tuple, 0 ) );

        if ( tmp_called_name_112 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );

            exception_lineno = 83;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_115 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 84;
        tmp_called_name_114 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_115, &PyTuple_GET_ITEM( const_tuple_str_digest_b1b2ece871cd7fad7169ffac4c13c155_tuple, 0 ) );

        if ( tmp_called_name_114 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );

            exception_lineno = 84;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_117 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 85;
        tmp_called_name_116 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_117, &PyTuple_GET_ITEM( const_tuple_str_digest_444f48b1092beda0606928a9606f012b_tuple, 0 ) );

        if ( tmp_called_name_116 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );

            exception_lineno = 85;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_119 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 86;
        tmp_called_name_118 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_119, &PyTuple_GET_ITEM( const_tuple_str_digest_e9a7688ceb262f6815aa65f8ee8703ab_tuple, 0 ) );

        if ( tmp_called_name_118 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );

            exception_lineno = 86;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_121 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 87;
        tmp_called_name_120 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_121, &PyTuple_GET_ITEM( const_tuple_str_digest_94c7a0cb80c5a4f2e9a8ddd3d95d0c88_tuple, 0 ) );

        if ( tmp_called_name_120 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );

            exception_lineno = 87;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_123 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 88;
        tmp_called_name_122 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_123, &PyTuple_GET_ITEM( const_tuple_str_plain_home_tuple, 0 ) );

        if ( tmp_called_name_122 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );

            exception_lineno = 88;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_125 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 89;
        tmp_called_name_124 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_125, &PyTuple_GET_ITEM( const_tuple_str_plain_end_tuple, 0 ) );

        if ( tmp_called_name_124 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );

            exception_lineno = 89;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_127 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 90;
        tmp_called_name_126 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_127, &PyTuple_GET_ITEM( const_tuple_str_plain_delete_tuple, 0 ) );

        if ( tmp_called_name_126 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );

            exception_lineno = 90;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_129 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 91;
        tmp_called_name_128 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_129, &PyTuple_GET_ITEM( const_tuple_str_digest_6dfa089e1785bfb099fb5067adfbc10c_tuple, 0 ) );

        if ( tmp_called_name_128 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );

            exception_lineno = 91;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_131 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 92;
        tmp_called_name_130 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_131, &PyTuple_GET_ITEM( const_tuple_str_digest_9102ee6eb9329cb31f5264512e00165d_tuple, 0 ) );

        if ( tmp_called_name_130 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );

            exception_lineno = 92;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_133 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 93;
        tmp_called_name_132 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_133, &PyTuple_GET_ITEM( const_tuple_str_plain_pageup_tuple, 0 ) );

        if ( tmp_called_name_132 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );

            exception_lineno = 93;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_135 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 94;
        tmp_called_name_134 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_135, &PyTuple_GET_ITEM( const_tuple_str_plain_pagedown_tuple, 0 ) );

        if ( tmp_called_name_134 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );

            exception_lineno = 94;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_137 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 95;
        tmp_called_name_136 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_137, &PyTuple_GET_ITEM( const_tuple_str_digest_be077fe6fbb1013afb24e5934140597e_tuple, 0 ) );

        if ( tmp_called_name_136 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );

            exception_lineno = 95;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_139 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 96;
        tmp_called_name_138 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_139, &PyTuple_GET_ITEM( const_tuple_str_plain_tab_tuple, 0 ) );

        if ( tmp_called_name_138 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );

            exception_lineno = 96;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_141 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 97;
        tmp_called_name_140 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_141, &PyTuple_GET_ITEM( const_tuple_str_digest_3e8226f37423ae997480ac891b15fb8d_tuple, 0 ) );

        if ( tmp_called_name_140 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );

            exception_lineno = 97;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_143 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 98;
        tmp_called_name_142 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_143, &PyTuple_GET_ITEM( const_tuple_str_digest_a969c3e8d69c0076a5175c71af8240d8_tuple, 0 ) );

        if ( tmp_called_name_142 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );

            exception_lineno = 98;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_145 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 99;
        tmp_called_name_144 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_145, &PyTuple_GET_ITEM( const_tuple_str_digest_334b87c0568648956e6ef5f0f9d62e81_tuple, 0 ) );

        if ( tmp_called_name_144 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );
            Py_DECREF( tmp_called_name_142 );

            exception_lineno = 99;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_147 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 100;
        tmp_called_name_146 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_147, &PyTuple_GET_ITEM( const_tuple_str_digest_993bf0215ec53a0afc0447dbbdf085c9_tuple, 0 ) );

        if ( tmp_called_name_146 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );
            Py_DECREF( tmp_called_name_142 );
            Py_DECREF( tmp_called_name_144 );

            exception_lineno = 100;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_149 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 101;
        tmp_called_name_148 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_149, &PyTuple_GET_ITEM( const_tuple_str_plain_insert_tuple, 0 ) );

        if ( tmp_called_name_148 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );
            Py_DECREF( tmp_called_name_142 );
            Py_DECREF( tmp_called_name_144 );
            Py_DECREF( tmp_called_name_146 );

            exception_lineno = 101;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_handle );
        tmp_called_name_151 = var_handle;
        tmp_mvar_value_4 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_4 == NULL ))
        {
            tmp_mvar_value_4 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_4 == NULL )
        {
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );
            Py_DECREF( tmp_called_name_142 );
            Py_DECREF( tmp_called_name_144 );
            Py_DECREF( tmp_called_name_146 );
            Py_DECREF( tmp_called_name_148 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 102;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_2 = tmp_mvar_value_4;
        tmp_args_element_name_75 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_Ignore );
        if ( tmp_args_element_name_75 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );
            Py_DECREF( tmp_called_name_142 );
            Py_DECREF( tmp_called_name_144 );
            Py_DECREF( tmp_called_name_146 );
            Py_DECREF( tmp_called_name_148 );

            exception_lineno = 102;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 102;
        {
            PyObject *call_args[] = { tmp_args_element_name_75 };
            tmp_called_name_150 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_151, call_args );
        }

        Py_DECREF( tmp_args_element_name_75 );
        if ( tmp_called_name_150 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );
            Py_DECREF( tmp_called_name_142 );
            Py_DECREF( tmp_called_name_144 );
            Py_DECREF( tmp_called_name_146 );
            Py_DECREF( tmp_called_name_148 );

            exception_lineno = 102;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_76 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_1__(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 102;
        {
            PyObject *call_args[] = { tmp_args_element_name_76 };
            tmp_args_element_name_74 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_150, call_args );
        }

        Py_DECREF( tmp_called_name_150 );
        Py_DECREF( tmp_args_element_name_76 );
        if ( tmp_args_element_name_74 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );
            Py_DECREF( tmp_called_name_142 );
            Py_DECREF( tmp_called_name_144 );
            Py_DECREF( tmp_called_name_146 );
            Py_DECREF( tmp_called_name_148 );

            exception_lineno = 102;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 101;
        {
            PyObject *call_args[] = { tmp_args_element_name_74 };
            tmp_args_element_name_73 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_148, call_args );
        }

        Py_DECREF( tmp_called_name_148 );
        Py_DECREF( tmp_args_element_name_74 );
        if ( tmp_args_element_name_73 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );
            Py_DECREF( tmp_called_name_142 );
            Py_DECREF( tmp_called_name_144 );
            Py_DECREF( tmp_called_name_146 );

            exception_lineno = 101;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 100;
        {
            PyObject *call_args[] = { tmp_args_element_name_73 };
            tmp_args_element_name_72 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_146, call_args );
        }

        Py_DECREF( tmp_called_name_146 );
        Py_DECREF( tmp_args_element_name_73 );
        if ( tmp_args_element_name_72 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );
            Py_DECREF( tmp_called_name_142 );
            Py_DECREF( tmp_called_name_144 );

            exception_lineno = 100;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 99;
        {
            PyObject *call_args[] = { tmp_args_element_name_72 };
            tmp_args_element_name_71 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_144, call_args );
        }

        Py_DECREF( tmp_called_name_144 );
        Py_DECREF( tmp_args_element_name_72 );
        if ( tmp_args_element_name_71 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );
            Py_DECREF( tmp_called_name_142 );

            exception_lineno = 99;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 98;
        {
            PyObject *call_args[] = { tmp_args_element_name_71 };
            tmp_args_element_name_70 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_142, call_args );
        }

        Py_DECREF( tmp_called_name_142 );
        Py_DECREF( tmp_args_element_name_71 );
        if ( tmp_args_element_name_70 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );
            Py_DECREF( tmp_called_name_140 );

            exception_lineno = 98;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 97;
        {
            PyObject *call_args[] = { tmp_args_element_name_70 };
            tmp_args_element_name_69 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_140, call_args );
        }

        Py_DECREF( tmp_called_name_140 );
        Py_DECREF( tmp_args_element_name_70 );
        if ( tmp_args_element_name_69 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );
            Py_DECREF( tmp_called_name_138 );

            exception_lineno = 97;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 96;
        {
            PyObject *call_args[] = { tmp_args_element_name_69 };
            tmp_args_element_name_68 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_138, call_args );
        }

        Py_DECREF( tmp_called_name_138 );
        Py_DECREF( tmp_args_element_name_69 );
        if ( tmp_args_element_name_68 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );
            Py_DECREF( tmp_called_name_136 );

            exception_lineno = 96;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 95;
        {
            PyObject *call_args[] = { tmp_args_element_name_68 };
            tmp_args_element_name_67 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_136, call_args );
        }

        Py_DECREF( tmp_called_name_136 );
        Py_DECREF( tmp_args_element_name_68 );
        if ( tmp_args_element_name_67 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );
            Py_DECREF( tmp_called_name_134 );

            exception_lineno = 95;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 94;
        {
            PyObject *call_args[] = { tmp_args_element_name_67 };
            tmp_args_element_name_66 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_134, call_args );
        }

        Py_DECREF( tmp_called_name_134 );
        Py_DECREF( tmp_args_element_name_67 );
        if ( tmp_args_element_name_66 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );
            Py_DECREF( tmp_called_name_132 );

            exception_lineno = 94;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 93;
        {
            PyObject *call_args[] = { tmp_args_element_name_66 };
            tmp_args_element_name_65 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_132, call_args );
        }

        Py_DECREF( tmp_called_name_132 );
        Py_DECREF( tmp_args_element_name_66 );
        if ( tmp_args_element_name_65 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );
            Py_DECREF( tmp_called_name_130 );

            exception_lineno = 93;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 92;
        {
            PyObject *call_args[] = { tmp_args_element_name_65 };
            tmp_args_element_name_64 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_130, call_args );
        }

        Py_DECREF( tmp_called_name_130 );
        Py_DECREF( tmp_args_element_name_65 );
        if ( tmp_args_element_name_64 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );
            Py_DECREF( tmp_called_name_128 );

            exception_lineno = 92;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 91;
        {
            PyObject *call_args[] = { tmp_args_element_name_64 };
            tmp_args_element_name_63 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_128, call_args );
        }

        Py_DECREF( tmp_called_name_128 );
        Py_DECREF( tmp_args_element_name_64 );
        if ( tmp_args_element_name_63 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );
            Py_DECREF( tmp_called_name_126 );

            exception_lineno = 91;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 90;
        {
            PyObject *call_args[] = { tmp_args_element_name_63 };
            tmp_args_element_name_62 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_126, call_args );
        }

        Py_DECREF( tmp_called_name_126 );
        Py_DECREF( tmp_args_element_name_63 );
        if ( tmp_args_element_name_62 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );
            Py_DECREF( tmp_called_name_124 );

            exception_lineno = 90;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 89;
        {
            PyObject *call_args[] = { tmp_args_element_name_62 };
            tmp_args_element_name_61 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_124, call_args );
        }

        Py_DECREF( tmp_called_name_124 );
        Py_DECREF( tmp_args_element_name_62 );
        if ( tmp_args_element_name_61 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );
            Py_DECREF( tmp_called_name_122 );

            exception_lineno = 89;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 88;
        {
            PyObject *call_args[] = { tmp_args_element_name_61 };
            tmp_args_element_name_60 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_122, call_args );
        }

        Py_DECREF( tmp_called_name_122 );
        Py_DECREF( tmp_args_element_name_61 );
        if ( tmp_args_element_name_60 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );
            Py_DECREF( tmp_called_name_120 );

            exception_lineno = 88;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 87;
        {
            PyObject *call_args[] = { tmp_args_element_name_60 };
            tmp_args_element_name_59 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_120, call_args );
        }

        Py_DECREF( tmp_called_name_120 );
        Py_DECREF( tmp_args_element_name_60 );
        if ( tmp_args_element_name_59 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );
            Py_DECREF( tmp_called_name_118 );

            exception_lineno = 87;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 86;
        {
            PyObject *call_args[] = { tmp_args_element_name_59 };
            tmp_args_element_name_58 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_118, call_args );
        }

        Py_DECREF( tmp_called_name_118 );
        Py_DECREF( tmp_args_element_name_59 );
        if ( tmp_args_element_name_58 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );
            Py_DECREF( tmp_called_name_116 );

            exception_lineno = 86;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 85;
        {
            PyObject *call_args[] = { tmp_args_element_name_58 };
            tmp_args_element_name_57 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_116, call_args );
        }

        Py_DECREF( tmp_called_name_116 );
        Py_DECREF( tmp_args_element_name_58 );
        if ( tmp_args_element_name_57 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );
            Py_DECREF( tmp_called_name_114 );

            exception_lineno = 85;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 84;
        {
            PyObject *call_args[] = { tmp_args_element_name_57 };
            tmp_args_element_name_56 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_114, call_args );
        }

        Py_DECREF( tmp_called_name_114 );
        Py_DECREF( tmp_args_element_name_57 );
        if ( tmp_args_element_name_56 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );
            Py_DECREF( tmp_called_name_112 );

            exception_lineno = 84;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 83;
        {
            PyObject *call_args[] = { tmp_args_element_name_56 };
            tmp_args_element_name_55 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_112, call_args );
        }

        Py_DECREF( tmp_called_name_112 );
        Py_DECREF( tmp_args_element_name_56 );
        if ( tmp_args_element_name_55 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );
            Py_DECREF( tmp_called_name_110 );

            exception_lineno = 83;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 82;
        {
            PyObject *call_args[] = { tmp_args_element_name_55 };
            tmp_args_element_name_54 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_110, call_args );
        }

        Py_DECREF( tmp_called_name_110 );
        Py_DECREF( tmp_args_element_name_55 );
        if ( tmp_args_element_name_54 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );
            Py_DECREF( tmp_called_name_108 );

            exception_lineno = 82;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 81;
        {
            PyObject *call_args[] = { tmp_args_element_name_54 };
            tmp_args_element_name_53 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_108, call_args );
        }

        Py_DECREF( tmp_called_name_108 );
        Py_DECREF( tmp_args_element_name_54 );
        if ( tmp_args_element_name_53 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );
            Py_DECREF( tmp_called_name_106 );

            exception_lineno = 81;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 80;
        {
            PyObject *call_args[] = { tmp_args_element_name_53 };
            tmp_args_element_name_52 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_106, call_args );
        }

        Py_DECREF( tmp_called_name_106 );
        Py_DECREF( tmp_args_element_name_53 );
        if ( tmp_args_element_name_52 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );
            Py_DECREF( tmp_called_name_104 );

            exception_lineno = 80;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 79;
        {
            PyObject *call_args[] = { tmp_args_element_name_52 };
            tmp_args_element_name_51 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_104, call_args );
        }

        Py_DECREF( tmp_called_name_104 );
        Py_DECREF( tmp_args_element_name_52 );
        if ( tmp_args_element_name_51 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );
            Py_DECREF( tmp_called_name_102 );

            exception_lineno = 79;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 78;
        {
            PyObject *call_args[] = { tmp_args_element_name_51 };
            tmp_args_element_name_50 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_102, call_args );
        }

        Py_DECREF( tmp_called_name_102 );
        Py_DECREF( tmp_args_element_name_51 );
        if ( tmp_args_element_name_50 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );
            Py_DECREF( tmp_called_name_100 );

            exception_lineno = 78;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 77;
        {
            PyObject *call_args[] = { tmp_args_element_name_50 };
            tmp_args_element_name_49 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_100, call_args );
        }

        Py_DECREF( tmp_called_name_100 );
        Py_DECREF( tmp_args_element_name_50 );
        if ( tmp_args_element_name_49 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );
            Py_DECREF( tmp_called_name_98 );

            exception_lineno = 77;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 76;
        {
            PyObject *call_args[] = { tmp_args_element_name_49 };
            tmp_args_element_name_48 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_98, call_args );
        }

        Py_DECREF( tmp_called_name_98 );
        Py_DECREF( tmp_args_element_name_49 );
        if ( tmp_args_element_name_48 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );
            Py_DECREF( tmp_called_name_96 );

            exception_lineno = 76;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 75;
        {
            PyObject *call_args[] = { tmp_args_element_name_48 };
            tmp_args_element_name_47 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_96, call_args );
        }

        Py_DECREF( tmp_called_name_96 );
        Py_DECREF( tmp_args_element_name_48 );
        if ( tmp_args_element_name_47 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );
            Py_DECREF( tmp_called_name_94 );

            exception_lineno = 75;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 74;
        {
            PyObject *call_args[] = { tmp_args_element_name_47 };
            tmp_args_element_name_46 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_94, call_args );
        }

        Py_DECREF( tmp_called_name_94 );
        Py_DECREF( tmp_args_element_name_47 );
        if ( tmp_args_element_name_46 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );
            Py_DECREF( tmp_called_name_92 );

            exception_lineno = 74;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 73;
        {
            PyObject *call_args[] = { tmp_args_element_name_46 };
            tmp_args_element_name_45 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_92, call_args );
        }

        Py_DECREF( tmp_called_name_92 );
        Py_DECREF( tmp_args_element_name_46 );
        if ( tmp_args_element_name_45 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );
            Py_DECREF( tmp_called_name_90 );

            exception_lineno = 73;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 72;
        {
            PyObject *call_args[] = { tmp_args_element_name_45 };
            tmp_args_element_name_44 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_90, call_args );
        }

        Py_DECREF( tmp_called_name_90 );
        Py_DECREF( tmp_args_element_name_45 );
        if ( tmp_args_element_name_44 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );
            Py_DECREF( tmp_called_name_88 );

            exception_lineno = 72;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 71;
        {
            PyObject *call_args[] = { tmp_args_element_name_44 };
            tmp_args_element_name_43 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_88, call_args );
        }

        Py_DECREF( tmp_called_name_88 );
        Py_DECREF( tmp_args_element_name_44 );
        if ( tmp_args_element_name_43 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );
            Py_DECREF( tmp_called_name_86 );

            exception_lineno = 71;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 70;
        {
            PyObject *call_args[] = { tmp_args_element_name_43 };
            tmp_args_element_name_42 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_86, call_args );
        }

        Py_DECREF( tmp_called_name_86 );
        Py_DECREF( tmp_args_element_name_43 );
        if ( tmp_args_element_name_42 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );
            Py_DECREF( tmp_called_name_84 );

            exception_lineno = 70;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 69;
        {
            PyObject *call_args[] = { tmp_args_element_name_42 };
            tmp_args_element_name_41 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_84, call_args );
        }

        Py_DECREF( tmp_called_name_84 );
        Py_DECREF( tmp_args_element_name_42 );
        if ( tmp_args_element_name_41 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );
            Py_DECREF( tmp_called_name_82 );

            exception_lineno = 69;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 68;
        {
            PyObject *call_args[] = { tmp_args_element_name_41 };
            tmp_args_element_name_40 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_82, call_args );
        }

        Py_DECREF( tmp_called_name_82 );
        Py_DECREF( tmp_args_element_name_41 );
        if ( tmp_args_element_name_40 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );
            Py_DECREF( tmp_called_name_80 );

            exception_lineno = 68;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 67;
        {
            PyObject *call_args[] = { tmp_args_element_name_40 };
            tmp_args_element_name_39 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_80, call_args );
        }

        Py_DECREF( tmp_called_name_80 );
        Py_DECREF( tmp_args_element_name_40 );
        if ( tmp_args_element_name_39 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );
            Py_DECREF( tmp_called_name_78 );

            exception_lineno = 67;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 66;
        {
            PyObject *call_args[] = { tmp_args_element_name_39 };
            tmp_args_element_name_38 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_78, call_args );
        }

        Py_DECREF( tmp_called_name_78 );
        Py_DECREF( tmp_args_element_name_39 );
        if ( tmp_args_element_name_38 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );
            Py_DECREF( tmp_called_name_76 );

            exception_lineno = 66;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 65;
        {
            PyObject *call_args[] = { tmp_args_element_name_38 };
            tmp_args_element_name_37 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_76, call_args );
        }

        Py_DECREF( tmp_called_name_76 );
        Py_DECREF( tmp_args_element_name_38 );
        if ( tmp_args_element_name_37 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );
            Py_DECREF( tmp_called_name_74 );

            exception_lineno = 65;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 64;
        {
            PyObject *call_args[] = { tmp_args_element_name_37 };
            tmp_args_element_name_36 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_74, call_args );
        }

        Py_DECREF( tmp_called_name_74 );
        Py_DECREF( tmp_args_element_name_37 );
        if ( tmp_args_element_name_36 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );
            Py_DECREF( tmp_called_name_72 );

            exception_lineno = 64;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 63;
        {
            PyObject *call_args[] = { tmp_args_element_name_36 };
            tmp_args_element_name_35 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_72, call_args );
        }

        Py_DECREF( tmp_called_name_72 );
        Py_DECREF( tmp_args_element_name_36 );
        if ( tmp_args_element_name_35 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );
            Py_DECREF( tmp_called_name_70 );

            exception_lineno = 63;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 62;
        {
            PyObject *call_args[] = { tmp_args_element_name_35 };
            tmp_args_element_name_34 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_70, call_args );
        }

        Py_DECREF( tmp_called_name_70 );
        Py_DECREF( tmp_args_element_name_35 );
        if ( tmp_args_element_name_34 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );
            Py_DECREF( tmp_called_name_68 );

            exception_lineno = 62;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 61;
        {
            PyObject *call_args[] = { tmp_args_element_name_34 };
            tmp_args_element_name_33 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_68, call_args );
        }

        Py_DECREF( tmp_called_name_68 );
        Py_DECREF( tmp_args_element_name_34 );
        if ( tmp_args_element_name_33 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );
            Py_DECREF( tmp_called_name_66 );

            exception_lineno = 61;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 60;
        {
            PyObject *call_args[] = { tmp_args_element_name_33 };
            tmp_args_element_name_32 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_66, call_args );
        }

        Py_DECREF( tmp_called_name_66 );
        Py_DECREF( tmp_args_element_name_33 );
        if ( tmp_args_element_name_32 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );
            Py_DECREF( tmp_called_name_64 );

            exception_lineno = 60;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 59;
        {
            PyObject *call_args[] = { tmp_args_element_name_32 };
            tmp_args_element_name_31 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_64, call_args );
        }

        Py_DECREF( tmp_called_name_64 );
        Py_DECREF( tmp_args_element_name_32 );
        if ( tmp_args_element_name_31 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );
            Py_DECREF( tmp_called_name_62 );

            exception_lineno = 59;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 58;
        {
            PyObject *call_args[] = { tmp_args_element_name_31 };
            tmp_args_element_name_30 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_62, call_args );
        }

        Py_DECREF( tmp_called_name_62 );
        Py_DECREF( tmp_args_element_name_31 );
        if ( tmp_args_element_name_30 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );
            Py_DECREF( tmp_called_name_60 );

            exception_lineno = 58;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 57;
        {
            PyObject *call_args[] = { tmp_args_element_name_30 };
            tmp_args_element_name_29 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_60, call_args );
        }

        Py_DECREF( tmp_called_name_60 );
        Py_DECREF( tmp_args_element_name_30 );
        if ( tmp_args_element_name_29 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );
            Py_DECREF( tmp_called_name_58 );

            exception_lineno = 57;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 56;
        {
            PyObject *call_args[] = { tmp_args_element_name_29 };
            tmp_args_element_name_28 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_58, call_args );
        }

        Py_DECREF( tmp_called_name_58 );
        Py_DECREF( tmp_args_element_name_29 );
        if ( tmp_args_element_name_28 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );
            Py_DECREF( tmp_called_name_56 );

            exception_lineno = 56;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 55;
        {
            PyObject *call_args[] = { tmp_args_element_name_28 };
            tmp_args_element_name_27 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_56, call_args );
        }

        Py_DECREF( tmp_called_name_56 );
        Py_DECREF( tmp_args_element_name_28 );
        if ( tmp_args_element_name_27 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );
            Py_DECREF( tmp_called_name_54 );

            exception_lineno = 55;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 54;
        {
            PyObject *call_args[] = { tmp_args_element_name_27 };
            tmp_args_element_name_26 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_54, call_args );
        }

        Py_DECREF( tmp_called_name_54 );
        Py_DECREF( tmp_args_element_name_27 );
        if ( tmp_args_element_name_26 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );
            Py_DECREF( tmp_called_name_52 );

            exception_lineno = 54;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 53;
        {
            PyObject *call_args[] = { tmp_args_element_name_26 };
            tmp_args_element_name_25 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_52, call_args );
        }

        Py_DECREF( tmp_called_name_52 );
        Py_DECREF( tmp_args_element_name_26 );
        if ( tmp_args_element_name_25 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );
            Py_DECREF( tmp_called_name_50 );

            exception_lineno = 53;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 52;
        {
            PyObject *call_args[] = { tmp_args_element_name_25 };
            tmp_args_element_name_24 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_50, call_args );
        }

        Py_DECREF( tmp_called_name_50 );
        Py_DECREF( tmp_args_element_name_25 );
        if ( tmp_args_element_name_24 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );
            Py_DECREF( tmp_called_name_48 );

            exception_lineno = 52;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 51;
        {
            PyObject *call_args[] = { tmp_args_element_name_24 };
            tmp_args_element_name_23 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_48, call_args );
        }

        Py_DECREF( tmp_called_name_48 );
        Py_DECREF( tmp_args_element_name_24 );
        if ( tmp_args_element_name_23 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );
            Py_DECREF( tmp_called_name_46 );

            exception_lineno = 51;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 50;
        {
            PyObject *call_args[] = { tmp_args_element_name_23 };
            tmp_args_element_name_22 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_46, call_args );
        }

        Py_DECREF( tmp_called_name_46 );
        Py_DECREF( tmp_args_element_name_23 );
        if ( tmp_args_element_name_22 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );
            Py_DECREF( tmp_called_name_44 );

            exception_lineno = 50;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 49;
        {
            PyObject *call_args[] = { tmp_args_element_name_22 };
            tmp_args_element_name_21 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_44, call_args );
        }

        Py_DECREF( tmp_called_name_44 );
        Py_DECREF( tmp_args_element_name_22 );
        if ( tmp_args_element_name_21 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );
            Py_DECREF( tmp_called_name_42 );

            exception_lineno = 49;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 48;
        {
            PyObject *call_args[] = { tmp_args_element_name_21 };
            tmp_args_element_name_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_42, call_args );
        }

        Py_DECREF( tmp_called_name_42 );
        Py_DECREF( tmp_args_element_name_21 );
        if ( tmp_args_element_name_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );
            Py_DECREF( tmp_called_name_40 );

            exception_lineno = 48;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 47;
        {
            PyObject *call_args[] = { tmp_args_element_name_20 };
            tmp_args_element_name_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_40, call_args );
        }

        Py_DECREF( tmp_called_name_40 );
        Py_DECREF( tmp_args_element_name_20 );
        if ( tmp_args_element_name_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );
            Py_DECREF( tmp_called_name_38 );

            exception_lineno = 47;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 46;
        {
            PyObject *call_args[] = { tmp_args_element_name_19 };
            tmp_args_element_name_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_38, call_args );
        }

        Py_DECREF( tmp_called_name_38 );
        Py_DECREF( tmp_args_element_name_19 );
        if ( tmp_args_element_name_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );
            Py_DECREF( tmp_called_name_36 );

            exception_lineno = 46;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 45;
        {
            PyObject *call_args[] = { tmp_args_element_name_18 };
            tmp_args_element_name_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_36, call_args );
        }

        Py_DECREF( tmp_called_name_36 );
        Py_DECREF( tmp_args_element_name_18 );
        if ( tmp_args_element_name_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );
            Py_DECREF( tmp_called_name_34 );

            exception_lineno = 45;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 44;
        {
            PyObject *call_args[] = { tmp_args_element_name_17 };
            tmp_args_element_name_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_34, call_args );
        }

        Py_DECREF( tmp_called_name_34 );
        Py_DECREF( tmp_args_element_name_17 );
        if ( tmp_args_element_name_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );
            Py_DECREF( tmp_called_name_32 );

            exception_lineno = 44;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 43;
        {
            PyObject *call_args[] = { tmp_args_element_name_16 };
            tmp_args_element_name_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_32, call_args );
        }

        Py_DECREF( tmp_called_name_32 );
        Py_DECREF( tmp_args_element_name_16 );
        if ( tmp_args_element_name_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );
            Py_DECREF( tmp_called_name_30 );

            exception_lineno = 43;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 42;
        {
            PyObject *call_args[] = { tmp_args_element_name_15 };
            tmp_args_element_name_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_30, call_args );
        }

        Py_DECREF( tmp_called_name_30 );
        Py_DECREF( tmp_args_element_name_15 );
        if ( tmp_args_element_name_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );
            Py_DECREF( tmp_called_name_28 );

            exception_lineno = 42;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 41;
        {
            PyObject *call_args[] = { tmp_args_element_name_14 };
            tmp_args_element_name_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_28, call_args );
        }

        Py_DECREF( tmp_called_name_28 );
        Py_DECREF( tmp_args_element_name_14 );
        if ( tmp_args_element_name_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );
            Py_DECREF( tmp_called_name_26 );

            exception_lineno = 41;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 40;
        {
            PyObject *call_args[] = { tmp_args_element_name_13 };
            tmp_args_element_name_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_26, call_args );
        }

        Py_DECREF( tmp_called_name_26 );
        Py_DECREF( tmp_args_element_name_13 );
        if ( tmp_args_element_name_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );
            Py_DECREF( tmp_called_name_24 );

            exception_lineno = 40;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 39;
        {
            PyObject *call_args[] = { tmp_args_element_name_12 };
            tmp_args_element_name_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_24, call_args );
        }

        Py_DECREF( tmp_called_name_24 );
        Py_DECREF( tmp_args_element_name_12 );
        if ( tmp_args_element_name_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );
            Py_DECREF( tmp_called_name_22 );

            exception_lineno = 39;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 38;
        {
            PyObject *call_args[] = { tmp_args_element_name_11 };
            tmp_args_element_name_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_22, call_args );
        }

        Py_DECREF( tmp_called_name_22 );
        Py_DECREF( tmp_args_element_name_11 );
        if ( tmp_args_element_name_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );
            Py_DECREF( tmp_called_name_20 );

            exception_lineno = 38;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 37;
        {
            PyObject *call_args[] = { tmp_args_element_name_10 };
            tmp_args_element_name_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_20, call_args );
        }

        Py_DECREF( tmp_called_name_20 );
        Py_DECREF( tmp_args_element_name_10 );
        if ( tmp_args_element_name_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );
            Py_DECREF( tmp_called_name_18 );

            exception_lineno = 37;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 36;
        {
            PyObject *call_args[] = { tmp_args_element_name_9 };
            tmp_args_element_name_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_18, call_args );
        }

        Py_DECREF( tmp_called_name_18 );
        Py_DECREF( tmp_args_element_name_9 );
        if ( tmp_args_element_name_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );
            Py_DECREF( tmp_called_name_16 );

            exception_lineno = 36;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 35;
        {
            PyObject *call_args[] = { tmp_args_element_name_8 };
            tmp_args_element_name_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_16, call_args );
        }

        Py_DECREF( tmp_called_name_16 );
        Py_DECREF( tmp_args_element_name_8 );
        if ( tmp_args_element_name_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );
            Py_DECREF( tmp_called_name_14 );

            exception_lineno = 35;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 34;
        {
            PyObject *call_args[] = { tmp_args_element_name_7 };
            tmp_args_element_name_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_14, call_args );
        }

        Py_DECREF( tmp_called_name_14 );
        Py_DECREF( tmp_args_element_name_7 );
        if ( tmp_args_element_name_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );
            Py_DECREF( tmp_called_name_12 );

            exception_lineno = 34;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 33;
        {
            PyObject *call_args[] = { tmp_args_element_name_6 };
            tmp_args_element_name_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_12, call_args );
        }

        Py_DECREF( tmp_called_name_12 );
        Py_DECREF( tmp_args_element_name_6 );
        if ( tmp_args_element_name_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );
            Py_DECREF( tmp_called_name_10 );

            exception_lineno = 33;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 32;
        {
            PyObject *call_args[] = { tmp_args_element_name_5 };
            tmp_args_element_name_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_10, call_args );
        }

        Py_DECREF( tmp_called_name_10 );
        Py_DECREF( tmp_args_element_name_5 );
        if ( tmp_args_element_name_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );
            Py_DECREF( tmp_called_name_8 );

            exception_lineno = 32;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 31;
        {
            PyObject *call_args[] = { tmp_args_element_name_4 };
            tmp_args_element_name_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_8, call_args );
        }

        Py_DECREF( tmp_called_name_8 );
        Py_DECREF( tmp_args_element_name_4 );
        if ( tmp_args_element_name_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );
            Py_DECREF( tmp_called_name_6 );

            exception_lineno = 31;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 30;
        {
            PyObject *call_args[] = { tmp_args_element_name_3 };
            tmp_args_element_name_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_6, call_args );
        }

        Py_DECREF( tmp_called_name_6 );
        Py_DECREF( tmp_args_element_name_3 );
        if ( tmp_args_element_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );
            Py_DECREF( tmp_called_name_4 );

            exception_lineno = 30;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 29;
        {
            PyObject *call_args[] = { tmp_args_element_name_2 };
            tmp_args_element_name_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_4, call_args );
        }

        Py_DECREF( tmp_called_name_4 );
        Py_DECREF( tmp_args_element_name_2 );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_2 );

            exception_lineno = 29;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 28;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_assign_source_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_called_name_2 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 28;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var__ == NULL );
        var__ = tmp_assign_source_4;
    }
    {
        PyObject *tmp_called_name_152;
        PyObject *tmp_called_name_153;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_77;
        PyObject *tmp_called_name_154;
        PyObject *tmp_mvar_value_5;
        CHECK_OBJECT( var_handle );
        tmp_called_name_153 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 115;
        tmp_called_name_152 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_153, &PyTuple_GET_ITEM( const_tuple_str_plain_home_tuple, 0 ) );

        if ( tmp_called_name_152 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_5 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_5 == NULL ))
        {
            tmp_mvar_value_5 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_5 == NULL )
        {
            Py_DECREF( tmp_called_name_152 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 115;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_154 = tmp_mvar_value_5;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 115;
        tmp_args_element_name_77 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_154, &PyTuple_GET_ITEM( const_tuple_str_digest_637ae2db1c15757d3f68020910fc7af5_tuple, 0 ) );

        if ( tmp_args_element_name_77 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_152 );

            exception_lineno = 115;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 115;
        {
            PyObject *call_args[] = { tmp_args_element_name_77 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_152, call_args );
        }

        Py_DECREF( tmp_called_name_152 );
        Py_DECREF( tmp_args_element_name_77 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 115;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_called_name_155;
        PyObject *tmp_called_name_156;
        PyObject *tmp_call_result_2;
        PyObject *tmp_args_element_name_78;
        PyObject *tmp_called_name_157;
        PyObject *tmp_mvar_value_6;
        CHECK_OBJECT( var_handle );
        tmp_called_name_156 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 116;
        tmp_called_name_155 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_156, &PyTuple_GET_ITEM( const_tuple_str_plain_end_tuple, 0 ) );

        if ( tmp_called_name_155 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_6 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_6 == NULL ))
        {
            tmp_mvar_value_6 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_6 == NULL )
        {
            Py_DECREF( tmp_called_name_155 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 116;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_157 = tmp_mvar_value_6;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 116;
        tmp_args_element_name_78 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_157, &PyTuple_GET_ITEM( const_tuple_str_digest_4dcf85ba674584b7335275c899c0ab24_tuple, 0 ) );

        if ( tmp_args_element_name_78 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_155 );

            exception_lineno = 116;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 116;
        {
            PyObject *call_args[] = { tmp_args_element_name_78 };
            tmp_call_result_2 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_155, call_args );
        }

        Py_DECREF( tmp_called_name_155 );
        Py_DECREF( tmp_args_element_name_78 );
        if ( tmp_call_result_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 116;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_2 );
    }
    {
        PyObject *tmp_called_name_158;
        PyObject *tmp_called_name_159;
        PyObject *tmp_call_result_3;
        PyObject *tmp_args_element_name_79;
        PyObject *tmp_called_name_160;
        PyObject *tmp_mvar_value_7;
        CHECK_OBJECT( var_handle );
        tmp_called_name_159 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 117;
        tmp_called_name_158 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_159, &PyTuple_GET_ITEM( const_tuple_str_plain_left_tuple, 0 ) );

        if ( tmp_called_name_158 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_7 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_7 == NULL ))
        {
            tmp_mvar_value_7 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_7 == NULL )
        {
            Py_DECREF( tmp_called_name_158 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 117;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_160 = tmp_mvar_value_7;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 117;
        tmp_args_element_name_79 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_160, &PyTuple_GET_ITEM( const_tuple_str_digest_21e45c1f878d59fe84fced59f3e10b63_tuple, 0 ) );

        if ( tmp_args_element_name_79 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_158 );

            exception_lineno = 117;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 117;
        {
            PyObject *call_args[] = { tmp_args_element_name_79 };
            tmp_call_result_3 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_158, call_args );
        }

        Py_DECREF( tmp_called_name_158 );
        Py_DECREF( tmp_args_element_name_79 );
        if ( tmp_call_result_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 117;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_3 );
    }
    {
        PyObject *tmp_called_name_161;
        PyObject *tmp_called_name_162;
        PyObject *tmp_call_result_4;
        PyObject *tmp_args_element_name_80;
        PyObject *tmp_called_name_163;
        PyObject *tmp_mvar_value_8;
        CHECK_OBJECT( var_handle );
        tmp_called_name_162 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 118;
        tmp_called_name_161 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_162, &PyTuple_GET_ITEM( const_tuple_str_plain_right_tuple, 0 ) );

        if ( tmp_called_name_161 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_8 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_8 == NULL ))
        {
            tmp_mvar_value_8 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_8 == NULL )
        {
            Py_DECREF( tmp_called_name_161 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 118;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_163 = tmp_mvar_value_8;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 118;
        tmp_args_element_name_80 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_163, &PyTuple_GET_ITEM( const_tuple_str_digest_820ffb9c299a4fe53ab8241c74e4288a_tuple, 0 ) );

        if ( tmp_args_element_name_80 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_161 );

            exception_lineno = 118;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 118;
        {
            PyObject *call_args[] = { tmp_args_element_name_80 };
            tmp_call_result_4 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_161, call_args );
        }

        Py_DECREF( tmp_called_name_161 );
        Py_DECREF( tmp_args_element_name_80 );
        if ( tmp_call_result_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 118;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_4 );
    }
    {
        PyObject *tmp_called_name_164;
        PyObject *tmp_called_name_165;
        PyObject *tmp_call_result_5;
        PyObject *tmp_args_element_name_81;
        PyObject *tmp_called_name_166;
        PyObject *tmp_mvar_value_9;
        CHECK_OBJECT( var_handle );
        tmp_called_name_165 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 119;
        tmp_called_name_164 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_165, &PyTuple_GET_ITEM( const_tuple_str_digest_334b87c0568648956e6ef5f0f9d62e81_tuple, 0 ) );

        if ( tmp_called_name_164 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_9 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_9 == NULL ))
        {
            tmp_mvar_value_9 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_9 == NULL )
        {
            Py_DECREF( tmp_called_name_164 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 119;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_166 = tmp_mvar_value_9;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 119;
        tmp_args_element_name_81 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_166, &PyTuple_GET_ITEM( const_tuple_str_digest_af1adbb7bec7eaa2f6c08a25f5ab2b5d_tuple, 0 ) );

        if ( tmp_args_element_name_81 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_164 );

            exception_lineno = 119;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 119;
        {
            PyObject *call_args[] = { tmp_args_element_name_81 };
            tmp_call_result_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_164, call_args );
        }

        Py_DECREF( tmp_called_name_164 );
        Py_DECREF( tmp_args_element_name_81 );
        if ( tmp_call_result_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 119;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_5 );
    }
    {
        PyObject *tmp_called_name_167;
        PyObject *tmp_called_name_168;
        PyObject *tmp_call_result_6;
        PyObject *tmp_args_element_name_82;
        PyObject *tmp_called_name_169;
        PyObject *tmp_mvar_value_10;
        CHECK_OBJECT( var_handle );
        tmp_called_name_168 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 120;
        tmp_called_name_167 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_168, &PyTuple_GET_ITEM( const_tuple_str_digest_993bf0215ec53a0afc0447dbbdf085c9_tuple, 0 ) );

        if ( tmp_called_name_167 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_10 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_10 == NULL ))
        {
            tmp_mvar_value_10 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_10 == NULL )
        {
            Py_DECREF( tmp_called_name_167 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 120;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_169 = tmp_mvar_value_10;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 120;
        tmp_args_element_name_82 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_169, &PyTuple_GET_ITEM( const_tuple_str_digest_ffa4782c58075c6dd84b572edd3109a7_tuple, 0 ) );

        if ( tmp_args_element_name_82 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_167 );

            exception_lineno = 120;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 120;
        {
            PyObject *call_args[] = { tmp_args_element_name_82 };
            tmp_call_result_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_167, call_args );
        }

        Py_DECREF( tmp_called_name_167 );
        Py_DECREF( tmp_args_element_name_82 );
        if ( tmp_call_result_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 120;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_6 );
    }
    {
        PyObject *tmp_called_name_170;
        PyObject *tmp_called_name_171;
        PyObject *tmp_call_result_7;
        PyObject *tmp_args_element_name_83;
        PyObject *tmp_called_name_172;
        PyObject *tmp_mvar_value_11;
        CHECK_OBJECT( var_handle );
        tmp_called_name_171 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 121;
        tmp_called_name_170 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_171, &PyTuple_GET_ITEM( const_tuple_str_digest_f643864f71e00e7bebfd9e255daa97dc_tuple, 0 ) );

        if ( tmp_called_name_170 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_11 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_11 == NULL ))
        {
            tmp_mvar_value_11 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_11 == NULL )
        {
            Py_DECREF( tmp_called_name_170 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 121;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_172 = tmp_mvar_value_11;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 121;
        tmp_args_element_name_83 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_172, &PyTuple_GET_ITEM( const_tuple_str_digest_4f8c7230e3d1a51c6fa37895ee70f054_tuple, 0 ) );

        if ( tmp_args_element_name_83 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_170 );

            exception_lineno = 121;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 121;
        {
            PyObject *call_args[] = { tmp_args_element_name_83 };
            tmp_call_result_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_170, call_args );
        }

        Py_DECREF( tmp_called_name_170 );
        Py_DECREF( tmp_args_element_name_83 );
        if ( tmp_call_result_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 121;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_7 );
    }
    {
        PyObject *tmp_called_name_173;
        PyObject *tmp_called_name_174;
        PyObject *tmp_args_name_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_call_result_8;
        PyObject *tmp_args_element_name_84;
        PyObject *tmp_called_name_175;
        PyObject *tmp_mvar_value_12;
        CHECK_OBJECT( var_handle );
        tmp_called_name_174 = var_handle;
        tmp_args_name_1 = const_tuple_str_digest_018e4648ae82f20bdde528d806765fb8_tuple;
        tmp_dict_key_1 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_1 = var_insert_mode;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 123;
        tmp_called_name_173 = CALL_FUNCTION( tmp_called_name_174, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_called_name_173 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_12 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_12 == NULL ))
        {
            tmp_mvar_value_12 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_12 == NULL )
        {
            Py_DECREF( tmp_called_name_173 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 123;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_175 = tmp_mvar_value_12;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 123;
        tmp_args_element_name_84 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_175, &PyTuple_GET_ITEM( const_tuple_str_digest_f6826d177b892c516eb853c8574210e2_tuple, 0 ) );

        if ( tmp_args_element_name_84 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_173 );

            exception_lineno = 123;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 123;
        {
            PyObject *call_args[] = { tmp_args_element_name_84 };
            tmp_call_result_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_173, call_args );
        }

        Py_DECREF( tmp_called_name_173 );
        Py_DECREF( tmp_args_element_name_84 );
        if ( tmp_call_result_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 123;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_8 );
    }
    {
        PyObject *tmp_called_name_176;
        PyObject *tmp_called_name_177;
        PyObject *tmp_args_name_2;
        PyObject *tmp_kw_name_2;
        PyObject *tmp_dict_key_2;
        PyObject *tmp_dict_value_2;
        PyObject *tmp_call_result_9;
        PyObject *tmp_args_element_name_85;
        PyObject *tmp_called_name_178;
        PyObject *tmp_mvar_value_13;
        CHECK_OBJECT( var_handle );
        tmp_called_name_177 = var_handle;
        tmp_args_name_2 = const_tuple_str_digest_2da2a9c575783ef454fa30e9f4e246ee_tuple;
        tmp_dict_key_2 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_2 = var_insert_mode;
        tmp_kw_name_2 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_2, tmp_dict_key_2, tmp_dict_value_2 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 124;
        tmp_called_name_176 = CALL_FUNCTION( tmp_called_name_177, tmp_args_name_2, tmp_kw_name_2 );
        Py_DECREF( tmp_kw_name_2 );
        if ( tmp_called_name_176 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_13 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_13 == NULL ))
        {
            tmp_mvar_value_13 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_13 == NULL )
        {
            Py_DECREF( tmp_called_name_176 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 124;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_178 = tmp_mvar_value_13;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 124;
        tmp_args_element_name_85 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_178, &PyTuple_GET_ITEM( const_tuple_str_digest_fe8d6ea5daee2fa16a6d6e2bae6159bb_tuple, 0 ) );

        if ( tmp_args_element_name_85 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_176 );

            exception_lineno = 124;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 124;
        {
            PyObject *call_args[] = { tmp_args_element_name_85 };
            tmp_call_result_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_176, call_args );
        }

        Py_DECREF( tmp_called_name_176 );
        Py_DECREF( tmp_args_element_name_85 );
        if ( tmp_call_result_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 124;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_9 );
    }
    {
        PyObject *tmp_called_name_179;
        PyObject *tmp_called_name_180;
        PyObject *tmp_args_name_3;
        PyObject *tmp_kw_name_3;
        PyObject *tmp_dict_key_3;
        PyObject *tmp_dict_value_3;
        PyObject *tmp_dict_key_4;
        PyObject *tmp_dict_value_4;
        PyObject *tmp_mvar_value_14;
        PyObject *tmp_call_result_10;
        PyObject *tmp_args_element_name_86;
        PyObject *tmp_called_name_181;
        PyObject *tmp_mvar_value_15;
        CHECK_OBJECT( var_handle );
        tmp_called_name_180 = var_handle;
        tmp_args_name_3 = const_tuple_str_plain_backspace_tuple;
        tmp_dict_key_3 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_3 = var_insert_mode;
        tmp_kw_name_3 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_3, tmp_dict_value_3 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_4 = const_str_plain_save_before;
        tmp_mvar_value_14 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_if_no_repeat );

        if (unlikely( tmp_mvar_value_14 == NULL ))
        {
            tmp_mvar_value_14 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_if_no_repeat );
        }

        if ( tmp_mvar_value_14 == NULL )
        {
            Py_DECREF( tmp_kw_name_3 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "if_no_repeat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 125;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_4 = tmp_mvar_value_14;
        tmp_res = PyDict_SetItem( tmp_kw_name_3, tmp_dict_key_4, tmp_dict_value_4 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 125;
        tmp_called_name_179 = CALL_FUNCTION( tmp_called_name_180, tmp_args_name_3, tmp_kw_name_3 );
        Py_DECREF( tmp_kw_name_3 );
        if ( tmp_called_name_179 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_15 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_15 == NULL ))
        {
            tmp_mvar_value_15 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_15 == NULL )
        {
            Py_DECREF( tmp_called_name_179 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 126;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_181 = tmp_mvar_value_15;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 126;
        tmp_args_element_name_86 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_181, &PyTuple_GET_ITEM( const_tuple_str_digest_647f11624bc526afd8ce034dc78066a1_tuple, 0 ) );

        if ( tmp_args_element_name_86 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_179 );

            exception_lineno = 126;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 125;
        {
            PyObject *call_args[] = { tmp_args_element_name_86 };
            tmp_call_result_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_179, call_args );
        }

        Py_DECREF( tmp_called_name_179 );
        Py_DECREF( tmp_args_element_name_86 );
        if ( tmp_call_result_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 125;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_10 );
    }
    {
        PyObject *tmp_called_name_182;
        PyObject *tmp_called_name_183;
        PyObject *tmp_args_name_4;
        PyObject *tmp_kw_name_4;
        PyObject *tmp_dict_key_5;
        PyObject *tmp_dict_value_5;
        PyObject *tmp_dict_key_6;
        PyObject *tmp_dict_value_6;
        PyObject *tmp_mvar_value_16;
        PyObject *tmp_call_result_11;
        PyObject *tmp_args_element_name_87;
        PyObject *tmp_called_name_184;
        PyObject *tmp_mvar_value_17;
        CHECK_OBJECT( var_handle );
        tmp_called_name_183 = var_handle;
        tmp_args_name_4 = const_tuple_str_plain_delete_tuple;
        tmp_dict_key_5 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_5 = var_insert_mode;
        tmp_kw_name_4 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_5, tmp_dict_value_5 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_6 = const_str_plain_save_before;
        tmp_mvar_value_16 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_if_no_repeat );

        if (unlikely( tmp_mvar_value_16 == NULL ))
        {
            tmp_mvar_value_16 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_if_no_repeat );
        }

        if ( tmp_mvar_value_16 == NULL )
        {
            Py_DECREF( tmp_kw_name_4 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "if_no_repeat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 127;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_6 = tmp_mvar_value_16;
        tmp_res = PyDict_SetItem( tmp_kw_name_4, tmp_dict_key_6, tmp_dict_value_6 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 127;
        tmp_called_name_182 = CALL_FUNCTION( tmp_called_name_183, tmp_args_name_4, tmp_kw_name_4 );
        Py_DECREF( tmp_kw_name_4 );
        if ( tmp_called_name_182 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_17 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_17 == NULL ))
        {
            tmp_mvar_value_17 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_17 == NULL )
        {
            Py_DECREF( tmp_called_name_182 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 128;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_184 = tmp_mvar_value_17;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 128;
        tmp_args_element_name_87 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_184, &PyTuple_GET_ITEM( const_tuple_str_digest_c1dec28d38befd226a053f4c881a493b_tuple, 0 ) );

        if ( tmp_args_element_name_87 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_182 );

            exception_lineno = 128;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 127;
        {
            PyObject *call_args[] = { tmp_args_element_name_87 };
            tmp_call_result_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_182, call_args );
        }

        Py_DECREF( tmp_called_name_182 );
        Py_DECREF( tmp_args_element_name_87 );
        if ( tmp_call_result_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 127;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_11 );
    }
    {
        PyObject *tmp_called_name_185;
        PyObject *tmp_called_name_186;
        PyObject *tmp_args_name_5;
        PyObject *tmp_kw_name_5;
        PyObject *tmp_dict_key_7;
        PyObject *tmp_dict_value_7;
        PyObject *tmp_dict_key_8;
        PyObject *tmp_dict_value_8;
        PyObject *tmp_mvar_value_18;
        PyObject *tmp_call_result_12;
        PyObject *tmp_args_element_name_88;
        PyObject *tmp_called_name_187;
        PyObject *tmp_mvar_value_19;
        CHECK_OBJECT( var_handle );
        tmp_called_name_186 = var_handle;
        tmp_args_name_5 = const_tuple_str_digest_9102ee6eb9329cb31f5264512e00165d_tuple;
        tmp_dict_key_7 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_7 = var_insert_mode;
        tmp_kw_name_5 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_7, tmp_dict_value_7 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_8 = const_str_plain_save_before;
        tmp_mvar_value_18 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_if_no_repeat );

        if (unlikely( tmp_mvar_value_18 == NULL ))
        {
            tmp_mvar_value_18 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_if_no_repeat );
        }

        if ( tmp_mvar_value_18 == NULL )
        {
            Py_DECREF( tmp_kw_name_5 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "if_no_repeat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 129;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_8 = tmp_mvar_value_18;
        tmp_res = PyDict_SetItem( tmp_kw_name_5, tmp_dict_key_8, tmp_dict_value_8 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 129;
        tmp_called_name_185 = CALL_FUNCTION( tmp_called_name_186, tmp_args_name_5, tmp_kw_name_5 );
        Py_DECREF( tmp_kw_name_5 );
        if ( tmp_called_name_185 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_19 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_19 == NULL ))
        {
            tmp_mvar_value_19 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_19 == NULL )
        {
            Py_DECREF( tmp_called_name_185 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 130;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_187 = tmp_mvar_value_19;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 130;
        tmp_args_element_name_88 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_187, &PyTuple_GET_ITEM( const_tuple_str_digest_c1dec28d38befd226a053f4c881a493b_tuple, 0 ) );

        if ( tmp_args_element_name_88 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_185 );

            exception_lineno = 130;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 129;
        {
            PyObject *call_args[] = { tmp_args_element_name_88 };
            tmp_call_result_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_185, call_args );
        }

        Py_DECREF( tmp_called_name_185 );
        Py_DECREF( tmp_args_element_name_88 );
        if ( tmp_call_result_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 129;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_12 );
    }
    {
        PyObject *tmp_called_name_188;
        PyObject *tmp_called_name_189;
        PyObject *tmp_args_name_6;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_20;
        PyObject *tmp_kw_name_6;
        PyObject *tmp_dict_key_9;
        PyObject *tmp_dict_value_9;
        PyObject *tmp_dict_key_10;
        PyObject *tmp_dict_value_10;
        PyObject *tmp_mvar_value_21;
        PyObject *tmp_call_result_13;
        PyObject *tmp_args_element_name_89;
        PyObject *tmp_called_name_190;
        PyObject *tmp_mvar_value_22;
        CHECK_OBJECT( var_handle );
        tmp_called_name_189 = var_handle;
        tmp_mvar_value_20 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_20 == NULL ))
        {
            tmp_mvar_value_20 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_20 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_20;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_Any );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_name_6 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_6, 0, tmp_tuple_element_1 );
        tmp_dict_key_9 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_9 = var_insert_mode;
        tmp_kw_name_6 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_6, tmp_dict_key_9, tmp_dict_value_9 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_10 = const_str_plain_save_before;
        tmp_mvar_value_21 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_if_no_repeat );

        if (unlikely( tmp_mvar_value_21 == NULL ))
        {
            tmp_mvar_value_21 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_if_no_repeat );
        }

        if ( tmp_mvar_value_21 == NULL )
        {
            Py_DECREF( tmp_args_name_6 );
            Py_DECREF( tmp_kw_name_6 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "if_no_repeat" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 131;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_10 = tmp_mvar_value_21;
        tmp_res = PyDict_SetItem( tmp_kw_name_6, tmp_dict_key_10, tmp_dict_value_10 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 131;
        tmp_called_name_188 = CALL_FUNCTION( tmp_called_name_189, tmp_args_name_6, tmp_kw_name_6 );
        Py_DECREF( tmp_args_name_6 );
        Py_DECREF( tmp_kw_name_6 );
        if ( tmp_called_name_188 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_22 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_22 == NULL ))
        {
            tmp_mvar_value_22 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_22 == NULL )
        {
            Py_DECREF( tmp_called_name_188 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 132;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_190 = tmp_mvar_value_22;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 132;
        tmp_args_element_name_89 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_190, &PyTuple_GET_ITEM( const_tuple_str_digest_204c920806e032799a87379e1294bcfc_tuple, 0 ) );

        if ( tmp_args_element_name_89 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_188 );

            exception_lineno = 132;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 131;
        {
            PyObject *call_args[] = { tmp_args_element_name_89 };
            tmp_call_result_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_188, call_args );
        }

        Py_DECREF( tmp_called_name_188 );
        Py_DECREF( tmp_args_element_name_89 );
        if ( tmp_call_result_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 131;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_13 );
    }
    {
        PyObject *tmp_called_name_191;
        PyObject *tmp_called_name_192;
        PyObject *tmp_args_name_7;
        PyObject *tmp_kw_name_7;
        PyObject *tmp_dict_key_11;
        PyObject *tmp_dict_value_11;
        PyObject *tmp_call_result_14;
        PyObject *tmp_args_element_name_90;
        PyObject *tmp_called_name_193;
        PyObject *tmp_mvar_value_23;
        CHECK_OBJECT( var_handle );
        tmp_called_name_192 = var_handle;
        tmp_args_name_7 = const_tuple_str_digest_9d28ad88315d87cb7857dca59e1b839e_tuple;
        tmp_dict_key_11 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_11 = var_insert_mode;
        tmp_kw_name_7 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_7, tmp_dict_key_11, tmp_dict_value_11 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 133;
        tmp_called_name_191 = CALL_FUNCTION( tmp_called_name_192, tmp_args_name_7, tmp_kw_name_7 );
        Py_DECREF( tmp_kw_name_7 );
        if ( tmp_called_name_191 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_23 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_23 == NULL ))
        {
            tmp_mvar_value_23 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_23 == NULL )
        {
            Py_DECREF( tmp_called_name_191 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 133;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_193 = tmp_mvar_value_23;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 133;
        tmp_args_element_name_90 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_193, &PyTuple_GET_ITEM( const_tuple_str_digest_d2a5158fd7bb2018f91aee17fcfed439_tuple, 0 ) );

        if ( tmp_args_element_name_90 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_191 );

            exception_lineno = 133;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 133;
        {
            PyObject *call_args[] = { tmp_args_element_name_90 };
            tmp_call_result_14 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_191, call_args );
        }

        Py_DECREF( tmp_called_name_191 );
        Py_DECREF( tmp_args_element_name_90 );
        if ( tmp_call_result_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 133;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_14 );
    }
    {
        PyObject *tmp_called_name_194;
        PyObject *tmp_called_name_195;
        PyObject *tmp_args_name_8;
        PyObject *tmp_kw_name_8;
        PyObject *tmp_dict_key_12;
        PyObject *tmp_dict_value_12;
        PyObject *tmp_call_result_15;
        PyObject *tmp_args_element_name_91;
        PyObject *tmp_called_name_196;
        PyObject *tmp_mvar_value_24;
        CHECK_OBJECT( var_handle );
        tmp_called_name_195 = var_handle;
        tmp_args_name_8 = const_tuple_str_digest_d2b85068d86fe4969537b7e2ea0dd346_tuple;
        tmp_dict_key_12 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_12 = var_insert_mode;
        tmp_kw_name_8 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_8, tmp_dict_key_12, tmp_dict_value_12 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 134;
        tmp_called_name_194 = CALL_FUNCTION( tmp_called_name_195, tmp_args_name_8, tmp_kw_name_8 );
        Py_DECREF( tmp_kw_name_8 );
        if ( tmp_called_name_194 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_24 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_24 == NULL ))
        {
            tmp_mvar_value_24 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_24 == NULL )
        {
            Py_DECREF( tmp_called_name_194 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 134;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_196 = tmp_mvar_value_24;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 134;
        tmp_args_element_name_91 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_196, &PyTuple_GET_ITEM( const_tuple_str_digest_2341ccadcc0716db92a6611c3a0c5709_tuple, 0 ) );

        if ( tmp_args_element_name_91 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_194 );

            exception_lineno = 134;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 134;
        {
            PyObject *call_args[] = { tmp_args_element_name_91 };
            tmp_call_result_15 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_194, call_args );
        }

        Py_DECREF( tmp_called_name_194 );
        Py_DECREF( tmp_args_element_name_91 );
        if ( tmp_call_result_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 134;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_15 );
    }
    {
        PyObject *tmp_called_name_197;
        PyObject *tmp_called_name_198;
        PyObject *tmp_args_name_9;
        PyObject *tmp_kw_name_9;
        PyObject *tmp_dict_key_13;
        PyObject *tmp_dict_value_13;
        PyObject *tmp_call_result_16;
        PyObject *tmp_args_element_name_92;
        PyObject *tmp_called_name_199;
        PyObject *tmp_mvar_value_25;
        CHECK_OBJECT( var_handle );
        tmp_called_name_198 = var_handle;
        tmp_args_name_9 = const_tuple_str_digest_be077fe6fbb1013afb24e5934140597e_tuple;
        tmp_dict_key_13 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_13 = var_insert_mode;
        tmp_kw_name_9 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_9, tmp_dict_key_13, tmp_dict_value_13 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 135;
        tmp_called_name_197 = CALL_FUNCTION( tmp_called_name_198, tmp_args_name_9, tmp_kw_name_9 );
        Py_DECREF( tmp_kw_name_9 );
        if ( tmp_called_name_197 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_25 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_25 == NULL ))
        {
            tmp_mvar_value_25 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_25 == NULL )
        {
            Py_DECREF( tmp_called_name_197 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 135;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_199 = tmp_mvar_value_25;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 135;
        tmp_args_element_name_92 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_199, &PyTuple_GET_ITEM( const_tuple_str_digest_7e022856dd1b5bd6663571c7df813303_tuple, 0 ) );

        if ( tmp_args_element_name_92 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_197 );

            exception_lineno = 135;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 135;
        {
            PyObject *call_args[] = { tmp_args_element_name_92 };
            tmp_call_result_16 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_197, call_args );
        }

        Py_DECREF( tmp_called_name_197 );
        Py_DECREF( tmp_args_element_name_92 );
        if ( tmp_call_result_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 135;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_16 );
    }
    {
        PyObject *tmp_called_name_200;
        PyObject *tmp_called_name_201;
        PyObject *tmp_args_name_10;
        PyObject *tmp_kw_name_10;
        PyObject *tmp_dict_key_14;
        PyObject *tmp_dict_value_14;
        PyObject *tmp_call_result_17;
        PyObject *tmp_args_element_name_93;
        PyObject *tmp_called_name_202;
        PyObject *tmp_mvar_value_26;
        CHECK_OBJECT( var_handle );
        tmp_called_name_201 = var_handle;
        tmp_args_name_10 = const_tuple_str_digest_d8156c92c3ac39743ec9345e59bfd2ed_tuple;
        tmp_dict_key_14 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_dict_value_14 = var_insert_mode;
        tmp_kw_name_10 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_10, tmp_dict_key_14, tmp_dict_value_14 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 139;
        tmp_called_name_200 = CALL_FUNCTION( tmp_called_name_201, tmp_args_name_10, tmp_kw_name_10 );
        Py_DECREF( tmp_kw_name_10 );
        if ( tmp_called_name_200 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_26 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_26 == NULL ))
        {
            tmp_mvar_value_26 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_26 == NULL )
        {
            Py_DECREF( tmp_called_name_200 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 139;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_202 = tmp_mvar_value_26;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 139;
        tmp_args_element_name_93 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_202, &PyTuple_GET_ITEM( const_tuple_str_digest_8ac25a148822dcf205845c372c142dc5_tuple, 0 ) );

        if ( tmp_args_element_name_93 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_200 );

            exception_lineno = 139;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 139;
        {
            PyObject *call_args[] = { tmp_args_element_name_93 };
            tmp_call_result_17 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_200, call_args );
        }

        Py_DECREF( tmp_called_name_200 );
        Py_DECREF( tmp_args_element_name_93 );
        if ( tmp_call_result_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 139;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_17 );
    }
    {
        PyObject *tmp_called_name_203;
        PyObject *tmp_called_name_204;
        PyObject *tmp_args_name_11;
        PyObject *tmp_kw_name_11;
        PyObject *tmp_dict_key_15;
        PyObject *tmp_dict_value_15;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_mvar_value_27;
        PyObject *tmp_call_result_18;
        PyObject *tmp_args_element_name_94;
        PyObject *tmp_called_name_205;
        PyObject *tmp_mvar_value_28;
        CHECK_OBJECT( var_handle );
        tmp_called_name_204 = var_handle;
        tmp_args_name_11 = const_tuple_str_plain_pageup_tuple;
        tmp_dict_key_15 = const_str_plain_filter;
        tmp_mvar_value_27 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_27 == NULL ))
        {
            tmp_mvar_value_27 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_27 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 141;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_1 = tmp_mvar_value_27;
        tmp_dict_value_15 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_1 );
        if ( tmp_dict_value_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_11 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_11, tmp_dict_key_15, tmp_dict_value_15 );
        Py_DECREF( tmp_dict_value_15 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 141;
        tmp_called_name_203 = CALL_FUNCTION( tmp_called_name_204, tmp_args_name_11, tmp_kw_name_11 );
        Py_DECREF( tmp_kw_name_11 );
        if ( tmp_called_name_203 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_28 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_28 == NULL ))
        {
            tmp_mvar_value_28 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_28 == NULL )
        {
            Py_DECREF( tmp_called_name_203 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 141;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_205 = tmp_mvar_value_28;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 141;
        tmp_args_element_name_94 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_205, &PyTuple_GET_ITEM( const_tuple_str_digest_af1adbb7bec7eaa2f6c08a25f5ab2b5d_tuple, 0 ) );

        if ( tmp_args_element_name_94 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_203 );

            exception_lineno = 141;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 141;
        {
            PyObject *call_args[] = { tmp_args_element_name_94 };
            tmp_call_result_18 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_203, call_args );
        }

        Py_DECREF( tmp_called_name_203 );
        Py_DECREF( tmp_args_element_name_94 );
        if ( tmp_call_result_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 141;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_18 );
    }
    {
        PyObject *tmp_called_name_206;
        PyObject *tmp_called_name_207;
        PyObject *tmp_args_name_12;
        PyObject *tmp_kw_name_12;
        PyObject *tmp_dict_key_16;
        PyObject *tmp_dict_value_16;
        PyObject *tmp_operand_name_2;
        PyObject *tmp_mvar_value_29;
        PyObject *tmp_call_result_19;
        PyObject *tmp_args_element_name_95;
        PyObject *tmp_called_name_208;
        PyObject *tmp_mvar_value_30;
        CHECK_OBJECT( var_handle );
        tmp_called_name_207 = var_handle;
        tmp_args_name_12 = const_tuple_str_plain_pagedown_tuple;
        tmp_dict_key_16 = const_str_plain_filter;
        tmp_mvar_value_29 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_29 == NULL ))
        {
            tmp_mvar_value_29 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_29 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_operand_name_2 = tmp_mvar_value_29;
        tmp_dict_value_16 = UNARY_OPERATION( PyNumber_Invert, tmp_operand_name_2 );
        if ( tmp_dict_value_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_12 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_12, tmp_dict_key_16, tmp_dict_value_16 );
        Py_DECREF( tmp_dict_value_16 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 142;
        tmp_called_name_206 = CALL_FUNCTION( tmp_called_name_207, tmp_args_name_12, tmp_kw_name_12 );
        Py_DECREF( tmp_kw_name_12 );
        if ( tmp_called_name_206 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_30 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_30 == NULL ))
        {
            tmp_mvar_value_30 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_30 == NULL )
        {
            Py_DECREF( tmp_called_name_206 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 142;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_208 = tmp_mvar_value_30;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 142;
        tmp_args_element_name_95 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_208, &PyTuple_GET_ITEM( const_tuple_str_digest_ffa4782c58075c6dd84b572edd3109a7_tuple, 0 ) );

        if ( tmp_args_element_name_95 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_206 );

            exception_lineno = 142;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 142;
        {
            PyObject *call_args[] = { tmp_args_element_name_95 };
            tmp_call_result_19 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_206, call_args );
        }

        Py_DECREF( tmp_called_name_206 );
        Py_DECREF( tmp_args_element_name_95 );
        if ( tmp_call_result_19 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 142;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_19 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_called_name_209;
        PyObject *tmp_mvar_value_31;
        PyObject *tmp_args_element_name_96;
        tmp_mvar_value_31 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_Condition );

        if (unlikely( tmp_mvar_value_31 == NULL ))
        {
            tmp_mvar_value_31 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Condition );
        }

        if ( tmp_mvar_value_31 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Condition" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_209 = tmp_mvar_value_31;
        tmp_args_element_name_96 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_2_lambda(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 146;
        {
            PyObject *call_args[] = { tmp_args_element_name_96 };
            tmp_assign_source_5 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_209, call_args );
        }

        Py_DECREF( tmp_args_element_name_96 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        assert( var_text_before_cursor == NULL );
        var_text_before_cursor = tmp_assign_source_5;
    }
    {
        PyObject *tmp_called_name_210;
        PyObject *tmp_called_name_211;
        PyObject *tmp_args_name_13;
        PyObject *tmp_kw_name_13;
        PyObject *tmp_dict_key_17;
        PyObject *tmp_dict_value_17;
        PyObject *tmp_left_name_2;
        PyObject *tmp_right_name_2;
        PyObject *tmp_call_result_20;
        PyObject *tmp_args_element_name_97;
        PyObject *tmp_called_name_212;
        PyObject *tmp_mvar_value_32;
        CHECK_OBJECT( var_handle );
        tmp_called_name_211 = var_handle;
        tmp_args_name_13 = const_tuple_str_digest_b4cd29bb1a0bd6b6362c18c3e7a98845_tuple;
        tmp_dict_key_17 = const_str_plain_filter;
        CHECK_OBJECT( var_text_before_cursor );
        tmp_left_name_2 = var_text_before_cursor;
        CHECK_OBJECT( var_insert_mode );
        tmp_right_name_2 = var_insert_mode;
        tmp_dict_value_17 = BINARY_OPERATION( PyNumber_And, tmp_left_name_2, tmp_right_name_2 );
        if ( tmp_dict_value_17 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_13 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_13, tmp_dict_key_17, tmp_dict_value_17 );
        Py_DECREF( tmp_dict_value_17 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 147;
        tmp_called_name_210 = CALL_FUNCTION( tmp_called_name_211, tmp_args_name_13, tmp_kw_name_13 );
        Py_DECREF( tmp_kw_name_13 );
        if ( tmp_called_name_210 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_32 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name );

        if (unlikely( tmp_mvar_value_32 == NULL ))
        {
            tmp_mvar_value_32 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_by_name );
        }

        if ( tmp_mvar_value_32 == NULL )
        {
            Py_DECREF( tmp_called_name_210 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_by_name" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 147;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_212 = tmp_mvar_value_32;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 147;
        tmp_args_element_name_97 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_212, &PyTuple_GET_ITEM( const_tuple_str_digest_c1dec28d38befd226a053f4c881a493b_tuple, 0 ) );

        if ( tmp_args_element_name_97 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_210 );

            exception_lineno = 147;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 147;
        {
            PyObject *call_args[] = { tmp_args_element_name_97 };
            tmp_call_result_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_210, call_args );
        }

        Py_DECREF( tmp_called_name_210 );
        Py_DECREF( tmp_args_element_name_97 );
        if ( tmp_call_result_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 147;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_20 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_called_name_213;
        PyObject *tmp_called_name_214;
        PyObject *tmp_args_name_14;
        PyObject *tmp_kw_name_14;
        PyObject *tmp_dict_key_18;
        PyObject *tmp_dict_value_18;
        PyObject *tmp_left_name_3;
        PyObject *tmp_right_name_3;
        PyObject *tmp_mvar_value_33;
        PyObject *tmp_args_element_name_98;
        CHECK_OBJECT( var_handle );
        tmp_called_name_214 = var_handle;
        tmp_args_name_14 = const_tuple_str_plain_enter_tuple;
        tmp_dict_key_18 = const_str_plain_filter;
        CHECK_OBJECT( var_insert_mode );
        tmp_left_name_3 = var_insert_mode;
        tmp_mvar_value_33 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_is_multiline );

        if (unlikely( tmp_mvar_value_33 == NULL ))
        {
            tmp_mvar_value_33 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_is_multiline );
        }

        if ( tmp_mvar_value_33 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "is_multiline" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 149;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_right_name_3 = tmp_mvar_value_33;
        tmp_dict_value_18 = BINARY_OPERATION( PyNumber_And, tmp_left_name_3, tmp_right_name_3 );
        if ( tmp_dict_value_18 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_14 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_14, tmp_dict_key_18, tmp_dict_value_18 );
        Py_DECREF( tmp_dict_value_18 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 149;
        tmp_called_name_213 = CALL_FUNCTION( tmp_called_name_214, tmp_args_name_14, tmp_kw_name_14 );
        Py_DECREF( tmp_kw_name_14 );
        if ( tmp_called_name_213 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_98 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_3__(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 149;
        {
            PyObject *call_args[] = { tmp_args_element_name_98 };
            tmp_assign_source_6 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_213, call_args );
        }

        Py_DECREF( tmp_called_name_213 );
        Py_DECREF( tmp_args_element_name_98 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 149;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_6;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_called_name_215;
        PyObject *tmp_called_name_216;
        PyObject *tmp_args_element_name_99;
        CHECK_OBJECT( var_handle );
        tmp_called_name_216 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 154;
        tmp_called_name_215 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_216, &PyTuple_GET_ITEM( const_tuple_str_digest_d759df6b8c71154cebd6df0ba1bf9b3f_tuple, 0 ) );

        if ( tmp_called_name_215 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_99 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_4__(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 154;
        {
            PyObject *call_args[] = { tmp_args_element_name_99 };
            tmp_assign_source_7 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_215, call_args );
        }

        Py_DECREF( tmp_called_name_215 );
        Py_DECREF( tmp_args_element_name_99 );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 154;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_7;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_called_name_217;
        PyObject *tmp_called_name_218;
        PyObject *tmp_args_element_name_100;
        CHECK_OBJECT( var_handle );
        tmp_called_name_218 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 166;
        tmp_called_name_217 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_218, &PyTuple_GET_ITEM( const_tuple_str_plain_up_tuple, 0 ) );

        if ( tmp_called_name_217 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_100 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_5__(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 166;
        {
            PyObject *call_args[] = { tmp_args_element_name_100 };
            tmp_assign_source_8 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_217, call_args );
        }

        Py_DECREF( tmp_called_name_217 );
        Py_DECREF( tmp_args_element_name_100 );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 166;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_8;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_called_name_219;
        PyObject *tmp_called_name_220;
        PyObject *tmp_args_element_name_101;
        CHECK_OBJECT( var_handle );
        tmp_called_name_220 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 170;
        tmp_called_name_219 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_220, &PyTuple_GET_ITEM( const_tuple_str_plain_down_tuple, 0 ) );

        if ( tmp_called_name_219 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_101 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_6__(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 170;
        {
            PyObject *call_args[] = { tmp_args_element_name_101 };
            tmp_assign_source_9 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_219, call_args );
        }

        Py_DECREF( tmp_called_name_219 );
        Py_DECREF( tmp_args_element_name_101 );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 170;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_9;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_called_name_221;
        PyObject *tmp_called_name_222;
        PyObject *tmp_args_name_15;
        PyObject *tmp_kw_name_15;
        PyObject *tmp_dict_key_19;
        PyObject *tmp_dict_value_19;
        PyObject *tmp_mvar_value_34;
        PyObject *tmp_args_element_name_102;
        CHECK_OBJECT( var_handle );
        tmp_called_name_222 = var_handle;
        tmp_args_name_15 = const_tuple_str_plain_delete_tuple;
        tmp_dict_key_19 = const_str_plain_filter;
        tmp_mvar_value_34 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_has_selection );

        if (unlikely( tmp_mvar_value_34 == NULL ))
        {
            tmp_mvar_value_34 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_has_selection );
        }

        if ( tmp_mvar_value_34 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "has_selection" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 174;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_dict_value_19 = tmp_mvar_value_34;
        tmp_kw_name_15 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_15, tmp_dict_key_19, tmp_dict_value_19 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 174;
        tmp_called_name_221 = CALL_FUNCTION( tmp_called_name_222, tmp_args_name_15, tmp_kw_name_15 );
        Py_DECREF( tmp_kw_name_15 );
        if ( tmp_called_name_221 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_102 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_7__(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 174;
        {
            PyObject *call_args[] = { tmp_args_element_name_102 };
            tmp_assign_source_10 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_221, call_args );
        }

        Py_DECREF( tmp_called_name_221 );
        Py_DECREF( tmp_args_element_name_102 );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 174;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_10;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_called_name_223;
        PyObject *tmp_called_name_224;
        PyObject *tmp_args_element_name_103;
        CHECK_OBJECT( var_handle );
        tmp_called_name_224 = var_handle;
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 181;
        tmp_called_name_223 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_224, &PyTuple_GET_ITEM( const_tuple_str_digest_e5fd2b9ef2333d150f68273f43406bf0_tuple, 0 ) );

        if ( tmp_called_name_223 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_103 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_8__(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 181;
        {
            PyObject *call_args[] = { tmp_args_element_name_103 };
            tmp_assign_source_11 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_223, call_args );
        }

        Py_DECREF( tmp_called_name_223 );
        Py_DECREF( tmp_args_element_name_103 );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 181;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_11;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_called_name_225;
        PyObject *tmp_called_name_226;
        PyObject *tmp_args_element_name_104;
        PyObject *tmp_source_name_4;
        PyObject *tmp_mvar_value_35;
        PyObject *tmp_args_element_name_105;
        CHECK_OBJECT( var_handle );
        tmp_called_name_226 = var_handle;
        tmp_mvar_value_35 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_35 == NULL ))
        {
            tmp_mvar_value_35 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_35 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 194;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_4 = tmp_mvar_value_35;
        tmp_args_element_name_104 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_BracketedPaste );
        if ( tmp_args_element_name_104 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 194;
        {
            PyObject *call_args[] = { tmp_args_element_name_104 };
            tmp_called_name_225 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_226, call_args );
        }

        Py_DECREF( tmp_args_element_name_104 );
        if ( tmp_called_name_225 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_105 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_9__(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 194;
        {
            PyObject *call_args[] = { tmp_args_element_name_105 };
            tmp_assign_source_12 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_225, call_args );
        }

        Py_DECREF( tmp_called_name_225 );
        Py_DECREF( tmp_args_element_name_105 );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 194;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_12;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_called_name_227;
        PyObject *tmp_called_name_228;
        PyObject *tmp_args_name_16;
        PyObject *tmp_tuple_element_2;
        PyObject *tmp_source_name_5;
        PyObject *tmp_mvar_value_36;
        PyObject *tmp_kw_name_16;
        PyObject *tmp_dict_key_20;
        PyObject *tmp_dict_value_20;
        PyObject *tmp_called_name_229;
        PyObject *tmp_mvar_value_37;
        PyObject *tmp_args_element_name_106;
        PyObject *tmp_dict_key_21;
        PyObject *tmp_dict_value_21;
        PyObject *tmp_args_element_name_107;
        CHECK_OBJECT( var_handle );
        tmp_called_name_228 = var_handle;
        tmp_mvar_value_36 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_36 == NULL ))
        {
            tmp_mvar_value_36 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_36 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 207;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_source_name_5 = tmp_mvar_value_36;
        tmp_tuple_element_2 = LOOKUP_ATTRIBUTE( tmp_source_name_5, const_str_plain_Any );
        if ( tmp_tuple_element_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_name_16 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_16, 0, tmp_tuple_element_2 );
        tmp_dict_key_20 = const_str_plain_filter;
        tmp_mvar_value_37 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_Condition );

        if (unlikely( tmp_mvar_value_37 == NULL ))
        {
            tmp_mvar_value_37 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Condition );
        }

        if ( tmp_mvar_value_37 == NULL )
        {
            Py_DECREF( tmp_args_name_16 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Condition" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 207;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }

        tmp_called_name_229 = tmp_mvar_value_37;
        tmp_args_element_name_106 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_11_lambda(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 207;
        {
            PyObject *call_args[] = { tmp_args_element_name_106 };
            tmp_dict_value_20 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_229, call_args );
        }

        Py_DECREF( tmp_args_element_name_106 );
        if ( tmp_dict_value_20 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_args_name_16 );

            exception_lineno = 207;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_16 = _PyDict_NewPresized( 2 );
        tmp_res = PyDict_SetItem( tmp_kw_name_16, tmp_dict_key_20, tmp_dict_value_20 );
        Py_DECREF( tmp_dict_value_20 );
        assert( !(tmp_res != 0) );
        tmp_dict_key_21 = const_str_plain_eager;
        tmp_dict_value_21 = Py_True;
        tmp_res = PyDict_SetItem( tmp_kw_name_16, tmp_dict_key_21, tmp_dict_value_21 );
        assert( !(tmp_res != 0) );
        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 207;
        tmp_called_name_227 = CALL_FUNCTION( tmp_called_name_228, tmp_args_name_16, tmp_kw_name_16 );
        Py_DECREF( tmp_args_name_16 );
        Py_DECREF( tmp_kw_name_16 );
        if ( tmp_called_name_227 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_107 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_10__(  );



        frame_078d10575ac2c0fb043b17bda86af556->m_frame.f_lineno = 207;
        {
            PyObject *call_args[] = { tmp_args_element_name_107 };
            tmp_assign_source_13 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_227, call_args );
        }

        Py_DECREF( tmp_called_name_227 );
        Py_DECREF( tmp_args_element_name_107 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;
            type_description_1 = "ooooo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var__;
            assert( old != NULL );
            var__ = tmp_assign_source_13;
            Py_DECREF( old );
        }

    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_078d10575ac2c0fb043b17bda86af556 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_078d10575ac2c0fb043b17bda86af556 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_078d10575ac2c0fb043b17bda86af556, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_078d10575ac2c0fb043b17bda86af556->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_078d10575ac2c0fb043b17bda86af556, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_078d10575ac2c0fb043b17bda86af556,
        type_description_1,
        var_key_bindings,
        var_insert_mode,
        var_handle,
        var__,
        var_text_before_cursor
    );


    // Release cached frame.
    if ( frame_078d10575ac2c0fb043b17bda86af556 == cache_frame_078d10575ac2c0fb043b17bda86af556 )
    {
        Py_DECREF( frame_078d10575ac2c0fb043b17bda86af556 );
    }
    cache_frame_078d10575ac2c0fb043b17bda86af556 = NULL;

    assertFrameObject( frame_078d10575ac2c0fb043b17bda86af556 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    CHECK_OBJECT( var_key_bindings );
    tmp_return_value = var_key_bindings;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)var_key_bindings );
    Py_DECREF( var_key_bindings );
    var_key_bindings = NULL;

    CHECK_OBJECT( (PyObject *)var_insert_mode );
    Py_DECREF( var_insert_mode );
    var_insert_mode = NULL;

    CHECK_OBJECT( (PyObject *)var_handle );
    Py_DECREF( var_handle );
    var_handle = NULL;

    CHECK_OBJECT( (PyObject *)var__ );
    Py_DECREF( var__ );
    var__ = NULL;

    CHECK_OBJECT( (PyObject *)var_text_before_cursor );
    Py_DECREF( var_text_before_cursor );
    var_text_before_cursor = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    Py_XDECREF( var_key_bindings );
    var_key_bindings = NULL;

    Py_XDECREF( var_insert_mode );
    var_insert_mode = NULL;

    Py_XDECREF( var_handle );
    var_handle = NULL;

    Py_XDECREF( var__ );
    var__ = NULL;

    Py_XDECREF( var_text_before_cursor );
    var_text_before_cursor = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_1__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *tmp_return_value = NULL;

    // Actual function body.
    // Tried code:
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_1__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // End of try:
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;


    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_1__ );
    return NULL;


function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_2_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_c938375228a721c91a9db1d8d4517f6e;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_c938375228a721c91a9db1d8d4517f6e = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_c938375228a721c91a9db1d8d4517f6e, codeobj_c938375228a721c91a9db1d8d4517f6e, module_prompt_toolkit$key_binding$bindings$basic, 0 );
    frame_c938375228a721c91a9db1d8d4517f6e = cache_frame_c938375228a721c91a9db1d8d4517f6e;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_c938375228a721c91a9db1d8d4517f6e );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_c938375228a721c91a9db1d8d4517f6e ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 146;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_c938375228a721c91a9db1d8d4517f6e->m_frame.f_lineno = 146;
        tmp_source_name_2 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;

            goto frame_exception_exit_1;
        }
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;

            goto frame_exception_exit_1;
        }
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_text );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 146;

            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c938375228a721c91a9db1d8d4517f6e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_c938375228a721c91a9db1d8d4517f6e );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_c938375228a721c91a9db1d8d4517f6e );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_c938375228a721c91a9db1d8d4517f6e, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_c938375228a721c91a9db1d8d4517f6e->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_c938375228a721c91a9db1d8d4517f6e, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_c938375228a721c91a9db1d8d4517f6e,
        type_description_1
    );


    // Release cached frame.
    if ( frame_c938375228a721c91a9db1d8d4517f6e == cache_frame_c938375228a721c91a9db1d8d4517f6e )
    {
        Py_DECREF( frame_c938375228a721c91a9db1d8d4517f6e );
    }
    cache_frame_c938375228a721c91a9db1d8d4517f6e = NULL;

    assertFrameObject( frame_c938375228a721c91a9db1d8d4517f6e );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_2_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_3__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_4e9b1d787fd3d410289f5155384148b9;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_4e9b1d787fd3d410289f5155384148b9 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_4e9b1d787fd3d410289f5155384148b9, codeobj_4e9b1d787fd3d410289f5155384148b9, module_prompt_toolkit$key_binding$bindings$basic, sizeof(void *) );
    frame_4e9b1d787fd3d410289f5155384148b9 = cache_frame_4e9b1d787fd3d410289f5155384148b9;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_4e9b1d787fd3d410289f5155384148b9 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_4e9b1d787fd3d410289f5155384148b9 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_operand_name_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_newline );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_copy_margin;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_in_paste_mode );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_in_paste_mode );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "in_paste_mode" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 152;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        frame_4e9b1d787fd3d410289f5155384148b9->m_frame.f_lineno = 152;
        tmp_operand_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_2 );
        if ( tmp_operand_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 152;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_res = CHECK_IF_TRUE( tmp_operand_name_1 );
        Py_DECREF( tmp_operand_name_1 );
        if ( tmp_res == -1 )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 152;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_dict_value_1 = ( tmp_res == 0 ) ? Py_True : Py_False;
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_4e9b1d787fd3d410289f5155384148b9->m_frame.f_lineno = 152;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 152;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4e9b1d787fd3d410289f5155384148b9 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_4e9b1d787fd3d410289f5155384148b9 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_4e9b1d787fd3d410289f5155384148b9, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_4e9b1d787fd3d410289f5155384148b9->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_4e9b1d787fd3d410289f5155384148b9, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_4e9b1d787fd3d410289f5155384148b9,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_4e9b1d787fd3d410289f5155384148b9 == cache_frame_4e9b1d787fd3d410289f5155384148b9 )
    {
        Py_DECREF( frame_4e9b1d787fd3d410289f5155384148b9 );
    }
    cache_frame_4e9b1d787fd3d410289f5155384148b9 = NULL;

    assertFrameObject( frame_4e9b1d787fd3d410289f5155384148b9 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_3__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_3__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_4__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_7e2a60d14f94af675b3e06051a6f3587;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_7e2a60d14f94af675b3e06051a6f3587 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7e2a60d14f94af675b3e06051a6f3587, codeobj_7e2a60d14f94af675b3e06051a6f3587, module_prompt_toolkit$key_binding$bindings$basic, sizeof(void *) );
    frame_7e2a60d14f94af675b3e06051a6f3587 = cache_frame_7e2a60d14f94af675b3e06051a6f3587;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7e2a60d14f94af675b3e06051a6f3587 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7e2a60d14f94af675b3e06051a6f3587 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_called_name_2;
        PyObject *tmp_mvar_value_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_mvar_value_2;
        PyObject *tmp_args_element_name_2;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_key_processor );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_feed );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_KeyPress );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_KeyPress );
        }

        if ( tmp_mvar_value_1 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "KeyPress" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_called_name_2 = tmp_mvar_value_1;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_Keys );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_Keys );
        }

        if ( tmp_mvar_value_2 == NULL )
        {
            Py_DECREF( tmp_called_name_1 );
            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "Keys" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 162;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }

        tmp_source_name_3 = tmp_mvar_value_2;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_ControlM );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 162;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_element_name_2 = const_str_chr_13;
        frame_7e2a60d14f94af675b3e06051a6f3587->m_frame.f_lineno = 162;
        {
            PyObject *call_args[] = { tmp_args_element_name_1, tmp_args_element_name_2 };
            tmp_tuple_element_1 = CALL_FUNCTION_WITH_ARGS2( tmp_called_name_2, call_args );
        }

        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 162;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_e2d16c98e7cb03ba7fd208a07fd5d185 );
        frame_7e2a60d14f94af675b3e06051a6f3587->m_frame.f_lineno = 161;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 161;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7e2a60d14f94af675b3e06051a6f3587 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7e2a60d14f94af675b3e06051a6f3587 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7e2a60d14f94af675b3e06051a6f3587, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7e2a60d14f94af675b3e06051a6f3587->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7e2a60d14f94af675b3e06051a6f3587, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7e2a60d14f94af675b3e06051a6f3587,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_7e2a60d14f94af675b3e06051a6f3587 == cache_frame_7e2a60d14f94af675b3e06051a6f3587 )
    {
        Py_DECREF( frame_7e2a60d14f94af675b3e06051a6f3587 );
    }
    cache_frame_7e2a60d14f94af675b3e06051a6f3587 = NULL;

    assertFrameObject( frame_7e2a60d14f94af675b3e06051a6f3587 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_4__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_4__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_5__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_45d48e22ad81947896cb4fd75504e88a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_45d48e22ad81947896cb4fd75504e88a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_45d48e22ad81947896cb4fd75504e88a, codeobj_45d48e22ad81947896cb4fd75504e88a, module_prompt_toolkit$key_binding$bindings$basic, sizeof(void *) );
    frame_45d48e22ad81947896cb4fd75504e88a = cache_frame_45d48e22ad81947896cb4fd75504e88a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_45d48e22ad81947896cb4fd75504e88a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_45d48e22ad81947896cb4fd75504e88a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_auto_up );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 168;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_45d48e22ad81947896cb4fd75504e88a->m_frame.f_lineno = 168;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 168;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_45d48e22ad81947896cb4fd75504e88a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_45d48e22ad81947896cb4fd75504e88a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_45d48e22ad81947896cb4fd75504e88a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_45d48e22ad81947896cb4fd75504e88a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_45d48e22ad81947896cb4fd75504e88a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_45d48e22ad81947896cb4fd75504e88a,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_45d48e22ad81947896cb4fd75504e88a == cache_frame_45d48e22ad81947896cb4fd75504e88a )
    {
        Py_DECREF( frame_45d48e22ad81947896cb4fd75504e88a );
    }
    cache_frame_45d48e22ad81947896cb4fd75504e88a = NULL;

    assertFrameObject( frame_45d48e22ad81947896cb4fd75504e88a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_5__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_5__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_6__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_7d523f7f31ee26acdccc49520af196e2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    int tmp_res;
    static struct Nuitka_FrameObject *cache_frame_7d523f7f31ee26acdccc49520af196e2 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_7d523f7f31ee26acdccc49520af196e2, codeobj_7d523f7f31ee26acdccc49520af196e2, module_prompt_toolkit$key_binding$bindings$basic, sizeof(void *) );
    frame_7d523f7f31ee26acdccc49520af196e2 = cache_frame_7d523f7f31ee26acdccc49520af196e2;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_7d523f7f31ee26acdccc49520af196e2 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_7d523f7f31ee26acdccc49520af196e2 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_kw_name_1;
        PyObject *tmp_dict_key_1;
        PyObject *tmp_dict_value_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_auto_down );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_dict_key_1 = const_str_plain_count;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_dict_value_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_arg );
        if ( tmp_dict_value_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 172;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_kw_name_1 = _PyDict_NewPresized( 1 );
        tmp_res = PyDict_SetItem( tmp_kw_name_1, tmp_dict_key_1, tmp_dict_value_1 );
        Py_DECREF( tmp_dict_value_1 );
        assert( !(tmp_res != 0) );
        frame_7d523f7f31ee26acdccc49520af196e2->m_frame.f_lineno = 172;
        tmp_call_result_1 = CALL_FUNCTION_WITH_KEYARGS( tmp_called_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 172;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7d523f7f31ee26acdccc49520af196e2 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_7d523f7f31ee26acdccc49520af196e2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_7d523f7f31ee26acdccc49520af196e2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_7d523f7f31ee26acdccc49520af196e2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_7d523f7f31ee26acdccc49520af196e2, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_7d523f7f31ee26acdccc49520af196e2,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_7d523f7f31ee26acdccc49520af196e2 == cache_frame_7d523f7f31ee26acdccc49520af196e2 )
    {
        Py_DECREF( frame_7d523f7f31ee26acdccc49520af196e2 );
    }
    cache_frame_7d523f7f31ee26acdccc49520af196e2 = NULL;

    assertFrameObject( frame_7d523f7f31ee26acdccc49520af196e2 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_6__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_6__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_7__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_data = NULL;
    struct Nuitka_FrameObject *frame_46b2eb01f86e4d19277b86b02001e840;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_46b2eb01f86e4d19277b86b02001e840 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_46b2eb01f86e4d19277b86b02001e840, codeobj_46b2eb01f86e4d19277b86b02001e840, module_prompt_toolkit$key_binding$bindings$basic, sizeof(void *)+sizeof(void *) );
    frame_46b2eb01f86e4d19277b86b02001e840 = cache_frame_46b2eb01f86e4d19277b86b02001e840;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_46b2eb01f86e4d19277b86b02001e840 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_46b2eb01f86e4d19277b86b02001e840 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_called_instance_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_called_instance_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_current_buffer );
        if ( tmp_called_instance_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        frame_46b2eb01f86e4d19277b86b02001e840->m_frame.f_lineno = 176;
        tmp_assign_source_1 = CALL_METHOD_NO_ARGS( tmp_called_instance_1, const_str_plain_cut_selection );
        Py_DECREF( tmp_called_instance_1 );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 176;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_data == NULL );
        var_data = tmp_assign_source_1;
    }
    {
        PyObject *tmp_called_instance_2;
        PyObject *tmp_source_name_2;
        PyObject *tmp_source_name_3;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_source_name_2 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_app );
        if ( tmp_source_name_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        tmp_called_instance_2 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_clipboard );
        Py_DECREF( tmp_source_name_2 );
        if ( tmp_called_instance_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_data );
        tmp_args_element_name_1 = var_data;
        frame_46b2eb01f86e4d19277b86b02001e840->m_frame.f_lineno = 177;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_2, const_str_plain_set_data, call_args );
        }

        Py_DECREF( tmp_called_instance_2 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 177;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_46b2eb01f86e4d19277b86b02001e840 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_46b2eb01f86e4d19277b86b02001e840 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_46b2eb01f86e4d19277b86b02001e840, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_46b2eb01f86e4d19277b86b02001e840->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_46b2eb01f86e4d19277b86b02001e840, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_46b2eb01f86e4d19277b86b02001e840,
        type_description_1,
        par_event,
        var_data
    );


    // Release cached frame.
    if ( frame_46b2eb01f86e4d19277b86b02001e840 == cache_frame_46b2eb01f86e4d19277b86b02001e840 )
    {
        Py_DECREF( frame_46b2eb01f86e4d19277b86b02001e840 );
    }
    cache_frame_46b2eb01f86e4d19277b86b02001e840 = NULL;

    assertFrameObject( frame_46b2eb01f86e4d19277b86b02001e840 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_7__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_data );
    Py_DECREF( var_data );
    var_data = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_7__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_8__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_0989717c2546f21f166c14362457ab06;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_0989717c2546f21f166c14362457ab06 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_0989717c2546f21f166c14362457ab06, codeobj_0989717c2546f21f166c14362457ab06, module_prompt_toolkit$key_binding$bindings$basic, sizeof(void *) );
    frame_0989717c2546f21f166c14362457ab06 = cache_frame_0989717c2546f21f166c14362457ab06;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_0989717c2546f21f166c14362457ab06 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_0989717c2546f21f166c14362457ab06 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        PyObject *tmp_source_name_3;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_insert_text );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_args_element_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_data );
        if ( tmp_args_element_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 192;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        frame_0989717c2546f21f166c14362457ab06->m_frame.f_lineno = 192;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_FUNCTION_WITH_ARGS1( tmp_called_name_1, call_args );
        }

        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_element_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 192;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0989717c2546f21f166c14362457ab06 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_0989717c2546f21f166c14362457ab06 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_0989717c2546f21f166c14362457ab06, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_0989717c2546f21f166c14362457ab06->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_0989717c2546f21f166c14362457ab06, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_0989717c2546f21f166c14362457ab06,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_0989717c2546f21f166c14362457ab06 == cache_frame_0989717c2546f21f166c14362457ab06 )
    {
        Py_DECREF( frame_0989717c2546f21f166c14362457ab06 );
    }
    cache_frame_0989717c2546f21f166c14362457ab06 = NULL;

    assertFrameObject( frame_0989717c2546f21f166c14362457ab06 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_8__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_8__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_9__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    PyObject *var_data = NULL;
    struct Nuitka_FrameObject *frame_8e6a86834f3d58c22814a3cb40a4bcc7;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_8e6a86834f3d58c22814a3cb40a4bcc7 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_8e6a86834f3d58c22814a3cb40a4bcc7, codeobj_8e6a86834f3d58c22814a3cb40a4bcc7, module_prompt_toolkit$key_binding$bindings$basic, sizeof(void *)+sizeof(void *) );
    frame_8e6a86834f3d58c22814a3cb40a4bcc7 = cache_frame_8e6a86834f3d58c22814a3cb40a4bcc7;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_8e6a86834f3d58c22814a3cb40a4bcc7 );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_8e6a86834f3d58c22814a3cb40a4bcc7 ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_assign_source_1;
        PyObject *tmp_source_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_1 = par_event;
        tmp_assign_source_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_data );
        if ( tmp_assign_source_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 197;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        assert( var_data == NULL );
        var_data = tmp_assign_source_1;
    }
    {
        PyObject *tmp_assign_source_2;
        PyObject *tmp_called_instance_1;
        CHECK_OBJECT( var_data );
        tmp_called_instance_1 = var_data;
        frame_8e6a86834f3d58c22814a3cb40a4bcc7->m_frame.f_lineno = 202;
        tmp_assign_source_2 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_1, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_digest_7ca129d2d421fe965ad48cbb290b579b_str_newline_tuple, 0 ) );

        if ( tmp_assign_source_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 202;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_data;
            assert( old != NULL );
            var_data = tmp_assign_source_2;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_assign_source_3;
        PyObject *tmp_called_instance_2;
        CHECK_OBJECT( var_data );
        tmp_called_instance_2 = var_data;
        frame_8e6a86834f3d58c22814a3cb40a4bcc7->m_frame.f_lineno = 203;
        tmp_assign_source_3 = CALL_METHOD_WITH_ARGS2( tmp_called_instance_2, const_str_plain_replace, &PyTuple_GET_ITEM( const_tuple_str_chr_13_str_newline_tuple, 0 ) );

        if ( tmp_assign_source_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 203;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        {
            PyObject *old = var_data;
            assert( old != NULL );
            var_data = tmp_assign_source_3;
            Py_DECREF( old );
        }

    }
    {
        PyObject *tmp_called_instance_3;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_element_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_called_instance_3 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_called_instance_3 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 205;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( var_data );
        tmp_args_element_name_1 = var_data;
        frame_8e6a86834f3d58c22814a3cb40a4bcc7->m_frame.f_lineno = 205;
        {
            PyObject *call_args[] = { tmp_args_element_name_1 };
            tmp_call_result_1 = CALL_METHOD_WITH_ARGS1( tmp_called_instance_3, const_str_plain_insert_text, call_args );
        }

        Py_DECREF( tmp_called_instance_3 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 205;
            type_description_1 = "oo";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e6a86834f3d58c22814a3cb40a4bcc7 );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_8e6a86834f3d58c22814a3cb40a4bcc7 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_8e6a86834f3d58c22814a3cb40a4bcc7, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_8e6a86834f3d58c22814a3cb40a4bcc7->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_8e6a86834f3d58c22814a3cb40a4bcc7, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_8e6a86834f3d58c22814a3cb40a4bcc7,
        type_description_1,
        par_event,
        var_data
    );


    // Release cached frame.
    if ( frame_8e6a86834f3d58c22814a3cb40a4bcc7 == cache_frame_8e6a86834f3d58c22814a3cb40a4bcc7 )
    {
        Py_DECREF( frame_8e6a86834f3d58c22814a3cb40a4bcc7 );
    }
    cache_frame_8e6a86834f3d58c22814a3cb40a4bcc7 = NULL;

    assertFrameObject( frame_8e6a86834f3d58c22814a3cb40a4bcc7 );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_9__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    CHECK_OBJECT( (PyObject *)var_data );
    Py_DECREF( var_data );
    var_data = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    Py_XDECREF( var_data );
    var_data = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_9__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_11_lambda( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    struct Nuitka_FrameObject *frame_dea0d25ef92a25226ef8ac77a4bcf66b;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    static struct Nuitka_FrameObject *cache_frame_dea0d25ef92a25226ef8ac77a4bcf66b = NULL;

    // Actual function body.
    MAKE_OR_REUSE_FRAME( cache_frame_dea0d25ef92a25226ef8ac77a4bcf66b, codeobj_dea0d25ef92a25226ef8ac77a4bcf66b, module_prompt_toolkit$key_binding$bindings$basic, 0 );
    frame_dea0d25ef92a25226ef8ac77a4bcf66b = cache_frame_dea0d25ef92a25226ef8ac77a4bcf66b;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_dea0d25ef92a25226ef8ac77a4bcf66b );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_dea0d25ef92a25226ef8ac77a4bcf66b ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_source_name_1;
        PyObject *tmp_called_name_1;
        PyObject *tmp_mvar_value_1;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_app );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain_get_app );
        }

        if ( tmp_mvar_value_1 == NULL )
        {

            exception_type = PyExc_NameError;
            Py_INCREF( exception_type );
            exception_value = PyUnicode_FromFormat( "name '%s' is not defined", "get_app" );
            exception_tb = NULL;
            NORMALIZE_EXCEPTION( &exception_type, &exception_value, &exception_tb );
            CHAIN_EXCEPTION( exception_value );

            exception_lineno = 207;

            goto frame_exception_exit_1;
        }

        tmp_called_name_1 = tmp_mvar_value_1;
        frame_dea0d25ef92a25226ef8ac77a4bcf66b->m_frame.f_lineno = 207;
        tmp_source_name_1 = CALL_FUNCTION_NO_ARGS( tmp_called_name_1 );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;

            goto frame_exception_exit_1;
        }
        tmp_return_value = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_quoted_insert );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_return_value == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 207;

            goto frame_exception_exit_1;
        }
        goto frame_return_exit_1;
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dea0d25ef92a25226ef8ac77a4bcf66b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_return_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_dea0d25ef92a25226ef8ac77a4bcf66b );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto function_return_exit;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_dea0d25ef92a25226ef8ac77a4bcf66b );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_dea0d25ef92a25226ef8ac77a4bcf66b, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_dea0d25ef92a25226ef8ac77a4bcf66b->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_dea0d25ef92a25226ef8ac77a4bcf66b, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_dea0d25ef92a25226ef8ac77a4bcf66b,
        type_description_1
    );


    // Release cached frame.
    if ( frame_dea0d25ef92a25226ef8ac77a4bcf66b == cache_frame_dea0d25ef92a25226ef8ac77a4bcf66b )
    {
        Py_DECREF( frame_dea0d25ef92a25226ef8ac77a4bcf66b );
    }
    cache_frame_dea0d25ef92a25226ef8ac77a4bcf66b = NULL;

    assertFrameObject( frame_dea0d25ef92a25226ef8ac77a4bcf66b );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto function_exception_exit;

    frame_no_exception_1:;

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_11_lambda );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}


static PyObject *impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_10__( struct Nuitka_FunctionObject const *self, PyObject **python_pars )
{
    // Preserve error status for checks
#ifndef __NUITKA_NO_ASSERT__
    NUITKA_MAY_BE_UNUSED bool had_error = ERROR_OCCURRED();
#endif

    // Local variable declarations.
    PyObject *par_event = python_pars[ 0 ];
    struct Nuitka_FrameObject *frame_18156eef96c515595973ffefa4cad84a;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    bool tmp_result;
    static struct Nuitka_FrameObject *cache_frame_18156eef96c515595973ffefa4cad84a = NULL;
    PyObject *tmp_return_value = NULL;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Actual function body.
    // Tried code:
    MAKE_OR_REUSE_FRAME( cache_frame_18156eef96c515595973ffefa4cad84a, codeobj_18156eef96c515595973ffefa4cad84a, module_prompt_toolkit$key_binding$bindings$basic, sizeof(void *) );
    frame_18156eef96c515595973ffefa4cad84a = cache_frame_18156eef96c515595973ffefa4cad84a;

    // Push the new frame as the currently active one.
    pushFrameStack( frame_18156eef96c515595973ffefa4cad84a );

    // Mark the frame object as in use, ref count 1 will be up for reuse.
    assert( Py_REFCNT( frame_18156eef96c515595973ffefa4cad84a ) == 2 ); // Frame stack

    // Framed code:
    {
        PyObject *tmp_called_name_1;
        PyObject *tmp_source_name_1;
        PyObject *tmp_source_name_2;
        PyObject *tmp_call_result_1;
        PyObject *tmp_args_name_1;
        PyObject *tmp_tuple_element_1;
        PyObject *tmp_source_name_3;
        PyObject *tmp_kw_name_1;
        CHECK_OBJECT( par_event );
        tmp_source_name_2 = par_event;
        tmp_source_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_2, const_str_plain_current_buffer );
        if ( tmp_source_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_called_name_1 = LOOKUP_ATTRIBUTE( tmp_source_name_1, const_str_plain_insert_text );
        Py_DECREF( tmp_source_name_1 );
        if ( tmp_called_name_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        CHECK_OBJECT( par_event );
        tmp_source_name_3 = par_event;
        tmp_tuple_element_1 = LOOKUP_ATTRIBUTE( tmp_source_name_3, const_str_plain_data );
        if ( tmp_tuple_element_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );
            Py_DECREF( tmp_called_name_1 );

            exception_lineno = 212;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_args_name_1 = PyTuple_New( 1 );
        PyTuple_SET_ITEM( tmp_args_name_1, 0, tmp_tuple_element_1 );
        tmp_kw_name_1 = PyDict_Copy( const_dict_e4181dea19ade43f9b9633b3e1fd3350 );
        frame_18156eef96c515595973ffefa4cad84a->m_frame.f_lineno = 212;
        tmp_call_result_1 = CALL_FUNCTION( tmp_called_name_1, tmp_args_name_1, tmp_kw_name_1 );
        Py_DECREF( tmp_called_name_1 );
        Py_DECREF( tmp_args_name_1 );
        Py_DECREF( tmp_kw_name_1 );
        if ( tmp_call_result_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 212;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        Py_DECREF( tmp_call_result_1 );
    }
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_source_name_4;
        tmp_assattr_name_1 = Py_False;
        CHECK_OBJECT( par_event );
        tmp_source_name_4 = par_event;
        tmp_assattr_target_1 = LOOKUP_ATTRIBUTE( tmp_source_name_4, const_str_plain_app );
        if ( tmp_assattr_target_1 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_quoted_insert, tmp_assattr_name_1 );
        Py_DECREF( tmp_assattr_target_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 213;
            type_description_1 = "o";
            goto frame_exception_exit_1;
        }
    }

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18156eef96c515595973ffefa4cad84a );
#endif

    // Put the previous frame back on top.
    popFrameStack();

    goto frame_no_exception_1;

    frame_exception_exit_1:;

#if 0
    RESTORE_FRAME_EXCEPTION( frame_18156eef96c515595973ffefa4cad84a );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_18156eef96c515595973ffefa4cad84a, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_18156eef96c515595973ffefa4cad84a->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_18156eef96c515595973ffefa4cad84a, exception_lineno );
    }

    // Attachs locals to frame if any.
    Nuitka_Frame_AttachLocals(
        (struct Nuitka_FrameObject *)frame_18156eef96c515595973ffefa4cad84a,
        type_description_1,
        par_event
    );


    // Release cached frame.
    if ( frame_18156eef96c515595973ffefa4cad84a == cache_frame_18156eef96c515595973ffefa4cad84a )
    {
        Py_DECREF( frame_18156eef96c515595973ffefa4cad84a );
    }
    cache_frame_18156eef96c515595973ffefa4cad84a = NULL;

    assertFrameObject( frame_18156eef96c515595973ffefa4cad84a );

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto try_except_handler_1;

    frame_no_exception_1:;
    tmp_return_value = Py_None;
    Py_INCREF( tmp_return_value );
    goto try_return_handler_1;
    // tried codes exits in all cases
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_10__ );
    return NULL;
    // Return handler code:
    try_return_handler_1:;
    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    goto function_return_exit;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)par_event );
    Py_DECREF( par_event );
    par_event = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto function_exception_exit;
    // End of try:

    // Return statement must have exited already.
    NUITKA_CANNOT_GET_HERE( prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_10__ );
    return NULL;

function_exception_exit:
    assert( exception_type );
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );

    return NULL;

function_return_exit:
   // Function cleanup code if any.


   // Actual function exit with return value, making sure we did not make
   // the error status worse despite non-NULL return.
   CHECK_OBJECT( tmp_return_value );
   assert( had_error || !ERROR_OCCURRED() );
   return tmp_return_value;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_1_if_no_repeat(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_1_if_no_repeat,
        const_str_plain_if_no_repeat,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_1fa52785aaac79e9088c70526f118a18,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        const_str_digest_9cccc676da4746edb9fb23677cfeea9d,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings,
        const_str_plain_load_basic_bindings,
#if PYTHON_VERSION >= 300
        NULL,
#endif
        codeobj_078d10575ac2c0fb043b17bda86af556,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_10__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_10__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_67c9f6e346eebf99869d712d74a96db7,
#endif
        codeobj_18156eef96c515595973ffefa4cad84a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        const_str_digest_fa8868e1b83a9d3b797546af06905b98,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_11_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_11_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_96aecf7b4adb5c3dbb234466987ace12,
#endif
        codeobj_dea0d25ef92a25226ef8ac77a4bcf66b,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_1__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_1__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_67c9f6e346eebf99869d712d74a96db7,
#endif
        codeobj_fc993341e9a0c21993f19d371d0bd0f7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        const_str_digest_f7fef436b96330c714feb9f16a46dfa1,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_2_lambda(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_2_lambda,
        const_str_angle_lambda,
#if PYTHON_VERSION >= 300
        const_str_digest_96aecf7b4adb5c3dbb234466987ace12,
#endif
        codeobj_c938375228a721c91a9db1d8d4517f6e,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_3__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_3__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_67c9f6e346eebf99869d712d74a96db7,
#endif
        codeobj_4e9b1d787fd3d410289f5155384148b9,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        const_str_digest_883cd3eeb3aa1b4846f1c9b83f62281a,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_4__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_4__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_67c9f6e346eebf99869d712d74a96db7,
#endif
        codeobj_7e2a60d14f94af675b3e06051a6f3587,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        const_str_digest_218b33d507522522a424d95c99dc4fc8,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_5__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_5__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_67c9f6e346eebf99869d712d74a96db7,
#endif
        codeobj_45d48e22ad81947896cb4fd75504e88a,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_6__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_6__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_67c9f6e346eebf99869d712d74a96db7,
#endif
        codeobj_7d523f7f31ee26acdccc49520af196e2,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_7__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_7__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_67c9f6e346eebf99869d712d74a96db7,
#endif
        codeobj_46b2eb01f86e4d19277b86b02001e840,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        NULL,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_8__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_8__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_67c9f6e346eebf99869d712d74a96db7,
#endif
        codeobj_0989717c2546f21f166c14362457ab06,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        const_str_digest_240b478312ccc51a929b8f08d19fd830,
        0
    );

    return (PyObject *)result;
}



static PyObject *MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_9__(  )
{
    struct Nuitka_FunctionObject *result = Nuitka_Function_New(
        impl_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings$$$function_9__,
        const_str_plain__,
#if PYTHON_VERSION >= 300
        const_str_digest_67c9f6e346eebf99869d712d74a96db7,
#endif
        codeobj_8e6a86834f3d58c22814a3cb40a4bcc7,
        NULL,
#if PYTHON_VERSION >= 300
        NULL,
        NULL,
#endif
        module_prompt_toolkit$key_binding$bindings$basic,
        const_str_digest_c2d3a8abc72fb9a2a0c25a3f41726cd6,
        0
    );

    return (PyObject *)result;
}



#if PYTHON_VERSION >= 300
static struct PyModuleDef mdef_prompt_toolkit$key_binding$bindings$basic =
{
    PyModuleDef_HEAD_INIT,
    "prompt_toolkit.key_binding.bindings.basic",
    NULL,                /* m_doc */
    -1,                  /* m_size */
    NULL,                /* m_methods */
    NULL,                /* m_reload */
    NULL,                /* m_traverse */
    NULL,                /* m_clear */
    NULL,                /* m_free */
  };
#endif

extern PyObject *const_str_plain___compiled__;

extern PyObject *const_str_plain___package__;

#if PYTHON_VERSION >= 300
extern PyObject *const_str_dot;
extern PyObject *const_str_plain___loader__;
#endif

#if PYTHON_VERSION >= 340
extern PyObject *const_str_plain___spec__;
extern PyObject *const_str_plain__initializing;
extern PyObject *const_str_plain_submodule_search_locations;
#endif

extern void _initCompiledCellType();
extern void _initCompiledGeneratorType();
extern void _initCompiledFunctionType();
extern void _initCompiledMethodType();
extern void _initCompiledFrameType();
#if PYTHON_VERSION >= 350
extern void _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
extern void _initCompiledAsyncgenTypes();
#endif

extern PyTypeObject Nuitka_Loader_Type;

#if defined(_NUITKA_EXE) || !0
// For executables or non top level modules, we need not export anything.
MOD_ENTRY_DECL(prompt_toolkit$key_binding$bindings$basic)
#else
// The exported interface to CPython. On import of the module, this function
// gets called. It has to have an exact function name, in cases it's a shared
// library export. This is hidden behind the MOD_INIT_DECL macro.
MOD_INIT_DECL(prompt_toolkit$key_binding$bindings$basic)
#endif
{
#if defined(_NUITKA_EXE) || PYTHON_VERSION >= 300
    static bool _init_done = false;

    // Modules might be imported repeatedly, which is to be ignored.
    if ( _init_done )
    {
        return MOD_RETURN_VALUE( module_prompt_toolkit$key_binding$bindings$basic );
    }
    else
    {
        _init_done = true;
    }
#endif

#ifdef _NUITKA_MODULE
    // In case of a stand alone extension module, need to call initialization
    // the init here because that's the first and only time we are going to get
    // called here.

    // May have to activate constants blob.
#if defined(_NUITKA_CONSTANTS_FROM_RESOURCE)
    loadConstantsResource();
#endif

    // Initialize the constant values used.
    _initBuiltinModule();
    createGlobalConstants();

    /* Initialize the compiled types of Nuitka. */
    _initCompiledCellType();
    _initCompiledGeneratorType();
    _initCompiledFunctionType();
    _initCompiledMethodType();
    _initCompiledFrameType();
#if PYTHON_VERSION >= 350
    _initCompiledCoroutineTypes();
#endif
#if PYTHON_VERSION >= 360
    _initCompiledAsyncgenTypes();
#endif

#if PYTHON_VERSION < 300
    _initSlotCompare();
#endif
#if PYTHON_VERSION >= 270
    _initSlotIternext();
#endif

    patchBuiltinModule();
    patchTypeComparison();

    // Enable meta path based loader if not already done.
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.basic: Calling setupMetaPathBasedLoader().");
#endif
    setupMetaPathBasedLoader();

#if PYTHON_VERSION >= 300
    patchInspectModule();
#endif

#endif

    /* The constants only used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.basic: Calling createModuleConstants().");
#endif
    createModuleConstants();

    /* The code objects used by this module are created now. */
#ifdef _NUITKA_TRACE
    puts("prompt_toolkit.key_binding.bindings.basic: Calling createModuleCodeObjects().");
#endif
    createModuleCodeObjects();

    // puts( "in initprompt_toolkit$key_binding$bindings$basic" );

    // Create the module object first. There are no methods initially, all are
    // added dynamically in actual code only.  Also no "__doc__" is initially
    // set at this time, as it could not contain NUL characters this way, they
    // are instead set in early module code.  No "self" for modules, we have no
    // use for it.
#if PYTHON_VERSION < 300
    module_prompt_toolkit$key_binding$bindings$basic = Py_InitModule4(
        "prompt_toolkit.key_binding.bindings.basic",       // Module Name
        NULL,                    // No methods initially, all are added
                                 // dynamically in actual module code only.
        NULL,                    // No "__doc__" is initially set, as it could
                                 // not contain NUL this way, added early in
                                 // actual code.
        NULL,                    // No self for modules, we don't use it.
        PYTHON_API_VERSION
    );
#else

    module_prompt_toolkit$key_binding$bindings$basic = PyModule_Create( &mdef_prompt_toolkit$key_binding$bindings$basic );
#endif

    moduledict_prompt_toolkit$key_binding$bindings$basic = MODULE_DICT( module_prompt_toolkit$key_binding$bindings$basic );

    // Set __compiled__ to what it we know.
    UPDATE_STRING_DICT1(
        moduledict_prompt_toolkit$key_binding$bindings$basic,
        (Nuitka_StringObject *)const_str_plain___compiled__,
        Nuitka_dunder_compiled_value
    );

    // Update "__package__" value to what it ought to be.
    {
#if 0
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___name__ );

        UPDATE_STRING_DICT1(
            moduledict_prompt_toolkit$key_binding$bindings$basic,
            (Nuitka_StringObject *)const_str_plain___package__,
            module_name
        );
#else

#if PYTHON_VERSION < 300
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___name__ );
        char const *module_name_cstr = PyString_AS_STRING( module_name );

        char const *last_dot = strrchr( module_name_cstr, '.' );

        if ( last_dot != NULL )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$key_binding$bindings$basic,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyString_FromStringAndSize( module_name_cstr, last_dot - module_name_cstr )
            );
        }
#else
        PyObject *module_name = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___name__ );
        Py_ssize_t dot_index = PyUnicode_Find( module_name, const_str_dot, 0, PyUnicode_GetLength( module_name ), -1 );

        if ( dot_index != -1 )
        {
            UPDATE_STRING_DICT1(
                moduledict_prompt_toolkit$key_binding$bindings$basic,
                (Nuitka_StringObject *)const_str_plain___package__,
                PyUnicode_Substring( module_name, 0, dot_index )
            );
        }
#endif
#endif
    }

    CHECK_OBJECT( module_prompt_toolkit$key_binding$bindings$basic );

// Seems to work for Python2.7 out of the box, but for Python3, the module
// doesn't automatically enter "sys.modules", so do it manually.
#if PYTHON_VERSION >= 300
    {
        int r = PyObject_SetItem( PyImport_GetModuleDict(), const_str_digest_bca2dca6bc566bcc5093959d9941cbc9, module_prompt_toolkit$key_binding$bindings$basic );

        assert( r != -1 );
    }
#endif

    // For deep importing of a module we need to have "__builtins__", so we set
    // it ourselves in the same way than CPython does. Note: This must be done
    // before the frame object is allocated, or else it may fail.

    if ( GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___builtins__ ) == NULL )
    {
        PyObject *value = (PyObject *)builtin_module;

        // Check if main module, not a dict then but the module itself.
#if !defined(_NUITKA_EXE) || !0
        value = PyModule_GetDict( value );
#endif

        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___builtins__, value );
    }

#if PYTHON_VERSION >= 300
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___loader__, (PyObject *)&Nuitka_Loader_Type );
#endif

#if PYTHON_VERSION >= 340
// Set the "__spec__" value

#if 0
    // Main modules just get "None" as spec.
    UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___spec__, Py_None );
#else
    // Other modules get a "ModuleSpec" from the standard mechanism.
    {
        PyObject *bootstrap_module = PyImport_ImportModule("importlib._bootstrap");
        CHECK_OBJECT( bootstrap_module );
        PyObject *module_spec_class = PyObject_GetAttrString( bootstrap_module, "ModuleSpec" );
        Py_DECREF( bootstrap_module );

        PyObject *args[] = {
            GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___name__ ),
            (PyObject *)&Nuitka_Loader_Type
        };

        PyObject *spec_value = CALL_FUNCTION_WITH_ARGS2(
            module_spec_class,
            args
        );
        Py_DECREF( module_spec_class );

        // We can assume this to never fail, or else we are in trouble anyway.
        CHECK_OBJECT( spec_value );

// For packages set the submodule search locations as well, even if to empty
// list, so investigating code will consider it a package.
#if 0
        SET_ATTRIBUTE( spec_value, const_str_plain_submodule_search_locations, PyList_New(0) );
#endif

// Mark the execution in the "__spec__" value.
        SET_ATTRIBUTE( spec_value, const_str_plain__initializing, Py_True );

        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___spec__, spec_value );
    }
#endif
#endif

    // Temp variables if any
    PyObject *tmp_import_from_1__module = NULL;
    struct Nuitka_FrameObject *frame_39e21f71248a54d4e4b95230c4628dc2;
    NUITKA_MAY_BE_UNUSED char const *type_description_1 = NULL;
    bool tmp_result;
    PyObject *exception_type = NULL;
    PyObject *exception_value = NULL;
    PyTracebackObject *exception_tb = NULL;
    NUITKA_MAY_BE_UNUSED int exception_lineno = 0;
    PyObject *exception_keeper_type_1;
    PyObject *exception_keeper_value_1;
    PyTracebackObject *exception_keeper_tb_1;
    NUITKA_MAY_BE_UNUSED int exception_keeper_lineno_1;

    // Module code.
    {
        PyObject *tmp_assign_source_1;
        tmp_assign_source_1 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___doc__, tmp_assign_source_1 );
    }
    {
        PyObject *tmp_assign_source_2;
        tmp_assign_source_2 = module_filename_obj;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___file__, tmp_assign_source_2 );
    }
    // Frame without reuse.
    frame_39e21f71248a54d4e4b95230c4628dc2 = MAKE_MODULE_FRAME( codeobj_39e21f71248a54d4e4b95230c4628dc2, module_prompt_toolkit$key_binding$bindings$basic );

    // Push the new frame as the currently active one, and we should be exclusively
    // owning it.
    pushFrameStack( frame_39e21f71248a54d4e4b95230c4628dc2 );
    assert( Py_REFCNT( frame_39e21f71248a54d4e4b95230c4628dc2 ) == 2 );

    // Framed code:
    {
        PyObject *tmp_assattr_name_1;
        PyObject *tmp_assattr_target_1;
        PyObject *tmp_mvar_value_1;
        tmp_assattr_name_1 = module_filename_obj;
        tmp_mvar_value_1 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_1 == NULL ))
        {
            tmp_mvar_value_1 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_1 );
        tmp_assattr_target_1 = tmp_mvar_value_1;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_1, const_str_plain_origin, tmp_assattr_name_1 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assattr_name_2;
        PyObject *tmp_assattr_target_2;
        PyObject *tmp_mvar_value_2;
        tmp_assattr_name_2 = Py_True;
        tmp_mvar_value_2 = GET_STRING_DICT_VALUE( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___spec__ );

        if (unlikely( tmp_mvar_value_2 == NULL ))
        {
            tmp_mvar_value_2 = GET_STRING_DICT_VALUE( dict_builtin, (Nuitka_StringObject *)const_str_plain___spec__ );
        }

        CHECK_OBJECT( tmp_mvar_value_2 );
        tmp_assattr_target_2 = tmp_mvar_value_2;
        tmp_result = SET_ATTRIBUTE( tmp_assattr_target_2, const_str_plain_has_location, tmp_assattr_name_2 );
        if ( tmp_result == false )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 1;

            goto frame_exception_exit_1;
        }
    }
    {
        PyObject *tmp_assign_source_3;
        tmp_assign_source_3 = Py_None;
        UPDATE_STRING_DICT0( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___cached__, tmp_assign_source_3 );
    }
    {
        PyObject *tmp_assign_source_4;
        PyObject *tmp_import_name_from_1;
        frame_39e21f71248a54d4e4b95230c4628dc2->m_frame.f_lineno = 2;
        tmp_import_name_from_1 = PyImport_ImportModule("__future__");
        assert( !(tmp_import_name_from_1 == NULL) );
        tmp_assign_source_4 = IMPORT_NAME( tmp_import_name_from_1, const_str_plain_unicode_literals );
        if ( tmp_assign_source_4 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 2;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_unicode_literals, tmp_assign_source_4 );
    }
    {
        PyObject *tmp_assign_source_5;
        PyObject *tmp_import_name_from_2;
        PyObject *tmp_name_name_1;
        PyObject *tmp_globals_name_1;
        PyObject *tmp_locals_name_1;
        PyObject *tmp_fromlist_name_1;
        PyObject *tmp_level_name_1;
        tmp_name_name_1 = const_str_digest_722e5e8c7f53b34663d9669ca7ddc056;
        tmp_globals_name_1 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$basic;
        tmp_locals_name_1 = Py_None;
        tmp_fromlist_name_1 = const_tuple_str_plain_get_app_tuple;
        tmp_level_name_1 = const_int_0;
        frame_39e21f71248a54d4e4b95230c4628dc2->m_frame.f_lineno = 4;
        tmp_import_name_from_2 = IMPORT_MODULE5( tmp_name_name_1, tmp_globals_name_1, tmp_locals_name_1, tmp_fromlist_name_1, tmp_level_name_1 );
        if ( tmp_import_name_from_2 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_5 = IMPORT_NAME( tmp_import_name_from_2, const_str_plain_get_app );
        Py_DECREF( tmp_import_name_from_2 );
        if ( tmp_assign_source_5 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 4;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_app, tmp_assign_source_5 );
    }
    {
        PyObject *tmp_assign_source_6;
        PyObject *tmp_name_name_2;
        PyObject *tmp_globals_name_2;
        PyObject *tmp_locals_name_2;
        PyObject *tmp_fromlist_name_2;
        PyObject *tmp_level_name_2;
        tmp_name_name_2 = const_str_digest_3a16fa49bea795e638aac8247b0f7f8d;
        tmp_globals_name_2 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$basic;
        tmp_locals_name_2 = Py_None;
        tmp_fromlist_name_2 = const_tuple_c96c9c34187205cbc4a189c4b751ac8f_tuple;
        tmp_level_name_2 = const_int_0;
        frame_39e21f71248a54d4e4b95230c4628dc2->m_frame.f_lineno = 5;
        tmp_assign_source_6 = IMPORT_MODULE5( tmp_name_name_2, tmp_globals_name_2, tmp_locals_name_2, tmp_fromlist_name_2, tmp_level_name_2 );
        if ( tmp_assign_source_6 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto frame_exception_exit_1;
        }
        assert( tmp_import_from_1__module == NULL );
        tmp_import_from_1__module = tmp_assign_source_6;
    }
    // Tried code:
    {
        PyObject *tmp_assign_source_7;
        PyObject *tmp_import_name_from_3;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_3 = tmp_import_from_1__module;
        tmp_assign_source_7 = IMPORT_NAME( tmp_import_name_from_3, const_str_plain_has_selection );
        if ( tmp_assign_source_7 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_has_selection, tmp_assign_source_7 );
    }
    {
        PyObject *tmp_assign_source_8;
        PyObject *tmp_import_name_from_4;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_4 = tmp_import_from_1__module;
        tmp_assign_source_8 = IMPORT_NAME( tmp_import_name_from_4, const_str_plain_Condition );
        if ( tmp_assign_source_8 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_Condition, tmp_assign_source_8 );
    }
    {
        PyObject *tmp_assign_source_9;
        PyObject *tmp_import_name_from_5;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_5 = tmp_import_from_1__module;
        tmp_assign_source_9 = IMPORT_NAME( tmp_import_name_from_5, const_str_plain_emacs_insert_mode );
        if ( tmp_assign_source_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_emacs_insert_mode, tmp_assign_source_9 );
    }
    {
        PyObject *tmp_assign_source_10;
        PyObject *tmp_import_name_from_6;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_6 = tmp_import_from_1__module;
        tmp_assign_source_10 = IMPORT_NAME( tmp_import_name_from_6, const_str_plain_vi_insert_mode );
        if ( tmp_assign_source_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_vi_insert_mode, tmp_assign_source_10 );
    }
    {
        PyObject *tmp_assign_source_11;
        PyObject *tmp_import_name_from_7;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_7 = tmp_import_from_1__module;
        tmp_assign_source_11 = IMPORT_NAME( tmp_import_name_from_7, const_str_plain_in_paste_mode );
        if ( tmp_assign_source_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_in_paste_mode, tmp_assign_source_11 );
    }
    {
        PyObject *tmp_assign_source_12;
        PyObject *tmp_import_name_from_8;
        CHECK_OBJECT( tmp_import_from_1__module );
        tmp_import_name_from_8 = tmp_import_from_1__module;
        tmp_assign_source_12 = IMPORT_NAME( tmp_import_name_from_8, const_str_plain_is_multiline );
        if ( tmp_assign_source_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 5;

            goto try_except_handler_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_is_multiline, tmp_assign_source_12 );
    }
    goto try_end_1;
    // Exception handler code:
    try_except_handler_1:;
    exception_keeper_type_1 = exception_type;
    exception_keeper_value_1 = exception_value;
    exception_keeper_tb_1 = exception_tb;
    exception_keeper_lineno_1 = exception_lineno;
    exception_type = NULL;
    exception_value = NULL;
    exception_tb = NULL;
    exception_lineno = 0;

    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    // Re-raise.
    exception_type = exception_keeper_type_1;
    exception_value = exception_keeper_value_1;
    exception_tb = exception_keeper_tb_1;
    exception_lineno = exception_keeper_lineno_1;

    goto frame_exception_exit_1;
    // End of try:
    try_end_1:;
    CHECK_OBJECT( (PyObject *)tmp_import_from_1__module );
    Py_DECREF( tmp_import_from_1__module );
    tmp_import_from_1__module = NULL;

    {
        PyObject *tmp_assign_source_13;
        PyObject *tmp_import_name_from_9;
        PyObject *tmp_name_name_3;
        PyObject *tmp_globals_name_3;
        PyObject *tmp_locals_name_3;
        PyObject *tmp_fromlist_name_3;
        PyObject *tmp_level_name_3;
        tmp_name_name_3 = const_str_digest_908c8ae8a1ea88b9446eaa47eb000fad;
        tmp_globals_name_3 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$basic;
        tmp_locals_name_3 = Py_None;
        tmp_fromlist_name_3 = const_tuple_str_plain_KeyPress_tuple;
        tmp_level_name_3 = const_int_0;
        frame_39e21f71248a54d4e4b95230c4628dc2->m_frame.f_lineno = 6;
        tmp_import_name_from_9 = IMPORT_MODULE5( tmp_name_name_3, tmp_globals_name_3, tmp_locals_name_3, tmp_fromlist_name_3, tmp_level_name_3 );
        if ( tmp_import_name_from_9 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_13 = IMPORT_NAME( tmp_import_name_from_9, const_str_plain_KeyPress );
        Py_DECREF( tmp_import_name_from_9 );
        if ( tmp_assign_source_13 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 6;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_KeyPress, tmp_assign_source_13 );
    }
    {
        PyObject *tmp_assign_source_14;
        PyObject *tmp_import_name_from_10;
        PyObject *tmp_name_name_4;
        PyObject *tmp_globals_name_4;
        PyObject *tmp_locals_name_4;
        PyObject *tmp_fromlist_name_4;
        PyObject *tmp_level_name_4;
        tmp_name_name_4 = const_str_digest_b7cd7d3061b29ef6badd990d7c83ddb1;
        tmp_globals_name_4 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$basic;
        tmp_locals_name_4 = Py_None;
        tmp_fromlist_name_4 = const_tuple_str_plain_Keys_tuple;
        tmp_level_name_4 = const_int_0;
        frame_39e21f71248a54d4e4b95230c4628dc2->m_frame.f_lineno = 7;
        tmp_import_name_from_10 = IMPORT_MODULE5( tmp_name_name_4, tmp_globals_name_4, tmp_locals_name_4, tmp_fromlist_name_4, tmp_level_name_4 );
        if ( tmp_import_name_from_10 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        tmp_assign_source_14 = IMPORT_NAME( tmp_import_name_from_10, const_str_plain_Keys );
        Py_DECREF( tmp_import_name_from_10 );
        if ( tmp_assign_source_14 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 7;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_Keys, tmp_assign_source_14 );
    }
    {
        PyObject *tmp_assign_source_15;
        PyObject *tmp_import_name_from_11;
        PyObject *tmp_name_name_5;
        PyObject *tmp_globals_name_5;
        PyObject *tmp_locals_name_5;
        PyObject *tmp_fromlist_name_5;
        PyObject *tmp_level_name_5;
        tmp_name_name_5 = const_str_plain_named_commands;
        tmp_globals_name_5 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$basic;
        tmp_locals_name_5 = Py_None;
        tmp_fromlist_name_5 = const_tuple_str_plain_get_by_name_tuple;
        tmp_level_name_5 = const_int_pos_1;
        frame_39e21f71248a54d4e4b95230c4628dc2->m_frame.f_lineno = 9;
        tmp_import_name_from_11 = IMPORT_MODULE5( tmp_name_name_5, tmp_globals_name_5, tmp_locals_name_5, tmp_fromlist_name_5, tmp_level_name_5 );
        if ( tmp_import_name_from_11 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_11 ) )
        {
           tmp_assign_source_15 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_11,
                (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$basic,
                const_str_plain_get_by_name,
                const_int_pos_1
            );
        }
        else
        {
           tmp_assign_source_15 = IMPORT_NAME( tmp_import_name_from_11, const_str_plain_get_by_name );
        }

        Py_DECREF( tmp_import_name_from_11 );
        if ( tmp_assign_source_15 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 9;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_get_by_name, tmp_assign_source_15 );
    }
    {
        PyObject *tmp_assign_source_16;
        PyObject *tmp_import_name_from_12;
        PyObject *tmp_name_name_6;
        PyObject *tmp_globals_name_6;
        PyObject *tmp_locals_name_6;
        PyObject *tmp_fromlist_name_6;
        PyObject *tmp_level_name_6;
        tmp_name_name_6 = const_str_plain_key_bindings;
        tmp_globals_name_6 = (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$basic;
        tmp_locals_name_6 = Py_None;
        tmp_fromlist_name_6 = const_tuple_str_plain_KeyBindings_tuple;
        tmp_level_name_6 = const_int_pos_2;
        frame_39e21f71248a54d4e4b95230c4628dc2->m_frame.f_lineno = 10;
        tmp_import_name_from_12 = IMPORT_MODULE5( tmp_name_name_6, tmp_globals_name_6, tmp_locals_name_6, tmp_fromlist_name_6, tmp_level_name_6 );
        if ( tmp_import_name_from_12 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        if ( PyModule_Check( tmp_import_name_from_12 ) )
        {
           tmp_assign_source_16 = IMPORT_NAME_OR_MODULE(
                tmp_import_name_from_12,
                (PyObject *)moduledict_prompt_toolkit$key_binding$bindings$basic,
                const_str_plain_KeyBindings,
                const_int_pos_2
            );
        }
        else
        {
           tmp_assign_source_16 = IMPORT_NAME( tmp_import_name_from_12, const_str_plain_KeyBindings );
        }

        Py_DECREF( tmp_import_name_from_12 );
        if ( tmp_assign_source_16 == NULL )
        {
            assert( ERROR_OCCURRED() );

            FETCH_ERROR_OCCURRED( &exception_type, &exception_value, &exception_tb );


            exception_lineno = 10;

            goto frame_exception_exit_1;
        }
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_KeyBindings, tmp_assign_source_16 );
    }

    // Restore frame exception if necessary.
#if 0
    RESTORE_FRAME_EXCEPTION( frame_39e21f71248a54d4e4b95230c4628dc2 );
#endif
    popFrameStack();

    assertFrameObject( frame_39e21f71248a54d4e4b95230c4628dc2 );

    goto frame_no_exception_1;

    frame_exception_exit_1:;
#if 0
    RESTORE_FRAME_EXCEPTION( frame_39e21f71248a54d4e4b95230c4628dc2 );
#endif

    if ( exception_tb == NULL )
    {
        exception_tb = MAKE_TRACEBACK( frame_39e21f71248a54d4e4b95230c4628dc2, exception_lineno );
    }
    else if ( exception_tb->tb_frame != &frame_39e21f71248a54d4e4b95230c4628dc2->m_frame )
    {
        exception_tb = ADD_TRACEBACK( exception_tb, frame_39e21f71248a54d4e4b95230c4628dc2, exception_lineno );
    }

    // Put the previous frame back on top.
    popFrameStack();

    // Return the error.
    goto module_exception_exit;

    frame_no_exception_1:;
    {
        PyObject *tmp_assign_source_17;
        tmp_assign_source_17 = LIST_COPY( const_list_str_plain_load_basic_bindings_list );
        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain___all__, tmp_assign_source_17 );
    }
    {
        PyObject *tmp_assign_source_18;
        tmp_assign_source_18 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_1_if_no_repeat(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_if_no_repeat, tmp_assign_source_18 );
    }
    {
        PyObject *tmp_assign_source_19;
        tmp_assign_source_19 = MAKE_FUNCTION_prompt_toolkit$key_binding$bindings$basic$$$function_2_load_basic_bindings(  );



        UPDATE_STRING_DICT1( moduledict_prompt_toolkit$key_binding$bindings$basic, (Nuitka_StringObject *)const_str_plain_load_basic_bindings, tmp_assign_source_19 );
    }

    return MOD_RETURN_VALUE( module_prompt_toolkit$key_binding$bindings$basic );
    module_exception_exit:
    RESTORE_ERROR_OCCURRED( exception_type, exception_value, exception_tb );
    return MOD_RETURN_VALUE( NULL );
}
